<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('recording/{id}', function ($id) {
    $url = 'http://' . env('PBX_HOST') . '/api/recording.php?id=' . $id;
    $recording_file = App\Rest::get($url);
    header("Content-Type: audio/mpeg, audio/x-mpeg, audio/x-mpeg-3, audio/mpeg3, audio/wav, audio/x-wav, audio/vnd.wave");
    header('Content-Description: File Transfer');
    header('Content-Disposition: attachment; filename="' . $id . '.wav"');
    //header('Content-Length: ' . filesize($path));
    header('Accept-Ranges: bytes');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    ob_clean();
    flush();
    return $recording_file;
});

Route::get('call_log/{phone}', function ($phone) {
    $url = 'http://' . env('PBX_HOST') . '/api/call_log.php?phone=' . $phone;
    $data = App\Rest::get($url);
    return $data;
});

Route::post('calls', function (Request $request) {
    $from_date_original = $request->from_date;
    $to_date_original = $request->to_date;
    $from_date = date('Y-m-d');
    $to_date = date('Y-m-d');
    $trunk = $request->trunk;
    $direction = $request->direction;
    $tele = $request->tele;
    $param_string = '';
    if (!empty($trunk)) {
        $param_string .= '&trunk=' . $trunk;
    }
    if (!empty($direction)) {
        $param_string .= '&direction=' . $direction;
    }
    $url = 'http://' . env('PBX_HOST') . '/api/call_details.php?status=ANSWERED&ext=' . $ext . '&from_date=' . $from_date . '&to_date=' . $to_date . $param_string;
    $data = App\Rest::get($url);
    return $data;
});
Route::get('call_history/{lead_id}', 'CallLogController@getCallLog');
Route::get('forceReload', 'UtilsController@forceReload');
Route::get('getNamefromNumber/{number}', 'AsteriskController@getNamefromNumber');
Route::get('getExtFromNumber/{number}', 'AsteriskController@getExtFromNumber');
Route::match(['get', 'post'], '/subiz', 'SubizController@getResponse');
//Route::get('recording/{id}', 'CdrController@getCallRecordingFile');