<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::match(['get', 'post'], '/', 'LeadController@index')->middleware('auth');
Route::get('logout', function () {
    Auth::logout();
    return redirect('/');
});

Route::match(['get', 'post'], 'lead/import', 'LeadController@import')->name('lead.import')->middleware('auth');
Route::get('lead/export', 'LeadController@export')->middleware('auth');;
Route::match(['get', 'post'], 'lead/list', 'LeadController@search')->middleware('auth');
Route::match(['get', 'post'], 'account/list', 'AccountController@listAll')->middleware('auth');
Route::get('lead/edit/{id}', 'LeadController@edit')->middleware('auth');
Route::post('lead/assign', 'LeadController@assignLead')->name('lead.assign');
Route::get('import', 'FileImportController@index')->middleware('auth');
Route::get('importSuspectHistory', 'SuspectFileImportController@index')->middleware('auth');
Route::get('province', 'ProvinceController@index')->middleware('auth');
Route::get('lead/edit/{id}', 'LeadController@edit')->middleware('auth');
Route::get('lead/clearFilters', 'LeadController@clearFiltering')->name('lead.clearFilters')->middleware('auth');
Route::resource('user', 'UserController');

Route::match(['get', 'post'], 'user/create', 'UserController@create')->name('user.create')->middleware('auth');
Route::match(['get', 'post'], 'user/changepassword', 'UserController@changePassword')->name('user.changepassword')->middleware('auth');
Route::match(['get', 'post'], 'user/{id?}/resetpassword', 'UserController@resetPassword')->name('user.resetpassword')->middleware('auth');
Route::match(['get', 'post'], 'user/{user_id}/loginAs', 'UserController@loginAs')->name('user.loginAs')->middleware('auth');
Route::match(['get', 'post'], 'notification', 'UserController@getNotifications')->name('user.notification')->middleware('auth');
Route::match(['get', 'post'], 'user/logout', 'UserController@logout')->name('user.logout')->middleware('auth');
Route::resource('lead', 'LeadController');
Route::match(['get', 'post'], 'lead/export', 'LeadController@export')->name('lead.export')->middleware('auth');
Route::resource('account', 'AccountController');
Route::resource('group', 'GroupController');
Route::match(['get', 'post'], 'email-list/{id}/import', 'EmailMarketing\EmailListController@import')->name('email-list.import');
Route::match(['get', 'post'], 'email-campaign/{id}/import', 'EmailMarketing\EmailCampaignController@import')->name('email-campaign.import');
Route::get('email-list/{id}/export', 'EmailMarketing\EmailListController@export')->name('email-list.export');
Route::resource('email-list', 'EmailMarketing\EmailListController');
Route::get('email-campaign/{id}/showPdf', 'EmailMarketing\EmailCampaignController@showPdf')->name('email-campaign.showPdf');
Route::get('email-campaign/{id}/genPdf', 'EmailMarketing\EmailCampaignController@genPdf')->name('email-campaign.genPdf');
Route::get('email-campaign/{id}/regenAllPdf', 'EmailMarketing\EmailCampaignController@regenAllPdf')->name('email-campaign.regenAllPdf');
Route::get('email-campaign/{id}/genAllPdf', 'EmailMarketing\EmailCampaignController@genAllPdf')->name('email-campaign.genAllPdf');
Route::match(['get', 'post'], 'email-template/{id}/edit', 'EmailMarketing\EmailCampaignController@editEmailTemplate')->name('email-template.edit');
Route::resource('email-template', 'EmailMarketing\EmailTemplateController');
Route::get('email-campaign/{id}/showEmail', 'EmailMarketing\EmailCampaignController@showEmail')->name('email-campaign.showEmail');
Route::get('email-campaign/{id}/sendEmail', 'EmailMarketing\EmailCampaignController@sendEmail')->name('email.send');
Route::get('email-campaign/{id}/resend', 'EmailMarketing\EmailCampaignController@resend')->name('email.resend');
Route::get('email-campaign/{id}/sendTestEmail', 'EmailMarketing\EmailCampaignController@sendTestEmail')->name('email.sendTestEmail');
Route::get('email-campaign/{id}/progress', 'EmailMarketing\EmailCampaignController@progress')->name('email.progress');
Route::get('email-campaign/{id}/sendPrivateEmail', 'EmailMarketing\EmailCampaignController@sendPrivateEmail')->name('email.sendPrivate');
Route::resource('email-campaign', 'EmailMarketing\EmailCampaignController');
//Route::post('lead/{data?}', 'LeadController@index');
Route::match(['get', 'post'], 'lead/import', 'LeadController@import')->middleware('auth');

$s = 'social.';
Route::get('/social/redirect/{provider}', ['as' => $s . 'redirect', 'uses' => 'Auth\SocialController@getSocialRedirect']);
Route::get('/social/handle/{provider}', ['as' => $s . 'handle', 'uses' => 'Auth\SocialController@getSocialHandle']);

Route::get('ami', 'AsteriskController@call');
Auth::routes();

Route::get('/home', 'LeadController@index')->middleware('auth');
Route::get('/lead/index', 'LeadController@index')->middleware('auth');
Route::get('/report/campaign', 'ReportController@campaign')->name('report.campaign')->middleware('auth');
Route::get('/test', 'TestController@test')->name('test.test');
Route::get('/parse', 'TestController@parse_grade')->name('test.parse');
Route::get('/authorize', 'TestController@authorizeGoogle')->name('test.authorize');
Route::get('/report/online', 'ReportController@showOnlineStatus')->name('report.online')->middleware('auth');
Route::match(['get', 'post'], '/report/call_summary', 'ReportController@showCallSummaryReport')->name('report.call_summary')->middleware('auth');
Route::match(['get', 'post'], '/report/dmr', 'ReportController@showDmrReport')->name('report.dmr')->middleware('auth');
Route::get('/report/performance', 'ReportController@performance')->name('report.performance')->middleware('auth');
Route::match(['get', 'post'], '/report/lead', 'ReportController@showLeadReport')->name('report.lead')->middleware('auth');
Route::get('/utils/getSchools/{province}', 'UtilsController@getSchools');
Route::get('/utils/getSchools/{province}/{selected}', 'UtilsController@getSchools');
Route::get('/utils/getDistricts/{province}', 'UtilsController@getDistricts');
Route::get('/utils/getDistricts/{province}/{selected}', 'UtilsController@getDistricts');
Route::get('/utils/checkIfPhoneExists/{phone}', 'UtilsController@checkIfPhoneExists');
Route::get('/utils/getOnline/{user_id}', 'UtilsController@getOnline');
Route::get('/utils/getWeight', 'UtilsController@getWeight');
Route::get('/utils/getLeadCost', 'UtilsController@getLeadCost');
Route::get('/utils/checkCost', 'UtilsController@checkCost');
Route::get('/utils/checkList', 'UtilsController@checkList');
Route::get('/utils/getOnlineUsers', 'UtilsController@getOnlineUsers');
Route::get('/utils/fixAssignment', 'UtilsController@fix_assignment');
Route::get('/utils/setOnline/{user_id}', 'UtilsController@setOnline');
Route::get('/call/{phone}/{trunk?}', 'LeadController@call')->name('lead.call')->middleware('auth');
Route::get('/call_test/{phone}/{trunk?}', 'LeadController@call_test')->name('lead.call_test')->middleware('auth');
Route::get('/utils/getOnlineStatus/{user_id}', 'UtilsController@getOnlineStatus');
Route::match(['get', 'post'], '/sms/send', 'UtilsController@sendSms');
Route::resource('cost', 'MarketingCostController');
Route::resource('alarm', 'AlarmController');
Route::resource('cdr', 'CdrController');
Route::resource('class', 'AvailableClassController');
Route::get('class/{id}/toggle', 'AvailableClassController@toggle')->name('class.toggle')->middleware('auth');
Route::resource('source', 'SourceController');
Route::get('lead_status/{id}/toggle', 'LeadStatusController@toggle')->name('lead_status.toggle')->middleware('auth');
Route::resource('lead_status', 'LeadStatusController');
Route::get('call_status/{id}/toggle', 'CallStatusController@toggle')->name('call_status.toggle')->middleware('auth');
Route::resource('call_status', 'CallStatusController');
Route::get('source/{id}/toggle', 'SourceController@toggle')->name('source.toggle')->middleware('auth');
Route::resource('info_source', 'InfoSourceController');
Route::get('info_source/{id}/toggle', 'InfoSourceController@toggle')->name('info_source.toggle')->middleware('auth');
Route::resource('channel', 'ChannelController');
Route::get('channel/{id}/toggle', 'ChannelController@toggle')->name('channel.toggle')->middleware('auth');
Route::resource('campaign', 'CampaignController');
Route::get('campaign/{id}/toggle', 'CampaignController@toggle')->name('campaign.toggle')->middleware('auth');
Route::resource('campus', 'CampusController');
Route::get('campus/{id}/toggle', 'CampusController@toggle')->name('campus.toggle')->middleware('auth');
Route::resource('department', 'DepartmentController');
Route::get('department/{id}/toggle', 'DepartmentController@toggle')->name('department.toggle')->middleware('auth');
Route::resource('school', 'SchoolController');
Route::get('school/{id}/toggle', 'SchoolController@toggle')->name('school.toggle')->middleware('auth');
Route::resource('promotion', 'PromotionController');
Route::get('promotion/{id}/toggle', 'PromotionController@toggle')->name('promotion.toggle')->middleware('auth');
Route::post('alarm/set', 'AlarmController@set')->name('alarm.set')->middleware('auth');
Route::post('alarm/stop', 'AlarmController@stopAlarm')->name('alarm.stop')->middleware('auth');
Route::get('alarm/{user_id}/list', 'AlarmController@getAlarmList')->name('alarm.list')->middleware('auth');
Route::resource('sms', 'SmsController');
Route::resource('alarm', 'AlarmController')->middleware('auth');
Route::post('sms/stop', 'SmsController@stopSmsNotification')->name('sms.stop')->middleware('auth');
Route::get('sms/{user_id}/list', 'SmsController@getUnopenedSmsList')->name('sms.list')->middleware('auth');
Route::get('transfer/{department_id}/show', 'TransferredLeadController@showList')->name('transfer.show')->middleware('auth');
Route::get('transfer/notified', 'TransferredLeadController@notified')->name('transfer.notified')->middleware('auth');
Route::get('email/{id}/open', 'EmailMarketing\EmailController@open')->name('email.open');
Route::get('forceReload', 'UtilsController@forceReload');
Route::get('needReload', 'UtilsController@getForceReloadStatus');
Route::post('call_history/log', 'CallLogController@log');
Route::resource('calls', 'CallLogController');
Route::match(['get', 'post'], 'suspect/import', 'SuspectController@import')->name('suspect.import');
Route::resource('suspect', 'SuspectController');
Route::match(['get', 'post'], 'report/call_log', 'Report\CallLogReportController@index')->name('report.call_log');
Route::match(['get', 'post'], 'report/call_log/test', 'Report\CallLogReportController@test')->name('report.call_log.test');
Route::match(['get', 'post'], 'report/call_log/getUserByDepartment', 'Report\CallLogReportController@getUserByDepartment')->name('report.call_log.get_user_by_department');
Route::post('lead/sendSms', 'LeadController@sendSms')->name('lead.sms')->middleware('auth');
Route::post('lead/sendSmsMultiple', 'LeadController@sendSmsMultiple')->name('lead.sms_multiple')->middleware('auth');
Route::post('lead/getUserByDepartment', 'LeadController@getUserByDepartment')->name('lead.get_user_by_department')->middleware('auth');
Route::post('lead/getCampaignByCategory', 'LeadController@getCampaignByCategory')->name('lead.getCampaignByCategory')->middleware('auth');

Route::match(['get', 'post'], 'report/logSms', 'Report\LogSmsController@index')->name('report.logSms');
Route::match(['get', 'post'], 'report/logSms/detail', 'Report\LogSmsController@detail')->name('report.logSms.detail');
Route::match(['get', 'post'], 'report/leadInteractive', 'Report\LeadInteractiveController@index')->name('report.leadInteractive');
Route::match(['get', 'post'], 'report/leadInteractiveExport', 'Report\LeadInteractiveController@export')->name('report.leadInteractive.export');
Route::match(['get', 'post'], 'leadMarketing/landingPage', 'LeadMarketing\LandingPageController@index')->name('leadMarketing.landingPage');
Route::match(['get', 'post'], 'leadMarketing/website', 'LeadMarketing\WebsiteController@index')->name('leadMarketing.website');
Route::match(['get', 'post'], 'leadMarketing/facebook', 'LeadMarketing\FacebookController@index')->name('leadMarketing.facebook');
Route::match(['get', 'post'], 'leadMarketing/tawkto', 'LeadMarketing\TawktoController@index')->name('leadMarketing.tawkto');
Route::match(['get', 'post'], 'leadMarketing/tawkto/offline', 'LeadMarketing\TawktoController@offline')->name('leadMarketing.tawkto.offline');
Route::match(['get', 'post'], 'leadMarketing/ahaChat', 'LeadMarketing\AhaChatController@index')->name('leadMarketing.ahaChat');
Route::match(['get', 'post'], 'leadMarketing/offline', 'LeadMarketing\OfflineController@index')->name('leadMarketing.offline');

Route::match(['get', 'post'], 'notify/markAllAsRead', 'NotifyController@markAllAsRead')->name('notify.markAllAsRead');
Route::match(['get', 'post'], 'notify/markNewAsPending', 'NotifyController@markNewAsPending')->name('notify.markNewAsPending');
Route::match(['get', 'post'], 'notify/markAsUnRead', 'NotifyController@markAsUnRead')->name('notify.markAsUnRead');
Route::match(['get', 'post'], 'notify/markAsRead', 'NotifyController@markAsRead')->name('notify.markAsRead');
Route::match(['get', 'post'], 'notify/getNewNotify', 'NotifyController@getNewNotify')->name('notify.getNewNotify');
Route::match(['get', 'post'], 'notify', 'NotifyController@index')->name('notify.index');
Route::match(['get', 'post'], 'notify/checkNotifyLeadNewAndNoAnswer', 'NotifyController@checkNotifyLeadNewAndNoAnswer')->name('notify.checkNotifyLeadNewAndNoAnswer');
Route::match(['get', 'post'], 'notify/{id}', 'NotifyController@detail')->name('notify.detail');

Route::match(['get', 'post'], 'report/logChangeStatus', 'Report\LogChangeStatusController@index')->name('report.logChangeStatus');
Route::match(['get', 'post'], 'report/logChangeStatus/export', 'Report\LogChangeStatusController@export')->name('report.logChangeStatus.export');
Route::match(['get', 'post'], 'report/otb', 'Report\OtbController@index')->name('report.otb');
Route::match(['get', 'post'], 'report/otb/export', 'Report\OtbController@export')->name('report.otb.export');

Route::match(['get', 'post'], 'location/getDistrictByProvince', 'LocationController@getDistrictByProvince')->name('location.getDistrictByProvince');

Route::match(['get', 'post'], 'report/cdr', 'Report\CdrController@index')->name('report.cdr');
Route::match(['get', 'post'], 'report/cdr/getUserByDepartment', 'Report\CdrController@getUserByDepartment')->name('report.cdr.get_user_by_department');
Route::match(['post'], 'report/cdr/getPbxConfig', 'Report\CdrController@getPbxConfig')->name('report.cdr.get_pbx_config');
Route::match(['post'], 'report/cdr/saveCallLog', 'Report\CdrController@saveCallLog')->name('report.cdr.saveCallLog');

Route::match(['get', 'post'], 'report/leadSource', 'Report\LeadSourceController@index')->name('report.leadSource');
Route::match(['get', 'post'], 'report/leadSourceSearch', 'Report\LeadSourceController@search')->name('report.leadSource.search');

Route::match(['get', 'post'], 'report/leadDetail/smis', 'Report\LeadDetailController@smis')->name('report.leadDetail.smis');
Route::match(['get', 'post'], 'report/leadDetail/smis/export', 'Report\LeadDetailController@exportSMIS')->name('report.leadDetail.exportSMIS');
Route::match(['get', 'post'], 'report/leadDetail/gis', 'Report\LeadDetailController@gis')->name('report.leadDetail.gis');
Route::match(['get', 'post'], 'report/leadDetail/gis/export', 'Report\LeadDetailController@exportGIS')->name('report.leadDetail.exportGIS');
Route::match(['get', 'post'], 'report/leadDetail/getUserByDepartment', 'Report\LeadDetailController@getUserByDepartment')->name('report.leadDetail.getUserByDepartment');

Route::match(['get', 'post'], 'report/leadCare', 'Report\LeadCareController@index')->name('report.leadCare');
Route::match(['get', 'post'], 'report/leadCare/export', 'Report\LeadCareController@export')->name('report.leadCare.export');

Route::match(['get', 'post'], 'user/getUserByDepartmentGroup', 'UserController@getUserByDepartmentGroup')->name('user.getUserByDepartmentGroup');
Route::match(['get', 'post'], 'user/getUserByDepartment', 'UserController@getUserByDepartment')->name('user.getUserByDepartment');

Route::match(['get', 'post'], 'report/leadConvert', 'Report\LeadConvertController@index')->name('report.leadConvert');
Route::match(['get', 'post'], 'report/leadConvert/export', 'Report\LeadConvertController@export')->name('report.leadConvert.export');

Route::match(['get', 'post'], 'report/callLogDuration', 'Report\CallLogDurationController@index')->name('report.callLogDuration');
Route::match(['get', 'post'], 'report/callLogDuration/export', 'Report\CallLogDurationController@export')->name('report.callLogDuration.export');

Route::match(['post'], 'lead/createLogLeadCare', 'LeadController@createLogLeadCare')->name('lead.createLogLeadCare');

Route::match(['post'], 'suspect/convertToLead', 'SuspectController@convertToLead')->name('suspect.convertToLead');

Route::match(['post'], 'lead/updateStatus', 'LeadUpdateStatusController@index')->name('lead.updateStatus');

Route::match(['get', 'post'], 'report/leadConvertStatus', 'Report\LeadConvertStatusController@index')->name('report.leadConvertStatus');
Route::match(['get', 'post'], 'report/leadConvertStatus/export', 'Report\LeadConvertStatusController@export')->name('report.leadConvertStatus.export');

Route::match(['get', 'post'], 'adminControl', 'AdminControl@index')->name('adminControl.index');

Route::match(['get', 'post'], 'source/updateFilterType', 'SourceController@updateFilterType')->name('source.updateFilterType');
Route::match(['get', 'post'], 'source/updateFilterSource', 'SourceController@updateFilterSource')->name('source.updateFilterSource');

Route::match(['get', 'post'], 'report/bonusTDS', 'Report\BonusTDSController@index')->name('report.bonusTDS');
Route::match(['get', 'post'], 'report/bonusTDS/export', 'Report\BonusTDSController@export')->name('report.bonusTDS.export');

Route::match(['get'], 'leadMarketing/lms/getCommonCrm', 'LeadMarketing\LmsController@getCommonCrm')->name('leadMarketing.lms.getCommonCrm');
Route::match(['get'], 'leadMarketing/lms/getListRefCrm', 'LeadMarketing\LmsController@getListRefCrm')->name('leadMarketing.lms.getListRefCrm');
Route::match(['post'], 'leadMarketing/lms/submitRef', 'LeadMarketing\LmsController@submitRef')->name('leadMarketing.lms.submitRef');
Route::match(['get'], 'leadMarketing/lms/getRuleRef', 'LeadMarketing\LmsController@getRuleRef')->name('leadMarketing.lms.getRuleRef');
Route::match(['get'], 'leadMarketing/lms/checkUpdate', 'LeadMarketing\LmsController@checkUpdate')->name('leadMarketing.lms.checkUpdate');
Route::match(['get'], 'leadMarketing/lms/commons', 'LeadMarketing\LmsController@commons')->name('leadMarketing.lms.commons');

Route::resource('lead_group', 'LeadGroupController');
Route::get('lead_group/{id}/toggle', 'LeadGroupController@toggle')->name('lead_group.toggle')->middleware('auth');

Route::get('/commons/smis', function() { return File::get(public_path() . '/static_page/commons/smis.html');});
Route::get('/commons/tds', function() { return File::get(public_path() . '/static_page/commons/tds.html');});
Route::get('/commons/asc', function() { return File::get(public_path() . '/static_page/commons/asc.html');});

Route::match(['post'], 'syncLead', 'SyncLeadController@index')->name('syncLead');
