$('#search_button').click(function () {
    window.history.pushState("", "", '/lead');
    $('#search_form').attr('action', '');
});
$('#export_button').click(function () {
    $('#search_form').attr('action', 'lead/list');
    $('#export_action').val('export');
    console.log($('#export_action').val());
    $('#search_mode').val('advanced_search');
    $('#search_form').submit();
});
$('#search_form').submit(function (event) {
    var action = $('#search_form').attr('action');
    $('#search_mode').val('advanced_search');
    console.log(action);
    if (!action) {
        event.preventDefault();
        $('#gsearch').val('');
        if(typeof turnOffCheckingUpdate === 'function'){
            turnOffCheckingUpdate();
        }
        $('#main_grid').DataTable().ajax.reload();
    }
    //$('#search_form').attr('action', '');
});
$(function () {
    var full_month_names = new Array("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");
    $.datepicker.regional["vi-VN"] =
        {
            closeText: "Đóng",
            prevText: "Trước",
            nextText: "Sau",
            currentText: "Hôm nay",
            monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
            monthNamesShort: full_month_names,
            dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
            dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
            dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
            weekHeader: "Tuần",
            dateFormat: "dd/mm/yy",
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: "",
            changeMonth: true,
        };
    $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
    $("#start_date").datepicker();
    $("#end_date").datepicker();
    $("#birthdate").datepicker({defaultDate: '01/01/2000'});
    $("#active_from").datepicker();
    $("#active_to").datepicker();
    $("#enrolled_from").datepicker();
    $("#enrolled_to").datepicker();
    $("#modified_from").datepicker();
    $("#modified_to").datepicker();
    $("#assigned_from").datepicker();
    $("#assigned_to").datepicker();
    $("#showup_school_from").datepicker();
    $("#showup_school_to").datepicker();
    $("#showup_seminor_from").datepicker();
    $("#showup_seminor_to").datepicker();
    $("#join_as_guest_from").datepicker();
    $("#join_as_guest_to").datepicker();
    $("#hot_lead_from").datepicker();
    $("#hot_lead_to").datepicker();
    $("#refuse_from").datepicker();
    $("#refuse_to").datepicker();
    $("#qualified_from").datepicker();
    $("#qualified_to").datepicker();
    $(".call-btn").click(function () {
        //e.preventDefault();
        console.log('abc');
        // console.log($(this).data('id'));
        // var edit_form_request = $.ajax({
        // url: "/lead/edit",
        // method: "GET",
        // data: { id : $(this).data('id') },
        // dataType: "html"
        // });
        // edit_form_request.done(function( msg ) {
        // $('#edit_form_container').html(msg);
        // });
    });
});

function change_search_form_visibility() {
    $('#search_form').toggle();
    if ($('#search_form').is(':visible'))
        $('#hide_search_form').html('<i class="fa fa-eye-slash" aria-hidden="true"></i> Ẩn bộ lọc');
    else
        $('#hide_search_form').html('<i class="fa fa-eye" aria-hidden="true"></i> Hiện bộ lọc');
}

$('#hide_search_form').click(function () {
    change_search_form_visibility();
});
$('#hide_search_form_2').click(function () {
    change_search_form_visibility();
});
$('#edit_form').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    console.log(event.relatedTarget);
    var lead_id = button.data('id');
    var modal = $(this);
    modal.find('.modal-title').text('Chắm sóc lead id ' + lead_id);
    //modal.find('.modal-body input').val(recipient)
});