<?php

namespace App;

use PHPExcel;
use PHPExcel_IOFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\ManualLeadAssigner;

class Importer
{
    private static $marTViet = array("à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ", "ì", "í", "ị", "ỉ", "ĩ", "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ", "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ", "ỳ", "ý", "ỵ", "ỷ", "ỹ", "đ", "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ", "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ", "Ì", "Í", "Ị", "Ỉ", "Ĩ", "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ", "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ", "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ", "Đ");
    private static $marKoDau = array("a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "i", "i", "i", "i", "i", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "y", "y", "y", "y", "y", "d", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "I", "I", "I", "I", "I", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "Y", "Y", "Y", "Y", "Y", "D");
    private static $tohop = array("à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ", "ì", "í", "ị", "ỉ", "ĩ", "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ", "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ", "ỳ", "ý", "ỵ", "ỷ", "ỹ", "đ", "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ", "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ", "Ì", "Í", "Ị", "Ỉ", "Ĩ", "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ", "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ", "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ", "Đ");

    function proper_name($string, $encoding = 'UTF-8')
    {
        $return_string = mb_strtolower($string, $encoding);
        $words = explode(' ', $return_string);
        foreach ($words as $index => $word) {
            $words[$index] = mb_strtoupper(mb_substr($word, 0, 1, $encoding), $encoding) . mb_substr($word, 1, mb_strlen($word, $encoding), $encoding);
        }
        $return_string = implode(' ', $words);
        return $return_string;
    }

    function prepare_name($name)
    {
        $name = str_replace(self::$tohop, self::$marTViet, $name);
        $name = $this->proper_name(mb_strtolower($name, 'UTF-8'));
        return $name;
    }

    function validate_phong($phong)
    {
        $phong_list = array('HN', 'DN', 'HCM', 'TT', 'CT');
        if (in_array($phong, $phong_list))
            return true;
        return false;
    }

    function validate_phone($phone)
    {
        //$phone = $this->sanitize_phone($phone);
        if (strlen($phone) != 10 && strlen($phone) != 11)
            return false;
        return true;
    }

    function romanize_string($string)
    {
        $string = trim($string);
        $string = str_replace(self::$marTViet, self::$marKoDau, $string);
        $string = preg_replace('/\./', '', $string);
        return $string;
    }

    function legacy_romanize_string($string)
    {
        $string = trim($string);
        $string = str_replace(self::$marTViet, self::$marKoDau, $string);
        $string = preg_replace("/[^A-Za-z0-9 ]/", '', $string);
        return $string;
    }

    function sanitize_province($province)
    {
        $province = $this->romanize_string($province);
        $province = str_replace(' ', '', $province);
        $province = str_replace('-', '', $province);
        $province = str_replace('TP.', '', $province);
        switch ($province) {
            case 'TPHOCHIMINH':
                $province = 'HOCHIMINH';
                break;
            case 'VUNGTAU':
                $province = 'BARIAVUNGTAU';
                break;
            case 'HUE':
                $province = 'THUATHIENHUE';
                break;
            case 'DACLAC':
                $province = 'DAKLAK';
                break;
            case 'DACNONG':
                $province = 'DAKNONG';
                break;
        }
        return strtoupper($province);
    }

    function validate_province($province)
    {
        ;
        $province = $this->sanitize_province($province);
        $count = DB::table('fptu_province')->where('name_upper', $province)->count();
        //var_dump(DB::table('fptu_province')->where('name_upper', $province)->toSql());
        //var_dump($province);
        if ($count)
            return true;
        return false;
    }

    function validate_source($source)
    {
        $source_check = DB::table('fptu_source')->where('name', $source);
        if ($source_check->count())
            return true;
        return false;
    }

    function validate_channel($channel)
    {
        $channel_check = DB::table('fptu_channel')->where('name', $channel);
        if ($channel_check->count())
            return true;
        return false;
    }

    function validate_campaign($campaign)
    {
        $campaign_check = DB::table('fptu_campaign')->where('name', $campaign);
        if ($campaign_check->count())
            return true;
        return false;
    }

    function validate_group($group_name)
    {
        $group_check = DB::table('fptu_group')->where('name', $group_name);
        if ($group_check->count())
            return true;
        return false;
    }

    function validate_campus($campus_code)
    {
        $campus_code = trim(strtoupper($campus_code));
        $check = DB::table('fptu_campus')->where('code', $campus_code)->count();
        if ($check)
            return true;
        return false;
    }

    function validate_assignee($assignee)
    {
        $count = DB::table('fptu_user')->where('username', $assignee)->count();
        if ($count)
            return true;
        return false;
    }

    function check_duplicate_phone($number)
    {
        $count = DB::table('fptu_lead')->where('phone1', $number)->orWhere('phone2', $number)->orWhere('phone3', $number)->orWhere('phone4', $number)->count();
        if ($count > 0)
            return true;
        return false;
    }

    function validate_date($date_string)
    {
        $day = 0;
        $month = 0;
        $year = 0;
        if (substr_count($date_string, '-') == 2) {
            $date_chunks = explode('-', $date_string);
            if (count($date_chunks) == 3) {
                $day = intval(trim($date_chunks[1]));
                $month = intval(trim($date_chunks[0]));
                $year = intval(trim($date_chunks[2]));
            }
        }
        if (substr_count($date_string, '/') == 2) {
            $date_chunks = explode('/', $date_string);
            if (count($date_chunks) == 3) {
                $day = intval(trim($date_chunks[0]));
                $month = intval(trim($date_chunks[1]));
                $year = intval(trim($date_chunks[2]));

            }
        }
        if (($year >= 45) && ($year <= 99)) {
            $year = 1900 + $year;
        } else {
            $year = 2000 + $year;
        }
        if (($day != 0) && ($month != 0))
            return checkdate($month, $day, $year);
        return false;
    }

    function parse_date($date_string)
    {
        $day = 0;
        $month = 0;
        $year = 0;
        if (substr_count($date_string, '-') == 2) {
            $date_chunks = explode('-', $date_string);
            if (count($date_chunks) == 3) {
                $day = intval(trim($date_chunks[1]));
                $month = intval(trim($date_chunks[0]));
                $year = intval(trim($date_chunks[2]));
            }
        }
        if (substr_count($date_string, '/') == 2) {
            $date_chunks = explode('/', $date_string);
            if (count($date_chunks) == 3) {
                $day = intval(trim($date_chunks[0]));
                $month = intval(trim($date_chunks[1]));
                $year = intval(trim($date_chunks[2]));

            }
        }
        if ($year <= 99) {
            if ($year >= 45) {
                $year = 1900 + $year;
            } else {
                $year = 2000 + $year;
            }
        }
        if (checkdate($month, $day, $year))
            return $year . '-' . $month . '-' . $day;

        return '';

    }

    function import_row($row)
    {
        $data = array();
        //var_dump($row);return;
        $department = strtoupper(trim($row[2]));
        $message = '';
        $status = 'ok';
        $duplicate_message = '';
        $source = strtoupper(trim($row[3]));
        if (empty($source) || !$this->validate_source($source)) {
            $message .= 'Nguồn dẫn \'<strong>' . $source . '</strong>\' không hợp lệ. ';
            $status = 'failed';
        }
        $channel = trim($row[4]);
        if (!empty($channel) && !$this->validate_channel($channel)) {
            $message .= 'Kênh quảng cáo \'<strong>' . $channel . '</strong>\' không hợp lệ. ';
            $status = 'failed';
        }
        $campaign = trim($row[5]);
        if (!empty($campaign) && !$this->validate_campaign($campaign)) {
            $message .= 'Chiến dịch quảng cáo \'<strong>' . $campaign . '</strong>\' không hợp lệ. ';
            $status = 'failed';
        }
        $keyword_string = trim($row[6]);
        $keyword_id = 0;
        if (!empty($keyword_string)) {
            $keyword = Keyword::firstOrCreate(['name' => $keyword_string]);
            $keyword->created_by = Auth::id();
            $keyword_id = $keyword->id;
        }
        $mobile = $this->sanitize_phone($row[9]);
        $parent1_phone = $this->sanitize_phone($row[10]);
        if (empty($mobile) && empty($parent1_phone)) {
            $message .= 'Thiếu cả 2 số điện thoại liên hệ. ';
            $status = 'failed';
            //$data['message'] =  $message;
        } elseif (!$this->validate_phone($mobile) && !$this->validate_phone($parent1_phone)) {
            $message .= 'Số điện thoại di động \'<strong>' . $mobile . '</strong>\' và phụ huynh 1 \'<strong>' . $parent1_phone . '</strong>\' đều không hợp lệ. ';
            $status = 'failed';
            //$data['message'] =  $message;
        } else {
            if (!empty($mobile)) {
                if ($this->check_duplicate_phone($mobile)) {
                    $message .= 'Số điện thoại chính \'<strong>' . $mobile . '</strong>\' bị trùng. ';
                    $status = 'dup';
                }
            }
            if (!empty($parent1_phone)) {
                if ($this->check_duplicate_phone($parent1_phone)) {
                    $message .= 'Số điện thoại phụ \'<strong>' . $parent1_phone . '</strong>\' bị trùng. ';
                    $status = 'dup';
                }
            }
            $phone_list = array();
            if (!empty($mobile))
                $phone_list[] = $mobile;
            if (!empty($parent1_phone))
                $phone_list[] = $parent1_phone;
            $lead_check = Lead::whereIn('phone1', $phone_list)->orWhereIn('phone2', $phone_list)->orWhereIn('phone3', $phone_list)->orWhereIn('phone4', $phone_list);
            if ($lead_check->count() > 0) {
                $lead_check = $lead_check->first();
                // get assignment list
                if ($lead_check->status == 5) {
                    if (!empty($lead_check->activator)) {
                        $activator_username = User::find($lead_check->activator)->username;
                        $duplicate_message = $activator_username;
                    }
                } else {
                    $assignment_list = DB::table('fptu_user')
                        ->join('fptu_lead_assignment', 'fptu_user.id', '=', 'fptu_lead_assignment.user_id')
                        ->where('fptu_lead_assignment.lead_id', $lead_check->id)
                        ->groupBy('fptu_lead_assignment.lead_id')
                        ->select(DB::raw('group_concat(fptu_user.username) as user_list'))
                        ->whereNull('fptu_user.deleted_at')
                        ->whereNull('fptu_lead_assignment.deleted_at')
                        ->first();
                    if (!empty($assignment_list) || !empty($assignment_list->user_list)) {
                        $duplicate_message = $assignment_list->user_list;
                    }
                }
                //var_dump($lead->import_count);
                if (!empty($lead_check->import_count)) {
                    $lead_check->import_count = $lead_check->import_count + 1;
                    //var_dump($lead_check->import_count);
                } else
                    $lead_check->import_count = 1;
                if (!empty($source)) {
                    $source_object = Source::where('name', $source)->first();
                    if (!empty($source_object) && !empty($source_object->id)) {
                        $source_id = $source_object->id;
                    }
                }
                if (!empty($channel)) {
                    $channel_id = Channel::where('name', $channel)->first();
                    if (!empty($channel_id) && $channel_id->id) {
                        $channel_id = $channel_id->id;
                    } else {
                        $channel_id = '';
                    }
                }
                if (!empty($campaign) && !empty(Campaign::where('name', $campaign)->first()->id))
                    $campaign_id = Campaign::where('name', $campaign)->first()->id;
                if (!empty($source_id)) {
                    $lead_source = new LeadSource;
                    $lead_source->lead_id = $lead_check->id;
                    $lead_source->source_id = $source_id;
                    $lead_source->created_by = Auth::id();
                    $lead_source->save();
                }
                if (!empty($channel_id)) {
                    $lead_channel = new LeadChannel;
                    $lead_channel->lead_id = $lead_check->id;
                    $lead_channel->channel_id = $channel_id;
                    //$lead_channel->created_by = Auth::id();
                    $lead_channel->save();
                }
                if (!empty($campaign_id)) {
                    $lead_campaign = new LeadCampaign;
                    $lead_campaign->lead_id = $lead_check->id;
                    $lead_campaign->campaign_id = $campaign_id;
                    //$lead_campaign->created_by = Auth::id();
                    $lead_campaign->save();
                }
                if (!empty($keyword_id)) {
                    $lead_keyword = new LeadKeyword;
                    $lead_keyword->lead_id = $lead_check->id;
                    $lead_keyword->keyword_id = $keyword_id;
                    $lead_keyword->created_by = Auth::id();
                    $lead_keyword->save();
                }
                if ((empty($lead_check->email) || (strtolower($lead_check->email) == 'chưa có')) && !empty($row[14])) {
                    $lead_check->email = trim($row[11]);
                }
                if ((empty($lead_check->facebook) || (strtolower($lead_check->facebook) == 'chưa có')) && !empty($row[15])) {
                    $lead_check->facebook = trim($row[12]);
                }
                $lead_check->last_duplicate = date('Y-m-d H:i:s');
                $lead_check->new_duplicate = 1;
                $lead_check->notes .= trim($row[18]);
                $lead_check->save();
            }
        }
        $birthdate = trim($row[7]);
        if (!empty($birthdate)) {
            if (!$this->validate_date($birthdate)) {
                $message .= 'Ngày sinh \'<strong>' . $birthdate . '</strong>\' không hợp lệ. ';
                $status = 'failed';
            }
        }
        $assignee = trim($row[14]);
        if (!empty($assignee)) {
            if (!$this->validate_assignee($assignee)) {
                $message .= 'Tên telesales \'<strong>' . $assignee . '</strong>\' không hợp lệ. ';
                if ($status != 'dup')
                    $status = 'failed';
            }
        }
        $province = $this->sanitize_province(trim($row[17]));
        if (!empty($province) && !$this->validate_province($province)) {
            $message .= 'Tên tỉnh \'<strong>' . $province . '</strong>\' không hợp lệ. ';
            if ($status != 'dup')
                $status = 'failed';

        }
        $province_data = null;
        if (!empty($province)) {
            $province_data = Province::where('name_upper', $province)->first();
        }
        if (!empty($message)) {
            $data['result'] = $status;
            $data['message'] = $message;
            $data['duplicate_message'] = $duplicate_message;
        } else {
            $data['result'] = 'ok';
            $data['message'] = 'Thành công!';
            $lead = new Lead;
            $lead->name = $this->prepare_name($row[10]);
            if (!empty($mobile))
                $lead->phone1 = $mobile;
            if (!empty($parent1_phone))
                $lead->phone2 = $parent1_phone;
            $lead->email = $row[14];
            $lead->facebook = $row[15];
            $lead->status = '';
            $source_id = Source::where('name', $source)->first()->id;
            if (!empty($channel))
                $channel_id = Channel::where('name', $channel)->first()->id;
            if (!empty($campaign))
                $campaign_id = Campaign::where('name', $campaign)->first()->id;
            if (!empty($birthdate))
                $lead->birthdate = $this->parse_date($birthdate);
            if (!empty($assignee))
                $assignee_id = User::where('username', $assignee)->first()->id;
            $lead->province_id = $province_data->id;
            $lead->department_id = Department::where('name', $department)->first()->id;
            $lead->major = $row[15];
            $lead->school = trim($row[16]);
            $lead->notes = $row[18];
            $lead->url_referrer = $row[19];
            $lead->url_request = $row[20];
            $lead->created_by = Auth::user()->id;
            $lead->status = 1;
            $lead->save();
            if (!empty($source_id)) {
                $lead_source = new LeadSource;
                $lead_source->lead_id = $lead->id;
                $lead_source->source_id = $source_id;
                $lead_source->created_by = Auth::id();
                $lead_source->save();
            }
            if (!empty($channel_id)) {
                $lead_channel = new LeadChannel;
                $lead_channel->lead_id = $lead->id;
                $lead_channel->channel_id = $channel_id;
                //$lead_channel->created_by = Auth::id();
                $lead_channel->save();
            }
            if (!empty($campaign_id)) {
                $lead_campaign = new LeadCampaign;
                $lead_campaign->lead_id = $lead->id;
                $lead_campaign->campaign_id = $campaign_id;
                //$lead_campaign->created_by = Auth::id();
                $lead_campaign->save();
            }
            if (!empty($keyword_id)) {
                $lead_keyword = new LeadKeyword;
                $lead_keyword->lead_id = $lead->id;
                $lead_keyword->keyword_id = $keyword_id;
                $lead_keyword->created_by = Auth::id();
                $lead_keyword->save();
            }
            if (!empty($assignee))

                if (!empty($assignee_id) && !empty($lead->id)) {
                    $manual_assigner = new ManualLeadAssigner;
                    $manual_assigner->user_id = $assignee_id;
                    $manual_assigner->lead_id = $lead->id;
                    $manual_assigner->save();
                }
        }

        return $data;
    }


    function sanitize_phone($mobile)
    {
        $mobile = preg_replace('/[^\d]/', '', $mobile);
        if (substr($mobile, 0, 1) != '0')
            $mobile = '0' . $mobile;
        if ($mobile == '0')
            return '';
        return $mobile;
    }

    function dump_file($row_list = array(), $file_name)
    {
        $template_path = public_path() . '/templates/template.xlsx';
        $output_path = public_path() . '/uploads/';
        if (file_exists($template_path)) {
            $objPHPExcel = PHPExcel_IOFactory::load($template_path);
            $objPHPExcel->getActiveSheet()->fromArray($row_list, '', 'A2');
            //Storage::disk('local')->put('Failed_Import.xlsx', 'Contents');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
            $objWriter->save($output_path . $file_name);
        } else {
            echo '<div class="alert alert-danger">Lỗi không tìm thấy file template</div>';
        }
    }

    public
    function import($path)
    {
        $now_date = date('Y-m-d H:i:s');
        $now_string = date('YmdHis');
        if (!file_exists($path))
            return '<div class="alert alert-danger">Không tìm thấy file import!</div>';
        ini_set('display_errors', 0);
        //ini_set('error_reporting', E_ALL);
        error_reporting(E_ERROR | E_PARSE);
        $objPHPExcel = PHPExcel_IOFactory::load($path);
        $data = $objPHPExcel->getActiveSheet()->toArray();
        $result = array('message' => '', 'data' => array());
        $first_row = true;
        $row_number = 2;
        $success = 0;
        $failed = 0;
        $failed_row_list = array();
        $dup_row_list = array();
        $success_row_list = array();
        $duplicated = 0;
        $total = count($data) - 1;
        $message = '';
        foreach ($data as $row) {
            if ($first_row) {
                $first_row = false;
                continue;
            }
            $import_result = $this->import_row($row);
            //var_dump($import_result);
            $banner = 'Dòng ' . $row_number . ': ';
            if ($import_result['result'] == 'ok') {
                $success++;
                $message .= '<div class="alert alert-success">' . $banner . 'Thành công!</div>';
                $success_row_list[] = $row;

            } elseif ($import_result['result'] == 'dup') {
                $duplicated++;
                $row[] = strip_tags($import_result['message']);
                $row[] = strip_tags($import_result['duplicate_message']);
                $dup_row_list[] = $row;
                $message .= '<div class="alert alert-warning">' . $banner . $import_result['message'] . '</div>';
            } elseif ($import_result['result'] == 'failed') {
                $failed++;
                $row[] = strip_tags($import_result['message']);
                $failed_row_list[] = $row;
                $message .= '<div class="alert alert-danger">' . $banner . $import_result['message'] . '</div>';
            }
            $row_number++;
        }
        $original_file_name = 'Import_Original_' . $now_string . '.xlsx';
        $original_file_path = public_path() . '/uploads/' . $original_file_name;
        //var_dump($original_file_path);
        //var_dump($path);
        //$archived = move_uploaded_file($path, $original_file_path);
        $archived = true;
        //var_dump($archived);
        if (!$archived)
            $message .= '<div class="alert alert-danger">Đã có lỗi trong quá trình lưu trữ lại file gốc.</div>';
        if ($failed > 0) {
            $error_file_name = 'Import_Error_' . $now_string . '.xlsx';
            $this->dump_file($failed_row_list, $error_file_name);
        }
        if ($duplicated > 0) {
            $dup_file_name = 'Import_Duplicate_' . $now_string . '.xlsx';
            $this->dump_file($dup_row_list, $dup_file_name);
        }
        if ($success > 0) {
            $success_file_name = 'Import_Success_' . $now_string . '.xlsx';
            $this->dump_file($success_row_list, $success_file_name);
        }
        $result['data']['success'] = $success;
        if ($success)
            $result['data']['success_file_name'] = $success_file_name;
        $result['data']['failed'] = $failed;
        if ($failed)
            $result['data']['error_file_name'] = $error_file_name;
        $result['data']['dup'] = $duplicated;
        if ($duplicated)
            $result['data']['duplicate_file_name'] = $dup_file_name;
        $result['data']['total'] = $total;
        $result['message'] = $message;
        $import = new FileImport;
        $import->original_file_name = $original_file_name;
        $import->original_row_num = $total;
        if (!empty($error_file_name)) {
            $import->error_file_name = $error_file_name;
            $import->error_row_num = $failed;
        }
        if (!empty($dup_file_name)) {
            $import->duplicate_file_name = $dup_file_name;
            $import->duplicate_row_num = $duplicated;
        }
        if (!empty($success_file_name)) {
            $import->success_file_name = $success_file_name;
            $import->success_row_num = $success;
        }
        $import->created_at = $now_date;
        $import->created_by = Auth::id();
        $import->save();
        return $result;
    }
}