<?php
require __DIR__.'/../bootstrap/autoload.php';
use Dompdf\Dompdf;

$html = '<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
    <p style="font-family: Times, sans-serif;">Lê Đại Hoàng tôi là Hoàng đến từ TP. Hà Nội</p>
</body>
</html>';
$dompdf = new Dompdf();
$dompdf->load_html($html, 'UTF-8');
$dompdf->set_paper('A4');
$dompdf->render();
file_put_contents('test.pdf', $dompdf->output());