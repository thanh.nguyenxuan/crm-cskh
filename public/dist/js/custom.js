var tableToExcel = (function () {
    var uri = 'data:application/vnd.ms-excel;base64,'
        , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
        , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
        , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) };
    return function (table, sheetname, filename) {
        if (!table.nodeType) table = document.getElementById(table);
        var ctx = { worksheet: sheetname || 'Worksheet', table: table.innerHTML };

        var link = document.createElement('a');
        link.download = filename;
        link.href = uri + base64(format(template, ctx));
        link.click();
    }
})();

/*
* init library
* add class table-freeze-grid to the grid that contain table
* add class table-freeze to table
*/
function initFreezeTable(gridId, columnNum, registeredSelectedRowEvent){
    if(typeof FreezeTable === 'undefined'){
        console.log('freeze-table js library is missing');
        return;
    }
    var grid, table, tableClone;
    if(typeof gridId === 'undefined'){
        grid = $(".table-freeze-grid");
    }else{
        grid = $("#"+gridId);
    }
    table = $(".table-freeze-grid .table-freeze");
    table = grid.find('.table-freeze');
    if(typeof columnNum === 'undefined'){
        columnNum = 1;
    }
    grid.freezeTable({
        'columnNum' : columnNum,
    });
    tableClone = grid.find('.clone-column-table-wrap table');
    if(typeof registeredSelectedRowEvent == 'undefined'){
        registeredSelectedRowEvent = true;
    }
    if(registeredSelectedRowEvent){
        table.on('click', 'tbody tr', function(){
            var index = $(this).index()+1;
                if($(this).hasClass('selected')){
                    tableClone.find('tbody tr:nth-child('+index+')').removeClass('selected');
                }else{
                    tableClone.find('tbody tr.selected').removeClass('selected');
                    tableClone.find('tbody tr:nth-child('+index+')').addClass('selected');
                }
            })
            .on('mouseover', 'tbody tr', function () {
                var index = $(this).index()+1;
                tableClone.find('tbody tr:nth-child('+index+')').addClass('hover');
            })
            .on('mouseout',' tbody tr', function(){
                var index = $(this).index()+1;
                tableClone.find('tbody tr:nth-child('+index+')').removeClass('hover');
            });
        tableClone.on('click', 'tbody tr', function(){
                var index = $(this).index()+1;
                if($(this).hasClass('selected')){
                    table.find('tbody tr:nth-child('+index+')').removeClass('selected');
                }else{
                    table.find('tbody tr.selected').removeClass('selected');
                    table.find('tbody tr:nth-child('+index+')').addClass('selected');
                }
            })
            .on('mouseover', 'tbody tr', function () {
                var index = $(this).index()+1;
                table.find('tbody tr:nth-child('+index+')').addClass('hover');
            })
            .on('mouseout',' tbody tr', function(){
                var index = $(this).index()+1;
                table.find('tbody tr:nth-child('+index+')').removeClass('hover');
            });
    }else{
        table.on('click', 'tbody tr', function(){
                var index = $(this).index()+1;
                if($(this).hasClass('selected')){
                    $(this).removeClass('selected');
                    tableClone.find('tbody tr:nth-child('+index+')').removeClass('selected');
                }else{
                    table.find('tbody tr.selected').removeClass('selected');
                    tableClone.find('tbody tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    tableClone.find('tbody tr:nth-child('+index+')').addClass('selected');
                }
            })
            .on('mouseover', 'tbody tr', function () {
                var index = $(this).index()+1;
                tableClone.find('tbody tr:nth-child('+index+')').addClass('hover');
            })
            .on('mouseout',' tbody tr', function(){
                var index = $(this).index()+1;
                tableClone.find('tbody tr:nth-child('+index+')').removeClass('hover');
            });
        tableClone.on('click', 'tbody tr', function(){
                var index = $(this).index()+1;
                if($(this).hasClass('selected')){
                    $(this).removeClass('selected');
                    table.find('tbody tr:nth-child('+index+')').removeClass('selected');
                }else{
                    table.find('tbody tr.selected').removeClass('selected');
                    tableClone.find('tbody tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                    table.find('tbody tr:nth-child('+index+')').addClass('selected');
                }
            })
            .on('mouseover', 'tbody tr', function () {
                var index = $(this).index()+1;
                table.find('tbody tr:nth-child('+index+')').addClass('hover');
            })
            .on('mouseout',' tbody tr', function(){
                var index = $(this).index()+1;
                table.find('tbody tr:nth-child('+index+')').removeClass('hover');
            });
    }

}
function lockColumn(grid_id, columnNum){
    initFreezeTable(grid_id, columnNum);
}
function unlockColumn(grid_id){
    initFreezeTable(grid_id);
}

/**
 * scroll by mouse drag
 * @param element
 */
function enableMouseScroll(element){
    let slider = document.querySelector(element);
    let isDown = false;
    let startX;
    let scrollLeft;

    slider.addEventListener('mousedown', (e) => {
        isDown = true;
        slider.classList.add('active');
        startX = e.pageX - slider.offsetLeft;
        startY = e.pageY - slider.offsetTop;
        scrollLeft = slider.scrollLeft;
        slider.style.cursor = 'all-scroll';
    });
    slider.addEventListener('mouseleave', () => {
        isDown = false;
        slider.classList.remove('active');
        slider.style.cursor = 'default';
    });
    slider.addEventListener('mouseup', () => {
        isDown = false;
        slider.classList.remove('active');
        slider.style.cursor = 'default';
    });
    slider.addEventListener('mousemove', (e) => {
        if(!isDown) return;
        e.preventDefault();
        const x = e.pageX - slider.offsetLeft;
        const walkX = (x - startX) * 3; //scroll-fast
        slider.scrollLeft = scrollLeft - walkX;
    });
}

const INPUT_FILTER_INTEGER = /^\d*$/;

function setInputFilter(input, filter) {
    ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
        input.addEventListener(event, function() {
            if (filter.test(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            }
        });
    });
}

$(document).ready(function () {
    $("input[type=submit]").on('click', function(){$(this).addClass('disabled')});
});