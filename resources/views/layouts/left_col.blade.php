<?php
$lead_redirect_url = \App\Dashboard::getLeadRedirectUrl();
?>

<div class="col-md-3 left_col menu_fixed">
    <div class="left_col scroll-view menu_fixed">

        <div class="navbar nav_title" style="border: 0;">
            <a href="/" class="site_title">
                CRM
            </a>
        </div>

        <div class="clearfix"></div>

        <div class="ln_solid no-margin"></div>

        <br/>

        <div id="sidebar-menu" class="main_menu_side hidden-print main_menu" style="min-height: 1200px;">
            <div class="menu_section">
                <ul class="nav side-menu">
                    <li><a href="/"><i class="fa fa-dashboard"></i>Dashboard</a></li>

                    <li><a href="javascript:void(0);"><i class="fa fa-search"></i>Lọc phễu kinh doanh<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li>
                                <a class="no_pad" href="{{ $lead_redirect_url }}{{ Auth::user()->hasRole('telesales') ? ('&assignee[]='.Auth::user()->id) : '' }}">
                                    <span class="btn btn-sm btn-default">Lead mới</span>
                                </a>
                            </li>
                            <li><a class="no_pad" href="{{ route('lead.index') }}?clear_filter=1&status_id[]={{ \App\LeadStatus::STATUS_HOT_LEAD }}{{ Auth::user()->hasRole('telesales') ? ('&assignee[]='.Auth::user()->id) : '' }}"><span class="btn btn-sm btn-danger">Hot Lead</span></a></li>
                            <li><a class="no_pad" href="{{ route('lead.index') }}?clear_filter=1&status_id[]={{ \App\LeadStatus::STATUS_SHOWUP_SCHOOL }}{{ Auth::user()->hasRole('telesales') ? ('&assignee[]='.Auth::user()->id) : '' }}"><span class="btn btn-sm btn-info">Show up (SU)</span></a></li>
                            <li><a class="no_pad" href="{{ route('lead.index') }}?clear_filter=1&status_id[]={{ \App\LeadStatus::STATUS_WAITLIST }}{{ Auth::user()->hasRole('telesales') ? ('&assignee[]='.Auth::user()->id) : '' }}"><span class="btn btn-sm btn-warning">Đặt cọc (WL)</span></a></li>
                            <li><a class="no_pad" href="{{ route('lead.index') }}?clear_filter=1&status_id[]={{ \App\LeadStatus::STATUS_NEW_SALE }}{{ Auth::user()->hasRole('telesales') ? ('&assignee[]='.Auth::user()->id) : '' }}"><span class="btn btn-sm btn-primary">New Sales (NS)</span></a></li>
                            <li><a class="no_pad" href="{{ route('lead.index') }}?clear_filter=1&status_id[]={{ \App\LeadStatus::STATUS_ENROLL }}{{ Auth::user()->hasRole('telesales') ? ('&assignee[]='.Auth::user()->id) : '' }}"><span class="btn btn-sm btn-success">Nhập học (NE)</span></a></li>
                        </ul>
                    </li>


                    <li><a href="javascript:void(0);"><i class="fa fa-id-card-o"></i>Phụ huynh<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="/lead"><i class="fa fa-table"></i>Danh sách</a></li>
                            <li><a href="/lead/create"><i class="fa fa-plus"></i>Tạo mới</a></li>
                            <li><a href="/lead/import"><i class="fa fa-download"></i>Import</a></li>
                        </ul>
                    </li>

                    <li><a href="javascript:void(0);"><i class="fa fa-database"></i>Data<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="/suspect"><i class="fa fa-table"></i>Danh sách</a></li>
                            <li><a href="/suspect/create"><i class="fa fa-plus"></i>Tạo mới</a></li>
                            <li><a href="/suspect/import"><i class="fa fa-download"></i>Import</a></li>
                        </ul>
                    </li>

                    <li><a href="javascript:void(0);"><i class="fa fa-check-square-o"></i>Quản lý<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            @if((Auth::user()->hasRole('admin') && in_array(\App\Department::HOLDING, Auth::user()->getManageDepartments())) || Auth::user()->hasRole('holding'))
                                <li><a href="/user"><i class="fa fa-users"></i>Người dùng</a></li>
                                <li><a href="/department"><i class="fa fa-building-o"></i>Cơ sở</a></li>
                                <li><a href="/rGroup"><i class="fa fa-object-group"></i>Nhóm</a></li>
                                <li><a href="/import"><i class="fa fa-download"></i>Lịch sử import Phụ huynh</a></li>
                                <li><a href="/importSuspectHistory"><i class="fa fa-download"></i>Lịch sử import data</a></li>
                                <li><a href="/promotion"><i class="fa fa-percent"></i>Promotion</a></li>
                                <li><a href="/source"><i class="fa fa-link"></i>Nguồn dẫn</a></li>
                                <li><a href="/info_source"><i class="fa fa-link"></i>Nguồn thông tin</a></li>
                            @endif

                            @if(Auth::user()->hasRoleInList(['admin', 'holding', 'marketer']))
                                <li><a href="/channel"><i class="fa fa-link"></i>Kênh quảng cáo</a></li>
                                <li><a href="/campaign"><i class="fa fa-link"></i>Campaign</a></li>
                            @endif

                            @if(Auth::user()->hasRole('admin'))
                                <li><a href="/lead_status"><i class="fa fa-check-square-o"></i>Trạng thái lead</a></li>
                                <li><a href="/call_status"><i class="fa fa-check-square-o"></i>Trạng thái chăm sóc</a></li>
                                <li><a href="/class"><i class="fa fa-graduation-cap"></i>Lớp học quan tâm</a></li>
                                <li><a href="/cdr"><i class="fa fa-phone"></i>Chi tiết cuộc gọi</a></li>
                            @endif

                            @if(Auth::user()->hasRoleInList(['admin', 'holding', 'teleteamleader', 'manager']))
                                <li><a href="/school"><i class="fa fa-building-o"></i>Trường</a></li>
                            @endif

                            <li><a href="/alarm"><i class="fa fa-calendar"></i>Lịch hẹn chăm sóc</a></li>
                            <li><a href="{{ route('calls.index') }}"><i class="fa fa-history"></i>Lịch sử chăm sóc</a></li>
                        </ul>
                    </li>

                    <li><a href="javascript:void(0);"><i class="fa fa-list"></i>Báo cáo<span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="/report/call_log">Thống kê Log call của CSKH</a></li>
                            <li><a href="/report/logSms">Thống kê tin nhắn của CSKH</a></li>
                            @if(Auth::user()->hasRole('admin') ||  Auth::user()->hasRole('holding') || in_array(Auth::user()->id, array(92, 126, 129, 195, 132)))
                                <li><a href="/report/leadInteractive">Chỉ số thời gian chăm sóc Phụ huynh</a></li>
                            @endif
                            @if(in_array(Auth::user()->id, array(92, 103, 195, 132)))
                                <li><a href="/report/logChangeStatus">Lịch sử thay đổi trạng thái Phụ huynh</a></li>
                            @endif
                            @if(Auth::user()->hasRole('admin') ||  Auth::user()->hasRole('holding'))
                                <li><a href="/report/otb">Báo cáo tổng hợp Phụ huynh - OTB</a></li>
                            @endif
                            @if(in_array(Auth::user()->id, array(92,5,77,103)))
                                <li><a href="/report/leadSource">Báo cáo nguồn dẫn lead</a></li>
                            @endif

                            @if(Auth::user()->hasRole('holding') || in_array(\App\Department::HOLDING, Auth::user()->getManageDepartments()))
                                @if(Auth::user()->isSMISUser())
                                    <li><a href="/report/leadDetail/smis">Báo cáo chi tiết</a></li>
                                @elseif(Auth::user()->isGISUser())
                                    <li><a href="/report/leadDetail/gis">Báo cáo chi tiết</a></li>
                                @else
                                    <li><a href="/report/leadDetail/smis">Báo cáo chi tiết - SMIS</a></li>
                                    <li><a href="/report/leadDetail/gis">Báo cáo chi tiết - GIS</a></li>
                                @endif
                            @endif

                            <li><a href="/report/leadCare">Báo cáo chăm sóc Phụ huynh</a></li>
                            @if(in_array(Auth::user()->id, array(92, 103, 133, 195, 132)))
                                <li><a href="/report/leadConvert">Báo cáo chuyển đổi Lead NE</a></li>
                            @endif

                            <li><a href="/report/leadConvertStatus">Báo cáo chuyển đổi trạng thái Phụ huynh</a></li>

                            @if(Auth::user()->hasRole('admin') ||  Auth::user()->hasRole('holding'))
                                <li><a href="/report/callLogDuration">Báo cáo thời lượng cuộc gọi</a></li>
                            @endif
                        </ul>
                    </li>

                </ul>

            </div>


        </div>


    </div>
</div>