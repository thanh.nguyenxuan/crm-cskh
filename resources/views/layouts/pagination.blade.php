<?php
/**
 * @var $pagination_total_page int
 * @var $pagination_current_page int
 * @var $pagination_url string
 * @var $pagination_url_params array
 */

if($pagination_total_page > 1 && $pagination_current_page > 1){
    $pagination_url_params['page'] = $pagination_current_page-1;
    $pagination_previous_url = $pagination_url . '?' . http_build_query($pagination_url_params);

}else{
    $pagination_previous_url = 'javascript:void(0)';
}

if($pagination_total_page > 1 && $pagination_current_page < $pagination_total_page){
    $pagination_url_params['page'] = $pagination_current_page+1;
    $pagination_next_url = $pagination_url . '?' . http_build_query($pagination_url_params);

}else{
    $pagination_next_url = 'javascript:void(0)';
}
?>
<div class="text-right">
    <ul class="pagination">
        <li class="paginate_button previous<?php echo ($pagination_current_page == 1) ? ' disabled' : ''?>">
            <a href="<?php echo $pagination_previous_url?>">Trước</a>
        </li>

        <?php for($i=1; $i<=$pagination_total_page; $i++){
        $pagination_url_params['page'] = $i;
        $pagination_page_url = $pagination_url . '?' . http_build_query($pagination_url_params);
        ?>
        <li class="paginate_button<?php echo ($pagination_current_page == $i) ? ' active' : ''?>">
            <a href="<?php echo $pagination_page_url?>"><?php echo $i?></a>
        </li>
        <?php }?>

        <li class="paginate_button next<?php echo ($pagination_current_page == $pagination_total_page) ? ' disabled' : ''?>">
            <a href="<?php echo $pagination_next_url?>">Tiếp</a>
        </li>
    </ul>
</div>
