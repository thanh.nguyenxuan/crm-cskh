<ul class="body dropdown-menu list-unstyled msg_list">
    <li class="empty text-center">Chưa có thông báo</li>
    <li>
        <a href="/notify">
            <div class="text-center">
                <strong>See All Alerts</strong>
                <i class="fa fa-angle-right"></i>
            </div>
        </a>
    </li>
</ul>

<div class="modal fade" id="notify_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Thông báo</h4>
            </div>
            <div class="modal-body" style="font-size: 16px">

            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btnClose btn btn-default">Đóng</button>
            </div>
        </div>
    </div>
</div>


<style>
    #notifications .body .item .message label.time{
        display: none;
    }
</style>

<script>

    var notify_see_all_html = '<li>' +
        '<a href="/notify">' +
        '<div class="text-center">' +
        '<strong>Xem tất cả</strong>' +
        ' <i class="fa fa-angle-right"></i>' +
        '</div>' +
        '</a>' +
        '</li>';

    function getNewNotify()
    {
        $.ajax({
            url: '<?php echo env('APP_URL')?>/notify/getNewNotify',
            type: 'POST',
            dataType: 'json',
            data: {
                '_token' : '{{ csrf_token() }}',
            },
            success: function(result){
                if(result.success){
                    if(result.data.total > 0){
                        $('#btnShowNotify span').text(result.data.total).removeClass('hidden');
                    }
                    generateNotifyItem(result.data.notify);
                }
            }
        });
    }

    function generateNotifyItem(notify)
    {
        var empty = '<li class="empty text-center">Chưa có thông báo</li>';
        if (notify === undefined || notify.length === 0) {
            $('#notifications .body').html(empty);
        }else{
            $('#notifications .body').html('');
            notify.forEach(function(item, index, arr){
                var html = '<li class="item" data-id="'+item.id+'"'+ ((item.lead_id) ? (' data-lead-id="'+item.lead_id+'"') : '')+ '>' +
                    '<a onclick="notifyRedirect('+item.id+')">'+
                    '<span class="sender">' +
                    '<span>&nbsp;</span>' +
                    '<span class="time">'+item.notify_at+'</span>' +
                    '</span>' +
                    '<span class="message">'+item.content+'</span>' +
                    '</a>' +
                    '</li>';

                $('#notifications .body').append(html);
            });
            $('#notifications .body').append(notify_see_all_html);
        }
    }

    function hasNotify()
    {
        return ($('#notifications .body .item').length > 0);
    }

    function notifyRedirect(notify_id)
    {
        alert("OK");
        markAsRead(notify_id);
        var item = $('#notifications .body .item[data-id='+notify_id+']');
        if($(item).attr('data-lead-id')){
            window.open('<?php echo env('APP_URL')?>/lead/edit/'+$(item).attr('data-lead-id'),'_self');
        }
    }

    function checkNotifyLeadNewAndNoAnswer()
    {
        $.ajax({
            url: '<?php echo env('APP_URL')?>/notify/checkNotifyLeadNewAndNoAnswer',
            type: 'POST',
            dataType: 'json',
            data: {
                '_token' : '{{ csrf_token() }}',
            },
            success: function(result){
                if(result.success){
                    console.log(result);
                    $('#notify_modal .modal-body').html(result.message);
                    $('#notify_modal').attr('data-id', result.data.id);

                    $('#notify_modal').find('.btnClose')
                        .text('Đóng')
                        .removeClass('btn-danger')
                        .addClass('btn-default')
                    ;

                    if(result.data.type == '<?php echo \App\Notify::TYPE_ALARM_LEAD_DAILY_KPI?>') {
                        var data = JSON.parse(result.data.data);
                        var text = 'Tôi sẽ hoàn thành trong ngày';
                        if (!data.finish) {
                            $('#notify_modal').find('.btnClose')
                                .text('Tôi sẽ hoàn thành trong ngày')
                                .removeClass('btn-default')
                                .addClass('btn-danger')
                            ;
                        }
                    }

                    $('#notify_modal').modal('show');
                }
            }
        });
    }

    var checkNotifySchedule;

    function startCheckNotifySchedule(){
        checkNotifySchedule = setInterval(function () {
            getNewNotify();
            checkNotifyLeadNewAndNoAnswer();
        }, 1000 * 60);
        console.log('start checkNotifySchedule');
    }

    function stopCheckNotifySchedule()
    {
        clearInterval(checkNotifySchedule);
        checkNotifySchedule = null;
        console.log('stop checkNotifySchedule');
    }

    function isCheckNotifyScheduleRunning()
    {
        return (checkNotifySchedule);
    }

    function markAsRead(notify_id)
    {
        $.ajax({
            url: '<?php echo env('APP_URL')?>/notify/markAsRead',
            type: 'POST',
            dataType: 'json',
            data: {
                '_token' : '{{ csrf_token() }}',
                'notify_id' : notify_id
            },
            success: function(result){
            }
        });
    }

    $(document).ready(function()
    {
        getNewNotify();
        checkNotifyLeadNewAndNoAnswer();
        startCheckNotifySchedule();
        $('#notify_modal').on('hidden.bs.modal', function () {
            var notify_id = $(this).attr('data-id');
            if(notify_id){
                markAsRead(notify_id);
            }
        })
    });
</script>