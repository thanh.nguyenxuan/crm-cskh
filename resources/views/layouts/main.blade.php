<?php CONST APP_VERSION = '2.2'?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/images/logo.png" type="image/ico" />
    <title>CRM Edufit</title>

    <!-- JS -->
    <!-- JQuery -->
    <script src="/vendors/jquery/jquery.min.js"></script>
    <script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="/vendors/bootstrap/dist/js/bootstrap-noconflict.js"></script>
    <script src="/vendors/bootbox/bootbox.min.js"></script>
    <script src="/vendors/notify/notify.min.js"></script>
    <script src="/js/jquery-ui.min.js"></script>
    <script src="/js/jquery.number.min.js"></script>
    <script src="/js/select2.full.min.js"></script>

    <!-- DatePicker -->
    <script type="text/javascript" charset="utf8" src="/js/moment.js"></script>
    <script src="/js/jquery-ui-timepicker-addon.min.js"></script>
    <script src="/vendors/daterangepicker/daterangepicker.js"></script>


    <!-- CSS -->
    <!-- Bootstrap -->
    <link href="/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="/vendors/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="/css/select2.min.css" rel="stylesheet">
    <link href="/css/style.css?v=1.1.6" rel="stylesheet">
    <link rel="stylesheet" href="/css/jquery-ui.min.css">
    <!-- DatePicker -->
    <link rel="stylesheet" href="/css/jquery-ui-timepicker-addon.min.css">
    <link rel="stylesheet" href="/vendors/daterangepicker/daterangepicker.css">
    <!-- Font Awesome -->
    <link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>
    <!-- iCheck -->
    <link href="/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- Theme Css -->
    <link href="/dist/css/theme.css?v=<?php echo APP_VERSION?>" rel="stylesheet">
    <link href="/dist/css/custom.css?v=<?php echo APP_VERSION?>" rel="stylesheet">

</head>

<body>

<div class="container body">
    <div class="main_container">

        @include('layouts.left_col')
        @include('layouts.top_nav')

        <div class="right_col" role="main">
            @yield('content')
        </div>

    </div>
</div>

<!-- FastClick -->
<script src="/vendors/fastclick/lib/fastclick.js"></script>
<!-- NProgress -->
<script src="/vendors/nprogress/nprogress.js"></script>
<!-- jQuery custom content scroller -->
<script src="/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- iCheck -->
<script src="/vendors/iCheck/icheck.min.js"></script>
<!-- Switchery -->
<script src="/vendors/switchery/dist/switchery.min.js"></script>
<!-- FreezeTable -->
<script src="/vendors/freeze-table/freeze-table.js"></script>
<!-- Custom Theme Scripts -->
<script src="/dist/js/theme.js?v=<?php echo APP_VERSION?>"></script>
<script src="/dist/js/custom.js?v=<?php echo APP_VERSION?>"></script>

</body>
</html>