<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <form class="navbar-form navbar-left" id="gsearch_form" method="get" action="{{ route('lead.index') }}">
                {{ csrf_field() }}
                <input type="hidden" name="search_mode" id="search_mode" value="global_search">
                <input type="hidden" name="clear_filter" value="1">
                <div class="form-group">
                    <div class="input-group" style="margin-bottom: 0">
                        <input type="text" class="form-control" size="75" name="gsearch" id="gsearch"
                               value="@php echo !empty($_REQUEST['gsearch'])?$_REQUEST['gsearch']:''; @endphp"
                               placeholder="Tìm kiếm nhanh - họ tên / đt / email / mã hs / lớp" required>
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-default" id="gsearch_button" name="gsearch_button"
                                    value="gsearch"><i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </form>

            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="javascript:;">
                        {{ Auth::user()->username }}
                        <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                        <li><a href="{{ route('user.edit', Auth::user()->id) }}">Thông tin cá nhân</a></li>
                        <li><a href="{{ route('user.logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-sign-out pull-right"></i> Đăng xuất</a></li>
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right" id="notifications">
                <li class="">
                    <a id="btnShowNotify" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false" href="javascript;:">
                        <i class="fa fa-envelope-o"></i>
                        <span class="badge bg-green"></span>
                    </a>

                    @include('layouts.notification')
                </li>
            </ul>

        </nav>
    </div>
</div>

<form id="logout-form" action="{{ route('user.logout') }}" method="POST"
      style="display: none;">
    {{ csrf_field() }}
</form>

@include('includes.set_alarm')
@include('includes.call_log.log_call')
@include('includes.alarm')
