@extends('layouts.main')

@section('content')

    <script type="text/javascript" src="/js/datatables.min.js"></script>
    <script src="//cdn.datatables.net/plug-ins/1.10.13/dataRender/datetime.js"></script>
    <script src="/js/dataTables.select.min.js"></script>
    <script src="/js/dataTables.fixedHeader.min.js"></script>

    <link rel="stylesheet" type="text/css"  href="/css/datatables.min.css"/>

@include('includes.sms_multiple')
<div id="edit_form_container">
</div>
<script type="text/javascript">
    function pad2(number) {
        return (number < 10 ? '0' : '') + number
    }

    function checkForceReload(grid) {
        var url = '{{ env('APP_URL') }}/needReload';
        $.get(url, function (data) {
            console.log(data);
            if (data == 1) {
                grid.ajax.reload();
            }
        });
    }

    var checkingUpdate;
    function turnOnCheckingUpdate(){
        if(!checkingUpdate){
            checkingUpdate = setInterval(function () {
                checkForceReload(main_table);
            }, 1000 * 60);
        }
    }

    function turnOffCheckingUpdate(){
        clearInterval(checkingUpdate);
        checkingUpdate = null;
    }

    var main_table;
    $(document).ready(function () {
        $.fn.dataTable.ext.errMode = 'none';
        // $('#main_grid').on('error.dt', function (e, settings, techNote, message) {
        //     alert('Đã có lỗi trong quá trình tải dữ liệu. Vui lòng refresh lại trang web và kiểm tra lại. Nếu vẫn gặp lỗi hãy liên hệ với cán bộ kỹ thuật để được hỗ trợ.');
        //     console.log('error');
        //     console.log("--e:");
        //     console.log(e);
        //     console.log("--settings:");
        //     console.log(settings);
        //     console.log("--techNote:");
        //     console.log(techNote);
        //     console.log("--message:");
        //     console.log(message);
        // });
        main_table = $('#main_grid').DataTable({
            "dom": '<"top"ilpB>rti<"bottom"flp><"clear">',
            "buttons": [
                {extend: 'selectAll', className: "fa fa-lg fa-check-square-o"},
                {extend: 'selectNone', className: "fa fa-lg fa-square-o"},
                {
                    text: '',
                    className: 'fa fa-lg fa-refresh',
                    title: 'Tải lại',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }
            ],
            "language": {
                url: '/js/dataTables.i18n.vi.lang',
                buttons: {
                    selectAll: "",
                    selectNone: ""
                },
                select: {
                    rows: {
                        _: "Đã chọn %d dòng",
                        1: "Đã chọn 1 dòng"
                    }
                },
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Đang xử lý...</span>',
            },
            select: true,
            "searching": false,
            "deferRender": true,
            "selected": true,
            "paging": true,
            "pageLength": {{ !empty($filtering->length)?$filtering->length:50 }},
            "displayStart": {{ !empty($filtering->start)?$filtering->start:0 }},
            "lengthMenu": [[10, 50, 100, 200, 300, 500], [10, 50, 100, 200, 300, 500]],
            "processing": true,
            "serverSide": true,
            "ordering": true,
            fixedHeader: true,
            "ajax": {
                "url": "/lead/list",
                "type": "POST",
                "data": function (d) {
                    d._token                = '{{ csrf_token() }}';
                    d.gsearch               = $('#gsearch').val();
                    d.search_mode           = ($('#gsearch').val()) ? $('#search_mode').val(): 'advanced_search';

                    d.source                = $('#source').val();
                    d.department_id         = $('#department').val();
                    d.creator               = $('#creator').val();
                    d.assignee              = $('#assignee').val();
                    d.activator             = $('#activator').val();
                    d.major                 = $('#major').val();
                    d.status_id             = $('#status_id').val();
                    d.call_status_id        = $('#call_status_id').val();
                    d.campaign              = $('#campaign').val();
                    d.campaign_category     = $('#campaign_category').val();
                    d.start_date            = $('#start_date').val();
                    d.end_date              = $('#end_date').val();
                    d.active_from           = $('#active_from').val();
                    d.active_to             = $('#active_to').val();
                    d.enrolled_from         = $('#enrolled_from').val();
                    d.enrolled_to           = $('#enrolled_to').val();
                    d.modified_from         = $('#modified_from').val();
                    d.modified_to           = $('#modified_to').val();
                    d.assigned_from         = $('#assigned_from').val();
                    d.assigned_to           = $('#assigned_to').val();
                    d.showup_school_from    = $('#showup_school_from').val();
                    d.showup_school_to      = $('#showup_school_to').val();
                    d.join_as_guest_from    = $('#join_as_guest_from').val();
                    d.join_as_guest_to      = $('#join_as_guest_to').val();
                    d.hot_lead_from         = $('#hot_lead_from').val();
                    d.hot_lead_to           = $('#hot_lead_to').val();
                    d.refuse_from           = $('#refuse_from').val();
                    d.refuse_to             = $('#refuse_to').val();
                    d.qualified_from        = $('#qualified_from').val();
                    d.qualified_to          = $('#qualified_to').val();
                    d.student_code          = $('#student_code').val();
                    d.class_name            = $('#class_name').val();
                },
                "error": function (xhr, error, code)
                {
                    console.log('--xhr:');
                    console.log(xhr);
                    console.log('--error:');
                    console.log(error);
                    console.log('--code:');
                    console.log(code);
                    alert('Đã có lỗi trong quá trình tải dữ liệu. Vui lòng refresh lại trang web và kiểm tra lại. Nếu vẫn gặp lỗi hãy liên hệ với cán bộ kỹ thuật để được hỗ trợ.');
                }
            },
            "columns": [
                {
                    "data": "id",
                    visible: false
                },
                {
                    "data": "name",
                    "orderable": true,
                    "render": function (data, type, row) {
                        var color_highlighted = '';
                        if (row['new_duplicate'] && (row['department_id'] == 4)) {
                            color_highlighted = ' color: red; font-weight: bold; ';
                        }
                        return '<a target="_blank" style="' + color_highlighted + '" href="/lead/edit/' + row['id'] + '" data-id="' + row['id'] + '">' + row['name'] + '</a>';
                    }
                },

                {
                    "data": "phone",
                    "render": function (data, type, row) {
                        if (data) {
                            var phone_list = data.split(',');
                            var render_data = '';
                            phone_list.forEach(function (element, index, array) {
                                render_data += "<a href='javascript:void(0)' class='phone-number' data-lead-id='" + row['id'] + "' data-phone='" + element + "'>" + element + "</a>";
                                if (index != (array.length - 1))
                                    render_data += '<br />';
                            });
                            return render_data;
                        }
                    },
                    className: 'phoneNumber'
                },
                {
                    "data": "email", "className": "break",
                    "render": function (data, type, row) {
                        if (row['email'] && row['email'].match('@'))
                            return '<a href="mailto:' + row['email'] + '">' + row['email'] + '</a>';
                        else
                            return row['email'];
                    }
                },
                {
                    "data": "student_name",
                },
                {
                    "data": "student_code",
                },
                {
                    "data": "class_name",
                },
                {
                    "data": "lead_status_name",
                    orderable: false,
                    "defaultContent": "",
                    "render": function (data, type, row) {
                        switch (row['status']) {
                            case '1':  return "<b style='color: #28a745;'>Mới<b>";
                            case '17': return "<b style='color: #007bff;'>Tiềm năng<b>";
                            case '9':  return "<b style='color: #ffc107;'>Đặt cọc giữ chỗ<b>";
                            case '15': return "<b style='color: #dc3545;'>Nhập học<b>";
                            case '6':  return "<b style='color: #343a40;'>Từ chối<b>";
                            default: return data;
                        }
                    }
                },
                {
                    "data": "call_status_name",
                    orderable: false,
                },
                {
                    "data" : 'total_log_call',
                    orderable: false
                },
                {
                    "data" : 'last_log_call',
                    "render": function (data, type, row) {
                        if (row['last_log_call']) {
                            var last_log_call = row['last_log_call'];
                            return "<span class='last_log_call' data-readmore aria-expanded='false' style='max-height: 80px'>" + last_log_call.replace("\r\n", "<br />") + "</span>";
                        } else {
                            return '';
                        }
                    },
                    orderable: false,
                    'width' : '20%',
                },
                {
                    "data": "notes"
                },
                {
                    "data": "assignee_list"
                },
                {
                    "data": "department",
                },
            ]
        });
        main_table.on('draw', function () {
            $('.last_log_call').readmore({
                collapsedHeight: 55,
                moreLink: '<a href="#"><i class="fa fa-caret-square-o-down" aria-hidden="true"></i></a>',
                lessLink: '<a href="#"><i class="fa fa-caret-square-o-up" aria-hidden="true"></i></a>'
            });
            turnOnCheckingUpdate();
        });
        main_table.on('page.dt', function () {
            turnOffCheckingUpdate();
        });
        $('#search_form').hide();
        $("#reset_button").click(function () {
            $('#source').val([]);
            $('#department').val([]);
            $('#creator').val([]);
            $('#assignee').val([]);
            $('#activator').val([]);
            $('#major').val('');
            @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('onlinemarketer') || Auth::user()->hasRole('promoterleader') || Auth::user()->hasRole('carer'))
            $('#status_id').val('');
            @else
            $('#status_id').val([1]);
            @endif
            $('#call_status_id').val([]);
            $('#campaign').val([]);
            $('#campaign_category').val([]);
            $('#start_date').val('');
            $('#end_date').val('');
            $('#active_from').val('');
            $('#active_to').val('');
            $('#enrolled_from').val('');
            $('#enrolled_to').val('');
            $('#modified_from').val('');
            $('#modified_to').val('');
            $('#assigned_from').val('');
            $('#assigned_to').val('');
            $('#showup_school_from').val('');
            $('#showup_school_to').val('');
            $('#join_as_guest_from').val('');
            $('#join_as_guest_to').val('');
            $('#hot_lead_from').val('');
            $('#hot_lead_to').val('');
            $('#refuse_from').val('');
            $('#refuse_to').val('');
            $('#qualified_from').val('');
            $('#qualified_to').val('');
            $('#student_code').val('');
            $('#class_name').val('');

            $('#department').trigger('change');
            $('#campaign_category').trigger('change');

            $.get("{{ route('lead.clearFilters') }}");
        });
        $('#lead_assignment_button').click(function () {
            $("#lead-assignment-modal").modal('show');
            var selected = [];
            var selected_items = main_table.rows({selected: true}).data().toArray();
            for (var i = 0; i < selected_items.length; i++)
                selected.push(selected_items[i].id);
            $('#selected_count').text(selected.length);
            //console.log(main_table.rows( { selected: true } ).data().toArray());
        });
        $('#assign_button').click(function () {
            var url = '{{ env('APP_URL') }}/lead/assign';
            var selected = [];
            var selected_items = main_table.rows({selected: true}).data().toArray();
            for (var i = 0; i < selected_items.length; i++)
                selected.push(selected_items[i].id);
            var additional_assignment = $("input[name=additional_assignment]:checked").val();
            var data = new Object();
            if (additional_assignment)
                data.additional_assignment = additional_assignment;
            data.selected = selected;
            data.assignee = $('#assignee_list').find(":selected").val();
            data.department = $('#department_list').find(":selected").val();
            data._token = "{{ csrf_token() }}";
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            if (selected.length > 0) {
                $.post(url, data, status, 'json');
                main_table.ajax.reload();
            }
            $("#lead-assignment-modal").modal('hide');
            $("#additional_assignment").prop('checked', false);
        });

        $('select#department').on('change', function(){
            var departments = $(this).val();
            var url = '{{ env('APP_URL') }}/lead/getUserByDepartment';
            var data = new Object();
            data.departments = departments;
            data._token = "{{ csrf_token() }}";
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            $.post(url, data, function(data,status,xhr){
                $('select#creator').html(data.option.creator);
                $('select#assignee').html(data.option.assignee);
                $('select#activator').html(data.option.activator);
            }, 'json');
        });

        $('select#campaign_category').on('change', function(){
            var campaign_categories = $(this).val();
            var url = '{{ env('APP_URL') }}/lead/getCampaignByCategory';
            var data = new Object();
            data.campaign_categories = campaign_categories;
            data._token = "{{ csrf_token() }}";
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            $.post(url, data, function(data,status,xhr){
                $('select#campaign').html(data.option.campaign);
            }, 'json');
        });

        $("#main_grid tbody").on('click', "a.phone-number", function () {
            var phone = $(this).data('phone');
            var lead_id = $(this).data('lead-id');
            //var trunk = $('#trunk').val();
            var trunk = {{ $trunk_id?$trunk_id:0 }};
            $.get('{{ env('APP_URL') }}/call/' + phone + '/' + trunk);
            console.log('Calling ' + phone + '...');
            window.open('{{ env('APP_URL') }}/lead/edit/' + lead_id, '_blank');
        });

    });
</script>
<form id="search_form" method="post">
    {{ csrf_field() }}

    <div class="col-md-3 col-sm-4">
        <div class="form-group">
            <label for="department">Cơ sở</label>
            {{ Form::select('department_id[]', $department_list, !empty($filtering->department_id)?$filtering->department_id:array(), ['id' => 'department', 'multiple' => true, 'class' => 'form-control']) }}
        </div>
        <div class="form-group">
            <label for="creator">Người tạo</label>
            <br/>
            {{ Form::select('creator[]', $creator_list, !empty($filtering->creator)?$filtering->creator:array(), ['id' => 'creator', 'multiple' => true, 'class' => 'form-control']) }}
        </div>
        <div class="form-group">
            <label for="assignee">Người được chia</label>
            <br/>
            {{ Form::select('assignee[]', $assignee_list, !empty($filtering->assignee)?$filtering->assignee:array(), ['id' => 'assignee', 'multiple' => true, 'class' => 'form-control']) }}
        </div>
        <div class="form-group">
            <label for="activator">Người active</label>
            <br/>
            {{ Form::select('activator[]', $activator_list, !empty($filtering->activator)?$filtering->activator:array(), ['id' => 'activator', 'multiple' => true, 'class' => 'form-control']) }}
        </div>
        <div class="form-group">
            <label for="major">Lớp học quan tâm</label>
            {{ Form::select('major[]', $major_list, !empty($filtering->major)?$filtering->major:array(), ['id' => 'major', 'multiple' => true, 'class' => 'form-control']) }}
        </div>
    </div>

    <div class="col-md-3 col-sm-4">
        <div class="form-group<?php echo (Auth::user()->hasRole('carer')) ? ' hidden' : ''?>">
            <label for="status_id">Trạng thái HS</label>
            {{ Form::select('status_id[]', $status_list, !empty($filtering->status_id)?$filtering->status_id:array(), ['id' => 'status_id', 'multiple' => true, 'class' => 'form-control']) }}
        </div>
        <div class="form-group">
            <label for="call_status_id">Trạng thái chăm sóc</label>
            {{ Form::select('call_status_id[]', $call_status_list, !empty($filtering->call_status_id)?$filtering->call_status_id:array(), ['id' => 'call_status_id', 'multiple' => true, 'class' => 'form-control']) }}
        </div>
        <div class="form-group">
            <label for="source">Nguồn dẫn</label>
            {{ Form::select('source[]', $source_list, !empty($filtering->source)?$filtering->source:'', ['id' => 'source', 'multiple' => true, 'class' => 'form-control']) }}
        </div>
        <div class="form-group">
            <label for="campaign_category">Nhóm chiến dịch</label>
            {{ Form::select('campaign_category[]', $campaign_category_list, !empty($filtering->campaign_category)?$filtering->campaign_category:'', ['id' => 'campaign_category', 'multiple' => true, 'class' => 'form-control']) }}
        </div>
        <div class="form-group">
            <label for="campaign">Chiến dịch</label>
            {{ Form::select('campaign[]', $campaign_list, !empty($filtering->campaign)?$filtering->campaign:'', ['id' => 'campaign', 'multiple' => true, 'class' => 'form-control']) }}
        </div>
    </div>

    <div class="col-md-3 col-sm-4">
        <div class="form-group">
            <label for="start_date">Ngày tạo (từ ngày - đến ngày)</label>
            <div class="row">
                <div class="col-sm-6">
                    {{ Form::text('start_date', !empty($filtering->start_date)?$filtering->start_date:'', ['id' => 'start_date', 'class' => 'form-control', 'placeholder' => 'dd/mm/yyyy']) }}
                </div>
                <div class="col-sm-6">
                    {{ Form::text('end_date', !empty($filtering->end_date)?$filtering->end_date:'', ['id' => 'end_date', 'class' => 'form-control', 'placeholder' => 'dd/mm/yyyy']) }}
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="hot_lead_from">Hot lead (từ ngày - đến ngày)</label>
            <div class="row">
                <div class="col-sm-6">
                    {{ Form::text('hot_lead_from', !empty($filtering->hot_lead_from)?$filtering->hot_lead_from:'', ['id' => 'hot_lead_from', 'class' => 'form-control', 'placeholder' => 'dd/mm/yyyy']) }}
                </div>
                <div class="col-sm-6">
                    {{ Form::text('hot_lead_to', !empty($filtering->hot_lead_to)?$filtering->hot_lead_to:'', ['id' => 'hot_lead_to', 'class' => 'form-control', 'placeholder' => 'dd/mm/yyyy']) }}
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="showup_school_from">Show Up đến trường (từ ngày - đến ngày)</label>
            <div class="row">
                <div class="col-sm-6">
                    {{ Form::text('showup_school_from', !empty($filtering->showup_school_from)?$filtering->showup_school_from:'', ['id' => 'showup_school_from', 'class' => 'form-control', 'placeholder' => 'dd/mm/yyyy']) }}
                </div>
                <div class="col-sm-6">
                    {{ Form::text('showup_school_to', !empty($filtering->showup_school_to)?$filtering->showup_school_to:'', ['id' => 'showup_school_to', 'class' => 'form-control', 'placeholder' => 'dd/mm/yyyy']) }}
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="active_from">Đặt cọc giữ chỗ (từ ngày - đến ngày)</label>
            <div class="row">
                <div class="col-sm-6">
                    {{ Form::text('active_from', !empty($filtering->active_from)?$filtering->active_from:'', ['id' => 'active_from', 'class' => 'form-control', 'placeholder' => 'dd/mm/yyyy']) }}
                </div>
                <div class="col-sm-6">
                    {{ Form::text('active_to', !empty($filtering->active_to)?$filtering->active_to:'', ['id' => 'active_to', 'class' => 'form-control', 'placeholder' => 'dd/mm/yyyy']) }}
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="enrolled_from">Nhập học (từ ngày - đến ngày)</label>
            <div class="row">
                <div class="col-sm-6">
                    {{ Form::text('enrolled_from', !empty($filtering->enrolled_from)?$filtering->enrolled_from:'', ['id' => 'enrolled_from', 'class' => 'form-control', 'placeholder' => 'dd/mm/yyyy']) }}
                </div>
                <div class="col-sm-6">
                    {{ Form::text('enrolled_to', !empty($filtering->enrolled_to)?$filtering->enrolled_to:'', ['id' => 'enrolled_to', 'class' => 'form-control', 'placeholder' => 'dd/mm/yyyy']) }}
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="student_code">Mã học sinh</label>
            {{ Form::text('student_code', !empty($filtering->student_code)?$filtering->student_code:'', ['id' => 'student_code', 'class' => 'form-control']) }}
        </div>
        <div class="form-group">
            <label for="class_name">Lớp nhập học</label>
            {{ Form::text('class_name', !empty($filtering->class_name)?$filtering->class_name:'', ['id' => 'class_name', 'class' => 'form-control']) }}
        </div>
    </div>

    <div class="col-md-3 col-sm-4">
        <div class="form-group">
            <label for="assigned_from">Phân công (từ ngày - đến ngày)</label>
            <div class="row">
                <div class="col-sm-6">
                    {{ Form::text('assigned_from', !empty($filtering->assigned_from)?$filtering->assigned_from:'', ['id' => 'assigned_from', 'class' => 'form-control', 'placeholder' => 'dd/mm/yyyy']) }}
                </div>
                <div class="col-sm-6">
                    {{ Form::text('assigned_to', !empty($filtering->assigned_to)?$filtering->assigned_to:'', ['id' => 'assigned_to', 'class' => 'form-control', 'placeholder' => 'dd/mm/yyyy']) }}
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="modified_from">Chăm sóc (từ ngày - đến ngày)</label>
            <div class="row">
                <div class="col-sm-6">
                    {{ Form::text('modified_from', !empty($filtering->modified_from)?$filtering->modified_from:'', ['id' => 'modified_from', 'class' => 'form-control', 'placeholder' => 'dd/mm/yyyy']) }}
                </div>
                <div class="col-sm-6">
                    {{ Form::text('modified_to', !empty($filtering->modified_to)?$filtering->modified_to:'', ['id' => 'modified_to', 'class' => 'form-control', 'placeholder' => 'dd/mm/yyyy']) }}
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="join_as_guest_from">Bé làm khách (từ ngày - đến ngày)</label>
            <div class="row">
                <div class="col-sm-6">
                    {{ Form::text('join_as_guest_from', !empty($filtering->join_as_guest_from)?$filtering->join_as_guest_from:'', ['id' => 'join_as_guest_from', 'class' => 'form-control', 'placeholder' => 'dd/mm/yyyy']) }}
                </div>
                <div class="col-sm-6">
                    {{ Form::text('join_as_guest_to', !empty($filtering->join_as_guest_to)?$filtering->join_as_guest_to:'', ['id' => 'join_as_guest_to', 'class' => 'form-control', 'placeholder' => 'dd/mm/yyyy']) }}
                </div>
            </div>
        </div>

        <div class="form-group">
            <label for="refuse_from">Từ chối (từ ngày - đến ngày)</label>
            <div class="row">
                <div class="col-sm-6">
                    {{ Form::text('refuse_from', !empty($filtering->refuse_from)?$filtering->refuse_from:'', ['id' => 'refuse_from', 'class' => 'form-control', 'placeholder' => 'dd/mm/yyyy']) }}
                </div>
                <div class="col-sm-6">
                    {{ Form::text('refuse_to', !empty($filtering->refuse_to)?$filtering->refuse_to:'', ['id' => 'refuse_to', 'class' => 'form-control', 'placeholder' => 'dd/mm/yyyy']) }}
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="qualified_from">Qualified (từ ngày - đến ngày)</label>
            <div class="row">
                <div class="col-sm-6">
                    {{ Form::text('qualified_from', !empty($filtering->qualified_from)?$filtering->qualified_from:'', ['id' => 'qualified_from', 'class' => 'form-control', 'placeholder' => 'dd/mm/yyyy']) }}
                </div>
                <div class="col-sm-6">
                    {{ Form::text('qualified_to', !empty($filtering->qualified_to)?$filtering->qualified_to:'', ['id' => 'qualified_to', 'class' => 'form-control', 'placeholder' => 'dd/mm/yyyy']) }}
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <button type="submit" class="btn btn-edufit-default" id="search_button">
                <i class="fa fa-filter" aria-hidden="true"></i> Tìm kiếm
            </button>
            <button type="button" class="btn btn-edufit-default" id="reset_button">
                <i class="fa fa-eraser" aria-hidden="true"></i> Xóa lọc
            </button>
            <button type="button" class="btn btn-edufit-default" id="hide_search_form_2">
                <i class="fa fa-eye-slash" aria-hidden="true"></i> Ẩn bộ lọc
            </button>
        </div>
    </div>
    <input type="hidden" name="search_mode" id="search_mode" value="advanced_search">

    <input type="hidden" id="export_action" name="export" value=""/>
</form>
<div class="row">
    <div class="col-md-12 pull-right utils">
        <div class="buttons">
            <button type="button" class="btn btn-edufit-default" id="hide_search_form">
                <i class="fa fa-eye" aria-hidden="true"></i> Hiện bộ lọc
            </button>
            <button type="button" class="btn btn-edufit-default" id="export_button">
                <i class="fa fa-download" aria-hidden="true"></i> Kết xuất
            </button>
            @if(Auth::user()->hasRoleInList(['admin', 'holding', 'teleteamleader']))
                <button type="button" class="btn btn-edufit-default" id="lead_assignment_button">
                    <i class="fa fa-user-plus" aria-hidden="true"></i> Phân công
                </button>
            @endif
            <button type="button" class="btn btn-edufit-default" id="send_sms_button">
                <i class="fa fa-envelope" aria-hidden="true"></i> Gửi SMS
            </button>
            @if(Auth::user()->hasRoleInList(['admin', 'holding', 'teleteamleader', 'telesales', 'marketer']))
                <a href="/lead/create" target="_blank" class="btn btn-edufit-default">
                    <i class="fa fa-plus" aria-hidden="true"></i> Tạo mới
                </a>
            @endif
        </div>
    </div>
</div>
<div class="x_panel">
    <div class="col-md-12 table-responsive" style="overflow-x: unset">
        <table id="main_grid" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>ID</th>
                <th>Họ tên phụ huynh</th>
                <th>SĐT</th>
                <th>Email</th>
                <th>Họ tên học sinh</th>
                <th>Mã học sinh</th>
                <th>Lớp học</th>
                <th>Trạng thái HS</th>
                <th>Trạng thái chăm sóc</th>
                <th>Tổng số Log Call</th>
                <th>Log Call gần nhất</th>
                <th>Ghi chú</th>
                <th>Người phụ trách</th>
                <th>Cơ sở</th>
            </tr>
            </thead>
        </table>
    </div>
</div>
@include('includes.assignment', ['assignee_list' => $assignee_list])
<script src="js/filter.js?v=2.0.2"></script>
<script src="js/readmore.min.js?v=2.2.1"></script>

@stop