@extends('layouts.main')

@section('content')

<div class="x_panel">
    <div class="x_title">
        <h2>Báo cáo tổng hợp - OTB</h2>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <div class="row">
            <div class="col-sm-12">
                <form action="{{ route('report.otb') }}" method="get" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-2">Từ ngày: </label>
                        <div class="col-md-2">
                            <input autocomplete="off" type="text" name="from_date" id="from_date" value="{{ old('from_date')?old('from_date'):'' }}" class="form-control" required/>
                        </div>
                        <label class="control-label col-md-2">Đến ngày: </label>
                        <div class="col-md-2">
                            <input autocomplete="off" type="text" name="to_date" id="to_date" value="{{ old('to_date')?old('to_date'):'' }}" class="form-control" required/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2">Cơ sở: </label>
                        <div class="col-md-2">
                            <select name="department_id" class="form-control">
                                <?php echo \App\Utils::generateOptions(\App\Department::listData(), old('department_id'), 'Tất cả')?>
                            </select>
                        </div>
                        <label class="control-label col-md-2">Trạng thái: </label>
                        <div class="col-md-2">
                            <select id="status" name="status" class="form-control">
                                <?php echo \App\LeadStatus::generateOptions(\App\LeadStatus::listData(), old('status'), 'Tất cả')?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 text-right">
                            <button type="submit" class="btn btn-edufit-default">Xem</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class="container-fluid">
            <?php if(isset($data)){?>
            <div class="grid-view-max-content">
                <?php if(!empty($data)){ ?>
                <div class="no-class">
                    <form action="{{ route('report.otb.export') }}" method="post" target="_blank">
                        {{ csrf_field() }}
                        {{ csrf_field() }}
                        <input type="hidden" name="from_date" value="{{ old('from_date') }}">
                        <input type="hidden" name="to_date" value="{{ old('to_date') }}">
                        <input type="hidden" name="department_id" value="{{ old('department_id') }}">
                        <input type="hidden" name="status" value="{{ old('status') }}">

                        <button type="submit" class="btn btn-warning" download>
                            <i class="fa fa-file-excel-o"></i> Excel
                        </button>
                    </form>
                </div>
                <?php } ?>
                <div class="summary">Tất cả: <?php echo $total?></div>
                <table class="table table-bordered table-responsive table-striped">
                    <thead>
                    <tr>
                        <th>Stt</th>
                        <th>ID</th>
                        <th>Họ tên PH</th>
                        <th>Họ tên HS</th>
                        <th>Điện thoại</th>
                        <th>Ngày sinh</th>
                        <th>Nhóm lớp mong muốn</th>
                        <th>Hệ học mong muốn</th>
                        <th>Trạng thái</th>
                        <th>Nguồn dẫn</th>
                        <th>Chiến dịch</th>
                        <th>Người tạo</th>
                        <th>Ngày tạo</th>
                        <th>Tháng tạo</th>
                        <th>Người phụ trách</th>
                        <th>Ngày call log 1</th>
                        <th>Ngày call log 2</th>
                        <th>Ngày call log 3</th>
                        <th>Ngày call log 4</th>
                        <th>Ngày call log 5</th>
                        <th>Ngày call log 6</th>
                        <th>Ngày call log 7</th>
                        <th>Ngày call log 8</th>
                        <th>Ngày call log 9</th>
                        <th>Ngày call log 10</th>
                        <th>Ngày show up đến trường</th>
                        <th>Ngày show up đến hội thảo</th>
                        <th>Ngày giữ chỗ</th>
                        <th>Tháng giữ chỗ</th>
                        <th>Số phiếu thu tiền/báo có giữ chỗ</th>
                        <th>Mã học sinh</th>
                        <th>Ngày tham gia CT bé làm khách</th>
                        <th>Lớp học CT bé làm khách</th>
                        <th>Nhóm lớp CT bé làm khách</th>
                        <th>Hệ học CT bé làm khách</th>
                        <th>Số phiếu thu tiền/báo có CT bé làm khách</th>
                        <th>Ngày nhập học</th>
                        <th>Tháng nhập học</th>
                        <th>Tên lớp chính thức</th>
                        <th>Nhóm lớp chính thức</th>
                        <th>Hệ học chính thức</th>
                        <th>Số biên lai/báo có</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(empty($data)){
                        echo "<tr><td colspan='42'>Không có dữ liệu</td></tr>";
                    }else{
                    $stt = $limit*$offset;
                    foreach ($data as $item){ ?>
                    <tr>
                        <td><?php echo ++$stt?></td>
                        <td><?php echo $item->id?></td>
                        <td><?php echo $item->name?></td>
                        <td><?php echo $item->student_name?></td>
                        <td><?php echo $item->phone?></td>
                        <td><?php echo !empty($item->birthday) ? date('d/m/Y', strtotime($item->birthday)) : null?></td>
                        <td><?php echo $item->class_group_desire?></td>
                        <td><?php echo $item->learning_system_desire?></td>
                        <td><?php echo $item->status?></td>
                        <td><?php echo $item->source?></td>
                        <td><?php echo $item->campaign?></td>
                        <td><?php echo $item->created_by?></td>
                        <td><?php echo !empty($item->created_at) ? date('d/m/Y H:i:s', strtotime($item->created_at)) : null?></td>
                        <td><?php echo $item->created_at_month?></td>
                        <td><?php echo $item->assignee?></td>
                        <td><?php echo !empty($item->call_log_1) ? date('d/m/Y H:i:s', strtotime($item->call_log_1)) : null?></td>
                        <td><?php echo !empty($item->call_log_2) ? date('d/m/Y H:i:s', strtotime($item->call_log_2)) : null?></td>
                        <td><?php echo !empty($item->call_log_3) ? date('d/m/Y H:i:s', strtotime($item->call_log_3)) : null?></td>
                        <td><?php echo !empty($item->call_log_4) ? date('d/m/Y H:i:s', strtotime($item->call_log_4)) : null?></td>
                        <td><?php echo !empty($item->call_log_5) ? date('d/m/Y H:i:s', strtotime($item->call_log_5)) : null?></td>
                        <td><?php echo !empty($item->call_log_6) ? date('d/m/Y H:i:s', strtotime($item->call_log_6)) : null?></td>
                        <td><?php echo !empty($item->call_log_7) ? date('d/m/Y H:i:s', strtotime($item->call_log_7)) : null?></td>
                        <td><?php echo !empty($item->call_log_8) ? date('d/m/Y H:i:s', strtotime($item->call_log_8)) : null?></td>
                        <td><?php echo !empty($item->call_log_9) ? date('d/m/Y H:i:s', strtotime($item->call_log_9)) : null?></td>
                        <td><?php echo !empty($item->call_log_10) ? date('d/m/Y H:i:s', strtotime($item->call_log_10)) : null?></td>
                        <td><?php echo !empty($item->showup_school_at) ? date('d/m/Y H:i:s', strtotime($item->showup_school_at)) : null?></td>
                        <td><?php echo !empty($item->showup_seminor_at) ? date('d/m/Y H:i:s', strtotime($item->showup_seminor_at)) : null?></td>
                        <td><?php echo !empty($item->waiting_list_at) ? date('d/m/Y H:i:s', strtotime($item->waiting_list_at)) : null?></td>
                        <td><?php echo $item->waiting_list_at_month?></td>
                        <td><?php echo $item->waiting_list_receipt_number?></td>
                        <td><?php echo $item->student_code?></td>
                        <td><?php echo !empty($item->join_as_guest_at) ? date('d/m/Y H:i:s', strtotime($item->join_as_guest_at)) : null?></td>
                        <td><?php echo $item->class_name_guest?></td>
                        <td><?php echo $item->class_group_guest?></td>
                        <td><?php echo $item->learning_system_guest?></td>
                        <td><?php echo $item->guest_receipt_number?></td>
                        <td><?php echo !empty($item->enrolled_at) ? date('d/m/Y H:i:s', strtotime($item->enrolled_at)) : null?></td>
                        <td><?php echo $item->enrolled_at_month?></td>
                        <td><?php echo $item->class_name_official?></td>
                        <td><?php echo $item->class_group_official?></td>
                        <td><?php echo $item->learning_system_official?></td>
                        <td><?php echo $item->receipt_number?></td>
                    </tr>
                    <?php }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            @include('layouts.pagination')
            <?php }?>

        </div>

    </div>
</div>

<script>

    var full_month_names = new Array("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");
    $.datepicker.regional["vi-VN"] =
        {
            closeText: "Đóng",
            prevText: "Trước",
            nextText: "Sau",
            currentText: "Hôm nay",
            monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
            monthNamesShort: full_month_names,
            dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
            dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
            dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
            weekHeader: "Tuần",
            dateFormat: "dd/mm/yy",
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: "",
            changeMonth: true,
        };
    $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
    $('#from_date').datepicker();
    $('#to_date').datepicker();

</script>
@stop