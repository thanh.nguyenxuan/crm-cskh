@extends('layouts.main')

@section('content')

<div class="x_panel">
    <div class="x_title">
        <h2>Thống kê Log call của TVTS</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('report.call_log') }}" method="post" class="form-horizontal">
                    {{ csrf_field() }}
                    {{--                <div class="form-group">--}}
                    {{--                    <label class="control-label col-md-1">Từ ngày: </label>--}}
                    {{--                    <div class="col-md-2">--}}
                    {{--                        <input type="text" name="from_date" id="from_date"--}}
                    {{--                               value="{{ old('from_date')?old('from_date'):date('d/m/Y') }}" class="form-control">--}}
                    {{--                    </div>--}}
                    {{--                    <label class="control-label col-md-1">Đến ngày: </label>--}}
                    {{--                    <div class="col-md-2">--}}
                    {{--                        <input type="text" name="to_date" id="to_date"--}}
                    {{--                               value="{{ old('to_date')?old('to_date'):date('d/m/Y') }}" class="form-control">--}}
                    {{--                    </div>--}}
                    {{--                    <div class="col-md-1">--}}
                    {{--                        <button type="submit" class="btn btn-edufit-default">Xem</button>--}}
                    {{--                    </div>--}}
                    {{--                </div>--}}

                    <div class="form-group">
                        <label class="control-label col-md-2">Từ ngày: </label>
                        <div class="col-md-2">
                            <input type="text" name="from_date" id="from_date"
                                   value="{{ old('from_date')?old('from_date'):date('d/m/Y') }}" class="form-control">
                        </div>
                        <label class="control-label col-md-2">Đến ngày: </label>
                        <div class="col-md-2">
                            <input type="text" name="to_date" id="to_date"
                                   value="{{ old('to_date')?old('to_date'):date('d/m/Y') }}" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2">Chiều cuộc gọi: </label>
                        <div class="col-md-2">
                            <select id="CallLog_direction" name="direction" class="form-control">
                                {!!  \App\Lead\CallLog::getOptionsDirection(old('direction')) !!}
                            </select>
                        </div>

                        <label class="control-label col-md-2">Trạng thái cuộc gọi: </label>
                        <div class="col-md-2">
                            <select id="CallLog_status" name="status" class="form-control">
                                {!! \App\Lead\CallLog::getOptionsStatus(old('status')) !!}
                            </select>
                        </div>
                    </div>

                    <div class="form-group">

                        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding'))
                            <label class="control-label col-md-2">Cơ sở: </label>
                            <div class="col-md-2">
                                <select id="CallLog_department_id" name="department_id" class="form-control">
                                    {!!  \App\Lead\CallLog::getOptionsDepartment(old('department_id')) !!}
                                </select>
                            </div>
                        @endif

                        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding') || Auth::user()->hasRole('teleteamleader'))
                            <label class="control-label col-md-2">Telesales: </label>
                            <div class="col-md-2">
                                <select id="CallLog_user_id" name="user_id" class="form-control">
                                    {!! \App\Lead\CallLog::getOptionsUser(old('user_id'), old('department_id')) !!}
                                </select>
                            </div>
                        @endif

                    </div>

                    <div class="form-group">
                        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding'))
                            <label class="control-label col-md-2">Cấp cơ sở: </label>
                            <div class="col-md-2">
                                <select id="CallLog_department_level" name="department_level" class="form-control">
                                    {!!  \App\Lead\CallLog::getOptionsDepartmentLevel(old('department_level')) !!}
                                </select>
                            </div>
                        @endif


                        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding') || Auth::user()->hasRole('teleteamleader'))

                            <label class="control-label col-md-2">Role: </label>
                            <div class="col-md-2">
                                <select id="CallLog_role_id" name="role_id" class="form-control">
                                    {!! \App\Lead\CallLog::getOptionsUserRole(old('role_id')) !!}
                                </select>
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 text-right">
                            <button type="submit" class="btn btn-edufit-default">Xem</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-responsive">
                    <thead>
                    <th class="text-center">STT</th>
                    <th>Telesales</th>
                    @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding'))
                        <th>Cơ sở</th>
                    @endif
                    <th class="text-center">Số call log</th>
                    </thead>
                    <tbody>
                    @php $stt = 1; @endphp
                    @foreach($tele_list as $tele)
                        <tr>
                            <td class="text-center">{{ $stt++ }}</td>
                            <td>{{ $tele->username }}</td>
                            @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding'))
                                <td>{{ \App\Department::find(\App\User::find($tele->id)->department_id)->name }}</td>
                            @endif
                            <td class="text-center">{{ !empty($call_log_stats[$tele->id])?$call_log_stats[$tele->id]:0 }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<script>
    var full_month_names = new Array("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");
    $.datepicker.regional["vi-VN"] =
        {
            closeText: "Đóng",
            prevText: "Trước",
            nextText: "Sau",
            currentText: "Hôm nay",
            monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
            monthNamesShort: full_month_names,
            dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
            dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
            dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
            weekHeader: "Tuần",
            dateFormat: "dd/mm/yy",
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: "",
            changeMonth: true,
        };
    $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
    $('#from_date').datepicker();
    $('#to_date').datepicker();

    @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding'))
    $('#CallLog_department_id').on('change', function(){
        var department_id = this.value;
        $.ajax({
            url: '{{ env('APP_URL') }}/report/call_log/getUserByDepartment',
            type: 'post',
            dataType: 'html',
            data: {
                'department_id' : department_id,
                '_token': '{{ csrf_token() }}'
            },
            success : function(result){
                $('#CallLog_user_id').html(result);
            }
        })
    });
    @endif



</script>
@stop