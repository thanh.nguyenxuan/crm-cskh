@include('includes.header')
@php $stt = 1 @endphp
<div class="container">
    <h4>Thống kê lead</h4>
    <div class="col-md-12">
        <form action="{{ route('report.lead') }}" method="post">
            {{ csrf_field() }}
            <label>Từ ngày: </label><input type="text" name="from-date" id="from-date"
                                           value="{{ old('from-date')?old('from-date'):date('d/m/Y') }}">
            <label>Đến ngày: </label><input type="text" name="to-date" id="to-date"
                                            value="{{ old('to-date')?old('to-date'):date('d/m/Y') }}">
            <button type="submit">Xem</button>
        </form>
    </div>
    <div class="col-md-12">
        <table class="table table-bordered">
            <thead>
            <th>STT</th>
            <th>Tên đăng nhập</th>
            <th>Phòng</th>
            <th>Lead được chia</th>
            <th>Lead thực chia</th>
            <th>Lead chăm sóc</th>
            <th>Số cuộc gọi</th>
            <th>Thời lượng</th>
            <th>Active lead</th>
            <th>Hồ sơ</th>
            <th>Gọi lại sau</th>
            <th>Không quan tâm</th>
            <th>Trùng lead</th>
            <th>Sai số</th>
            <th>Không nghe máy</th>
            <th>Sai đối tượng</th>
            <th>Chuyển cơ sở</th>
            </thead>
            <tbody>
            @foreach($data as $user_name => $row)
                @if($user_name != 'summary')
                    <tr>
                        <td>{{ $stt++ }}</td>
                        <td>{{ $user_name }}</td>
                        <td>{{ $user_group_list[$user_name] }}</td>
                        <td>{{ $row['total'] }}</td>
                        <td>{{ $row['quality'] }}</td>
                        <td>{{ $row['modified'] }}</td>
                        <td>{{ $row['called'] }}</td>
                        <td>@php printf("%02d:%02d:%02d (%dp)", floor($row['duration']/3600), floor($row['duration']/60%60), $row['duration']%60, $row['duration']/60); @endphp</td>
                        <td>{{ $row['active'] }}</td>
                        <td>{{ $row['application'] }}</td>
                        <td>{{ $row['call_later'] }}</td>
                        <td>{{ $row['dont_care'] }}</td>
                        <td>{{ $row['duplicate'] }}</td>
                        <td>{{ $row['wrong_number'] }}</td>
                        <td>{{ $row['no_answer'] }}</td>
                        <td>{{ $row['mistargeted'] }}</td>
                        <td>{{ $row['transfered'] }}</td>
                    </tr>
                @endif
            @endforeach
            <tr>
                <td colspan="3"><strong>Tổng</strong></td>
                <td>{{ $data['summary']['total'] }}</td>
                <td>{{ $data['summary']['quality'] }}</td>
                <td>{{ $data['summary']['modified'] }}</td>
                <td>{{ $data['summary']['called'] }}</td>
                <td>@php  printf("%02d:%02d:%02d (%dp)", floor($data['summary']['duration']/3600), floor($data['summary']['duration']/60%60), $data['summary']['duration']%60, $data['summary']['duration']/60); @endphp</td>
                <td>{{ $data['summary']['active'] }}</td>
                <td>{{ $data['summary']['application'] }}</td>
                <td>{{ $data['summary']['call_later'] }}</td>
                <td>{{ $data['summary']['dont_care'] }}</td>
                <td>{{ $data['summary']['duplicate'] }}</td>
                <td>{{ $data['summary']['wrong_number'] }}</td>
                <td>{{ $data['summary']['no_answer'] }}</td>
                <td>{{ $data['summary']['mistargeted'] }}</td>
                <td>{{ $data['summary']['transfered'] }}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<script>
    $(function () {
        var full_month_names = new Array("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");
        $.datepicker.regional["vi-VN"] =
            {
                closeText: "Đóng",
                prevText: "Trước",
                nextText: "Sau",
                currentText: "Hôm nay",
                monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
                monthNamesShort: full_month_names,
                dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
                dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
                dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
                weekHeader: "Tuần",
                dateFormat: "dd/mm/yy",
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: "",
                changeMonth: true,
            };
        $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
        $("#from-date").datepicker();
        $("#to-date").datepicker();
    });
</script>
</body>
</html>