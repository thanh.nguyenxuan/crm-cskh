@extends('layouts.main')

@section('content')

<div class="x_panel">
    <div class="x_title">
        <h2>Thống kê tin nhắn của TVTS</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('report.logSms') }}" method="post" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label col-md-2">Từ ngày: </label>
                        <div class="col-md-2">
                            <input type="text" name="from_date" id="from_date"
                                   value="{{ old('from_date')?old('from_date'):date('d/m/Y') }}" class="form-control">
                        </div>
                        <label class="control-label col-md-2">Đến ngày: </label>
                        <div class="col-md-2">
                            <input type="text" name="to_date" id="to_date"
                                   value="{{ old('to_date')?old('to_date'):date('d/m/Y') }}" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2">Loại: </label>
                        <div class="col-md-2">
                            <select name="type" class="form-control">
                                {!!  \App\LogSms::getTypeOptions(old('type')) !!}
                            </select>
                        </div>

                        <label class="control-label col-md-2">Trạng thái tin nhắn: </label>
                        <div class="col-md-2">
                            <select name="status" class="form-control">
                                {!! \App\LogSms::getReportStatusOption(old('status')) !!}
                            </select>
                        </div>
                    </div>

                    <div class="form-group">

                        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding'))
                            <label class="control-label col-md-2">Cơ sở: </label>
                            <div class="col-md-2">
                                <select id="department_id" name="department_id" class="form-control">
                                    {!!  \App\Lead\CallLog::getOptionsDepartment(old('department_id')) !!}
                                </select>
                            </div>
                        @endif

                        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding') || Auth::user()->hasRole('teleteamleader'))
                            <label class="control-label col-md-2">User: </label>
                            <div class="col-md-2">
                                <select id="user_id" name="user_id" class="form-control">
                                    {!! \App\Lead\CallLog::getOptionsUser(old('user_id'), old('department_id')) !!}
                                </select>
                            </div>
                        @endif

                    </div>

                    <div class="form-group">
                        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding'))
                            <label class="control-label col-md-2">Cấp cơ sở: </label>
                            <div class="col-md-2">
                                <select name="department_level" class="form-control">
                                    {!!  \App\Lead\CallLog::getOptionsDepartmentLevel(old('department_level')) !!}
                                </select>
                            </div>
                        @endif


                        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding') || Auth::user()->hasRole('teleteamleader'))
                            <label class="control-label col-md-2">Role: </label>
                            <div class="col-md-2">
                                <select name="role_id" class="form-control">
                                    {!! \App\Lead\CallLog::getOptionsUserRole(old('role_id')) !!}
                                </select>
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2">Lead ID: </label>
                        <div class="col-md-2">
                            <input name="lead_id" value="{!! old('role_id') ? old('role_id') : '' !!}" class="form-control"/>
                        </div>

                        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding'))
                            <label class="control-label col-md-2">BrandName: </label>
                            <div class="col-md-2">
                                <select name="brand_name" class="form-control">
                                    {!! \App\LogSms::getBrandNameOptions(old('brand_name')) !!}
                                </select>
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 text-right">
                            <button type="submit" class="btn btn-edufit-default">Xem</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>


        @php if(!empty($data)){ @endphp
        <table class="table table-bordered table-responsive">
            <thead>
            <tr>
                <th>Stt</th>
                <th>BrandName</th>
                <th>Target</th>
                <th>Nội dung</th>
                <th>Trạng thái</th>
                <th>Thời gian gửi</th>
                <th>Thời gian tạo</th>
                <th>Người tạo</th>
                <td>Lead ID</td>
                <td>Loại</td>
                <td>#</td>
            </tr>
            </thead>
            <tbody>
            @php
                $stt = 0;
                $all_status = \App\LogSms::getAllStatus();
                $user_list = \App\User::all()->pluck('username', 'id');
                $type_list = \App\LogSms::getListType();
            @endphp
            @foreach($data as $log_sms)
                <tr>
                    <td>{{ ++$stt }}</td>
                    <td>{{ $log_sms->brand_name }}</td>
                    <td>{{ $log_sms->type == \App\LogSms::TYPE_MULTIPLE ? '...' : $log_sms->target }}</td>
                    <td>{{ $log_sms->content }}</td>
                    <td>{{ $all_status[$log_sms->status] }}</td>
                    <td>{{ !empty($log_sms->send_at) ? Carbon\Carbon::parse($log_sms->send_at)->format('d/m/Y H:i:s') : '' }}</td>
                    <td>{{ Carbon\Carbon::parse($log_sms->created_at)->format('d/m/Y H:i:s') }}</td>
                    <td>
                        {!! (!empty($log_sms->created_by) && isset($user_list[$log_sms->created_by])) ? $user_list[$log_sms->created_by] : '' !!}
                        {!! (!empty($log_sms->original_user_id) && isset($user_list[$log_sms->original_user_id])) ? '('.$user_list[$log_sms->original_user_id].')' : '' !!}
                    </td>
                    <td>
                        {!! $log_sms->type == \App\LogSms::TYPE_MULTIPLE ? '...' : ((!empty($log_sms->lead_id)) ? "<a target='_blank' href='/lead/edit/".$log_sms->lead_id."'>".$log_sms->lead_id."</a>" : '')!!}
                    </td>
                    <td>{!! (!empty($log_sms->type) && isset($type_list[$log_sms->type])) ? $type_list[$log_sms->type] : $log_sms->type!!}</td>
                    <td><a onclick="showSmsDetail('<?php echo $log_sms->id?>')"><i class="fa fa-eye"></i></a></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @php } @endphp
    </div>


</div>

<style>
    #modal_sms_detail .modal-body{
        max-height: 400px;
        overflow-y: auto;
    }
</style>

<div class="modal fade" id="modal_sms_detail" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Chi tiết tin nhắn #<span></span></h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-responsive">
                    <thead>
                    <tr>
                        <th>Lead ID</th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>


<script>
    var full_month_names = new Array("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");
    $.datepicker.regional["vi-VN"] =
        {
            closeText: "Đóng",
            prevText: "Trước",
            nextText: "Sau",
            currentText: "Hôm nay",
            monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
            monthNamesShort: full_month_names,
            dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
            dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
            dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
            weekHeader: "Tuần",
            dateFormat: "dd/mm/yy",
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: "",
            changeMonth: true,
        };
    $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
    $('#from_date').datepicker();
    $('#to_date').datepicker();

    @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding'))
    $('#department_id').on('change', function(){
        var department_id = this.value;
        $.ajax({
            url: '{{ env('APP_URL') }}/report/call_log/getUserByDepartment',
            type: 'post',
            dataType: 'html',
            data: {
                'department_id' : department_id,
                '_token': '{{ csrf_token() }}'
            },
            success : function(result){
                $('#user_id').html(result);
            }
        });
    });
    @endif
    var loadingSmsDetail = FALSE;
    function showSmsDetail(sms_id)
    {
        if(!loadingSmsDetail){
            loadingSmsDetail = true;
            $.ajax({
                url: '{{ env('APP_URL') }}/report/logSms/detail',
                type: 'post',
                dataType: 'json',
                data: {
                    'sms_id' : sms_id,
                    '_token': '{{ csrf_token() }}'
                },
                success : function(result){
                    console.log(result);
                    $("#modal_sms_detail .modal-header span").text(sms_id);
                    if(result.data.lead_ids && result.data.lead_ids.length > 0){
                        var html = '';
                        result.data.lead_ids.forEach(function (value,key) {
                            html+= '<tr><td><a href="/lead/edit/'+value+'" target="_blank">'+value+'</a></td></tr>'
                        });
                        $('#modal_sms_detail .modal-body table tbody').html(html);
                        $('#modal_sms_detail').modal('show');
                    }
                    loadingSmsDetail = false;
                }
            })
        }
    }

</script>
@stop