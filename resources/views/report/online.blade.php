@include('includes.header')
@php $stt = 1 @endphp
<div class="container">
    <table class="table table-bordered">
        <thead>
        <th>STT</th>
        <th>Tên đăng nhập</th>
        <th>Trạng thái</th>
        </thead>
        <tbody>
        @foreach($user_list as $user)
            <tr>
                <td>{{ $stt++ }}</td>
                <td>{{ $user->username }}</td>
                <td>
                    @if(\App\Http\Controllers\UtilsController::getOnlineStatus($user->id))
                        <span style="color: green">Online</span>
                        @else
                        <span style="color: red">Offline</span>
                    @endif

                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</body>
</html>