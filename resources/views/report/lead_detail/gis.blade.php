@extends('layouts.main')

@section('content')

<div class="x_panel">
    <div class="x_title">
        <h2>Báo cáo chi tiết Lead - GIS</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('report.leadDetail.gis') }}" method="post" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label col-md-2">Từ ngày: </label>
                        <div class="col-md-2">
                            <input type="text" name="from_date" id="from_date"
                                   value="{{ old('from_date')?old('from_date'):date('d/m/Y') }}" class="form-control">
                        </div>
                        <label class="control-label col-md-2">Đến ngày: </label>
                        <div class="col-md-2">
                            <input type="text" name="to_date" id="to_date"
                                   value="{{ old('to_date')?old('to_date'):date('d/m/Y') }}" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2">Cơ sở: </label>
                        <div class="col-md-2">
                            <select id="department_id" name="department_id" class="form-control">
                                {!!  \App\Department::getOptionsGIS(old('department_id')) !!}
                            </select>
                        </div>
                        <label class="control-label col-md-2">Người phụ trách: </label>
                        <div class="col-md-2">
                            <select id="assignee_id" name="assignee_id" class="form-control">
                                {!! \App\User::getOptionsUserGIS(old('assignee_id'), old('department_id')) !!}
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 text-right">
                            <button type="submit" class="btn btn-edufit-default">Xem</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="container-fluid">
            <?php if(isset($data)){?>
            <div class="grid-view-max-content">
                <?php if($data->isNotEmpty()){ ?>
                <div class="no-class">
                    <form action="{{ route('report.leadDetail.exportGIS') }}" method="post" target="_blank">
                        {{ csrf_field() }}
                        {{ csrf_field() }}
                        <input type="hidden" name="from_date" value="{{ old('from_date') }}">
                        <input type="hidden" name="to_date" value="{{ old('to_date') }}">
                        <input type="hidden" name="department_id" value="{{ old('department_id') }}">
                        <input type="hidden" name="assignee_id" value="{{ old('assignee_id') }}">

                        <button type="submit" class="btn btn-warning" download>
                            <i class="fa fa-file-excel-o"></i> Excel
                        </button>
                    </form>
                </div>
                <?php } ?>
                <div class="summary">Tất cả: <?php echo $total?></div>
                <table class="table table-bordered table-responsive table-striped">
                    <thead>
                    <tr>
                        <th rowspan="2">Stt</th>
                        <th colspan="17">Lead</th>
                        <th colspan="2">Show up</th>
                        <th colspan="4">Waiting List</th>
                        <th colspan="3">NS</th>
                        <th colspan="4">NE</th>
                    </tr>
                    <tr>
                        <th>ID</th>
                        <th>Họ và tên PHHS</th>
                        <th>Họ và tên HS</th>
                        <th>Số điện thoại</th>
                        <th>Ngày sinh</th>
                        <th>Hệ học</th>
                        <th>Cấp học</th>
                        <th>Khối</th>
                        <th>Loại Lead</th>
                        <th>Trạng thái Lead</th>
                        <th>Nguồn dẫn</th>
                        <th>Chiến dịch</th>
                        <th>Người tạo</th>
                        <th>Ngày tạo</th>
                        <th>Tháng tạo</th>
                        <th>Người phụ trách hiện tại</th>
                        <th>Người phụ trách trước</th>
                        <th>Ngày</th>
                        <th>Tháng</th>
                        <th>Ngày</th>
                        <th>Tháng</th>
                        <th>Số biên lai thu tiền/báo có giữ chỗ</th>
                        <th>Mã học sinh</th>
                        <th>Ngày</th>
                        <th>Tháng</th>
                        <th>Số biên lai/báo có</th>
                        <th>Ngày</th>
                        <th>Tháng</th>
                        <th>Tên lớp nhập học chính thức</th>
                        <th>Khối lớp nhập học chính thức</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if($data->isEmpty()){
                        echo "<tr><td colspan='31'>Không có dữ liệu</td></tr>";
                    }else{
                    $statusListData = \App\LeadStatus::all()->pluck('name', 'id')->toArray();
                    $userListData = \App\User::all()->pluck('username', 'id')->toArray();
                    $leadTypeListData = \App\LeadType::all()->pluck('name', 'id')->toArray();
                    $desireClassListData = \App\Major::all()->pluck('name', 'id')->toArray();
                    $officialClassListData = \App\OfficialClass::all()->pluck('name', 'id')->toArray();
                    $learningSystemDesireListData = \App\LearningSystemDesire::all()->pluck('name', 'id')->toArray();

                    $stt = $limit*$offset;
                    foreach ($data as $item){ ?>
                    <tr>
                        <td><?php echo ++$stt?></td>
                        <td><?php echo $item->id?></td>
                        <td><?php echo $item->name?></td>
                        <td><?php echo $item->student_name?></td>
                        <td><?php echo $item->phone?></td>
                        <td><?php echo (!empty($item->birthdate) ? date('d/m/Y', strtotime($item->birthdate)) : '')?></td>
                        <td><?php echo (!empty($item->learning_system_desire) ? $learningSystemDesireListData[$item->learning_system_desire] : '')?></td>
                        <td><?php echo (!empty($item->major) ? \App\Major::getSchoolLevel($item->major) : '')?></td>
                        <td><?php echo (!empty($item->major) ? $desireClassListData[$item->major] : '')?></td>
                        <td><?php echo (!empty($item->lead_type) ? $leadTypeListData[$item->lead_type] : '')?></td>
                        <td><?php echo (!empty($statusListData[$item->status]) ? $statusListData[$item->status] : '')?></td>
                        <td><?php echo $item->source?></td>
                        <td><?php echo $item->campaign?></td>
                        <td><?php echo (!empty($userListData[$item->created_by]) ? $userListData[$item->created_by] : '')?></td>
                        <td><?php echo (!empty($item->created_at) ? date('d/m/Y', strtotime($item->created_at)) : '')?></td>
                        <td><?php echo (!empty($item->created_at) ? date('m', strtotime($item->created_at)) : '')?></td>
                        <td><?php echo (!empty($userListData[$item->assignee_id]) ? $userListData[$item->assignee_id] : '')?></td>
                        <td><?php echo (!empty($userListData[$item->old_assignee_id]) ? $userListData[$item->old_assignee_id] : '')?></td>
                        <td><?php echo (!empty($item->showup_school_at) ? date('d/m/Y', strtotime($item->showup_school_at)) : '')?></td>
                        <td><?php echo (!empty($item->showup_school_at) ? date('m', strtotime($item->showup_school_at)) : '')?></td>
                        <td><?php echo (!empty($item->activated_at) ? date('d/m/Y', strtotime($item->activated_at)) : '')?></td>
                        <td><?php echo (!empty($item->activated_at) ? date('m', strtotime($item->activated_at)) : '')?></td>
                        <td><?php echo $item->status_waitlist_number?></td>
                        <td><?php echo $item->student_code?></td>
                        <td><?php echo (!empty($item->new_sale_at) ? date('d/m/Y', strtotime($item->new_sale_at)) : '')?></td>
                        <td><?php echo (!empty($item->new_sale_at) ? date('m', strtotime($item->new_sale_at)) : '')?></td>
                        <td><?php echo $item->receipt_number?></td>
                        <td><?php echo (!empty($item->enrolled_at) ? date('d/m/Y', strtotime($item->enrolled_at)) : '')?></td>
                        <td><?php echo (!empty($item->enrolled_at) ? date('m', strtotime($item->enrolled_at)) : '')?></td>
                        <td><?php echo $item->class_name?></td>
                        <td><?php echo (!empty($item->official_class) ? $officialClassListData[$item->official_class] : '')?></td>
                    </tr>
                    <?php }
                    }
                    ?>
                    </tbody>
                </table>
            </div>
            @include('layouts.pagination')
            <?php }?>

        </div>
    </div>

</div>



<script>
    var full_month_names = new Array("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");
    $.datepicker.regional["vi-VN"] =
        {
            closeText: "Đóng",
            prevText: "Trước",
            nextText: "Sau",
            currentText: "Hôm nay",
            monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
            monthNamesShort: full_month_names,
            dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
            dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
            dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
            weekHeader: "Tuần",
            dateFormat: "dd/mm/yy",
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: "",
            changeMonth: true,
        };
    $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
    $('#from_date').datepicker();
    $('#to_date').datepicker();

    $('#department_id').on('change', function(){
        var department_id = this.value;
        $.ajax({
            url: '{{ env('APP_URL') }}/report/leadDetail/getUserByDepartment',
            type: 'post',
            dataType: 'html',
            data: {
                'type' : 'GIS',
                'department_id' : department_id,
                '_token': '{{ csrf_token() }}'
            },
            success : function(result){
                $('#assignee_id').html(result);
            }
        })
    });
</script>
@stop
