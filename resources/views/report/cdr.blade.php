@extends('layouts.main')

@section('content')

<div class="x_panel">
    <div class="x_title">
        <h2>Chi tiết cuộc gọi</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('report.cdr') }}" method="post" class="form-horizontal">
                    {{ csrf_field() }}

                    <div class="text-danger">
                        @if(!empty($result) && $result == 'failed' && $message != 'No Data Found')
                            <p>{{ $message }}</p>
                        @endif
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2">Từ ngày: </label>
                        <div class="col-md-2">
                            <input type="text" name="start_date" id="start_date"
                                   value="{{ old('start_date')?old('start_date'):date('d/m/Y') }}" class="form-control">
                        </div>
                        <label class="control-label col-md-2">Đến ngày: </label>
                        <div class="col-md-2">
                            <input type="text" name="end_date" id="end_date"
                                   value="{{ old('end_date')?old('end_date'):date('d/m/Y') }}" class="form-control">
                        </div>
                        <div class="col-md-4">
                            <p class="note">Khoảng thời gian phải trong cùng tháng</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2">Chiều cuộc gọi: </label>
                        <div class="col-md-2">
                            <select id="Cdr_type" name="type" class="form-control">
                                {!!  \App\Report\Cdr::getOptionsDirection(old('type')) !!}
                            </select>
                        </div>

                        <label class="control-label col-md-2">Trạng thái cuộc gọi: </label>
                        <div class="col-md-2">
                            <select id="Cdr_status" name="status[]" class="form-control" multiple>
                                <optgroup label="Gọi đi">
                                    {!! \App\Report\Cdr::getOptionsStatus(old('status'), \App\Report\Cdr::DIRECTION_OUTBOUND) !!}
                                </optgroup>
                                <optgroup label="Gọi đến">
                                    {!! \App\Report\Cdr::getOptionsStatus(old('status'), \App\Report\Cdr::DIRECTION_INBOUND) !!}
                                </optgroup>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2">Số máy bàn: </label>
                        <div class="col-md-2">
                            <input type="text" name="extension" id="extension" value="{{ old('extension') }}" class="form-control">
                        </div>
                        <label class="control-label col-md-2">Số khách hàng: </label>
                        <div class="col-md-2">
                            <input type="text" name="phone_number" id="phone_number" value="{{ old('phone_number') }}" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 text-right">
                            <button type="submit" class="btn btn-edufit-default">Xem</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">

                <table class="table table-bordered table-responsive">
                    <thead>
                    <tr>
                        <th>STT</th>
                        <th>Số máy bàn</th>
                        <th>Số khách hàng</th>
                        <th>Loại</th>
                        <th>Trạng thái</th>
                        <th>Thời gian gọi</th>
                        <th>TG chờ (s)</th>
                        <th>TG cuộc gọi (s)</th>
                        <th>Nội dung</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(empty($data))
                        <tr>
                            <td colspan="9">
                                <p>Không tìm thấy dữ liệu</p>
                            </td>
                        </tr>
                    @else
                        @php
                            $stt = 1;
                            $list_direction = \App\Report\Cdr::getListDirection();
                            $list_status = \App\Report\Cdr::getListStatus();
                        @endphp
                        @foreach($data as $cdr)
                            <tr>
                                <td>{{ $stt++ }}</td>
                                <td>{{ $cdr->extension }}</td>
                                <td>
                                    @if(strlen($cdr->number_phone) > 10)
                                        {{ substr($cdr->number_phone,0,5) }}<b class="text-info">{{ substr($cdr->number_phone,5) }}</b>
                                    @else
                                        {{ $cdr->number_phone }}
                                    @endif
                                </td>
                                <td>{{ $list_direction[$cdr->type] }}</td>
                                <td>{{ $list_status[$cdr->status] }}</td>
                                <td>{{ $cdr->date_time }}</td>
                                <td>
                                    @if(!empty($cdr->wait_time))
                                        {{ $cdr->wait_time }}
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($cdr->talk_time))
                                        {{ $cdr->talk_time }}
                                    @endif
                                </td>
                                <td style="min-width: 260px">
                                    @if(!empty($cdr->talk_time) && ($cdr->talk_time > 0) && !empty($cdr->recordingfile))
                                        <div>
                                            <a onclick="loadRecordingFile($(this).closest('div'),'{!! $cdr->recordingfile !!}')"><i class="fa fa-file-audio-o"></i></a>
                                            File ghi âm
                                        </div>
                                    @endif
                                </td>
                                <td>
                                    @if(!empty($cdr->download_url))
                                        <div>
                                            <a onclick="downloadRecordingFile('{!! $cdr->download_url !!}')"><i class="fa fa-download"></i></a>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<script>
    var full_month_names = new Array("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");
    $.datepicker.regional["vi-VN"] =
        {
            closeText: "Đóng",
            prevText: "Trước",
            nextText: "Sau",
            currentText: "Hôm nay",
            monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
            monthNamesShort: full_month_names,
            dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
            dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
            dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
            weekHeader: "Tuần",
            dateFormat: "dd/mm/yy",
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: "",
            changeMonth: true,
        };
    $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
    $('#start_date').datepicker();
    $('#end_date').datepicker();

    $('#Cdr_status').select2();

    $('button[type=submit]').on('click', function(){
        $(this).addClass('disabled');
    });

    function loadRecordingFile(container, url)
    {
        $(container).html('<iframe width="240px" height="40px"  src="'+url+'"></iframe>');
    }

    function downloadRecordingFile(url)
    {
        window.open(url, '_blank');
    }

</script>

<div class="text-center">
    <h2>Test Click2Call</h2>
    <div class="form-group">
        <label for="ip_phone" style="width: 100px; text-align: right;">IP Phone</label>
        <input type="text" id="ip_phone" name="ip_phone" value="8778"/>
    </div>
    <div class="form-group">
        <label for="call_number" style="width: 100px; text-align: right;">Call Number</label>
        <input type="text" id="call_number" name="call_number" value="0333869688"/>
    </div>
    <div>
        <a onclick="connect()" class="btn btn-default">Connect</a>
        <a onclick="authen()" class="btn btn-primary">Authen</a>
        <a onclick="click2call()" class="btn btn-success">Click2Call</a>
        <a onclick="hangup()" class="btn btn-danger">Hangup</a>
    </div>
</div>


<script src="/js/socket.io.js"></script>
<script>

var connect_res, authen_res, click2call_res, hangup_res;
var socketConnect;
var cloud_pbx_host = 'https://cxcs001.cloudpbx.vn:8083';

function connect(){
    socketConnect = io.connect(cloud_pbx_host);
    socketConnect.on('connect', function(){
        connect_res = {'id' : socketConnect.id};
        console.log(connect_res);
    });
    socketConnect.on('message', function(response){
        console.log(response);
        switch (response.code) {
            case '301':
                if(response.status === 'success'){
                    authen_res = clone(response);
                }else if(response.status === 'fail'){
                    alert(response.message);
                }
                break;
            case '314':
                click2call_res = clone(response);
                if(response.status === 'fail'){
                    alert(response.message);
                }
                break;
            case '330':
                hangup_res = clone(response);
                if(hangup_res.status === 'success'){
                    click2call_res = false;
                }else if(response.status === 'fail'){
                    alert(response.message);
                }
                break;
        }
    });
}

function authen(){
    var ip_phone = $('#ip_phone').val();
    if(!ip_phone){
        alert('Please enter ip phone number');
        return;
    }
    if(!connect_res || typeof connect_res !== 'object' || !connect_res.id){
        alert("Please connect");
        return;
    }
    $.ajax({
        url: '{{ env('APP_URL') }}/report/cdr/getPbxConfig',
        type: 'post',
        dataType: 'json',
        data: {
            '_token': '{{ csrf_token() }}'
        },
        success : function(result){
            socketConnect.emit('authen', {
                'token': result.token,
                'organization' : result.organization,
                'extension' : ip_phone
            });
        }
    })

}

function click2call(){
    var call_number = $('#call_number').val();
    if(!call_number){
        alert('Please enter call number');
        return;
    }
    console.log(authen_res);
    if(!authen_res || typeof authen_res !== 'object' || authen_res.status !== 'success'){
        alert("Please authenticate the IP Phone number");
        return;
    }

    socketConnect.emit('click2call', {
        'phone_number' : call_number
    });
}

function hangup(){
    socketConnect.emit('hangup', {});
}

function clone(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
}

</script>

@stop