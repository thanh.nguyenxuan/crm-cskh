@extends('layouts.main')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Báo cáo chuyển đổi lead NE</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('report.leadConvert') }}" method="post" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label col-md-2">(Nhập học) Từ ngày: </label>
                        <div class="col-md-2">
                            <input type="text" name="from_date" id="from_date"
                                   value="{{ old('from_date')?old('from_date'):date('d/m/Y') }}" class="form-control">
                        </div>
                        <label class="control-label col-md-2">(Nhập học) Đến ngày: </label>
                        <div class="col-md-2">
                            <input type="text" name="to_date" id="to_date"
                                   value="{{ old('to_date')?old('to_date'):date('d/m/Y') }}" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding'))
                            <label class="control-label col-md-2">Cơ sở: </label>
                            <div class="col-md-2">
                                <select id="department_id" name="department_id" class="form-control">
                                    {!!  \App\Department::getOptions(old('department_id')) !!}
                                </select>
                            </div>
                        @endif

                        <label class="control-label col-md-2">TVTS: </label>
                        <div class="col-md-2">
                            <select id="assignee_id" name="assignee_id" class="form-control">
                                {!! \App\User::getOptionsUserByDepartment(old('assignee_id'), old('department_id')) !!}
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 text-right">
                            <button type="submit" class="btn btn-edufit-default">Xem</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="container">
            <?php if(isset($data)){?>
            <div class="text-right" style="margin-bottom: 10px">
                <form action="{{ route('report.leadConvert.export') }}" method="post" target="_blank">
                    {{ csrf_field() }}
                    <input type="hidden" name="from_date" value="{{ old('from_date') }}">
                    <input type="hidden" name="to_date" value="{{ old('to_date') }}">
                    <input type="hidden" name="department_id" value="{{ old('department_id') }}">
                    <input type="hidden" name="assignee_id" value="{{ old('assignee_id') }}">

                    <button type="submit" class="btn btn-warning" download>
                        <i class="fa fa-file-excel-o"></i> Excel
                    </button>
                </form>
            </div>

            <table class="table table-bordered table-responsive table-striped table-sticky-header">
                <thead>
                <tr>
                    <th style="width: 160px">Cơ sở</th>
                    <th>TVTS</th>
                    <th>Tổng số Lead NE</th>
                    <th>Chuyển đổi NE trong 1 tuần</th>
                    <th>Chuyển đổi NE trong 2 tuần</th>
                    <th>Chuyển đổi NE trong 3 tuần</th>
                    <th>Chuyển đổi NE sau 3 tuần</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $stt = 0;
                foreach ($data as $key => $item){
                if(!empty($item['detail'])){
                foreach ($item['detail'] as $sub_key => $sub_item){?>
                <tr>
                    <td><?php echo $item['name']?></td>
                    <td><?php echo $sub_item['name']?></td>
                    <td><?php echo $sub_item['total']?></td>
                    <td><?php echo $sub_item['1_week_convert']?></td>
                    <td><?php echo $sub_item['2_week_convert']?></td>
                    <td><?php echo $sub_item['3_week_convert']?></td>
                    <td><?php echo $sub_item['above_3_week_convert']?></td>
                </tr>
                <?php }
                }
                ?>
                <tr>
                    <th><?php echo $item['name']?></th>
                    <th>Tổng</th>
                    <th><?php echo $item['total']?></th>
                    <th><?php echo $item['1_week_convert']?></th>
                    <th><?php echo $item['2_week_convert']?></th>
                    <th><?php echo $item['3_week_convert']?></th>
                    <th><?php echo $item['above_3_week_convert']?></th>
                </tr>
                <?php }
                ?>
                </tbody>
            </table>
            <?php }?>
        </div>
    </div>

</div>



<script>
    var full_month_names = new Array("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");
    $.datepicker.regional["vi-VN"] =
        {
            closeText: "Đóng",
            prevText: "Trước",
            nextText: "Sau",
            currentText: "Hôm nay",
            monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
            monthNamesShort: full_month_names,
            dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
            dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
            dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
            weekHeader: "Tuần",
            dateFormat: "dd/mm/yy",
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: "",
            changeMonth: true,
        };
    $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
    $('#from_date').datepicker();
    $('#to_date').datepicker();

    $('#department_id').on('change', function(){
        var department_id = this.value;
        $.ajax({
            url: '{{ env('APP_URL') }}/user/getUserByDepartment',
            type: 'post',
            dataType: 'html',
            data: {
                'type' : 'SMIS',
                'department_id' : department_id,
                'only_telesales' : true,
                '_token': '{{ csrf_token() }}'
            },
            success : function(result){
                $('#assignee_id').html(result);
            }
        })
    });
</script>

@stop