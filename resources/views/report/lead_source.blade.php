@extends('layouts.main')

@section('content')

    <script type="text/javascript" src="/js/datatables.min.js"></script>
    <script src="//cdn.datatables.net/plug-ins/1.10.13/dataRender/datetime.js"></script>
    <script src="/js/dataTables.select.min.js"></script>
    <script src="/js/dataTables.fixedHeader.min.js"></script>

    <link rel="stylesheet" type="text/css"  href="/css/datatables.min.css"/>

<div class="col-sm-12"><h2>Báo cáo nguồn dẫn Lead</h2></div>

<script type="text/javascript">
    function pad2(number) {
        return (number < 10 ? '0' : '') + number
    }

    var main_table;
    $(document).ready(function () {
        $.fn.dataTable.ext.errMode = 'none';
        // $('#main_grid').on('error.dt', function (e, settings, techNote, message) {
        //     alert('Đã có lỗi trong quá trình tải dữ liệu. Vui lòng refresh lại trang web và kiểm tra lại. Nếu vẫn gặp lỗi hãy liên hệ với cán bộ kỹ thuật để được hỗ trợ.');
        //     console.log('error');
        //     console.log("--e:");
        //     console.log(e);
        //     console.log("--settings:");
        //     console.log(settings);
        //     console.log("--techNote:");
        //     console.log(techNote);
        //     console.log("--message:");
        //     console.log(message);
        // });
        main_table = $('#main_grid').DataTable({
            "dom": '<"top"ilpB>rti<"bottom"flp><"clear">',
            "buttons": [
                {extend: 'selectAll', className: "fa fa-lg fa-check-square-o"},
                {extend: 'selectNone', className: "fa fa-lg fa-square-o"},
                {
                    text: '',
                    className: 'fa fa-lg fa-refresh',
                    title: 'Tải lại',
                    action: function (e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }
            ],
            "language": {
                url: '/js/dataTables.i18n.vi.lang',
                buttons: {
                    selectAll: "",
                    selectNone: ""
                },
                select: {
                    rows: {
                        _: "Đã chọn %d dòng",
                        1: "Đã chọn 1 dòng"
                    }
                },
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Đang xử lý...</span>',
            },
            select: true,
            "searching": false,
            "deferRender": true,
            "selected": true,
            "paging": true,
            "pageLength": {{ !empty($filtering->length)?$filtering->length:50 }},
            "displayStart": {{ !empty($filtering->start)?$filtering->start:0 }},
            "lengthMenu": [[10, 50, 100, 200, 300, 500], [10, 50, 100, 200, 300, 500]],
            "processing": true,
            "serverSide": true,
            "ordering": true,
            fixedHeader: true,
            "ajax": {
                "url": "/report/leadSourceSearch",
                "type": "POST",
                "data": function (d) {
                    d._token = '{{ csrf_token() }}';
                    d.gsearch = $('#gsearch').val();
                    d.search_mode = ($('#gsearch').val()) ? $('#search_mode').val() : 'advanced_search';
                    d.name = $('#name').val();
                    d.student_name = $('#student_name').val();
                    d.phone = $('#phone').val();
                    d.email = $('#email').val();
                    d.birthdate = $('#birthdate').val();
                    d.birthyear = $('#birthyear').val();
                    d.status_id = $('#status_id').val();
                    d.call_status_id = $('#call_status_id').val();
                    d.source = $('#source').val();
                    d.school = $('#school').val();
                    d.major = $('#major').val();
                    d.province = $('#province').val();
                    d.department_id = $('#department').val();
                    d.start_date = $('#start_date').val();
                    d.end_date = $('#end_date').val();
                    d.active_from = $('#active_from').val();
                    d.active_to = $('#active_to').val();
                    d.enrolled_from = $('#enrolled_from').val();
                    d.enrolled_to = $('#enrolled_to').val();
                    d.modified_from = $('#modified_from').val();
                    d.modified_to = $('#modified_to').val();
                    d.assigned_from = $('#assigned_from').val();
                    d.assigned_to = $('#assigned_to').val();
                    d.showup_school_from = $('#showup_school_from').val();
                    d.showup_school_to = $('#showup_school_to').val();
                    d.showup_seminor_from = $('#showup_seminor_from').val();
                    d.showup_seminor_to = $('#showup_seminor_to').val();
                    d.join_as_guest_from = $('#join_as_guest_from').val();
                    d.join_as_guest_to = $('#join_as_guest_to').val();
                    d.hot_lead_from = $('#hot_lead_from').val();
                    d.hot_lead_to = $('#hot_lead_to').val();
                    d.assignee = $('#assignee').val();
                    d.channel = $('#channel').val();
                    d.campaign = $('#campaign').val();
                    d.campaign_category = $('#campaign_category').val();
                    d.keyword = $('#keyword').val();
                    d.activator = $('#activator').val();
                    d.caller = $('#caller').val();
                    d.creator = $('#creator').val();
                    d.notes = $('#notes').val();
                    d.rating = $('#rating').val();

                },
                "error": function (xhr, error, code)
                {
                    console.log('--xhr:');
                    console.log(xhr);
                    console.log('--error:');
                    console.log(error);
                    console.log('--code:');
                    console.log(code);
                    alert('Đã có lỗi trong quá trình tải dữ liệu. Vui lòng refresh lại trang web và kiểm tra lại. Nếu vẫn gặp lỗi hãy liên hệ với cán bộ kỹ thuật để được hỗ trợ.');
                }
            },
            "columns": [
                {
                    "data": "id",
                    visible: false
                },
                {
                    "data": "name",
                    "orderable": true,
                    "render": function (data, type, row) {
                        var color_highlighted = '';
                        if (row['new_duplicate'] && (row['department_id'] == 4)) {
                            color_highlighted = ' color: red; font-weight: bold; ';
                        }
                        return '<a target="_blank" style="' + color_highlighted + '" href="/lead/edit/' + row['id'] + '" data-id="' + row['id'] + '">' + row['name'] + '</a>';
                    }
                },

                {
                    "data": "phone",
                    "render": function (data, type, row) {
                        if (data) {
                            var phone_list = data.split(',');
                            var render_data = '';
                            phone_list.forEach(function (element, index, array) {
                                render_data += "<a href='javascript:void(0)' class='phone-number' data-lead-id='" + row['id'] + "' data-phone='" + element + "'>" + element + "</a>";
                                if (index != (array.length - 1))
                                    render_data += '<br />';
                            });
                            return render_data;
                        }
                    },
                    className: 'phoneNumber'
                },
                {
                    "data": "email", "className": "break",
                    "render": function (data, type, row) {
                        if (row['email'] && row['email'].match('@'))
                            return '<a href="mailto:' + row['email'] + '">' + row['email'] + '</a>';
                        else
                            return row['email'];
                    }
                },

                {
                    "data": "address",
                },
                {
                    "data": "student_name",
                },
                {
                    "data": "birthdate",
                    "defaultContent": "",
                    "render": function (data, type, row) {
                        if (data) {
                            var date = new Date(data);
                            var month = pad2(date.getMonth() + 1);
                            var day = pad2(date.getDate());
                            var year = date.getFullYear();
                            return day + "/" + month + "/" + year;
                            //return $.fn.dataTable.render.moment( data, 'YYYY-MM-DD').format('DD/MM/YYYY');
                        }
                    }
                },
                {
                    "data": "school",
                },
                {
                    "data": "lead_status_name",
                    orderable: false,
                    "defaultContent": "",
                    "render": function (data, type, row) {
                        switch (row['status']) {
                            case '1':  return "<b style='color: #28a745;'>Mới<b>";
                            case '17': return "<b style='color: #007bff;'>Tiềm năng/ Potential<b>";
                            case '9':  return "<b style='color: #ffc107;'>Đặt cọc giữ chỗ/ Waiting list<b>";
                            case '15': return "<b style='color: #dc3545;'>Nhập học/ NE<b>";
                            case '6':  return "<b style='color: #343a40;'>Không chăm sóc nữa/ Dead<b>";
                            default: return data;
                        }
                    }
                },
                {
                    "data": "call_status_name",
                    orderable: false
                },
                {
                    "data" : 'total_log_call',
                    orderable: false
                },
                {
                    "data" : 'last_log_call',
                    "render": function (data, type, row) {
                        if (row['last_log_call']) {
                            var last_log_call = row['last_log_call'];
                            return "<span class='last_log_call' data-readmore aria-expanded='false' style='max-height: 80px'>" + last_log_call.replace("\r\n", "<br />") + "</span>";
                        } else {
                            return '';
                        }
                    },
                    orderable: false
                },
                {
                    "data": "notes",
                    "render": function (data, type, row) {
                        if (row['notes']) {
                            var notes = row['notes'];
                            return "<span class='notes' data-readmore aria-expanded='false' style='max-height: 80px'>" + notes.replace("\r\n", "<br />") + "</span>";
                        } else {
                            return '';
                        }
                    },
                    orderable: false
                },
                {"data": "assignee_list"},
                {
                    "data": "source_list",
                    "render": function (data, type, row) {
                        var source_list = row['source_list'];
                        if (source_list)
                            source_list = source_list.replace(/,/g, '<br />');
                        return source_list;
                    },
                    orderable: false
                },
                {
                    "data": "info_source_name",
                },
                {
                    "data": "channel_list",
                    "render": function (data, type, row) {
                        var channel_list = row['channel_list'];
                        if (channel_list)
                            channel_list = channel_list.replace(/,/g, '<br />');
                        return channel_list;
                    },
                    orderable: false
                },
                {
                    "data": "campaign_list",
                    "render": function (data, type, row) {
                        var campaign_list = row['campaign_list'];
                        if (campaign_list)
                            campaign_list = campaign_list.replace(/,/g, '<br />');
                        return campaign_list;
                    },
                    orderable: false
                },
                {
                    "data": "keyword_list",
                    "render": function (data, type, row) {
                        var keyword_list = row['keyword_list'];
                        if (keyword_list)
                            keyword_list = keyword_list.replace(/,/g, '<br />');
                        return keyword_list;
                    },
                    orderable: false
                },
                {"data": "department"},
                {"data": "creator"},
                {
                    "data": "created_at",
                    "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'DD/MM/YYYY HH:mm:ss'),
                },
            ]
        });
        main_table.on('draw', function () {
            $('.notes').readmore({
                collapsedHeight: 55,
                moreLink: '<a href="#"><i class="fa fa-caret-square-o-down" aria-hidden="true"></i></a>',
                lessLink: '<a href="#"><i class="fa fa-caret-square-o-up" aria-hidden="true"></i></a>'
            });
            $('.last_log_call').readmore({
                collapsedHeight: 55,
                moreLink: '<a href="#"><i class="fa fa-caret-square-o-down" aria-hidden="true"></i></a>',
                lessLink: '<a href="#"><i class="fa fa-caret-square-o-up" aria-hidden="true"></i></a>'
            });
        });

        $('#search_form').hide();
        $("#reset_button").click(function () {
            $('#school').val('');
            $('#name').val('');
            $('#student_name').val('');
            $('#phone').val('');
            $('#email').val('');
            $('#birthdate').val('');
            $('#birthyear').val('');
            $('#source').val([]);
            $('#province').val([]);
            $('#major').val('');
            @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('onlinemarketer') || Auth::user()->hasRole('promoterleader'))
            $('#status_id').val('');
            @else
            $('#status_id').val([1]);
            @endif
            $('#call_status_id').val([]);
            $('#department').val([]);
            $('#start_date').val('');
            $('#end_date').val('');
            $('#active_from').val('');
            $('#active_to').val('');
            $('#enrolled_from').val('');
            $('#enrolled_to').val('');
            $('#modified_from').val('');
            $('#modified_to').val('');
            $('#assigned_from').val('');
            $('#assigned_to').val('');
            $('#import_count').val('');
            $('#caller').val([]);
            $('#activator').val([]);
            $('#assignee').val([]);
            $('#creator').val([]);
            $('#notes').val('');
            $('#rating').val([]);
            $('#campaign').val([]);
            $('#campaign_category').val([]);
            $('#channel').val([]);
            $('#showup_school_from').val('');
            $('#showup_school_to').val('');
            $('#showup_seminor_from').val('');
            $('#showup_seminor_to').val('');
            $('#join_as_guest_from').val('');
            $('#join_as_guest_to').val('');
            $('#hot_lead_from').val('');
            $('#hot_lead_to').val('');
            $('#department').trigger('change');
            $('#campaign_category').trigger('change');

            $.get("{{ route('lead.clearFilters') }}");
        });
        $('#lead_assignment_button').click(function () {
            $("#lead-assignment-modal").modal('show');
            var selected = [];
            var selected_items = main_table.rows({selected: true}).data().toArray();
            for (var i = 0; i < selected_items.length; i++)
                selected.push(selected_items[i].id);
            $('#selected_count').text(selected.length);
            //console.log(main_table.rows( { selected: true } ).data().toArray());
        });
        $('#assign_button').click(function () {
            var url = '{{ env('APP_URL') }}/lead/assign';
            var selected = [];
            var selected_items = main_table.rows({selected: true}).data().toArray();
            for (var i = 0; i < selected_items.length; i++)
                selected.push(selected_items[i].id);
            var additional_assignment = $("input[name=additional_assignment]:checked").val();
            var data = new Object();
            if (additional_assignment)
                data.additional_assignment = additional_assignment;
            data.selected = selected;
            data.assignee = $('#assignee_list').find(":selected").val();
            data._token = "{{ csrf_token() }}";
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            if (selected.length > 0) {
                $.post(url, data, status, 'json');
                main_table.ajax.reload();
            }
            $("#lead-assignment-modal").modal('hide');
            $("#additional_assignment").prop('checked', false);
        });

        $('select#department').on('change', function(){
            var departments = $(this).val();
            var url = '{{ env('APP_URL') }}/lead/getUserByDepartment';
            var data = new Object();
            data.departments = departments;
            data._token = "{{ csrf_token() }}";
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            $.post(url, data, function(data,status,xhr){
                $('select#creator').html(data.option.creator);
                $('select#assignee').html(data.option.assignee);
                $('select#activator').html(data.option.activator);
            }, 'json');
        });

        $('select#campaign_category').on('change', function(){
            var campaign_categories = $(this).val();
            var url = '{{ env('APP_URL') }}/lead/getCampaignByCategory';
            var data = new Object();
            data.campaign_categories = campaign_categories;
            data._token = "{{ csrf_token() }}";
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
            $.post(url, data, function(data,status,xhr){
                $('select#campaign').html(data.option.campaign);
            }, 'json');
        });


        $("#main_grid tbody").on('click', "a.phone-number", function () {
            var phone = $(this).data('phone');
            var lead_id = $(this).data('lead-id');
            //var trunk = $('#trunk').val();
            var trunk = {{ $trunk_id?$trunk_id:0 }};
            $.get('{{ env('APP_URL') }}/call/' + phone + '/' + trunk);
            console.log('Calling ' + phone + '...');
            window.open('{{ env('APP_URL') }}/lead/edit/' + lead_id, '_blank');
        });

    });
</script>

<form class="form-inline search-form" id="search_form" method="post">
    {{ csrf_field() }}

    <div class="col-md-12 no_pad">
        <div class="col-md-4">
            <div class="form-group">
                <label for="name">Họ tên phụ huynh</label>
                <input type="text" class="form-control" id="name" name="name"
                       value="{{ !empty($filtering->name)?$filtering->name:'' }}">
            </div>

            <div class="form-group">
                <label for="phone">SĐT</label>
                <input type="text" class="form-control" id="phone" name="phone"
                       value="{{ !empty($filtering->phone)?$filtering->phone:'' }}">
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" id="email" name="email"
                       value="{{ !empty($filtering->email)?$filtering->email:'' }}">
            </div>

            <div class="form-group">
                <label for="notes">Ghi chú</label>
                <input type="text" class="form-control" id="notes" name="notes"
                       value="{{ !empty($filtering->notes)?$filtering->notes:'' }}">
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="student_name">Họ tên học sinh</label>
                <input type="text" class="form-control" id="student_name" name="student_name"
                       value="{{ !empty($filtering->student_name)?$filtering->student_name:'' }}">
            </div>

            <div class="form-group">
                <label for="birthdate">Ngày sinh</label>
                <input type="text" class="form-control" id="birthdate" name="birthdate"
                       value="{{ !empty($filtering->birthdate)?$filtering->birthdate:'' }}" placeholder="dd/mm/yyyy">
            </div>

            <div class="form-group">
                <label for="birthyear">Năm sinh</label>
                <input type="text" class="form-control" id="birthyear" name="birthyear"
                       value="{{ !empty($filtering->birthyear)?$filtering->birthyear:'' }}"
                       placeholder="Dạng 4 số, ví dụ 2000">
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="school">Trường đang học</label>
                <input type="text" class="form-control" id="school" name="school"
                       value="{{ !empty($filtering->school)?$filtering->school:'' }}">
            </div>

            <div class="form-group">
                <label for="major">Lớp học quan tâm</label>
                {{ Form::select('major[]', $major_list, !empty($filtering->major)?$filtering->major:array(), ['id' => 'major', 'multiple' => true, 'class' => 'form-control']) }}
            </div>
        </div>
    </div>

    <div class="col-md-12 no_pad">
        <div class="col-md-4">
            <div class="form-group">
                <label for="source">Nguồn dẫn</label>
                {{ Form::select('source[]', $source_list, !empty($filtering->source)?$filtering->source:'', ['id' => 'source', 'multiple' => true, 'class' => 'form-control']) }}
            </div>
            <div class="form-group">
                <label for="province">Tỉnh/Thành phố</label>
                {{ Form::select('province_id[]', $province_list, !empty($filtering->province)?$filtering->province:array(), ['id' => 'province', 'multiple' => true, 'class' => 'form-control']) }}
            </div>

            <div class="form-group">
                <label for="department">Cơ sở</label>
                {{ Form::select('department_id[]', $department_list, !empty($filtering->department_id)?$filtering->department_id:array(), ['id' => 'department', 'multiple' => true, 'class' => 'form-control']) }}
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label for="channel">Kênh QC</label>
                {{ Form::select('channel[]', $channel_list, !empty($filtering->channel)?$filtering->channel:'', ['id' => 'channel', 'multiple' => true, 'class' => 'form-control']) }}
            </div>
            <div class="form-group">
                <label for="keyword">Content QC</label>
                {{ Form::select('keyword[]', $keyword_list, !empty($filtering->keyword)?$filtering->keyword:'', ['id' => 'keyword', 'multiple' => true, 'class' => 'form-control']) }}
            </div>
            <div class="form-group">
                <label for="campaign_category">Loại chiến dịch</label>
                {{ Form::select('campaign_category[]', $campaign_category_list, !empty($filtering->campaign_category)?$filtering->campaign_category:'', ['id' => 'campaign_category', 'multiple' => true, 'class' => 'form-control']) }}
            </div>
            <div class="form-group">
                <label for="campaign">Campaign</label>
                {{ Form::select('campaign[]', $campaign_list, !empty($filtering->campaign)?$filtering->campaign:'', ['id' => 'campaign', 'multiple' => true, 'class' => 'form-control']) }}
            </div>
        </div>

        <div class="col-md-4">

            <div class="form-group<?php echo (Auth::user()->hasRole('carer')) ? ' hidden' : ''?>">
                <label for="status_id">Trạng thái lead</label>
                {{ Form::select('status_id[]', $status_list, !empty($filtering->status_id)?$filtering->status_id:array(), ['id' => 'status_id', 'multiple' => true, 'class' => 'form-control']) }}
            </div>
            <div class="form-group">
                <label for="call_status_id">Trạng thái chăm sóc</label>
                {{ Form::select('call_status_id[]', $call_status_list, !empty($filtering->call_status_id)?$filtering->call_status_id:array(), ['id' => 'call_status_id', 'multiple' => true, 'class' => 'form-control']) }}
            </div>

            <div class="form-group">
                <label for="rating">Mức độ quan tâm</label>
                {{ Form::select('rating[]', $rating_list, !empty($filtering->rating)?$filtering->rating:'', ['id' => 'rating', 'multiple' => true, 'class' => 'form-control']) }}
            </div>
        </div>
    </div>

    <div class="col-md-12 no_pad">
        <div class="col-md-4">
            <div class="form-group">
                <label for="creator">Người tạo</label>
                <br/>
                {{ Form::select('creator[]', $creator_list, !empty($filtering->creator)?$filtering->creator:array(), ['id' => 'creator', 'multiple' => true, 'class' => 'form-control']) }}
            </div>
            <div class="form-group">
                <label for="assignee">Người được chia</label>
                <br/>
                {{ Form::select('assignee[]', $assignee_list, !empty($filtering->assignee)?$filtering->assignee:array(), ['id' => 'assignee', 'multiple' => true, 'class' => 'form-control']) }}
            </div>
            <div class="form-group">
                <label for="activator">Người active</label>
                <br/>
                {{ Form::select('activator[]', $activator_list, !empty($filtering->activator)?$filtering->activator:array(), ['id' => 'activator', 'multiple' => true, 'class' => 'form-control']) }}
            </div>

        </div>

        <div class="col-md-8">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="start_date">Ngày tạo (từ ngày)</label>
                        <input type="text" class="form-control" id="start_date" name="start_date"
                               value="{{ !empty($filtering->start_date)?$filtering->start_date:'' }}" placeholder="dd/mm/yyyy">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="end_date">Ngày tạo (đến ngày)</label>
                        <input type="text" class="form-control" id="end_date" name="end_date"
                               value="{{ !empty($filtering->end_date)?$filtering->end_date:'' }}" placeholder="dd/mm/yyyy">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="active_from">Đặt cọc giữ chỗ (từ ngày)</label>
                        <input type="text" class="form-control" id="active_from" name="active_from"
                               value="{{ !empty($filtering->active_from)?$filtering->active_from:'' }}" placeholder="dd/mm/yyyy">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="active_to">Đặt cọc giữ chỗ (đến ngày)</label>
                        <input type="text" class="form-control" id="active_to" name="active_to"
                               value="{{ !empty($filtering->active_to)?$filtering->active_to:'' }}" placeholder="dd/mm/yyyy">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="enrolled_from">Nhập học (từ ngày)</label>
                        <input type="text" class="form-control" id="enrolled_from" name="enrolled_from"
                               value="{{ !empty($filtering->enrolled_from)?$filtering->enrolled_from:'' }}"
                               placeholder="dd/mm/yyyy">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="enrolled_to">Nhập học (đến ngày)</label>
                        <input type="text" class="form-control" id="enrolled_to" name="enrolled_to"
                               value="{{ !empty($filtering->enrolled_to)?$filtering->enrolled_to:'' }}" placeholder="dd/mm/yyyy">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="modified_from">Chăm sóc (từ ngày)</label>
                        <input type="text" class="form-control" id="modified_from" name="modified_from"
                               value="{{ !empty($filtering->modified_from)?$filtering->modified_from:'' }}"
                               placeholder="dd/mm/yyyy">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="modified_to">Chăm sóc (đến ngày)</label>
                        <input type="text" class="form-control" id="modified_to" name="modified_to"
                               value="{{ !empty($filtering->modified_to)?$filtering->modified_to:'' }}" placeholder="dd/mm/yyyy">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="assigned_from">Phân công (từ ngày)</label>
                        <input type="text" class="form-control" id="assigned_from" name="assigned_from"
                               value="{{ !empty($filtering->assigned_from)?$filtering->assigned_from:'' }}"
                               placeholder="dd/mm/yyyy">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="assigned_to">Phân công (đến ngày)</label>
                        <input type="text" class="form-control" id="assigned_to" name="assigned_to"
                               value="{{ !empty($filtering->assigned_to)?$filtering->assigned_to:'' }}" placeholder="dd/mm/yyyy">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="showup_school_from">Show Up/đến trường (từ ngày)</label>
                        <input type="text" class="form-control" id="showup_school_from" name="showup_school_from"
                               value="{{ !empty($filtering->showup_school_from)?$filtering->showup_school_from:'' }}"
                               placeholder="dd/mm/yyyy">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="showup_school_to">Show Up/đến trường (đến ngày)</label>
                        <input type="text" class="form-control" id="showup_school_to" name="showup_school_to"
                               value="{{ !empty($filtering->showup_school_to)?$filtering->showup_school_to:'' }}"
                               placeholder="dd/mm/yyyy">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="showup_seminor_from">Show Up/hội thảo (từ ngày)</label>
                        <input type="text" class="form-control" id="showup_seminor_from" name="showup_seminor_from"
                               value="{{ !empty($filtering->showup_seminor_from)?$filtering->showup_seminor_from:'' }}"
                               placeholder="dd/mm/yyyy">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="showup_seminor_to">Show Up/hội thảo (đến ngày)</label>
                        <input type="text" class="form-control" id="showup_seminor_to" name="showup_seminor_to"
                               value="{{ !empty($filtering->showup_seminor_to)?$filtering->showup_seminor_to:'' }}"
                               placeholder="dd/mm/yyyy">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="join_as_guest_from">Bé làm khách (từ ngày)</label>
                        <input type="text" class="form-control" id="join_as_guest_from" name="join_as_guest_from"
                               value="{{ !empty($filtering->join_as_guest_from)?$filtering->join_as_guest_from:'' }}"
                               placeholder="dd/mm/yyyy">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="join_as_guest_to">Bé làm khách (đến ngày)</label>
                        <input type="text" class="form-control" id="join_as_guest_to" name="join_as_guest_to"
                               value="{{ !empty($filtering->join_as_guest_to)?$filtering->join_as_guest_to:'' }}"
                               placeholder="dd/mm/yyyy">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="hot_lead_from">Hot lead (từ ngày)</label>
                        <input type="text" class="form-control" id="hot_lead_from" name="hot_lead_from"
                               value="{{ !empty($filtering->hot_lead_from)?$filtering->hot_lead_from:'' }}"
                               placeholder="dd/mm/yyyy">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="hot_lead_to">Hot lead (đến ngày)</label>
                        <input type="text" class="form-control" id="hot_lead_to" name="hot_lead_to"
                               value="{{ !empty($filtering->hot_lead_to)?$filtering->hot_lead_to:'' }}"
                               placeholder="dd/mm/yyyy">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <button type="submit" class="btn btn-edufit-default" id="search_button"><i class="fa fa-filter"
                                                                                       aria-hidden="true"></i> Tìm kiếm
            </button>
            <button type="button" class="btn btn-edufit-default" id="reset_button"><i class="fa fa-eraser"
                                                                                      aria-hidden="true"></i> Xóa lọc
            </button>
            <button type="button" class="btn btn-edufit-default" id="hide_search_form_2"><i class="fa fa-eye-slash"
                                                                                            aria-hidden="true"></i>
                Ẩn bộ lọc
            </button>
        </div>
    </div>
    <input type="hidden" name="search_mode" id="search_mode" value="advanced_search">

    <input type="hidden" id="export_action" name="export" value=""/>
</form>
<div class="row">
    <div class="col-md-12 pull-right utils">
        <div class="buttons">
            <button type="button" class="btn btn-edufit-default" id="hide_search_form">
                <i class="fa fa-eye" aria-hidden="true"></i>
                Hiện bộ lọc
            </button>
            <button type="button" class="btn btn-edufit-default" id="lead_source_export_button">
                <i class="fa fa-download" aria-hidden="true"></i>
                Kết xuất
            </button>
        </div>
    </div>
</div>
<div class="x_panel">
    <div class="col-md-12 table-responsive" style="overflow-x: unset">
        <table id="main_grid" class="table table-striped table-bordered table-hover" cellspacing="0"
               width="100%">
            <thead>
            <tr>
                <th>ID</th>
                <th>Họ tên phụ huynh</th>
                <th>SĐT</th>
                <th>Email</th>
                <th>Địa chỉ</th>
                <th>Họ tên học sinh</th>
                <th>Ngày sinh</th>
                <th>Trường đang học</th>
                <th>Trạng thái lead</th>
                <th>Trạng thái chăm sóc</th>
                <th>Tổng số Log Call</th>
                <th>Log Call gần nhất</th>
                <th>Ghi chú</th>
                <th>Người phụ trách</th>
                <th>Nguồn dẫn</th>
                <th>Nguồn thông tin</th>
                <th>Kênh QC</th>
                <th>Campaign</th>
                <th>Content QC</th>
                <th>Cơ sở</th>
                <th>Người tạo</th>
                <th>Ngày tạo</th>
            </tr>
            </thead>
        </table>
    </div>
</div>
@include('includes.assignment', ['assignee_list' => $assignee_list])
<script src="/js/filter.js?v=1.1.8"></script>
<script src="/js/readmore.min.js?v=2.2.1"></script>
<script>
    $('#lead_source_export_button').click(function () {
        $('#search_form').attr('action', '/report/leadSourceSearch');
        $('#export_action').val('export');
        console.log($('#export_action').val());
        $('#search_mode').val('advanced_search');
        $('#search_form').submit();
    });
</script>
@stop