@extends('layouts.main')

@section('content')

<div class="x_panel">
    <div class="x_title">
        <h2>Chỉ số thời gian chăm sóc Lead</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('report.leadInteractive') }}" method="post" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label col-md-2">Từ ngày: </label>
                        <div class="col-md-2">
                            <input type="text" name="from_date" id="from_date"
                                   value="{{ old('from_date')?old('from_date'):date('d/m/Y') }}" class="form-control">
                        </div>
                        <label class="control-label col-md-2">Đến ngày: </label>
                        <div class="col-md-2">
                            <input type="text" name="to_date" id="to_date"
                                   value="{{ old('to_date')?old('to_date'):date('d/m/Y') }}" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">

                        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding'))
                            <label class="control-label col-md-2">Cơ sở: </label>
                            <div class="col-md-2">
                                <select id="department_id" name="department_id" class="form-control">
                                    {!!  \App\Lead\CallLog::getOptionsDepartment(old('department_id')) !!}
                                </select>
                            </div>
                        @endif

                        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding') || Auth::user()->hasRole('teleteamleader'))
                            <label class="control-label col-md-2">Người tạo: </label>
                            <div class="col-md-2">
                                <select id="user_id" name="user_id" class="form-control">
                                    {!! \App\Lead\CallLog::getOptionsUser(old('user_id'), old('department_id')) !!}
                                </select>
                            </div>
                        @endif

                    </div>

                    <div class="form-group">
                        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding') || Auth::user()->hasRole('teleteamleader'))
                            <label class="control-label col-md-2">Người phụ trách: </label>
                            <div class="col-md-2">
                                <select id="assignee_id" name="assignee_id" class="form-control">
                                    {!! \App\Lead\CallLog::getOptionsUser(old('assignee_id'), old('department_id')) !!}
                                </select>
                            </div>
                        @endif

                        <label class="control-label col-md-2">Trạng thái: </label>
                        <div class="col-md-2">
                            <select id="interact" name="interact" class="form-control">
                                {!! \App\Report\LeadInteractive::getListInteractOptions(old('interact')) !!}
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 text-right">
                            <button type="submit" class="btn btn-edufit-default">Xem</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        @php if(!empty($data)){ @endphp
        <div class="text-right" style="margin-bottom: 10px">
            <form action="{{ route('report.leadInteractive.export') }}" method="post" target="_blank">
                {{ csrf_field() }}
                <input type="hidden" name="from_date" value="{{ old('from_date') }}">
                <input type="hidden" name="to_date" value="{{ old('to_date') }}">
                <input type="hidden" name="department_id" value="{{ old('department_id') }}">
                <input type="hidden" name="user_id" value="{{ old('user_id') }}">
                <input type="hidden" name="assignee_id" value="{{ old('assignee_id') }}">
                <input type="hidden" name="interact" value="{{ old('interact') }}">
                <input type="hidden" name="lead_id" value="{{ old('lead_id') }}">

                <button type="submit" class="btn btn-warning" download>
                    <i class="fa fa-file-excel-o"></i> Excel
                </button>
            </form>
        </div>
        <table class="table table-bordered table-responsive table-striped table-sticky-header">
            <thead>
            <tr>
                <th>Stt</th>
                <th>Lead</th>
                <th>SĐT</th>
                <th>Người tạo</th>
                <th>Người phụ trách</th>
                <th>Cở sở</th>
                <th>Ngày tạo</th>
                <th>Ngày tương tác đầu tiên</th>
                <th>Thời gian tương tác (ngày)</th>
            </tr>
            </thead>
            <tbody>
            @php
                $stt = 0;
                $user_list = \App\User::all()->pluck('username', 'id');
                $department_list = \App\Department::all()->pluck('name', 'id');
            @endphp
            @foreach($data as $lead)
                <tr>
                    <td>{{ ++$stt }}</td>
                    <td>
                        <a href="/lead/edit/{{ $lead->id }}" target="_blank">
                            {{ $lead->name ? $lead->name : ($lead->student_name ? $lead->student_name : $lead->nick_name) }}
                        </a>
                    </td>
                    <td>{{ $lead->phone1 ? $lead->phone1 : $lead->phone2 }}</td>
                    <td>{!! (!empty($lead->created_by) && isset($user_list[$lead->created_by])) ? $user_list[$lead->created_by] : '' !!}</td>
                    <td>{!! (!empty($lead->assignee) && isset($user_list[$lead->assignee])) ? $user_list[$lead->assignee] : '' !!}</td>
                    <td>{!! (!empty($lead->department_id) && isset($department_list[$lead->department_id])) ? $department_list[$lead->department_id] : '' !!}</td>
                    <td>{{ Carbon\Carbon::parse($lead->created_at)->format('d/m/Y H:i:s') }}</td>
                    <td>{{ !empty($lead->first_call_log_time) ? Carbon\Carbon::parse($lead->first_call_log_time)->format('d/m/Y H:i:s') : ''}}</td>
                    <td>{{ $lead->diff_day }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @php } @endphp
    </div>


</div>
<script>
    var full_month_names = new Array("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");
    $.datepicker.regional["vi-VN"] =
        {
            closeText: "Đóng",
            prevText: "Trước",
            nextText: "Sau",
            currentText: "Hôm nay",
            monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
            monthNamesShort: full_month_names,
            dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
            dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
            dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
            weekHeader: "Tuần",
            dateFormat: "dd/mm/yy",
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: "",
            changeMonth: true,
        };
    $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
    $('#from_date').datepicker();
    $('#to_date').datepicker();

    @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding'))
    $('#department_id').on('change', function(){
        var department_id = this.value;
        $.ajax({
            url: '{{ env('APP_URL') }}/report/call_log/getUserByDepartment',
            type: 'post',
            dataType: 'html',
            data: {
                'department_id' : department_id,
                '_token': '{{ csrf_token() }}'
            },
            success : function(result){
                $('#user_id').html(result);
                $('#assignee_id').html(result);
            }
        })
    });
    @endif

</script>
@stop