@include('includes.header')
<script src="/js/bootstrap.min.js"></script>
<div class="col-md-12">
    <form action="{{ route('report.dmr') }}" method="post">
        {{ csrf_field() }}
        <label>Từ ngày: </label><input type="text" name="from_date" id="from_date"
                                       value="{{ old('from_date')?old('from_date'):date('d/m/Y') }}">
        <label>Đến ngày: </label><input type="text" name="to_date" id="to_date"
                                        value="{{ old('to_date')?old('to_date'):date('d/m/Y') }}">
        <button type="submit">Xem</button>
    </form>
</div>
<div class="container">
    @if($from_date == $to_date)
        <h3>DMR ngày {{ \Carbon\Carbon::parse($from_date)->format('d/m/Y') }}</h3>
    @else
        <h3>DMR từ {{ \Carbon\Carbon::parse($from_date)->format('d/m/Y') }}
            đến {{ \Carbon\Carbon::parse($to_date)->format('d/m/Y') }}</h3>
    @endif
    <table class="table table-bordered table-responsive">
        <thead>
        <th>Ngành</th>
        <th>Dự thi</th>
        <th>Xét</th>
        <th>Thi HB</th>
        <th>Xét + Dự thi</th>
        <th>Xét + Thi HB</th>
        <th><strong>2018</strong></th>
        </thead>
        <tbody>
        <tr class="group">
            <td><strong>ICT</strong></td>
            <td>{{ $data[2]['ict'] }}</td>
            <td>{{ $data[1]['ict'] }}</td>
            <td>{{ $data[4]['ict'] }}</td>
            <td>{{ $data[3]['ict'] }}</td>
            <td>{{ $data[5]['ict'] }}</td>
            <td>{{ $data['ict']['total'] }}</td>
        </tr>
        <tr>
            <td>Kỹ thuật phần mềm</td>
            <td>{{ $data[2][4] }}</td>
            <td>{{ $data[1][4] }}</td>
            <td>{{ $data[4][4] }}</td>
            <td>{{ $data[3][4] }}</td>
            <td>{{ $data[5][4] }}</td>
            <td>{{ $data[4]['total'] }}</td>
        </tr>
        <tr>
            <td>An toàn thông tin</td>
            <td>{{ $data[2][5] }}</td>
            <td>{{ $data[1][5] }}</td>
            <td>{{ $data[4][5] }}</td>
            <td>{{ $data[3][5] }}</td>
            <td>{{ $data[5][5] }}</td>
            <td>{{ $data[5]['total'] }}</td>
        </tr>
        <tr>
            <td>Khoa học máy tính</td>
            <td>{{ $data[2][6] }}</td>
            <td>{{ $data[1][6] }}</td>
            <td>{{ $data[4][6] }}</td>
            <td>{{ $data[3][6] }}</td>
            <td>{{ $data[5][6] }}</td>
            <td>{{ $data[6]['total'] }}</td>
        </tr>
        <tr class="group">
            <td><strong>BIZ</strong></td>
            <td>{{ $data[2]['biz'] }}</td>
            <td>{{ $data[1]['biz'] }}</td>
            <td>{{ $data[4]['biz'] }}</td>
            <td>{{ $data[3]['biz'] }}</td>
            <td>{{ $data[5]['biz'] }}</td>
            <td>{{ $data['biz']['total'] }}</td>
        </tr>
        <tr>
            <td>Quản trị kinh doanh</td>
            <td>{{ $data[2][2] }}</td>
            <td>{{ $data[1][2] }}</td>
            <td>{{ $data[4][2] }}</td>
            <td>{{ $data[3][2] }}</td>
            <td>{{ $data[5][2] }}</td>
            <td>{{ $data[2]['total'] }}</td>
        </tr>
        <tr>
            <td>Quản trị khách sạn</td>
            <td>{{ $data[2][11] }}</td>
            <td>{{ $data[1][11] }}</td>
            <td>{{ $data[4][11] }}</td>
            <td>{{ $data[3][11] }}</td>
            <td>{{ $data[5][11] }}</td>
            <td>{{ $data[11]['total'] }}</td>
        </tr>
        <tr>
            <td>Kinh doanh quốc tế</td>
            <td>{{ $data[2][3] }}</td>
            <td>{{ $data[1][3] }}</td>
            <td>{{ $data[4][3] }}</td>
            <td>{{ $data[3][3] }}</td>
            <td>{{ $data[5][3] }}</td>
            <td>{{ $data[3]['total'] }}</td>
        </tr>
        <tr class="group">
            <td><strong>C&L</strong></td>
            <td>{{ $data[2]['cl'] }}</td>
            <td>{{ $data[1]['cl'] }}</td>
            <td>{{ $data[4]['cl'] }}</td>
            <td>{{ $data[3]['cl'] }}</td>
            <td>{{ $data[5]['cl'] }}</td>
            <td>{{ $data['cl']['total'] }}</td>
        </tr>
        <tr>
            <td>Ngôn ngữ Anh</td>
            <td>{{ $data[2][7] }}</td>
            <td>{{ $data[1][7] }}</td>
            <td>{{ $data[4][7] }}</td>
            <td>{{ $data[3][7] }}</td>
            <td>{{ $data[5][7] }}</td>
            <td>{{ $data[7]['total'] }}</td>
        </tr>
        <tr>
            <td>Ngôn ngữ Nhật</td>
            <td>{{ $data[2][8] }}</td>
            <td>{{ $data[1][8] }}</td>
            <td>{{ $data[4][8] }}</td>
            <td>{{ $data[3][8] }}</td>
            <td>{{ $data[5][8] }}</td>
            <td>{{ $data[8]['total'] }}</td>
        </tr>
        <tr>
            <td>Ngôn ngữ Trung</td>
            <td>{{ $data[2][10] }}</td>
            <td>{{ $data[1][10] }}</td>
            <td>{{ $data[4][10] }}</td>
            <td>{{ $data[3][10] }}</td>
            <td>{{ $data[5][10] }}</td>
            <td>{{ $data[10]['total'] }}</td>
        </tr>
        <tr>
            <td>Truyền thông đa phương tiện</td>
            <td>{{ $data[2][13] }}</td>
            <td>{{ $data[1][13] }}</td>
            <td>{{ $data[4][13] }}</td>
            <td>{{ $data[3][13] }}</td>
            <td>{{ $data[5][13] }}</td>
            <td>{{ $data[13]['total'] }}</td>
        </tr>
        <tr class="group">
            <td><strong>AA</strong></td>
            <td>{{ $data[2]['aa'] }}</td>
            <td>{{ $data[1]['aa'] }}</td>
            <td>{{ $data[4]['aa'] }}</td>
            <td>{{ $data[3]['aa'] }}</td>
            <td>{{ $data[5]['aa'] }}</td>
            <td>{{ $data['aa']['total'] }}</td>
        </tr>
        <tr>
            <td>Thiết kế đồ họa</td>
            <td>{{ $data[2][1] }}</td>
            <td>{{ $data[1][1] }}</td>
            <td>{{ $data[4][1] }}</td>
            <td>{{ $data[3][1] }}</td>
            <td>{{ $data[5][1] }}</td>
            <td>{{ $data[1]['total'] }}</td>
        </tr>
        <tr class="group">
        <td><strong>Total</strong></td>
        <td>{{ $data[2]['total'] }}</td>
        <td>{{ $data[1]['total'] }}</td>
        <td>{{ $data[4]['total'] }}</td>
        <td>{{ $data[3]['total'] }}</td>
        <td>{{ $data[5]['total'] }}</td>
        <td>{{ $data['total']['total'] }}</td>
        </tr>
        </tbody>
    </table>
</div>
<script>
    var full_month_names = new Array("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");
    $.datepicker.regional["vi-VN"] =
        {
            closeText: "Đóng",
            prevText: "Trước",
            nextText: "Sau",
            currentText: "Hôm nay",
            monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
            monthNamesShort: full_month_names,
            dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
            dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
            dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
            weekHeader: "Tuần",
            dateFormat: "dd/mm/yy",
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: "",
            changeMonth: true,
        };
    $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
    $('#from_date').datepicker();
    $('#to_date').datepicker();
</script>
</body>
</html>