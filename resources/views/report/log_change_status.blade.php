@extends('layouts.main')

@section('content')

<div class="x_panel">
    <div class="x_title">
        <h2>Lịch sử thay đổi trạng thái Lead</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('report.logChangeStatus') }}" method="post" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label col-md-2">Từ ngày: </label>
                        <div class="col-md-2">
                            <input type="text" name="from_date" id="from_date"
                                   value="{{ old('from_date')?old('from_date'):date('d/m/Y') }}" class="form-control">
                        </div>
                        <label class="control-label col-md-2">Đến ngày: </label>
                        <div class="col-md-2">
                            <input type="text" name="to_date" id="to_date"
                                   value="{{ old('to_date')?old('to_date'):date('d/m/Y') }}" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding'))
                            <label class="control-label col-md-2">Cơ sở: </label>
                            <div class="col-md-2">
                                <select id="department_id" name="department_id" class="form-control">
                                    {!!  \App\Lead\CallLog::getOptionsDepartment(old('department_id')) !!}
                                </select>
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 text-right">
                            <button type="submit" class="btn btn-edufit-default">Xem</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <?php if(!empty($data)){ ?>
        <div class="text-right" style="margin-bottom: 10px">
            <form action="{{ route('report.logChangeStatus.export') }}" method="post" target="_blank">
                {{ csrf_field() }}
                <input type="hidden" name="from_date" value="{{ old('from_date') }}">
                <input type="hidden" name="to_date" value="{{ old('to_date') }}">
                <input type="hidden" name="department_id" value="{{ old('department_id') }}">

                <button type="submit" class="btn btn-warning" download>
                    <i class="fa fa-file-excel-o"></i> Excel
                </button>
            </form>
        </div>
        <?php $list_status = \App\LeadStatus::where('hide', 0)->orderBy('id', 'ASC')->pluck('name','id')->toArray(); ?>
        <table class="table table-bordered table-responsive table-striped table-sticky-header">
            <thead>
            <tr>
                <th class="text-center" rowspan="2">ID</th>
                <th class="text-center" rowspan="2">Lead</th>
                <th class="text-center" rowspan="2">Trạng thái hiện tại</th>
                <th class="text-center" rowspan="2">Ngày tạo</th>
                <th class="text-center" colspan="<?php echo count($list_status)?>">Thời gian thay đổi trạng thái</th>
            </tr>
            <tr>
                <?php foreach ($list_status as $status_id => $status_name){ ?>
                <th class="text-center"><?php echo $status_name?></th>
                <?php } ?>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($data as $lead_id => $lead_info){?>
            <tr>
                <td><a href="{{ env('APP_URL') }}/lead/edit/<?php echo $lead_id?>"><?php echo $lead_id?></a></td>
                <td><a href="{{ env('APP_URL') }}/lead/edit/<?php echo $lead_id?>"><?php echo $lead_info['lead']->name?></a></td>
                <td><?php echo isset($list_status[$lead_info['lead']->status]) ? $list_status[$lead_info['lead']->status] : ''?></td>
                <td><?php echo $lead_info['lead']->created_at?></td>
                <?php foreach ($lead_info['log_status'] as $status_id => $change_time) { ?>
                <td><?php echo $change_time?></td>
                <?php } ?>
            </tr>
            <?php } ?>
            </tbody>
        </table>
        <?php } ?>
    </div>

</div>
<style>
    .table-sticky-header thead tr:nth-child(2) th{
        top: 36px
    }
</style>
<script>
    var full_month_names = new Array("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");
    $.datepicker.regional["vi-VN"] =
        {
            closeText: "Đóng",
            prevText: "Trước",
            nextText: "Sau",
            currentText: "Hôm nay",
            monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
            monthNamesShort: full_month_names,
            dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
            dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
            dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
            weekHeader: "Tuần",
            dateFormat: "dd/mm/yy",
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: "",
            changeMonth: true,
        };
    $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
    $('#from_date').datepicker();
    $('#to_date').datepicker();

</script>
@stop