@extends('layouts.main')

@section('content')

<div class="x_panel">
    <div class="x_title">
        <h2>Báo cáo thời lượng cuộc gọi</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('report.callLogDuration') }}" method="post" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label col-md-2">Từ ngày: </label>
                        <div class="col-md-2">
                            <input type="text" name="from_date" id="from_date"
                                   value="{{ old('from_date')?old('from_date'):date('d/m/Y') }}" class="form-control">
                        </div>
                        <label class="control-label col-md-2">Đến ngày: </label>
                        <div class="col-md-2">
                            <input type="text" name="to_date" id="to_date"
                                   value="{{ old('to_date')?old('to_date'):date('d/m/Y') }}" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding'))
                            <label class="control-label col-md-2">Nhóm Cơ sở: </label>
                            <div class="col-md-2">
                                <select id="department_group" name="department_group" class="form-control">
                                    {!!  \App\Department::getOptionsGroup(old('department_group')) !!}
                                </select>
                            </div>
                        @endif

                        <label class="control-label col-md-2">TVTS: </label>
                        <div class="col-md-2">
                            <select id="assignee_id" name="assignee_id" class="form-control">
                                {!! \App\User::getOptionsUserByGroupDepartment(old('assignee_id'), old('department_group')) !!}
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding'))
                            <label class="control-label col-md-2">Cơ sở: </label>
                            <div class="col-md-2">
                                <select id="department_ids" name="department_ids[]" class="form-control" multiple>
                                    {!!  \App\Department::getOptions(old('department_ids'), TRUE) !!}
                                </select>
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 text-right">
                            <button type="submit" class="btn btn-edufit-default">Xem</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="container">
        <?php if(isset($data)){?>
        <div class="text-right" style="margin-bottom: 10px">
            <form action="{{ route('report.callLogDuration.export') }}" method="post" target="_blank">
                {{ csrf_field() }}
                <input type="hidden" name="from_date" value="{{ old('from_date') }}">
                <input type="hidden" name="to_date" value="{{ old('to_date') }}">
                <input type="hidden" name="department_group" value="{{ old('department_group') }}">
                <select name="department_ids[]" class="hidden" multiple>
                    {!!  \App\Department::getOptions(old('department_ids'), TRUE) !!}
                </select>
                <input type="hidden" name="assignee_id" value="{{ old('assignee_id') }}">

                <button type="submit" class="btn btn-warning" download>
                    <i class="fa fa-file-excel-o"></i> Excel
                </button>
            </form>
        </div>

        <table class="table table-bordered table-responsive table-striped table-sticky-header">
            <thead>
            <tr>
                <th>Giờ (00:00 -> 59:59)</th>
                <th>Có nghe máy</th>
                <th>Dưới 1 phút</th>
                <th>Từ 1 đến 3 phút</th>
                <th>Trên 3 phút</th>
                <th>Không nghe máy</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $stt = 0;
            foreach ($data as $key => $item){ ?>
            <tr>
                <td><?php echo ($key < 10) ? "0$key" : $key?></td>
                <td><?php echo $item['success']['total']?></td>
                <td><?php echo $item['success']['under_1']?></td>
                <td><?php echo $item['success']['between_1_3']?></td>
                <td><?php echo $item['success']['above_3']?></td>
                <td><?php echo $item['fail']['total']?></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>
        <?php }?>
    </div>

</div>


<script>
    var full_month_names = new Array("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");
    $.datepicker.regional["vi-VN"] =
        {
            closeText: "Đóng",
            prevText: "Trước",
            nextText: "Sau",
            currentText: "Hôm nay",
            monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
            monthNamesShort: full_month_names,
            dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
            dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
            dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
            weekHeader: "Tuần",
            dateFormat: "dd/mm/yy",
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: "",
            changeMonth: true,
        };
    $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
    $('#from_date').datepicker();
    $('#to_date').datepicker();

</script>
@stop