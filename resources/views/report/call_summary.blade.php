@extends('layouts.main')

@section('content')
@php $stt = 1 @endphp
<div class="x_panel">
    <div class="x_title">
        <h2>Thống kê cuộc gọi CTV</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="col-md-12">
            <form action="{{ route('report.call_summary') }}" method="post">
                {{ csrf_field() }}
                <label>Từ ngày: </label><input type="text" name="from_date" id="from_date"
                                               value="{{ old('from_date')?old('from_date'):date('d/m/Y') }}">
                <label>Đến ngày: </label><input type="text" name="to_date" id="to_date"
                                                value="{{ old('to_date')?old('to_date'):date('d/m/Y') }}">
                <button type="submit">Xem</button>
            </form>
        </div>
        <h4>CTV TT Hà Nội</h4>
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                <th>STT</th>
                <th>Số máy lẻ</th>
                <th>Số cuộc gọi</th>
                <th>Thời lượng</th>
                </thead>
                <tbody>
                @foreach($hn as $ext => $row)
                    <tr>
                        <td>{{ $stt++ }}</td>
                        <td>{{ $ext }}</td>
                        <td>{{ $row->calls }}</td>
                        <td>@php  printf("%02d:%02d:%02d (%dp)", floor($row->duration/3600), floor($row->duration/60%60), $row->duration%60, $row->duration/60); @endphp</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @php $stt = 1 @endphp
        <h4>CTV TT Hồ Chí Minh</h4>
        <div class="col-md-12">
            <table class="table table-bordered">
                <thead>
                <th>STT</th>
                <th>Số máy lẻ</th>
                <th>Số cuộc gọi</th>
                <th>Thời lượng</th>
                </thead>
                <tbody>
                @foreach($hcm as $ext => $row)
                    <tr>
                        <td>{{ $stt++ }}</td>
                        <td>{{ $ext }}</td>
                        <td>{{ $row->calls }}</td>
                        <td>@php  printf("%02d:%02d:%02d (%dp)", floor($row->duration/3600), floor($row->duration/60%60), $row->duration%60, $row->duration/60); @endphp</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

</div>
<script>
    $(function () {
        var full_month_names = new Array("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");
        $.datepicker.regional["vi-VN"] =
            {
                closeText: "Đóng",
                prevText: "Trước",
                nextText: "Sau",
                currentText: "Hôm nay",
                monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
                monthNamesShort: full_month_names,
                dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
                dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
                dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
                weekHeader: "Tuần",
                dateFormat: "dd/mm/yy",
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: "",
                changeMonth: true,
            };
        $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
        $("#from_date").datepicker();
        $("#to_date").datepicker();
    });
</script>
@stop