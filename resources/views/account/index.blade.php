@include('includes.header')
<?php
//var_dump($filters);
?>
<div id="edit_form_container">
</div>
<script type="text/javascript">
    function pad2(number) {

        return (number < 10 ? '0' : '') + number

    }

    $(document).ready(function () {
        $('#main_grid').DataTable({
            "buttons": [
                'selectAll',
                'selectNone',
            ],
            "language": {
                buttons: {
                    selectAll: "Select all items",
                    selectNone: "Select none"
                }
            },
            "searching": false,
            "select": true,
            "paging": false,
            "processing": false,
            "serverSide": true,
            "ordering": false,
            "ajax": {
                "url": "/account/list",
                "type": "POST",
                "data": function (d) {
                    d._token = '{{ csrf_token() }}';
                    d.gsearch = $('#gsearch').val();
                    d.name = $('#name').val();
                    d.phone = $('#phone').val();
                    d.email = $('#email').val();
                    d.source = $('#source').val();
                    d.school = $('#school').val();
                    d.province = $('#province').val();
                    d.import_count = $('#import_count').val();
                    d.import_count_operator = $('#import_count_operator').val();
                    d.department = $('#department').val();
                    d.area = $('#area').val();
                    d.start_date = $('#start_date').val();
                    d.end_date = $('#end_date').val();
                    d.active_from = $('#active_from').val();
                    d.active_to = $('#active_to').val();
                    d.search_mode = $('#search_mode').val();
                    d.assignee = $('#assignee').val();
                    d.activator = $('#activator').val();
                    d.creator = $('#creator').val();
                    d.notes = $('#notes').val();
                }
            },
            //"order": [[13, "asc"]],
            "columns": [
                {"data": "app_id"},
                {"data": "app_code"},
                {"data": "assessment_name"},
                {
                    "data": "created_at",
                    "render": $.fn.dataTable.render.moment('YYYY-MM-DD HH:mm:ss', 'DD/MM/YYYY HH:mm:ss'),
                },
                {"data": "app_type_name"},
                {"data": "method_normal_name"},
                {"data": "method_scholarship_name"},
                {"data": "name"},
                {
                    "data": "gender",
                    "render": function (data, type, row) {
                        if (row['gender'] == 1)
                            return 'Nam';
                        if (row['gender'] == 2)
                            return 'Nữ';
                        return '';
                    }
                },
                {
                    "data": "birthdate",
                    "render": function (data, type, row) {
                        //console.log(data);
                        if (data) {
                            var date = new Date(data);
                            var month = pad2(date.getMonth() + 1);
                            var day = pad2(date.getDate());
                            var year = date.getFullYear();
                            return day + "/" + month + "/" + year;
                        }
                        return '';
                    }
                },
                {"data": "province_name"},
                {"data": "school_name"},
                {"data": "year_graduated"},
                {"data": "id_number"},
                {
                    "data": "issue_date",
                    "render": function (data, type, row) {
                        if (data) {
                            var date = new Date(data);
                            var month = pad2(date.getMonth() + 1);
                            var day = pad2(date.getDate());
                            var year = date.getFullYear();
                            return day + "/" + month + "/" + year;
                        }
                        return '';
                    }
                },
                {"data": "issue_province_name"},
                {"data": "contact_name"},
                {"data": "contact_address"},
                {"data": "phone1"},
                {"data": "phone2"},
                {"data": "email"},
                {"data": "exam_place_name"},
                {"data": "major_no_exam_name"},
                {"data": "major_exam_name"},
                {"data": "subject_group_name"},
                {"data": "s1_t2_1"},
                {"data": "s1_t1"},
                {"data": "s1_t2_2"},
                {"data": "s2_t2_1"},
                {"data": "s2_t1"},
                {"data": "s2_t2_2"},
                {"data": "s3_t2_1"},
                {"data": "s3_t1"},
                {"data": "s3_t2_2"},
                {
                    "data": "status",
                    "render": function (data, type, row) {
                        if (data)
                            return 'Đủ';
                        else
                            return 'Thiếu';
                    }
                },
                {"data": "notes"},
                {"data": "campus_name"},
                {"data": "tele_username"},
                {"data": "submission_channel_name"},
                {"data": "creator"},
            ]
        });
    });
</script>
<form class="form-inline search-form" id="search_form">
    {{ csrf_field() }}
    <div class="col-md-4">
        <div class="form-group">
            <label for="exampleInputName2">Mã hồ sơ</label>
            <input type="text" class="form-control" id="app_code" name="app_code" value="{{ old('app_code') }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="exampleInputEmail2">SĐT</label>
            <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="exampleInputEmail2">Email</label>
            <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="exampleInputEmail2">Trường</label>
            <input type="text" class="form-control" id="school" name="school" value="{{ old('school') }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="exampleInputEmail2">Tỉnh</label>
            {{ Form::select('province_id', $province_list, null, ['id' => 'province', 'multiple' => true]) }}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="exampleInputEmail2">Phòng</label>
            {{ Form::select('department_id', $department_list, null, ['id' => 'department', 'placeholder' => 'Chọn phòng']) }}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="exampleInputEmail2">Khu vực</label>
            {{ Form::select('area', $area_list, null, ['id' => 'area', 'multiple' => true]) }}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="start_date">Ngày tạo (từ ngày)</label>
            <input type="text" class="form-control" id="start_date" name="start_date" value="{{ old('start_date') }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="end_date">Ngày tạo (đến ngày)</label>
            <input type="text" class="form-control" id="end_date" name="end_date" value="{{ old('end_date') }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="active_from">Active (từ ngày)</label>
            <input type="text" class="form-control" id="active_from" name="active_from"
                   value="{{ old('active_from') }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="active_to">Active (đến ngày)</label>
            <input type="text" class="form-control" id="active_to" name="active_to" value="{{ old('active_to') }}">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="import_count">Số lần nhập</label>
            {{ Form::select('import_count_operator', ['ge' => '>=', 'gt' => '>', 'lt' => '<', 'le' => '<='], old('import_count_operator'), ['id' => 'import_count_operator']) }}
            <input type="text" class="form-control" id="import_count" name="import_count"
                   value="{{ old('import_count') }}" size="2">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="assignee">Người được chia</label>
            {{ Form::select('assignee', $assignee_list, old('assignee'), ['id' => 'assignee', 'multiple' => true]) }}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="assignee">Người active</label>
            {{ Form::select('activator', $activator_list, old('activator'), ['id' => 'activator', 'multiple' => true]) }}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="assignee">Người tạo</label>
            {{ Form::select('creator', $creator_list, old('creator'), ['id' => 'creator', 'multiple' => true]) }}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="notes">Ghi chú</label>
            <textarea class="form-control" id="notes" name="notes" cols="21" rows="4">{{ old('notes') }}</textarea>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <button type="submit" class="btn btn-default" id="search_button"><i class="fa fa-filter"
                                                                                aria-hidden="true"></i> Tìm kiếm
            </button>
            <button type="reset" class="btn btn-default"><i class="fa fa-eraser" aria-hidden="true"></i> Xóa lọc
            </button>
        </div>
    </div>
</form>
<div class="col-md-4 pull-right utils">
    <div class="buttons">
        <button type="button" class="btn btn-default" id="hide_search_form"><i class="fa fa-eye-slash"
                                                                               aria-hidden="true"></i> Ẩn bộ lọc
        </button>
        <!--<a href="#" class="btn btn-default"><i class="fa fa-user-plus" aria-hidden="true"></i> Phân bổ</a>
        <a href="/lead/create" class="btn btn-default"><i class="fa fa-plus" aria-hidden="true"></i> Tạo mới</a>-->
    </div>
</div>
<div class="col-md-12">
    <table id="main_grid" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
        <thead>

        <tr>
            <th>ID</th>
            <th>Mã hồ sơ</th>
            <th>Đợt thi</th>
            <th>Ngày tư vấn</th>
            <th>Loại hồ sơ</th>
            <th>Hình thức xét học bạ</th>
            <th>Hình thức thi học bổng</th>
            <th>Họ và tên</th>
            <th>Giới tính</th>
            <th>Ngày sinh</th>
            <th>Tỉnh</th>
            <th>Trường</th>
            <th>Năm tốt nghiệp</th>
            <th>CMND</th>
            <th>Ngày cấp</th>
            <th>Nơi cấp</th>
            <th>Người liên hệ</th>
            <th>Địa chỉ liên lạc</th>
            <th>ĐTDĐ</th>
            <th>ĐTNR</th>
            <th>Email</th>
            <th>Địa điểm ĐKDT</th>
            <th>Ngành ĐKDT</th>
            <th>Ngành XT</th>
            <th>Tổ hợp XT</th>
            <th>HB/M1<br/>Kỳ 2</th>
            <th>HB/M1<br/>Kỳ 1</th>
            <th>HB/M1<br/>Kỳ 2</th>
            <th>HB/M2<br/>Kỳ 2</th>
            <th>HB/M2<br/>Kỳ 1</th>
            <th>HB/M2<br/>Kỳ 2</th>
            <th>HB/M3<br/>Kỳ 2</th>
            <th>HB/M3<br/>Kỳ 1</th>
            <th>HB/M3<br/>Kỳ 2</th>
            <th>TTHS</th>
            <th>Ghi chú</th>
            <th>Cơ sở</th>
            <th>Tele</th>
            <th>Nhận HS</th>
            <th>Counsellor</th>
        </tr>
        </thead>
    </table>
</div>
<script src="js/bootstrap.min.js"></script>
<script src="js/filter.js"></script>
<script>
    $(function () {
        $('#search_form').remove();
    });

</script>
</body>
</html>