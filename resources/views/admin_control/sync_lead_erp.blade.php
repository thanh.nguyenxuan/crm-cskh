@extends('layouts.main')

@section('content')

{!! Form::model($model, [
   'method'     => 'POST',
   'route'      => 'adminControl.syncLeadERP',
   'enctype'    => 'multipart/form-data'
]) !!}

<div class="form-group">
    {{ Form::file('filepath') }}
</div>

{{ Form::submit('Sync', ['class' => 'btn btn-primary']) }}

{!! Form::close() !!}

@stop