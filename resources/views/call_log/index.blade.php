@extends('layouts.main')

@section('content')

<div class="x_panel">
    <div class="x_title">
        <h2>Lịch sử chăm sóc - Call Log</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('calls.index') }}" method="get" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="control-label col-md-2">Từ ngày: </label>
                        <div class="col-md-2">
                            <input type="text" name="from_date" class="form-control" id="from_date"
                                   value="{{ $from_date?date('d/m/Y', strtotime($from_date)):date('d/m/Y') }}">
                        </div>
                        <label class="control-label col-md-2">Đến ngày: </label>
                        <div class="col-md-2">
                            <input type="text" name="to_date" class="form-control" id="to_date"
                                   value="{{ $to_date?date('d/m/Y', strtotime($to_date)):date('d/m/Y') }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Chiều cuộc gọi: </label>
                        <div class="col-md-2">
                            {{ Form::select('direction', array(1 => 'Gọi đi', 2 => 'Gọi đến'), old('direction')?old('direction'):'', ['placeholder'=> 'Chọn chiều cuộc gọi', 'class' => 'form-control']) }}
                        </div>
                        <label class="control-label col-md-2">Người tạo: </label>
                        <div class="col-md-2">
                            {{ Form::select('created_by', $tele_list, old('created_by')?old('created_by'):'', ['placeholder'=> 'Chọn người tạo', 'class' => 'form-control']) }}
                        </div>
                        <label class="control-label col-md-1">Trạng thái: </label>
                        <div class="col-md-2">
                            {{ Form::select('status', array(1 => 'Có nghe máy', 2 => 'Không nghe máy', 3 => 'Thuê bao', 4 => 'Sai số'), old('status')?old('status'):'', ['placeholder'=> 'Chọn trạng thái cuộc gọi', 'class' => 'form-control']) }}
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-edufit-default">Xem</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 table-responsive">
                <table class="table table-condensed table-bordered">
                    <thead>
                    <th class="text-center">STT</th>
                    <th class="text-center">Lead</th>
                    <?php if(Auth::user()->hasRole('carer')){ ?>
                    <th class="text-center">Học sinh</th>
                    <th class="text-center">Mã HS</th>
                    <th class="text-center">Lớp</th>
                    <?php } ?>
                    <th class="text-center">Số điện thoại</th>
                    <th class="text-center">Chiều cuộc gọi</th>
                    <th class="text-center">Trạng thái</th>
                    <th class="text-center" style="width: 30%">Nội dung chăm sóc</th>
                    <th class="text-center">Người chăm sóc</th>
                    <th class="text-center">Thời điểm chăm sóc</th>
                    </thead>
                    <tbody>
                    @php $stt = 0 @endphp
                    @foreach($call_history_list as $call)
                        @php
                            $lead = \App\Lead::withTrashed()->find($call->lead_id);
                        @endphp
                        <tr>
                            <td class="text-center">{{ ++$stt }}</td>
                            <td>
                                <a target="_blank" href="{{ route('lead.edit', $call->lead_id) }}">
                                    {{ $lead->name ? $lead->name : ($lead->student_name ? $lead->student_name : $lead->nick_name) }}
                                </a>
                            </td>
                            <?php if(Auth::user()->hasRole('carer')){ ?>
                            <td class="text-center">{{ $lead->student_name }}</td>
                            <td class="text-center">{{ $lead->student_code }}</td>
                            <td class="text-center">{{ $lead->class_name }}</td>
                            <?php } ?>
                            <td class="text-center">{{ $lead->phone1 ? $lead->phone1 : $lead->phone2 }}</td>
                            <td class="text-center">{{ $call->direction==1?'Gọi đi':'Gọi đến' }}</td>
                            <td class="text-center">{{ $call->status?$call_status_list[$call->status]:'' }}</td>
                            <td>{{ $call->notes }}</td>
                            <td class="text-center">{{ (!empty($call->created_by) && App\User::find($call->created_by))?App\User::find($call->created_by)->username:''}}</td>
                            <td class="text-center">{{ date('d/m/Y H:i:s', strtotime($call->created_at)) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<script>
    $(function () {
        var full_month_names = new Array("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");
        $.datepicker.regional["vi-VN"] =
            {
                closeText: "Đóng",
                prevText: "Trước",
                nextText: "Sau",
                currentText: "Hôm nay",
                monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
                monthNamesShort: full_month_names,
                dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
                dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
                dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
                weekHeader: "Tuần",
                dateFormat: "dd/mm/yy",
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: "",
                changeMonth: true,
            };
        $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
        $("#from_date").datepicker();
        $("#to_date").datepicker();
    });
</script>

@stop