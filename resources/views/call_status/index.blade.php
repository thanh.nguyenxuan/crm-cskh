@extends('layouts.main')

@section('content')

<div class="x_panel">
    <div class="x_title">
        <h2>Quản lý trạng thái chăm sóc</h2>
        <div class="pull-right">
            <a class="btn btn-default" href="{{ route('call_status.create') }}">Thêm trạng thái chăm sóc mới</a><br/>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <table class="table table-hover">
            <thead>
            <th></th>
            <th></th>
            <th>Tên trạng thái chăm sóc</th>
            </thead>
            <tbody>
            @foreach($call_status_list as $call_status)
                <tr>
                    <td><a href="{{ route('call_status.edit', $call_status->id) }}" title="Chỉnh sửa"><i
                                    class="fa fa-lg fa-pencil"
                                    aria-hidden="true"></i></a>
                    </td>
                    <td><a href="{{ route('call_status.toggle', $call_status->id) }}" title="{!! $call_status->hide ?'Hiện' : 'Ẩn' !!}"><i
                                    class="fa fa-lg {!! $call_status->hide ?'fa-eye' : 'fa-eye-slash' !!}"
                                    aria-hidden="true"></i></a></td>
                    <td>{{ $call_status->name }}</td>
                    <td>{!! $call_status->hide ?'<span class="class-status active">Ẩn</span>':'<span class="class-status inactive">Hiện</span>' !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>

@stop