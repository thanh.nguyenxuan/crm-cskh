@include('includes.header')
<div class="col-md-12">
    <h4>Quản lý cơ sở</h4>
    <a class="btn btn-default" href="{{ route('campus.create') }}">Thêm cơ sở mới</a><br/>
    <table class="table table-hover">
        <thead>
        <th></th>
        <th></th>
        <th>Mã cơ sở</th>
        <th>Tên cơ sở</th>
        </thead>
        <tbody>
        @foreach($campus_list as $campus)
            <tr>
                <td><a href="{{ route('campus.edit', $campus->id) }}" title="Chỉnh sửa"><i
                                class="fa fa-lg fa-pencil"
                                aria-hidden="true"></i></a>
                </td>
                <td><a href="{{ route('campus.toggle', $campus->id) }}" title="{!! $campus->hide ?'Hiện' : 'Ẩn' !!}"><i
                                class="fa fa-lg {!! $campus->hide ?'fa-eye' : 'fa-eye-slash' !!}"
                                aria-hidden="true"></i></a></td>
                <td>{{ $campus->code }}</td>
                <td>{{ $campus->name }}</td>
                <td>{!! $campus->hide ?'<span class="class-status active">Ẩn</span>':'<span class="class-status inactive">Hiện</span>' !!}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</body>
</html>