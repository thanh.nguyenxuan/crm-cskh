@extends('layouts.main')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Quản lý kênh quảng cáo</h2>
        <div class="pull-right">
            <a class="btn btn-default" href="{{ route('channel.create') }}">Thêm kênh QC mới</a><br/>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <table class="table table-hover">
            <thead>
            <th></th>
            <th></th>
            <th>Tên kênh QC</th>
            </thead>
            <tbody>
            @foreach($channel_list as $channel)
                <tr>
                    <td><a href="{{ route('channel.edit', $channel->id) }}" title="Chỉnh sửa"><i
                                    class="fa fa-lg fa-pencil"
                                    aria-hidden="true"></i></a>
                    </td>
                    <td><a href="{{ route('channel.toggle', $channel->id) }}" title="{!! $channel->hide ?'Hiện' : 'Ẩn' !!}"><i
                                    class="fa fa-lg {!! $channel->hide ?'fa-eye' : 'fa-eye-slash' !!}"
                                    aria-hidden="true"></i></a></td>
                    <td>{{ $channel->name }}</td>
                    <td>{!! $channel->hide ?'<span class="class-status active">Ẩn</span>':'<span class="class-status inactive">Hiện</span>' !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop