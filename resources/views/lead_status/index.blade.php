@extends('layouts.main')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Quản lý trạng thái lead</h2>
        <div class="pull-right">
            <a class="btn btn-default" href="{{ route('lead_status.create') }}">Thêm trạng thái lead mới</a><br/>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <table class="table table-hover">
            <thead>
            <th></th>
            <th></th>
            <th>Tên trạng thái lead</th>
            </thead>
            <tbody>
            @foreach($lead_status_list as $lead_status)
                <tr>
                    <td><a href="{{ route('lead_status.edit', $lead_status->id) }}" title="Chỉnh sửa"><i
                                    class="fa fa-lg fa-pencil"
                                    aria-hidden="true"></i></a>
                    </td>
                    <td><a href="{{ route('lead_status.toggle', $lead_status->id) }}" title="{!! $lead_status->hide ?'Hiện' : 'Ẩn' !!}"><i
                                    class="fa fa-lg {!! $lead_status->hide ?'fa-eye' : 'fa-eye-slash' !!}"
                                    aria-hidden="true"></i></a></td>
                    <td>{{ $lead_status->name }}</td>
                    <td>{!! $lead_status->hide ?'<span class="class-status active">Ẩn</span>':'<span class="class-status inactive">Hiện</span>' !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>



</div>
@stop