@extends('layouts.main')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Cập nhật nhóm</div>
                    <div class="panel-body">
                        @if(!empty($message))
                            <div class="alert {!! $success ? 'alert-success' : 'alert-danger' !!}" role="alert">
                                {!! $message !!}
                            </div>
                        @endif
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('rGroup.update', $model->id) }}">
                            {!! method_field('patch') !!}
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label for="name" class="col-md-4 control-label required">Tên nhóm</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ !empty(old('name')) ? old('name') : $model->name }}" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="description" class="col-md-4 control-label required">Cơ sở</label>

                                <div class="col-md-12">
                                    <br/>
                                    <div class="col-md-3" style="padding-left: 30px">
                                        <?php foreach (\App\Department::listData() as $key => $value){ if(strpos($value,'SMIS') !== FALSE){?>
                                        <div>
                                            <input name="departments[]" type="checkbox" value="{{ $key }}" {{ (!empty(old('departments') && in_array($key, old('departments')))) ? 'checked' : (in_array($key, $selected_departments) ? 'checked' : '') }} /> {{ $value }}
                                        </div>
                                        <?php }} ?>
                                    </div>

                                    <div class="col-md-3" style="padding-left: 30px">
                                        <?php foreach (\App\Department::listData() as $key => $value){ if(strpos($value,'IMS') !== FALSE){?>
                                        <div>
                                            <input name="departments[]" type="checkbox" value="{{ $key }}" {{ (!empty(old('departments') && in_array($key, old('departments')))) ? 'checked' : (in_array($key, $selected_departments) ? 'checked' : '') }} /> {{ $value }}
                                        </div>
                                        <?php }} ?>
                                    </div>

                                    <div class="col-md-3" style="padding-left: 30px">
                                        <?php foreach (\App\Department::listData() as $key => $value){ if(strpos($value,'GIS') !== FALSE){?>
                                        <div>
                                            <input name="departments[]" type="checkbox" value="{{ $key }}" {{ (!empty(old('departments') && in_array($key, old('departments')))) ? 'checked' : (in_array($key, $selected_departments) ? 'checked' : '') }} /> {{ $value }}
                                        </div>
                                        <?php }} ?>
                                    </div>

                                    <div class="col-md-3" style="padding-left: 30px">
                                        <?php foreach (\App\Department::listData() as $key => $value){ if(strpos($value,'ASC') !== FALSE){?>
                                        <div>
                                            <input name="departments[]" type="checkbox" value="{{ $key }}" {{ (!empty(old('departments') && in_array($key, old('departments')))) ? 'checked' : (in_array($key, $selected_departments) ? 'checked' : '') }} /> {{ $value }}
                                        </div>
                                        <?php }} ?>
                                    </div>

                                </div>

                            </div>


                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">Cập nhật</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop