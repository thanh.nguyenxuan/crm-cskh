@extends('layouts.main')

@section('content')

    <div class="x_panel">
        <div class="x_title">
            <h2>Quản lý Nhóm</h2>
            <div class="pull-right">
                <a class="btn btn-default" href="{{ route('rGroup.create') }}">Thêm nhóm mới</a><br/>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="x_content">
            <table class="table table-hover">
                <thead>
                <th></th>
                <th></th>
                <th>Tên nhóm</th>
                <th>Cơ sở</th>
                </thead>
                <tbody>
                @foreach($model->search() as $item)
                    <tr>
                        <td>
                            <a href="{{ route('rGroup.edit', $item->id) }}" title="Chỉnh sửa">
                                <i class="fa fa-lg fa-pencil" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td>
                            <a href="{{ route('rGroup.toggle', $item->id) }}" title="{!! ($item->status == 0) ? 'Hiện' : 'Ẩn' !!}">
                                <i class="fa fa-lg {!! ($item->status == 0) ? 'fa-eye' : 'fa-eye-slash' !!}" aria-hidden="true"></i>
                            </a>
                        </td>
                        <td>{{ $item->name }}</td>
                        <td>

                        </td>
                        <td>{!! !empty($item->status == 0) ?'<span class="class-status active">Ẩn</span>':'<span class="class-status inactive">Hiện</span>' !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>

    </div>
@stop