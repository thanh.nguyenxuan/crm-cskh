@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Thêm promotion mới</div>
                <div class="panel-body">
                    @if(!empty($message))
                        <div class="alert alert-danger" role="alert">{!! $message !!}</div>
                    @endif
                    @if(!empty($success))
                        <div class="alert alert-success" role="alert">Tạo promotion mới thành công!</div>
                    @endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('promotion.store') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                            <label for="class_name" class="col-md-4 control-label required">Mã promotion</label>

                            <div class="col-md-6">
                                <input id="code" type="text" class="form-control" name="code" value="{{ old('code') }}"
                                       required autofocus>

                                @if ($errors->has('code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('code') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="class_name" class="col-md-4 control-label required">Tên promotion</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"
                                       required autofocus>

                                @if ($errors->has('class_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('class_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('percentage') ? ' has-error' : '' }}">
                            <label for="class_name" class="col-md-4 control-label required">Số phần trăm giảm
                                trừ</label>

                            <div class="col-md-6">
                                <input id="percentage" type="number" min="0" max="100" class="form-control"
                                       name="percentage" value="{{ old('percentage') }}" required autofocus
                                       style="display:inline-block; width: 60px;"><strong>%</strong>

                                @if ($errors->has('percentage'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('percentage') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Khởi tạo
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop