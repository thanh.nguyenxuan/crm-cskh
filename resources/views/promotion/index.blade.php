@extends('layouts.main')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Quản lý promotion</h2>
        <div class="pull-right">
            <a class="btn btn-default" href="{{ route('promotion.create') }}">Thêm promotion mới</a><br/>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <table class="table table-hover">
            <thead>
            <th></th>
            <th></th>
            <th>Mã promotion</th>
            <th>Tên promotion</th>
            <th>Phần trăm giảm trừ</th>
            <th>Ẩn/Hiện</th>
            </thead>
            <tbody>
            @foreach($promotion_list as $promotion)
                <tr>
                    <td><a href="{{ route('promotion.edit', $promotion->id) }}" title="Chỉnh sửa"><i
                                    class="fa fa-lg fa-pencil"
                                    aria-hidden="true"></i></a>
                    </td>
                    <td><a href="{{ route('promotion.toggle', $promotion->id) }}" title="{!! $promotion->hide ?'Hiện' : 'Ẩn' !!}"><i
                                    class="fa fa-lg {!! $promotion->hide ?'fa-eye' : 'fa-eye-slash' !!}"
                                    aria-hidden="true"></i></a></td>
                    <td>{{ $promotion->code }}</td>
                    <td>{{ $promotion->name }}</td>
                    <td>{{ $promotion->percentage }}%</td>
                    <td>{!! $promotion->hide ?'<span class="class-status active">Ẩn</span>':'<span class="class-status inactive">Hiện</span>' !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

</div>
@stop