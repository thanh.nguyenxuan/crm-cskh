@extends('layouts.main')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Quản lý lớp học quan tâm</h2>
        <div class="pull-right">
            <a class="btn btn-default" href="{{ route('class.create') }}">Thêm lớp mới</a><br/>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <table class="table table-hover">
            <thead>
            <th></th>
            <th></th>
            <th>Tên lớp</th>
            </thead>
            <tbody>
            @foreach($class_list as $class)
                <tr>
                    <td><a href="{{ route('class.edit', $class->id) }}" title="Chỉnh sửa"><i
                                    class="fa fa-lg fa-pencil"
                                    aria-hidden="true"></i></a>
                    </td>
                    <td><a href="{{ route('class.toggle', $class->id) }}" title="{!! $class->hide ?'Hiện' : 'Ẩn' !!}"><i
                                    class="fa fa-lg {!! $class->hide ?'fa-eye' : 'fa-eye-slash' !!}"
                                    aria-hidden="true"></i></a></td>
                    <td>{{ $class->name }}</td>
                    <td>{!! $class->hide ?'<span class="class-status active">Ẩn</span>':'<span class="class-status inactive">Hiện</span>' !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop