@extends('layouts.main')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Quản lý nguồn dẫn</h2>
        <div class="pull-right">
            <a class="btn btn-default" href="{{ route('source.create') }}">Thêm nguồn dẫn mới</a><br/>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <table class="table table-hover">
            <thead>
            <th></th>
            <th></th>
            <th>Tên nguồn dẫn</th>
            </thead>
            <tbody>
            @foreach($source_list as $source)
                <tr>
                    <td><a href="{{ route('source.edit', $source->id) }}" title="Chỉnh sửa"><i
                                    class="fa fa-lg fa-pencil"
                                    aria-hidden="true"></i></a>
                    </td>
                    <td><a href="{{ route('source.toggle', $source->id) }}" title="{!! $source->hide ?'Hiện' : 'Ẩn' !!}"><i
                                    class="fa fa-lg {!! $source->hide ?'fa-eye' : 'fa-eye-slash' !!}"
                                    aria-hidden="true"></i></a></td>
                    <td>{{ $source->name }}</td>
                    <td>{!! $source->hide ?'<span class="class-status active">Ẩn</span>':'<span class="class-status inactive">Hiện</span>' !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop