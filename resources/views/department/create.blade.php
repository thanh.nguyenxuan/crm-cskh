@extends('layouts.main')

@section('content')

    <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Thêm cơ sở mới</div>
                <div class="panel-body">
                    @if(!empty($message))
                        <div class="alert {!! $success == 1?'alert-success':'alert-danger' !!}"
                             role="alert">{!! $message !!}</div>
                    @endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('department.store') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('code') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label required">Mã cơ sở</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"
                                       required autofocus>

                                @if ($errors->has('code'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label required">Tên cơ sở</label>

                            <div class="col-md-6">
                                <input id="description" type="text" class="form-control" name="description"
                                       value="{{ old('description') }}" required autofocus>

                                @if ($errors->has('class_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('class_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label required">Tỉnh/thành phố</label>

                            <div class="col-md-6">
                                {{ Form::select('province', $province_list, old('province'), ['id' => 'province', 'multiple' => true, 'class' => 'form-control', 'required' => 'required']) }}

                                @if ($errors->has('province'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('province') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Đăng ký
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop