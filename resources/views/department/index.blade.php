@extends('layouts.main')

@section('content')

<div class="x_panel">
    <div class="x_title">
        <h2>Quản lý cơ sở</h2>
        <div class="pull-right">
            <a class="btn btn-default" href="{{ route('department.create') }}">Thêm cơ sở mới</a><br/>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <table class="table table-hover">
            <thead>
            <th></th>
            <th></th>
            <th>Mã cơ sở</th>
            <th>Tên cơ sở</th>
            </thead>
            <tbody>
            @foreach($department_list as $department)
                <tr>
                    <td><a href="{{ route('department.edit', $department->id) }}" title="Chỉnh sửa"><i
                                    class="fa fa-lg fa-pencil"
                                    aria-hidden="true"></i></a>
                    </td>
                    <td><a href="{{ route('department.toggle', $department->id) }}"
                           title="{!! $department->hide ?'Hiện' : 'Ẩn' !!}"><i
                                    class="fa fa-lg {!! $department->hide ?'fa-eye' : 'fa-eye-slash' !!}"
                                    aria-hidden="true"></i></a></td>
                    <td>{{ $department->name }}</td>
                    <td>{{ $department->description }}</td>
                    <td>{{ !empty($department->province)?$province_list[$department->province]:'' }}</td>
                    <td>{!! !empty($department->hide) ?'<span class="class-status active">Ẩn</span>':'<span class="class-status inactive">Hiện</span>' !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

</div>
@stop