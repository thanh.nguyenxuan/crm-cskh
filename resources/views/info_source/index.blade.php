@extends('layouts.main')

@section('content')

<div class="x_panel">
    <div class="x_title">
        <h2>Quản lý nguồn thông tin</h2>
        <div class="pull-right">
            <a class="btn btn-default" href="{{ route('info_source.create') }}">Thêm nguồn thông tin mới</a><br/>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <table class="table table-hover">
            <thead>
            <th></th>
            <th></th>
            <th>Tên nguồn thông tin</th>
            </thead>
            <tbody>
            @foreach($info_source_list as $info_source)
                <tr>
                    <td><a href="{{ route('info_source.edit', $info_source->id) }}" title="Chỉnh sửa"><i
                                    class="fa fa-lg fa-pencil"
                                    aria-hidden="true"></i></a>
                    </td>
                    <td><a href="{{ route('info_source.toggle', $info_source->id) }}" title="{!! $info_source->hide ?'Hiện' : 'Ẩn' !!}"><i
                                    class="fa fa-lg {!! $info_source->hide ?'fa-eye' : 'fa-eye-slash' !!}"
                                    aria-hidden="true"></i></a></td>
                    <td>{{ $info_source->name }}</td>
                    <td>{!! $info_source->hide ?'<span class="class-status active">Ẩn</span>':'<span class="class-status inactive">Hiện</span>' !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

</div>
@stop