@include('includes.header')
<div class="col-md-12">
<h4>Quản lý nhóm</h4>
<a class="btn btn-default" href="{{ route('group.create') }}">Tạo nhóm mới</a><br />
<table class="table table-hover">
	<thead>
		<th></th>
		<th>Tên nhóm</th>
		<th>Trưởng nhóm</th>
		<th>Loại nhóm</th>
		<th>Phòng</th>
		<th>Thành viên</th>
	</thead>
	<tbody>
		@foreach($group_list as $group)
			<tr>
				<td><a href="{{ route('group.edit', $group->id) }}"><i class="fa fa-lg fa-pencil" aria-hidden="true"></i></a></td>
				<td>{{ $group->name }}</td>
				<td>{{ $group->team_leader }}</td>
				<td>{{ $group->type }}</td>
				<td>{{ $group->department }}</td>
				<td>
					@foreach($group->members as $member)
					{{ $member->username }} - {{ $member->full_name }}<br />
					@endforeach
				</td>
			</tr>
		@endforeach
	</tbody>
</table>
</div>
</body>
</html>