@include('includes.header')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Chỉnh sửa nhóm</div>
                <div class="panel-body">
				@if(!empty($message))
					<div class="alert alert-danger" role="alert">{{ $message }}</div>
				@endif
				@if(!empty($success))
					<div class="alert alert-success" role="alert">Lưu dữ liệu thành công!</div>
				@endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('group.update', $group->id) }}">
                        {{ csrf_field() }}
						{!! method_field('patch') !!}

						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Tên nhóm</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ $group->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Loại nhóm</label>

                            <div class="col-md-6">
                                {{ Form::select('type', $group_type_list, $group->type) }}
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Trưởng nhóm</label>

                            <div class="col-md-6">
							{{ Form::select('team_leader', $user_list, $group->team_leader, ['id' => 'teamleader', 'placeholder' => 'Chưa có']) }}
                            </div>
                        </div>
						
						<div class="form-group">
                            <label for="department" class="col-md-4 control-label">Phòng</label>

                            <div class="col-md-6">
                                {{
								Form::select('department', $department_list, $group->department, ['id' => 'department', 'placeholder' => 'Chọn phòng', 'required' => 'required']) }}
                            </div>
                        </div>
						
						<div class="form-group">
                            <label for="department" class="col-md-4 control-label">Thành viên</label>

                            <div class="col-md-6">
                                {{	Form::select('member_list[]', $user_list, $group->members, ['id' => 'member_list', 'multiple' => true, 'size' => 6]) }}
                            </div>
                        </div>
						
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Cập nhật
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
