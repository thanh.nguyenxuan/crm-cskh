@include('includes.header')
<div class="col-md-12">
    <h4>Quản lý chi phí marketing</h4>
    <div class="alert alert-info">
        Dữ liệu chi phí được cập nhật lần cuối lúc <strong>{{ date('d/m/Y H:i:s', strtotime($last_updated)) }}</strong>
    </div>
    @if(!empty($success))
        <div class="alert alert-success">
            Chi phí marketing đã được cập nhật!
        </div>
    @endif
    <form method="post">
        {{ csrf_field() }}
        <button type="submit" class="btn btn-primary">Cập nhật</button>
        <table class="table table-hover">
            <thead>
            <th>STT</th>
            <th>Tên từ khóa</th>
            <th>Số lượng lead</th>
            <th>Số lượng hồ sơ</th>
            <th>Tổng chi phí</th>
            <th>Đơn giá lead</th>
            <th>Người phụ trách</th>
            <th>Cập nhật lần cuối bởi</th>
            </thead>
            <tbody>
            @php $stt = 1; @endphp
            @foreach($keyword_list as $keyword)
                <tr>
                    <td>{{ $stt++ }}</td>
                    <td>{{ $keyword->name }}</td>
                    <td>{{ $keyword->lead_count }}</td>
                    <td>{{ $keyword->account_count }}</td>
                    <td><input type="text" value="{{ $keyword->marketing_cost }}"
                               name="marketing_cost[{{$keyword->id}}]">
                    </td>
                    <td>@php echo number_format($keyword->cost_per_lead, 0); @endphp</td>
                    <td>{{ $keyword->person_in_charge }}</td>
                    <td>
                        @if(!empty($keyword->updater))
                            {{ $keyword->updater }} vào lúc {{ date('d/m/Y H:i:s', strtotime($keyword->updated_at)) }}
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <button type="submit" class="btn btn-primary">Cập nhật</button>
    </form>
</div>
</body>
</html>