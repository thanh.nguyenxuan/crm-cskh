Bạn vừa nhận được tin nhắn SMS vào lúc
<strong>{{ Carbon\Carbon::parse($sms->created_at)->format('d/m/Y H:i:s') }}</strong> từ
<strong>{!! $sms->lead_name?' lead <a href="http://crmdh.fpt.edu.vn/lead/edit/' . $sms->lead_id . '">' . $sms->lead_name  . '</a>': '' !!}</strong> số điện thoại
<strong>{{ $sms->to }}</strong> với nội dung:<br/>
{{ $sms->content }}<br/>
