@extends('layouts.main')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Danh sách tin nhắn SMS</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="col-md-12">
            <form action="{{ route('cdr.index') }}" method="get">
                {{ csrf_field() }}
                <label>Từ ngày: </label><input type="text" name="from_date" id="from_date"
                                               value="{{ $from_date?$from_date:'' }}">
                <label>Đến ngày: </label><input type="text" name="to_date" id="to_date"
                                                value="{{ $to_date?$to_date:'' }}">
                <button type="submit">Xem</button>
            </form>
        </div>
        <table class="table table-hover">
            <thead>
            <th>STT</th>
            <th>Thời điểm gửi/nhận</th>
            <th>Gửi/nhận</th>
            <th>Lead</th>
            <th>SĐT</th>
            <th>Nội dung</th>
            <th>Người gửi</th>
            </thead>
            <tbody>
            @php $stt = 0; @endphp
            @foreach($data as $sms)
                <tr>
                    <td>{{ ++$stt }}</td>
                    <td>{{ Carbon\Carbon::parse($sms->created_at)->format('d/m/Y H:i:s') }}</td>
                    <td>
                        @if($sms->type == 1)
                            Gửi đi
                        @elseif($sms->type == 2)
                            Nhận về
                        @endif
                    </td>
                    <td>
                        @if(!empty($sms->lead_id))
                            <a href="{{ env('APP_URL') }}/lead/{{ $sms->lead_id }}/edit"
                               target="_blank">{{ $sms->lead_name }}</a>
                        @endif
                    </td>
                    <td>
                        {{ $sms->to }}
                    </td>
                    <td>
                        {{ $sms->content }}
                    </td>
                    <td>
                        {{ !empty($sms->sender)?$sms->sender:'' }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

</div>
<script>
    $(function () {
        var full_month_names = new Array("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");
        $.datepicker.regional["vi-VN"] =
            {
                closeText: "Đóng",
                prevText: "Trước",
                nextText: "Sau",
                currentText: "Hôm nay",
                monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
                monthNamesShort: full_month_names,
                dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
                dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
                dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
                weekHeader: "Tuần",
                dateFormat: "dd/mm/yy",
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: "",
                changeMonth: true,
            };
        $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
        $("#from_date").datepicker();
        $("#to_date").datepicker();
    });
</script>
@stop