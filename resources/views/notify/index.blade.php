@extends('layouts.main')

@section('content')

<div class="x_panel">
    <div class="x_title">
        <h2>Thông báo</h2>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <div class="row">
            <div class="col-sm-12">
                <form action="{{ route('notify.index') }}" method="get" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-2">Từ ngày: </label>
                        <div class="col-md-2">
                            <input autocomplete="off" type="text" name="from_date" id="from_date" value="{{ old('from_date')?old('from_date'):'' }}" class="form-control"/>
                        </div>
                        <label class="control-label col-md-2">Đến ngày: </label>
                        <div class="col-md-2">
                            <input autocomplete="off" type="text" name="to_date" id="to_date" value="{{ old('to_date')?old('to_date'):'' }}" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2">Trạng thái: </label>
                        <div class="col-md-2">
                            <select id="status" name="status" class="form-control">
                                {!! \App\Notify::getOptionsStatus(old('status')) !!}
                            </select>
                        </div>
                        <label class="control-label col-md-2">Từ khóa: </label>
                        <div class="col-md-2">
                            <input type="text" name="key" id="key" value="{{ old('key') }}" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2">Lead trùng lặp: </label>
                        <div class="col-md-2">
                            <select id="is_duplicate" name="is_duplicate" class="form-control">
                                {!! \App\Notify::getOptionsDuplicate(old('is_duplicate')) !!}
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-8 text-right">
                            <button type="submit" class="btn btn-edufit-default">Xem</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">

                <table class="table table-bordered table-responsive table-striped table-sticky-header">
                    <thead>
                    <tr>
                        <th class="text-center">Thời gian</th>
                        <th class="text-center">Nội dung</th>
                        <th class="text-center">Trạng thái</th>
                        <th class="text-center">Thông tin thêm</th>
                        <th class="text-center">Chi tiết</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if(count($data) == 0){
                        echo "<tr><td colspan='5'>Chưa có thông báo</td></tr>";
                    }else{
                    $statusListData = \App\Notify::getStatusListData();
                    foreach ($data as $item){ ?>
                    <tr>
                        <td class="text-center"><?php echo date("d/m/Y H:i",strtotime($item->created_at))?></td>
                        <td><?php echo $item->content?></td>
                        <td class="text-center"><?php echo $statusListData[$item->status]?></td>
                        <td class="text-center<?php if(!empty($item->lead_id)){echo !empty($item->lead_duplicate) ? ' bg-warning' : ' bg-success';}?>">
                            <?php if(!empty($item->lead_id)){
                                echo !empty($item->lead_duplicate) ? '<span class="text-warning">Trùng lặp</span>' : '<span class="text-success">Mới</span>';
                            }?>
                        </td>
                        <td class="text-center">
                            <?php if(!empty($item->lead_id)){ ?>
                            <a  onclick="view_notify_detail('<?php echo $item->id?>', '<?php echo !empty($item->lead_id) ? (env('APP_URL').'/lead/edit/'.$item->lead_id) : ''?>')" title="Chi tiết"><i class="fa fa-eye"></i></a>
                            <?php }?>
                        </td>
                    </tr>
                    <?php }
                    }
                    ?>
                    </tbody>
                </table>

                @include('layouts.pagination')

            </div>
        </div>
    </div>


</div>
<script>
    function view_notify_detail(notify_id, url)
    {
        markAsRead(notify_id);
        if(url){
            window.open(url, '_self');
        }
    }


    var full_month_names = new Array("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");
    $.datepicker.regional["vi-VN"] =
        {
            closeText: "Đóng",
            prevText: "Trước",
            nextText: "Sau",
            currentText: "Hôm nay",
            monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
            monthNamesShort: full_month_names,
            dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
            dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
            dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
            weekHeader: "Tuần",
            dateFormat: "dd/mm/yy",
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: "",
            changeMonth: true,
        };
    $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
    $('#from_date').datepicker();
    $('#to_date').datepicker();

</script>
@stop