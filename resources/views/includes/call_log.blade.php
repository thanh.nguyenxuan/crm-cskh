<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 27/04/2018
 * Time: 1:05 PM
 */
?>
<div class="tabbable">
    <ul class="nav nav-tabs" id="call_log_tabs">
        <li role="presentation" class="active">
            <a href="#call_history_pane" aria-controls="call_history_pane" role="tab" data-toggle="tab">
                Lịch sử Call Log
            </a>
        </li>
        <li role="presentation">
            <a href="#lead_care_history_pane" aria-controls="lead_care_history_pane" role="tab" data-toggle="tab">
                Lịch sử chăm sóc
            </a>
        </li>
        <li role="presentation">
            <a href="#log_status" aria-controls="log_status" role="tab" data-toggle="tab">
                Lịch sử thay đổi trạng thái
            </a>
        </li>
        <li role="presentation">
            <a href="#log_sms" aria-controls="log_sms" role="tab" data-toggle="tab">
                Lịch sử tin nhắn
            </a>
        </li>
        <li>
            <a href="#log_alarm" aria-controls="log_alarm" role="tab" data-toggle="tab">
                Lịch hẹn
            </a>
        </li>
    </ul>
    <div class="tab-content">
        @include('includes.call_history')
        @include('includes.lead_care_history')
        @include('includes.log_status')
        @include('includes.log_sms')
        @include('includes.log_alarm')
    </div>
</div>