<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="/css/select2.min.css" rel="stylesheet">
    <link href="/css/style.css?v=1.1.6" rel="stylesheet">
    <link href="/css/notifications.css?v=1.0.0" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript" charset="utf8" src="/js/moment.js"></script>
    <link rel="stylesheet" href="/css/jquery-ui.min.css">
    <link rel="stylesheet" href="/css/jquery-ui.custom.css">
    <script src="/js/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/select2.full.min.js"></script>
    <script type="text/javascript"
            src="/js/datatables.min.js"></script>
    <script src="//cdn.datatables.net/plug-ins/1.10.13/dataRender/datetime.js"></script>
    <script src="/js/dataTables.select.min.js"></script>
    <link rel="stylesheet" type="text/css"
          href="/css/datatables.min.css"/>
    <script src="/js/dataTables.fixedHeader.min.js"></script>
    <script src="/js/jquery.number.min.js"></script>
    <link rel="stylesheet" href="/css/fixedHeader.bootstrap.min.css">
    <link rel="stylesheet" href="/css/jquery-ui-timepicker-addon.min.css">
    <script src="/js/jquery-ui-timepicker-addon.min.js"></script>
    <script>
        var notification_counter = 0;
        var original_title = 'CRM';
        var title = original_title;

        function show_online_check_popup() {
            $("#myModal").modal('show');
            if (!$('#myModal').hasClass('in'))
                notification_counter++;
            if (notification_counter > 0)
                title_string = '(' + notification_counter + ') ' + title;
            else
                title_string = title;
            $(document).attr('title', title_string);
        }


        $(document).ready(function () {
            $('body').on('click', 'a.phonenumberx', function () {
                //alert($(this).data('phone'));
                //alert('Click');
                var phone = $(this).data('phone');
                var lead = $(this).data('lead');
                //window.location.replace('sip://' + phone);
                //window.location.replace('http://localhost/lead/edit/' + lead);
                //window.location = 'sip://' + phone;
                console.log(phone);
                window.location = 'http://localhost/lead/edit/' + lead;
                return false;
            });
            $('[data-toggle="popover"]').popover();
            //setInterval(show_online_check_popup, {{ env('ONLINE_CHECK_INTERVAL') }});
            $('#btn-onlinecheck').click(function () {
                notification_counter--;
                if (notification_counter > 0)
                    title_string = '(' + notification_counter + ') ' + title;
                else
                    title_string = title;
                $(document).attr('title', title_string);
                //window.location = "sip://0989663230";
                var url = '{{ env('APP_URL') }}/utils/setOnline/{{ Auth::user()->id }}';
                $.get(url, function (data, status) {
                    //console.log("Data: " + data + "\nStatus: " + status);
                });
                $("#myModal").modal('hide');
            });
        });

    </script>
    <title>CRM Edufit</title>
</head>
<body>
<nav class="navbar navbar-default">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/"><img src="/images/logo.png" class="img"/></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a href="{{ route('lead.index') }}" class="dropdown-toggle" data-toggle="dropdown" role="button"
                   aria-haspopup="true" aria-expanded="false">Lead</a>
                <ul class="dropdown-menu">
                    <li><a href="/lead">Danh sách lead</a></li>
                    <?php if (Auth::user()->hasRoleInList(['admin', 'holding', 'telesales', 'teleteamleader', 'marketer'])){ ?>
                    <li><a href="/lead/import">Import lead</a></li>
                    <li><a href="/lead/create">Tạo lead</a></li>
                    <?php } ?>
                </ul>
            </li>
            <li class="dropdown">
                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Tìm kiếm nhanh</a>
                <ul class="dropdown-menu">
                    <li><a href="{{ route('lead.index') }}?status_id[]={!! \App\LeadStatus::STATUS_NEW !!}">Lead mới</a></li>
                    <li><a href="{{ route('lead.index') }}?status_id[]={!! \App\LeadStatus::STATUS_HOT_LEAD !!}">Hot Lead</a></li>
                    <li><a href="{{ route('lead.index') }}?status_id[]={!! \App\LeadStatus::STATUS_SHOWUP_SCHOOL !!}">Show up</a></li>
                    <li><a href="{{ route('lead.index') }}?status_id[]={!! \App\LeadStatus::STATUS_WAITLIST !!}">Waiting List (đặt cọc)</a></li>
                    <li><a href="{{ route('lead.index') }}?status_id[]={!! \App\LeadStatus::STATUS_NEW_SALE !!}">New Sales (hoàn thiện hồ sơ, học phí)</a></li>
                    <li><a href="{{ route('lead.index') }}?status_id[]={!! \App\LeadStatus::STATUS_ENROLL !!}">New Enrollment (nhập học)</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="{{ route('lead.index') }}" class="dropdown-toggle" data-toggle="dropdown" role="button"
                   aria-haspopup="true" aria-expanded="false">Data</a>
                <ul class="dropdown-menu">
                    <li><a href="/suspect">Danh sách data</a></li>
                    <li><a href="/suspect/import">Import data</a></li>
                    <li><a href="/suspect/create">Tạo data</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                   aria-expanded="false">Quản lý</a>
                <ul class="dropdown-menu">
                    @if(Auth::user()->hasRole('admin'))
                    <li><a href="/user">Người dùng</a></li>
                    <li><a href="/import">Lịch sử import lead</a></li>
                    <li><a href="/importSuspectHistory">Lịch sử import data</a></li>
                    <li><a href="/promotion">Promotion</a></li>
                    <li><a href="/source">Nguồn dẫn</a></li>
                    <li><a href="/info_source">Nguồn thông tin</a></li>
                    @endif

                    @if(Auth::user()->hasRoleInList(['admin', 'holding', 'marketer']))
                    <li><a href="/channel">Kênh quảng cáo</a></li>
                    <li><a href="/campaign">Campaign</a></li>
                    @endif

                    @if(Auth::user()->hasRole('holding'))
                    <li><a href="/department">Cơ sở</a></li>
                    <li><a href="/lead_status">Trạng thái HS</a></li>
                    <li><a href="/call_status">Trạng thái chăm sóc</a></li>
                    <li><a href="/class">Lớp học quan tâm</a></li>
                    <li><a href="/cdr">Chi tiết cuộc gọi</a></li>
                    <li><a href="http://192.168.50.52/api/missed_calls.php" target="_blank">Cuộc gọi nhỡ</a>
                    @endif

                    @if(Auth::user()->hasRoleInList(['admin', 'holding', 'teleteamleader', 'manager']))
                    <li><a href="/school">Trường</a></li>
                    @endif

                    <li><a href="/alarm">Lịch hẹn chăm sóc</a></li>
                    <li><a href="{{ route('calls.index') }}">Lịch sử chăm sóc</a></li>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                   aria-expanded="false">Báo cáo</a>
                <ul class="dropdown-menu">
                    <li><a href="/report/call_log">Thống kê Log call của TVTS</a></li>
                    <li><a href="/report/logSms">Thống kê tin nhắn của TVTS</a></li>
                    @if(Auth::user()->hasRole('admin') ||  Auth::user()->hasRole('holding') || in_array(Auth::user()->id, array(92, 126, 129, 195, 132)))
                        <li><a href="/report/leadInteractive">Chỉ số thời gian chăm sóc Lead</a></li>
                    @endif
                    @if(in_array(Auth::user()->id, array(92, 103, 195, 132)))
                        <li><a href="/report/logChangeStatus">Lịch sử thay đổi trạng thái lead</a></li>
                    @endif
                    @if(Auth::user()->hasRole('admin') ||  Auth::user()->hasRole('holding'))
                        <li><a href="/report/otb">Báo cáo tổng hợp lead - OTB</a></li>
                    @endif
                    @if(in_array(Auth::user()->id, array(92,5,77,103)))
                        <li><a href="/report/leadSource">Báo cáo nguồn dẫn lead</a></li>
                    @endif

                    @if(Auth::user()->hasRole('holding') || Auth::user()->department_id == \App\Department::HOLDING)
                        @if(Auth::user()->isSMISUser())
                            <li><a href="/report/leadDetail/smis">Báo cáo chi tiết</a></li>
                        @elseif(Auth::user()->isGISUser())
                            <li><a href="/report/leadDetail/gis">Báo cáo chi tiết</a></li>
                        @else
                            <li><a href="/report/leadDetail/smis">Báo cáo chi tiết - SMIS</a></li>
                            <li><a href="/report/leadDetail/gis">Báo cáo chi tiết - GIS</a></li>
                        @endif
                    @endif

                    <li><a href="/report/leadCare">Báo cáo chăm sóc lead</a></li>
                    @if(in_array(Auth::user()->id, array(92, 103, 133, 195, 132)))
                        <li><a href="/report/leadConvert">Báo cáo chuyển đổi Lead NE</a></li>
                    @endif

                    <li><a href="/report/leadConvertStatus">Báo cáo chuyển đổi trạng thái Lead</a></li>

                    @if(Auth::user()->hasRole('admin') ||  Auth::user()->hasRole('holding'))
                        <li><a href="/report/callLogDuration">Báo cáo thời lượng cuộc gọi</a></li>
                    @endif
                </ul>
            </li>
        </ul>
        @if(Route::current()->getName() == 'lead.index' || Route::current()->getName() == '')
            <form class="navbar-form navbar-left" id="gsearch_form" method="get"
                  action="{{ route('lead.index') }}">
                {{ csrf_field() }}
                <input type="hidden" name="search_mode" id="search_mode" value="global_search">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" size="75" name="gsearch" id="gsearch"
                               value="@php echo !empty($_REQUEST['gsearch'])?$_REQUEST['gsearch']:''; @endphp"
                               placeholder="Tìm nhanh bằng họ tên phụ huynh hoặc học sinh, số điện thoại, email, Facebook">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-default" id="gsearch_button" name="gsearch_button"
                                    value="gsearch"><i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                        </span>
                    </div>
                </div>
            </form>
        @endif
        <ul class="nav navbar-nav navbar-right">
            <li>
                <a id="btnShowNotify" href="javascript:void(0)" onclick="toggleNotifications()">
                    <i class="fa fa-bell"></i>
                    <span class="count_new hidden">0</span>
                </a>

                @include('includes.notifications')
            </li>

            <li><a id="reportError" href="javascript:void(0)" target="_blank">Thông báo lỗi</a></li>

            <li>
                <a href="{{ route('user.edit', Auth::user()->id) }}">
                    Xin chào <strong>{{ Auth::user()->full_name }}</strong>
                </a>
            </li>

            <li>
                <a href="{{ route('user.logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    Thoát
                </a>
                <form id="logout-form" action="{{ route('user.logout') }}" method="POST"
                      style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        </ul>
    </div><!-- /.navbar-collapse -->
</nav>
@include('includes.set_alarm')
@include('includes.call_log.log_call')
@include('includes.alarm')
@include('includes.error')