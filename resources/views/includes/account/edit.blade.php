<div role="tabpanel" class="tab-pane {{ $lead->converted?' active ':'' }}" id="account">
    <div class="col-md-12"><h4>Chăm sóc hồ sơ</h4></div>
    {!! Form::model($account, [
    'method' => 'PATCH',
    'route' => ['lead.update', $account->lead_id],
    'class' =>'form-inline edit-form',
    ]) !!}
    <div class="col-md-3">
        <div class="form-group">
            <label for="exampleInputEmail1">Mã HS</label>
            <input type="text" class="form-control" id="app_code" name="app_code"
                   value="{{ old('app_code')?old('app_code'):$account->app_code }}" required="required">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="exampleInputEmail1">Họ và tên</label>
            <input type="text" class="form-control" id="name" name="name"
                   value="{{ old('name')?old('name'):$account->name }}" required="required">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="exampleInputEmail1">Giới tính</label>
            Nam {{ Form::radio('gender', 1, $account->gender == 1) }}
            Nữ {{ Form::radio('gender', 2, $account->gender == 2) }}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="exampleInputEmail1">CMND</label>
            <input type="text" class="form-control" id="national_id" name="national_id"
                   value="{{ $account->id_number }}">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="exampleInputEmail1">Ngày cấp</label>
            <input type="text" class="form-control" id="issue_date" name="issue_date"
                   value="{{ $account->issue_date }}">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="exampleInputEmail1">Nơi cấp</label>
            {{ Form::select('issue_province', $province_list, old('issue_province'), ['id' => 'issue_province', 'placeholder' => 'Chọn tỉnh']) }}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="exampleInputEmail1">Ngày sinh</label>
            <input type="text" class="form-control" id="birthdate" name="birthdate" value="{{ $account->birthdate }}">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="exampleInputEmail1">Tỉnh</label>
            {{ Form::select('province', $province_list, old('province'), ['id' => 'province', 'placeholder' => 'Chọn tỉnh']) }}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="exampleInputEmail2">Trường</label>
            <select class="form-control" id="school" name="school" style="width: 100%;" multiple="multiple">
            </select>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="exampleInputEmail1">Năm tốt nghiệp</label>
            <input type="text" class="form-control" id="year_graduated" name="year_graduated"
                   value="{{ $account->year_graduated }}">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="exampleInputEmail1">Loại hồ sơ</label>
            {{ Form::select('status', $account_status_list, old('status'), ['id' => 'status', 'placeholder' => 'Chọn loại hồ sơ']) }}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="exampleInputEmail1">Nơi thu hồ sơ</label>
            {{ Form::select('status', $account_status_list, old('status'), ['id' => 'status', 'placeholder' => 'Chọn nơi thu']) }}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="exampleInputEmail1">Ngày nộp hồ sơ</label>
            <input type="text" class="form-control" id="submitted_at" name="submitted_at"
                   value="{{ $account->submitted_at }}">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="exampleInputEmail1">Ngành đăng ký</label>
            {{ Form::select('major', $major_list, old('major'), ['id' => 'major', 'placeholder' => 'Chọn ngành đăng ký']) }}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="exampleInputEmail1">Đợt thi</label>
            {{ Form::select('status', $campaign, old('campaign'), ['id' => 'campaign', 'placeholder' => 'Chọn đợt thi']) }}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="exampleInputEmail1">Trạng thái hồ sơ</label>
            {{ Form::select('status', $account_status_list, old('status'), ['id' => 'status', 'placeholder' => 'Chọn tình trạng']) }}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="exampleInputEmail1">Trạng thái tài chính hồ sơ</label>
            {{ Form::select('financial_status', $account_financial_status_list, old('financial_status'), ['id' => 'financial_status', 'placeholder' => 'Chọn tình trạng']) }}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="exampleInputEmail1">Hình thức nộp tiền</label>
            {{ Form::select('payment_method', $payment_method_list, old('payment_method'), ['id' => 'payment_method', 'placeholder' => 'Chọn hình thức nộp tiền']) }}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="exampleInputEmail1">Số HĐ nộp</label>
            <input type="text" class="form-control" id="invoice_number" name="invoice_number"
                   value="{{ $account->invoice_number }}">
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="exampleInputEmail1">Nơi nộp tiền</label>
            {{ Form::select('receiving_place', $receiving_place_list, old('receiving_place'), ['id' => 'receiving_place', 'placeholder' => 'Chọn nơi nộp tiền']) }}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="notes">Ghi chú</label>
            <textarea class="form-control" id="notes" name="notes" cols="25" rows="5">{{ $account->notes }}</textarea>
        </div>
    </div>
    <div class="col-md-12">
        <!--<button type="submit" class="btn btn-primary" name="account_save_button" value="save">Lưu</button>-->
    </div>
    {!! Form::close() !!}
</div>
<script>

    $('#birthdate').datepicker();
    $('#submitted_at').datepicker();
</script>