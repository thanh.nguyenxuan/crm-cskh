<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 27/04/2018
 * Time: 2:21 PM
 */
?>
<div role="tabpanel" class="tab-pane fade in" id="sms_history_pane">
    <table class="table table-condensed table-bordered">
        <thead>
        <th>STT</th>
        <th>Loại tin nhắn</th>
        <th>SĐT</th>
        <th>Nội dung</th>
        <th>Ngày gửi/nhận</th>
        </thead>
        <tbody>
        @php $stt = 0 @endphp
        @foreach($sms_list as $sms)
            <tr>
                <td>{{ ++$stt }}</td>
                <td>{{ $sms->type==1?'Gửi đi':'Gửi đến' }}</td>
                <td>{{ $sms->to }}</td>
                <td>{{ $sms->content }}</td>
                <td>{{ Carbon\Carbon::parse($sms->created_at)->format('d/m/Y H:i:s') }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>