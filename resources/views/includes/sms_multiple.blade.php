<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 19/12/2017
 * Time: 12:31 PM
 */
?>
<!-- Modal -->
<div class="modal" id="send_sms_multiple_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Gửi tin nhắn</h4>
            </div>
            <div class="modal-body">
                <div>Bạn đã chọn <strong><span id="send_sms_multiple_selected_count">0</span></strong> lead.</div>
                @php if(in_array(\App\Department::HOLDING, Auth::user()->getManageDepartments())){ @endphp
                <div class="form-group">
                    <label for="sms_multiple_brandname" class="required">BrandName:</label>
                    <select id="sms_multiple_brandname" name="sms_multiple_brandname" required>
                        {!! \App\Telcom::getListBrandNameOptions() !!}
                    </select>
                </div>
                @php } @endphp

                <div class="form-group">
                    <label for="sms_multiple_content" class="required">Nội dung:</label>
                    <span id="sms_multiple_content_count">0/400</span>
                    <textarea name="sms_multiple_content" id="sms_multiple_content" class="form-control" rows="5" maxlength="400" required></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="send_sms_multiple_button" class="btn btn-edufit-default">Gửi</button>
                <button type="button" id="btn-close-modal-sms" class="btn btn-danger">Đóng</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="send_sms_multiple_result_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Gửi tin nhắn</h4>
            </div>
            <div class="modal-body">
                <div id="send_sms_multiple_loading" class="text-center"><img src="/images/loading.gif"></div>
                <div id="send_sms_multiple_success" class="alert alert-success">Thành công</div>
                <div id="send_sms_multiple_fail" class="alert alert-danger">Thất bại</div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-danger">Đóng</button>
            </div>
        </div>
    </div>
</div>

<style>
    #sms_multiple_content_count{
        float: right;
        font-size: 12px;
    }
</style>

<script>
    function clear_send_sms_form() {
        $('#sms_multiple_content').val('');
        $('#sms_multiple_content_count').text('0/400');
    }

    $(function(){

        $('#sms_multiple_content').on('input', function(){
            var max_length = $(this).attr('maxlength');
            var content_length = $(this).val().length;
            $('#sms_multiple_content_count').text(content_length+'/'+max_length);
        });

        $('#send_sms_button').click(function () {
            clear_send_sms_form();
            $('#send_sms_multiple_success').hide();
            $('#send_sms_multiple_fail').hide();
            $("#send_sms_multiple_modal").modal('show');
            var selected = [];
            var selected_items = main_table.rows({selected: true}).data().toArray();
            for (var i = 0; i < selected_items.length; i++)
                selected.push(selected_items[i].id);
            $('#send_sms_multiple_selected_count').text(selected.length);
            //console.log(main_table.rows( { selected: true } ).data().toArray());
        });

        $('#btn-close-modal-sms').click(function () {
            $("#send_sms_multiple_modal").modal('hide');
            clear_send_sms_form();
        });

        $('#send_sms_multiple_button').click(function () {
            if(!$(this).hasClass('disabled')){
                var url = '{{ env('APP_URL') }}/lead/sendSmsMultiple';
                var selected = [];
                var selected_items = main_table.rows({selected: true}).data().toArray();
                for (var i = 0; i < selected_items.length; i++) selected.push(selected_items[i].id);
                var data = new Object();
                data.selected = selected;
                data.sms_content = $('#sms_multiple_content').val().trim();
                data._token = "{{ csrf_token() }}";
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    }
                });
                if (selected.length > 0) {
                    if($('#sms_multiple_brandname')){
                        data.brandname = $('#sms_multiple_brandname').val();
                        if(data.brandname === ''){
                            alert('Chưa chọn BrandName');
                            return;
                        }
                    }
                    if(data.sms_content !== ""){
                        $('#send_sms_multiple_button').addClass('disabled');
                        $('#send_sms_multiple_modal').modal('hide');
                        $('#send_sms_multiple_result_modal').modal('show');
                        $('#send_sms_multiple_loading').removeClass('hidden');
                        $.post(url, data, function(data,status,xhr){
                            $('#send_sms_multiple_button').removeClass('disabled');
                            $('#send_sms_multiple_loading').addClass('hidden');
                            if(data.success.length){
                                $('#send_sms_multiple_success').show();
                            }
                            if(data.fail.length){
                                $('#send_sms_multiple_fail').show();
                            }
                            clear_send_sms_form();
                        }, 'json');
                    }else{
                        alert('Nội dung tin nhắn rỗng');
                    }
                }else{
                    alert('Chưa lead nào được chọn');
                }
            }
        });



    });
</script>