<div role="tabpanel" class="tab-pane {{ $lead->converted?' active ':'' }}" id="ne">
	<div class="col-md-12"><h4>Chăm sóc nhập học</h4></div>
	{!! Form::model($account, [
	'method' => 'PATCH',
	'route' => ['lead.update', $account->lead_id],
	'class' =>'form-inline edit-form',
	]) !!}
	<div class="col-md-3">
	<div class="form-group">
		<label for="exampleInputEmail1">Họ và tên</label>
		<input type="text" class="form-control" id="name" name="name" value="{{ $account->name }}" required="required">
	</div>
	</div>
		<div class="col-md-3">
	<div class="form-group">
		<label for="exampleInputEmail1">Giới tính</label>
		Nam {{ Form::radio('gender', 'm', $account->gender == 'm') }}
		Nữ {{ Form::radio('gender', 'f', $account->gender == 'f') }}
	</div>
	</div>
	<div class="col-md-3">
	<div class="form-group">
		<label for="exampleInputEmail1">CMND</label>
		<input type="text" class="form-control" id="national_id" name="national_id" value="{{ $account->national_id }}" >
	</div>
	</div>
	<div class="col-md-3">
	<div class="form-group">
		<label for="exampleInputEmail1">Ngày sinh</label>
		<input type="text" class="form-control" id="birthdate" name="birthdate" value="{{ $account->birthdate }}">
	</div>
	</div>
	<div class="col-md-3">
	<div class="form-group">
		<label for="exampleInputEmail1">Trạng thái hồ sơ</label>
		{{ Form::select('status', $account_status_list, old('status'), ['id' => 'status', 'placeholder' => 'Chọn tình trạng']) }}
	</div>
	</div>
	<div class="col-md-3">
	<div class="form-group">
		<label for="exampleInputEmail1">Tỉnh</label>
		{{ Form::select('province', $province_list, old('province'), ['id' => 'province', 'placeholder' => 'Chọn tỉnh']) }}
	</div>
	</div>
	<div class="col-md-3">
	<div class="form-group">
		<label for="exampleInputEmail1">Loại hồ sơ</label>
		{{ Form::select('status', $account_status_list, old('status'), ['id' => 'status', 'placeholder' => 'Chọn tình trạng']) }}
	</div>
	</div>
	<div class="col-md-3">
	<div class="form-group">
		<label for="exampleInputEmail1">Nơi thu hồ sơ</label>
		{{ Form::select('status', $account_status_list, old('status'), ['id' => 'status', 'placeholder' => 'Chọn tình trạng']) }}
	</div>
	</div>
	<div class="col-md-3">
	<div class="form-group">
		<label for="exampleInputEmail1">Ngày nộp hồ sơ</label>
		<input type="text" class="form-control" id="submitted_at" name="submitted_at" value="{{ $account->submitted_at }}">
	</div>
	</div>
		<div class="col-md-3">
	<div class="form-group">
		<label for="exampleInputEmail1">Ngành đăng ký</label>
		{{ Form::select('major', $major_list, old('major'), ['id' => 'major', 'placeholder' => 'Chọn ngành đăng ký']) }}
	</div>
	</div>
	<div class="col-md-3">
	<div class="form-group">
		<label for="exampleInputEmail1">Đợt thi</label>
		{{ Form::select('status', $campaign, old('campaign'), ['id' => 'campaign', 'placeholder' => 'Chọn đợt thi']) }}
	</div>
	</div>
	<div class="col-md-3">
	<div class="form-group">
		<label for="exampleInputEmail1">Trạng thái tài chính hồ sơ</label>
		{{ Form::select('financial_status', $account_financial_status_list, old('financial_status'), ['id' => 'financial_status', 'placeholder' => 'Chọn tình trạng']) }}
	</div>
	</div>
	<div class="col-md-3">
	<div class="form-group">
		<label for="exampleInputEmail1">Hình thức nộp tiền</label>
		{{ Form::select('payment_method', $payment_method_list, old('payment_method'), ['id' => 'payment_method', 'placeholder' => 'Chọn hình thức nộp tiền']) }}
	</div>
	</div>
		<div class="col-md-3">
	<div class="form-group">
		<label for="exampleInputEmail1">Số HĐ nộp</label>
		<input type="text" class="form-control" id="invoice_number" name="invoice_number" value="{{ $account->invoice_number }}">
	</div>
	</div>
	<div class="col-md-3">
	<div class="form-group">
		<label for="exampleInputEmail1">Nơi nộp tiền</label>
		{{ Form::select('receiving_place', $receiving_place_list, old('receiving_place'), ['id' => 'receiving_place', 'placeholder' => 'Chọn nơi nộp tiền']) }}
	</div>
	</div>
	<div class="col-md-6">
	<div class="form-group">
		<label for="notes">Ghi chú</label>
		<textarea class="form-control" id="notes" name="notes" cols="25" rows="5">{{ $account->notes }}</textarea>
	</div>
	</div>
	<div class="col-md-12">
		<button type="submit" class="btn btn-primary" name="account_save_button">Lưu</button>
	</div>
	{!! Form::close() !!}
</div>