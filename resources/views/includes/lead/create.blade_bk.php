@include('includes.header')
<div class="col-md-12">
    <h4>Tạo lead mới</h4>
    @if(!empty(session('success')) && !empty(session('return_message')))
        <div class="col-md-12">
            <div class="alert alert-success">
                {{ session('return_message') }}
            </div>
        </div>
    @endif
    @if(empty(session('success')) && !empty(session('return_message')))
        <div class="col-md-12">
            <div class="alert alert-danger">
                {{ session('return_message') }}
            </div>
        </div>
    @endif
    {!! Form::model($lead, [
        'method' => 'POST',
        'route' => ['lead.update', $lead->id],
        'class' =>'form-inline edit-form',
    ]) !!}
    <div class="panel panel-edufit-default">
        <div class="panel-heading">
            <h3 class="panel-title">Thông tin cơ bản</h3>
        </div>
        <div class="panel-body">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1" class="required">Họ tên phụ huynh</label>
                    <input type="text" class="form-control" id="name" name="name"
                           value="{{ old('name') }}" required="required">

                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1" class="required">Họ tên học sinh</label>
                    <input type="text" class="form-control" id="student_name" name="student_name"
                           value="{{ old('student_name') }}" required="required">

                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1">Ngày sinh</label>
                    <input type="text" class="form-control" id="birthdate" name="birthdate" placeholder="dd/mm/yyyy"
                           value="{{ old('birthdate') }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="school">Trường</label>
                    <select class="form-control" id="school" name="school" style="width: 100%;"></select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="nick_name">Nick name</label>
                    <input type="text" class="form-control" id="nick_name" name="nick_name"
                           value="{{ old('nick_name') }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="gender">Giới tính</label>
                    <span style="margin-right: 20px;">{{ Form::radio('gender', 'm') }} Nam</span>
                    <span>{{ Form::radio('gender', 'f') }} Nữ</span>

                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="gender">Sinh đôi</label>
                    {{ Form::checkbox('twins', 1) }}

                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-edufit-default">
        <div class="panel-heading">
            <h3 class="panel-title">Thông tin liên hệ</h3>
        </div>
        <div class="panel-body">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1" class="required">SĐT 1</label>
                    <input type="text" class="form-control" id="phone1" name="phone1"
                           value="{{ old('phone1') }}"
                           required="required">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1">SĐT 2</label>
                    <input type="text" class="form-control" id="phone2" name="phone2"
                           value="{{ old('phone2') }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email" name="email"
                           value="{{ old('email') }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1">Facebook</label>
                    <input type="text" class="form-control" id="phone2" name="facebook"
                           value="{{ old('facebook') }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1">Địa chỉ</label>
                    <textarea class="form-control" id="address" name="address" cols="20"
                              rows="3">{{ old('address') }}</textarea>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Tỉnh/thành phố</label>
                    {{ Form::select('province', $province_list, Auth::user()->department_id?App\Department::find(Auth::user()->department_id)->province:old('province_id'), ['id' => 'province', 'placeholder' => 'Chọn tỉnh/thành phố']) }}
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Quận/huyện</label>
                    <select class="form-control" id="district" name="district" style="width: 100%;"></select>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-edufit-default">
        <div class="panel-heading">
            <h3 class="panel-title">Hành động</h3>
        </div>
        <div class="panel-body">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1">Long-term objective</label>
                    @foreach($objective_list as $objective)
                        <div>{{ Form::checkbox('objective', $objective->id) }} {{ $objective->name }}</div>
                    @endforeach
                    <input type="text" class="form-control" id="objective_others" name="objective_others"
                           value="{{ !empty(old('objective_others'))?old('objective_others'):$lead->objective_others }}"
                           data-toggle="tooltip" data-placement="right">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1">Parents' concern</label>
                    @foreach($parents_concern_list as $concern)
                        <div>{{ Form::checkbox('parents_concern', $concern->id) }} {{ $concern->name }}</div>
                    @endforeach
                    <input type="text" class="form-control" id="parents_concern_others" name="parents_concern_others"
                           value="{{ !empty(old('parents_concern_others'))?old('parents_concern_others'):$lead->parents_concern_others }}"
                           data-toggle="tooltip" data-placement="right">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1">Student's challenges</label>
                    @foreach($student_challenge_list as $challenge)
                        <div>{{ Form::checkbox('student_challenge', $challenge->id) }} {{ $challenge->name }}</div>
                    @endforeach
                    <input type="text" class="form-control" id="student_challenge_others"
                           name="student_challenge_others"
                           value="{{ !empty(old('student_challenge_others'))?old('student_challenge_others'):$lead->student_challenge_others }}"
                           data-toggle="tooltip" data-placement="right">
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-edufit-default">
        <div class="panel-heading">
            <h3 class="panel-title">Thông tin khác</h3>
        </div>
        <div class="panel-body">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1" class="required">Trạng thái lead</label>
                    {{ Form::select('status', $status_list, old('status'), ['id' => 'status', 'placeholder' => 'Chọn trạng thái', 'required' => 'required']) }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1" class="required">Nguồn dẫn</label>
                    {{ Form::select('source', $source_list, old('source'), ['id' => 'source', 'placeholder' => 'Chọn nguồn dẫn', 'required' => 'required']) }}

                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1">Chiến dịch QC</label>
                    <input type="text" class="form-control" id="utm_source" name="utm_campaign"
                           value="{{ $lead->utm_campaign }}">
                </div>
            </div>
        <!--
<div class="col-md-4">
    <div class="form-group">
        <label for="exampleInputEmail1">Keyword QC</label>
        <input type="text" class="form-control" id="utm_source" name="utm_medium" value="{{ $lead->utm_medium }}">
    </div>
</div>
-->
        <!---
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1" class="required">Cơ sở</label>
                    {{ Form::select('department', $department_list, !empty(old('department_id'))?old('department_id'):Auth::user()->department_id, ['id' => 'department', 'placeholder' => 'Chọn trường', 'required' => 'required']) }}
                </div>
            </div>
            !-->
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1">Nguồn thông tin</label>
                    {{ Form::select('info_source', $info_source_list, !empty(old('info_source'))?old('info_source'):$lead->info_source, ['id' => 'info_source', 'placeholder' => 'Chọn nguồn thông tin']) }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1">Kênh quảng cáo</label>
                    {{ Form::select('channel', $channel_list, !empty(old('channel'))?old('channel'):$lead->channel, ['id' => 'channel', 'placeholder' => 'Chọn kênh quảng cáo']) }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1">Promotion</label>
                    {{ Form::select('promotion', $promotion_list, !empty(old('promotion'))?old('promotion'):$lead->promotion, ['id' => 'promotion', 'placeholder' => 'Chọn promotion']) }}
                </div>
            </div>
        <!--
<div class="col-md-4">
    <div class="form-group">
        <label for="school">Mong muốn học</label>
        <input type="text" class="form-control" id="want_study" name="want_study" value="{{ $lead->want_study }}">
    </div>
</div>
<div class="col-md-4">
    <div class="form-group">
        <label for="school">Mong muốn thi</label>
        <input type="text" class="form-control" id="want_exam" name="want_exam" value="{{ $lead->want_exam }}">
    </div>
</div>
-->
            <div class="col-md-4">
                <div class="form-group">
                    <label for="school">Lớp học quan tâm</label>
                    {{ Form::select('major', $major_list, old('major'), ['id' => 'major', 'placeholder' => 'Chọn lớp học quan tâm']) }}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="rating">Mức độ quan tâm</label>
                    {{ Form::select('rating', $rating_list, old('rating'), ['id' => 'rating', 'placeholder' => 'Chọn mức độ quan tâm']) }}
                </div>
            </div>
        <!---
            <div class="col-md-4">
                <div class="form-group">
                    <label for="school">Người phụ trách</label>
                    {{ Form::select('assignee', $assignee_list, !empty(old('assignee'))?old('assignee'):Auth::id(), ['id' => 'assignee', 'placeholder' => 'Chọn người phụ trách']) }}
                </div>
            </div>
            !-->
            <div class="col-md-6">
                <div class="form-group">
                    <label for="notes">Ghi chú</label>
                    <textarea class="form-control" id="notes" name="notes" cols="50"
                              rows="6">{{ old('notes') }}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <button type="submit" class="btn btn-edufit-default" name="save_button">Lưu</button>
    </div>
</div>
{!! Form::close() !!}
<script src="/js/bootstrap.min.js"></script>
<script src="/js/filter.js"></script>
<script>
    function selectSchool(province) {
        if (province) {
            var url = '{{ env('APP_URL') }}/utils/getSchools/' + province;
            $('#school').select2({
                placeholder: 'Chọn trường',
                ajax: {
                    url: url,
                    dataType: 'json',

                }
            });
        } else {
            $('#school').select2({
                placeholder: 'Chưa chọn tỉnh/thành phố'
            });
        }
    }

    function selectDistrict(province) {
        if (province) {
            var url = '{{ env('APP_URL') }}/utils/getDistricts/' + province;
            $('#district').select2({
                placeholder: 'Chọn quận/huyện',
                ajax: {
                    url: url,
                    dataType: 'json',

                }
            });
        } else {
            $('#district').select2({
                placeholder: 'Chưa chọn tỉnh/thành phố'
            });
        }
    }

    $(function () {
        var province = $("#province").val();
        selectSchool(province);
        selectDistrict(province);
                @if(old('school'))
        var school = '{{ old('school') }}';
        var url = '{{ env('APP_URL') }}/utils/getSchools/' + province + '/' + school;
        $.getJSON(url).then(function (data) {
            // create the option and append to Select2
            if (data.results.length > 0) {
                var option = new Option(data.results[0].text, data.results[0].id, true, true);
                $('#school').append(option).trigger('change');
                $('#school').trigger({
                    type: 'select2:select',
                    params: {
                        data: data
                    }
                });
            }
        });
        @endif
        $("#province").change(function () {
            var province = $("#province").val();
            selectSchool(province);
            selectDistrict(province);
        });
    });

</script>
</body>
</html>
