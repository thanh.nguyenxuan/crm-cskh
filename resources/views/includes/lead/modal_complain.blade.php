<div class="modal fade" id="modal_complain" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <form method="post" id="modal_complain_form">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Than phiền</h4>
                </div>
                <div class="modal-body">

                    <div id="complain_success_logged_message" class="hidden alert alert-success">Lưu dữ liệu thành công!</div>
                    <div id="complain_error_logged_message" class="hidden alert alert-danger">Đã có lỗi trong quá trình lưu dữ liệu!</div>

                    <div class="form-group">
                        <label for="complain_reason" class="required">Lý do:</label>
                        {{ Form::select('complain_reason', \App\ComplainReason::listData(), '', [
                            'placeholder' => 'Chọn',
                            'required' => true,
                            'id' => 'complain_reason',
                            'class' => 'form-control'
                        ]) }}
                    </div>

                    <div class="form-group">
                        <label for="complain_note">Ghi chú:</label>
                        <textarea name="complain_note" id="complain_note" class="form-control" rows="5"></textarea>
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-modal-complain-save" class="btn btn-primary">Lưu</button>
                    <button type="button" id="btn-modal-complain-close" class="btn btn-danger">Đóng</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    function clear_complain_form() {
        $('#complain_reason').val('');
        $('#complain_note').val('');
        $('#complain_success_logged_message').removeClass('hidden');
        $('#complain_success_logged_message').addClass('hidden');
        $('#complain_error_logged_message').removeClass('hidden');
        $('#complain_error_logged_message').addClass('hidden');
    }

    $(function () {

        $('#modal_complain_form').submit(function (event) {
            event.preventDefault();
            if($("#btn-modal-complain-save").hasClass('disabled')){
                return false;
            }

            $.ajax({
                url : '{{ env('APP_URL') }}/lead/createComplain',
                type : 'POST',
                dataType: 'json',
                data : {
                    _token : '{{ csrf_token() }}',
                    reason : $('#complain_reason').val(),
                    note : $('#complain_note').val(),
                    lead_id : '{{ $lead->id }}'
                },
                success: function(result) {
                    if(result.success){
                        clear_complain_form();
                        $('#table-complain tbody').prepend(result.dataHtml);
                        $('#complain_success_logged_message').removeClass('hidden');
                        $('#complain_error_logged_message').addClass('hidden');
                        $("#btn-modal-complain-save").addClass('disabled');
                    }else{
                        $('#complain_success_logged_message').addClass('hidden');
                        $('#complain_error_logged_message').removeClass('hidden');
                        $('#complain_error_logged_message').html(result.message);
                    }
                }
            });
        });

        $('#btn-modal-complain-open').click(function () {
            $("#modal_complain").modal('show');
            $("#btn-modal-complain-save").removeClass('disabled');
        });

        $('#btn-modal-complain-close').click(function () {
            $("#modal_complain").modal('hide');
            clear_complain_form();
        });

    });
</script>