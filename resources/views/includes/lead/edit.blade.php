@extends('layouts.main')

@section('content')
@include('includes.call_log.log_call')
@include('includes.sms')
@php if(!empty(\Illuminate\Support\Facades\Auth::user()->extension)){ @endphp
@include('cdr.call_log')
@php } @endphp
@include('includes.log_lead_care')
@include('includes.lead.modal_complain')
@if(isset($duplicate_warning) && $duplicate_warning['warning'])
    <div class="alert alert-danger">
        <strong>Cảnh báo</strong>: {!! $duplicate_warning['message'] !!}
    </div>
@endif
{!! Form::model($lead, [
    'method' => 'PATCH',
    'route' => ['lead.update', $lead->id],
    'class' =>'form-horizontal',
]) !!}
{{ method_field('PUT') }}
<div class="col-md-12">

    <div class="row">
        <div class="col-sm-9">
            <div class="row">
                <div class="col-sm-8">
                    <h4>Chăm sóc Phụ huynh
                        <a id="set_alarm_button" title="Hẹn lịch" style="color: #337ab7"><i class="fa fa-lg fa-calendar"></i></a>
                        <a id="log_call_button" title="Log call" style="margin-left: 0.3rem; color: #337ab7"><i class="fa fa-lg fa-phone"></i></a>
                        <a id="send_sms_button" title="Gửi tin nhắn" style="margin-left: 0.3rem; color: #337ab7"><i class="fa fa-lg fa-envelope"></i></a>
                        <?php if(!empty(\Illuminate\Support\Facades\Auth::user()->extension)){ ?>
                        <a id="cdr_call_log_btn" title="Gọi điện" style="margin-left: 0.3rem; color: #337ab7">
                            <img src="/images/phone-office.png"/>
                        </a>
                        <?php } ?>
                        <a id="log_lead_care_btn" title="Tạo ghi chú chăm sóc" style="margin-left: 0.3rem; color: #337ab7">
                            <i class="fa fa-file-text"></i>
                        </a>
                        <a id="btn-modal-complain-open" title="Than phiền" style="margin-left: 0.3rem; color: #337ab7">
                            <i class="fa fa-exclamation-circle"></i>
                        </a>
                    </h4>
                </div>
                <div class="col-sm-4 text-right">
                    @if(
                    (Auth::user()->username == trim($lead->assignee_list) && empty($lead->leave_school))
                    ||
                    (Auth::user()->hasRole('manager') && in_array($lead->department_id, Auth::user()->getManageDepartments()))
                    ||
                    (Auth::user()->hasRole('carer') && in_array($lead->department_id, Auth::user()->getManageDepartments()) && $lead->status == \App\LeadStatus::STATUS_ENROLL)
                    )
                    <button type="submit" class="btn btn-edufit-default" name="lead_save_button" value="save" style="width: 100px">
                        Lưu
                    </button>
                    @endif

                    <button type="button" class="btn btn-edufit-danger" name="close_button" value="close" style="width: 100px"
                            onclick="window.close()">Đóng
                    </button>
                </div>
            </div>
        </div>
    </div>

    @if(!empty(session('success')) && !empty(session('return_message')))
        <div class="alert alert-success">
            {!! session('return_message') !!}
        </div>
    @endif
    @if(empty(session('success')) && !empty(session('return_message')))
        <div class="alert alert-danger">
            {{ session('return_message') }}
        </div>
    @endif

    <style>
        .lead-info-tabs .panel,
        .lead-log-tabs .panel{
            border: 1px solid #ddd;
            border-top-left-radius: unset;
            border-top-right-radius: unset;
        }
    </style>
    <div class="row">
        <div class="col-sm-9">

            <div class="lead-info-tabs">
                <!-- Navigation -->
                <ul class="nav nav-tabs nav-pills">
                    <li class="active">
                        <a class="nav-link" data-toggle="pill" role="tab"  href="#contact-info">Thông tin liên hệ</a>
                    </li>
                    <li>
                        <a class="nav-link" data-toggle="pill" role="tab" href="#status-info">Trạng thái - Thông tin khác</a>
                    </li>
                    <li>
                        <a class="nav-link" data-toggle="pill" role="tab" href="#concern-info">Quan tâm</a>
                    </li>
                    <li>
                        <a class="nav-link" data-toggle="pill" role="tab" href="#enroll-info">Nhập học</a>
                    </li>
                    <li>
                        <a class="nav-link" data-toggle="pill" role="tab" href="#guest-info">Bé làm khách</a>
                    </li>
                    <li>
                        <a class="nav-link" data-toggle="pill" role="tab" href="#history-info">Lịch sử chăm sóc</a>
                    </li>
                    <li>
                        <a class="nav-link" data-toggle="pill" role="tab" href="#complain-info">Than phiền</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content" role="tabpanel">

                    <div role="tabpanel" class="tab-pane fade in active" id="contact-info">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <div class="col-sm-4 text-right">
                                                <label for="name" class="control-label required">Họ tên PH</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <input id="name" type="text" name="name" value="{{ !empty(old('name'))?old('name'):$lead->name }}" class="form-control" data-toggle="tooltip" data-placement="right">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4 text-right">
                                                <label for="phone1" class="control-label required">SĐT 1</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <input id="phone1" type="text" name="phone1" value="{{ !empty(old('phone1'))?old('phone1'):$lead->phone1 }}" class="form-control" data-toggle="tooltip" data-placement="right">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4 text-right">
                                                <label for="phone2" class="control-label">SĐT 2</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <input id="phone2" type="text" name="phone2" value="{{ !empty(old('phone2'))?old('phone2'):$lead->phone2 }}" class="form-control" data-toggle="tooltip" data-placement="right">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <div class="col-sm-4 text-right">
                                                <label for="email" class="control-label">Email 1</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <input id="email" type="email" name="email" value="{{ !empty(old('email'))?old('email'):$lead->email }}" class="form-control" data-toggle="tooltip" data-placement="right">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4 text-right">
                                                <label for="email_2" class="control-label">Email 2</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <input id="email_2" type="email" name="email_2" value="{{ !empty(old('email_2'))?old('email_2'):$lead->email_2 }}" class="form-control" data-toggle="tooltip" data-placement="right">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4 text-right">
                                                <label for="facebook" class="control-label">Facebook</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <input id="facebook" type="text" name="facebook" value="{{ !empty(old('facebook'))?old('facebook'):$lead->facebook }}" class="form-control" data-toggle="tooltip" data-placement="right">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <div class="col-sm-4 text-right">
                                                <label for="" class="control-label">Tỉnh/Thành phố</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <select id="province" name="province_id" class="form-control">
                                                    <?php echo \App\Utils::generateOptions(\App\Province::listData(), !empty(old('province_id'))?old('province_id'):$lead->province_id, 'Chọn tỉnh/thành phố') ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4 text-right">
                                                <label for="district" class="control-label">Quận/Huyện</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <select id="district" name="district" class="form-control">
                                                    <?php echo \App\Utils::generateOptions(
                                                        !empty(old('province_id'))
                                                            ? \App\District::listData(old('province_id'))
                                                            : (!empty($lead->province_id)
                                                            ? \App\District::listData($lead->province_id)
                                                            : array()
                                                        )
                                                        , !empty(old('district'))?old('district'):$lead->district, 'Chọn quận/huyện') ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4 text-right">
                                                <label for="address" class="control-label">Địa chỉ</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <textarea id="address" name="address" class="form-control">{{ !empty(old('address'))?old('address'):$lead->address }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <hr size="30">
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <div class="col-sm-4 text-right">
                                                <label for="student_name" class="control-label required">Họ tên học sinh</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <input id="student_name" type="text" name="student_name" value="{{ !empty(old('student_name'))?old('student_name'):$lead->student_name }}" class="form-control" data-toggle="tooltip" data-placement="right">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4 text-right">
                                                <label for="nick_name" class="control-label">Nick name</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <input id="nick_name" type="text" name="nick_name" value="{{ !empty(old('nick_name'))?old('nick_name'):$lead->nick_name }}" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <div class="col-sm-4 text-right">
                                                <label for="birthdate" class="control-label">Ngày sinh</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <input id="birthdate" type="text" name="birthdate" value="{{ !empty(old('birthdate'))?old('birthdate'):($lead->birthdate?Carbon\Carbon::parse($lead->birthdate)->format('d/m/Y'):'') }}" class="form-control" placeholder="dd/mm/yyyy">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4 text-right">
                                                <label for="" class="control-label">Giới tính</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <div class="radio" style="padding-left: 20px">
                                                    {{ Form::radio('gender', 'm') }} Nam
                                                    &emsp; &emsp;
                                                    {{ Form::radio('gender', 'f') }} Nữ
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <div class="col-sm-4 text-right">
                                                <label for="school" class="control-label">Trường</label>
                                            </div>
                                            <div class="col-sm-8">
                                                <select id="school" name="school" class="form-control">
                                                    <?php echo \App\Utils::generateOptions(\App\School::listData(), !empty(old('school'))?old('school'):$lead->school, 'Chọn trường', true) ?>
                                                </select>
                                                <input id="other_school" type="text" name="other_school" value="{{ old('other_school') }}" class="form-control<?php echo (old('school') == \App\Utils::OTHER_OPTION) ? '' : ' hidden'?>" style="margin-top: 5px" placeholder="Tên trường đang học">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4 text-right">
                                                <label for="" class="control-label">Nhiều con</label>
                                            </div>
                                            <div class="col-sm-8 radio">
                                                {{ Form::checkbox('twins', 1) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <hr size="30">
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="col-sm-2 text-right">
                                                <label for="siblings_info" class="control-label">Thông tin anh chị em</label>
                                            </div>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" id="siblings_info" name="siblings_info" cols="50" rows="6">{{ !empty(old('siblings_info'))?old('siblings_info'):$lead->siblings_info }}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="status-info">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="status" class="control-label required">Trạng thái HS</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <select name="status" class="form-control" id="status">
                                                {!! $lead->generateOptionStatus($status_list, $lead->status, 'Chọn trạng thái') !!}
                                            </select>
                                        </div>
                                    </div>
                                    <?php if(Auth::user()->hasRole('carer')){  ?>
                                        <div class="form-group">
                                            <div class="col-sm-4 text-right">
                                                <label for="care_status" class="control-label">Trạng thái chăm sóc</label>
                                            </div>
                                            <div class="col-sm-8">
                                                {{ Form::select('care_status', \App\CareStatus::listData(), !empty(old('care_status'))?old('care_status'):$lead->care_status, ['id' => 'care_status', 'placeholder' => 'Chọn trạng thái chăm sóc', 'class' => 'form-control']) }}
                                            </div>
                                        </div>
                                    <?php }else{ ?>
                                        <div class="form-group">
                                            <div class="col-sm-4 text-right">
                                                <label for="call_status_id" class="control-label">Trạng thái chăm sóc</label>
                                            </div>
                                            <div class="col-sm-8">
                                                {{ Form::select('call_status_id', $call_status_list, !empty(old('call_status_id'))?old('call_status_id'):$lead->call_status, ['id' => 'call_status_id', 'placeholder' => 'Chọn trạng thái chăm sóc', 'class' => 'form-control']) }}

                                                <input id="call_status_no_concern_reason" type="text"
                                                       name="call_status_no_concern_reason"
                                                       value="{{ !empty(old('call_status_no_concern_reason'))?old('call_status_no_concern_reason'):$lead->call_status_no_concern_reason }}"
                                                       class="form-control<?php echo !empty(old('call_status_id')) ? (old('call_status_id') == \App\CallStatus::STATUS_NO_CONCERN ? '' : ' hidden') : ($lead->call_status == \App\CallStatus::STATUS_NO_CONCERN ? '' : ' hidden')?>"
                                                       style="margin-top: 5px" placeholder="Lý do">
                                            </div>
                                        </div>
                                    <?php }?>

                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="source" class="control-label">Nguồn dẫn</label>
                                        </div>
                                        <div class="col-sm-8">
                                            @if(empty($source_list_array))
                                                {{ Form::select('source', $source_list, !empty(old('source'))?old('source'):$source_list_array, ['id' => 'source', 'placeholder' => 'Chọn nguồn lead', 'class' => 'form-control']) }}
                                            @else
                                                <p class="form-control-static">{{ $source_list_string }}</p>
                                            @endif
                                            <?php echo (!empty($lead->source) ? '('.$lead->source.')' : '')?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="name" class="control-label">Người giới thiệu</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input id="referrer_name" type="text" name="referrer_name" value="{{ !empty(old('referrer_name'))?old('referrer_name'):$lead->referrer_name }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="name" class="control-label">SĐT người GT</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input id="referrer_phone" type="text" name="referrer_phone" value="{{ !empty(old('referrer_phone'))?old('referrer_phone'):$lead->referrer_phone }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="" class="control-label">Nguồn thông tin</label>
                                        </div>
                                        <div class="col-sm-8">
                                            {{ Form::select('info_source', $info_source_list, !empty(old('info_source'))?old('info_source'):$lead->info_source, ['id' => 'info_source', 'placeholder' => 'Chọn nguồn thông tin', 'class' => 'form-control']) }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="learning_system_desire" class="control-label">Loại Lead</label>
                                        </div>
                                        <div class="col-sm-8">
                                            {{ Form::select('lead_type',
                                                \App\LeadType::listData(),
                                                !empty(old('lead_type'))?old('lead_type'):$lead->lead_type,
                                                ['id' => 'lead_type', 'placeholder' => 'Chọn loại', 'class' => 'form-control']
                                            )}}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="department" class="control-label required">Cơ sở</label>
                                        </div>
                                        <div class="col-sm-8">
                                            @if(!empty($lead->department_id) && !Auth::user()->hasRole('admin') && !Auth::user()->hasRole('holding'))
                                                {{ $department_name }}
                                            @else
                                                {{ Form::select('department', $department_list, !empty(old('department'))?old('department'):$lead->department_id, ['id' => 'department', 'placeholder' => 'Chọn cơ sở', 'class' => 'form-control']) }}
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="channel" class="control-label">Kênh quảng cáo</label>
                                        </div>
                                        <div class="col-sm-8">
                                            {{ Form::select('channel', $channel_list, !empty(old('channel'))?old('channel'):$lead->channel, ['id' => 'channel', 'placeholder' => 'Chọn kênh quảng cáo', 'class' => 'form-control']) }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="utm_campaign" class="control-label">Chiến dịch QC</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="utm_campaign" name="utm_campaign" value="{{ $lead->utm_campaign }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="promotion" class="control-label">Promotion</label>
                                        </div>
                                        <div class="col-sm-8">
                                            {{ Form::select('promotion', $promotion_list, !empty(old('promotion'))?old('promotion'):$lead->promotion, ['id' => 'promotion', 'placeholder' => 'Chọn promotion', 'class' => 'form-control']) }}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="col-sm-2 text-right">
                                            <label for="notes" class="control-label">Ghi chú</label>
                                        </div>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" id="notes" name="notes" cols="50" rows="6">{{ !empty(old('notes'))?old('notes'):$lead->notes }}</textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="concern-info">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="" class="control-label">Mục tiêu dài hạn</label>
                                        </div>
                                        <div class="col-sm-8">
                                            @foreach($objective_list as $objective)
                                                <div>{{ Form::checkbox('objective[]', $objective->id, $objective->isChecked(old('objective'), $list_lead_objective, !empty($lead->objective_others))) }} {{ $objective->name }}</div>
                                            @endforeach

                                            <input id="objective_others" type="text" name="objective_others"
                                                   value="{{ !empty(old('objective_others'))?old('objective_others'):$lead->objective_others }}"
                                                   class="form-control<?php echo (!empty(old('objective_others')) || !empty($lead->objective_others)) ? '' : ' hidden'?>"
                                                   style="margin-top: 5px">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="" class="control-label">Quan tâm của phụ huynh</label>
                                        </div>
                                        <div class="col-sm-8">
                                            @foreach($parents_concern_list as $concern)
                                                <div>{{ Form::checkbox('parents_concern[]', $concern->id, $concern->isChecked(old('parents_concern'), $list_lead_parents_concern, !empty($lead->parents_concern_others))) }} {{ $concern->name }}</div>
                                            @endforeach

                                            <input id="parents_concern_others" type="text" name="parents_concern_others"
                                                   value="{{ !empty(old('parents_concern_others'))?old('parents_concern_others'):$lead->parents_concern_others }}"
                                                   class="form-control<?php echo (!empty(old('parents_concern_others')) || !empty($lead->parents_concern_others)) ? '' : ' hidden'?>"
                                                   style="margin-top: 5px">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="" class="control-label">Mục tiêu của học sinh</label>
                                        </div>
                                        <div class="col-sm-8">
                                            @foreach($student_challenge_list as $challenge)
                                                <div>{{ Form::checkbox('student_challenge[]', $challenge->id, $concern->isChecked(old('student_challenge'), $list_lead_student_challenge, !empty($lead->student_challenge_others))) }} {{ $challenge->name }}</div>
                                            @endforeach

                                            <input id="student_challenge_others" type="text" name="student_challenge_others"
                                                   value="{{ !empty(old('student_challenge_others'))?old('student_challenge_others'):$lead->student_challenge_others }}"
                                                   class="form-control<?php echo (!empty(old('student_challenge_others')) || !empty($lead->student_challenge_others)) ? '' : ' hidden'?>"
                                                   style="margin-top: 5px">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <hr size="30">
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="major" class="control-label">Mức độ quan tâm</label>
                                        </div>
                                        <div class="col-sm-8">
                                            {{ Form::select('rating', $rating_list, !empty(old('rating'))?old('rating'):$lead->rating, ['id' => 'rating', 'placeholder' => 'Chọn mức độ quan tâm', 'class' => 'form-control']) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="major" class="control-label">Lớp học quan tâm</label>
                                        </div>
                                        <div class="col-sm-8">
                                            {{ Form::select('major', $major_list, !empty(old('major'))?old('major'):$lead->major, ['id' => 'major', 'placeholder' => 'Chọn lớp học quan tâm', 'class' => 'form-control']) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="learning_system_desire" class="control-label">Hệ học quan tâm</label>
                                        </div>
                                        <div class="col-sm-8">
                                            {{ Form::select('learning_system_desire',
                                                \App\LearningSystemDesire::listData(),
                                                !empty(old('learning_system_desire'))?old('learning_system_desire'):$lead->learning_system_desire,
                                                ['id' => 'learning_system_desire', 'placeholder' => 'Chọn hệ học quan tâm', 'class' => 'form-control']
                                            )}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="enroll_desire_at" class="control-label">Thời gian học dự kiến</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input id="enroll_desire_at" type="text" name="enroll_desire_at" value="{{ !empty(old('enroll_desire_at'))?old('enroll_desire_at'):($lead->enroll_desire_at?Carbon\Carbon::parse($lead->enroll_desire_at)->format('d/m/Y'):'') }}" class="form-control" placeholder="dd/mm/yyyy">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <?php if(!empty($lead->enroll_desire_at) && !empty($lead->enroll_desire_register_at)){ ?>
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="enroll_desire_at" class="control-label">Thời gian chờ</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <label class="control-label"><?php echo $lead->getEnrollDesireDateDiff(); ?> ngày</label>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="enroll-info">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <div class="col-sm-8 text-right">
                                            <label for="" class="control-label"><b style="color: #ffc107">(WL) Đặt cọc giữ chỗ</b></label>
                                        </div>
                                        <div class="col-sm-4 radio">
                                            <?php if($lead->activated){ ?>
                                            <b>Hoàn thành</b>
                                            <?php }else{ ?>
                                            {{ Form::checkbox('activated', 1) }}
                                            <?php }?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-8 text-right">
                                            <label for="" class="control-label"><b style="color: #6f42c1">(NS) Hoàn thành nghĩa vụ tài chính</b></label>
                                        </div>
                                        <div class="col-sm-4 radio">
                                            <?php if($lead->new_sale){ ?>
                                            <b>Hoàn thành</b>
                                            <?php }else{ ?>
                                            {{ Form::checkbox('new_sale', 1) }}
                                            <?php }?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-8 text-right">
                                            <label for="" class="control-label"><b style="color: #dc3545">(NE) Nhập học</b></label>
                                        </div>
                                        <div class="col-sm-4 radio">
                                            <?php if($lead->enrolled){ ?>
                                            <b>Hoàn thành</b>
                                            <?php }else{ ?>
                                            {{ Form::checkbox('enrolled', 1) }}
                                            <?php }?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-8 text-right">
                                            <label for="" class="control-label"><b style="color: #26B99A">(NP) Ủng hộ</b></label>
                                        </div>
                                        <div class="col-sm-4 radio">
                                            <?php if($lead->new_promoter){ ?>
                                            <b>Hoàn thành</b>
                                            <?php }else{ ?>
                                            {{ Form::checkbox('new_promoter', 1) }}
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="waitlist_class" class="control-label">Nhóm lớp giữ chỗ</label>
                                        </div>
                                        <div class="col-sm-8">
                                            {{ Form::select('waitlist_class',
                                                \App\OfficialClass::listData(),
                                                !empty(old('waitlist_class'))?old('waitlist_class'):$lead->waitlist_class,
                                                ['id' => 'waitlist_class', 'placeholder' => 'Chọn nhóm lớp giữ chỗ', 'class' => 'form-control']
                                            )}}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="learning_system_waitlist" class="control-label">Hệ học giữ chỗ</label>
                                        </div>
                                        <div class="col-sm-8">
                                            {{ Form::select('learning_system_waitlist',
                                                \App\LearningSystemOfficial::listData(),
                                                !empty(old('learning_system_waitlist'))?old('learning_system_waitlist'):$lead->learning_system_waitlist,
                                                ['id' => 'learning_system_waitlist', 'placeholder' => 'Chọn hệ học giữ chỗ', 'class' => 'form-control']
                                            )}}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="status_waitlist_number" class="control-label">Số phiếu thu tiền/báo có giữ chỗ</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="status_waitlist_number" name="status_waitlist_number" value="{{ !empty(old('status_waitlist_number'))?old('status_waitlist_number'):$lead->status_waitlist_number }}">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="receipt_number" class="control-label">Số biên lai/báo có nhập học</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input id="receipt_number" type="text" name="receipt_number" value="{{ !empty(old('receipt_number'))?old('receipt_number'):$lead->receipt_number }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="student_code" class="control-label">Mã học sinh</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="student_code" name="student_code" value="{{ !empty(old('student_code'))?old('student_code'):$lead->student_code }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="class_name" class="control-label">Tên lớp nhập học chính thức</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="class_name" name="class_name" value="{{ !empty(old('class_name'))?old('class_name'):$lead->class_name }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="official_class" class="control-label">Nhóm lớp chính thức</label>
                                        </div>
                                        <div class="col-sm-8">
                                            {{ Form::select('official_class',
                                                \App\OfficialClass::listData(),
                                                !empty(old('official_class'))?old('official_class'):$lead->official_class,
                                                ['id' => 'official_class', 'placeholder' => 'Chọn nhóm lớp chính thức', 'class' => 'form-control']
                                            )}}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="learning_system_official" class="control-label">Hệ học chính thức</label>
                                        </div>
                                        <div class="col-sm-8">
                                            {{ Form::select('learning_system_official',
                                                \App\LearningSystemOfficial::listData(),
                                                !empty(old('learning_system_official'))?old('learning_system_official'):$lead->learning_system_official,
                                                ['id' => 'learning_system_official', 'placeholder' => 'Chọn hệ học chính thức', 'class' => 'form-control']
                                            )}}
                                        </div>
                                    </div>
                                </div>

                                <?php if(in_array($lead->status, array(15, 16))) { ?>
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <div class="col-sm-8 text-right">
                                                    <label for="" class="control-label"><b style="color: #dc3545">HS thôi học</b></label>
                                                </div>
                                                <div class="col-sm-4 radio">
                                                    <?php if($lead->leave_school){ ?>
                                                    <b>Đã thôi học</b>
                                                    <?php }else{ ?>
                                                    {{ Form::checkbox('leave_school', 1) }}
                                                    <?php }?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <div class="col-sm-4 text-right">
                                                    <label for="leave_school_reason" class="control-label">Lý do thôi học</label>
                                                </div>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="leave_school_reason" name="leave_school_reason" value="{{ !empty(old('leave_school_reason'))?old('leave_school_reason'):$lead->leave_school_reason }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>

                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="guest-info">
                        <div class="panel">
                            <div class="panel-body">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="guest_class_name" class="control-label">Lớp học CT Bé làm khách</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="guest_class_name" name="guest_class_name" value="{{ !empty(old('guest_class_name'))?old('guest_class_name'):$lead->guest_class_name }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="guest_class" class="control-label">Nhóm lớp CT bé làm khách</label>
                                        </div>
                                        <div class="col-sm-8">
                                            {{ Form::select('guest_class',
                                                \App\GuestClass::listData(),
                                                !empty(old('guest_class'))?old('guest_class'):$lead->guest_class,
                                                ['id' => 'guest_class', 'placeholder' => 'Chọn nhóm lớp CT bé làm khách', 'class' => 'form-control']
                                            )}}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="learning_system_guest" class="control-label">Hệ học CT Bé làm khách</label>
                                        </div>
                                        <div class="col-sm-8">
                                            {{ Form::select('learning_system_guest',
                                                \App\LearningSystemGuest::listData(),
                                                !empty(old('learning_system_guest'))?old('learning_system_guest'):$lead->learning_system_guest,
                                                ['id' => 'learning_system_guest', 'placeholder' => 'Chọn hệ học CT Bé làm khách', 'class' => 'form-control']
                                            )}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="status_guest_number" class="control-label">Số phiếu thu tiền/báo có CT bé làm khách</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="status_guest_number" name="status_guest_number" value="{{ !empty(old('status_guest_number'))?old('status_guest_number'):$lead->status_guest_number }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <div class="col-sm-4 text-right">
                                            <label for="" class="control-label">Ngày chuyển trạng thái làm khách</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <p class="form-control-static">
                                                @if(!empty($lead->join_as_guest_at))
                                                    {{ \Carbon\Carbon::parse($lead->join_as_guest_at)->format('d/m/Y H:i:s') }}
                                                @else
                                                    Chưa xác định
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="history-info">
                        <div class="panel">
                            <div class="panel-body">
                                @include('includes.call_log')
                            </div>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane fade" id="complain-info">
                        <div class="panel">
                            <div class="panel-body">
                                @include('includes.lead.complain')
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-sm-3">
            <div class="lead-log-tabs">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a class="nav-link" data-toggle="pill" role="tab"  href="#log-info">Người phụ trách - Lưu vết</a>
                    </li>
                </ul>
                <div class="tab-content" role="tabpanel">
                    <div role="tabpanel" class="tab-pane fade in active" id="log-info">
                        <div class="panel">
                            <div class="panel-body">

                                <div class="form-group">
                                    <div class="col-sm-6 text-right">
                                        <label for="" class="control-label">Người phụ trách</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="form-control-static"><strong>{!! $lead->assignee_list !!}</strong></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6 text-right">
                                        <label for="" class="control-label">Người phụ trách trước đó</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="form-control-static"><strong>{!! $lead->getRelateAssignee() !!}</strong></p>
                                    </div>
                                </div>
                                <div class="form-group">
                                        <div class="col-sm-6 text-right">
                                        <label for="" class="control-label">Ngày tạo</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="form-control-static">{{ Carbon\Carbon::parse($lead->created_at)->format('d/m/Y H:i:s') }}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6 text-right">
                                        <label for="" class="control-label">Qualified</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="form-control-static">{{ !empty($lead->qualified_at) ? Carbon\Carbon::parse($lead->qualified_at)->format('d/m/Y H:i:s') : '' }}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6 text-right">
                                        <label for="" class="control-label">Ngày ShowUp Đến trường</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="form-control-static">
                                            @if(!empty($lead->showup_school_at))
                                                {{ \Carbon\Carbon::parse($lead->showup_school_at)->format('d/m/Y H:i:s') }}
                                            @else
                                                Chưa xác định
                                            @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6 text-right">
                                        <label for="" class="control-label">Ngày đặt cọc giữ chỗ/waiting list</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="form-control-static">
                                            @if(!empty($lead->activated_at))
                                                <strong>{{ \Carbon\Carbon::parse($lead->activated_at)->format('d/m/Y H:i:s') }}</strong>
                                            @else
                                                <span class="label label-danger"><strong>Chưa xác định</strong></span>
                                            @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6 text-right">
                                        <label for="" class="control-label">Ngày hoàn thành nghĩa vụ tài chính</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="form-control-static">
                                            @if(!empty($lead->new_sale_at))
                                                <strong>{{ \Carbon\Carbon::parse($lead->new_sale_at)->format('d/m/Y H:i:s') }}</strong>
                                            @else
                                                <span class="label label-danger"><strong>Chưa xác định</strong></span>
                                            @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-6 text-right">
                                        <label for="" class="control-label">Ngày nhập học</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="form-control-static">
                                            @if(!empty($lead->enrolled_at))
                                                <strong>{{ \Carbon\Carbon::parse($lead->enrolled_at)->format('d/m/Y H:i:s') }}</strong>
                                            @else
                                                <span class="label label-danger"><strong>Chưa nhập học</strong></span>
                                            @endif
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="form-group visible-xs">
        @if(
        (Auth::user()->username == trim($lead->assignee_list) && empty($lead->leave_school))
        ||
        (Auth::user()->hasRole('manager') && in_array($lead->department_id, Auth::user()->getManageDepartments()))
        ||
        (Auth::user()->hasRole('carer') && in_array($lead->department_id, Auth::user()->getManageDepartments()) && $lead->status == \App\LeadStatus::STATUS_ENROLL)
        )
        <div class="col-md-1">
            <button type="submit" class="btn btn-edufit-default form-control" name="lead_save_button" value="save">
                Lưu
            </button>
        </div>
        @endif
        <div class="col-md-1">
            <button type="button" class="btn btn-edufit-danger form-control" name="close_button" value="close"
                    onclick="window.close()">Đóng
            </button>
        </div>

    </div>
</div>
{!! Form::close() !!}
<div class="col-md-12">
    <hr size="30">
    {{-- @include('includes.changelog')  --}}
</div>

<script src="/js/filter.js"></script>
<script>
    $(document).ready(function () {
        $('#province').select2();
        $('#district').select2();
        $('#school').select2();
        $('#enroll_desire_at').datepicker();

        $("#province").on('select2:select', function (e) {
            var province = $(this).val();
            $.ajax({
                url: '<?php echo env('APP_URL')?>/location/getDistrictByProvince',
                type: 'POST',
                dataType: 'html',
                data: {
                    _token : '{{ csrf_token() }}',
                    province : province
                },
                success: function(result){
                    $('#district').html(result);
                    $('#district').select2('val','');
                }
            });
        });

        $('#school').on('select2:select', function (e){
            if($(this).val() == '<?php echo \App\Utils::OTHER_OPTION?>'){
                $('#other_school').removeClass('hidden');
                $('#other_school').focus();
            }else{
                $('#other_school').addClass('hidden');
            }
        });

        $("input[name='objective[]'][value='<?php echo \App\Objective::OTHER_OPTION?>']").change(function() {
            if(this.checked) {
                $("#objective_others").removeClass('hidden');
                $("#objective_others").focus();
            }else{
                $("#objective_others").addClass('hidden');
            }
        });

        $("input[name='parents_concern[]'][value='<?php echo \App\ParentsConcern::OTHER_OPTION?>']").change(function() {
            if(this.checked) {
                $("#parents_concern_others").removeClass('hidden');
                $("#parents_concern_others").focus();
            }else{
                $("#parents_concern_others").addClass('hidden');
            }
        });

        $("input[name='student_challenge[]'][value='<?php echo \App\StudentChallenge::OTHER_OPTION?>']").change(function() {
            if(this.checked) {
                $("#student_challenge_others").removeClass('hidden');
                $("#student_challenge_others").focus();
            }else{
                $("#student_challenge_others").addClass('hidden');
            }
        });

        $("#call_status_id").on('change', function(e) {
            if($(this).val() == '<?php echo \App\CallStatus::STATUS_NO_CONCERN?>'){
                $('#call_status_no_concern_reason').removeClass('hidden');
            }else{
                $('#call_status_no_concern_reason').addClass('hidden');
            }
        });

        $('input[name="activated"]').click(function(e){
            if($(this).is(":checked")){
                if(!confirm('Bạn có chắc chắn lead đã hoàn thành đặt cọc giữ chỗ (WL) ?')){
                    e.preventDefault();
                }
            }
        });

        $('input[name="new_sale"]').click(function(e){
            if($(this).is(":checked")){
                if(!confirm('Bạn có chắc chắn lead đã hoàn thành nghĩa vụ tài chính (NS) ?')){
                    e.preventDefault();
                }
            }
        });

        $('input[name="enrolled"]').click(function(e){
            if($(this).is(":checked")){
                if(!confirm('Bạn có chắc chắn lead đã nhập học (NE) ?')){
                    e.preventDefault();
                }
            }
        });


    });
</script>

@stop