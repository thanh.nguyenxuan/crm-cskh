@extends('layouts.main')

@section('content')

<div class="col-md-12">
    <h4>Tạo lead mới</h4>
    @if(!empty(session('success')) && !empty(session('return_message')))
        <div class="alert alert-success">
            {{ session('return_message') }}
        </div>
    @endif
    @if(empty(session('success')) && !empty(session('return_message')))
        <div class="alert alert-danger">
            {{ session('return_message') }}
        </div>
    @endif
    {!! Form::model($lead, [
        'method' => 'POST',
        'route' => ['lead.update', $lead->id],
        'class' =>'form-horizontal',
    ]) !!}

    <div class="panel panel-edufit-default">
        <div class="panel-heading">
            <h3 class="panel-title">Thông tin liên hệ</h3>
        </div>
        <div class="panel-body">
            <div class="col-sm-4">
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="name" class="control-label required">Họ tên phụ huynh</label>
                    </div>
                    <div class="col-sm-7">
                        <input id="name" type="text" name="name" value="{{ old('name') }}" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="phone1" class="control-label required">SĐT 1</label>
                    </div>
                    <div class="col-sm-7">
                        <input id="phone1" type="text" name="phone1" value="{{ old('phone1') }}" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="phone2" class="control-label">SĐT 2 &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        <input id="phone2" type="text" name="phone2" value="{{ old('phone2') }}" class="form-control">
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="email" class="control-label">Email 1 &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        <input id="email" type="email" name="email" value="{{ old('email') }}" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="email_2" class="control-label">Email 2 &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        <input id="email_2" type="email" name="email_2" value="{{ old('email_2') }}" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="facebook" class="control-label">Facebook &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        <input id="facebook" type="text" name="facebook" value="{{ old('facebook') }}" class="form-control">
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="" class="control-label">Tỉnh/Thành phố &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        <select id="province" name="province_id" class="form-control">
                        <?php echo \App\Utils::generateOptions(\App\Province::listData(), old('province_id'), 'Chọn tỉnh/thành phố') ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="district" class="control-label">Quận/Huyện &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        <select id="district" name="district" class="form-control">
                            <?php echo \App\Utils::generateOptions(!empty(old('province_id')) ? \App\District::listData(old('province_id')) : array(), old('district'), 'Chọn quận/huyện') ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="address" class="control-label">Địa chỉ &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        <textarea id="address" name="address" class="form-control"><?php echo !empty(old('address') ? old('address') : '')?></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-edufit-default">
        <div class="panel-heading">
            <h3 class="panel-title">Thông tin cơ bản</h3>
        </div>
        <div class="panel-body">
            <div class="col-sm-4">
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="student_name" class="control-label required">Họ tên học sinh</label>
                    </div>
                    <div class="col-sm-7">
                        <input id="student_name" type="text" name="student_name" value="{{ old('student_name') }}" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="nick_name" class="control-label">Nick name &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        <input id="nick_name" type="text" name="nick_name" value="{{ old('nick_name') }}" class="form-control">
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="birthdate" class="control-label">Ngày sinh &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        <input id="birthdate" type="text" name="birthdate" value="{{ old('birthdate') }}" class="form-control" placeholder="dd/mm/yyyy">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="" class="control-label">Giới tính &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        <div class="radio" style="padding-left: 20px">
                            {{ Form::radio('gender', 'm') }} Nam
                            &emsp;&emsp;
                            {{ Form::radio('gender', 'f') }} Nữ
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="school" class="control-label">Trường &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        <select id="school" name="school" class="form-control">
                            <?php echo \App\Utils::generateOptions(\App\School::listData(), old('school'), 'Chọn trường', true) ?>
                        </select>
                        <input id="other_school" type="text" name="other_school" value="{{ old('other_school') }}" class="form-control<?php echo (old('school') == \App\Utils::OTHER_OPTION) ? '' : ' hidden'?>" style="margin-top: 5px" placeholder="Tên trường đang học">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="twins" class="control-label">Nhiều con &emsp;</label>
                    </div>
                    <div class="col-sm-7 radio">
                        {{ Form::checkbox('twins', 1) }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-edufit-default">
        <div class="panel-heading">
            <h3 class="panel-title">Hành động</h3>
        </div>
        <div class="panel-body">
            <div class="col-sm-4">
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="" class="control-label">Mục tiêu dài hạn &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        @foreach($objective_list as $objective)
                            <div>{{ Form::checkbox('objective[]', $objective->id) }} {{ $objective->name }}</div>
                        @endforeach

                        <input id="objective_others" type="text" name="objective_others" value="{{ old('objective_others') }}" class="form-control<?php echo !empty(old('objective')) ? (in_array(\App\Objective::OTHER_OPTION, old('objective')) ? '' : ' hidden') : ' hidden'?>" style="margin-top: 5px" placeholder="">
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="" class="control-label">Quan tâm của phụ huynh &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        @foreach($parents_concern_list as $concern)
                            <div>{{ Form::checkbox('parents_concern[]', $concern->id) }} {{ $concern->name }}</div>
                        @endforeach

                        <input id="parents_concern_others" type="text" name="parents_concern_others" value="{{ old('parents_concern_others') }}" class="form-control<?php echo !empty(old('parents_concern')) ? (in_array(\App\ParentsConcern::OTHER_OPTION, old('parents_concern')) ? '' : ' hidden') : ' hidden'?>" style="margin-top: 5px" placeholder="">
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="" class="control-label">Mục tiêu của học sinh &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        @foreach($student_challenge_list as $challenge)
                            <div>{{ Form::checkbox('student_challenge[]', $challenge->id) }} {{ $challenge->name }}</div>
                        @endforeach

                        <input id="student_challenge_others" type="text" name="student_challenge_others" value="{{ old('student_challenge_others') }}" class="form-control<?php echo !empty(old('student_challenge')) ? (in_array(\App\StudentChallenge::OTHER_OPTION, old('student_challenge')) ? '' : ' hidden') : ' hidden'?>" style="margin-top: 5px" placeholder="">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-edufit-default">
        <div class="panel-heading">
            <h3 class="panel-title">Thông tin khác</h3>
        </div>
        <div class="panel-body">
            <div class="col-sm-4">
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="status" class="control-label required">Trạng thái Phụ huynh</label>
                    </div>
                    <div class="col-sm-7">
                        <select id="status" name="status" class="form-control">
                            {!! $lead->generateOptionStatus($status_list, old('status'), 'Chọn trạng thái') !!}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="source" class="control-label required">Nguồn dẫn</label>
                    </div>
                    <div class="col-sm-7">
                        {{ Form::select('source', $source_list, old('source'), ['id' => 'source', 'placeholder' => 'Chọn nguồn dẫn', 'class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="info_source" class="control-label">Nguồn thông tin &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        {{ Form::select('info_source', $info_source_list, !empty(old('info_source'))?old('info_source'):$lead->info_source, ['id' => 'info_source', 'placeholder' => 'Chọn nguồn thông tin', 'class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="learning_system_desire" class="control-label">Loại Lead &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        {{ Form::select('lead_type',
                            \App\LeadType::listData(),
                            old('lead_type'),
                            ['id' => 'lead_type', 'placeholder' => 'Chọn loại', 'class' => 'form-control']
                        )}}
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <?php if(count(Auth::user()->getManageDepartments()) > 1){?>
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="department_id" class="control-label required">Cơ sở</label>
                    </div>
                    <div class="col-sm-7">
                        {{ Form::select('department_id', Auth::user()->getManageDepartmentsListData(), old('department_id'), ['id' => 'department_id', 'placeholder' => 'Chọn cơ sở', 'class' => 'form-control']) }}
                    </div>
                </div>
                <?php } ?>

                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="channel" class="control-label">Kênh quảng cáo &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        {{ Form::select('channel', $channel_list, !empty(old('channel'))?old('channel'):$lead->channel, ['id' => 'channel', 'placeholder' => 'Chọn kênh quảng cáo', 'class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="utm_campaign" class="control-label">Chiến dịch QC &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        <input type="text" class="form-control" id="utm_campaign" name="utm_campaign" value="{{ $lead->utm_campaign }}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="promotion" class="control-label">Promotion &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        {{ Form::select('promotion', $promotion_list, !empty(old('promotion'))?old('promotion'):$lead->promotion, ['id' => 'promotion', 'placeholder' => 'Chọn promotion', 'class' => 'form-control']) }}
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="rating" class="control-label">Mức độ quan tâm &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        {{ Form::select('rating', $rating_list, old('rating'), ['id' => 'rating', 'placeholder' => 'Chọn mức độ quan tâm', 'class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="major" class="control-label">Lớp học quan tâm &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        {{ Form::select('major', $major_list, old('major'), ['id' => 'major', 'placeholder' => 'Chọn lớp học quan tâm', 'class' => 'form-control']) }}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="learning_system_desire" class="control-label">Hệ học quan tâm &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        {{ Form::select('learning_system_desire',
                            \App\LearningSystemDesire::listData(),
                            old('learning_system_desire'),
                            ['id' => 'learning_system_desire', 'placeholder' => 'Chọn hệ học quan tâm', 'class' => 'form-control']
                        )}}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-5 text-right no_pad_right">
                        <label for="enroll_desire_at" class="control-label">Thời gian học dự kiến &emsp;</label>
                    </div>
                    <div class="col-sm-7">
                        <input id="enroll_desire_at" type="text" name="enroll_desire_at" value="{{ old('enroll_desire_at') }}" class="form-control" placeholder="dd/mm/yyyy">
                    </div>
                </div>
            </div>
            <div class="col-sm-12 no_pad">
                <div class="col-sm-8">
                    <div class="form-group">
                        <div class="col-sm-2 text-right no_pad_right" style="width: 20.66667%">
                            <label for="notes" class="control-label">Ghi chú &emsp;</label>
                        </div>
                        <div class="col-sm-9" style="width: 79.33333%">
                            <textarea class="form-control" id="notes" name="notes" cols="50" rows="6">{{ old('notes') }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-bottom: 20px">
        <button type="submit" class="btn btn-edufit-default" name="save_button">Lưu</button>
    </div>
</div>
{!! Form::close() !!}
<script src="/js/filter.js"></script>
<script>
    $(document).ready(function () {
        $('#province').select2();
        $('#district').select2();
        $('#school').select2();
        $('#enroll_desire_at').datepicker();

        $("#province").on('select2:select', function (e) {
            var province = $(this).val();
            $.ajax({
                url: '<?php echo env('APP_URL')?>/location/getDistrictByProvince',
                type: 'POST',
                dataType: 'html',
                data: {
                    _token : '{{ csrf_token() }}',
                    province : province
                },
                success: function(result){
                    $('#district').html(result);
                    $('#district').select2('val','');
                }
            });
        });

        $('#school').on('select2:select', function (e){
            if($(this).val() == '<?php echo \App\Utils::OTHER_OPTION?>'){
                $('#other_school').removeClass('hidden');
                $('#other_school').focus();
            }else{
                $('#other_school').addClass('hidden');
            }
        });

        $("input[name='objective[]'][value='<?php echo \App\Objective::OTHER_OPTION?>']").change(function() {
            if(this.checked) {
                $("#objective_others").removeClass('hidden');
                $("#objective_others").focus();
            }else{
                $("#objective_others").addClass('hidden');
            }
        });

        $("input[name='parents_concern[]'][value='<?php echo \App\ParentsConcern::OTHER_OPTION?>']").change(function() {
            if(this.checked) {
                $("#parents_concern_others").removeClass('hidden');
                $("#parents_concern_others").focus();
            }else{
                $("#parents_concern_others").addClass('hidden');
            }
        });

        $("input[name='student_challenge[]'][value='<?php echo \App\StudentChallenge::OTHER_OPTION?>']").change(function() {
            if(this.checked) {
                $("#student_challenge_others").removeClass('hidden');
                $("#student_challenge_others").focus();
            }else{
                $("#student_challenge_others").addClass('hidden');
            }
        });
    });

</script>

@stop
