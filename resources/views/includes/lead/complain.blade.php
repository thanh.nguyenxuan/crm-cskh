<?php
$lead_complain_list = \App\LeadComplain::where('lead_id', $lead->id)->orderBy('id','DESC')->get();
$complainReasonListData = \App\ComplainReason::listData();
$stt = 0;
?>

@if(!empty($lead_complain_list))
    <table id="table-complain" class="table table-condensed table-bordered">
        <thead>
        <th>STT</th>
        <th>Lý do</th>
        <th>Ghi chú</th>
        <th>Người tạo</th>
        <th>Ngày tạo</th>
        <th>Trạng thái</th>
        <th>#</th>
        </thead>
        <tbody>
        @foreach($lead_complain_list as $lead_complain)
            <tr data-id="{{ $lead_complain->id }}">
                <td>{{ ++$stt }}</td>
                <td>{{ Form::select('reason', \App\ComplainReason::listData(), $lead_complain->complain_id, ['class' => 'form-control']) }}</td>
                <td><textarea name="note" class="form-control">{{ $lead_complain->note }}</textarea></td>
                <td>{{ $user_list[$lead_complain->created_by] }}</td>
                <td>{{ Carbon\Carbon::parse($lead_complain->created_at)->format('d/m/Y H:i:s') }}</td>
                <td>{{ Form::select('status', \App\LeadComplain::statusListData(), $lead_complain->status, ['class' => 'form-control']) }}</td>
                <td>
                    <a class="btn btn-primary btnSave" onclick="updateComplain({{$lead_complain->id}})">Lưu</a>
                    <span class="loading spin"><i class="fa fa-spinner"></i></span>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <div class="alert alert-info">Không có dữ liệu!</div>
@endif

<script>
    function updateComplain(id)
    {
        var container = $('#table-complain tr[data-id='+id+']');
        var reason = container.find('select[name="reason"]').val();
        var note = container.find('textarea[name="note"]').val();
        var status = container.find('select[name="status"]').val();
        var loading = container.find('span.loading');
        var btnSave = container.find('a.btnSave');

        if(btnSave.hasClass('disabled')){
            return;
        }
        loading.show();
        btnSave.addClass('disabled');
        $.ajax({
            url : '{{ env('APP_URL') }}/lead/updateComplain',
            type : 'POST',
            dataType: 'json',
            data : {
                _token : '{{ csrf_token() }}',
                complain_id : id,
                reason : reason,
                note : note,
                status : status
            },
            success: function(result) {
                loading.hide();
                btnSave.removeClass('disabled');
                if(!result.success){
                    alert("Lưu dữ liệu thất bại");
                }
            }
        });

    }
</script>