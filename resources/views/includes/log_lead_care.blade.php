<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 24/11/2017
 * Time: 9:37 AM
 */
?>
<!-- Modal -->
<div class="modal fade" id="log_lead_care_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     data-keyboard="false" data-backdrop="static">
    <form method="post" id="log_lead_care_form">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Ghi chú chăm sóc</h4>
                </div>
                <div class="modal-body">
                    <div class="hidden alert alert-success">Lưu dữ liệu thành công! </div>
                    <div class="hidden alert alert-danger">Đã có lỗi trong quá trình lưu dữ liệu!</div>

                    <div class="form-group">
                        <label for="log_lead_care_channel" class="required">Kênh:</label>
                        <select id="log_lead_care_channel" name="log_lead_care_channel" class="form-control" required>
                            <?php echo \App\Utils::generateOptions(\App\LogLeadCare::channelListData(), null, 'Chọn')?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="log_lead_care_content" class="required">Nội dung:</label>
                        <textarea class="form-control" id="log_lead_care_content" name="log_lead_care_content" rows="5" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="log_lead_care_btn-save" class="btn btn-primary">Lưu</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script>

    $(function () {

        $('#log_lead_care_btn').click(function () {
            $('#log_lead_care_modal .alert-success').addClass('hidden');
            $('#log_lead_care_modal .alert-error').addClass('hidden');
            $('#log_lead_care_channel').val('');
            $('#log_lead_care_content').val('');

            $("#log_lead_care_modal").modal('show');
            $('#log_lead_care_btn-save').removeClass('disabled');
        });

        $('#log_lead_care_form').on('submit', function (e) {

            e.preventDefault();
            if($('#log_lead_care_btn-save').hasClass('disabled')){
                return false;
            }

            var channel = $(this).find('#log_lead_care_channel').val();
            var content = $(this).find('#log_lead_care_content').val();

            $.ajax({
                url: '{{ route('lead.createLogLeadCare') }}',
                type: 'POST',
                dataType: 'json',
                data: {
                    _token : '{{ csrf_token() }}',
                    lead_id: '<?php echo $lead->id?>',
                    channel: channel,
                    log_content: content
                },
                success: function(result){
                    if(result.success){
                        $('#log_lead_care_modal .alert-success').removeClass('hidden');

                        var html = '<tr>' +
                            '<td>*</td>' +
                            '<td>'+result.data.content+'</td>' +
                            '<td>'+result.data.channel+'</td>' +
                            '<td>'+result.data.created_at+'</td>' +
                            '<td>'+result.data.created_by+'</td>' +
                            '</tr>';

                        $("#lead_care_history_pane table tbody").prepend(html);
                        $('#log_lead_care_btn-save').addClass('disabled');
                    }else{
                        $('#log_lead_care_modal .alert-error').removeClass('hidden');
                    }
                }
            });

            return false;
        });

    });
</script>