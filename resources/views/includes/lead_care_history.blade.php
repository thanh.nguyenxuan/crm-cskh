<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 27/04/2018
 * Time: 2:21 PM
 */
?>
<div role="tabpanel" class="tab-pane fade" id="lead_care_history_pane">
    @if(!empty($call_history_list))
        <table class="table table-condensed table-bordered">
            <thead>
            <th>STT</th>
            <th>Nội dung</th>
            <th>Kênh</th>
            <th>Ngày tạo</th>
            <th>Người tạo</th>
            </thead>
            <tbody>
            @php $stt = 0 @endphp
            @php $logLeadCareChannelList = \App\LogLeadCare::channelListData() @endphp
            @foreach($log_lead_care_list as $log_lead_care)
                <tr>
                    <td>{{ ++$stt }}</td>
                    <td>{{ $log_lead_care->content }}</td>
                    <td>{{ $logLeadCareChannelList[$log_lead_care->channel] }}</td>
                    <td>{{ date('d/m/Y H:i:s', strtotime($log_lead_care->created_at)) }}</td>
                    <td>{{ $user_list[$log_lead_care->created_by] }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <div class="alert alert-info">Không có dữ liệu!</div>
    @endif
</div>
