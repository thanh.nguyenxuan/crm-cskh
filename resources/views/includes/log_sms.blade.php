<div role="tabpanel" class="tab-pane" id="log_sms">
    @if(!empty($log_sms_list))
        <table class="table table-condensed table-bordered">
            <thead>
            <th>STT</th>
            <th>BrandName</th>
            <th>Target</th>
            <th>Nội dung</th>
            <th>Trạng thái</th>
            <th>Thời gian gửi</th>
            <th>Thời gian tạo</th>
            <th>Người tạo</th>
            </thead>
            <tbody>
            @php
                $stt = 0;
                $all_status = \App\LogSms::getAllStatus();
            @endphp
            @foreach($log_sms_list as $log_sms)
                <tr>
                    <td>{{ ++$stt }}</td>
                    <td>{{ $log_sms->brand_name }}</td>
                    <td>{{ $log_sms->target }}</td>
                    <td>{{ $log_sms->content }}</td>
                    <td>{{ $all_status[$log_sms->status] }}</td>
                    <td>{{ !empty($log_sms->send_at) ? Carbon\Carbon::parse($log_sms->send_at)->format('d/m/Y H:i:s') : '' }}</td>
                    <td>{{ Carbon\Carbon::parse($log_sms->created_at)->format('d/m/Y H:i:s') }}</td>
                    <td>
                        {!! (!empty($log_sms->created_by) && isset($user_list[$log_sms->created_by])) ? $user_list[$log_sms->created_by] : '' !!}
                        {!! (!empty($log_sms->original_user_id) && isset($user_list[$log_sms->original_user_id])) ? '('.$user_list[$log_sms->original_user_id].')' : '' !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <div class="alert alert-info">Không có dữ liệu!</div>
    @endif
</div>