<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 19/12/2017
 * Time: 12:31 PM
 */
?>
<!-- Modal -->
<div class="modal fade" id="lead-assignment-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     data-keyboard="true" data-backdrop="static">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Phân công chăm sóc lead</h4>
            </div>
            <div class="modal-body">
                <div>Bạn đã chọn <strong><span id="selected_count">0</span></strong> lead.</div>
                <form id="assignment_form">
                    <div class="form-group">
                        <label for="assignee_list">Phân công cho: </label>
                        {{ Form::select('assignee_list', $assignee_tool_list, old('assignee_list'), ['id' => 'assignee_list']) }}
                    </div>

                    <?php if(count(Auth::user()->getManageDepartments()) > 1){ ?>
                    <div class="form-group">
                        <label for="department_list">Chuyển sang cơ sở: </label>
                        {{ Form::select('department_list', Auth::user()->getManageDepartmentsListData(), old('department_list'), ['id' => 'department_list', 'placeholder' => 'Chọn']) }}
                    </div>
                    <?php } ?>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="assign_button" class="btn btn-edufit-default">Phân công</button>
            </div>
        </div>
    </div>
</div>