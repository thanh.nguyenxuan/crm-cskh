<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 27/04/2018
 * Time: 2:21 PM
 */
?>
<div role="tabpanel" class="tab-pane active fade in" id="call_history_pane">
    @if(!empty($call_history_list))
        <table class="table table-condensed table-bordered">
            <thead>
            <th>STT</th>
            <th>Chiều cuộc gọi</th>
            <th>Trạng thái</th>
            <th>Nội dung chăm sóc</th>
            <th>Người chăm sóc</th>
            <th>Thời điểm chăm sóc</th>
            <th>Thời lượng cuộc gọi</th>
            <th>File ghi âm</th>
            <th> # </th>
            </thead>
            <tbody>
            @php $stt = 0 @endphp
            @foreach($call_history_list as $call)
                <tr>
                    <td>{{ ++$stt }}</td>
                    <td>{{ $call->direction==1?'Gọi đi':'Gọi đến' }}</td>
                    <td>{{ $call->status?$call_log_status_list[$call->status]:'' }}</td>
                    <td>{{ $call->notes }}</td>
                    <td>{{ (!empty($call->created_by) && App\User::find($call->created_by))?App\User::find($call->created_by)->username:''}}</td>
                    <td>{{ Carbon\Carbon::parse($call->created_at)->format('d/m/Y H:i:s') }}</td>

                    <?php if(!empty($call->data)){
                        $call_data = json_decode($call->data);
                        $download_url = !empty($call->file_url) ? $call->file_url : $call_data->download_url;
                        $recording_file = !empty($call->file_url) ? $call->file_url : $call_data->recordingfile;
                        if(!empty($call_data->talk_time) && ($call_data->talk_time > 0) && !empty($call_data->recordingfile)){?>
                            <td><?php echo isset($call_data->talk_time) ? \App\Utils::prettyTime($call_data->talk_time) : ''?></td>
                            <td>
                                <div>
                                    <?php if(date('Y-m-d', strtotime($call->created_at)) >= '2020-12-22'){?>
                                        <a onclick="loadRecordingFileV2($(this).closest('div'),'{!! $download_url !!}')"><i class="fa fa-file-audio-o"></i></a>
                                    <?php }else{ ?>
                                        <a onclick="loadRecordingFile($(this).closest('div'),'{!! $recording_file !!}')"><i class="fa fa-file-audio-o"></i></a>
                                    <?php } ?>
                                    File ghi âm
                                </div>
                            </td>
                            <td>
                                <div>
                                    <a onclick="downloadRecordingFile('{!! $download_url !!}')"><i class="fa fa-download"></i></a>
                                </div>
                            </td>
                        <?php }else{?>
                            <td><?php echo isset($call_data->talk_time) ? \App\Utils::prettyTime($call_data->talk_time) : ''?></td>
                            <td colspan="2">Không có file ghi âm</td>
                        <?php }
                        }else{ ?>
                        <td></td>
                        <td colspan="2">Không có file ghi âm</td>
                    <?php }?>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <div class="alert alert-info">Không có dữ liệu!</div>
    @endif
</div>

<script>
    function loadRecordingFile(container, url)
    {
        $(container).html('<iframe width="240px" height="40px"  src="'+url+'"></iframe>');
    }

    function loadRecordingFileV2(container, url)
    {
        var audioHtml = '<audio controls>' +
            '<source src="'+url+'" type="audio/wav">' +
            '</audio>';
        $(container).html(audioHtml);
    }

    function downloadRecordingFile(url)
    {
        window.open(url, '_blank');
    }
</script>
