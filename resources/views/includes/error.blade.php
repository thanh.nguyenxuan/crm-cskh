<script>
    $(document).ready(function(){
        var action_url = window.location.pathname;
        var error_code = 'MANAGE_LEAD';

        if(action_url.indexOf('lead/import') > 0){
            error_code = 'IMPORT_LEAD'
        }else if(action_url.indexOf('user') > 0){
            error_code = 'MANAGE_USER'
        }else if(action_url.indexOf('promotion') > 0){
            error_code = 'MANAGE_PROMOTION'
        }else if(action_url.indexOf('info_source') > 0){
            error_code = 'MANAGE_INFO_SOURCE'
        }else if(action_url.indexOf('source') > 0){
            error_code = 'MANAGE_SOURCE'
        }else if(action_url.indexOf('channel') > 0){
            error_code = 'MANAGE_CHANNEL'
        }else if(action_url.indexOf('campaign') > 0){
            error_code = 'MANAGE_CAMPAIGN'
        }else if(action_url.indexOf('department') > 0){
            error_code = 'MANAGE_DEPARTMENT'
        }else if(action_url.indexOf('class') > 0){
            error_code = 'MANAGE_CLASS'
        }else if(action_url.indexOf('school') > 0){
            error_code = 'MANAGE_SCHOOL'
        }else if(action_url.indexOf('cdr') > 0){
            error_code = 'DETAIL_CALL'
        }else if(action_url.indexOf('missed_calls') > 0){
            error_code = 'MISSED_CALL'
        }else if(action_url.indexOf('alarm') > 0){
            error_code = 'CARE_SCHEDULE'
        }else if(action_url.indexOf('calls') > 0){
            error_code = 'CARE_HISTORY'
        }else if(action_url.indexOf('report/call_log') > 0){
            error_code = 'LOG_CALL'
        }

        $('#reportError').attr('href', "http://id.edufit.vn/apex/f?p=IL:LOGIN:::::P101_LOGIN_ID,P101_USERNAME,P101_PASSWORD,P101_TOKEN_KEY,P101_FLOW_PAGE,P101_SUPPORT_KEY,P101_LICENSE_VALUE,P101_FORM_CODE,P101_APP_PROFILES,P101_ERROR_PARAMETERS:0,{{Auth::user()->email}},pwd,,95,TB59,1519-7636-1798-1953,"+error_code+",,")
    });

</script>