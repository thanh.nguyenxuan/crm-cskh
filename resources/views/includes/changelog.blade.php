<div>
<h4>Lịch sử thay đổi</h4>
<table class="table table-hover">
<thead>
<th>STT</th>
<th>Loại thay đổi</th>
<th>Người thay đổi</th>
<th>Nội dung thay đổi</th>
<th>Thời gian</th>
</thead>
<tbody>
@php 
$stt = 1
@endphp
	@if(empty($change_log))
		<tr>
			<td colspan="5">Không có dữ liệu</td>
		</tr>
	@else
		@foreach($change_log as $log)
		<tr>
			<td>
				{{ $stt++ }}
			</td>
			<td>
				{{ $log->type }}
			</td>
			<td>
				{{ $log->changer }}
			</td>
			<td>
				{!! $log->log !!}
			</td>
			<td>
				{{ Carbon\Carbon::parse($log->created_at)->format('d/m/Y H:i:s') }}
			</td>
		</tr>
		@endforeach
	@endif
</tbody>
</table>
</div>