<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 24/11/2017
 * Time: 9:37 AM
 */
?>
<!-- Modal -->
<div class="modal fade" id="alarm_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Nhắc lịch hẹn <span></span></h4>
            </div>
            <div class="modal-body">
                <div id="alarm_message"></div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn-remind-later" class="btn btn-primary">Nhắc lại sau</button>
                <button type="button" id="btn-done" class="btn btn-danger">Đã hoàn thành</button>
            </div>
        </div>
    </div>
</div>
<script>
    var alarm_id_list = new Array();
    function get_alarm_list() {
        var user_id = {{ Auth::id() }};
        var data = null;
        $.get('/alarm/' + user_id + '/list', function (data) {
            alarm_list = JSON.parse(data);
            var message = '';
            for (var i = 0; i < alarm_list.length; i++) {
                var alarm = alarm_list[i];
                var alarm_at = '';
                if (alarm.alarm_at) {
                    var alarm_date = new Date(alarm.alarm_at);
                    var alarm_minutes = alarm_date.getMinutes();
                    if (alarm_minutes < 10)
                        alarm_minutes = '0' + alarm_minutes;
                    var alarm_seconds = alarm_date.getSeconds();
                    if (alarm_seconds < 10)
                        alarm_seconds = '0' + alarm_seconds;
                    alarm_at = alarm_minutes + ':' + alarm_seconds;
                }
                alarm_id_list.push(alarm.id);
                message += "<strong>" + alarm_at + "</strong>: Hẹn với lead <a target='_blank' href='/lead/" + alarm.lead_id + "/edit'><strong>" + alarm.lead_name + "</strong> (<strong>" + alarm.lead_phone + "</strong>)</a> với nội dung '<strong>" + (alarm.notes ? alarm.notes : '') + "</strong>'.<br/>";
            }
            if (message) {
                message += "Vui lòng nhấn nút <strong>Nhắc lại sau</strong> để tạm đóng lời nhắc hoặc nút <strong>Đã hoàn thành</strong> nếu không cần nhắc lại.<br />";
                $('#alarm_message').html(message);
                $("#alarm_modal").modal('show');
            } else {
                $("#alarm_modal").modal('hide');
            }
        });
    }

    var checkAlarmSchedule;

    function startCheckAlarmSchedule(){
        checkAlarmSchedule = setInterval(get_alarm_list, 2*60*1000);
        console.log('start checkAlarmSchedule');
    }

    function stopCheckAlarmSchedule(){
        clearInterval(checkAlarmSchedule);
        checkAlarmSchedule = null;
        console.log('stop checkAlarmSchedule');
    }

    function isCheckAlarmScheduleRunning()
    {
        return (checkAlarmSchedule);
    }

    $(function () {
        startCheckAlarmSchedule();
        $('#btn-remind-later').click(function () {
            $("#alarm_modal").modal('hide');
        });
        $('#btn-done').click(function () {
            $("#alarm_modal").modal('hide');
            var url = '/alarm/stop';
            $.post(url, { 'alarm_list': alarm_id_list, '_token' : '{{ csrf_token() }}' });
        });
    });
</script>
