<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 24/11/2017
 * Time: 9:37 AM
 */
?>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Online Check</h4>
            </div>
            <div class="modal-body">
                Have a nice day!
                </div>
            <div class="modal-footer">
                <button type="button" id="btn-onlinecheck" class="btn btn-primary">I'm online</button>
            </div>
        </div>
    </div>
</div>
