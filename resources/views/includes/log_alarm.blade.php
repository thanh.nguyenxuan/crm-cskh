<div role="tabpanel" class="tab-pane" id="log_alarm">
    @if(!empty($log_alarm_list))
        <table class="table table-condensed table-bordered">
            <thead>
            <th>STT</th>
            <th>Ghi chú</th>
            <th>Trạng thái</th>
            <th>Thời gian hẹn</th>
            <th>Thông báo trước (phút)</th>
            <th>Thời gian tạo</th>
            <th>Người tạo</th>
            </thead>
            <tbody>
            @php
                $stt = 0;
                $all_status = \App\Alarm::getAllStatus();
            @endphp
            @foreach($log_alarm_list as $log_alarm)
                <tr>
                    <td>{{ ++$stt }}</td>
                    <td>{{ $log_alarm->notes }}</td>
                    <td>{{ $all_status[$log_alarm->closed] }}</td>
                    <td>{{ !empty($log_alarm->alarm_at) ? Carbon\Carbon::parse($log_alarm->alarm_at)->format('d/m/Y H:i:s') : '' }}</td>
                    <td>{{ $log_alarm->alarm_before }}</td>
                    <td>{{ Carbon\Carbon::parse($log_alarm->created_at)->format('d/m/Y H:i:s') }}</td>
                    <td>
                        {!! (!empty($log_alarm->alarm_to) && isset($user_list[$log_alarm->alarm_to])) ? $user_list[$log_alarm->alarm_to] : '' !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <div class="alert alert-info">Không có dữ liệu!</div>
    @endif
</div>