<div role="tabpanel" class="tab-pane" id="log_status">
    @if(!empty($log_status_list))
        <table class="table table-condensed table-bordered">
            <thead>
            <th>STT</th>
            <th>Trạng thái cũ</th>
            <th>Trạng thái chuyển đổi</th>
            <th>Thời gian</th>
            <th>Người thao tác</th>
            <th>Loại</th>
            </thead>
            <tbody>
            @php
            $stt = 0;
            $leadStatusListData = \App\LeadStatus::getAllStatus();
            $callStatusListData = \App\CallStatus::all()->pluck('name', 'id');
            $careStatusListData = \App\CareStatus::listData();
            @endphp
            @foreach($log_status_list as $log_status)
                @if($log_status->type == \App\LeadLogStatus::TYPE_LEAD_STATUS)
                <tr>
                    <td>{{ ++$stt }}</td>
                    <td>{{ !empty($log_status->status_old) ? $leadStatusListData[$log_status->status_old] : ''}}</td>
                    <td>{{ !empty($log_status->status_new) ? $leadStatusListData[$log_status->status_new] : ''}}</td>
                    <td>{{ Carbon\Carbon::parse($log_status->created_at)->format('d/m/Y H:i:s') }}</td>
                    <td>{{ (!empty($log_status->created_by) && isset($user_list[$log_status->created_by])) ? $user_list[$log_status->created_by] : '' }}</td>
                    <td>Trạng thái lead</td>
                </tr>
                @endif
                @if($log_status->type == \App\LeadLogStatus::TYPE_CALL_STATUS)
                <tr>
                    <td>{{ ++$stt }}</td>
                    <td>{{ !empty($log_status->status_old) ? $callStatusListData[$log_status->status_old] : ''}}</td>
                    <td>{{ !empty($log_status->status_new) ? $callStatusListData[$log_status->status_new] : ''}}</td>
                    <td>{{ Carbon\Carbon::parse($log_status->created_at)->format('d/m/Y H:i:s') }}</td>
                    <td>{{ (!empty($log_status->created_by) && isset($user_list[$log_status->created_by])) ? $user_list[$log_status->created_by] : '' }}</td>
                    <td>Trạng thái chăm sóc</td>
                </tr>
                @endif
                @if($log_status->type == \App\LeadLogStatus::TYPE_CARE_STATUS)
                    <tr>
                        <td>{{ ++$stt }}</td>
                        <td>{{ !empty($log_status->status_old) ? $careStatusListData[$log_status->status_old] : ''}}</td>
                        <td>{{ !empty($log_status->status_new) ? $careStatusListData[$log_status->status_new] : ''}}</td>
                        <td>{{ Carbon\Carbon::parse($log_status->created_at)->format('d/m/Y H:i:s') }}</td>
                        <td>{{ (!empty($log_status->created_by) && isset($user_list[$log_status->created_by])) ? $user_list[$log_status->created_by] : '' }}</td>
                        <td>Trạng thái chăm sóc</td>
                    </tr>
                @endif
            @endforeach
            </tbody>
        </table>
    @else
        <div class="alert alert-info">Không có dữ liệu!</div>
    @endif
</div>