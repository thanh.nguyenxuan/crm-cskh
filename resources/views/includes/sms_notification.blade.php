<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 24/11/2017
 * Time: 9:37 AM
 */
?>
<!-- Modal -->
<div class="modal fade" id="sms_notification_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Tin nhắn SMS mới</h4>
            </div>
            <div class="modal-body">
                <div id="sms_message"></div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn-sms-remind-later" class="btn btn-primary">Nhắc lại sau</button>
                <button type="button" id="btn-sms-done" class="btn btn-danger">Đã hoàn thành</button>
            </div>
        </div>
    </div>
</div>
<script>
    var sms_id_list = new Array();

    function get_sms_list() {
        var user_id = {{ Auth::id() }};
        var data = null;
        $.get('/sms/' + user_id + '/list', function (data) {
            sms_list = JSON.parse(data);
            var message = '';
            for (var i = 0; i < sms_list.length; i++) {
                var sms = sms_list[i];
                sms_id_list.push(sms.id);
                message += "Lead <a target='_blank' href='/lead/" + sms.lead_id + "/edit'><strong>" + sms.lead_name + "</strong></a> đã nhắn tin đến tổng đài di động vào lúc <strong>" + sms.created_at + "</strong> với nội dung: <strong>" + sms.content + "</strong><br />";
            }
            if (message) {
                message += "Vui lòng nhấn nút <strong>Nhắc lại sau</strong> để tạm đóng lời nhắc hoặc nút <strong>Đã hoàn thành</strong> nếu không cần nhắc lại.<br />";
                $('#sms_message').html(message);
                $("#sms_notification_modal").modal('show');
            } else {
                $("#sms_notification_modal").modal('hide');
            }
        });
    }

    $(function () {
        setInterval(get_sms_list, 60000);
        $('#btn-sms-remind-later').click(function () {
            $("#sms_notification_modal").modal('hide');
        });
        $('#btn-sms-done').click(function () {
            $("#sms_notification_modal").modal('hide');
            var url = '/sms/stop';
            $.post(url, {'sms_list': sms_id_list, '_token': '{{ csrf_token() }}'});
        });
    });
</script>
