<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 24/11/2017
 * Time: 9:37 AM
 */
?>
<!-- Modal -->
<div class="modal fade" id="send_sms_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     data-keyboard="false" data-backdrop="static">
    <form method="post" id="send_sms_form">
        <input type="hidden" id="alarm_to" value="{{ Auth::id() }}">
        <input type="hidden" id="object_type" value="{{ (!empty($lead))?'lead':'account' }}">
        <input type="hidden" id="object_id" value="{{ (!empty($lead))?$lead->id:'' }}">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Gửi tin nhắn SMS</h4>
                </div>
                <div class="modal-body">
                    <div id="success_sent_message" class="hidden alert alert-success">Gửi tin nhắn SMS thành công!
                    </div>
                    <div id="error_sent_message" class="hidden alert alert-danger">Đã có lỗi trong quá trình gửi tin
                        nhắn SMS!
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1" class="required">Đầu số:</label>
                        <select id="sms_port" name="sms_port" required>
                            <option value="">Chọn đầu số</option>
                            <option value="10">1. 0123456891</option>
                            <!--<option value="7">6. 0984471866</option>-->
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1" class="required">Gửi đến SĐT:</label>
                        @if(!empty($phone_list))
                            {{ Form::select('sms_to', $phone_list, '', ['id' => 'sms_to', 'placeholder' => 'Chọn số cần nhắn', 'required' => 'required']) }}
                        @else
                            Không tìm thấy SĐT
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="sms_content" class="required">Nội dung tin nhắn (tối đa 300 ký tự):</label>
                        <textarea name="sms_content" id="sms_content" class="form-control" required cols="30" rows="10"
                                  maxlength="300"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-send-sms" class="btn btn-primary">Gửi</button>
                    <button type="button" id="btn-close-sms" class="btn btn-danger">Đóng</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    function clear_send_sms_form() {
        $('#sms_to').val('');
        $('#sms_port').val('');
        $('#sms_content').val('');
        $('#success_sent_message').removeClass('hidden');
        $('#success_sent_message').addClass('hidden');
        $('#error_sent_message').removeClass('hidden');
        $('#error_sent_message').addClass('hidden');
    }

    $(function () {
        $('#send_sms_button').click(function () {
            $("#send_sms_modal").modal('show');
        });
        $('#send_sms_form').submit(function (event) {
            event.preventDefault();
            var url = '';
            var data = new Object();
            data.sms_port = $('#sms_port').val();
            data.sms_to = $('#sms_to').val();
            data.sms_content = $('#sms_content').val();
            data._token = '{{ csrf_token() }}';
            $.post(url, data)
                .done(function () {
                    $('#success_sent_message').removeClass('hidden');
                    $('#error_sent_message').addClass('hidden');
                })
                .fail(function () {
                    $('#error_sent_message').removeClass('hidden');
                    $('#success_sent_message').addClass('hidden');
                });
            ;
        });
        $('#btn-close-sms').click(function () {
            $("#send_sms_modal").modal('hide');
            clear_send_sms_form();
        });

    });
</script>