<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 24/11/2017
 * Time: 9:37 AM
 */
?>
<!-- Modal -->
<div class="modal fade" id="log_call_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     data-keyboard="false" data-backdrop="static">
    <form method="post" id="log_call_form">
        <input type="hidden" id="lead_id" value="{{ (!empty($lead))?$lead->id:'' }}">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Log call</h4>
                </div>
                <div class="modal-body">
                    <div id="success_logged_message" class="hidden alert alert-success">Lưu dữ liệu thành công!
                    </div>
                    <div id="error_logged_message" class="hidden alert alert-danger">Đã có lỗi trong quá trình lưu dữ
                        liệu!
                    </div>
                    <div class="form-group">
                        <label for="log_call_direction" class="required">Chiều cuộc gọi:</label>
                        {{ Form::select('log_call_direction', array(1 => 'Gọi đi', 2 => 'Gọi đến'), old('log_call_direction')?old('log_call_direction'):'', ['placeholder'=> 'Chọn chiều cuộc gọi', 'required', 'id' => 'log_call_direction']) }}
                    </div>
                    <div class="form-group">
                        <label for="log_call_status" class="required">Trạng thái cuộc gọi:</label>
                        {{ Form::select('log_call_status', array(1 => 'Có nghe máy', 2 => 'Không nghe máy', 3 => 'Thuê bao', 4 => 'Sai số'), old('log_call_status')?old('log_call_status'):'', ['placeholder'=> 'Chọn trạng thái cuộc gọi', 'required', 'id' => 'log_call_status']) }}
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1" class="required">Thời điểm chăm sóc:</label>
                        <input type="text" class="form-control" id="logged_at" name="logged_at"
                               aria-describedby="emailHelp"
                               placeholder="Thời điểm chăm sóc" required value="{{ date('d/m/Y H:i') }}">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1" class="required">Nội dung trao đổi:</label>
                        <textarea name="log_call_notes" id="log_call_notes" class="form-control"
                                  required rows="5">{{ old('log_call_notes')?old('log_call_notes'):'' }}</textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-log-call" class="btn btn-primary">Lưu</button>
                    <button type="button" id="btn-log-call-close" class="btn btn-danger">Đóng</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    function clear_log_call_form() {
        $('#logged_at').val('');
        $('#log_call_notes').val('');
        $('#log_call_direction').val('');
        $('#log_call_status').val('');
        $('#success_logged_message').removeClass('hidden');
        $('#success_logged_message').addClass('hidden');
        $('#error_logged_message').removeClass('hidden');
        $('#error_logged_message').addClass('hidden');
    }

    $(function () {
        $('#logged_at').datetimepicker({
            timeInput: true,
            timeFormat: "HH:mm"
        });
        $('#log_call_button').click(function () {
            $("#log_call_modal").modal('show');
            $("#btn-log-call").removeClass('disabled');
        });
        $('#log_call_form').submit(function (event) {
            event.preventDefault();
            if($("#btn-log-call").hasClass('disabled')){
                return false;
            }
            var url = '{{ env('APP_URL') }}/call_history/log';
            var data = new Object();
            data.direction = $('#log_call_direction').val();
            data.status = $('#log_call_status').val();
            data.lead_id = $('#lead_id').val();
            data.created_at = $('#logged_at').val();
            data.notes = $('#log_call_notes').val();
            data._token = '{{ csrf_token() }}';

            $.ajax({
                url : url,
                type : 'POST',
                dataType: 'json',
                data : {
                    _token : '{{ csrf_token() }}',
                    direction : $('#log_call_direction').val(),
                    status : $('#log_call_status').val(),
                    lead_id : $('#lead_id').val(),
                    created_at : $('#logged_at').val(),
                    notes : $('#log_call_notes').val()
                },
                success: function(result) {
                    if(result.success){
                        clear_log_call_form();
                        $('#success_logged_message').removeClass('hidden');
                        $('#error_logged_message').addClass('hidden');
                        $("#btn-log-call").addClass('disabled');
                    }else{
                        $('#success_logged_message').addClass('hidden');
                        $('#error_logged_message').removeClass('hidden');
                        $('#error_logged_message').html(result.message);
                    }
                },
                error: function (jqXHR, exception) {
                    $('#error_scheduled_message').removeClass('hidden');
                    $('#error_logged_message').addClass('hidden');
                }
            });
        });
        $('#btn-log-call-close').click(function () {
            $("#log_call_modal").modal('hide');
            clear_log_call_form();
        });

    });
</script>