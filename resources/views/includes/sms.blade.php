<div class="modal fade" id="send_sms_modal" tabindex="-1" role="dialog">

    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Gửi tin nhắn 2</h4>
            </div>

            <input type="hidden" id="lead_id" value="{{ Auth::id() }}">
            <input type="hidden" id="target" value="{{$lead->getSMSTarget()}}"/>

            <div class="modal-body">
                @php if(in_array(\App\Department::HOLDING, Auth::user()->getManageDepartments())){ @endphp
                <div class="form-group">
                    <label for="sms_brandname" class="required">BrandName:</label>
                    <select id="sms_brandname" name="sms_brandname" required>
                        {!! \App\Telcom::getListBrandNameOptions() !!}
                    </select>
                </div>
                @php } @endphp

                <div class="form-group">
                    <label for="sms_content" class="required">Nội dung:</label>
                    <span id="sms_content_count">0/400</span>
                    <textarea name="sms_content" rows="5" id="sms_content" class="form-control" maxlength="400" required></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="btn-sms" class="btn btn-primary">Gửi</button>
                <button type="button" id="btn-close-modal-sms" class="btn btn-danger">Đóng</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="send_sms_result_modal" tabindex="-1" role="dialog">

    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Gửi tin nhắn</h4>
            </div>

            <div class="modal-body">
                <div id="send_sms_loading" class="text-center"><img src="/images/loading.gif"></div>
                <div id="success_sms_message" class="hidden alert alert-success">
                    Gửi tin nhắn thành công
                </div>
                <div id="error_sms_message" class="hidden alert alert-danger">
                    Đã có lỗi trong quá trình gửi tin nhắn!
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-danger">Đóng</button>
            </div>
        </div>
    </div>
</div>

<style>
    #sms_content_count{
        float: right;
        font-size: 12px;
    }
</style>
<script>
    function clear_send_sms_form() {
        $('#sms_content').val('');
        $('#sms_content_count').text('0/400');
    }

    $(function () {
        $('#sms_content').on('input', function(){
            var max_length = $(this).attr('maxlength');
            var content_length = $(this).val().length;
            $('#sms_content_count').text(content_length+'/'+max_length);
        });

        $('#send_sms_button').click(function () {
            clear_send_sms_form();
            $("#send_sms_modal").modal('show');
        });

        $('#btn-close-modal-sms').click(function () {
            $("#send_sms_modal").modal('hide');
            clear_send_sms_form();
        });

        $('#btn-sms').on('click', function (event) {
            if(!$(this).hasClass('disabled')){
                var url = '/lead/sendSms';
                var data = new Object();
                data.sms_content = $('#sms_content').val().trim();
                data.target = $('#target').val();
                data.lead_id = $('#lead_id').val();
                data._token = '{{ csrf_token() }}';

                if(data.target !== ""){
                    if($('#sms_brandname')){
                        data.brandname = $('#sms_brandname').val();
                        if(data.brandname === ''){
                            alert('Chưa chọn BrandName');
                            return;
                        }
                    }
                    if(data.sms_content !== ""){
                        $('#btn-sms').addClass('disabled');
                        $('#send_sms_modal').modal('hide');
                        $('#send_sms_result_modal').modal('show');
                        $('#send_sms_loading').removeClass('hidden');
                        $.post(url, data, function(result,status,xhr){
                                $('#btn-sms').removeClass('disabled');
                                $('#send_sms_loading').addClass('hidden');
                                if(result.success){
                                    $('#success_sms_message').removeClass('hidden');
                                    $('#error_sms_message').addClass('hidden');
                                    $("#log_sms tbody").prepend(result.dataHtml);
                                }else{
                                    $('#error_sms_message').removeClass('hidden');
                                    $('#success_sms_message').addClass('hidden');
                                }

                                clear_send_sms_form();
                            },
                            'json')
                            .fail(function () {
                                $('#error_sms_message').removeClass('hidden');
                                $('#success_sms_message').addClass('hidden');
                            });
                    }else{
                        alert('Nội dung tin nhắn rỗng');
                    }
                }else{
                    alert('Lead chưa có số điện thoại');
                }
            }
        });
    });
</script>