<div role="tabpanel" class="tab-pane {{ $lead->converted?' active ':'' }}" id="account">
    <div class="col-md-12">
        <h4>Danh sách lịch hẹn</h4>
        @php $stt = 0; @endphp
        <table>
            <thead>
            <th>STT</th>
            <th>Thời điểm hẹn</th>
            <th>Thông báo trước</th>
            <th>Nội dung</th>
            <th>Người đặt</th>
            <th>Thời điểm đặt lịch</th>
            </thead>
            <tbody>
            @foreach($alarm_list as $alarm)
                <tr>
                    <td>{{ $stt++ }}</td>
                    <td>{{ $alarm->alarm_at }}</td>
                    <td>{{ $alarm->alarm_before }} phút</td>
                    <td>{{ $alarm->notes }}</td>
                    <td>{{ $alarm->alarm_to }}</td>
                    <td>{{ $alarm->created_at }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>