<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 24/11/2017
 * Time: 9:37 AM
 */
?>
<!-- Modal -->
<div class="modal fade" id="set_alarm_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     data-keyboard="false" data-backdrop="static">
    <form method="post" id="set_alarm_form">
        <input type="hidden" id="alarm_to" value="{{ Auth::id() }}">
        <input type="hidden" id="object_type" value="{{ (!empty($lead))?'lead':'account' }}">
        <input type="hidden" id="object_id" value="{{ (!empty($lead))?$lead->id:'' }}">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Tạo nhắc lịch hẹn</h4>
                </div>
                <div class="modal-body">
                    <div id="success_scheduled_message" class="hidden alert alert-success">Đặt lịch hẹn thành công!
                    </div>
                    <div id="error_scheduled_message" class="hidden alert alert-danger">Đã có lỗi trong quá trình đặt
                        lịch hẹn!
                    </div>
                    <div class="form-group">
                        <label for="" class="required">Thời điểm hẹn lúc:</label>
                        <input type="text" class="form-control" id="alarm_at" aria-describedby="emailHelp"
                               placeholder="Thời điểm cần hẹn" required>
                    </div>
                    <div class="form-group">
                        <label for="" class="required">Báo trước (phút):</label>
                        <input type="text" class="form-control" id="alarm_before"
                               placeholder="Báo trước ? phút" value="5" required>
                    </div>
                    <div class="form-group">
                        <label for="" class="required">Loại</label>
                        <select id="alarm_type" class="form-control" required><?php echo \App\Utils::generateOptions(\App\Alarm::getTypeListData(), null)?></select>
                    </div>
                    <div class="form-group">
                        <label for="">Nội dung hẹn:</label>
                        <textarea name="alarm_notes" id="alarm_notes" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="btn-schedule" class="btn btn-primary">Đặt lịch</button>
                    <button type="button" id="btn-close" class="btn btn-danger">Đóng</button>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    function clear_set_alarm_form() {
        $('#alarm_at').val('');
        $('#alarm_before').val(5);
        $('#alarm_notes').val('');
        $('#success_scheduled_message').removeClass('hidden');
        $('#success_scheduled_message').addClass('hidden');
        $('#error_scheduled_message').removeClass('hidden');
        $('#error_scheduled_message').addClass('hidden');
    }

    $(function () {
        $('#alarm_at').datetimepicker({
            timeInput: true,
            timeFormat: "HH:mm"
        });
        $('#set_alarm_button').click(function () {
            $("#set_alarm_modal").modal('show');
            $('#btn-schedule').removeClass('disabled')
        });
        $('#set_alarm_form').submit(function (event) {
            event.preventDefault();
            if($('#btn-schedule').hasClass('disabled')){
                return false;
            }
            var url = '/alarm/set';
            var data = new Object();
            data.alarm_to = $('#alarm_to').val();
            data.object_type = $('#object_type').val();
            data.object_id = $('#object_id').val();
            data.alarm_at = $('#alarm_at').val();
            data.alarm_before = $('#alarm_before').val();
            data.type = $('#alarm_type').val();
            data.notes = $('#alarm_notes').val();
            data._token = '{{ csrf_token() }}';
            $.post(url, data)
                .done(function () {
                    $('#success_scheduled_message').removeClass('hidden');
                    $('#error_scheduled_message').addClass('hidden');
                    $('#btn-schedule').addClass('disabled')
                })
                .fail(function () {
                    $('#error_scheduled_message').removeClass('hidden');
                    $('#success_scheduled_message').addClass('hidden');
                });
            ;
        });
        $('#btn-close').click(function () {
            $("#set_alarm_modal").modal('hide');
            clear_set_alarm_form();
        });

    });
</script>