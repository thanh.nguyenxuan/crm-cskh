<div id="notifications" class="hidden">
    <div class="header">
        <div class="col-xs-6"><b>Thông báo</b></div>
        <div class="col-xs-6 text-right">
            <a id="btnMarkAllRead" onclick="markAllAsRead()">Đánh dấu tất cả đã đọc</a>
        </div>
    </div>
    <div class="body"></div>
    <div class="footer">
        <a href="<?php echo env('APP_URL')?>/notify">Xem tất cả</a>
    </div>
</div>

<div class="modal fade" id="notify_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Thông báo</h4>
            </div>
            <div class="modal-body" style="font-size: 16px">

            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btnClose btn btn-default">Đóng</button>
            </div>
        </div>
    </div>
</div>

<script>
    var notifyUpdating = false;

    function toggleNotifications()
    {
        if($('#notifications').hasClass('hidden')){
            markNewAsPending();
        }
        $('#notifications').toggleClass('hidden');
        $('#btnShowNotify .count_new').addClass('hidden');
    }

    function markAllAsRead()
    {
        if(!notifyUpdating){
            if(hasNotify()){
                notifyUpdating = true;
                $.ajax({
                    url: '<?php echo env('APP_URL')?>/notify/markAllAsRead',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        '_token' : '{{ csrf_token() }}'
                    },
                    success: function(result){
                        if(result.success){
                            $('#notifications').find('.item').each(function(){
                                if(!$(this).hasClass('read')){
                                    $(this).addClass('read');
                                }
                            });
                            $('#btnShowNotify .count_new').addClass('hidden');
                        }
                        notifyUpdating = false;
                    }
                });
            }
        }
    }

    function markNewAsPending()
    {
        if(!notifyUpdating){
            if(hasNotify()){
                if(!$('#btnShowNotify .count_new').hasClass('hidden')){
                    notifyUpdating = true;
                    $.ajax({
                        url: '<?php echo env('APP_URL')?>/notify/markNewAsPending',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            '_token' : '{{ csrf_token() }}'
                        },
                        success: function(result){
                            notifyUpdating = false;
                        }
                    });
                }
            }
        }
    }

    function markAsRead(notify_id)
    {
        if(!notifyUpdating){
            notifyUpdating = true;
            if(!$('#notifications .body .item[data-id='+notify_id+']').hasClass('read')){
                $.ajax({
                    url: '<?php echo env('APP_URL')?>/notify/markAsRead',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        '_token' : '{{ csrf_token() }}',
                        'notify_id' : notify_id
                    },
                    success: function(result){
                        $('#notifications').find('.item[data-id='+notify_id+']').addClass('read');
                        notifyUpdating = false;
                    }
                });
            }
        }
    }

    function markAsUnRead(notify_id)
    {
        if(!notifyUpdating){
            notifyUpdating = true;
            $.ajax({
                url: '<?php echo env('APP_URL')?>/notify/markAsUnRead',
                type: 'POST',
                dataType: 'json',
                data: {
                    '_token' : '{{ csrf_token() }}',
                    'notify_id' : notify_id
                },
                success: function(result){
                    $('#notifications').find('.item[data-id='+notify_id+']').removeClass('read');
                    notifyUpdating = false;
                }
            });
        }
    }

    function getNewNotify()
    {
        $.ajax({
            url: '<?php echo env('APP_URL')?>/notify/getNewNotify',
            type: 'POST',
            dataType: 'json',
            data: {
                '_token' : '{{ csrf_token() }}',
            },
            success: function(result){
                if(result.success){
                    if(result.data.total > 0){
                        $('#btnShowNotify .count_new').text(result.data.total).removeClass('hidden');
                    }
                    generateNotifyItem(result.data.notify);
                }
            }
        });
    }

    function generateNotifyItem(notify)
    {
        var empty = '<div class="empty text-center">Chưa có thông báo</div>';
        if (notify === undefined || notify.length === 0) {
            $('#notifications .body').html(empty);
        }else{
            $('#notifications .body').html('');
            notify.forEach(function(item, index, arr){
                var html = '<div class="item'+(item.status == '<?php echo \App\Notify::STATUS_SEEN ?>' ? ' read' : '') +'" data-id="'+item.id+'"' +
                    ((item.lead_id) ? (' data-lead-id="'+item.lead_id+'"') : '')+ '>' +
                    '<div class="content">' +
                    '<div class="text" onclick="notifyRedirect('+item.id+')">'+item.content+'</div>' +
                    '<div class="date">' +
                    '<i class="fa fa-clock-o"></i> ' + item.notify_at +
                    '</div>' +
                    '</div>' +
                    '<div class="options">' +
                    '<a class="btnMarkAsRead" onclick="markAsRead('+item.id+')" title="Đánh dấu là đã đọc">' +
                    '<i class="fa fa-circle"></i>' +
                    '</a>' +
                    '<a class="btnMarkAsUnRead" onclick="markAsUnRead('+item.id+')" title="Đánh dấu là chưa đọc">' +
                    '<i class="fa fa-dot-circle-o"></i>' +
                    '</a>' +
                    '</div>' +
                    '</div>';
                $('#notifications .body').append(html);
            });
        }
    }

    function hasNotify()
    {
        return ($('#notifications .body .item').length > 0);
    }

    function notifyRedirect(notify_id)
    {
        markAsRead(notify_id);
        var item = $('#notifications .body .item[data-id='+notify_id+']');
        if($(item).attr('data-lead-id')){
            window.open('<?php echo env('APP_URL')?>/lead/edit/'+$(item).attr('data-lead-id'),'_self');
        }
    }

    function checkNotifyLeadNewAndNoAnswer()
    {
        $.ajax({
            url: '<?php echo env('APP_URL')?>/notify/checkNotifyLeadNewAndNoAnswer',
            type: 'POST',
            dataType: 'json',
            data: {
                '_token' : '{{ csrf_token() }}',
            },
            success: function(result){
                if(result.success){
                    console.log(result);
                    $('#notify_modal .modal-body').html(result.message);
                    $('#notify_modal').attr('data-id', result.data.id);

                    $('#notify_modal').find('.btnClose')
                        .text('Đóng')
                        .removeClass('btn-danger')
                        .addClass('btn-default')
                    ;

                    if(result.data.type == '<?php echo \App\Notify::TYPE_ALARM_LEAD_DAILY_KPI?>') {
                        var data = JSON.parse(result.data.data);
                        var text = 'Tôi sẽ hoàn thành trong ngày';
                        if (!data.finish) {
                            $('#notify_modal').find('.btnClose')
                                .text('Tôi sẽ hoàn thành trong ngày')
                                .removeClass('btn-default')
                                .addClass('btn-danger')
                            ;
                        }
                    }

                    $('#notify_modal').modal('show');
                }
            }
        });
    }


    var checkNotifySchedule;

    function startCheckNotifySchedule(){
        checkNotifySchedule = setInterval(function () {
            getNewNotify();
            checkNotifyLeadNewAndNoAnswer();
        }, 1000 * 60);
        console.log('start checkNotifySchedule');
    }

    function stopCheckNotifySchedule()
    {
        clearInterval(checkNotifySchedule);
        checkNotifySchedule = null;
        console.log('stop checkNotifySchedule');
    }

    function isCheckNotifyScheduleRunning()
    {
        return (checkNotifySchedule);
    }

    $(document).ready(function()
    {
        getNewNotify();
        checkNotifyLeadNewAndNoAnswer();
        startCheckNotifySchedule();
        $('#notify_modal').on('hidden.bs.modal', function () {
            var notify_id = $(this).attr('data-id');
            if(notify_id){
                markAsRead(notify_id);
            }
        })
    });
</script>

