
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">
    <meta name="google-signin-client_id" content="353697397653-803rjkecieitopopipvltark1mqrin75.apps.googleusercontent.com">

    <title>FPTU CRM</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="https://apis.google.com/js/platform.js" async defer></script>
  </head>

  <body>

    <div class="container">
	@include('includes.status')
      <form class="form-signin" method="post">
		{{ csrf_field() }}
		<div class="logo">
			<img src="images/logo.png" class="img img-responsive" />
		</div>
        <div class="g-signin2" data-onsuccess="onSignIn"></div>
        <label for="username" class="sr-only">Tên đăng nhập</label>
        <input type="text" id="username" name="username" class="form-control" placeholder="Tên đăng nhập" required autofocus>
        <label for="password" class="sr-only">Mật khẩu</label>
        <input type="password" id="password" class="form-control" placeholder="Mật khẩu" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Đăng nhập</button>
		<a class="btn btn-lg btn-danger btn-block" href="{{ route('social.redirect', ['provider' => 'google']) }}">Đăng nhập bằng Google</a>
      </form>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
