<script src="/js/Chart.js"></script>

<div class="graph_container">
    <h2 style="opacity: 0.85"><b>Tỉ lệ lead từ các nguồn online</b></h2>
    <canvas id="chartStackedBars"></canvas>
</div>

<script>
    var barOptions_stacked = {
        tooltips: {
            enabled: true,
        },
        color: [
            '#4B5F71', '#d9534f', '#5bc0de', '#f0ad4e', '#286090', '#26B99A'
        ],
        hover :{
            animationDuration:0
        },
        toolbox: {
            show: true,
            feature: {
                restore: {
                    show: true,
                    title: "Trở lại ban đầu"
                },
            }
        },
        scales: {
            xAxes: [{
                ticks: {
                    beginAtZero:true,
                    fontFamily: "'Open Sans Bold', sans-serif",
                    fontSize:12
                },
                scaleLabel:{
                    display:true
                },
                gridLines: {
                },
                stacked: true
            }],
            yAxes: [{
                gridLines: {
                    display:false,
                    color: "#fff",
                    zeroLineColor: "#fff",
                    zeroLineWidth: 0
                },
                ticks: {
                    fontFamily: "'Open Sans Bold', sans-serif",
                    fontSize:12
                },
                stacked: true
            }]
        },
        legend:{
            display:true
        },
        pointLabelFontFamily : "Quadon Extra Bold",
        scaleFontFamily : "Quadon Extra Bold",
    };

    $(document).ready(function () {
        var ctx = document.getElementById("chartStackedBars");
        var chartStackedBars = new Chart(ctx, {
            type: 'horizontalBar',
            data: {
                labels: <?php echo json_encode($sourceRate['labels'])?>,
                datasets: [
                    {
                        label: 'Lead',
                        data: <?php $values = []; foreach($sourceRate['items'] as $item){$values[] = $item['count']['lead'];} echo json_encode($values)?>,
                        backgroundColor: "#4B5F71",
                        hoverBackgroundColor: "#394D5F"
                    }
                    , {
                        label: 'Hot Lead',
                        data: <?php $values = []; foreach($sourceRate['items'] as $item){$values[] = $item['count']['hot_lead'];} echo json_encode($values)?>,
                        backgroundColor: "#d9534f",
                        hoverBackgroundColor: "#c9302c"
                    }
                    , {
                        label: 'Show Up (SU)',
                        data: <?php $values = []; foreach($sourceRate['items'] as $item){$values[] = $item['count']['show_up'];} echo json_encode($values)?>,
                        backgroundColor: "#5bc0de",
                        hoverBackgroundColor: "#31b0d5"
                    }
                    , {
                        label: 'Đặt cọc (WL)',
                        data: <?php $values = []; foreach($sourceRate['items'] as $item){$values[] = $item['count']['wait_list'];} echo json_encode($values)?>,
                        backgroundColor: "#f0ad4e",
                        hoverBackgroundColor: "#ec971f"
                    }
                    , {
                        label: 'New Sale (NS)',
                        data: <?php $values = []; foreach($sourceRate['items'] as $item){$values[] = $item['count']['new_sale'];} echo json_encode($values)?>,
                        backgroundColor: "#337ab7",
                        hoverBackgroundColor: "#286090"
                    }
                    , {
                        label: 'Nhập học (NE)',
                        data: <?php $values = []; foreach($sourceRate['items'] as $item){$values[] = $item['count']['enroll'];} echo json_encode($values)?>,
                        backgroundColor: "#26B99A",
                        hoverBackgroundColor: "#169F85"
                    }
                ]
            },

            options: barOptions_stacked,
        });

        ctx.onclick = function(e){
            var item = chartStackedBars.getElementAtEvent(e)[0];
            if(item){
                var url = "{{ route('lead.index') }}?clear_filter=1&start_date={{ $start_date }}&end_date={{ $end_date }}{!! Auth::user()->hasRole('telesales') ? ('&assignee[]='.Auth::user()->id) : '' !!}{!! !empty(old('department_id')) ? ('&department_id[]='.old('department_id')) : '' !!}";
                switch (item._model.label) {
                    case 'Website' :            url+= '&source[]=1'; break;
                    case 'FB Inbox/Comment' :   url+= '&source[]=20'; break;
                    case 'Landingpage' :        url+= '&source[]=3'; break;
                    case 'Lead ads' :           url+= '&source[]=4'; break;
                    case 'Livechat' :           url+= '&source[]=2'; break;
                    default : url+= '&source[]=1&source[]=2&source[]=3&source[]=4&source[]=20'; break;
                }
                switch (item._model.datasetLabel) {
                    case 'Lead':            url+= ''; break;
                    case 'Hot Lead':        url+= '&hot_lead_from={{ $start_date }}'; break;
                    case 'Show Up (SU)':    url+= '&showup_school_from={{ $start_date }}'; break;
                    case 'Đặt cọc (WL)':    url+= '&active_from={{ $start_date }}'; break;
                    case 'New Sales (NS)':  url+= '&new_sale_from={{ $start_date }}'; break;
                    case 'Nhập học (NE)':   url+= '&enrolled_from={{ $start_date }}'; break;
                }
                window.open(url, '_blank');
            }
        }
    });

</script>