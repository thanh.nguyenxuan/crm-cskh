<script src="/vendors/incubator-echarts/dist/echarts.min.js"></script>

<div id="echart_pyramid" style="min-height: 400px"></div>

<script>
    function init_funnel_chart() {
        if( typeof (echarts) === 'undefined'){ return; }
        if ($('#echart_pyramid').length ){
            var echartFunnel = echarts.init(document.getElementById('echart_pyramid'));
            echartFunnel.setOption({
                title: {
                    text: 'Tỉ lệ chuyển đổi lead',
                    subtext: ''
                },
                textStyle: {
                    fontFamily: 'Arial, Verdana, sans-serif'
                },
                color: [
                    '#4B5F71', '#d9534f', '#5bc0de', '#f0ad4e', '#286090', '#26B99A'
                ],
                tooltip: {
                    trigger: 'item',
                    formatter: "{c}"
                },
                legend: {
                    data: [
                        'Lead',
                        'Hot Lead',
                        'Show Up (SU)',
                        'Đặt cọc (WL)',
                        'New Sales (NS)',
                        'Nhập học (NE)'
                    ],
                    orient: 'vertical',
                    x: 'left',
                    y: 'bottom'
                },
                calculable: false,
                series: [{
                    name: 'Loại',
                    type: 'funnel',
                    width: '60%',
                    data: [
                        {
                            value: {{ $conversionRate['count']['lead'] }},
                            name: 'Lead'
                        }
                        ,{
                            value: {{ $conversionRate['count']['hot_lead'] }},
                            name: 'Hot Lead'
                        }
                        ,{
                            value: {{ $conversionRate['count']['show_up'] }},
                            name: 'Show Up (SU)'
                        }
                        ,{
                            value: {{ $conversionRate['count']['wait_list'] }},
                            name: 'Đặt cọc (WL)'
                        }
                        ,{
                            value: {{ $conversionRate['count']['new_sale'] }},
                            name: 'New Sales (NS)'
                        }
                        ,{
                            value: {{ $conversionRate['count']['enroll'] }},
                            name: 'Nhập học (NE)'
                        }
                    ],
                    sort: "none"
                }]
            });

            echartFunnel.on('click', function (e) {
                var url = "{{ route('lead.index') }}?clear_filter=1&start_date={{ $start_date }}&end_date={{ $end_date }}{!! Auth::user()->hasRole('telesales') ? ('&assignee[]='.Auth::user()->id) : '' !!}{!! !empty(old('department_id')) ? ('&department_id[]='.old('department_id')) : '' !!}";
                switch (e.name) {
                    case 'Lead':            url+= ''; break;
                    case 'Hot Lead':        url+= '&hot_lead_from={{ $start_date }}'; break;
                    case 'Show Up (SU)':    url+= '&showup_school_from={{ $start_date }}'; break;
                    case 'Đặt cọc (WL)':    url+= '&active_from={{ $start_date }}'; break;
                    case 'New Sales (NS)':  url+= '&new_sale_from={{ $start_date }}'; break;
                    case 'Nhập học (NE)':   url+= '&enrolled_from={{ $start_date }}'; break;
                }
                window.open(url, '_blank');
            });

        }

    }

    $(document).ready(function () {
        init_funnel_chart();
    });
</script>