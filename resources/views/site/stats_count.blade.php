<?php
$lead_redirect_url = \App\Dashboard::getLeadRedirectUrl();
?>

<div class="row tile_count">
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <div class="x_panel">
            <span class="count_top">
                <a target="_blank" class="btn btn-xs btn-dark" href="{{ $lead_redirect_url.'&start_date='.$start_date.'&end_date='.$end_date }}{{ Auth::user()->hasRole('telesales') ? ('&assignee[]='.Auth::user()->id) : '' }}{{ !empty(old('department_id')) ? ('&department_id[]='.old('department_id')) : '' }}">
                    Lead
                </a>
            </span>
            <a target="_blank" href="{{ $lead_redirect_url.'&start_date='.$start_date.'&end_date='.$end_date }}{{ Auth::user()->hasRole('telesales') ? ('&assignee[]='.Auth::user()->id) : '' }}{{ !empty(old('department_id')) ? ('&department_id[]='.old('department_id')) : '' }}">
                <div class="count count_lead">{{ $tileStatsCounts['lead']['total'] }}</div>
            </a>
            <a target="_blank" href="{{ route('lead.index') }}?clear_filter=1&start_date={{ $start_date }}&end_date={{ $end_date }}&status_id[]={{ \App\LeadStatus::STATUS_NEW }}{{ Auth::user()->hasRole('telesales') ? ('&assignee[]='.Auth::user()->id) : '' }}{{ !empty(old('department_id')) ? ('&department_id[]='.old('department_id')) : '' }}">
                <span class="count_bottom">- Lead mới: <span class="count_lead_new">{{ $tileStatsCounts['lead']['new'] }}</span></span>
            </a>
            {{--<br/>
            <a target="_blank" href="javascript:void(0);">
                <span class="count_bottom">- Cần gọi: <span class="count_lead_need_call">{{ $tileStatsCounts['lead']['need_call'] }}</span></span>
            </a>
            <br/>
            <a target="_blank" href="javascript:void(0);">
                <span class="count_bottom">- Lịch hẹn: <span class="count_lead_have_alarm">{{ $tileStatsCounts['lead']['have_alarm'] }}</span></span>
            </a>--}}
        </div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <div class="x_panel">
            <span class="count_top">
                <a target="_blank" class="btn btn-xs btn-danger" href="{{ route('lead.index') }}?clear_filter=1&start_date={{ $start_date }}&end_date={{ $end_date }}&status_id[]={{ \App\LeadStatus::STATUS_HOT_LEAD }}{{ Auth::user()->hasRole('telesales') ? ('&assignee[]='.Auth::user()->id) : '' }}{{ !empty(old('department_id')) ? ('&department_id[]='.old('department_id')) : '' }}">
                    Hot Lead
                </a>
            </span>
            <a target="_blank" href="{{ route('lead.index') }}?clear_filter=1&start_date={{ $start_date }}&end_date={{ $end_date }}&status_id[]={{ \App\LeadStatus::STATUS_HOT_LEAD }}{{ Auth::user()->hasRole('telesales') ? ('&assignee[]='.Auth::user()->id) : '' }}{{ !empty(old('department_id')) ? ('&department_id[]='.old('department_id')) : '' }}">
                <div class="count count_lead_hot">{{ $tileStatsCounts['hot_lead']['total'] }}</div>
            </a>
            {{--<a target="_blank" href="javascript:void(0);"><span class="count_bottom">- Cần hẹn gặp: <span class="count_hot_lead_need_alarm">{{ $tileStatsCounts['hot_lead']['need_alarm'] }}</span></span></a>
            <br/>
            <a target="_blank" href="javascript:void(0);"><span class="count_bottom">- Cần gặp mặt: <span class="count_hot_lead_need_confirm">{{ $tileStatsCounts['hot_lead']['need_confirm'] }}</span></span></a>
            --}}
        </div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <div class="x_panel">
            <span class="count_top">
                <a target="_blank" class="btn btn-xs btn-info" href="{{ route('lead.index') }}?clear_filter=1&start_date={{ $start_date }}&end_date={{ $end_date }}&status_id[]={{ \App\LeadStatus::STATUS_SHOWUP_SCHOOL }}{{ Auth::user()->hasRole('telesales') ? ('&assignee[]='.Auth::user()->id) : '' }}{{ !empty(old('department_id')) ? ('&department_id[]='.old('department_id')) : '' }}">
                    Show Up (SU)
                </a>
            </span>
            <a target="_blank" href="{{ route('lead.index') }}?clear_filter=1&start_date={{ $start_date }}&end_date={{ $end_date }}&status_id[]={{ \App\LeadStatus::STATUS_SHOWUP_SCHOOL }}{{ Auth::user()->hasRole('telesales') ? ('&assignee[]='.Auth::user()->id) : '' }}{{ !empty(old('department_id')) ? ('&department_id[]='.old('department_id')) : '' }}">
                <div class="count count_lead_su">{{ $tileStatsCounts['show_up']['total'] }}
                </div>
            </a>
        </div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <div class="x_panel">
            <span class="count_top">
                <a target="_blank" class="btn btn-xs btn-warning" href="{{ route('lead.index') }}?clear_filter=1&start_date={{ $start_date }}&end_date={{ $end_date }}&status_id[]={{ \App\LeadStatus::STATUS_WAITLIST }}{{ Auth::user()->hasRole('telesales') ? ('&assignee[]='.Auth::user()->id) : '' }}{{ !empty(old('department_id')) ? ('&department_id[]='.old('department_id')) : '' }}">
                    Đặt cọc (WL)
                </a>
            </span>
            <a target="_blank" href="{{ route('lead.index') }}?clear_filter=1&start_date={{ $start_date }}&end_date={{ $end_date }}&status_id[]={{ \App\LeadStatus::STATUS_WAITLIST }}{{ Auth::user()->hasRole('telesales') ? ('&assignee[]='.Auth::user()->id) : '' }}{{ !empty(old('department_id')) ? ('&department_id[]='.old('department_id')) : '' }}">
                <div class="count count_lead_wl">{{ $tileStatsCounts['wait_list']['total'] }}</div>
            </a>
        </div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <div class="x_panel">
            <span class="count_top">
                <a target="_blank" class="btn btn-xs btn-primary" href="{{ route('lead.index') }}?clear_filter=1&start_date={{ $start_date }}&end_date={{ $end_date }}&status_id[]={{ \App\LeadStatus::STATUS_NEW_SALE }}{{ Auth::user()->hasRole('telesales') ? ('&assignee[]='.Auth::user()->id) : '' }}{{ !empty(old('department_id')) ? ('&department_id[]='.old('department_id')) : '' }}">
                    New Sales (NS)
                </a>
            </span>
            <a target="_blank" href="{{ route('lead.index') }}?clear_filter=1&start_date={{ $start_date }}&end_date={{ $end_date }}&status_id[]={{ \App\LeadStatus::STATUS_NEW_SALE }}{{ Auth::user()->hasRole('telesales') ? ('&assignee[]='.Auth::user()->id) : '' }}{{ !empty(old('department_id')) ? ('&department_id[]='.old('department_id')) : '' }}">
                <div class="count count_lead_ns">{{ $tileStatsCounts['new_sale']['total'] }}</div>
            </a>
        </div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <div class="x_panel">
            <span class="count_top">
                <a target="_blank" class="btn btn-xs btn-success" href="{{ route('lead.index') }}?clear_filter=1&start_date={{ $start_date }}&end_date={{ $end_date }}&status_id[]={{ \App\LeadStatus::STATUS_ENROLL }}{{ Auth::user()->hasRole('telesales') ? ('&assignee[]='.Auth::user()->id) : '' }}{{ !empty(old('department_id')) ? ('&department_id[]='.old('department_id')) : '' }}">
                    Nhập học (NE)
                </a>
            </span>
            <a target="_blank" href="{{ route('lead.index') }}?clear_filter=1&start_date={{ $start_date }}&end_date={{ $end_date }}&status_id[]={{ \App\LeadStatus::STATUS_ENROLL }}{{ Auth::user()->hasRole('telesales') ? ('&assignee[]='.Auth::user()->id) : '' }}{{ !empty(old('department_id')) ? ('&department_id[]='.old('department_id')) : '' }}">
                <div class="count count_lead_ne">{{ $tileStatsCounts['enroll']['total'] }}</div>
            </a>
        </div>
    </div>
</div>