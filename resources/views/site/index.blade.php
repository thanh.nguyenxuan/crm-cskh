@extends('layouts.main')

@section('content')

    @include('site.stats_count')

    <div class="x_panel">

        <div class="x_title">
            <form method="get">
                <div class="col-sm-3">
                    <input id="date_range" class="form-control" name="date_range" value="{{ $date_range }}">
                </div>
                <?php
                $departmentListData = Auth::user()->getManageDepartmentsListData();
                if(count($departmentListData) > 1){ ?>
                <div class="col-sm-3">
                    <select id="department_id" name="department_id" class="form-control">
                        {!!  \App\Utils::generateOptions($departmentListData, old('department_id'), 'Tất cả'); !!}
                    </select>
                </div>
                <?php } ?>
                <button class="btn btn-primary" type="submit">Lọc</button>
            </form>

            <div class="clearfix"></div>
        </div>

        <div class="x_content">

            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    @include('site.funnel')
                </div>

                <div class="col-sm-6 col-xs-12">
                    @include('site.stacked_bar')
                </div>
            </div>

        </div>
    </div>



    <script>
        $('#date_range').daterangepicker({locale: {format: 'DD/MM/YYYY'}});
    </script>
@stop