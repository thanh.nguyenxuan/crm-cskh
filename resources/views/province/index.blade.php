@include('includes.header')
<div class="col-md-12">
    <h4>Quản lý tỉnh thành</h4>
    <table class="table table-hover">
        <thead>
        <th>STT</th>
        <th>Tên tỉnh</th>
        <th>Phòng</th>
        </thead>
        <tbody>
        @php $stt = 1; @endphp
        @foreach($province_list as $province)
            <tr>
                <td>{{ $stt++ }}</td>
                <td>{{ $province->name }}</td>
                <td>{{ $province->department }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</body>
</html>