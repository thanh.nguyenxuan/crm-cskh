<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-signin-client_id"
          content="353697397653-803rjkecieitopopipvltark1mqrin75.apps.googleusercontent.com">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'CRM') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <script src="https://apis.google.com/js/platform.js" async defer></script>
</head>
<body>
<div id="app">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">

                        <div id="logo">
                            <img src="images/logo.png" class="img" style="max-width: 200px;"/>
                        </div>
                        <div class="login-button-wrapper">
                            @if(!empty(session('message')))
                                <div class="alert alert-danger">
                                    {{ session('message') }}
                                </div>
                            @endif
                            @if ($errors->has('username'))
                                <div class="alert alert-danger">
                                    {{ $errors->first('username') }}
                                </div>
                            @endif
                            <a class="btn btn-google-signin"
                               href="{{ route('social.redirect', ['provider' => 'google']) }}"><img
                                        src="images/google_logo.png"><span>Đăng nhập với Google</span></a>
                        </div>
                        <div class="login-button-wrapper">
                            <button type="submit" class="btn btn-password-signin" role="button" data-toggle="collapse"
                                    data-target="#password-login-wrapper" aria-expanded="false"
                                    aria-controls="password-login-wrapper">
                                Đăng nhập với mật khẩu
                            </button>
                        </div>
                        <div class="collapse col-md-7 col-md-offset-3" id="password-login-wrapper">

                            <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="username" class="col-md-4 control-label">Tên đăng nhập</label>

                                    <div class="col-md-6">
                                        <input id="username" type="text" class="form-control" name="username"
                                               value="{{ old('username') }}" required autofocus>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password" class="col-md-4 control-label">Mật khẩu</label>

                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control" name="password"
                                               required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">

                                        <button type="submit" class="btn btn-primary">
                                            Đăng nhập
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
