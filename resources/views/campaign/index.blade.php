@extends('layouts.main')

@section('content')

<div class="x_panel">
    <div class="x_title">
        <h2>Quản lý campaign</h2>
        <div class="pull-right">
            <a class="btn btn-default" href="{{ route('campaign.create') }}">Thêm campaign mới</a><br/>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <table class="table table-hover">
            <thead>
            <th></th>
            <th></th>
            <th>Tên campaign</th>
            <th>Loại</th>
            </thead>
            <tbody>
            @foreach($campaign_list as $campaign)
                <tr>
                    <td><a href="{{ route('campaign.edit', $campaign->id) }}" title="Chỉnh sửa"><i
                                    class="fa fa-lg fa-pencil"
                                    aria-hidden="true"></i></a>
                    </td>
                    <td><a href="{{ route('campaign.toggle', $campaign->id) }}"
                           title="{!! $campaign->hide ?'Hiện' : 'Ẩn' !!}"><i
                                    class="fa fa-lg {!! $campaign->hide ?'fa-eye' : 'fa-eye-slash' !!}"
                                    aria-hidden="true"></i></a></td>
                    <td>{{ $campaign->name }}</td>
                    <td><?php echo str_replace(',','<br/>',$campaign->category_list)?></td>
                    <td>{!! $campaign->hide ?'<span class="class-status active">Ẩn</span>':'<span class="class-status inactive">Hiện</span>' !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

</div>

    @stop