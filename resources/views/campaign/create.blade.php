@extends('layouts.main')

@section('content')

    <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Thêm campaign mới</div>
                <div class="panel-body">
                    @if(!empty($message))
                        <div class="alert {!! $success == 1?'alert-success':'alert-danger' !!}"
                             role="alert">{!! $message !!}</div>
                    @endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('campaign.store') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label required">Tên campaign</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"
                                       required autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                            <label for="category" class="col-md-4 control-label">Loại</label>

                            <div class="col-md-6">
                                <select id="category" class="form-control" name="category">
                                    <?php echo \App\Utils::generateOptions(\App\CampaignCategory::listData(), old('category'), 'Chọn loại')?>
                                </select>
                                @if ($errors->has('category'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Khởi tạo
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

    @stop