@extends('layouts.main')

@section('content')

<div class="x_panel">
    <div class="x_title">
        <h2>Danh sách lịch hẹn chăm sóc</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('alarm.index') }}" method="get" class="form-horizontal">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-2">
                                <label>Từ ngày</label>
                                <input class="form-control" type="text" name="from_date" id="from_date" value="{{ $from_date?$from_date:date('d/m/Y') }}">
                            </div>
                            <div class="col-sm-2">
                                <label>Từ ngày</label>
                                <input class="form-control" type="text" name="to_date" id="to_date" value="{{ $to_date?$to_date:date('d/m/Y') }}">
                            </div>
                            <div class="col-sm-2">
                                <label>Theo thời gian</label>
                                <select id="date_type" name="date_type" class="form-control">
                                    <option value="created_at" {{ $date_type == 'created_at' ? 'selected' : '' }}>Ngày tạo</option>
                                    <option value="alarm_at" {{ $date_type == 'alarm_at' ? 'selected' : '' }}>Ngày hẹn</option>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <label>TVTS</label>
                                <select id="assignee" name="assignee" class="form-control">
                                    {!! \App\Utils::generateOptions(\App\User::listData(FALSE, !in_array(\App\Department::HOLDING, Auth::user()->getManageDepartments())), old('assignee'), 'Chọn') !!}
                                </select>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-edufit-default">Xem</button>

                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover table-bordered table-responsive">
                    <thead>
                    <th class="text-center">STT</th>
                    <th>Lead</th>
                    <th class="text-center">Hẹn gọi lại vào lúc</th>
                    <th class="text-center">Báo trước</th>
                    <th>Ghi chú</th>
                    <th class="text-center">Ngày đặt lịch</th>
                    <th>Người đặt lịch</th>
                    </thead>
                    <tbody>
                    @php $stt = 0; @endphp
                    @foreach($alarm_list as $alarm)
                        <tr>
                            <td class="text-center">{{ ++$stt }}</td>
                            <td>
                                @if(!empty($alarm->lead_id))
                                    <a href="{{ config('app.url') }}/lead/{{ $alarm->lead_id }}"
                                       target="_blank">{{ $alarm->lead_name }}</a>
                                @endif
                            </td>
                            <td class="text-center">{{ Carbon\Carbon::parse($alarm->alarm_at)->format('d/m/Y H:i:s') }}</td>
                            <td class="text-center">
                                {{ $alarm->alarm_before }} phút
                            </td>
                            <td>
                                {{ $alarm->notes }}
                            </td>
                            <td class="text-center">{{ Carbon\Carbon::parse($alarm->created_at)->format('d/m/Y H:i:s') }}</td>
                            <td>
                                {{ !empty($alarm->alarm_to)?App\User::find($alarm->alarm_to)->username:'' }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
<script>
    $(function () {
        var full_month_names = new Array("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");
        $.datepicker.regional["vi-VN"] =
            {
                closeText: "Đóng",
                prevText: "Trước",
                nextText: "Sau",
                currentText: "Hôm nay",
                monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
                monthNamesShort: full_month_names,
                dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
                dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
                dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
                weekHeader: "Tuần",
                dateFormat: "dd/mm/yy",
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: "",
                changeMonth: true,
            };
        $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
        $("#from_date").datepicker();
        $("#to_date").datepicker();
    });
</script>

@stop