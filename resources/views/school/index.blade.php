@extends('layouts.main')

@section('content')

<div class="x_panel">
    <div class="x_title">
        <h2>Quản lý trường</h2>
        <div class="pull-right">
            <a class="btn btn-default" href="{{ route('school.create') }}">Thêm trường mới</a><br/>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <table class="table table-hover">
            <thead>
            <th></th>
            <th></th>
            <th>Loại trường</th>
            <th>Tên trường</th>
            <th>Địa chỉ</th>
            <th>Tỉnh</th>
            </thead>
            <tbody>
            @foreach($school_list as $school)
                <tr>
                    <td><a href="{{ route('school.edit', $school->id) }}" title="Chỉnh sửa"><i
                                    class="fa fa-lg fa-pencil"
                                    aria-hidden="true"></i></a>
                    </td>
                    <td><a href="{{ route('school.toggle', $school->id) }}"
                           title="{!! $school->hide ?'Hiện' : 'Ẩn' !!}"><i
                                    class="fa fa-lg {!! $school->hide ?'fa-eye' : 'fa-eye-slash' !!}"
                                    aria-hidden="true"></i></a></td>
                    <td>{{ !empty($school->type)?App\SchoolType::find($school->type)->name:'' }}</td>
                    <td>{{ $school->name }}</td>
                    <td>{{ $school->address }}</td>
                    <td>{{ !empty($school->province)?$province_list[$school->province]:'' }}</td>
                    <td>{!! !empty($school->hide) ?'<span class="class-status active">Ẩn</span>':'<span class="class-status inactive">Hiện</span>' !!}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop