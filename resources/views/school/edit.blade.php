@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Chỉnh sửa trường</div>
                <div class="panel-body">
                    @if(!empty($message))
                        <div class="alert {!! $success == 1?'alert-success':'alert-danger' !!}"
                             role="alert">{!! $message !!}</div>
                    @endif
                    <form class="form-horizontal" role="form" method="POST"
                          action="{{ route('school.update', $school->id) }}">
                        {!! method_field('patch') !!}
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Tên cơ sở</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name"
                                       value="{{ $school->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label for="type" class="col-md-4 control-label required">Loại trường</label>

                            <div class="col-md-6">
                                {{ Form::select('type', $school_type_list, !empty($school->type)?$school->type:old('type'), ['id' => 'type', 'multiple' => true, 'class' => 'form-control', 'required' => 'required']) }}

                                @if ($errors->has('type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-4 control-label">Địa chỉ</label>

                            <div class="col-md-6">
                                <input id="address" type="text" class="form-control" name="address"
                                       value="{{ !empty($school->address)?$school->address:old('address') }}" autofocus>

                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('province') ? ' has-error' : '' }}">
                            <label for="province" class="col-md-4 control-label">Tỉnh/thành
                                phố</label>

                            <div class="col-md-6">
                                {{ Form::select('province', $province_list, !empty($school->province)?$school->province:old('province'), ['id' => 'province', 'multiple' => true, 'class' => 'form-control']) }}
                                @if ($errors->has('province'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('province') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Cập nhật
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop