<?php
/**
 * @var $lead \App\Lead
 */
?>
<style>
    body.modal-open{
        overflow: unset !important;
    }

    @media (min-width: 768px){
        #cdr_call_log .modal-dialog {
            width: 450px !important;
        }
    }
    #cdr_call_log{
        position: relative;
    }
    #cdr_call_log .modal-dialog {
        position: fixed;
        width: 100%;
        margin: 0;
        padding: 10px;
    }
</style>
<div class="modal fade" id="cdr_call_log" tabindex="-1" role="dialog" data-backdrop="false" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Gọi qua IP phone</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="cdr_target">Số điện thoại</label>
                            <?php if(!empty($lead->phone1) && !empty($lead->phone2)){ ?>
                            <select id="cdr_target" name="cdr_target" class="form-control">
                                <option value="{{ $lead->phone1 }}">SĐT 1: {{ $lead->phone1 }}</option>
                                <option value="{{ $lead->phone2 }}">SĐT 2: {{ $lead->phone2 }}</option>
                            </select>
                            <?php }else{ ?>
                            <input id="cdr_target" type="text" name="cdr_target" class="form-control" value="{{ $lead->phone1 }}"/>
                            <?php }?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="cdr_target">IP Phone</label>
                            <input id="cdr_agent" type="text" name="cdr_agent" class="form-control" value="{{ \Illuminate\Support\Facades\Auth::user()->extension}}" readonly/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label>Thời gian</label>
                            <p><b class="col-sm-4 no_pad">Bắt đầu</b>: <span id="cdr_call_start_time"></span></p>
                            <p><b class="col-sm-4 no_pad">Kết thúc</b>: <span id="cdr_call_end_time"></span></p>
                            <p><b class="col-sm-4 no_pad">Trạng thái</b>: <span id="cdr_call_status"></span></p>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="cdr_note">Ghi chú</label>
                            <textarea id="cdr_note" name="cdr_note" class="form-control" rows="5"></textarea>
                        </div>

                        <div class="form-group <?php echo (Auth::user()->hasRole('carer')) ? 'hidden' : ''?>">
                            <label for="cdr_lead_call_status">Trạng thái chăm sóc</label>
                            <select id="cdr_lead_call_status" name="cdr_lead_call_status" class="form-control">
                                {!! \App\Utils::generateOptions($call_status_list, $lead->call_status, 'Chọn trạng thái') !!}
                            </select>
                        </div>
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-sm-6 text-left">
                        <button id="cdr_btn_authen" class="btn btn-warning<?php echo !empty(\Illuminate\Support\Facades\Auth::user()->extension)?' hidden':''?>">Xác thực</button>
                        <button id="cdr_btn_click2call" class="btn btn-success<?php echo !empty(\Illuminate\Support\Facades\Auth::user()->extension)?'':' disabled'?>">Gọi</button>
                        <button id="cdr_btn_hangup" class="btn btn-danger disabled">Kết thúc</button>
                    </div>
                    <div class="col-sm-6 text-right">
                        <button id="cdr_btn_save" class="btn btn-primary disabled">Lưu</button>
                        <button id="cdr_btn_close" class="btn btn-default">Đóng</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#cdr_call_log").draggable({
        handle: ".modal-header"
    });
</script>

<script src="/js/socket.io.js"></script>
<script>
    var connect_res, authen_res, click2call_res, hangup_res;
    var socketConnect;
    var cloud_pbx_host = '<?php echo \App\Report\Cdr::HOST?>';
    var call_session;

    function connect(){
        socketConnect = io.connect(cloud_pbx_host);
        socketConnect.on('connect', function(){
            connect_res = {'id' : socketConnect.id};
            console.log(connect_res);
            $('#cdr_call_status').html('Đã kết nối');
            if(!authen_res){
                authen();
            }
        });
        socketConnect.on('message', function(response){
            console.log(response);
            switch (response.code) {
                case '301':
                    if(response.status === 'success'){
                        authen_res = clone(response);
                        $('#cdr_btn_authen').addClass('disabled');
                        $('#cdr_btn_click2call').removeClass('disabled');
                        $('#cdr_call_status').html('Đã xác thực');
                    }else if(response.status === 'fail'){
                        alert(response.message);
                    }
                    break;
                case '314':
                    click2call_res = clone(response);
                    if(response.status === 'fail'){
                        alert(response.message);
                    }
                    break;
                case '330':
                    hangup_res = clone(response);
                    if(hangup_res.status === 'success'){
                        click2call_res = false;
                        $('#cdr_btn_hangup').addClass('disabled');
                    }else if(response.status === 'fail'){
                        alert(response.message);
                    }
                    break;
                case '324':
                    if(!call_session){
                        $('#cdr_call_start_time').html(getCurrentTime());
                        $('#cdr_btn_hangup').removeClass('disabled');
                        $('#cdr_btn_click2call').addClass('disabled');
                        $('#cdr_btn_close').addClass('disabled');
                        $('#cdr_call_status').html('Đang gọi ...');
                        call_session = response.data.call_session;
                    }
                    break;
                case '326':
                    if(call_session === response.data.call_session){
                        $('#cdr_call_status').html('Đang nghe ...');
                    }
                    break;
                case '328':
                    if(call_session === response.data.call_session){
                        $('#cdr_btn_hangup').addClass('disabled');
                        $('#cdr_btn_save').removeClass('disabled');
                        $('#cdr_call_end_time').html(getCurrentTime());
                        $('#cdr_call_status').html('Kết thúc');
                    }
                    break;

            }
            console.log(call_session);
        });
    }

    function authen(){
        var cdr_agent = $('#cdr_agent').val();
        if(!cdr_agent){
            alert('Please enter ip phone number');
            return;
        }
        if(!connect_res || typeof connect_res !== 'object' || !connect_res.id){
            alert("Please connect");
            return;
        }
        $.ajax({
            url: '{{ env('APP_URL') }}/report/cdr/getPbxConfig',
            type: 'post',
            dataType: 'json',
            data: {
                '_token': '{{ csrf_token() }}'
            },
            success : function(result){
                socketConnect.emit('authen', {
                    'token': result.token,
                    'organization' : result.organization,
                    'extension' : cdr_agent
                });
            }
        })

    }

    function click2call(){
        var target = $('#cdr_target').val();
        var validTargetRegex = /^0[0-9]*$/;
        if(!target){
            alert('Please enter call number! Vui lòng nhập số điện thoại');
            return;
        }
        if(!validTargetRegex.test(target)){
            alert('Invalid call number! Số điện thoại không hợp lệ');
            return;
        }
        console.log(authen_res);
        if(!authen_res || typeof authen_res !== 'object' || authen_res.status !== 'success'){
            alert("Please authenticate the IP Phone number! Vui lòng xác thực số máy IP Phone");
            return;
        }

        stopCheckNotifySchedule();
        stopCheckAlarmSchedule();

        socketConnect.emit('click2call', {
            'phone_number' : target
        });
    }

    function hangup(){
        socketConnect.emit('hangup', {});
    }

    function clone(obj) {
        if (null == obj || "object" != typeof obj) return obj;
        var copy = obj.constructor();
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
        }
        return copy;
    }

    function clear_send_cdr_call_log_form()
    {
        call_session = null;
        $('#cdr_btn_click2call').removeClass('disabled');
        $('#cdr_btn_hangup').addClass('disabled');
        $('#cdr_btn_save').addClass('disabled');
        $('#cdr_btn_close').removeClass('disabled');
        $('#cdr_call_start_time').html('');
        $('#cdr_call_end_time').html('');
        $('#cdr_note').val('');
    }

    function getCurrentTime()
    {
        var d = new Date();
        return d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();
    }

    $(document).ready(function(){
        var firstOpen = true;
        $('#cdr_call_log_btn').click(function () {
            clear_send_cdr_call_log_form();

            if(firstOpen){
                if (!($('#cdr_call_log.in').length)) {
                    $('#cdr_call_log .modal-dialog').css({
                        top: 0,
                        right: 0
                    });
                }
                firstOpen = false;
            }
            $('#cdr_call_log .modal-dialog').draggable({
                handle: ".modal-header"
            });
            $('#cdr_call_log').modal({
                backdrop: false,
                show: true
            });

            if(!connect_res){
                connect();
            }else{
                if(authen_res){
                    $('#cdr_call_status').html('Đã xác thực');
                }else{
                    $('#cdr_call_status').html('Đã kết nối');
                }
            }
        });

        $('#cdr_btn_click2call').click(function () {
            if(!$(this).hasClass('disabled')){
                click2call();
            }
        });

        $('#cdr_btn_close').click(function () {
            if(!$(this).hasClass('disabled')){
                $("#cdr_call_log").modal('hide');
                /*if(call_session){
                    location.reload();
                }*/
            }
            if(!isCheckAlarmScheduleRunning()){
                startCheckAlarmSchedule();
            }
            if(!isCheckNotifyScheduleRunning()){
                startCheckNotifySchedule();
            }
        });

        $('#cdr_btn_hangup').click(function () {
            if(!$(this).hasClass('disabled')){
                hangup();
                $('#cdr_btn_save').removeClass('disabled');
                $('#cdr_call_end_time').html(getCurrentTime());
            }
        });

        $('#cdr_btn_save').click(function () {
            if(!$(this).hasClass('disabled') && call_session){
                $('#cdr_btn_save').addClass('disabled');
                $.ajax({
                    url: '<?php echo env('APP_URL')?>/report/cdr/saveCallLog',
                    type: 'POST',
                    dataType: 'json',
                    data: {
                        '_token' : '{{ csrf_token() }}',
                        'call_session' : call_session,
                        'lead_id' : '{{ $lead->id }}',
                        'note' : $('#cdr_note').val(),
                        'lead_call_status' : $('#cdr_lead_call_status').val()
                    },
                    success: function(result){
                        console.log(result);
                        if(result.success){

                            $('#cdr_btn_close').removeClass('disabled');
                            $('#cdr_call_status').html('Đã lưu');

                            $('#call_status_id').val($('#cdr_lead_call_status').val());
                            $('#call_history_pane table tbody').prepend(result.data.html);
                        }else{
                            alert(result.message);
                            $('#cdr_btn_save').removeClass('disabled');
                        }
                    },
                    error: function (jqXHR, exception) {
                        var msg = '';
                        if (jqXHR.status === 0) {
                            msg = 'Not connect.\n Verify Network.';
                        } else if (jqXHR.status == 404) {
                            msg = 'Requested page not found. [404]';
                        } else if (jqXHR.status == 500) {
                            msg = 'Internal Server Error [500].';
                        } else if (exception === 'parsererror') {
                            msg = 'Requested JSON parse failed.';
                        } else if (exception === 'timeout') {
                            msg = 'Time out error.';
                        } else if (exception === 'abort') {
                            msg = 'Ajax request aborted.';
                        } else {
                            msg = 'Uncaught Error.\n' + jqXHR.responseText;
                        }
                        alert(msg);
                    },
                });
            }
        });

        $('#cdr_btn_authen').click(function () {
            if(!$(this).hasClass('disabled') && !$(this).hasClass('hidden')){
                authen();
            }
        });


    });

</script>
