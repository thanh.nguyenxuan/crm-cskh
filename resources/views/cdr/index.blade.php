@extends('layouts.main')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Chi tiết cuộc gọi</h2>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <div class="col-md-12">
            <form action="{{ route('cdr.index') }}" method="get">
                {{ csrf_field() }}
                <label>Từ ngày: </label><input type="text" name="from_date" id="from_date"
                                               value="{{ $from_date?$from_date:'' }}">
                <label>Đến ngày: </label><input type="text" name="to_date" id="to_date"
                                                value="{{ $to_date?$to_date:'' }}">
                <label>Chiều gọi: </label>
                {{ Form::select('direction', $direction_list, old('direction'), ['placeholder' => 'Chọn chiều gọi']) }}
                </select>
                <label>Đầu số: </label>
                {{ Form::select('trunk', $trunk_list, old('trunk'), ['placeholder' => 'Chọn đầu số']) }}
                <label>Telesales: </label>
                {{ Form::select('tele', $telesales_list, null, ['placeholder' => 'Chọn tele']) }}
                <button type="submit">Xem</button>
            </form>
        </div>
        <table class="table table-hover">
            <thead>
            <th>STT</th>
            <th>Thời điểm bắt đầu</th>
            <th>Chiều gọi</th>
            <th>Đầu số</th>
            <th>Lead</th>
            <th>SĐT</th>
            <th>File ghi âm</th>
            <th>Thời lượng</th>
            <th>Telesales</th>
            <th>Trạng thái</th>
            </thead>
            <tbody>
            @php $stt = 0; @endphp
            @foreach($data as $call)
                <tr>
                    <td>{{ ++$stt }}</td>
                    <td>{{ Carbon\Carbon::parse($call->call_date)->format('d/m/Y H:i:s') }}</td>
                    <td>
                        @if($call->direction == 'outgoing')
                            Gọi đi
                        @elseif($call->direction == 'incoming')
                            Gọi đến
                        @endif
                    </td>
                    <td>
                        @if(!empty($call->trunk_name))
                            {{ $call->trunk_name }}
                        @endif
                    </td>
                    <td>
                        @if(!empty($call->lead_id))
                            <a href="{{ env('APP_URL') }}/lead/{{ $call->lead_id }}/edit"
                               target="_blank">{{ $call->lead_name }}</a>
                        @endif
                    </td>
                    <td>
                        @if($call->direction == 'outgoing')
                            {{ $call->to }}
                        @elseif($call->direction == 'incoming')
                            {{ $call->from }}
                        @endif
                    </td>
                    <td>
                        @if(!empty($call->recording_file))
                            <audio controls>
                                <source src="https://{{ env('PBX_HOST') }}/api/recording.php?id={{ $call->id }}"
                                        type="audio/wav">
                            </audio>
                        @endif
                    </td>
                    <td>
                        {{ sprintf('%02d', floor($call->duration/60)) }}:{{ sprintf('%02d', $call->duration%60) }}
                    </td>
                    <td>
                        {{ !empty($call->caller)?$call->caller:$call->ext }}
                    </td>
                    <td>
                        {{ $call->status }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

</div>
<script>
    $(function () {
        var full_month_names = new Array("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");
        $.datepicker.regional["vi-VN"] =
            {
                closeText: "Đóng",
                prevText: "Trước",
                nextText: "Sau",
                currentText: "Hôm nay",
                monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
                monthNamesShort: full_month_names,
                dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
                dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
                dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
                weekHeader: "Tuần",
                dateFormat: "dd/mm/yy",
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: "",
                changeMonth: true,
            };
        $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
        $("#from_date").datepicker();
        $("#to_date").datepicker();
    });
</script>

    @stop