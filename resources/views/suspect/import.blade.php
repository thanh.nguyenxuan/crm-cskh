@extends('layouts.main')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Import data</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div>
            Tải về <strong><a href="{{ env('APP_URL') . '/templates/template_import_suspect_20201112.xlsx' }}">template import data</a></strong>
        </div>
        <form method="post" action="<?php echo url('suspect/import'); ?>" enctype="multipart/form-data" class="form">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="import">File import:</label>
                <input type="file" name="import"/>
            </div>
            <button type="submit" name="submit" value="submit" class="btn btn-edufit-default">Import</button>
        </form>
        @if(!empty($result))
            <h2>Kết quả import</h2>
            <table class="table">
                <thead>
                <th>Loại kết quả</th>
                <th>Số lượng (dòng)</th>
                <th>Tải về</th>
                </thead>
                <tbody>
                <tr>
                    <td>Thành công</td>
                    <td>{{ $result['data']['success'] }}</td>
                    <td>
                        @if(!empty($result['data']['success_file_name']))
                            <a href="/uploads/{{ $result['data']['success_file_name'] }}"><i class="fa fa-lg fa-download"
                                                                                             aria-hidden="true"></i></a>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Trùng</td>
                    <td>{{ $result['data']['dup'] }}</td>
                    <td>
                        @if(!empty($result['data']['duplicate_file_name']))
                            <a href="/uploads/{{ $result['data']['duplicate_file_name'] }}"><i class="fa fa-lg fa-download"
                                                                                               aria-hidden="true"></i></a>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Lỗi</td>
                    <td>{{ $result['data']['failed'] }}</td>
                    <td>
                        @if(!empty($result['data']['error_file_name']))
                            <a href="/uploads/{{ $result['data']['error_file_name'] }}"><i class="fa fa-lg fa-download"
                                                                                           aria-hidden="true"></i></a>
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Tổng</td>
                    <td>{{ $result['data']['total'] }}</td>
                    <td></td>
                </tr>
                </tbody>
            </table>
            {!! $result['message'] !!}
        @endif
    </div>

</div>
@stop