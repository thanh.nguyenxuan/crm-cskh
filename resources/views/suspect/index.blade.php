@extends('layouts.main')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Danh sách data</h2>
        <div class="clearfix"></div>
    </div>

    <div class="x_content">
        <div id="suspect-search-form-container" class="collapse" style="margin-bottom: 20px">
            <form id="suspect-search-form" method="get" action="/suspect" >

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4">
                                    <label>Họ tên</label>
                                </div>
                                <div class="col-xs-7">
                                    <input type="text" name="name" class="form-control" value="{{ old('name') }}" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4">
                                    <label>SĐT</label>
                                </div>
                                <div class="col-xs-7">
                                    <input type="text" name="phone" class="form-control" value="{{ old('phone') }}" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4">
                                    <label>Email</label>
                                </div>
                                <div class="col-xs-7">
                                    <input type="text" name="email" class="form-control" value="{{ old('email') }}" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4">
                                    <label>Nguồn dẫn</label>
                                </div>
                                <div class="col-xs-7">
                                    <select name="source" class="form-control">
                                        <?php echo \App\Utils::generateOptions(\App\Source::listData(), old('source'), 'Tất cả')?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4">
                                    <label>Trạng thái</label>
                                </div>
                                <div class="col-xs-7">
                                    <select name="status" class="form-control">
                                        <?php echo \App\SuspectStatus::generateOptions(\App\SuspectStatus::listData(), old('status'), 'Tất cả')?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4">
                                    <label>Cơ sở</label>
                                </div>
                                <div class="col-xs-7">
                                    <select name="department_id" class="form-control">
                                        <?php echo \App\Utils::generateOptions(\App\Department::listData(), old('department_id'), 'Tất cả')?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4">
                                    <label>Người phụ trách</label>
                                </div>
                                <div class="col-xs-7">
                                    <select name="assignee" class="form-control">
                                        <?php echo \App\Utils::generateOptions(\App\User::listData(TRUE), old('assignee'), 'Tất cả')?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-8">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <label>Ngày tạo (từ ngày)</label>
                                        </div>
                                        <div class="col-xs-7">
                                            <input type="text" name="start_date" class="form-control" value="{{ old('start_date') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <label>Ngày tạo (đến ngày)</label>
                                        </div>
                                        <div class="col-xs-7">
                                            <input type="text" name="end_date" class="form-control"value="{{ old('end_date') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="text-right">
                    <button type="button" onclick="clearSuspectSearchForm();" class="btn btn-edufit-default">Xóa lọc <i class="fa fa-eraser"></i></button>
                    <button type="submit" class="btn btn-edufit-default">Tìm kiếm <i class="fa fa-filter"></i></button>
                </div>
            </form>
        </div>

        <div class="row" style="margin-bottom: 20px">
            <div class="col-md-6">
                <button class="btn btn-edufit-default" data-toggle="collapse" data-target="#suspect-search-form-container">Bộ lọc <i class="fa fa-toggle-down"></i></button>
            </div>
            <div class="col-md-6 text-right">
                <a class="btn btn-edufit-default" href="{{ route('suspect.create') }}">Thêm mới <i class="fa fa-plus"></i></a>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <th>Họ & tên</th>
                        <th>SĐT 1</th>
                        <th>SĐT 2</th>
                        <th>Email</th>
                        <th>Nguồn</th>
                        <th>Trạng thái</th>
                        <th>Người phụ trách</th>
                        <th>Cơ sở</th>
                        <th>Ngày tạo</th>
                        <th></th>
                        </thead>
                        <tbody>
                        @foreach($suspect_list as $suspect)
                            <tr>
                                <td><a href="{{ route('suspect.edit', $suspect->id) }}" target="_blank">{{ $suspect->name }}</a></td>
                                <td>{{ $suspect->phone1 }}</td>
                                <td>{{ $suspect->phone2 }}</td>
                                <td>{{ $suspect->email }}</td>
                                <td>{{ $suspect->source?$sourceListData[$suspect->source]:'' }}</td>
                                <td>{{ $suspect->status?$statusListData[$suspect->status]:'' }}</td>
                                <td>{{ $suspect->assignee?$userListData[$suspect->assignee]:'' }}</td>
                                <td>{{ $suspect->department_id?$departmentListData[$suspect->department_id]:'' }}</td>
                                <td>{{ !empty($suspect->created_at)?Carbon\Carbon::parse($suspect->created_at)->format('d/m/Y H:i:s'):'' }}</td>
                                <td>
                                    <a href="{{ route('suspect.edit', $suspect->id) }}" title="Chỉnh sửa">
                                        <i class="fa fa-lg fa-pencil" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>

<script>
    var suspectSearchForm = $('#suspect-search-form');
    var full_month_names = new Array("Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12");
    $.datepicker.regional["vi-VN"] =
        {
            closeText: "Đóng",
            prevText: "Trước",
            nextText: "Sau",
            currentText: "Hôm nay",
            monthNames: ["Tháng một", "Tháng hai", "Tháng ba", "Tháng tư", "Tháng năm", "Tháng sáu", "Tháng bảy", "Tháng tám", "Tháng chín", "Tháng mười", "Tháng mười một", "Tháng mười hai"],
            monthNamesShort: full_month_names,
            dayNames: ["Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"],
            dayNamesShort: ["CN", "Hai", "Ba", "Tư", "Năm", "Sáu", "Bảy"],
            dayNamesMin: ["CN", "T2", "T3", "T4", "T5", "T6", "T7"],
            weekHeader: "Tuần",
            dateFormat: "dd/mm/yy",
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: "",
            changeMonth: true,
        };
    $.datepicker.setDefaults($.datepicker.regional["vi-VN"]);
    suspectSearchForm.find('input[name="start_date"]').datepicker();
    suspectSearchForm.find('input[name="end_date"]').datepicker();

    function clearSuspectSearchForm()
    {
        suspectSearchForm.find('input[type="text"]').each(function (e){
            $(this).val('');
        });
        suspectSearchForm.find('select').each(function (e){
            $(this).val('');
        });
    }
</script>

@stop