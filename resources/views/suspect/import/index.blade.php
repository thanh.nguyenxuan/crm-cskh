@extends('layouts.main')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Lịch sử import lead</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <table class="table table-hover">
            <thead>
            <th>STT</th>
            <th>Thời điểm import</th>
            <th>Tổng</th>
            <th>Thành công</th>
            <th>Trùng</th>
            <th>Lỗi</th>
            <th>Người import</th>
            </thead>
            <tbody>
            @php $stt = 1; @endphp
            @foreach($import_list as $import)
                <tr>
                    <td>{{ $stt++ }}</td>
                    <td><strong>{{ Carbon\Carbon::parse($import->created_at)->format('d/m/Y H:i:s') }}</strong></td>
                    <td>
                        <strong>{{ $import->original_row_num }} </strong>
                        @if($import->original_row_num > 0)
                            <a href="{{ $import->original_file_url }}"><i class="fa fa-lg fa-download" aria-hidden="true"></i></a>
                        @endif
                    </td>
                    <td>
                        <strong>{{ $import->success_row_num }} </strong>
                        @if($import->success_row_num > 0)
                            <a href="{{ $import->success_file_url }}"><i class="fa fa-lg fa-download" aria-hidden="true"></i></a>
                        @endif
                    </td>
                    <td>
                        <strong>{{ $import->duplicate_row_num }} </strong>
                        @if($import->duplicate_row_num > 0)
                            <a href="{{ $import->duplicate_file_url }}"><i class="fa fa-lg fa-download" aria-hidden="true"></i></a>
                        @endif
                    </td>
                    <td>
                        <strong>{{ $import->error_row_num }} </strong>
                        @if($import->error_row_num > 0)
                            <a href="{{ $import->error_file_url }}"><i class="fa fa-lg fa-download" aria-hidden="true"></i></a>
                        @endif
                    </td>
                    <td><strong>{{ $import->creator }}</strong></td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>

</div>
@stop