@extends('layouts.main')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Chỉnh sửa data</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            @if(!empty($message))
                <div class="alert {!! $success?'alert-success':'alert-danger' !!}"
                     role="alert">{!! $message !!}</div>
            @endif
            <form class="form-horizontal" role="form" method="POST" action="{{ route('suspect.update', $suspect->id) }}">
                {!! method_field('patch') !!}
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4 text-right">
                                    <label>Họ tên</label>
                                </div>
                                <div class="col-xs-7">
                                    <input type="text" name="name" class="form-control" value="{{ old('name') ? old('name') : $suspect->name }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4 text-right">
                                    <label class="required">SĐT 1</label>
                                </div>
                                <div class="col-xs-7">
                                    <input type="text" name="phone1" class="form-control" value="{{ old('phone1') ? old('phone1') : $suspect->phone1 }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4 text-right">
                                    <label>SĐT 2</label>
                                </div>
                                <div class="col-xs-7">
                                    <input type="text" name="phone2" class="form-control" value="{{ old('phone2') ? old('phone2') : $suspect->phone2 }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4 text-right">
                                    <label>Email</label>
                                </div>
                                <div class="col-xs-7">
                                    <input type="email" name="email" class="form-control" value="{{ old('email') ? old('email') : $suspect->email }}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4">
                                    <label>Nguồn dẫn</label>
                                </div>
                                <div class="col-xs-7">
                                    <select name="source" class="form-control">
                                        <?php echo \App\Utils::generateOptions(\App\Source::listData(), (old('source') ? old('source') : $suspect->source), 'Tất cả')?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4">
                                    <label class="required">Trạng thái</label>
                                </div>
                                <div class="col-xs-7">
                                    <select name="status" class="form-control" required>
                                        <?php echo \App\SuspectStatus::generateOptions(\App\SuspectStatus::listData(), (old('status') ? old('status') : $suspect->status), 'Tất cả')?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <?php if(Auth::user()->hasRole('holding') || (Auth::user()->hasRole('admin') && in_array(\App\Department::HOLDING, Auth::user()->getManageDepartments()))){ ?>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4">
                                    <label class="required">Cơ sở</label>
                                </div>
                                <div class="col-xs-7">
                                    <?php if(empty($suspect->department_id)){ ?>
                                    <select name="department_id" class="form-control" required>
                                        <?php echo \App\Utils::generateOptions(\App\Department::listData(), (old('department_id') ? old('department_id') : $suspect->department_id), 'Chọn')?>
                                    </select>
                                    <?php }else{ ?>
                                    <label><?php echo \App\Department::find($suspect->department_id)->name?></label>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4">
                                    <label>Người phụ trách</label>
                                </div>
                                <div class="col-xs-7">
                                    <?php if(empty($suspect->assignee)){ ?>
                                    <select name="assignee" class="form-control">
                                        <?php echo \App\Utils::generateOptions(\App\User::listData(), (old('assignee') ? old('assignee') : $suspect->assignee), 'Chọn')?>
                                    </select>
                                    <?php }else{ ?>
                                    <label><?php echo \App\User::find($suspect->assignee)->username?></label>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-8">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-2">
                                    <label>Ghi chú</label>
                                </div>
                                <div class="col-xs-10">
                                    <textarea name="note" class="form-control" rows="5">{{ old('note') ? old('note') : $suspect->note }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-edufit-default">
                    Lưu
                </button>

                &emsp;

                <?php if(!empty($suspect->lead_id) && $suspect->status == \App\SuspectStatus::STATUS_CONVERTED){ ?>
                <a class="btn btn-default" href="/lead/edit/<?php echo $suspect->lead_id?>" target="_blank">Thông tin lead</a>
                <?php } ?>

                <?php if(empty($suspect->lead_id) && $suspect->status != \App\SuspectStatus::STATUS_CONVERTED){ ?>
                <button onclick="convertToLead(<?php echo $suspect->id?>)" type="button" class="btn btn-success">Chuyển đổi thành lead</button>
                <?php } ?>
            </form>
        </div>
    </div>

</div>

<script>
    function convertToLead(suspect_id)
    {
        if(confirm("Bạn có chắc muốn chuyển data này thành Lead?")){
            var data = new Object();
            data.suspect_id = suspect_id;
            data._token = "{{ csrf_token() }}";
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });

            $.post('/suspect/convertToLead', data, function(response,status,xhr){
                if(response.success){
                    alert("Chuyển đổi thành công");
                    location.reload();
                }else{
                    alert(response.message);
                }
            }, 'json');
        }

    }
</script>
@stop