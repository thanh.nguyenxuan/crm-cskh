@extends('layouts.main')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Thêm data mới</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="row">
            @if(!empty($message))
                <div class="alert {!! $success?'alert-success':'alert-danger' !!}"
                     role="alert">{!! $message !!}</div>
            @endif
            <form class="form-horizontal" role="form" method="POST" action="{{ route('suspect.store') }}">
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4 text-right">
                                    <label>Họ tên</label>
                                </div>
                                <div class="col-xs-7">
                                    <input type="text" name="name" class="form-control" value="{{ old('name') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4 text-right">
                                    <label class="required">SĐT 1</label>
                                </div>
                                <div class="col-xs-7">
                                    <input type="text" name="phone1" class="form-control" value="{{ old('phone1') }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4 text-right">
                                    <label>SĐT 2</label>
                                </div>
                                <div class="col-xs-7">
                                    <input type="text" name="phone2" class="form-control" value="{{ old('phone2') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4 text-right">
                                    <label>Email</label>
                                </div>
                                <div class="col-xs-7">
                                    <input type="email" name="email" class="form-control" value="{{ old('email') }}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4">
                                    <label>Nguồn dẫn</label>
                                </div>
                                <div class="col-xs-7">
                                    <select name="source" class="form-control">
                                        <?php echo \App\Utils::generateOptions(\App\Source::listData(), old('source'), 'Tất cả')?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4">
                                    <label class="required">Trạng thái</label>
                                </div>
                                <div class="col-xs-7">
                                    <select name="status" class="form-control" required>
                                        <?php echo \App\SuspectStatus::generateOptions(\App\SuspectStatus::listData(), old('status'), 'Tất cả')?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <?php if(Auth::user()->hasRole('holding') || (Auth::user()->hasRole('admin') && in_array(\App\Department::HOLDING, Auth::user()->getManageDepartments()))){ ?>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4">
                                    <label class="required">Cơ sở</label>
                                </div>
                                <div class="col-xs-7">
                                    <select name="department_id" class="form-control" required>
                                        <?php echo \App\Utils::generateOptions(\App\Department::listData(), old('department_id'), 'Tất cả')?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-4">
                                    <label>Người phụ trách</label>
                                </div>
                                <div class="col-xs-7">
                                    <select name="assignee" class="form-control">
                                        <?php echo \App\Utils::generateOptions(\App\User::listData(), old('assignee'), 'Tất cả')?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-8">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-xs-2">
                                    <label>Ghi chú</label>
                                </div>
                                <div class="col-xs-10">
                                    <textarea name="note" class="form-control" rows="5">{{ old('note') }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit" class="btn btn-edufit-default">
                    Lưu
                </button>
            </form>
        </div>
    </div>

</div>
@stop