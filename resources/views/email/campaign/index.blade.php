@include("includes.header")
<div class="container">
<h3>Danh sách chiến dịch gửi email</h3>
<table class="table table-hovered">
<thead>
	<tr>
		<th>STT</th>
		<th>Tên chiến dịch</th>
		<th>Loại chiến dịch</th>
		<th>Số lượng email</th>
		<th>Ghi chú</th>
		<th>Ngày tạo</th>
	</tr>
</thead>
<tbody>
@php $stt = 1 @endphp
	@foreach($list as $item)
		<tr>
			<td>{{$stt}}</td>
			<td><a href="{{ route('email-campaign.show', $item->id) }}">{{$item->name}}</a></td>
			<td>{{$item->type}}</td>
			<td>{{$item->count}}</td>
			<td>{{$item->notes}}</td>
			<td>{{$item->created_at}}</td>
		<tr>
		@php $stt++ @endphp
	@endforeach
</tbody>
</table>
</div>