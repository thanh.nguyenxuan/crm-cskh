@include('includes.header')
<script src="{{ env('APP_URL') . '/js/ckeditor/ckeditor.js' }}"></script>
<div class="container">
    <h3>Chỉnh sửa {{ $template->type }} template "{{ $template->name }}"</h3>
    <form method="post" action="{{ route('email-template.edit', $template->id) }}" enctype="multipart/form-data"
          class="form">
        {{ csrf_field() }}
        <div class="form-group">
            <textarea id="template_content" name="template_content">{{ $template->content }}</textarea>
        </div>
        <button id="save_button" name="save_button" value="save" type="submit" class="btn btn-primary">Lưu</button>
        <span>Cập nhật lần cuối vào
            lúc <strong>{{ !empty($template->updated_at)?\Carbon\Carbon::parse($template->updated_at)->format('d/m/Y H:i:s'):\Carbon\Carbon::parse($template->created_at)->format('d/m/Y H:i:s') }}</strong>
        </span>
    </form>
</div>
<script>
    CKEDITOR.replace('template_content');
</script>
</body>
</html>