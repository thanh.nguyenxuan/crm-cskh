@include('includes.header')
<div class="container">
    <h3>Chiến dịch: {{ $detail->name }}</h3>
    @if(!empty($detail->template))
        <a class="btn btn-primary" href="{{ route('email-template.edit', $detail->template) }}">Sửa email template</a>
    @endif
    <a class="btn btn-primary" href="{{ route('email-campaign.import', $detail->id) }}">Import</a>
    <!--
    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#delete-popup">Xóa DS email</button>
    -->
    @if(!empty($detail->has_attachment))
        <a class="btn btn-primary" href="{{ route('email-campaign.genAllPdf', $detail->id) }}" target="_blank">Khởi tạo
            file đính kèm</a>
    @endif
    <a class="btn btn-primary" href="{{ route('email.send', $detail->id) }}" target="_blank">Gửi tất cả email</a>
    <!-- Modal -->
    <div id="delete-popup" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Xóa hết email</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger">Bạn có chắc muốn xóa hết toàn bộ email trong danh sách email này.
                    </div>
                </div>
                <div class="modal-footer">
                    <form method="post" action="{{ route('email-campaign.destroy', $detail->id) }}">
                        {{ csrf_field() }}
                        <input name="_method" type="hidden" value="DELETE">
                        <button id="del-button" type="submit" class="btn btn-danger">Có</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Không</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <br/>
    <br/>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <tr>
                    <td>Tên chiến dịch:</td>
                    <td>{{ $detail->name }}</td>
                </tr>
                <tr>
                    <td>Tiêu đề email:</td>
                    <td>{{ $detail->subject }}</td>
                </tr>
                <tr>
                    <td>Số lượng email:</td>
                    <td>{{ $detail->count }}</td>
                </tr>
                <tr>
                    <td>Số lượng email đã gửi:</td>
                    <td>{{ $detail->sent_count }}</td>
                </tr>
                <tr>
                    <td>Số lượng email đã được mở:</td>
                    <td>{{ $detail->opened_count }}</td>
                </tr>
                <tr>
                    <td>Gửi lần cuối lúc:</td>
                    <td>{{ $detail->last_sent?Carbon\Carbon::parse($detail->last_sent)->format('d/m/Y H:i:s'):'' }}</td>
                </tr>
                @if($has_image)
                    <tr>
                        <td>Số lượng có ảnh:</td>
                        <td>{{ $has_image }}</td>
                    </tr>
                @endif
            </table>
        </div>
    </div>
    <h4>Danh sách email</h4>
    <table class="table table-hover table-bordered">
        <thead>
        <tr>
            <th></th>
            <th>STT</th>
            <th>Họ & tên</th>
            <th>Email</th>
            <th>Reply To</th>
            <th>File đính kèm</th>
            <th>Trạng thái</th>
            <th>Người tạo</th>
        </tr>
        </thead>
        <tbody>
        @php $stt = 1 @endphp
        @foreach($detail->email_list as $contact)
            <tr>
                <td><a class="btn btn-default" target="_blank"
                       href="{{ route('email-campaign.showEmail', $contact->id) }}">View Email</a>
                    <a class="btn btn-default" target="_blank" href="{{ route('email.sendTestEmail', $contact->id) }}">Send
                        test email</a>
                    <a class="btn btn-default" target="_blank" href="{{ route('email.sendPrivate', $contact->id) }}">Send
                        Email</a>
                    <a class="btn btn-default" target="_blank" href="{{ route('email.resend', $contact->id) }}">Resend
                        Email</a>
                </td>
                <td>{{ $stt++ }}</td>
                <td>{{ $contact->name }}</td>
                <td>{{ $contact->email }}</td>
                <td>{{ $contact->reply_to }}</td>
                <td>
                    @php $attachment_no = 0; @endphp
                    @foreach($contact->attachments as $attachment)
                        {{ ++$attachment_no }}. <a href="{{ $attachment->file_path }}"
                                                   target="_blank">{{ $attachment->file_name }}</a>
                        @if(!empty($attachment->status))
                            @if($attachment->status == 'found')
                                <span class="label-success">OK!</span>
                            @elseif($attachment->status =='notfound')
                                <span class="label-danger">Không tìm thấy!</span>
                            @endif
                        @endif
                        <br/>
                    @endforeach
                </td>
                <td>
                    @if($contact->status == 'sent')
                        <span class="text-success">
                            <strong>Đã gửi</strong> lúc <strong>@php echo $contact->sent_at?Carbon\Carbon::parse($contact->sent_at)->format('d/m/Y H:i:s'):''; @endphp</strong>
                        </span>
                    @else
                        <span class="text-info">Chưa gửi</span>
                    @endif
                    <br/>
                    @if(!empty($contact->opened_at))
                        <span class="text-success">
                            <strong>Đã được mở</strong> lúc <strong>@php echo $contact->opened_at?Carbon\Carbon::parse($contact->opened_at)->format('d/m/Y H:i:s'):''; @endphp</strong>
                        </span>
                    @else
                        <span class="text-info">Chưa được mở</span>
                    @endif
                </td>
                <td>{{ $contact->creator }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $("#image_file").change(function () {
        this.form.submit();
    });
    // $("del-button").click(function() {
    // this.form
    // });
</script>