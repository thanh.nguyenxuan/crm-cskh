@include('includes.header')
<div class="container">
    <h3>Import email vào chiến dịch {{ $list->name }}</h3>
    @if($list->id == 20)
        <div class="well well-sm">
            Tải về template nhập liệu tại <a href="/files/EmailImportTemplate_TBXT2018.xlsx">đây</a>.
        </div>
    @endif
    <form method="post" action="{{ route('email-campaign.import', $list->id) }}" enctype="multipart/form-data"
          class="form">
        {{ csrf_field() }}
        <div class="form-group">
            <input type="file" name="import"/>
        </div>
        <button type="submit" name="submit" value="submit">Import</button>
    </form>
    @if(!empty($result))
        <h2>Kết quả import</h2>
        <table class="table">
            <thead>
            <th>Loại kết quả</th>
            <th>Số lượng (dòng)</th>
            <th>Tải về</th>
            </thead>
            <tbody>
            <tr>
                <td>Thành công</td>
                <td>{{ $result['data']['success'] }}</td>
                <td><a href="/uploads/Import_Success.xlsx">Tải về</a></td>
            </tr>
            <tr>
                <td>Trùng</td>
                <td>{{ $result['data']['dup'] }}</td>
                <td><a href="/uploads/Import_Duplicate.xlsx">Tải về</a></td>
            </tr>
            <tr>
                <td>Lỗi</td>
                <td>{{ $result['data']['failed'] }}</td>
                <td><a href="/uploads/Import_Failed.xlsx">Tải về</a></td>
            </tr>
            <tr>
                <td>Tổng</td>
                <td>{{ $result['data']['total'] }}</td>
                <td></td>
            </tr>
            </tbody>
        </table>
{!! $result['message'] !!}
@endif