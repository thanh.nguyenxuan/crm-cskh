<html>
<head>
	<style type="text/css">
		table, th, td {
			border: 1px solid #000;
			border-collapse: collapse;
		}
	</style>
</head>
<body>
<p>Bạn <strong>{{ $email->name }}</strong> thân mến,</p>
<p>Hội đồng tuyển sinh Trường Đại học FPT chính thức thông báo kết quả kỳ thi tuyển sinh vào Ngành {{ $email->major }} hệ đại học chính quy Trường Đại học FPT ngày 14/05/2017 của bạn như sau:</p>
<p>Bài 1: <strong>{{ $email->grade1 }}</strong></p>
<p>Bài 2: <strong>{{ $email->grade2 }}</strong></p>
<p>Điểm chuẩn vào Ngành {{ $email->major }} hệ đại học chính quy Trường Đại học FPT trong kỳ thi ngày 14/05/2017 là {{ $email->total }}/105 điểm, vì vậy bạn chưa đủ điểm trúng tuyển vào Trường Đại học FPT trong kỳ thi này.</p>
<p>Chúc bạn đạt kết quả cao trong kỳ thi sắp tới của Trường Đại học FPT.</p>
<p>Thân chào Bạn !</p>
<p>*********************************************************************</p>
<p>Phòng tuyển sinh - Trường Đại Học FPT</p>
<p>Website: <a href="http://daihoc.fpt.edu.vn">http://daihoc.fpt.edu.vn</a></p>
<p>Hotline: 04(08) 73001866/(0236) 7301866</p>
</body>
</html>