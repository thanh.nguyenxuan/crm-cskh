<html>
<head>
	<style type="text/css">
		table, th, td {
			border: 1px solid #000;
			border-collapse: collapse;
		}
	</style>
</head>
<body>
<p>Bạn <strong>{{ $email->name }}</strong> thân mến,</p>
 
<p>Hội đồng tuyển sinh Trường Đại học FPT chính thức thông báo và chúc mừng bạn đã <strong><em>trúng tuyển & đạt học bổng {{ $email->scholarship_percent }}%</em></strong> kỳ thi tuyển sinh vào <strong>Ngành {{ $email->major }}</strong> hệ đại học chính quy Trường Đại học FPT ngày 14/05/2017 với điểm số như sau:</p>
<p>Bài 1: {{ $email->grade1 }}/90 điểm</p>
<p>Bài 2: {{ $email->grade2 }}/15 điểm</p>
<p><strong>Tổng điểm: {{ $email->grade_total }}/105 điểm</strong></p>
<p>Tại Trường Đại học FPT, các sinh viên luôn được khuyến khích rèn luyện ý chí và khát khao mong muốn thành công, xây dựng một sự nghiệp vững chắc trong giai đoạn hội nhập toàn cầu. Chúng tôi rất vui mừng và chào đón bạn sẽ tham gia đội ngũ sinh viên năng động, tích cực của Trường Đại học FPT.</p>
<p>Gửi kèm thư này là Hướng dẫn để bạn hoàn thành thủ tục nhập học trước khi chính thức trở thành Tân sinh viên của Trường Đại học FPT năm 2017.</p>
<p>Một lần nữa, xin chúc mừng và chào đón bạn tại Trường Đại học FPT.</p>

<p>Thân chào Bạn !</p>
<p>*********************************************************************</p>
<p>Phòng tuyển sinh - Trường Đại Học FPT</p>
<p>Website: <a href="http://daihoc.fpt.edu.vn">http://daihoc.fpt.edu.vn</a></p>
<p>Hotline: 04(08) 73001866/(0236) 7301866</p>
</body>
</html>