<html>
<head>
<style type="text/css">
html {
	font-family: Arial, Helvetica, sans-serif;
}
body {
	margin: 0 40px;
	font-size: 14px;
}
p {
	margin-right: 50px;
}
h1 {
	text-transform: uppercase;
	color: #f37125;
	text-align: center;
	font-size: 20px;
}

h2 {
	color: #f37125;
	font-size: 1.2em;
}
a {
	text-decoration: none;
	color: #f37125;
}

a:visited {
	color: #f37125;
}

</style>
</head>
<body>
<div style="text-align: center;"><a href="http://daihoc.fpt.edu.vn/tuyen-sinh-da-nang/?utm_source=EmailMKT&utm_medium=DNloclan1&utm_campaign=50NBDN"><img src="http://crm.daihocfpt.edu.vn/images/dn/banner.jpg"></a></div>
<h1 style="text-transform: uppercase; color: #f06c2c;">Để không phải nói: "Giá như tôi không lựa chọn như vậy..."</h1>
<p style="font-size: 1.2em;"><strong>{{ $email->name }}</strong> thân mến,</p>
<p>Trường đại học không chỉ là “nơi” bạn sẽ gắn kết trong 4 năm tới mà còn quyết định ”bạn là ai khi bước ra từ cánh cổng đại học”. Dưới đây là lời khuyên dành cho bạn về những yếu tố lựa chọn trường đại học:</p>
<h2>Chất lượng đào tạo & bằng cấp</h2>
<p>Một trường học tập có chất lượng đào tạo tốt sẽ trang bị cho bạn nền tảng kiến thức, hệ thống kỹ năng mềm cần thiết và trải nghiệm giá trị. Thêm vào đó, bằng cấp của trường có được quốc tế công nhận không? Sau khi tốt nghiệp, bạn có dễ dàng học lên cao, hay làm việc ở các môi trường quốc tế, thậm chí là ở nước ngoài?</p>
<p>>> Xem thêm: <a style="color: #f37125;" href="http://daihoc.fpt.edu.vn/le-tot-nghiep-lon-nhat-trong-lich-su-truong-dai-hoc-fpt/?utm_source=EmailMKT&utm_medium=DNloclan1&utm_campaign=50NBDN">Bằng cấp Đại học FPT có giá trị như thế nào?</a>
<h2>Môi trường học tập và hoạt động ngoại khóa của trường</h2>
<p>"Làm gì để trải qua những ngày sinh viên ý nghĩa nhất?" Bên cạnh học tập, việc tham gia các hoạt động rất quan trọng, nó giúp bạn trang bị những kỹ năng mềm cần thiết nhanh chóng hòa nhập với môi trường công việc khi mới bắt đầu.</p>
<p>>> Xem thêm: <a style="color: #f37125;" href="http://daihoc.fpt.edu.vn/giang-duong-o-dai-hoc-fpt-soi-dong-nhu-the-nao/?utm_source=EmailMKT&utm_medium=DNloclan1&utm_campaign=50NBDN">Giảng đường Đại học FPT sôi động như thế nào?</a></p>
<h2>Tỉ lệ sinh viên có việc làm sau tốt nghiệp</h2>
<p>Có bao nhiêu % sinh viên sau khi tốt nghiệp đại học có thể xin được việc đúng ngành nghề và thành công trong sự nghiệp – đây chắc hẳn là yếu tố quan trọng nhất mà bạn nên quan tâm về ngôi trường đại học mình sẽ chọn.</p>
<p>>> Xem thêm: <a style="color: #f37125;" href="http://daihoc.fpt.edu.vn/dai-hoc-viet-duy-nhat-gianh-5-sao-cho-ty-le-viec-lam/?utm_source=EmailMKT&utm_medium=DNloclan1&utm_campaign=50NBDN">Đại học Việt duy nhất giành 5 sao cho tỉ lệ việc làm</a></p>
<div style="color: #f37125; font-size: 1.2em; text-align: center; font-weight: bold;"><a style="color: #f37125;" href="http://daihoc.fpt.edu.vn/tuyen-sinh-da-nang/?utm_source=EmailMKT&utm_medium=DNloclan1&utm_campaign=50NBDN">ĐẠI HỌC FPT<br />
TUYỂN THẲNG 19 ĐIỂM THI THPTQG<br />
ƯU ĐÃI 30% HỌC PHÍ tại ĐÀ NẴNG<br />
TẶNG HỌC BỔNG FPT SOFTWARE TRỊ GIÁ 5.000.000 VNĐ</a>
</strong>
</div>
<div style="text-align: center;">
<a style="color: #f37125;" href="http://daihoc.fpt.edu.vn/tuyen-sinh-da-nang/?utm_source=EmailMKT&utm_medium=DNloclan1&utm_campaign=50NBDN"><div style="display: inline-block; padding: 10px 15px; color: #fff; background-color: #f06c2c; border-radius: 7px; text-align: center; font-weight: bold; text-transform: uppercase; margin-top: 10px; font-size: 1.3em;">Đăng ký ngay</div></a>
</div>
<p>Dù đã có sự lựa chọn cho mình hay chưa, hãy cân nhắc lời khuyên trên để tự tin bước ra cánh cổng đại học, không phải nói rằng “giá như tôi không lựa chọn như vậy”. Chúc bạn thành công!</p>
<div style="float: right">
Trân trọng,<br />
Trường Đại học FPT.
</div>
<img src="https://www.google-analytics.com/collect?v=1&tid=UA-89648819-1&t=event&ec=email&ea=open&el=DaNangBoSung&cid={{ $email->id }}" />
</body>
</html>