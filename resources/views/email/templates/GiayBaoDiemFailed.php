<html>
<head>
	<style type="text/css">
		body {

			font-size: 10pt;
			line-height: 12pt;
			font-family: DejaVu Serif, Helvetica, serif;
		}
		
		h1 {
			font-size: 18pt;
			text-transform: uppercase;
			text-align: center;
		}
		div.main {
			margin: 0.5cm 0.5cm 0.5cm 0.5cm;
		}
		div.footer {
			margin: 0 0.5cm 0.5cm 0.2cm;
		}
		.signature {
			font-weight: bold;
			margin-top: 3em;
		}
		p, strong, em {
			line-height: 1.5em;
			margin: 0;
		}
		.page-break {page-break-after: always;}
		hr {
			border: 0;
			height: 1pt;
			background: #000;
			margin-bottom: 10pt;
		}
	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<div class="header">
	<div style="margin-top: 1cm; margin-bottom: 0.5cm;">
		<center>
			<img src="/var/www/html/public/images/logo.png" />
		</center>
	</div>
</div>
<div class="main">
<p><em>{{ $email->place }}, ngày {{ date('d') }}, tháng {{ date('m') }}, năm {{ date('Y') }}</em></p>
<div style="margin-left: 30%">
		<p><strong><em>Thân gửi: Bạn {{ $email->name }}</em></strong></p>
		<p><strong><em>Địa chỉ: {{ $email->contact_name }}, {{ $email->address }}, {{ $email->phone1 }}{{ !empty($email->phone1) && !empty($email->phone2)?' - ':''}} {{$email->phone2}}</em></strong></p>
</div>
<h1 style="margin-top: 1cm">Thư báo điểm</h1>
<p>Bạn <strong>{{ $email->name }}</strong> thân mến,</p>
<p>Hội đồng tuyển sinh Trường Đại học FPT chính thức thông báo kết quả kỳ thi tuyển sinh vào Ngành {{ $email->major }} hệ đại học chính quy Trường Đại học FPT ngày 14/05/2017 của bạn như sau:</p>
<p>Bài 1: <strong>{{ $email->grade1 }}</strong></p>
<p>Bài 2: <strong>{{ $email->grade2 }}</strong></p>
<p>Điểm chuẩn vào Ngành {{ $email->major }} hệ đại học chính quy Trường Đại học FPT trong kỳ thi ngày 14/05/2017 là {{ $email->total }}/105 điểm, vì vậy bạn chưa đủ điểm trúng tuyển vào Trường Đại học FPT trong kỳ thi này.</p>
<p>Chúc bạn đạt kết quả cao trong kỳ thi sắp tới của Trường Đại học FPT.</p>
<div style="margin-left: 50%" class="signature">
HỘI ĐỒNG TUYỂN SINH<br/>
TRƯỜNG ĐẠI HỌC FPT
</div>
</div>
<div class="footer" style="position: absolute; bottom: 0">
<center>
	<img style="max-width: 100%" src="/var/www/html/public/images/pdf_footer.png" />
</center>
</div>
</body>
</html>