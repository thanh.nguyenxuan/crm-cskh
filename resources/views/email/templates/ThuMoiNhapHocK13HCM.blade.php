<html>
<head>
	<style type="text/css">
		body {

			font-size: 10pt;
			line-height: 12pt;
			font-family: Times, serif;
		}
		
		h1 {
			font-size: 18pt;
			text-transform: uppercase;
			text-align: center;
		}
		div.main {
			margin: 0.5cm 0.5cm 0.5cm 0.5cm;
		}
		div.footer {
			margin: 0 0.5cm 0.5cm 0.2cm;
		}
		.signature {
			font-weight: bold;
			margin-top: 3em;
			text-align: center;
		}
		p, strong, em {
			line-height: 1.5em;
			margin: 0;
		}
		.page-break {page-break-after: always;}
		hr {
			border: 0;
			height: 1pt;
			background: #000;
			margin-bottom: 10pt;
		}
		td {
			padding: 0 45px;
		}
		table {
			width: 100%;
		}
	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<div class="header">
	<div style="margin-top: -2cm; margin-bottom: 0.5cm;">
		<center>
			<img src="/var/www/html/public/images/logo.png" />
		</center>
	</div>
</div>
<div class="main">
<h1 style="margin-top: 1cm">THƯ MỜI NHẬP HỌC</h1>
<p><strong>TRƯỜNG ĐẠI HỌC FPT</strong> trân trọng thông báo và chúc mừng:</p>
<table border="0">
	<tr>
		<td>Thí sinh: <strong>{{ $email->name }}</strong></td>
		<td>Giới tính: {{ $email->gender }}</td>
	</tr>
	<tr>
		<td>Năm sinh: {{ $email->birthyear }}</td>
		<td>CMND: {{ $email->id_passport }}</td>
	</tr>
</table>
<p><strong>Đã chính thức trở thành TÂN SINH VIÊN trường ĐẠI HỌC FPT – hệ đào tạo ĐẠI HỌC CHÍNH QUY</strong></p>
<table border="0">
	<tr>
		<td>Ngành học: <strong>{{ $email->major }}</strong></td>
		<td>Niên khóa: 2017 - 2021</td>
	</tr>
	<tr>
		<td>Khóa học: 13</td>
		<td>Địa điểm học: TP. Hồ Chí Minh</td>
	</tr>
</table>
<p>Là trường Đại học được thành lập bởi tập đoàn FPT, trường Đại học FPT thấu hiểu những điều mà nhà tuyển dụng tìm kiếm ở nguồn nhân lực chất lượng cao trong bối cảnh hội nhập toàn cầu. 11 năm đào tạo - 11 năm cháy bỏng khát vọng đổi thay nền giáo dục Việt Nam. Đến nay, Đại học FPT tự hào là môi trường đào tạo uy tín, chắp cánh cho hàng ngàn sinh viên Việt Nam vươn ra thế giới -  Sẵn sàng cho sự nghiệp toàn cầu.</p>
<p>Nhà trường trân trọng thông báo đến Quý phụ huynh, Tân sinh viên lịch tập trung và thời gian diễn ra các hoạt động đầu năm. Sinh viên sắp xếp nhập trường đúng thời gian và tham dự các hoạt động đầy đủ.</p>
<p>Chào đón bạn tại Đại học FPT.</p>
<p>Trân trọng!</p>
<div style="margin-left: 50%" class="signature">
TRƯỜNG ĐẠI HỌC FPT TP.HCM<br />
TM. HỘI ĐỒNG TUYỂN SINH<br />
CHỦ TỊCH<br />
PHÓ HIỆU TRƯỞNG<br />
Trần Ngọc Tuấn
</div>
</div>
<div class="footer" style="position: absolute; bottom: -1cm">
<center>
	<img style="max-width: 100%" src="/var/www/html/public/images/pdf_footer.png" />
</center>
</div>
</body>
</html>