<html>
<head>
	<style type="text/css">
		table, th, td {
			border: 1px solid #000;
			border-collapse: collapse;
		}
	</style>
</head>
<body>
<p>Bạn <strong>{{ $email->name }}</strong> thân mến,</p>
 
<p>Hội đồng tuyển sinh Trường Đại học FPT chúc mừng bạn đã trúng tuyển vào Ngành <strong>{{ $email->major }}</strong> hệ đại học chính quy Trường Đại học FPT theo diện <strong>xét tuyển học bạ THPT</strong>.</p>
<p>Theo yêu cầu của Bộ Giáo dục & Đào tạo về việc các trường có Đề án tuyển sinh riêng phải công bố danh sách thí sinh trúng tuyển đã xác nhận nguyện vọng nhập học trên Cổng thông tin của Bộ, đề nghị bạn nộp bản gốc <strong><em>Giấy chứng nhận kết quả thi THPT Quốc gia</em></strong> về trường <strong>trước 17h ngày 22/07/2017</strong> bằng một trong hai cách sau:</p>
<ul>
	<li>Nộp trực tiếp tại các phòng tuyển sinh của trường.</li>
	<li>Nộp qua đường bưu điện về các Phòng tuyển sinh của trường.</li>
</ul>
<p><strong><em>Lưu ý:</em></strong></p>
<p>
<ul>
	<li>Cách thức chuyển phát nhanh hoặc chuyển phát ưu tiên nếu nộp qua đường bưu điện. Ngoài phong bì ghi thêm thông tin "Nộp Giấy chứng nhận kết quả thi THPT".</li>
	<li>Để tránh bị thất lạc cũng như được chuyển đến không đúng thời gian quy định, nhà trường khuyến khích thí sinh nộp bản chính Giấy chứng nhận kết quả thi THPT Quốc gia theo hình thức trực tiếp tại trường.</li>
</ul>

<p>*********************************************************************</p>
<p>Phòng tuyển sinh - Trường Đại Học FPT</p>
<p>Website: <a href="http://daihoc.fpt.edu.vn">http://daihoc.fpt.edu.vn</a></p>
<p>Hotline: 024(028) 73001866/(0236) 7301866</p>
<img src="https://www.google-analytics.com/collect?v=1&tid=UA-89648819-1&t=event&ec=email&ea=open&el=QG_XHB&cid={{$email->excel_order}}" />
</body>
</html>