<html>
<head>
	<style type="text/css">
		table, th, td {
			border: 1px solid #000;
			border-collapse: collapse;
		}
	</style>
</head>
<body>
<p>Bạn <strong>{{ $email->name }}</strong> thân mến,</p>
 
<p>Hội đồng tuyển sinh Trường Đại học FPT chúc mừng bạn đã <strong>đủ điều kiện xét tuyển</strong> vào Ngành <strong>{{ $email->major }}</strong> hệ đại học chính quy Trường Đại học FPT theo diện xét tuyển.</p>
 
<p>Tại Trường Đại học FPT, các sinh viên luôn được khuyến khích rèn luyện ý chí và khát khao mong muốn thành công, xây dựng một sự nghiệp vững chắc trong giai đoạn hội nhập toàn cầu. Chúng tôi rất vui mừng và chào đón bạn sẽ tham gia đội ngũ sinh viên năng động, tích cực của Trường Đại học FPT.</p>
 
<p><strong><em>Nhân dịp ĐH FPT chính thức là thành viên hiệp hội CDIO Thế giới, ĐH FPT hỗ trợ 6.000.000 VNĐ chi phí mua máy tính xách tay cho thí sinh nhập học tại Đà nẵng trước 1/6/2017 và miễn lệ phí nhập học (4.600.000 VNĐ) cho thí sinh nhập học tại Hà Nội và Hồ Chí Minh từ 20/5/2017 – 31/5/2017.</em></strong></p>
 
<p>Gửi kèm mail này là thư báo trúng tuyển, hướng dẫn nhập học, phiếu đăng ký nhập học của nhà Trường trong file đính kèm. Gia đình vui lòng đọc kỹ lại hướng dẫn và quy định tài chính của Trường trước khi nhập học. <strong>Nếu có thắc mắc vui lòng liên hệ với Cán bộ tư vấn trực tiếp của nhà Trường</strong>.</p>

<p>*Thí sinh đủ điều kiện trở thành sinh viên Đại học FPT muốn nhập học cần thực hiện 2 bước sau để hoàn thành thủ tục nhập học:</p>
 
- <strong>Bước 1</strong>: Nộp phí đăng ký nhập học và Học phí Học kỳ I theo Quy định tài chính sinh viên Trường Đại học FPT hiện hành khi lựa chọn cơ sở học như sau;
<table>
<thead>
<tr>
<th>Đại học FPT Hà Nội và Hồ Chí Minh</th>
<th>Đại học FPT Đà Nẵng</th>
</tr>
</thead>
<tbody>
<tr>
<td>
<p>Thí sinh nộp <strong>14.950.000 VNĐ</strong> (tương ứng với mục (1) + (2) bên dưới) <strong>vào tài khoản ngân hàng của Trường, bao gồm</strong>:
<p>(1)  Phí đăng ký nhập học: <strong>4.600.000 VNĐ</strong>.</p>
<p>(2)  Học phí 1 mức học tiếng Anh dự bị: <strong>10,350,000 VNĐ</strong> (Nếu thuộc diện được miễn học tiếng Anh, học phí này sẽ chuyển thành học phí cho học kỳ đầu tiên)</p>
</td>
<td>
<p>Thí sinh nộp <strong>11.850.000 VNĐ</strong> (tương ứng với mục (1) + (2) bên dưới) <strong>vào tài khoản ngân hàng của Trường, bao gồm</strong>:
<p>(1)  Phí đăng ký nhập học: <strong>4.600.000 VNĐ</strong>.</p>
<p>(2)  Học phí 1 mức học tiếng Anh dự bị: <strong>7.250.000 VNĐ</strong> (Nếu thuộc diện được miễn học tiếng Anh, học phí này sẽ chuyển thành học phí cho học kỳ đầu tiên)</p>
</td>
</tr>
</tbody>
</table>

<p>Thông tin tài khoản Trường Đại học FPT:</p>
<table>
<thead>
<tr>
<th>Đại học FPT Hà Nội</th>
<th>Đại học FPT Hồ Chí Minh</th>
<th>Đại học FPT Đà Nẵng</th>
</tr>
</thead>
<tbody>
<tr>
<td>
<p>Số TK: 00006969009</p>
<p>Tên TK: Trường Đại học FPT</p>
<p>Địa chỉ: Ngân hàng Thương Mại cổ phần Tiên Phong – Chi nhánh Hoàn Kiếm</p>
<p>Nội dung nộp tiền: &lt;TSTT - Họ và tên thí sinh – Số CMND của thí sinh – Lệ phí NH 2017&gt;</p>
</td>
<td>
<p>Số TK: 6150201010751</p>
<p>Tên TK: Viện Đào Tạo Quốc Tế FPT TP.HCM</p>
<p>Địa chỉ: Ngân hàng Nông nghiệp và Phát triển Nông thôn VN - CN Xuyên Á – TP.HCM</p>
<p>Nội dung nộp tiền: &lt;TSTT - Họ và tên thí sinh – Số CMND của thí sinh – Lệ phí NH 2017&gt;</p>
</td>
<td>
<p>Số TK: 2002201204827</p>
<p>Tên TK: Trung tâm đào tạo quốc tế FPT Đà Nẵng</p>
<p>Tại Ngân hàng: Nông nghiệp và phát triển nông thôn – Chi nhánh Liên Chiểu – Đà Nẵng</p>
<p>Nội dung nộp tiền: &lt;TSTT - Họ và tên thí sinh – Số CMND của thí sinh – Lệ phí NH 2017&gt;</p>
</td>
</tr>
</table>
- <strong>Bước 2</strong>: Chụp ảnh giấy nộp tiền vào tài khoản ngân hàng của trường và các giấy tờ, hồ sơ nhập học yêu cầu dưới đây và gửi vào hòm thư <a href="mailto:daihocfpt@fpt.edu.vn">daihocfpt@fpt.edu.vn</a>. Khi tới trường tập trung học, thí sinh mang đầy đủ hồ sơ bản cứng nộp cho Trường Đại học FPT.
<ul>
<li><a href="http://daihoc.fpt.edu.vn/media/2016/12/phieu-nhap-hoc-dai-hoc-fpt.doc">Phiếu đăng ký nhập học</a> theo mẫu của Trường Đại học FPT (file đính kèm);</li>
<li>Phí đăng ký nhập học và Học phí 1 mức Tiếng Anh dự bị theo quy định tài chính sinh viên Trường Đại học FPT hiện hành;</li>
<li>02 bản sao chứng thực Bằng tốt nghiệp THPT (có thể nộp giấy chứng nhận tốt nghiệp THPT tạm thời và bổ sung sau khi có Bằng);</li>
<li>01 bản gốc giấy báo điểm thi THPTQG năm 2017;</li>
<li>01 bản sao chứng thực Học bạ THPT;</li>
<li>01 bản gốc: Đơn xin chuyển trường và giấy tờ chứng minh đang là sinh viên đại học hệ chính quy của một trường đại học có cùng khối thi (nếu đang là sinh viên);</li>
<li>02 bản sao chứng thực CMND;</li>
<li>02 ảnh 3×4;</li>
<li>Chứng chỉ tiếng Anh TOEFL iBT từ 80 hoặc IELTS (Học thuật) từ 6.0 hoặc quy đổi tương đương (nếu có);</li>
<li>01 bản sao chứng thực giấy khai sinh.</li>
</ul>

<p>Thân chào Bạn !</p>
<p>*********************************************************************</p>
<p>Phòng tuyển sinh - Trường Đại Học FPT</p>
<p>Website: <a href="http://daihoc.fpt.edu.vn">http://daihoc.fpt.edu.vn</a></p>
<p>Hotline: 04(08) 73001866/(0236) 7301866</p>
</body>
</html>