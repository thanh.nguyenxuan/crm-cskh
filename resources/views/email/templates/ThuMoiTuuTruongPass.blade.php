<html>
<head>
	<style type="text/css">
		@page { margin: 0; }
		body, table, td {
			margin: 0;
			font-size: 8pt;
			line-height: 12pt;
			font-family: DejaVu Serif, Helvetica, serif;
		}
		
		h1 {
			font-size: 13pt;
			text-transform: uppercase;
			text-align: center;
		}
		div.main {
			margin: 0 1.5cm 0.5cm 1.5cm;
		}
		div.footer {
			margin: 0 0.5cm 0.5cm 0.2cm;
		}
		.signature {
			font-weight: bold;
			margin-top: 3em;
		}
		
		p {
			text-indent: 20pt;
		}
		p, strong, em {
			line-height: 1.5em;
			margin: 0;
		}
		.page-break {page-break-after: always;}
		hr {
			border: 0;
			height: 1pt;
			background: #000;
			margin-bottom: 10pt;
		}
		ul {
			margin: 0;
		}
	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<div class="header">
	<div style="margin-top: 0.5cm; margin-bottom: 0; height: 64px; width: auto;">
		<center>
			<img src="/var/www/html/public/images/logo.png" />
		</center>
	</div>
</div>
<div class="main">
<p style="text-align: right"><em>Hà Nội, ngày {{ date('d') }}, tháng {{ date('m') }}, năm {{ date('Y') }}</em></p>
<h1>Thư mời tựu trường</h1>
<p><strong><em>Kính gửi:</em></strong> Ông/Bà <strong>{{ $email->contact_name }}</strong> thân mến,</p>
<p>Phụ huynh của thí sinh <strong>{{ $email->name }}</strong></p>
<p>Hội đồng tuyển sinh Trường Đại học FPT nhiệt liệt chúc mừng gia đình và thí sinh <strong>{{ $email->name }}</strong> đã vượt qua kỳ thi tuyển vào hệ đại học chính quy Trường ĐH FPT ngày 14/05/2017. Đây là thành công quan trọng, ghi nhận những cố gắng và nỗ lực bước đầu của em sau 12 năm học tập phổ thông.</p>
<p>Trường ĐH FPT trân trọng kính mời gia đình và thí sinh <strong>{{ $email->name }}</strong> tới tham gia <strong>NGÀY HỘI TỰU TRƯỜNG</strong> vào ngày 11/06/2017 tại Khu Công nghệ cao Hòa Lạc, Thạch Thất, Hà Nội.</p>
<p>Chương trình được tổ chức dành riêng cho những thí sinh xuất sắc vượt qua kỳ thi tuyển và kỳ thi học bổng vào Đại học FPT tháng 5 vừa qua và các thí sinh đủ điều kiện xét tuyển vào Đại học FPT nhằm hướng dẫn thí sinh và gia đình các thông tin cần thiết để chuẩn bị cho quá trình học tập tại Đại học FPT:</p>
<ul>
<li>Hướng dẫn hoàn thiện thủ tục nhập học;</li>
<li>Đăng ký áo đồng phục, võ phục, bộ tài liệu hướng dẫn tân sinh viên;</li>
<li>Đăng ký phòng ở tại Ký túc xá;</li>
<li>Đăng ký tham gia các Câu lạc bộ sinh viên;</li>
<li>Kiểm tra tiếng Anh đầu vào;</li>
<li>Giải đáp các thắc mắc về cách thức học tập, sinh hoạt và quản lý sinh viên.</li>
</ul>
<p>Mọi thông tin chi tiết xin vui lòng liên hệ: <strong><em>0473001866 hoặc 0984471866</em></strong> để đăng ký tham dự hội thảo.</p>
<p>Trường Đại học FPT bố trí xe đưa đón phụ huynh và học sinh lên khu CNC Hòa Lạc, Thạch Thất với thời gian & địa điểm như sau:</p>
<p><strong>Thời gian: 	7h00 Chủ Nhật, ngày 11 tháng 6 năm 2017</strong></p>
<p><strong>Địa điểm: Tòa nhà Detech, số 8 Tôn Thất Thuyết, Mỹ Đình, Nam Từ Liêm, Hà Nội</strong> (đối diện bến xe Mỹ Đình)</p>
<p>Trân trọng kính mời các vị phụ huynh tới tham dự!</p>
<br />
<table>
<tr>
<td width="65%">
<table style="border-radius: 20px; border: solid 1px #000; padding: 5px;">
<tr>
<td colspan="2" style="text-align: center"><strong>Lịch trình đón tiếp phụ huynh</strong></td>
</tr>
<tr>
<td><strong>7h00–7h15</strong></td><td>: Tập trung tại Số 8 Tôn Thất Thuyết để xe đón lên KCN Hoà Lạc, Thạch Thất.</td>
</tr>
<tr>
<td><strong>7h40–8h00</strong></td><td>: Tới trường ĐH FPT tại Hòa Lạc.</td>
</tr>
<tr>
<td><strong>8h00–9h30</strong></td><td>: Phụ huynh và học sinh làm thủ tục nhập học, tham quan trường và tham gia "Ngày hội các Câu lạc bộ".</td>
</tr>
<tr>
<td><strong>9h30–11h30</strong></td><td>: Phụ huynh dự hội thảo, trao đổi trực tiếp với đại diện ĐH FPT. Học sinh tập trung làm bài kiểm tra tiếng Anh đầu vào, đăng ký và nộp tiền Ký túc xá.</td>
</tr>
<tr>
<td><strong>11h30-12h30</strong></td><td>: Dùng tiệc ngọt và nhận các ấn phẩm của ĐH FPT.</td>
</tr>
<tr>
<td><strong>12h30–13h00</strong></td><td>: Tập trung lên xe về số 8 Tôn Thất Thuyết, kết thúc chương trình.</td>
</tr>
</table>
</td>
<td>
	<div style="text-align: center; font-weight: bold;">
	<p>TM. HỘI ĐỒNG TUYỂN SINH</p>
    <p>CHỦ TỊCH</p>
	<br />
	<p><img style="height: 20pt;" src="/var/www/html/public/images/tuantn_signature.png" /></p>
	<br />
	<p>PHÓ HIỆU TRƯỞNG</p>
	<p>Trần Ngọc Tuấn</p>
	</div>
</td>
</tr>
</table>
</div>
<div class="footer" style="position: absolute; bottom: 0">
<center>
	<img style="max-width: 100%" src="/var/www/html/public/images/pdf_footer.png" />
</center>
</div>
</body>
</html>