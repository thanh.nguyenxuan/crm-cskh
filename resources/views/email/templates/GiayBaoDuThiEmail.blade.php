<html>
<head>
	<style type="text/css">
		body {
			font-family: Times New Roman, serif;
			font-size: 12pt;
			line-height: 1.5em;
		}
		h1 {
			font-size: 18pt;
			text-transform: uppercase;
			text-align: center;
		}
		h1.subtitle {
			font-size: 14pt;
			text-transform: uppercase;
			text-align: center;
			margin-bottom: 1cm;
		}
		.image {
			display: inline-block;
			width: 33%;
		}
		.image img {
			width: 4cm;
		}
		.personal-info {
			display: inline-block;
			width: 66%;
			font-weight: bold;
			font-style: italic;
		}
		hr {
			border: 0;
			height: 1px;
			background: #000;
		}
		p {
			line-height: 1em;
		}
		p.special {
			text-indent: 0.5cm;
		}
		ol {
			font-weight: bold;
		}
		ol li span {
			font-weight: normal;
		}
		
		div.fillable .label {
			display: inline-block;
			overflow: hidden;
		}
		div.fillable {
			display: inline-block;
			overflow: hidden;
			padding: 0.3cm;
			width: 45%;
			border: 1px solid #000; 
			display: inline-block;
			white-space: nowrap;
		}
		
		div.fillable .row {
			overflow: hidden;
			white-space: nowrap;
		}
		div.dotted {
			display: inline-block;
			width: 500px;
			overflow: hidden;
			white-space: nowrap;
		}
		
		ul {
		  list-style-type: none;
		}

		ul li:before {
		  content: '-';
		  position: absolute;
		  margin-left: -20px;
		}
		ul li li:before {
		  content: '+';
		  position: absolute;
		  margin-left: -20px;
		}

		
	</style>
</head>
<body>
<p>Hội đồng tuyển sinh Trường Đại học FPT trân trọng gửi Giấy báo dự thi hệ đại học chính quy - kỳ thi ngày 14/05/2017 cho thí sinh:</p>
<ul>
	<li>Họ tên: <strong>{{ $email->name }}</strong></li>
	<li>Ngày sinh: <strong>{{ $email->birthdate->format('d/m/Y') }}</strong></li>
	<li>Số CMND: <strong>{{ $email->id_passport }}</strong></li>
</ul>
<p><em>*Chú ý:</em></p>
<ul>
<li>Thí sinh xuất trình bản in hoặc bản mềm Giấy báo dự thi này và Chứng minh nhân dân trước khi vào phòng thi</li>
<li>Xem Số báo danh, Phòng thi, Thời gian thi và Sơ đồ địa điểm thi trên website của Trường theo địa chỉ: http://daihoc.fpt.edu.vn từ 8h30 ngày 13/05/2017</li>
<li>Các vật dụng được phép mang vào phòng thi:
	<ul>
		<li>Giấy Chứng minh thư nhân dân bản chính</li>
		<li>Giấy báo dự thi</li>
		<li>Các vật dụng: 02 chiếc bút chì loại 2B hoặc HB, 01 bút bi, 01 cục tẩy</li>
	</ul>
</li>
<li>Để chuẩn bị tốt cho kỳ thi sắp tới, thí sinh tham khảo thêm các thông tin sau:
	<ul>
		<li>Nội quy kỳ thi</li>
		<li>Thi thử: http://daihoc.fpt.edu.vn/thi-thu/</li>
		<li>Tham khảo thêm: <a href="https://www.facebook.com/daihocfpt">https://www.facebook.com/daihocfpt</a></li>
	</ul>
</li>
</ul>
<p>Hội đồng tuyển sinh Trường Đại học FPT đề nghị các thí sinh nghiêm túc thực hiện đúng các yêu cầu và quy định của kỳ thi.</p>
<p>Chúc các thí sinh làm bài tốt và thể hiện được hết các năng lực của mình.</p>
<p>*************************************************************************************</p>
<p>Hội đồng tuyển sinh Trường Đại học FPT</p>
<p>Website: <a href="http://daihoc.fpt.edu.vn">http://daihoc.fpt.edu.vn</a></p>
<p>Hotline: 04(08) 73001866</p>
</body>
</html>