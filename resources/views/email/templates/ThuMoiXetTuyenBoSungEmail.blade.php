<html>
<head>
<style type="text/css">
html {
	font-family: Arial, Helvetica, sans-serif;
}
body {
	margin: 0 40px;
	font-size: 14px;
}
p {
	margin-right: 50px;
}
h1 {
	text-transform: uppercase;
	color: #f06c2c;
	text-align: center;
	font-size: 20px;
}

a {
	text-decoration: none;
	color: #f06c2c;
}

a:visited {
	color: #f06c2c;
}
</style>
</head>
<body>
<div style="text-align: center;"><a href="http://daihoc.fpt.edu.vn/dang-ky-truc-tuyen/?utm_source=EmailMKT&utm_medium=dkdt13.8&utm_campaign=planemkt8"><img src="http://crm.daihocfpt.edu.vn/images/xetbs/banner.jpg"></a></div>
<h1 style="text-transform: uppercase; color: #f06c2c;">Đại học FPT xét tuyển bổ sung 500 chỉ tiêu</h1>
<p style="font-size: 1.2em;"><strong>{{ $email->name }}</strong> thân mến,</p>
<p>Bạn đủ điều kiện học đại học theo quy định của Bộ GD & ĐT?</p>
<p>Bạn đạt:</p>
<ul>
	<li>19 điểm trở lên tổ hợp Toán + 2 môn bất kỳ trong kỳ thi THPT Quốc Gia?</li>
	<li>Hoặc 7.0 điểm học bạ THPT (điểm TB 3 môn theo tổ hợp: A00, A01, D01, D90, D96)?</li>
</ul>
<div style="text-align: center; color: #f06c2c; font-weight: bold;">
<p>Xin chúc mừng!</p>
<p>Bạn đủ điều kiện được TUYỂN THẲNG vào Đại học FPT!</p>
<p>Cơ hội chỉ dành cho các bạn nộp hồ sơ xét tuyển sớm nhất!</p>
</div>
<p>Đại học FPT còn 500 chỉ tiêu cho đợt xét nguyện vọng bổ sung. Trường sẽ ngưng nhận hồ sơ ngay khi hết chỉ tiêu. Hãy chọn cho mình một <a style="color: #f06c2c;" href="http://daihoc.fpt.edu.vn/nganh-hoc/?utm_source=EmailMKT&utm_medium=mocanhcuadh&utm_campaign=planemkt8">chuyên ngành phù hợp</a> tại Đại học FPT và nộp hồ sơ đăng ký xét tuyển ngay <a style="color: #f06c2c;" href="http:/daihoc.fpt.edu.vn/dang-ky-truc-tuyen/?utm_source=EmailMKT&utm_medium=tuyenthangvaodhfpt&utm_campaign=planemkt8">tại đây</a> để nắm bắt cơ hội trở thành tân sinh viên của trường nhé. </p>
<p>{{ $email->name }}, khi cánh cửa này khép lại, sẽ có cánh cửa khác mở ra, quan trọng là chúng ta phải biết nắm lấy cơ hội và biến nó trở thành kết quả mình mong đợi. Publilius Syrus có câu: "Thường trong khi dừng lại để nghĩ, chúng ta đã để lỡ cơ hội."</p>
<div style="color: #f06c2c;" >
<p style="color: #f06c2c;"><strong>Thông tin về Đại học FPT:</strong></p>
<p><a  style="color: #f06c2c;" href="http://daihoc.fpt.edu.vn/tuyen-sinh/?utm_source=EmailMKT&utm_medium=mocanhcuadh&utm_campaign=planemkt8">Top ngành học HOT 2017 của Đại học FPT</a></p>
<p><a  style="color: #f06c2c;" href="http://daihoc.fpt.edu.vn/dai-hoc-fpt-khong-danh-cho-nguoi-bo-cuoc/?utm_source=EmailMKT&utm_medium=mocanhcuadh&utm_campaign=planemkt8">Đại học FPT không dành cho người bỏ cuộc</a></p>
<p><a  style="color: #f06c2c;" href="http://daihoc.fpt.edu.vn/hoan-toan-bi-chinh-phuc-boi-dai-hoc-fpt/?utm_source=EmailMKT&utm_medium=mocanhcuadh&utm_campaign=planemkt8">Đại học FPT đã chinh phục tôi như thế nào?</a></p>
<p><a style="color: #f06c2c;" href="http://daihoc.fpt.edu.vn/hanh-trinh-toi-google-cua-chang-cuu-sinh-vien-dai-hoc-fpt/?utm_source=EmailMKT&utm_medium=mocanhcuadh&utm_campaign=planemkt8">Hành trình tới Google của cựu SV ĐH FPT</a></p>
</p>
</div>
<img src="https://www.google-analytics.com/collect?v=1&tid=UA-89648819-1&t=event&ec=email&ea=open&el=XetTuyenBS500&cid={{ $email->id }}" />
</body>
</html>