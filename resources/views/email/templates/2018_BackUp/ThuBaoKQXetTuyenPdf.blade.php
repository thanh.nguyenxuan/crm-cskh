<html>
<head>
    <style type="text/css">
        body {

            font-size: 13pt;
            line-height: 13pt;
            font-family: "Times", serif;

        }

        h1 {
            font-size: 15pt;
            text-transform: uppercase;
            text-align: center;
        }

        div.main {
            margin: 0.5cm 0.5cm 0.5cm 0.5cm;
        }

        div.footer {
            margin: 0 0.5cm 0.5cm 0.2cm;
        }

        .signature {
            font-weight: bold;
            margin-top: 3em;
            text-align: center;
        }

        p, strong, em {
            line-height: 1.5em;
            margin: 0;
        }

        .page-break {
            page-break-after: always;
        }

        hr {
            border: 0;
            height: 1pt;
            background: #000;
            margin-bottom: 10pt;
        }

        table {
            width: 100%;
        }

        table, td, th {
            border: 1px solid black;
            border-collapse: collapse;
        }
    </style>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<div class="header">
    <div style="margin-top: -2cm; margin-bottom: 0.5cm;">
        <center>
            <img src="{{ public_path() }}/images/logo.png"/>
        </center>
    </div>
</div>
<div class="main">
    <h1 style="margin-top: 1cm">KẾT QUẢ XÉT TUYỂN</h1>
    <p>Bạn <strong>{{ $data->name }}</strong> thân mến,</p>

    <p>Hội đồng tuyển sinh trường Đại học FPT chúc mừng bạn đã <strong><em>vượt qua vòng sơ tuyển</em></strong> vào
        Ngành <strong>{{ $data->major }}</strong> hệ đại học chính quy Trường Đại học FPT theo diện xét tuyển học bạ
        THPT. Bạn chỉ cần tốt nghiệp THPT là chính thức trúng tuyển vào trường.</p>

    <p>Điểm số xét tuyển học bạ của bạn như sau:</p>
    <p>Môn/Tổ hợp môn xét tuyển: <strong>{{ $data->subject_group }}</strong></p>
    <p>Tổng điểm TB môn/tổ hợp môn xét tuyển: <strong>{{ $data->average_grade }}</strong></p>
    <p>Để trở thành sinh viên hệ chính quy Trường Đại học FPT, thí sinh cần hoàn thành các thủ tục nhập học theo quy
        định của trường.</p>
    <p>Gửi kèm thư này là Hướng dẫn các bước để hoàn thành thủ tục nhập học của Trường Đại học FPT năm 2018.</p>
    <p>Một lần nữa, xin chúc mừng và chào đón bạn tại Trường Đại học FPT.</p>
    <div style="margin-left: 50%" class="signature">
        TRƯỜNG ĐẠI HỌC FPT<br/>
        TM. HỘI ĐỒNG TUYỂN SINH<br/>
        CHỦ TỊCH<br/>
        HIỆU TRƯỞNG<br/>
        Nguyễn Khắc Thành
    </div>
    <div class="footer" style="position: absolute; bottom: -1cm">
        <center>
            <img style="max-width: 100%" src="{{ public_path() }}/images/pdf_footer_tstt.png"/>
        </center>
    </div>
    <div class="page-break"></div>
    <div class="header">
        <div style="margin-top: -2cm; margin-bottom: 0.5cm;">
            <center>
                <img src="{{ public_path() }}/images/logo.png"/>
            </center>
        </div>
    </div>
    <h1>HƯỚNG DẪN THỦ TỤC NHẬP HỌC</h1>
    <p><strong>I. Thời gian</strong></p>
    Trước 17h00 ngày 31/7/2018.
    <p><strong>II. Hướng dẫn nhập học</strong></p>
    <p>Thí sinh đủ điều kiện trở thành sinh viên Đại học FPT muốn nhập học cần thực hiện 3 bước sau để hoàn thành thủ
        tục nhập học. Thí sinh đã vượt qua vòng sơ tuyển, chưa tốt nghiệp THPT có thể thực hiện bước 1 và 2 để giữ chỗ.
        Nhà trường sẽ ưu tiên các thí sinh đã giữ chỗ cho tới khi hết chỉ tiêu.</p>

    <p><strong>Bước 1</strong>: Nộp lệ phí đăng ký nhập học và Học phí 1 mức Tiếng Anh dự bị theo Quyết định số
        275/QĐ-ĐHFPT ngày 21/3/2018 của Hiệu trưởng Trường Đại học FPT như sau:</p>
    <p><strong>Tại Hà Nội và Hồ Chí Minh</strong></p>
    <p>Thí sinh nộp <strong>14.950.000 VNĐ</strong> bao gồm:</p>
    <p>(1) Phí đăng ký nhập học: <strong>4.600.000 VNĐ</strong>;</p>
    <p>(2) Học phí 1 mức học tiếng Anh dự bị: <strong>10,350,000 VNĐ</strong> (Nếu thuộc diện được miễn học tiếng Anh,
        học phí này sẽ chuyển thành học phí cho học kỳ đầu tiên).</p>
    <p><strong>Thông tin tài khoản ngân hàng của Trường Đại học FPT</strong></p>
    <table border="1">
        <thead>
        <tr>
            <td><strong>Tài khoản</strong></td>
            <td><strong>Hà Nội</strong></td>
            <td><strong>Tp.HCM</strong></td>
        </tr>
        <tr>
            <td><strong>Chủ tài khoản</strong></td>
            <td>Trường Đại học FPT</td>
            <td>Viện đào tạo quốc tế FPT Tp.HCM</td>
        </tr>
        <tr>
            <td><strong>Số tài khoản</strong></td>
            <td>00006969009</td>
            <td>6150201010751</td>
        </tr>
        <tr>
            <td><strong>Ngân hàng</strong></td>
            <td>Ngân hàng Thương mại Cổ phần Tiên Phong – Chi nhánh Hoàn Kiếm</td>
            <td>Ngân hàng Nông nghiệp và Phát triển Nông thôn VN – Chi nhánh Xuyên Á – TP.HCM</td>
        </tr>
        </thead>
    </table>
    <strong><em>Lưu ý:</em></strong>
    <ul>
        <li><em>Sinh viên thôi học trước ngày đầu tiên (là ngày học hoặc tập trung đầu tiên, lịch cụ thể được ghi trong
                thông báo gửi tới sinh viên) của khóa học hoặc học kỳ sẽ được trả lại 80% học phí thực nộp trừ đi phí
                bản quyển (nếu có). Học phí thực nộp là học phí nộp đã trừ đi hỗ trợ laptop và các hỗ trợ tài chính
                khác.</em></li>
        <li><em>Sinh viên thôi học trong vòng 02 tuần kể từ ngày đầu tiên của khóa học hoặc học kỳ sẽ được hoàn trả 50%
                học phí thực nộp sau khi trừ đi phí bản quyền (nếu có).</em></li>
        <li><em>Sinh viên thôi học sau 02 tuần kể từ ngày đầu tiên sẽ không được hoàn trả học phí đã đóng.</em></li>
    </ul>
    <p><strong>Bước 2</strong>: Chụp ảnh biên lai nộp tiền và hồ sơ nhập học yêu cầu dưới đây và gửi vào email của
        trường. Tiêu đề thư: <strong><em>Hồ sơ nhập học - &lt;Họ tên thí sinh&gt; - &lt;Số CMT&gt; - &lt;Địa điểm đăng
                ký học&gt;</em></strong></p>

    <p>Sinh viên nộp đầy đủ hồ sơ bản cứng cho Trường Đại học FPT trực tiếp hoặc qua đường bưu điện trước ngày đầu tiên
        đi học.</p>

    <p><strong>Hồ sơ nhập học bao gồm: </strong></p>
    <ol>
        <li><em>Phiếu nhập học</em>(theo mẫu của Trường Đại học FPT);</li>
        <li>Bản công chứng Học bạ trung học phổ thông: 01 bản;</li>
        <li>Bản công chứng Chứng minh thư nhân dân: 02 bản;</li>
        <li>Bản photo công chứng Giấy khai sinh: 01 bản;</li>
        <li>Ảnh 3x4 (bỏ vào phong bì nhỏ, ghi rõ họ tên và ngày tháng năm sinh ra bên ngoài phong bì và đằng sau ảnh):
            02 ảnh,
        </li>
        <li>Bản photo công chứng Chứng chỉ tiếng Anh TOEFL iBT từ 80 hoặc IELTS từ 6.0 hoặc quy đổi tương đương (nếu
            có);
        </li>
        <li>Lệ phí đăng ký nhập học và học phí (theo QĐ số 275/QĐ – ĐH FPT ngày 21 tháng 03 năm 2018 của Hiệu trưởng
            Trường Đại học FPT):
        </li>
    </ol>
    <p><strong>Bước 3: </strong>Bổ sung hồ sơ nhập học</p>

    <p>Thí sinh bổ sung hồ sơ nhập học, hồ sơ bao gồm:</p>
    <ol>
        <li>Bản photo công chứng Bằng tốt nghiệp trung học phổ thông (có thể nộp Giấy chứng nhận tốt nghiệp trung học
            phổ thông tạm thời và bổ sung sau khi có Bằng tốt nghiệp): 02 bản;
        </li>
        <li>Giấy chứng nhận kết quả kỳ thi trung học phổ thông Quốc gia 2018 do Bộ Giáo dục và Đào tạo cấp: 01 bản
            gốc.
        </li>
    </ol>
</div>
<div class="footer" style="position: absolute; bottom: -1cm">
    <center>
        <img style="max-width: 100%" src="{{ public_path() }}/images/pdf_footer_tstt.png"/>
    </center>
</div>
</body>
</html>