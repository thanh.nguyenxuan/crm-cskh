<html>
<head>
    <style type="text/css">
        table, th, td {
            border: 1px solid #000;
            border-collapse: collapse;
        }
    </style>
</head>
<body>
Thân gửi: <strong>{{ $data->name }}</strong>

<br />

Hội đồng tuyển sinh trường Đại học FPT chúc mừng bạn đã vượt qua vòng sơ tuyển vào <strong>Ngành {{ $data->major }}</strong> hệ đại học chính quy Trường Đại học FPT theo diện xét tuyển học bạ THPT. Bạn chỉ cần tốt nghiệp THPT là chính thức trúng tuyển vào trường.<br />

Điểm số xét tuyển của bạn như sau:<br /><br />

<strong>Môn/tổ hợp môn xét tuyển: {{ $data->subject_group }}</strong><br />

<strong>Tổng điểm môn/ tổ hợp xét tuyển: {{ $data->average_grade }}</strong>

<br /><br />

Để trở thành sinh viên hệ chính quy Trường Đại học FPT, thí sinh cần hoàn thành các thủ tục nhập học theo quy định của trường.<br />

Gửi kèm thư này là Hướng dẫn các bước để hoàn thành thủ tục nhập học của Trường Đại học FPT năm 2018.<br /><br />

Bạn và gia đình vui lòng đọc kỹ lại hướng dẫn nhập học và Quy định tài chính của Trường trước khi nhập học. <strong>Nếu có thắc mắc vui lòng liên hệ với Cán bộ tư vấn trực tiếp của nhà Trường</strong>.<br /><br />

Thí sinh đủ điều kiện trở thành sinh viên Đại học FPT muốn nhập học cần thực hiện 3 bước sau để hoàn thành thủ tục nhập học. Thí sinh đã vượt qua vòng sơ tuyển, chưa tốt nghiệp THPT có thể thực hiện bước 1 và 2 để giữ chỗ. Nhà trường sẽ ưu tiên các thí sinh đã giữ chỗ cho tới khi hết chỉ tiêu.<br />

<strong>Bước 1</strong>: Nộp lệ phí đăng ký nhập học và Học phí 1 mức Tiếng Anh dự bị theo Quyết định số 275/QĐ-ĐHFPT ngày 21/3/2018 của Hiệu trưởng Trường Đại học FPT như sau:<br />

<strong>Tại Hà Nội và Hồ Chí Minh</strong><br />

Thí sinh nộp <strong>14.950.000 VNĐ</strong> bao gồm:<br />

(1) Phí đăng ký nhập học: <strong>4.600.000 VNĐ</strong>;<br />

(2) Học phí 1 mức học tiếng Anh dự bị: <strong>10,350,000 VNĐ</strong> (Nếu thuộc diện được miễn học tiếng Anh, học phí này sẽ chuyển thành học phí cho học kỳ đầu tiên).<br />

<strong>Thông tin tài khoản ngân hàng của Trường Đại học FPT</strong><br />
<table>
    <thead>
    <tr>
        <td>Đại học FPT Hà Nội</td>
        <td>Đại học FPT Hồ Chí Minh</td>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Số TK: 00006969009

            Tên TK: Trường Đại học FPT

            Địa chỉ: Ngân hàng Thương Mại cổ phần Tiên Phong – Chi nhánh Hoàn Kiếm

            Nội dung nộp tiền: &lt;TSTT - Họ và tên thí sinh – Số CMND của thí sinh – Lệ phí NH 2018&gt;</td>
        <td>Số TK: 6150201010751

            Tên TK: Viện Đào Tạo Quốc Tế FPT TP.HCM

            Địa chỉ: Ngân hàng Nông nghiệp và Phát triển Nông thôn VN - CN Xuyên Á – TP.HCM

            Nội dung nộp tiền: &lt;TSTT - Họ và tên thí sinh – Số CMND của thí sinh – Lệ phí NH 2018&gt;</td>
    </tr>
    </tbody>
</table><br />
<strong><em>Lưu ý:</em></strong><br />
<ul>
    <li><strong><em>Sinh viên thôi học trước ngày đầu tiên (là ngày học hoặc tập trung đầu tiên, lịch cụ thể được ghi trong thông báo gửi tới sinh viên) của khóa học hoặc học kỳ sẽ được trả lại 80% học phí thực nộp trừ đi phí bản quyển (nếu có). Học phí thực nộp là học phí nộp đã trừ đi hỗ trợ laptop và các hỗ trợ tài chính khác.</em></strong></li>
    <li><strong><em>Sinh viên thôi học trong vòng 02 tuần kể từ ngày đầu tiên của khóa học hoặc học kỳ sẽ được hoàn trả 50% học phí thực nộp sau khi trừ đi phí bản quyền (nếu có).</em></strong></li>
    <li><strong><em>Sinh viên thôi học sau 02 tuần kể từ ngày đầu tiên sẽ không được hoàn trả học phí đã đóng.</em></strong></li>
</ul>
<strong>Bước 2</strong>: Chụp ảnh biên lai nộp tiền và hồ sơ nhập học yêu cầu dưới đây và gửi vào email của trường. Tiêu đề thư: <strong><em>Hồ sơ nhập học - &lt;Họ tên thí sinh&gt; - &lt;Số CMT&gt; - &lt;Địa điểm đăng ký học&gt;</em></strong><br />

Sinh viên nộp đầy đủ hồ sơ bản cứng cho Trường Đại học FPT trực tiếp hoặc qua đường bưu điện trước ngày đầu tiên đi học.<br />

<strong>Hồ sơ nhập học bao gồm: </strong><br />
<ol>
    <li>Phiếu nhập học (theo mẫu của Trường Đại học FPT);</li>
    <li>Bản công chứng Học bạ trung học phổ thông: 01 bản;</li>
    <li>Bản công chứng Chứng minh thư nhân dân: 02 bản;</li>
    <li>Bản photo công chứng Giấy khai sinh: 01 bản;</li>
    <li>Ảnh 3x4 (bỏ vào phong bì nhỏ, ghi rõ họ tên và ngày tháng năm sinh ra bên ngoài phong bì và đằng sau ảnh): 02 ảnh,</li>
    <li>Bản photo công chứng Chứng chỉ tiếng Anh TOEFL iBT từ 80 hoặc IELTS từ 6.0 hoặc quy đổi tương đương (nếu có);</li>
    <li>Lệ phí đăng ký nhập học và học phí (theo QĐ số 275/QĐ – ĐH FPT ngày 21 tháng 03 năm 2018 của Hiệu trưởng Trường Đại học FPT):</li>
</ol>
<strong>Bước 3</strong>: Bổ sung hồ sơ nhập học <strong>trước ngày 31/07/2018</strong><br />

Thí sinh bổ sung hồ sơ nhập học, hồ sơ bao gồm:<br />
<ol>
    <li>Bản photo công chứng Bằng tốt nghiệp trung học phổ thông (có thể nộp Giấy chứng nhận tốt nghiệp trung học phổ thông tạm thời và bổ sung sau khi có Bằng tốt nghiệp): 02 bản;</li>
    <li>Giấy chứng nhận kết quả kỳ thi trung học phổ thông Quốc gia 2018 do Bộ Giáo dục và Đào tạo cấp: 01 bản gốc.</li>
</ol>
<strong> </strong><br />

<strong>Một lần nữa, xin chúc mừng và chào đón bạn tại Trường Đại học FPT.</strong>

<br />

<p>*********************************************************************</p>
<p>Phòng tuyển sinh - Trường Đại học FPT</p>
<p>Website: <a href="http://daihoc.fpt.edu.vn">http://daihoc.fpt.edu.vn</a></p>
<p>Hotline: 024(028) 73001866</p>
</body>
</html>