<html>
<head>
<style type="text/css">
html {
	font-family: Arial, Helvetica, sans-serif;
}
body {
	margin: 0 40px;
	font-size: 14px;
}
p {
	margin-right: 50px;
}
h1 {
	text-transform: uppercase;
	color: #f37125;
	text-align: center;
	font-size: 20px;
}

h2 {
	color: #f37125;
	font-size: 1.2em;
}
a {
	text-decoration: none;
	color: #f37125;
}

a:visited {
	color: #f37125;
}

div > img {
	width: 160px;
}
</style>
</head>
<body>
<div style="text-align: center;"><a href="http://daihoc.fpt.edu.vn/dang-ky-truc-tuyen/?utm_source=EmailMKT&utm_medium=emailoclan2&utm_campaign=50NBDN"><img src="http://crm.daihocfpt.edu.vn/images/dn/banner2.png"></a></div>
<h1 style="text-transform: uppercase; color: #f06c2c;">Đà nẵng - Thành phố "thiên đường" của tân sinh viên năm 2017</h1>
<p style="font-size: 1.2em;"><strong>{{ $email->name }}</strong> thân mến,</p>
<p>Đà Nẵng không chỉ là thành phố lý tưởng cho những chuyến du lịch, trải nghiệm mà còn là điểm đến tuyệt vời để học đại học trong năm tới. Dưới đây là những lý do khiến bạn nhất định phải học tại Đà Nẵng.</p>
<div>
<div style="width: 75%; display: inline-block;">
<h2>No 1. Mệt bở hơi tai với những địa điểm “check-in” tuyệt đẹp</h2>
<p>Sáng uống café Long, chạy bộ đón bình minh dọc biển Mỹ Khê, tối ngồi “Pub” ven sông Hàn, thứ bảy chủ nhật đi xem cầu Rồng phun lửa... Có quá nhiều địa điểm đẹp khiến bạn khó có thời gian rảnh rỗi sau giờ học.</p>
</div>
<div style="width: 15%; display: inline-block;">
	<img src="http://crm.daihocfpt.edu.vn/images/dn/no1.jpg">
</div>
</div>
<div>
<div style="width: 75%; display: inline-block;">
<h2>No 2. Lạc lối trong thiên đường ẩm thực vừa rẻ, vừa ngon</h2>
<p>Điểm cộng hấp dẫn của Đà Nẵng là những địa điểm ăn uống phong phú. Học tại Đà Nẵng, bạn tha hồ thưởng thức các đặc sản “siêu ngon” từ biển và rừng. Vì là thành phố biển nên hải sản ở Đà Nẵng ngon nhưng rẻ hơn nhiều so với những miền biển khác.</p>
</div>
<div style="width: 15%; display: inline-block;">
	<img src="http://crm.daihocfpt.edu.vn/images/dn/no2.jpg">
</div>
</div>
<div>
<div style="width: 75%; display: inline-block;">
<h2>No 3. Vất vả vì có quá nhiều sự kiện phải tham gia</h2>
<p>Đà Nẵng không chỉ đẹp, thân thiện mà còn cực kỳ thú vị và chẳng lúc nào thiếu niềm vui với rất nhiều sự kiện hấp dẫn của thành phố: pháo hoa, lễ hội ánh sáng, marathon, mặc bikini nhảy flashmob trên biển…</p>
</div>
<div style="width: 15%; display: inline-block;">
	<img src="http://crm.daihocfpt.edu.vn/images/dn/no3.jpg">
</div>
</div>
<div>
<div style="width: 75%; display: inline-block;">
<h2>No 4. Bội thực ngoại ngữ vì gặp người nước ngoài quá nhiều</h2>
<p>Thành phố du lịch với sự ghé thăm của nhiều du khách nên việc suốt ngày “chạm trán” với bạn bè quốc tế là chuyện thường ngày.</p>
</div>
<div style="width: 15%; display: inline-block;">
	<img src="http://crm.daihocfpt.edu.vn/images/dn/no4.jpg">
</div>
</div>
<div>
<div style="width: 75%; display: inline-block;">
<h2>No 5. Quá tải với nhiều học bổng giá trị khi học tập tại Đà Nẵng trong năm 2017</h2>
<p>Nếu đạt >= 19 điểm thi THPT QG (tổ hợp Toán + 2 môn bất kỳ) hoặc: >= 7.0 điểm TB 3 môn trong 5 kỳ theo kết quả học bạ THPT và nhập học ĐH FPT tại Đà Nẵng từ 21/8 đến 5/9, bạn có cơ hội nhận ngay học bổng trị giá 5.000.000 VNĐ từ FPT Software hoặc học bổng <strong>"Chọn đúng ngành - Học đúng đam mê"</strong> trị giá tối đa 5.000.000 VNĐ.</p>
</div>
<div style="width: 15%; display: inline-block;">
	<img src="http://crm.daihocfpt.edu.vn/images/dn/no5.jpg">
</div>
</div>
<div style="text-align: center;">
<a style="color: #f37125;" href="http://daihoc.fpt.edu.vn/dang-ky-truc-tuyen/?utm_source=EmailMKT&utm_medium=emailoclan2&utm_campaign=50NBDN"><div style="display: inline-block; padding: 10px 15px; color: #fff; background-color: #f06c2c; border-radius: 7px; text-align: center; font-weight: bold; text-transform: uppercase; margin-top: 10px; font-size: 1.3em;">ĐH FPT tuyển 100 chỉ tiêu bổ sung tại Đà Nẵng<br />Ưu đãi 30% học phí và tặng học bổng trị giá 5 triệu đồng/suất</div></a>
</div>
<p>Để biết thêm thông tin chi tiết về các suất học bổng khi nhập học tại Đà Nẵng năm 2017, vui lòng liên hệ (0236) 730 1866 hoặc đăng ký tư vấn.
<h2>Thông tin thêm về Đại học FPT tại Đà Nẵng:</h2>
<a style="color: #f37125;" href="http://daihoc.fpt.edu.vn/tuyen-sinh-da-nang/?utm_source=EmailMKT&utm_medium=emailoclan2&utm_campaign=50NBDN">Top ngành học hot 2017 Đại học FPT tại Đà Nẵng</a><br />
<a style="color: #f37125;" href="http://daihoc.fpt.edu.vn/5-ly-do-khien-sinh-vien-dai-hoc-fpt-ngai-hoc-tai-da-nang/?utm_source=EmailMKT&utm_medium=emailoclan2&utm_campaign=50NBDN">5 lý do khiến sinh viên “ngại” học Đại học FPT tại Đà Nẵng</a><br />
<a style="color: #f37125;" href="http://daihoc.fpt.edu.vn/nam-bat-co-hoi-hoc-tap-va-viec-lam-tai-thanh-pho-tre-nang-dong-nhat-mien-trung/?utm_source=EmailMKT&utm_medium=emailoclan2&utm_campaign=50NBDN">Nắm bắt cơ hội học tập và làm việc tại thành phố năng động nhất miền Trung</a><br />
<img src="https://www.google-analytics.com/collect?v=1&tid=UA-89648819-1&t=event&ec=email&ea=open&el=DaNangBoSung2&cid={{ $email->id }}" />
</body>
</html>