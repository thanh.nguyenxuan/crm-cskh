<html>
<head>
	<style type="text/css">
		body {

			font-size: 10pt;
			line-height: 12pt;
			font-family: DejaVu Serif, Helvetica, serif;
		}
		
		h1 {
			font-size: 18pt;
			text-transform: uppercase;
			text-align: center;
		}
		div.main {
			margin: 0 0.5cm 0.5cm 0.5cm;
		}
		div.footer {
			margin: 0 0.5cm 0.5cm 0.2cm;
		}
		.signature {
			font-weight: bold;
			margin-top: 3em;
		}
		p, strong, em {
			line-height: 1.5em;
			margin: 0;
		}
		.page-break {page-break-after: always;}
		hr {
			border: 0;
			height: 1pt;
			background: #000;
			margin-bottom: 10pt;
		}
	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<div class="header">
	<div style="margin-top: 0; margin-bottom: 0;">
		<center>
			<img src="/var/www/html/public/images/logo.png" />
		</center>
	</div>
</div>
<div class="main">
<p><em>{{ $email->place }}, ngày {{ date('d') }}, tháng {{ date('m') }}, năm {{ date('Y') }}</em></p>
<div style="margin-left: 30%">
		<p><strong><em>Thân gửi: Bạn {{ $email->name }}</em></strong></p>
</div>
<h1>Thư báo kết quả học bổng</h1>
<p>Bạn <strong>{{ $email->name }}</strong> thân mến,</p>
<p>Hội đồng tuyển sinh Trường Đại học FPT chính thức thông báo và chúc mừng bạn đã <strong><em>trúng tuyển & đạt học bổng {{ $email->scholarship_percent }}%</em></strong> kỳ thi tuyển sinh vào <strong>Ngành {{ $email->major }}</strong> hệ đại học chính quy Trường Đại học FPT ngày 14/05/2017 với điểm số như sau:</p>
<p>Bài 1: {{ $email->grade1 }}/90 điểm</p>
<p>Bài 2: {{ $email->grade2 }}/15 điểm</p>
<p><strong>Tổng điểm: {{ $email->grade_total }}/105 điểm</strong></p>
<p>Tại Trường Đại học FPT, các sinh viên luôn được khuyến khích rèn luyện ý chí và khát khao mong muốn thành công, xây dựng một sự nghiệp vững chắc trong giai đoạn hội nhập toàn cầu. Chúng tôi rất vui mừng và chào đón bạn sẽ tham gia đội ngũ sinh viên năng động, tích cực của Trường Đại học FPT.</p>
<p>Gửi kèm thư này là Hướng dẫn để bạn hoàn thành thủ tục nhập học trước khi chính thức trở thành Tân sinh viên của Trường Đại học FPT năm 2017.</p>
<p>Một lần nữa, xin chúc mừng và chào đón bạn tại Trường Đại học FPT.</p>

<div style="margin-left: 50%; margin-top: 0.5cm;" class="signature">
HỘI ĐỒNG TUYỂN SINH<br/>
TRƯỜNG ĐẠI HỌC FPT
</div>
</div>
<div class="footer" style="position: absolute; bottom: 0">
<center>
	<img style="max-width: 100%" src="/var/www/html/public/images/pdf_footer.png" />
</center>
</div>

<div class="page-break"></div>
<div class="header">
	<div style="margin-top: 0; margin-bottom: 0.5cm;">
		<center>
			<img src="/var/www/html/public/images/logo.png" />
		</center>
	</div>
</div>
<div class="main">
<h1>HƯỚNG DẪN THỦ TỤC NHẬP HỌC</h1>
<p><strong>I. Thời gian tiến hành làm thủ tục nhập học</strong></p>
<p>Từ 8h00 ngày 15/05/2017 đến trước 17h00 ngày 15/06/2017</p>
<p><strong>II.	Hồ sơ cần có khi làm thủ tục nhập học</strong></p>
<p>1. <em>Phiếu nhập học</em> theo mẫu của Trường Đại học FPT.</p>
<p>2. 01 bản công chứng Học bạ trung học phổ thông.</p>
<p>3. 01 bản gốc Đơn xin chuyển trường và giấy tờ chứng minh đang là sinh viên đại học hệ chính quy của một trường đại học (nếu đang là sinh viên).</p>
<p>4. 02 bản công chứng Chứng minh thư nhân dân.</p>
<p>5. 01 bản photo công chứng Giấy khai sinh.</p>
<p>6. 02 ảnh 3x4 (bỏ vào phong bì nhỏ, ghi rõ họ tên và ngày tháng năm sinh ra bên ngoài phong bì và đằng sau ảnh).</p>
<p>7. Chứng chỉ tiếng Anh TOEFL iBT từ 80 hoặc IELTS từ 6.0 hoặc quy đổi tương đương (nếu có).</p>
<p>8. Lệ phí đăng ký nhập học và học phí theo Quy định tài chính sinh viên Trường Đại học FPT năm 2017.</p>
<p>Đợt đóng học phí đầu tiên khi nhập học Trường tạm thu <strong>14.950.000 VNĐ</strong> = (1) + (2) bao gồm:</p>
<p>(1) Phí đăng ký nhập học: 4.600.000 VNĐ.</p>
<p>(2) Học phí 1 mức học tiếng Anh dự bị: 10.350.000 VNĐ. Nếu thuộc diện được miễn học tiếng Anh, học phí này sẽ chuyển thành học phí cho học kỳ đầu tiên.</p>
<strong>III. Bổ sung hồ sơ nhập học</strong>
<p>Thí sinh bổ sung hồ sơ nhập học từ ngày 07/07/2017 đến hết 17h00 ngày 20/07/2017 gồm:</p>
<ol>
<li>Bản photo công chứng Bằng tốt nghiệp trung học phổ thông (có thể nộp Giấy chứng nhận tốt nghiệp trung học phổ thông tạm thời và bổ sung sau khi có Bằng tốt nghiệp): 02 bản.</li>
<li>Giấy chứng nhận kết quả kỳ thi trung học phổ thông Quốc gia 2017 do Bộ Giáo dục và Đào tạo cấp: 01 bản gốc.</li>
</ol>
</div>
</body>
</html>