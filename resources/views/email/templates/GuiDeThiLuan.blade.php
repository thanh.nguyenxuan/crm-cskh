<html>
<head>
	<style type="text/css">
		table, th, td {
			border: 1px solid #000;
			border-collapse: collapse;
		}
	</style>
</head>
<body>
<p>Bạn <strong>{{ $email->name }}</strong> thân mến,</p>
 
<p>Hội đồng tuyển sinh Trường Đại học FPT chính thức thông báo và chúc mừng bạn đã <strong><em>vượt qua</em></strong> vòng thi viết trong kỳ thi học bổng vào hệ đại học chính quy Trường Đại học FPT ngày 14/05/2017 với điểm số như sau:</p>
<p>Bài 1: {{ $email->grade1 }}/90 điểm</p>
<p>Bài 2: {{ $email->grade2 }}/15 điểm</p>
<p><strong>Tổng điểm: {{ $email->grade_total }}/105 điểm</strong></p>
<p>Mời bạn làm bài thi luận để xác định mức học bổng được nhận của Trường Đại học FPT - hệ đại học chính quy.</p></br />
<strong>
<p>Đề luận</p>
<p>"Bạn hãy viết một bài luận để giúp chúng tôi hiểu về bạn, về những điều mà bạn đã trải nghiệm, về các giá trị của cuộc sống mà bạn trân trọng hay về những điều mà bạn quan tâm. Bạn có thể kể về một con người hoặc một sự kiện có ảnh hưởng đặc biệt đến cuộc đời của bạn hoặc bạn có thể viết về bất kỳ điều gì mà bạn cho rằng sẽ giúp chúng tôi hiểu bạn rõ hơn.</p>
<p>Và hãy cho chúng tôi biết lý do vì sao bạn lại chọn Đại học FPT.</p>
<p>Cảm ơn bạn."</p>
</strong>
<br />
<p><strong><em>Thời hạn nộp bài: 17h00 ngày 02/06/2017</em></strong></p>
<p>Cách thức nộp bài:</p>
<p>Thí sinh điền thông tin và nộp bài luận trên website của trường tại: <a href="http://daihoc.fpt.edu.vn/bai-luan-hoc-bong/">http://daihoc.fpt.edu.vn/bai-luan-hoc-bong/</a></p>
<p>Lưu ý:</p>
<p>Bài làm định dạng file Microsoft Word.</p>
<p>Đặt tên file &lt;Số CMND&gt; &lt;Họ và tên (không có dấu)&gt; &lt;SBD&gt;</p>
<p>Ví dụ: 11984394_NguyenVanA_FU000012</p><br />
<p>Trân trọng!</p>
<p>HỘI ĐỒNG TUYỂN SINH TRƯỜNG ĐẠI HỌC FPT</p>

<p>*********************************************************************</p>
<p>Phòng tuyển sinh - Trường Đại Học FPT</p>
<p>Website: <a href="http://daihoc.fpt.edu.vn">http://daihoc.fpt.edu.vn</a></p>
<p>Hotline: 04(08) 73001866/(0236) 7301866</p>
</body>
</html>