@include("includes.header")
<div class="container">
    <h3>Danh sách email</h3>
    <table class="table table-hovered">
        <thead>
        <tr>
            <th>STT</th>
            <th>Tên template</th>
            <th>Loại template</th>
            <th>Ngày tạo</th>
        </tr>
        </thead>
        <tbody>
        @php $stt = 1 @endphp
        @foreach($template_list as $item)
            <tr>
                <td>{{$stt}}</td>
                <td><a href="{{ route('email-template.edit', $item->id) }}">{{$item->name}}</a></td>
                <td>{{$item->type}}</td>
                <td>{{ \Carbon\Carbon::parse($item->created_at)->format('d/m/Y H:i:s')}}</td>
            <tr>
            @php $stt++ @endphp
        @endforeach
        </tbody>
    </table>
</div>