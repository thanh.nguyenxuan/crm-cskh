<html>
<head>
	<style type="text/css">
		body {
			font-family: Times New Roman, serif;
			font-size: 12pt;
			line-height: 1.5em;
		}
		
		div.main {
			margin: 0.5cm 2.5cm 0.7cm 2.5cm;
		}
		
		div.footer {
			margin: 0 1cm 1cm 0.2cm;
		}
		h1 {
			font-size: 18pt;
			text-transform: uppercase;
			text-align: center;
		}
		h1.subtitle {
			font-size: 14pt;
			text-transform: uppercase;
			text-align: center;
		}
		.image {
			display: inline-block;
			width: 33%;
		}
		.image img {
			width: 3cm;
		}
		.personal-info {
			display: inline-block;
			width: 66%;
			font-weight: bold;
			font-style: italic;
		}
		hr {
			border: 0;
			height: 1pt;
			background: #000;
			margin: 0.5cm 1.5cm 0.5cm 2cm;
		}
		p {
			line-height: 1em;
		}
		p.special {
			text-indent: 1cm;
			line-height: 20pt;
			margin-top: 0;
		}
		ol {
			font-weight: bold;
			margin-top: 0;
			margin-bottom: 0;
		}
		ol li span {
			font-weight: normal;
		}
		
		div.fillable .label {
			display: inline-block;
			overflow: hidden;
		}
		div.fillable {
			display: inline-block;
			overflow: hidden;
			padding: 0.3cm;
			width: 45%;
			border: 1px solid #000; 
			display: inline-block;
			white-space: nowrap;
		}
		
		div.fillable .row {
			overflow: hidden;
			white-space: nowrap;
		}
		div.dotted {
			display: inline-block;
			width: 500px;
			overflow: hidden;
			white-space: nowrap;
		}

		@media print {
			.noprint {
				display:none;
			}
		}

		
	</style>
</head>
<body>
<div class="noprint" style="width:100%; height: 100%; position: absolute;" onclick="window.close()">&nbsp;
</div>
	<div class="header">
		<div style="margin-top: 1cm; margin-bottom: 0.5cm;">
			<center>
				<img src="/images/logo.png" />
			</center>
		</div>
		<div style="display: inline-block; position: absolute; top:1cm; right: 1cm;">
			<img style="width: 4.6cm;" src="/images/paid.png" />
		</div>
	</div>
	<div class="main">
		<h1>Giấy báo dự thi</h1>
		<h1 class="subtitle">(Hệ đại học chính quy)</h1>
		<div class="image" style="margin-top: -1.5cm;">
			@if(!empty($email->image))
				<img src="/uploads/students_2/{{ $email->image }}" />
				<img style="margin-top: -1.5cm; margin-left: 1.5cm; width: 4cm; display: block;" src="/images/stamp.png" />
			@endif
		</div>
		<div class="personal-info">
			<p>Thí sinh: {{ $email->name }}</p>
			<p>Địa chỉ: {{ $email->address }}</p>
			<p>Điện thoại: {{ $email->phone1 }}{{ !empty($email->phone1) && !empty($email->phone2)?' - ':''}} {{$email->phone2}}</p>
			<p>Ngày sinh:	{{ !empty($email->birthdate)?$email->birthdate->format('d/m/Y'):'' }}</p>
			<p>CMND: {{ $email->id_passport }}</p>
		</div>
		<div>
			<p style="margin: 0; line-height: 20pt;">Trường Đại học FPT xin được thông báo đến Thí sinh về thời gian và địa điểm thi tuyển sinh kỳ thi ngày 14 tháng 5 năm 2017 của Trường như sau:</p>
			<ol>
			<li><span><strong>Các thí sinh đã hoàn thành thủ tục dự thi xem Số báo danh, Phòng thi , Thời gian thi và Sơ đồ địa điểm thi trên website của Trường theo địa chỉ: <a href="http://daihoc.fpt.edu.vn">http://daihoc.fpt.edu.vn</a> từ 8h30 ngày 13/5/2017.</strong></span></li>
			<li><span><strong>Các thí sinh chưa hoàn thành thủ tục dự thi có thể đến trực tiếp các Văn phòng Tư vấn tuyển sinh của Trường từ 8h30 đến 17h00 ngày 13/5/2017.</strong></span></li>
			<li><span><strong>Các giấy tờ cần mang theo</strong>: Giấy báo dự thi của ĐH FPT, CMND bản chính.</span></li>
			<li><span><strong>Các vật dụng được phép mang vào phòng thi</strong>: 02 chiếc bút chì loại 2B hoặc HB, 01 bút bi, 01 cục tẩy, Giấy báo dự thi và Chứng minh thư nhân dân.</span></li>
			</ol>
			<p class="special"><em>Hội đồng tuyển sinh đề nghị các thí sinh nghiêm túc thực hiện đúng các yêu cầu và quy định của kỳ thi. Chúc các thí sinh làm bài tốt và thể hiện được hết các năng lực của mình.</em></p>
		</div>
		<div>
			<div class="fillable">
			<div class="row">
				<div class="label"><strong>SBD: </strong>...................................................................................................</div>
			</div>
				<div class="row"><div class="label"><strong>Phòng: </strong>...................................................................................................</div></div>
				<div class="row"><div class="label"><strong>Thời gian thi: </strong>...................................................................................................</div></div>
				<div class="row"><div class="label"><strong>Địa điểm thi: </strong>...................................................................................................</div></div>
				<div class="row"><div class="label">...............................................................................................................</div></div>
				<div class="row"><div class="label">................................................................................................................</div></div>
			</div>
			<div style="width: 49%; display: inline-block; text-align: center; vertical-align: top;">
				<p style="margin-top: 0"><em>Ngày {{ date('d') }} tháng {{ date('m') }} năm {{ date('Y') }}</em>
				<p><strong>TL. Chủ tịch Hội đồng tuyển sinh</strong></p>
				<p style="margin-bottom: 0"><strong>Cán bộ tuyển sinh</strong></p>
				<img style="width: 4.5cm" src="/images/signature.png" />
				<p style="margin-top: 0"><strong>Lê Thanh Tú</strong></p>
			</div>
		</div>
		</div>
		<div class="footer" style="position: absolute; bottom: 0">
		<center>
			<hr />
			<img style="max-width: 100%" src="/images/pdf_footer.png" />
		</center>
		</div>
</body>
<script type="text/javascript">
	document.title="{{ $email->excel_order }}";
	window.print();
</script>
</html>