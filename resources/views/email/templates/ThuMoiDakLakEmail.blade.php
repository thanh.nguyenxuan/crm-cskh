<html>
<head>
<style type="text/css">
html {
	font-family: Arial, Helvetica, sans-serif;
}
body {
	margin: 0 40px;
	font-size: 14px;
}
p {
	margin-right: 50px;
}
h1 {
	text-transform: uppercase;
	color: #f47224;
	text-align: center;
	font-size: 20px;
}

a {
	text-decoration: none;
	color: #f47224;
}

a:visited {
	color: #f47224;
}
</style>
</head>
<body>
<a href="http://daihoc.fpt.edu.vn/thu-thach-tu-duy-logic-iq-tai-dak-lak/?utm_source=EmailMKT&utm_medium=EmailMKTloclan1&utm_campaign=thi9.7daklak"><img alt="Thử thách tư duy logic, IQ cùng Đại học FPT" src="http://crm.daihocfpt.edu.vn/images/daklak/banner_daklak.jpg" /></a>
<br />
<h1>Làm gì sau kỳ thi THPT Quốc Gia 2017?</h1>
<p><strong>{{ $email->name }}</strong> thân mến,</p>
<p>Chúc mừng em đã hoàn thành xong kỳ thi THPT Quốc Gia 2017 - dấu mốc quan trọng sau 12 năm nỗ lực đèn sách. Chỉ còn vài ngày nữa, kết quả của kỳ thi sẽ được công bố chính thức, em:</p>
<p>Đang hài lòng sau kỳ thi, chắc chắn với tấm vé vào đại học và sẵn sàng những kế hoạch xả hơi, nghỉ ngơi, khám phá?</p>
<p>Hay, đang trông ngóng kết quả từng ngày và cố gắng tìm kiếm thêm những cánh cổng khác vào đại học?</p>
<p>Vậy, tại sao không tham gia:</p>
<h1><a href="http://daihoc.fpt.edu.vn/thu-thach-tu-duy-logic-iq-tai-dak-lak/?utm_source=EmailMKT&utm_medium=EmailMKTloclan1&utm_campaign=thi9.7daklak">Thử thách IQ cùng Đại học FPT</a></h1>
<br />
<p>Ngày 9.7.2017 tới đây, Đại học FPT dành riêng cho các bạn học sinh tại Đắk Lắk cơ hội trải nghiệm thú vị sau kỳ thi THPT Quốc Gia căng thẳng với chương trình "Thử thách IQ cùng Đại học FPT". Đây là dịp để em:</p>
<p>Khám phá đề thi đầu vào theo hình thức trắc nghiệm và tư duy logic độc, lạ, thú vị - KHÔNG áp lực. Đây cũng là cơ hội để em tìm hiểu về đề tuyển dụng mà các doanh nghiệp, tập đoàn lớn hiện nay như: FPT, Samsung,… sử dụng để đánh giá ứng viên.</p>
<p>Chọn lựa và nhận ngay quà tặng yêu thích cho năm tới tại đại học: balo, áo phông, sạc dự phòng, gậy tự sướng, chuột máy tính…</p>
<p>Nhân đôi cơ hội vào đại học với kết quả thử thách IQ dùng để sơ tuyển vào Đại học FPT năm 2017.</p>
<p>Tìm kiếm những những suất học bổng giá trị vào ĐH FPT, có gần 100 suất học bổng các mức 50%, 70%, 100% và 100%+ (bao gồm cả học phí và sinh hoạt phí trong 4 năm học) với tổng giá trị gần 20 tỷ đồng.</p>
<p>Để biết thêm thông tin về chương trình, em hãy liên hệ tổng đài 04 (08) 73001866 hoặc để lại thông tin, cán bộ nhà trường sẽ gọi lại tư vấn.</p>

<div style="text-align: center"><a href="http://daihoc.fpt.edu.vn/thu-thach-tu-duy-logic-iq-tai-dak-lak/?utm_source=EmailMKT&utm_medium=EmailMKTloclan1&utm_campaign=thi9.7daklak"><img alt="Đăng ký ngay" src="http://crm.daihocfpt.edu.vn/images/daklak/dangkyngay_daklak.jpg" /></a></div>

<div style="float: right; font-weight: bold;">
<p>Trân trọng,</p>
<p><em>Trường Đại học FPT</em></p>
</div>

<img src="https://www.google-analytics.com/collect?v=1&tid=UA-89648819-1&t=event&ec=email&ea=open&el=DL200&cid=fe2017" />
</body>
</html>