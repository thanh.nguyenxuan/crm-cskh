<html>
<head>
	<style type="text/css">
		body {

			font-size: 12pt;
			line-height: 15pt;
			font-family: Times, serif;
		}
		
		h1 {
			font-size: 14pt;
			text-transform: uppercase;
			text-align: center;
		}
		div.main {
			margin: 0.5cm 0.5cm 0.5cm 0.5cm;
		}
		
		.first-page {
			font-size: 14pt;
		}
		
		div.footer {
			margin: 0 0.5cm 0.5cm 0.2cm;
		}
		.signature {
			font-weight: bold;
			margin-top: 3em;
			text-align: center;
		}
		p, strong, em {
			line-height: 15pt;
			margin: 0;
		}
		.page-break {page-break-after: always;}
		hr {
			border: 0;
			height: 1pt;
			background: #000;
			margin-bottom: 10pt;
		}
		table {
			border-collapse: collapse;
		}
		table, td, th {
			border: 1px solid #000;
		}
		th {
			text-align: center;
		}
		td {
			padding-left: 5pt;
			padding-right: 5pt;
		}
		ul {
			margin-top: 0;
			margin-bottom: 0;
		}
	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<div class="header">
	<div style="margin-top: -2cm;">
		<center>
			<img src="/var/www/html/public/images/logo.png" />
		</center>
	</div>
</div>
<div class="main first-page">
<h1 style="margin-top: 0.5cm">GIẤY BÁO TRÚNG TUYỂN ĐẠI HỌC CHÍNH QUY</h1>
<p>Bạn <strong>{{ $email->name }}</strong> thân mến,</p>
<p>Hội đồng tuyển sinh Trường Đại học FPT chúc mừng bạn đã trúng tuyển vào Ngành <strong>{{ $email->major }}</strong> hệ đại học chính quy Trường Đại học FPT theo diện <strong>lọc tuyển của Bộ GD & ĐT</strong> trong đợt xét tuyển lần 1 năm 2017.</p>
<p>Đề nghị bạn nhanh chóng hoàn thiện thủ tục nhập học trước 17h00 ngày 07/08/2017 để chính thức trở thành sinh viên của Đại học FPT.</p>
<p>Gửi kèm Giấy báo trúng tuyển này là hướng dẫn nhập học của Đại học FPT. Bạn vui lòng đọc kỹ hướng dẫn và quy định tài chính của Trường trước khi nhập học. Nếu có thắc mắc vui lòng liên hệ với Cán bộ tư vấn của nhà Trường qua hotline: <strong>0984471866/024 73001866/028 73001866</strong> để được giải đáp!</p>
<p>Hẹn gặp lại bạn tại Trường Đại học FPT!</p>
<div style="margin-left: 50%" class="signature">
TM. HỘI ĐỒNG TUYỂN SINH<br />
CHỦ TỊCH<br /><br />
<img src="/var/www/html/public/images/tuantn_signature.png" style="width: 2.3cm" /><br /><br />
PHÓ HIỆU TRƯỞNG<br />
Trần Ngọc Tuấn
</div>
</div>
<div class="footer" style="position: absolute; bottom: -2cm">
<center>
	<img style="max-width: 100%" src="/var/www/html/public/images/pdf_footer.png" />
</center>
</div>
<div class="page-break"></div>
<div class="header">
	<div style="margin-top: 0; text-align: center;">
			<img src="/var/www/html/public/images/logo.png" />
	</div>
</div>
<div class="main">
<h1>HƯỚNG DẪN THỦ TỤC NHẬP HỌC</h1>
<p>Thí sinh trúng tuyển vào Đại học FPT muốn nhập học cần thực hiện 3 bước sau để hoàn thành thủ tục nhập học trước 17h ngày 7/8/2017.</p>
<p>- <strong>Bước 1</strong>: Nộp hồ sơ về Trường ĐH FPT theo một trong ba hình thức: trực tiếp/qua bưu điện/email (qua email gửi bản chụp hoặc scan và bổ sung bản gốc/bản sao công chứng trong ngày đầu tiên nhập học).</p>
<p>Hồ sơ bao gồm:</p>
<ul>
<li>Phiếu đăng ký nhập học (đính kèm);</li>
<li>Bằng tốt nghiệp THPT (có thể nộp Giấy chứng nhận tốt nghiệp trung học phổ thông tạm thời và bổ sung khi có Bằng tốt nghiệp) (02 bản sao công chứng);</li>
<li>Học bạ THPT (01 bản sao công chứng);</li>
<li>CMND hoặc Căn cước công dân (02 bản sao công chứng)</li>
<li>Giấy khai sinh (01 bản sao công chứng)</li>    
<li>Ảnh 3×4 (02 ảnh);</li> 
<li>Chứng chỉ tiếng Anh TOEFL iBT từ 80 hoặc IELTS (Học thuật) từ 6.0 hoặc quy đổi tương đương (nếu có – 01 bản sao công chứng);</li> 
<li>Bảng điểm thi THPT (bản chính - dành cho đối tượng trúng tuyển theo phương thức xét lọc của Bộ GD-ĐT)</li> 
</ul>
<p>- <strong>Bước 2</strong>: Nộp phí đăng ký nhập học và học phí 1 mức Tiếng Anh dự bị theo quy định tài chính sinh viên Trường Đại học FPT hiện hành (trực tiếp hoặc chuyển khoản).</p>
<table>
	<thead>
		<tr>
			<th>Hà Nội và Hồ Chí Minh</th>
			<th>Đà Nẵng</th>
			<th>Cần Thơ</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
				<p>Thí sinh nộp 14.950.000 VNĐ (tương ứng với mục (1) + (2) bên dưới) vào tài khoản ngân hàng của Trường, bao gồm:</p>
<p>(1) Phí đăng ký nhập học: 4.600.000 VNĐ;</p>
<p>(2) Học phí 1 mức học tiếng Anh dự bị: 10,350,000 VNĐ (Nếu thuộc diện được miễn học tiếng Anh, học phí này sẽ chuyển thành học phí cho học kỳ đầu tiên)</p>
			</td>
			<td>
			<p>Thí sinh nộp 11.850.000 VNĐ (tương ứng với mục (1) + (2) bên dưới) vào tài khoản ngân hàng của Trường, bao gồm:</p>
<p>(1) Phí đăng ký nhập học: 4.600.000 VNĐ;</p>
<p>(2) Học phí 1 mức học tiếng Anh dự bị: 7.250.000 VNĐ (Nếu thuộc diện được miễn học tiếng Anh, học phí này sẽ chuyển thành học phí cho học kỳ đầu tiên).</p>
			</td>
			<td>
			<p>Thí sinh nộp 8.250.000 VNĐ (tương ứng với mục (1) + (2) bên dưới) vào tài khoản ngân hàng của Trường, bao gồm:</p>
<p>(1) Phí đăng ký nhập học: 2.500.000 VNĐ;</p>
<p>(2) Học phí 1 mức học tiếng Anh dự bị: 5.750.000 VNĐ (Nếu thuộc diện được miễn học tiếng Anh, học phí này sẽ chuyển thành học phí cho học kỳ đầu tiên).</p>
			</td>
		</tr>
	</tbody>
</table>
<div class="page-break"></div>
<div class="header">
	<div style="margin-top: 0;">
		<center>
			<img src="/var/www/html/public/images/logo.png" />
		</center>
	</div>
</div>
<p>Thông tin tài khoản Trường Đại học FPT:</p>
<table>
	<thead>
		<tr>
			<th>Hà Nội</th>
			<th>Hồ Chí Minh</th>
			<th>Đà Nẵng</th>
			<th>Cần Thơ</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
			<p>Số TK: 00006969009</p>
<p>Tên TK: Trường Đại học FPT</p>
<p>Địa chỉ: Ngân hàng Thương Mại cổ phần Tiên Phong – Chi nhánh Hoàn Kiếm</p>
<p>Nội dung nộp tiền: &lt;TSTT - Họ và tên thí sinh – Số CMND của thí sinh – Lệ phí NH 2017&gt;</p>
			</td>
			<td>
			<p>Số TK: 6150201010751</p>
<p>Tên TK: Viện Đào Tạo Quốc Tế FPT TP.HCM</p>
<p>Địa chỉ: Ngân hàng Nông nghiệp và Phát triển Nông thôn VN - CN Xuyên Á – TP.HCM</p>
<p>Nội dung nộp tiền: &lt;TSTT - Họ và tên thí sinh – Số CMND của thí sinh – Lệ phí NH 2017&gt;</p>
			</td>
			<td>
			<p>Số TK: 2002201204827</p>
<p>Tên TK: Trung tâm đào tạo quốc tế FPT Đà Nẵng</p>
<p>Tại Ngân hàng: Nông nghiệp và phát triển nông thôn – Chi nhánh Liên Chiểu – Đà Nẵng</p>
<p>Nội dung nộp tiền: &lt;TSTT - Họ và tên thí sinh – Số CMND của thí sinh – Lệ phí NH 2017&gt;</p>
			</td>
			<td>
			<p>Số TK: 00006969007</p>
<p>Tên TK: Trường Đại học FPT</p>
<p>Tại Ngân hàng: Ngân hàng Thương mại Cổ phần Tiên Phong – Chi nhánh Hoàn Kiếm</p>
<p>Nội dung nộp tiền: &lt;TSTT - Họ và tên thí sinh – Số CMND của thí sinh – Lệ phí NH 2017&gt;</p>
			</td>
		</tr>
	</tbody>
</table>
<p>- <strong>Bước 3</strong>: Nếu chuyển tiền qua ngân hàng, chụp ảnh giấy nộp tiền vào tài khoản ngân hàng của trường và gửi vào hòm thư daihocfpt@fpt.edu.vn.</p>
<p>Địa chỉ các văn phòng Trư-ờng đại học FPT:</p>
<table>
	<thead>
		<tr>
			<th>Hà Nội</th>
			<th>Hồ Chí Minh</th>
			<th>Đà Nẵng</th>
			<th>Cần Thơ</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>
			<p>Khu Giáo dục và Đào tạo - Khu Công nghệ cao Hòa Lạc - Km29 Đại lộ Thăng Long, Thạch Thất, TP. Hà Nội</p>
<p>Điện thoại: (024) 7300 1866</p>
<p>Email: daihocfpt@fpt.edu.vn</p>
			</td>
			<td>
			<p>Lầu 2, toà nhà Innovation, lô 24 - CVPM Quang Trung - P. Tân Chánh Hiệp, Quận 12, TP. HCM</p>
<p>Điện thoại: (028) 7300 1866</p>
<p>Email: daihocfpt@fpt.edu.vn</p>
			</td>
			<td>
			<p>137 Nguyễn Thị Thập, Quận Liên Chiểu, TP. Đà Nẵng.</p>
<p>Điện thoại: (0236) 730 1866</p>
<p>Email: daihocfpt@fpt.edu.vn</p>
			</td>
			<td>
			<p>160 đường 30/4, Q. Ninh Kiều, TP. Cần Thơ</p>
<p>Điện thoại: 0984471866</p>
<p>Email: daihocfpt@fpt.edu.vn</p>
			</td>
		</tr>
	</tbody>
</table>
<p><strong>Lưu ý</strong>: với thí sinh đã nộp một số hồ sơ hoặc một phần tài chính thì chỉ cần nộp các hồ sơ/tài chính còn lại.</p>
</body>
</html>