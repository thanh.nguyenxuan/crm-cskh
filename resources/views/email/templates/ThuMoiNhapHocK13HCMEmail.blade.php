<html>
<head>
<style type="text/css">
	td {
		padding: 5px 35px;
	}
</style>
</head>
<body>
<p><strong>TRƯỜNG ĐẠI HỌC FPT</strong> trân trọng thông báo và chúc mừng:</p>
<table border="0">
	<tr>
		<td>Thí sinh: <strong>{{ $email->name }}</strong></td>
		<td>Giới tính: {{ $email->gender }}</td>
	</tr>
	<tr>
		<td>Năm sinh: {{ $email->birthyear }}</td>
		<td>CMND: {{ $email->id_passport }}</td>
	</tr>
</table>
<p><strong>Đã chính thức trở thành TÂN SINH VIÊN trường ĐẠI HỌC FPT – hệ đào tạo ĐẠI HỌC CHÍNH QUY</strong></p>
<table border="0">
	<tr>
		<td>Ngành học: <strong>{{ $email->major }}</strong></td>
		<td>Niên khóa: 2017 - 2021</td>
	</tr>
	<tr>
		<td>Khóa học: 13</td>
		<td>Địa điểm học: TP. Hồ Chí Minh</td>
	</tr>
</table>
<p>Là trường Đại học được thành lập bởi tập đoàn FPT, trường Đại học FPT thấu hiểu những điều mà nhà tuyển dụng tìm kiếm ở nguồn nhân lực chất lượng cao trong bối cảnh hội nhập toàn cầu. 11 năm đào tạo - 11 năm cháy bỏng khát vọng đổi thay nền giáo dục Việt Nam. Đến nay, Đại học FPT tự hào là môi trường đào tạo uy tín, chắp cánh cho hàng ngàn sinh viên Việt Nam vươn ra thế giới -  Sẵn sàng cho sự nghiệp toàn cầu.</p>
<p>Nhà trường trân trọng thông báo đến Quý phụ huynh, Tân sinh viên lịch tập trung và thời gian diễn ra các hoạt động đầu năm. Sinh viên sắp xếp nhập trường đúng thời gian và tham dự các hoạt động đầy đủ.</p>
<p>Chào đón bạn tại Đại học FPT.</p>

<p>Thân chào Bạn !</p>
<p>*********************************************************************</p>
<p>Phòng tuyển sinh - Trường Đại Học FPT</p>
<p>Website: <a href="http://daihoc.fpt.edu.vn">http://daihoc.fpt.edu.vn</a></p>
<p>Hotline: (028) 73001866</p>
</body>
</html>