<html>
<head>
	<style type="text/css">
		table, th, td {
			border: 1px solid #000;
			border-collapse: collapse;
		}
	</style>
</head>
<body>
<p>Bạn <strong>{{ $email->name }}</strong> thân mến,</p>
 
<p>Hội đồng tuyển sinh Trường Đại học FPT nhiệt liệt chúc mừng gia đình và thí sinh <strong>{{ $email->name }}</strong> đã vượt qua kỳ thi tuyển vào hệ đại học chính quy Trường ĐH FPT ngày 14/05/2017. Đây là thành công quan trọng, ghi nhận những cố gắng và nỗ lực bước đầu của em sau 12 năm học tập phổ thông.</p>
<p>Trường ĐH FPT trân trọng kính mời gia đình và thí sinh <strong>{{ $email->name }}</strong> tới tham gia <strong>NGÀY HỘI TỰU TRƯỜNG</strong> vào ngày 11/06/2017 tại Khu Công nghệ cao Hòa Lạc, Thạch Thất, Hà Nội.</p>
<p>Chương trình được tổ chức dành riêng cho những thí sinh xuất sắc vượt qua kỳ thi tuyển và kỳ thi học bổng vào Đại học FPT tháng 5 vừa qua và các thí sinh đủ điều kiện xét tuyển vào Đại học FPT nhằm hướng dẫn thí sinh và gia đình các thông tin cần thiết để chuẩn bị cho quá trình học tập tại Đại học FPT:</p>
<ul>
<li>Hướng dẫn hoàn thiện thủ tục nhập học;</li>
<li>Đăng ký áo đồng phục, võ phục, bộ tài liệu hướng dẫn tân sinh viên;</li>
<li>Đăng ký phòng ở tại Ký túc xá;</li>
<li>Đăng ký tham gia các Câu lạc bộ sinh viên;</li>
<li>Kiểm tra tiếng Anh đầu vào;</li>
<li>Giải đáp các thắc mắc về cách thức học tập, sinh hoạt và quản lý sinh viên.</li>
</ul>
<p>Mọi thông tin chi tiết xin vui lòng liên hệ: <strong><em>0473001866 hoặc 0984471866</em></strong> để đăng ký tham dự hội thảo.</p>
<p>Trường Đại học FPT bố trí xe đưa đón phụ huynh và học sinh lên khu CNC Hòa Lạc, Thạch Thất với thời gian & địa điểm như sau:</p>
<p><strong>Thời gian: 	7h00 Chủ Nhật, ngày 11 tháng 6 năm 2017</strong></p>
<p><strong>Địa điểm: Tòa nhà Detech, số 8 Tôn Thất Thuyết, Mỹ Đình, Nam Từ Liêm, Hà Nội</strong> (đối diện bến xe Mỹ Đình)</p>
<p><strong>Lịch trình đón tiếp phụ huynh</strong></p>
<ul>
<li><strong>7h00 – 7h15</strong>: Tập trung tại Số 8 Tôn Thất Thuyết để xe đón lên KCN Hoà Lạc, Thạch Thất.</li>
<li><strong>7h40 – 8h00</strong>: Tới trường ĐH FPT tại Hòa Lạc.</li>
<li><strong>8h00 – 9h30</strong>: Phụ huynh và học sinh làm thủ tục nhập học, tham quan trường và tham gia "Ngày hội các Câu lạc bộ".</li>
<li><strong>9h30 – 11h30</strong>: Phụ huynh dự hội thảo, trao đổi trực tiếp với đại diện ĐH FPT. Học sinh tập trung làm bài kiểm tra tiếng Anh đầu vào, đăng ký và nộp tiền Ký túc xá.</li>
<li><strong>11h30-12h30</strong>: Dùng tiệc ngọt và nhận các ấn phẩm của ĐH FPT.</li>
<li><strong>12h30 – 13h00</strong>: Tập trung lên xe về số 8 Tôn Thất Thuyết, kết thúc chương trình.</li>
</ul>
<p>Trân trọng kính mời các vị phụ huynh tới tham dự!</p>

<p>*********************************************************************</p>
<p>Phòng tuyển sinh - Trường Đại Học FPT</p>
<p>Website: <a href="http://daihoc.fpt.edu.vn">http://daihoc.fpt.edu.vn</a></p>
<p>Hotline: 04(08) 73001866/(0236) 7301866</p>
</body>
</html>