<html>
<head>
	<style type="text/css">
		body {

			font-size: 10pt;
			line-height: 12pt;
			font-family: DejaVu Serif, Helvetica, serif;
		}
		
		h1 {
			font-size: 18pt;
			text-transform: uppercase;
			text-align: center;
		}
		div.main {
			margin: 0.5cm 0.5cm 0.5cm 0.5cm;
		}
		div.footer {
			margin: 0 0.5cm 0.5cm 0.2cm;
		}
		.signature {
			font-weight: bold;
			margin-top: 3em;
			text-align: center;
		}
		p, strong, em {
			line-height: 1.5em;
			margin: 0;
		}
		.page-break {page-break-after: always;}
		hr {
			border: 0;
			height: 1pt;
			background: #000;
			margin-bottom: 10pt;
		}
	</style>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<div class="header">
	<div style="margin-top: -2cm; margin-bottom: 0.5cm;">
		<center>
			<img src="/var/www/html/public/images/logo.png" />
		</center>
	</div>
</div>
<div class="main">
<h1 style="margin-top: 1cm">THÔNG BÁO</h1>
<p><em>Về việc nộp bản chính Giấy chứng nhận kết quả thi THPT quốc gia 2017</em></p><br />
<p>Bạn <strong>{{ $email->name }}</strong> thân mến,</p>
<p>Hội đồng tuyển sinh Trường Đại học FPT chúc mừng bạn đã trúng tuyển vào Ngành <strong>{{ $email->major }}</strong> hệ đại học chính quy Trường Đại học FPT theo diện <strong>xét tuyển học bạ THPT</strong>.</p>
<p>Theo yêu cầu của Bộ Giáo dục & Đào tạo về việc các trường có Đề án tuyển sinh riêng phải công bố danh sách thí sinh trúng tuyển đã xác nhận nguyện vọng nhập học trên Cổng thông tin của Bộ, đề nghị bạn nộp bản gốc <strong><em>Giấy chứng nhận kết quả thi THPT Quốc gia</em></strong> về trường trước 17h ngày 22/07/2017 bằng một trong hai cách sau:</p>
<ul>
	<li>Nộp trực tiếp tại các phòng tuyển sinh của trường.</li>
	<li>Nộp qua đường bưu điện về các Phòng tuyển sinh của trường.</li>
</ul>
<p><strong><em>Lưu ý:</em></strong></p>
<p>
<ul>
	<li>Cách thức chuyển phát nhanh hoặc chuyển phát ưu tiên nếu nộp qua đường bưu điện. Ngoài phong bì ghi thêm thông tin "Nộp Giấy chứng nhận kết quả thi THPT". Thời gian gửi: trước 17h ngày 21/7/2017 (tính theo dấu bưu điện).</li>
	<li>Để tránh bị thất lạc cũng như được chuyển đến không đúng thời gian quy định, nhà trường khuyến khích thí sinh nộp bản chính Giấy chứng nhận kết quả thi THPT Quốc gia theo hình thức trực tiếp tại trường.</li>
</ul>
<div style="margin-left: 50%" class="signature">
TM. HỘI ĐỒNG TUYỂN SINHc
CHỦ TỊCH<br />
PHÓ HIỆU TRƯỞNG<br />
Trần Ngọc Tuấn
</div>
</div>
<div class="footer" style="position: absolute; bottom: -2cm">
<center>
	<img style="max-width: 100%" src="/var/www/html/public/images/pdf_footer.png" />
</center>
</div>
</body>
</html>