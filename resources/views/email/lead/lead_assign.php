<?php
/**
 * @var $title string
 * @var $name string
 * @var $phone string
 * @var $created_at string
 * @var $source string
 * @var $url string
 */
?>
<h3><?php echo $title?></h3>
<p>Dear TVTS,</p>
<p>Lead "<?php echo $name?>" mới được phân công cho bạn bởi <?php echo $source?>. Chi tiết: <?php echo $url?>. Thời gian tạo: <?php echo $created_at?>. Số điện thoại: <?php echo $phone?>.</p>
<p>Trân trọng.</p>

