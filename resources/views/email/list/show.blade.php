@include('includes.header')
<div class="container">
	<h3>Danh sách: {{ $detail->name }}</h3>
	<div class="row">
		<div class="col-md-12">
			<a class="btn btn-default" href="{{ route('email-list.import', $detail->id) }}">Import</a>
			<a class="btn btn-default" href="{{ route('email-list.export', $detail->id) }}">Export</a>
		</div>
	</div><br />
	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<tr>
					<td>Tên danh sách:</td>
					<td>{{ $detail->name }}</td>
				</tr>
				<tr>
					<td>Số lượng email:</td>
					<td>{{ $detail->count }}</td>
				</tr>
			</table>
		</div>
	</div>
	<h4>Danh sách email</h4>
	<table class="table table-hover table-bordered">
		<thead>
			<tr>
				<th>STT</th>
				<th>Họ và tên</th>
				<th>Điểm</th>
				<th>CMND</th>
				<th>SĐT 1</th>
				<th>SĐT 2</th>
				<th>Địa chỉ</th>
				<th>Email</th>
			</tr>
		</thead>
		<tbody>
			@php $stt = 1 @endphp
			@foreach($detail->email_list as $contact)
			<tr>
				<td>{{ $stt }}</td>
				<td>{{ $contact->name }}</td>
				<td>{{ $contact->grade }}</td>
				<td>{{ $contact->id_passport }}</td>
				<td>{{ $contact->phone1 }}</td>
				<td>{{ $contact->phone2 }}</td>
				<td>{{ $contact->address }}</td>
				<td>{{ $contact->email }}</td>
			</tr>
			@php $stt++ @endphp
			@endforeach
		</tbody>
	</table>
</div>