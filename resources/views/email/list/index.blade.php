@include("includes.header")
<div class="container">
<h3>Danh sách email</h3>
<table class="table table-hovered">
<thead>
	<tr>
		<th>STT</th>
		<th>Tên danh sách</th>
		<th>Số lượng email</th>
		<th>Ngày tạo</th>
	</tr>
</thead>
<tbody>
@php $stt = 1 @endphp
	@foreach($list as $item)
		<tr>
			<td>{{$stt}}</td>
			<td><a href="{{ route('email-list.show', $item->id) }}">{{$item->name}}</a></td>
			<td>{{$item->number}}</td>
			<td>{{$item->created_at}}</td>
		<tr>
		@php $stt++ @endphp
	@endforeach
</tbody>
</table>
</div>