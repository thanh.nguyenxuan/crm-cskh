@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-edufit-default">
                <div class="panel-heading">Thêm người dùng</div>
                <div class="panel-body">
				@if(!empty($message))
					<div class="alert alert-danger" role="alert">{!! $message !!}</div>
				@endif
				@if(!empty($success))
					<div class="alert alert-success" role="alert">Tạo tài khoản người dùng thành công!</div>
				@endif
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('user.store') }}">
                        {{ csrf_field() }}

						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label required">Tên đăng nhập</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label required">Họ và tên</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label required">Email</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label required">Mật khẩu</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label required">Xác nhận mật khẩu</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="department" class="col-md-4 control-label required">Cơ sở</label>

                            <div class="col-md-6">
                                {{ Form::select('department', \App\Department::listData(), old('department'), ['id' => 'department', 'placeholder' => 'Chọn cơ sở', 'required' => 'required', 'class' => 'form-control']) }}
                            </div>
                        </div>

						<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="extension" class="col-md-4 control-label">Số máy lẻ</label>

                            <div class="col-md-6">
                                <input id="extension" type="text" class="form-control" name="extension" value="{{ old('extension') }}">

                                @if ($errors->has('extension'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('extension') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
						<div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Active</label>

                            <div class="col-md-6">
							{{ Form::checkbox('active', 1, true)}}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div style="padding-left: 50px">
                                    <label for="departments" class="control-label">Cơ sở khác</label>
                                    <div class="form-group">
                                        @foreach (\App\Department::listData() as $department_id => $department_name)
                                            {{ Form::checkbox('departments[]', $department_id) }} {{ $department_name }}<br />
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div style="padding-left: 50px">
                                    <label for="groups" class="control-label">Nhóm cơ sở</label>
                                    <div class="form-group">
                                        @foreach (\App\RGroup::listData() as $group_id => $group_name)
                                            {{ Form::checkbox('groups[]', $group_id) }} {{ $group_name }}<br />
                                        @endforeach
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div style="padding-left: 50px">
                                    <label for="role" class="control-label required">Quyền hạn</label>
                                    <div class="form-group">
                                        @foreach ($role_list as $role)
                                            {{ Form::checkbox('role[]', $role->id) }} {{ $role->display_name }}<br />
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-edufit-default">
                                    Đăng ký
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
