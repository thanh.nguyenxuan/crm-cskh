@extends('layouts.main')

@section('content')
<div class="x_panel">
    <div class="x_title">
        <h2>Quản lý người dùng</h2>
        <div class="pull-right">
            <a class="btn btn-edufit-default" href="{{ route('user.create') }}">Thêm người dùng</a><br/>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <table class="table table-hover">
            <thead>
            <th></th>
            <th></th>
            <th>Tên đăng nhập</th>
            <th>Họ và tên</th>
            <th>Email</th>
            <th>Cơ sở</th>
            <th>Số máy lẻ</th>
            <th>Active</th>
            <th>Quyền hạn</th>
            @if(Auth::user()->hasRole('admin'))
                <th>Đăng nhập như</th>
            @endif
            </thead>
            <tbody>
            @foreach($user_list as $user)
                <tr>
                    <td><a href="{{ route('user.edit', $user->id) }}" title="Chỉnh sửa"><i class="fa fa-lg fa-pencil"
                                                                                           aria-hidden="true"></i></a></td>
                    <td><a href="{{ route('user.resetpassword', $user->id) }}" title="Reset mật khẩu"><i
                                    class="fa fa-lg fa-key" aria-hidden="true"></i></a></td>
                    <td>{{ $user->username }}</td>
                    <td>{{ $user->full_name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->department }}</td>
                    <td>{{ $user->extension }}</td>
                    <td>{!! $user->active?'<span class="user-status active">Active</span>':'<span class="user-status inactive">Inactive</span>' !!}</td>
                    <td>
                        @foreach($user->roles as $role)
                            {{ $role->display_name }}<br/>
                        @endforeach
                    </td>
                    @if(Auth::user()->hasRole('admin'))
                        <td>
                            <a href="{{ route('user.loginAs', $user->id) }}" title="Đăng nhập như {{ $user->username }}"><i
                                        class="fa fa-lg fa-user" aria-hidden="true"></i></a>
                        </td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
</div>
@stop