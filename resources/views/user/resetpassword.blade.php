@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-edufit-default">
                <div class="panel-heading">Đổi mật khẩu</div>
                <div class="panel-body">
                    @if(!empty($message))
                        <div class="alert alert-danger" role="alert">{{ $message }}</div>
                    @endif
                    @if(!empty($success))
                        <div class="alert alert-success" role="alert">Lưu dữ liệu thành công!</div>
                    @endif
                    <div class="alert alert-warning" role="alert">
                        Mật khẩu cần tối thiểu gồm 6 ký tự.
                    </div>
                    <form class="form-horizontal" role="form" method="POST"
                          action="{{ route('user.resetpassword', $user->id) }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-md-4 control-label">Tên đăng nhập</label>

                            <div class="col-md-6">
                                <label class="control-label">{{ $user->username }}</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="col-md-4 control-label">Họ và tên</label>

                            <span class="col-md-6">
                                <label class="control-label">{{ $user->full_name }}</label>
						</span>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label required">Mật khẩu mới</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="col-md-4 control-label required">Xác nhận mật khẩu mới</label>

                            <div class="col-md-6">
                                <input id="password_confirmation" type="password" class="form-control"
                                       name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" name="submit" id="submit" value="save"
                                        class="btn btn-edufit-default">
                                    Lưu
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#password_change').change(function () {
            //console.log($('#password').prop('disabled'));
            if ($('#password').prop('disabled')) {
                $('#password').prop('disabled', false);
                $('#password-confirm').prop('disabled', false);
            } else {
                $('#password').prop('disabled', true);
                $('#password-confirm').prop('disabled', true);
                $('#password').val('');
                $('#password-confirm').val('');
            }
        });
    });
</script>
@stop