<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PbxTrunk extends Model
{
    protected $table = 'fptu_trunk';
}
