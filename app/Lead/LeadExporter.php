<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 11/05/2018
 * Time: 7:58 PM
 */

namespace App\Lead;

use App\Major;
use Illuminate\Support\Facades\Auth;
use PHPExcel;
use PHPExcel_IOFactory;
use App\Department;
use App\LeadStatus;
use App\CallStatus;
use App\Rating;


class LeadExporter
{
    public function export($data)
    {
        set_time_limit(-1);
        ini_set('memory_limit', -1);
        //var_dump($data);exit;
        $objPHPExcel = new PHPExcel();

        // Miscellaneous glyphs, UTF-8
        $objPHPExcel->setActiveSheetIndex(0);
        $row_list = array();
        $header_row = array(
            'STT',
            'ID',
            'Họ tên phụ huynh',
            'Họ tên học sinh',
            'Ngày sinh',
            'SĐT 1',
            'SĐT 2',
            'Địa chỉ',
            'Email',
            'Facebook',
            'Trạng thái lead',
            'Trạng thái chăm sóc',
            'Lớp học quan tâm',
            'Mức độ quan tâm',
            'Đặt cọc giữ chỗ/waiting list',
            'Ngày nhập học',
            'Cơ sở',
            'Tỉnh',
            'Trường đang học',
            'Nguồn dẫn',
            'Nguồn thông tin',
            'Ghi chú',
            'Người phụ trách',
            'Người tạo',
            'Ngày tạo',
            'Tổng số Log Call',
            'Log Call gần nhất',
            'Ngày showup đến trường',
            'Ngày showup hội thảo',
            'Campaign',
            'Source',
            'UTM Source',
            'UTM Campaign',
            'UTM Medium',
            'UTM Content',
            'UTM Term',
            'UTM Link',
            'Mã Học sinh',
            'Lớp học',
        );
        $row_list[] = $header_row;
        $stt = 0;
        $lead_status_array = LeadStatus::all()->pluck('name', 'id')->toArray();
        $call_status_array = CallStatus::all()->pluck('name', 'id')->toArray();
        $department_array = Department::all()->pluck('name', 'id')->toArray();
        $rating_array = Rating::all()->pluck('name', 'id')->toArray();
        $major_array = Major::all()->pluck('name', 'id')->toArray();

        foreach ($data as $item) {
            $row = array();
            $stt++;
            $row[] = $stt;
            $row[] = $item->id;
            $row[] = $item->name;
            $row[] = $item->student_name;
            $row[] = $item->birthdate;
            $row[] = (Auth::user()->hasRole('reporter')) ? $item->phone1 : '';
            $row[] = (Auth::user()->hasRole('reporter')) ? $item->phone2 : '';
            $row[] = $item->address;
            $row[] = $item->email;
            $row[] = $item->facebook;
            if (!empty($item->status && !empty($lead_status_array[$item->status]))) {
                $row[] = $lead_status_array[$item->status];
            } else
                $row[] = '';
            if (!empty($item->call_status) && !empty($call_status_array[$item->call_status]))
                $row[] = $call_status_array[$item->call_status];
            else
                $row[] = '';
            if (!empty($item->rating) && !empty($rating_array[$item->rating]))
                $row[] = $rating_array[$item->rating];
            else
                $row[] = '';
            if (!empty($item->major) && !empty($major_array[$item->major]))
                $row[] = $major_array[$item->major];
            else
                $row[] = '';
            if (!empty($item->activated_at))
                $row[] = date('d/m/Y H:i:s', strtotime($item->activated_at));
            else
                $row[] = '';
            if (!empty($item->enrolled_at))
                $row[] = date('d/m/Y H:i:s', strtotime($item->enrolled_at));
            else
                $row[] = '';
            if (!empty($item->department_id) && !empty($department_array[$item->department_id]))
                $row[] = $department_array[$item->department_id];
            else
                $row[] = '';
            $row[] = $item->province;
            $row[] = $item->school;
            $row[] = $item->source_list;
            $row[] = $item->info_source_name;
            $row[] = $item->notes;
            $row[] = $item->assignee_list;
            $row[] = $item->creator;
            $row[] = $item->created_at;
            $row[] = $item->total_log_call;
            $row[] = $item->last_log_call;
            $row[] = (!empty($item->showup_school_at)) ? date('d/m/Y H:i:s', strtotime($item->showup_school_at)) : '';
            $row[] = (!empty($item->showup_seminor_at)) ? date('d/m/Y H:i:s', strtotime($item->showup_seminor_at)) : '';
            $row[] = $item->campaign_list;
            $row[] = $item->source;
            $row[] = $item->utm_source;
            $row[] = $item->utm_campaign;
            $row[] = $item->utm_medium;
            $row[] = $item->utm_content;
            $row[] = $item->utm_term;
            $row[] = $item->utm_link;
            $row[] = $item->student_code;
            $row[] = $item->class_name;
            $row_list[] = $row;
        }

        $objPHPExcel->getActiveSheet()->fromArray($row_list, '', 'A1');
        $objPHPExcel->setActiveSheetIndex(0);


        // Redirect output to a client’s web browser (Excel2007)
        $date = date('YmdHis');
        //var_dump($date);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Leads_Export_' . $date . '.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }
}