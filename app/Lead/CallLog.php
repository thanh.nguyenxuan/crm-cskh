<?php

namespace App\Lead;

use App\Department;
use App\Report\Cdr;
use App\Role;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Parser\Exception;

class CallLog extends Model
{
    protected $table = 'edf_call_log';

    CONST DIRECTION_OUT = 1;    // gọi đi
    CONST DIRECTION_IN = 2;     // gọi đến

    CONST STATUS_ANSWERED = 1;      // có trả lời
    CONST STATUS_NOT_ANSWER = 2;    // không trả lời
    CONST STATUS_NOT_CONNECTED = 3; // thuê bao không liên lạc được
    CONST STATUS_WRONG_NUMBER = 4;  // Sai số

    /**
     * Chiều cuộc gọi
     *
     * @return array
     */
    public static function getListDirection(){
        return array(
            self::DIRECTION_OUT => 'Gọi đi',
            self::DIRECTION_IN => 'Gọi đến',
        );
    }

    /**
     * Trạng thái cuộc gọi
     *
     * @return array
     */
    public static function getListStatus(){
        return array(
            self::STATUS_ANSWERED => 'Có trả lời',
            self::STATUS_NOT_ANSWER => 'Không trả lời',
            self::STATUS_NOT_CONNECTED => 'Thuê bao không liên lạc được',
            self::STATUS_WRONG_NUMBER => 'Sai số',
        );
    }

    /**
     * All user
     *
     * @param $department_id
     * @return array
     */
    public static function getListUser($department_id = null){
        $data = array();
        if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding')) {
            if(!empty($department_id)){
                $data = User::whereRaw("(department_id = $department_id
                        OR id IN (SELECT user_id FROM user_department WHERE department_id = $department_id)   
                        OR id IN (SELECT user_id FROM group_user WHERE group_id IN (
                            SELECT group_id FROM group_department WHERE department_id = $department_id
                        ))
                    )")
                    ->where('active', 1)->whereNull('deleted_at')->select(DB::raw("CONCAT(username,' - ', full_name) as username"), 'id')->orderBy('username', 'ASC')->pluck('username', 'id')->toArray();
            }else{
                $data = User::where('active', 1)->whereNull('deleted_at')->select(DB::raw("CONCAT(username,' - ', full_name) as username"), 'id')->orderBy('username', 'ASC')->pluck('username', 'id')->toArray();
            }
        }else{
            $departments_str = implode(',', Auth::user()->getManageDepartments());
            $data = User::whereRaw("(department_id IN ($departments_str)
                    OR id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                    OR id IN (SELECT user_id FROM group_user WHERE group_id IN (
                        SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                    ))
                )")
                ->where('active', 1)->whereNull('deleted_at')->select(DB::raw("CONCAT(username,' - ', full_name) as username"), 'id')->orderBy('username', 'ASC')->pluck('username', 'id')->toArray();
        }
        return $data;
    }

    /**
     * Telesales - Nhân viên nghe máy
     *
     * @param $department_id
     * @return array
     */
    public static function getListUserTVTS($department_id = null){
        $data = array();
        if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding')) {
            if(!empty($department_id)){
                $query = User::whereRaw("(department_id = $department_id
                        OR id IN (SELECT user_id FROM user_department WHERE department_id = $department_id)   
                        OR id IN (SELECT user_id FROM group_user WHERE group_id IN (
                            SELECT group_id FROM group_department WHERE department_id = $department_id
                        ))
                    )")
                    ->where('active', 1)->whereNull('deleted_at');
            }else{
                $query = User::where('active', 1)->whereNull('deleted_at');
            }
        }else{
            $departments_str = implode(',', Auth::user()->getManageDepartments());
            $query = User::whereRaw("(department_id IN ($departments_str)
                    OR id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                    OR id IN (SELECT user_id FROM group_user WHERE group_id IN (
                        SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                    ))
                )")
                ->where('active', 1)->whereNull('deleted_at');
        }
        $query->whereHas('roles', function ($q) {
            $q->whereIn('name', array('telesales','teleteamleader'));
        });
        $data = $query->select(DB::raw("CONCAT(username,' - ', full_name) as username"), 'id')->orderBy('username', 'ASC')->pluck('username', 'id')->toArray();

        return $data;
    }

    public static function getListDepartment(){
        $data = array();
        if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding')){
            $data = Department::orderBy('sort_index')->pluck('name', 'id')->toArray();
        }
        return $data;
    }

    public static function getListUserRole(){
        $data = array();
        if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding')){
            $data = Role::all()->pluck('display_name', 'id')->toArray();
        }else if(Auth::user()->hasRole('teleteamleader')){
            $data = Role::whereIn('id', array(2,4))->pluck('display_name', 'id')->toArray();
        }
        return $data;
    }

    /**
     * tạo html option cho select
     *
     * @param $data array (key => value)
     * @param $selected_value
     * @param $empty_text
     * @return string
     */
    public static function generateOptions($data, $selected_value = null, $empty_text = null){
        $html = "";
        if(!empty($empty_text)){
            $html.= "<option value=''>{$empty_text}</option>";
        }
        foreach ($data as $key => $value){
            $selected = (!empty($selected_value) && $key == $selected_value) ? "selected" : "";
            $html.= "<option value='{$key}' {$selected}>{$value}</option>";
        }
        return $html;
    }

    /**
     * Các option cho filter chiều cuộc gọi
     *
     * @param $selected_value
     * @return string
     */
    public static function getOptionsDirection($selected_value = null){
        return self::generateOptions(self::getListDirection(), $selected_value, 'Tất cả');
    }

    /**
     * Các option cho filter trạng thái cuộc gọi
     *
     * @param $selected_value
     * @return string
     */
    public static function getOptionsStatus($selected_value = null){
        return self::generateOptions(self::getListStatus(), $selected_value, 'Tất cả');
    }

    /**
     * Các option cho filter user
     *
     * @param $selected_value
     * @param $department_id
     * @return string
     */
    public static function getOptionsUser($selected_value = null, $department_id = null){
        return self::generateOptions(self::getListUser($department_id), $selected_value, 'Tất cả');
    }

    /**
     * Các option cho filter telesales
     *
     * @param $selected_value
     * @param $department_id
     * @return string
     */
    public static function getOptionsUserTVTS($selected_value = null, $department_id = null){
        return self::generateOptions(self::getListUserTVTS($department_id), $selected_value, 'Tất cả');
    }

    /**
     * Các option cho filter cơ sở
     *
     * @param $selected_value
     * @param $department_id
     * @return string
     */
    public static function getOptionsDepartment($selected_value = null){
        return self::generateOptions(self::getListDepartment(), $selected_value, 'Tất cả');
    }

    public static function getOptionsUserRole($selected_value = null){
        return self::generateOptions(self::getListUserRole(), $selected_value, 'Tất cả');
    }

    public static function getOptionsDepartmentLevel($selected_value = null){
        $data = array(
            'GIS' => 'GIS',
            'SMIS' => 'SMIS',
        );
        return self::generateOptions($data, $selected_value, 'Tất cả');
    }


    public static function dailySync()
    {
        $now = date('Y-m-d');
        $start_date = date('Y-m-d 23:00:00', strtotime($now . ' - 1 days'));
        $end_date = date('Y-m-d 22:59:59', strtotime($now));
        $query = self::whereNull('deleted_at')
            ->where('created_at', '>=', $start_date)
            ->where('created_at', '<=', $end_date)
            ->where('status', self::STATUS_ANSWERED)
            ->whereNotNull('data')
            ->whereNull('file_url')
        ;
        $call_logs = $query->get();
        if(!empty($call_logs)){
            $total = count($call_logs);
            $i=0;
            foreach ($call_logs as $call_log){
                $i++;
                echo $i.'/'.$total.': ';
                echo $call_log->id.' ';
                $data = json_decode($call_log->data);
                if(!empty($data->download_url)){
                    $folder = '/uploads/call_logs/' . date('Y/m/d');
                    $folder_path = public_path() . $folder;
                    $file_name = $data->session.'_'.$call_log->id.'.wav';
                    $file_path = $folder_path.'/'.$file_name;
                    $file_source = str_replace(' ', '%20', $data->download_url);
                    if (!file_exists($folder_path)) {
                        mkdir($folder_path, 0777, TRUE);
                    }
                    $file = fopen($file_path, "w");
                    file_put_contents($file_path, file_get_contents($file_source));
                    fclose($file);
                    $call_log->file_url = $folder.'/'.$file_name;
                    $call_log->save();
                }
                echo " DONE";
                echo "\n";
            }
        }


    }

    public static function syncEmptyFileDaily()
    {
        $start_date = date('Y-m-d 00:00:00');
        $end_date = date('Y-m-d 23:59:59');

        $callLogs = self::whereRaw("created_at >= '$start_date' AND created_at <= '$end_date'")
            ->where('status', self::STATUS_ANSWERED)
            ->whereNotNull('data')
            ->whereRaw("data LIKE '%\"recordingfile\":\"\"%'")
            ->get();
        if(!empty($callLogs)){
            $cdr = new Cdr();
            foreach ($callLogs as $callLog){
                try{
                    $call_log_data = json_decode($callLog->data);
                    if(!empty($call_log_data->session)){
                        $data = $cdr->getReportCdr($start_date, $end_date, array('call_session' => $call_log_data->session));
                        if(!empty($data['data'])){
                            $callLog->data = json_encode($data['data'][0]);
                            $callLog->save();
                        }
                    }
                }catch (Exception $e){
                    continue;
                }
            }
        }

    }

}