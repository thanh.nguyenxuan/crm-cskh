<?php
function px_shortcode_reservation_form() {
    $action = sanitize_text_field($_POST['action']);
    if(!empty($action) && $action == 'register') {
        $name = sanitize_text_field($_POST['fullname']);
        $phone = sanitize_text_field($_POST['phone']);
        $cmnd = sanitize_text_field($_POST['cmnd']);
        $route = sanitize_text_field($_POST['route1']);
        $pickup = sanitize_text_field($_POST['pickup']);
        $destination = sanitize_text_field($_POST['destination']);
        $date_reserved = sanitize_text_field($_POST['date_reserved']);
        $time = sanitize_text_field($_POST['departure_time']);
        $vehicle_no = sanitize_text_field($_POST['vehicle_no']);
        $selected_seats = sanitize_text_field($_POST['selected_seats']);
        $seat_array = explode(',', $selected_seats);
        $reservation_id = wp_insert_post(array(
            'post_title' => $date_reserved. ' - ' . $phone . ' - ' . $name,
            'post_type' => 'reservation',
            'post_status' => 'publish',
        ));
        add_post_meta($reservation_id, 'full_name', $name);
        add_post_meta($reservation_id, 'phone', $phone);
        add_post_meta($reservation_id, 'cmnd', $cmnd);
        add_post_meta($reservation_id, 'route', $route);
        add_post_meta($reservation_id, 'pickup', $pickup);
        add_post_meta($reservation_id, 'destination', $destination);
        add_post_meta($reservation_id, 'time', $time);
        $date_reserved = implode('-', array_reverse(explode('/', $date_reserved)));
        add_post_meta($reservation_id, 'date_reserved', $date_reserved);
        add_post_meta($reservation_id, 'vehicle_no', $vehicle_no);
        add_post_meta($reservation_id, 'seat', $seat_array);
        $successful = 1;
        $date_reserved_human = implode('/', array_reverse(explode('-', $date_reserved)));
        $route_name = get_the_title($route);
        $departure_time = get_post_meta( $time, 'departure_time', true);
        $date_created = get_the_date('d/m/Y H:i:s', $reservation_id);
        $to = array('lehoang267@gmail.com', 'px.limousine@gmail.com');
        sort($seat_array, SORT_NUMERIC);
        $location = implode(', ', $seat_array);
        $message = "<h2>THÔNG TIN ĐẶT VÉ</h2>Họ và tên: <strong>$name</strong><br />SĐT: <strong>$phone</strong><br />CMND: $cmnd<br />Lộ trình: $route_name<br />Điểm đón: $pickup <br />Điểm đến: $destination<br />Giờ xuất bến: $departure_time<br />Ngày đặt xe: $date_reserved_human<br />Xe số: $vehicle_no<br />Vị trí ghế: $location<br />Thời điểm đặt vé online: $date_created";
        add_filter( 'wp_mail_content_type', 'px_set_html_content_type' );
        wp_mail($to, "Khách hàng $phone đặt xe Limousine", $message);
        remove_filter( 'wp_mail_content_type', 'px_set_html_content_type' );
    }
    ?>
    <p style="text-transform: uppercase; color: red;">
        <strong>Quý khách sẽ được giảm giá 10% khi đặt vé trực tuyến trên website</strong>
    </p>
    Các mục có dấu <span class="required">(*)</span> là bắt buộc phải nhập.</br />
    <form method="post">
        <table border="0">
            <?php
            if($successful) {
                ?>
                <tr>
                    <td colspan="2">
                        <span style="color: green; font-size: 20px; text-transform: uppercase; font-weight:bold;">Quý khách đã đặt vé thành công.</span>
                    </td>
                </tr>
                <?php
            }
            ?>
            <tr>
                <td><label for="fullname" class="label-required">Họ và tên</label></td>
                <td><input type="text" name="fullname" size="30" required="required"/></td>
            </tr>
            <tr>
                <td><label for="phone" class="label-required">Số điện thoại</label></td>
                <td><input type="text" name="phone" size="30" required="required"/></td>
            </tr>
            <tr>
                <td><label for="cmnd">Số CMND: </label></td>
                <td><input type="text" name="cmnd" size="30"/></td>
            </tr>
            <tr>
                <td><label for="cmnd" class="label-required">Lộ trình</label></td>
                <td>
                    <select id="route1" name="route1" required="required">
                        <option>Chọn một lộ trình</option>
                        <?php
                        $routes = get_posts(array('post_type' => 'route', 'order' => 'ASC', 'post_status' => 'publish', 'posts_per_page' => 100));
                        foreach($routes as $route) {
                            echo "<option value='{$route->ID}'>{$route->post_title}</option>";
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td><label for="cmnd" class="label-required">Giờ xuất bến</label></td>
                <td>
                    <select id="departure_time" name="departure_time" required="required">
                        <option>Chọn giờ xuất bến</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td><label for="date_reserved" class="label-required">Ngày đặt vé</label></td>
                <td><input type="text" name="date_reserved" id="date_reserved" size="30" required="required"/></td>
            </tr>
            <tr>
                <td><label for="pickup" class="label-required">Điểm đón</label></td>
                <td><input type="text" name="pickup" size="30" required="required"/></td>
            </tr>
            <tr>
                <td><label for="destination" class="label-required">Điểm đến</label></td>
                <td><input type="text" name="destination" size="30" required="required"/></td>
            </tr>
            <tr>
                <td colspan="2">
                    <label for="destination" class="label-required">Chọn ghế ngồi</label><br />
                    <input type="hidden" name="vehicle_no" id="vehicle_no" value="1" />
                    <input type="hidden" name="selected_seats" id="selected_seats" />
                    <div id="seat_tips"></div>
                    <div id="seat-selector">

                    </div>
            </tr>
            <tr>
                <td><label for="subtotal">Thành tiền</label></td>
                <td><span id="subtotal">0</span>đ</td>
            </tr>
            <tr>
                <td><label for="promotion">Giảm trừ</label></td>
                <td><span id="promotion">0</span>đ</td>
            </tr>
            <tr>
                <td><label for="total">Tổng tiền phải thanh toán</label></td>
                <td><span id="total" style="color: red; font-weight: bold;">0đ</span></td>
            </tr>
            <tr>
                <td></td>
                <td><button type="submit" name="action" value="register">Đặt vé</button></td>
            </tr>
        </table>
    </form>
    <div id="route_description">
        DANH SÁCH CÁC LỘ TRÌNH:<br />
        <?php
        foreach($routes as $route) {
            $route_id = $route->ID;
            $route_title = $route->post_title;
            $route_description = get_post_meta($route_id, 'description', true);
            echo "<strong>$route_title</strong><br />" . (!empty($route_description)?$route_description .'<br />':'');
        }
        ?>
    </div>
    <?php
}
$times = get_posts(array('post_type' => 'time', 'posts_per_page' => 100));
$departure_location = array();
$departure_location_json = array();
foreach($times as $time) {
    $depart_loc = get_post_meta($time->ID, 'departure_location', true);
    $departure_time = get_post_meta($time->ID, 'departure_time', true);
    //echo $depart_loc . ' - ' . $time->ID, '<br />';
    if(!isset($departure_location[$depart_loc]))
        $departure_location[$depart_loc] = array();
    $time_id = $time->ID;
    $departure_location[$depart_loc][] = array( 'id' => $time_id, 'time' => $departure_time );
}
foreach($departure_location as $loc => $time) {
    $depart_loc_object = array();
    $depart_loc_object['location'] = $loc;
    $depart_loc_object['time'] = $time;
    $departure_location_json[] = $depart_loc_object;
}
add_shortcode('reservation-form', 'px_shortcode_reservation_form');
function add_reservation_script() {
    global $departure_location_json;
    wp_enqueue_script('reservation-main-script', plugin_dir_url( __FILE__ ) . 'js/reservation.js');
    $data = array(
        'ajaxurl' => admin_url('admin-ajax.php'),
        'departure_location' => json_encode($departure_location_json),
    );
    wp_localize_script('reservation-main-script', 'reservation', $data);
}
add_action( 'wp_enqueue_scripts', 'add_reservation_script');
