function update_time_selector(location, time) {
    newOptions = {'0': "Chọn giờ xuất bến"};
    available = false;
    for(i = 0; i < time.length; i++) {
        depart_record = time[i];
        if(depart_record.location == location) {
            available = true;
            for(j = 0; j < depart_record.time.length; j++) {
                newOptions[depart_record.time[j]['id']] = depart_record.time[j]['time'];
            }
        }
    }
    if(!available) {
        newOptions = {'': 'Lộ trình tạm dừng hoạt động'};
    }
    var el = jQuery("#departure_time");
    el.empty(); // remove old options
    jQuery.each(newOptions, function(value,key) {
        el.append(jQuery("<option></option>")
            .attr("value", value).text(key));
    });
    jQuery('#departure_time option[value=""]').attr("selected",true);
}
function update_seat_selector() {
    var data = {
        'action': 'trips_no',
        'date' : jQuery('#date_reserved').val(),
        'route': jQuery('#route1').val(),
        'departure_time' : jQuery('#departure_time').val(),
    };

    // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
    var tip = "";
    jQuery.post(reservation.ajaxurl, data, function(response) {
        if(response > 1)
            tip = "Quý khách vui lòng chọn chỗ ngồi trên một trong <strong>" + response + "</strong> xe dưới đây";
        else
            tip = "Quý khách vui lòng chọn chỗ ngồi trên xe";
        jQuery("#seat_tips").html(tip);
        var post_data = {
            'action': 'print_seat_selector_html',
            'num' : response,
            'route': jQuery('#route1').val(),
            'time': jQuery('#departure_time').val(),
            'date': jQuery('#date_reserved').val()
        };
        jQuery.post(reservation.ajaxurl, post_data, function(responseHTML) {
            jQuery("#seat-selector").html(responseHTML);
        });
    });
}

function calculate_payment(seats) {
    var total = 0;
    var seat = 0;
    for(var i = 0; i < seats.length; i++) {
        console.log(seats[i]);
        seat = parseInt(seats[i]);
        switch(seat) {
            case 9:
            case 8:
                total+=180000;
                break;
            case 5:
            case 6:
            case 7:
                total += 220000;
                break;
            default:
                total += 240000;
                break;
        }
    }
    return total;
}
jQuery(document).ready(function() {
    var v1 = [];
    var today = new Date();
    jQuery('#date_reserved').datepicker({
        dateFormat : 'dd/mm/yy',
        minDate: today
    });
    //alert(reservation.departure_location);
    jQuery("#route1").on("change", function() {
        var str = "";
        jQuery( "#route1 option:selected" ).each(function() {
            str = jQuery( this ).val();
        });

        update_time_selector(str, JSON.parse(reservation.departure_location));
    });
    jQuery("#route1").change(update_seat_selector);
    jQuery("#departure_time").change(update_seat_selector);
    jQuery("#date_reserved").change(update_seat_selector);
    jQuery(".seat").live("click", function() {
        var seat_id = jQuery(this).attr("id");
        var seat_arr = seat_id.split("-");
        var seat = "";
        if(seat_arr.length == 4)
            seat = seat_arr[1];
        vehicle = seat_arr[3];
        if(jQuery(this).hasClass('seat-selected')) {
            jQuery(this).removeClass('seat-selected').addClass('seat-available');
            var new_v1 = jQuery.grep(v1, function(value) {
                if(value != seat)
                    return value;
            });
            v1 = new_v1;
        } else if(jQuery(this).hasClass('seat-available')) {
            jQuery(this).removeClass('seat-available').addClass('seat-selected');
            if(jQuery.inArray(seat, v1) == -1)
                v1.push(seat);
            jQuery("#vehicle_no").val(vehicle);
        }
        var subtotal = calculate_payment(v1);
        var promotion = subtotal * 0.1;
        var total = subtotal - promotion;
        jQuery('#subtotal').text(subtotal.toLocaleString('vi-VN'));
        jQuery('#promotion').text(promotion.toLocaleString('vi-VN'));
        jQuery('#total').text(total.toLocaleString('vi-VN') + 'đ');
        jQuery("#selected_seats").val(v1.join());
        tip = "Quý khách đã chọn ghế <strong>"+v1.join()+"</strong> trên xe <strong>"+vehicle+"</strong>";
        jQuery("#seat_tips").html(tip);
    });
    jQuery(".label-required").append("<span class='required'> (*): </span>");
});