<?php
/**
 * @package Phuc Xuyen Reservation
 * @version 1.0
 */
/*
Plugin Name: Phuc Xuyen Reservation
Plugin URI: http://phucxuyen.com.vn
Description: Trang đặt vé của Phúc Xuyên
Author: Lê Đại Hoàng
Version: 1.0
Author URI: http://lehoang.net
*/
function px_register_time_post_type() {
    register_post_type( 'time',
        array(
            'labels' => array(
                'name' => 'Giờ xuất bến',
                'singular_name' => 'Giờ xuất bến',
                'add_new' => 'Thêm giờ xuất bến',
                'add_new_item' => 'Thêm giờ xuất bến',
                'edit' => 'Sửa',
                'edit_item' => 'Sửa giờ xuất bến',
                'new_item' => 'New Route',
                'view' => 'View',
                'view_item' => 'View Route',
                'search_items' => 'Tìm giờ xuất bến',
                'not_found' => 'No Routes found',
                'not_found_in_trash' => 'No Routes found in Trash',
                'parent' => 'Parent Route'
            ),

            'public' => true,
            'menu_position' => 15,
            'supports' => array( 'title'),
            'taxonomies' => array( '' ),
            'has_archive' => true
        )
    );

}
add_action('init', 'px_register_time_post_type');

function px_register_routes_post_type() {
    register_post_type( 'route',
        array(
            'labels' => array(
                'name' => 'Lộ trình',
                'singular_name' => 'Lộ trình',
                'add_new' => 'Thêm lộ trình mới',
                'add_new_item' => 'Thêm lộ trình mới',
                'edit' => 'Edit',
                'edit_item' => 'Edit Route',
                'new_item' => 'New Route',
                'view' => 'View',
                'view_item' => 'View Route',
                'search_items' => 'Tìm lộ trình',
                'not_found' => 'No Routes found',
                'not_found_in_trash' => 'No Routes found in Trash',
                'parent' => 'Parent Route'
            ),

            'public' => true,
            'menu_position' => 15,
            'supports' => array( 'title'),
            'taxonomies' => array( '' ),
            'has_archive' => true
        )
    );

}
add_action('init', 'px_register_routes_post_type');
function px_register_reservations_post_type() {
    register_post_type( 'reservation',
        array(
            'labels' => array(
                'name' => 'Đặt vé',
                'singular_name' => 'Đặt vé',
                'add_new' => 'Thêm vé mới',
                'add_new_item' => 'Thêm vé mới',
                'edit' => 'Sửa vé',
                'edit_item' => 'Sửa vé',
                'new_item' => 'Thêm vé mới',
                'view' => 'Xem',
                'view_item' => 'Xem vé mới',
                'search_items' => 'Tìm vé',
                'not_found' => 'Không tìm thấy vé nào có thông tin như bạn yêu cầu',
                'not_found_in_trash' => 'Không có vé nào trong thùng rác',
                'parent' => 'Parent Route'
            ),

            'public' => true,
            'menu_position' => 15,
            'supports' => array( 'title'),
            'taxonomies' => array( '' ),
            'has_archive' => true
        )
    );

}

add_action('init', 'px_register_reservations_post_type');

function px_register_trip_post_type() {
    register_post_type( 'trip',
        array(
            'labels' => array(
                'name' => 'Số chuyến',
                'singular_name' => 'Số chuyến',
                'add_new' => 'Thêm số chuyến mới',
                'add_new_item' => 'Thêm số chuyến mới',
                'edit' => 'Sửa số chuyến',
                'edit_item' => 'Sửa số chuyến',
                'new_item' => 'New Route',
                'view' => 'View',
                'view_item' => 'Xem chuyến',
                'search_items' => 'Search Routes',
                'not_found' => 'No Routes found',
                'not_found_in_trash' => 'No Routes found in Trash',
                'parent' => 'Parent Route'
            ),

            'public' => true,
            'menu_position' => 15,
            'supports' => array( 'title'),
            'taxonomies' => array( '' ),
            'has_archive' => true
        )
    );

}
add_action('init', 'px_register_trip_post_type');

add_action( 'admin_menu', 'px_register_reservation_config_menu' );
function px_register_reservation_config_menu() {
    add_submenu_page('edit.php?post_type=reservation', 'Danh sách khách hàng', 'Danh sách', 'manage_options', 'reservation/report.php' );

}
function px_custom_active_route_status() {

    $args = array(
        'label'                     => _x( 'Active', 'Status General Name', 'phucxuyen' ),
        'label_count'               => _n_noop( 'Active (%s)',  'Active (%s)', 'phucxuyen' ),
        'public'                    => true,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'exclude_from_search'       => false,
    );
    register_post_status( 'active', $args );

}
add_action( 'init', 'px_custom_active_route_status', 0 );

add_action('admin_footer-post.php', 'jc_append_post_status_list');
function jc_append_post_status_list(){
    global $post;
    $complete = '';
    $label = '';
    if($post->post_type == 'route'){
        if($post->post_status == 'active'){
            $complete = ' selected=\"selected\"';
            $label = '<span id=\"post-status-display\"> Active</span>';
        }
        echo '<script>
          jQuery(document).ready(function($){
               $("select#post_status").append("<option value=\"active\" '.$complete.'>Active</option>");
               $(".misc-pub-post-status label").append("'.$label.'");
          });
          </script>
          ';
    }
}
function hkdc_admin_styles() {
    wp_enqueue_style( 'jquery-ui-datepicker-style' , '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css');
}
add_action('admin_print_styles', 'hkdc_admin_styles');
function hkdc_admin_scripts() {
    wp_enqueue_script( 'jquery-ui-datepicker' );
}
add_action('admin_enqueue_scripts', 'hkdc_admin_scripts');

add_action( 'wp_ajax_trips_no', 'px_ajax_get_number_of_trips' );
add_action( 'wp_ajax_nopriv_trips_no', 'px_ajax_get_number_of_trips' );

function px_ajax_get_number_of_trips() {
    global $wpdb;

    $date = implode('-', array_reverse(explode('/', sanitize_text_field($_POST['date']))));
    $departure_time = sanitize_text_field($_POST['departure_time']);
    $route_id = sanitize_text_field($_POST['route']);
    if( empty($date) || empty($departure_time) || empty($route_id) ) {
        echo 0;
        wp_die();
    }
    $date = implode('-', array_reverse(explode('/', $date)));
    $query = "SELECT a.meta_value FROM wp_postmeta a 
			INNER JOIN wp_postmeta b
			ON a.post_id = b.post_id
			INNER JOIN wp_postmeta c
			ON a.post_id = c.post_id
			INNER JOIN wp_postmeta d
			ON a.post_id = d.post_id
			WHERE a.meta_key = 'trip_no'
			AND b.meta_key = 'route'
			AND b.meta_value = '$route_id'
			AND c.meta_key = 'departure_time'
			AND c.meta_value = '$departure_time'
			AND d.meta_key = 'date'
			AND d.meta_value = '$date'
	";
    $result = $wpdb->get_var($query);
    if(!empty($result))
        echo $result;
    else
        echo 1;
    wp_die();
}

function px_ajax_get_departure_time_list() {
    global $wpdb;

    $route = sanitize_text_field($_POST['route']);
    $query = "SELECT a.post_id as route, b.meta_value as time FROM wp_postmeta a
	INNER JOIN wp_postmeta  b
	ON a.post_id = b.post_id
	WHERE a.meta_key = 'departure_location'
	AND a.meta_value = '$route'
	AND b.meta_key = 'departure_time'";
    $result = $wpdb->get_results($query, 0);
    $data = array();
    foreach($result as $record) {
        $data[$record->route] = $record->time;
    }
    if(!empty($result))
        echo json_encode($data);
    wp_die();
}
add_action( 'wp_ajax_get_available_seats', 'px_ajax_get_available_seats' );
add_action( 'wp_ajax_nopriv_get_available_seats', 'px_ajax_get_available_seats' );
function px_get_available_seats($route, $time, $date, $vehicle_no) {
    global $wpdb;
    $available_seats = array(1, 2, 3, 4, 5, 6, 7, 8, 9);
    $sql = "SELECT f.meta_value FROM wp_posts a
			INNER JOIN wp_postmeta b
			ON a.id = b.post_id
			INNER JOIN wp_postmeta c
			ON a.id = c.post_id
			INNER JOIN wp_postmeta d
			ON a.id = d.post_id
			INNER JOIN wp_postmeta e
			ON a.id = e.post_id
			INNER JOIN wp_postmeta f
			ON a.id = f.post_id
			WHERE a.post_status = 'publish'
AND b.meta_key = 'route'
			AND b.meta_value = '$route'
			AND c.meta_key = 'time'
			AND c.meta_value = '$time'
			AND d.meta_key = 'date_reserved'
			AND d.meta_value = '$date'
			AND e.meta_key = 'vehicle_no'
			AND e.meta_value = '$vehicle_no'
			AND f.meta_key = 'seat'";
    $result = $wpdb->get_col($sql, 0);
    foreach($result as $record) {
        $seat_o = unserialize($record);
    }
    foreach($result as $record) {
        $seated_array = unserialize($record);
        foreach($seated_array as $seated) {
            $seat = intval($seated);
            $index = array_search($seat, $available_seats);
            if($index !== FALSE && $index>=0) {
                array_splice($available_seats,$index, 1);
            }
        }
    }
    return $available_seats;
}
function px_ajax_get_available_seats() {
    global $wpdb;
    $route = sanitize_text_field($_POST['route']);
    $time = sanitize_text_field($_POST['time']);
    $date= implode('-', array_reverse(explode('/', sanitize_text_field($_POST['date']))));
    $vehicle_no = sanitize_text_field($_POST['vehicle_no']);
    $available_seats = px_get_available_seats($route, $time, $date, $vehicle_no);
    echo json_encode($available_seats);
    wp_die();
}
add_action( 'wp_ajax_get_departure_time_list', 'px_ajax_get_departure_time_list' );
add_action( 'wp_ajax_nopriv_get_departure_time_list', 'px_ajax_get_departure_time_list' );
function px_acf_trip_admin_head() {
    ?>
    <script type="text/javascript">
        function update_acf_departure_time_field() {
            var ajaxurl = '/wp-admin/admin-ajax.php';
            var data = {
                'action': 'get_departure_time_list',
                'route' : jQuery(".post-type-trip #acf-field-route").val(),
            };
            jQuery.post(ajaxurl, data, function(response) {
                var newOptions = new Object();
                if(response)
                    newOptions = JSON.parse(response);
                else {
                    newOptions = {"": "Lộ trình tạm ngừng hoạt động"};
                }
                var el = jQuery(".post-type-trip #acf-field-departure_time");
                el.empty(); // remove old options
                jQuery.each(newOptions, function(value,key) {
                    el.append(jQuery("<option></option>")
                        .attr("value", value).text(key));
                });
            });
        };
        jQuery(document).ready(function() {
            update_acf_departure_time_field();
            jQuery(".post-type-trip #acf-field-route").change( update_acf_departure_time_field );

        });
    </script>
    <?php
}

add_action('acf/input/admin_head', 'px_acf_trip_admin_head');

function px_acf_reservation_admin_head() {
    ?>
    <script type="text/javascript">
        function update_acf_reservation_time_field() {
            var ajaxurl = '/wp-admin/admin-ajax.php';
            var data = {
                'action': 'get_departure_time_list',
                'route' : jQuery(".post-type-reservation #acf-field-route").val(),
            };
            jQuery.post(ajaxurl, data, function(response) {
                var newOptions = new Object();
                if(response)
                    newOptions = JSON.parse(response);
                else {
                    newOptions = {"": "Lộ trình tạm ngừng hoạt động"};
                }
                var el = jQuery(".post-type-reservation #acf-field-time");
                el.empty(); // remove old options
                jQuery.each(newOptions, function(value,key) {
                    el.append(jQuery("<option></option>")
                        .attr("value", value).text(key));
                });
            });
        };
        function update_acf_trip_time_field() {
            var ajaxurl = '/wp-admin/admin-ajax.php';
            var data = {
                'action': 'get_departure_time_list',
                'route' : jQuery(".post-type-trip #acf-field-route").val(),
            };
            jQuery.post(ajaxurl, data, function(response) {
                var newOptions = new Object();
                if(response)
                    newOptions = JSON.parse(response);
                else {
                    newOptions = {"": "Lộ trình tạm ngừng hoạt động"};
                }
                var el = jQuery(".post-type-trip #acf-field-departure_time");
                el.empty(); // remove old options
                jQuery.each(newOptions, function(value,key) {
                    el.append(jQuery("<option></option>")
                        .attr("value", value).text(key));
                });
            });
        };
        jQuery(document).ready(function() {
            update_acf_reservation_time_field();
            jQuery(".post-type-reservation #acf-field-route").change( update_acf_reservation_time_field() );
            update_acf_trip_time_field();
            jQuery(".post-type-trip #acf-field-route").change( update_acf_trip_time_field() );

        });
    </script>
    <?php
}

add_action('acf/input/admin_head', 'px_acf_reservation_admin_head');

function px_set_html_content_type() {
    return 'text/html';
}
add_action( 'wp_ajax_print_seat_selector_html', 'px_print_seat_selector_html' );
add_action( 'wp_ajax_nopriv_print_seat_selector_html', 'px_print_seat_selector_html' );
function get_seat_class($seat, $seat_array) {
    if(in_array($seat, $seat_array))
        return 'seat-available';
    else
        return 'seat-unavailable';
}
function px_print_seat_selector_html() {
    $number = sanitize_text_field($_POST['num']);
    $route = sanitize_text_field($_POST['route']);
    $time = sanitize_text_field($_POST['time']);
    $date= implode('-', array_reverse(explode('/', sanitize_text_field($_POST['date']))));
    for($i=1;$i<=$number;$i++) {
        $available_seat_array = px_get_available_seats($route, $time, $date, $i);

        ?>
        <div class="vehicle-wrapper">
            <div class="vehicle-caption">XE <?php echo $i; ?></div>
            <div class="vehicle" id="vehicle-<?php echo $i; ?>">
                <div class="seat-row">
                    <div id="seat-driver-vehicle-<?php echo $i; ?>" class="seat driver seat-unavailable">LX</div>
                    <div id="seat-9-vehicle-<?php echo $i; ?>" class="seat seat9 <?php echo get_seat_class(9, $available_seat_array);?>">9</div>
                    <div id="seat-8-vehicle-<?php echo $i; ?>" class="seat seat8 <?php echo get_seat_class(8, $available_seat_array);?>">8</div>
                </div>
                <div class="seat-row">
                    <div id="seat-2-vehicle-<?php echo $i; ?>" class="seat seat2 <?php echo get_seat_class(2, $available_seat_array);?> special-seat">2</div>
                    <div id="seat-1-vehicle-<?php echo $i; ?>" class="seat seat1 <?php echo get_seat_class(1, $available_seat_array);?> special-seat special-seat-right">1</div>
                </div>
                <div class="seat-row">
                    <div id="seat-4-vehicle-<?php echo $i; ?>" class="seat seat4 <?php echo get_seat_class(4, $available_seat_array);?> special-seat">4</div>
                    <div id="seat-3-vehicle-<?php echo $i; ?>" class="seat seat3 <?php echo get_seat_class(3, $available_seat_array);?> special-seat special-seat-right">3</div>
                </div>
                <div class="seat-row">
                    <div id="seat-5-vehicle-<?php echo $i; ?>" class="seat seat5 <?php echo get_seat_class(5, $available_seat_array);?>">5</div>
                    <div id="seat-6-vehicle-<?php echo $i; ?>" class="seat seat6 <?php echo get_seat_class(6, $available_seat_array);?>">6</div>
                    <div id="seat-7-vehicle-<?php echo $i; ?>" class="seat seat7 <?php echo get_seat_class(7, $available_seat_array);?>">7</div>
                </div>
            </div>
        </div>
        <?php
    }
    if($number > 0) {
        ?>
        <div class="vehicle-wrapper">
            <div class="vehicle-caption">CHÚ THÍCH</div>
            <div class="vehicle">
                <div class="seat-row">
                    <div class="seat driver seat-unavailable">LX</div> Chỗ của lái xe
                </div>
                <div class="seat-row">
                    <div class="seat seat-unavailable">&nbsp;</div> Chỗ đã có người đặt
                </div>
                <div class="seat-row">
                    <div class="seat seat-available">&nbsp;</div> Chỗ còn trống
                </div>
                <div class="seat-row">
                    <div class="seat seat-selected">&nbsp;</div> Chỗ bạn đã chọn
                </div>
            </div>
        </div>
        <?php
    }
    wp_die();
}
add_filter( 'manage_reservation_posts_columns', 'px_set_custom_reservation_columns' );
add_action( 'manage_reservation_posts_custom_column' , 'px_reservation_custom_columns', 10, 2 );

function px_set_custom_reservation_columns($columns) {
    $columns['route'] = "Lộ trình";
    $columns['full_name'] = "Họ và tên";
    $columns['phone'] = "SĐT";
    $columns['time'] = "Giờ xuất bến";
    $columns['date_reserved'] = "Ngày đặt xe";
    $columns['vehicle_no'] = "Xe số";
    $columns['seat'] = "Ghế số";
    $columns['timestamp'] = "Đặt vé lúc";
    unset($columns['date']);
    unset($columns['td_post_views']);
    return $columns;
}
function px_reservation_custom_columns( $column, $post_id ) {
    switch ( $column ) {
        case 'route':
            $route = get_post_meta( $post_id, 'route', true );
            $route_name = get_the_title($route);
            echo $route_name;
            break;
        case 'full_name':
            echo get_post_meta( $post_id, 'full_name', true );
            break;
        case 'phone':
            echo get_post_meta( $post_id, 'phone', true );
            break;
        case 'time':
            $time = get_post_meta( $post_id, 'time', true );
            $departure_time = get_post_meta( $time, 'departure_time', true );
            echo $departure_time ;
            break;
        case 'vehicle_no':
            echo get_post_meta( $post_id, 'vehicle_no', true );
            break;
        case 'seat':
            $seat = get_post_meta( $post_id, 'seat', true );
            sort($seat, SORT_NUMERIC);
            echo implode(', ', $seat);
            break;
        case 'date_reserved':
            echo implode('/', array_reverse(explode('-', get_post_meta( $post_id, 'date_reserved', true ))));
            break;
        case 'timestamp':
            echo get_the_date('d/m/Y H:i:s', $post_id);
            break;
    }
}

add_filter( 'manage_route_posts_columns', 'px_set_custom_route_columns' );
add_action( 'manage_route_posts_custom_column' , 'px_route_custom_columns', 10, 2 );

function px_set_custom_route_columns($columns) {
    $columns['title'] = "Lộ trình";
    $columns['description'] = "Chi tiết lộ trình";
    $columns['timestamp'] = "Ngày tạo";
    unset($columns['date']);
    unset($columns['td_post_views']);
    return $columns;
}
function px_route_custom_columns( $column, $post_id ) {
    switch ( $column ) {
        case 'description':
            echo get_post_meta( $post_id, 'description', true );
            break;
        case 'timestamp':
            echo get_the_date('d/m/Y H:i:s', $post_id);
            break;
    }
}

function px_set_custom_trip_columns($columns) {
    $columns['date'] = "Ngày chạy";
    $columns['route'] = "Lộ trình";
    $columns['departure_time'] = "Giờ xuất bến";
    $columns['trip_no'] = "Số lượng chuyến";
    $columns['timestamp'] = "Ngày tạo";
    unset($columns['date']);
    unset($columns['td_post_views']);
    return $columns;
}
function px_trip_custom_columns( $column, $post_id ) {
    switch ( $column ) {
        case 'date':
            echo implode('/', array_reverse(explode('-', get_post_meta( $post_id, 'date', true ))));
            break;
        case 'route':
            $route = get_post_meta( $post_id, 'route', true );
            $route_name = get_the_title($route);
            echo $route_name;
            break;
        case 'departure_time':
            $time = get_post_meta( $post_id, 'departure_time', true );
            echo get_post_meta($time, 'departure_time', true);
            break;
        case 'trip_no':
            echo get_post_meta( $post_id, 'trip_no', true );
            break;
        case 'timestamp':
            echo get_the_date('d/m/Y H:i:s', $post_id);
            break;
    }
}
add_filter( 'manage_trip_posts_columns', 'px_set_custom_trip_columns' );
add_action( 'manage_trip_posts_custom_column' , 'px_trip_custom_columns', 10, 2 );

function px_set_custom_time_columns($columns) {
    $columns['departure_location'] = "Lộ trình";
    $columns['departure_time'] = "Giờ xuất bến";
    $columns['timestamp'] = "Ngày tạo";
    unset($columns['date']);
    unset($columns['td_post_views']);
    return $columns;
}
function px_time_custom_columns( $column, $post_id ) {
    switch ( $column ) {
        case 'departure_location':
            $route = get_post_meta( $post_id, 'departure_location', true );
            $route_name = get_the_title($route);
            echo $route_name;
            break;
        case 'departure_time':
            echo get_post_meta( $post_id, 'departure_time', true );
            break;
        case 'timestamp':
            echo get_the_date('d/m/Y H:i:s', $post_id);
            break;
    }
}
add_filter( 'manage_time_posts_columns', 'px_set_custom_time_columns' );
add_action( 'manage_time_posts_custom_column' , 'px_time_custom_columns', 10, 2 );
include_once 'reservation-form-shortcode.php';
function px_enqueue_reservation_scripts() {
    wp_enqueue_style( 'px-limo-reservation-style', plugins_url( 'css/style.css', __FILE__ ));
    //wp_enqueue_script( 'px-limo-reservation-main-script', get_template_directory_uri() . '/js/example.js', array(), '1.0.0', true );
}

add_action( 'wp_enqueue_scripts', 'px_enqueue_reservation_scripts' );
// function px_remove_unneccessary_meta_boxes() {
// remove_meta_box('mymetabox_revslider_0', 'reservation', 'normal');
// remove_meta_box('fbc_sectionid', 'reservation', 'normal');
// }
// add_action( 'admin_menu', 'px_remove_unneccessary_meta_boxes' );

function add_route_filter_to_reservation_administration(){

    //execute only on the 'post' content type
    global $post_type;
    if($post_type == 'reservation'){
        $route_list = get_posts(array('post_type' => 'route', 'posts_per_page' => 1000));
        ?>
        <select name = "route">
            <option value="all">Tất cả các lộ trình</option>
            <?php
            foreach($route_list as $route) {
                echo "<option value='{$route->ID}'>{$route->post_title}</option>";
            }
            ?>
        </select>
        <?php

    }
}
//add_action('restrict_manage_posts','add_route_filter_to_reservation_administration');

function px_filter_route_on_reservation($query){

    global $post_type, $pagenow;

    //if we are currently on the edit screen of the post type listings
    if($pagenow == 'edit.php' && $post_type == 'reservation'){
        if(isset($_GET['route'])){

            //get the desired post format
            $route = sanitize_text_field($_GET['route']);

            var_dump($route);

            // $query->query_vars['tax_query'] = array(
            // array(
            // 'taxonomy'  => 'post_format',
            // 'field'     => 'ID',
            // 'terms'     => array($post_format)
            // )
            // );
        }
    }
}
add_action('pre_get_posts','px_filter_route_on_reservation');
?>