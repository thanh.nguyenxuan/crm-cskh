<?php

namespace App;

use App\Suspect\Suspect;
use Carbon\Carbon;
use function GuzzleHttp\Psr7\str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class Lead extends Model
{
    use SoftDeletes;
    protected $table = 'fptu_lead';
    protected $dates = ['deleted_at'];
    //protected $fillable = array('name', 'phone1', 'phone2', 'phone3', 'phone4', 'email', 'facebook');
    // const CREATED_AT = 'date_created';
    // const UPDATED_AT = 'date_modified';
    public function checkDuplicate($number)
    {
        if (!empty($number)) {
            $count = Lead::where('id', '!=', $this->id)
                ->where(function ($count) use ($number) {
                    $count->where('phone1', $number)
                        ->orWhere('phone2', $number);
                })
                ->whereNull('deleted_at')
                ->where('department_id', $this->department_id)
                ->whereRaw('leave_school IS NULL OR leave_school != 1')
                ->count();
            if ($count > 0)
                return true;
        }
        return false;
    }

    public function getSMSTarget()
    {
        $target = (!empty($this->phone1) && is_numeric($this->phone1))
            ? $this->phone1
            : ((!empty($this->phone2) && is_numeric($this->phone2))
                ? $this->phone2
                : null);

        if(!empty($target)){
            if(strlen($target) == 9){
                return '84' . $target;
            }
            if(strlen($target) == 10 && substr($target, 0, 1) == '0'){
                return '84' . substr($target, 1);
            }
            if(strlen($target) == 11 && substr($target, 0, 2) == '84'){
                return $target;
            }
        }
        return '';
    }

    public function getRelateAssignee()
    {
        $assignee = ManualLeadAssigner::withTrashed()->where('lead_id', $this->id)
            ->orderBy('id', 'DESC')
            ->offset(1)
            ->first();
        if($assignee){
            $user = User::where('id', $assignee->user_id)->first();
            if($user){
                return $user->username;
            }
        }
        return '';
    }

    public function getAssignee()
    {

        $department_id = $this->department_id;

        $gis_department_list_id = array(
            2,  //GIS-HN-CG-DV
            8,  //GIS-HP-DK-AD
            16, //GIS-HN-TL-THT
        );
        $smis_department_list_id = array(
            1,  //SMIS-HN-CG-DV
            3,  //SMIS-HN-BĐ-TK
            4,  //SMIS-HN-HBT-LY
            5,  //SMIS-HN-HĐ-CA
            6,  //SMIS-HP-NQ-LHP
            7,  //SMIS-HP-NQ-VC
            11, //SMIS-QN-HL-NTH
            12, //SMIS-HCM-Q2-AK
            13, //IMS-TB-TL-HTC
            15, //SMIS-HP-DK-AD
            17, //SMIS-HN-TX-NT
            18, //SMIS-HN-TL-THT
            19, //IMS-HN-NTL-TH
        );
        $asc_department_list_id = array(
            20,  //ASC AquaTots Cầu Giấy
            21,  //ASC AquaTots Dương Kinh
            22,  //ASC Jacpa
            25,  //ASC AquaTots Tây Hồ
        );
        $user = null;
        if(in_array($department_id, $gis_department_list_id)){

            $user = User::whereHas('roles', function ($q) {
                $q->whereIn('name', ['telesales', 'teleteamleader']);
            })
                ->where('active', 1)->where('department_id', $department_id)
                ->whereNotIn('id', [
                    44,     // thao.pham
                    129,    // thao.phambich
                    126     // van.dinhmong
                ])
                ->orderBy('assign_count', 'ASC')
                ->first();

            if($user){
                if(!$user->assign_count){
                    $user->assign_count = 1;
                }else{
                    if($department_id == 2 && $user->hasRole('teleteamleader') && $user->assign_count % 10 == 6){
                        $user->assign_count += 4;
                    }else{
                        $user->assign_count = $user->assign_count + 1;
                    }
                }
                $user->save();
            }
        }else if(in_array($department_id, $smis_department_list_id)){

            $user = User::whereHas('roles', function ($q) {
                $q->where('name', 'telesales');
            })
                ->where('id', '!=', 84) // ignore myhao.lai
                ->where('active', 1)->where('department_id', $department_id)
                ->orderBy('assign_count', 'ASC')
                ->first();

            if($user){
                if(!$user->assign_count){
                    $user->assign_count = 1;
                }else{
                    $user->assign_count = $user->assign_count + 1;
                }
                $user->save();
            }
        }else if(in_array($department_id, $asc_department_list_id)){
            if($department_id == 20){
                $user = User::whereIn('id', array(180,233))
                    ->where('active', 1)
                    ->orderBy('assign_count', 'ASC')
                    ->first();
            }
            if($department_id == 21){
                $user = User::whereIn('id', array(161,160))
                    ->where('active', 1)
                    ->orderBy('assign_count', 'ASC')
                    ->first();
            }
            if($department_id == 22){
                $user = User::whereIn('id', array(193))
                    ->where('active', 1)
                    ->orderBy('assign_count', 'ASC')
                    ->first();
            }
            if($department_id == 25){
                $user = User::whereIn('id', array(232, 236, 239))
                    ->where('active', 1)
                    ->orderBy('assign_count', 'ASC')
                    ->first();
            }

            if($user){
                if(!$user->assign_count){
                    $user->assign_count = 1;
                }else{
                    $user->assign_count = $user->assign_count + 1;
                }
                $user->save();
            }
        }
        if(!$user){
            $user = User::where('id', 4)->first(); // crm
        }
        return $user;
    }

    public function generateOptionStatus($data, $selected_value, $empty_text)
    {
        $html = "";
        if(!empty($empty_text)){
            $html.= "<option value=''>{$empty_text}</option>";
        }
        if(empty($selected_value)){
            $selected_value = $this->status;
        }
        foreach ($data as $key => $value){
            $selected = (!empty($selected_value) && $key == $selected_value) ? "selected" : "";
            $color = '';
            switch ($key) {
                case LeadStatus::STATUS_NEW:              $color = '#28a745'; break;
                case LeadStatus::STATUS_POTENTIAL:        $color = '#007bff'; break;
                case LeadStatus::STATUS_WAITLIST:         $color = '#ffc107'; break;
                case LeadStatus::STATUS_ENROLL:           $color = '#dc3545'; break;
                case LeadStatus::STATUS_REFUSE:           $color = '#6c757d'; break;
                case LeadStatus::STATUS_NEW_SALE:         $color = '#6f42c1'; break;
                CASE LeadStatus::STATUS_HOT_LEAD:         $color = '#d92934'; break;
            }
            $html.= "<option ".(!empty($color) ? "style='color: $color' " : "")." value='{$key}' {$selected}>{$value}</option>";
        }
        return $html;
    }

    public function getAvailableStatusListData()
    {
        $data = array();
        if (!in_array($this->status, array(
            LeadStatus::STATUS_WAITLIST,
            LeadStatus::STATUS_NEW_SALE,
            LeadStatus::STATUS_ENROLL,
        ))){
            $data = LeadStatus::where('hide', 0)->where('is_auto','!=',1)->orderBy('sort_index', 'ASC')->get()->pluck('name', 'id');
        }else if($this->status == LeadStatus::STATUS_ENROLL){
            $data = LeadStatus::where('id', LeadStatus::STATUS_ENROLL)->get()->pluck('name', 'id');
        }else if($this->status == LeadStatus::STATUS_WAITLIST){
            $data = LeadStatus::where('id', $this->status)->orWhereIn('id', [LeadStatus::STATUS_CANCEL_DEPOSIT, LeadStatus::STATUS_ENTRANCE_EXAM_FAIL])->orderBy('sort_index', 'ASC')->get()->pluck('name', 'id');
        }else{
            $data = LeadStatus::where('id', $this->status)->orWhere('id', LeadStatus::STATUS_CANCEL_DEPOSIT)->orderBy('sort_index', 'ASC')->get()->pluck('name', 'id');
        }
        return $data;
    }


    public function search()
    {
        //echo 'Searching...';exit;
        $search_mode = Input::get('search_mode');
        if ($search_mode == 'advanced_search') {
            $settings = UserPref::firstOrNew(['key' => 'filtering', 'user_id' => Auth::id()]);
            $settings->value = json_encode(Input::all());
            if (empty($settings->id))
                $settings->created_at = date('Y-m-d H:i:s');
            else
                $settings->updated_at = date('Y-m-d H:i:s');
            $settings->save();
        }

        $query = DB::table('fptu_lead AS t');
        $query->select('t.*',
            DB::raw('(SELECT name FROM fptu_province WHERE id = t.province_id) AS province'),
            DB::raw('(SELECT name FROM fptu_department WHERE id = t.department_id) AS department'),
            DB::raw('(SELECT name FROM fptu_major WHERE id = t.major) as major_name'),
            DB::raw('(SELECT GROUP_CONCAT(DISTINCT username) FROM fptu_user WHERE id IN (SELECT user_id FROM fptu_lead_assignment WHERE lead_id = t.id AND deleted_at IS NULL)) as assignee_list'),
            DB::raw('(SELECT username FROM fptu_user WHERE id = t.created_by) as creator'),
            DB::raw('CONCAT_WS(",", t.phone1, t.phone2) as phone'),
            DB::raw('(SELECT name FROM fptu_lead_status WHERE id = t.status) as lead_status_name'),
            DB::raw('(SELECT name FROM fptu_call_status WHERE id = t.call_status) as call_status_name'),
            DB::raw('(SELECT username FROM fptu_user WHERE id = t.activator) as activator_username'),
            DB::raw('(SELECT name FROM fptu_source WHERE id = (SELECT source_id FROM fptu_lead_source WHERE id = (SELECT MIN(id) FROM fptu_lead_source WHERE lead_id = t.id AND deleted_at IS NULL))) as source_list'),
            DB::raw('(SELECT GROUP_CONCAT(DISTINCT name) FROM fptu_channel WHERE id IN (SELECT channel_id FROM fptu_lead_channel WHERE lead_id = t.id AND deleted_at IS NULL)) as channel_list'),
            DB::raw('(SELECT GROUP_CONCAT(DISTINCT name) FROM fptu_campaign WHERE id IN (SELECT campaign_id FROM fptu_lead_campaign WHERE lead_id = t.id AND deleted_at IS NULL)) as campaign_list'),
            DB::raw('(SELECT GROUP_CONCAT(DISTINCT name) FROM fptu_keyword WHERE id IN (SELECT keyword_id FROM fptu_lead_keyword WHERE lead_id = t.id AND deleted_at IS NULL)) as keyword_list'),
            DB::raw('(SELECT name FROM fptu_info_source WHERE id = t.info_source) as info_source_name'),
            DB::raw('(SELECT count(*) FROM edf_call_log WHERE lead_id = t.id) as total_log_call'),
            DB::raw('(SELECT notes FROM edf_call_log WHERE lead_id = t.id ORDER BY id DESC LIMIT 1) as last_log_call')
        );
        $query->whereNull('t.deleted_at');

        if (Auth::user()->hasRole('carer') && !Auth::user()->hasRole('admin') && !Auth::user()->hasRole('holding')) {
            $query->where('t.status', LeadStatus::STATUS_ENROLL);
        }
        if((Auth::user()->hasRole('admin') && Auth::user()->department_id == Department::HOLDING) || Auth::user()->hasRole('holding')){

        }else{
            $query->where(function ($sub_query) {
                $sub_query->whereIn('t.department_id', Auth::user()->getManageDepartments())
                    ->orWhereRaw('t.id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = ? AND deleted_at IS NULL)', [Auth::id()]);
            });
        }

        if($search_mode == 'global_search'){
            if(!empty($_REQUEST['gsearch'])){
                $gsearch = $_REQUEST['gsearch'];
                $query->where(function ($sub_query) use ($gsearch){
                    $sub_query->where('t.name', 'LIKE', "%$gsearch%")
                        ->orWhere('t.phone1', 'LIKE', "%$gsearch%")
                        ->orWhere('t.phone2', 'LIKE', "%$gsearch%")
                        ->orWhere('t.student_name', 'LIKE', "%$gsearch%")
                        ->orWhere('t.email', 'LIKE', "%$gsearch%")
                        ->orWhere('t.facebook', 'LIKE', "%$gsearch%")
                    ;
                });
            }
        }

        if($search_mode == 'advanced_search'){
            if(!empty($_REQUEST['name'])){
                $query->where('t.name', 'LIKE', '%'.$_REQUEST['name'].'%');
            }
            if(!empty($_REQUEST['student_name'])){
                $query->where('t.student_name', 'LIKE', '%'.$_REQUEST['student_name'].'%');
            }
            if(!empty($_REQUEST['phone'])){
                $phone = $_REQUEST['phone'];
                $query->where(function ($sub_query) use ($phone){
                    $sub_query->where('t.phone1', 'LIKE', '%'.$phone.'%')
                        ->orWhere('t.phone2', 'LIKE', '%'.$phone.'%')
                    ;
                });
            }
            if(!empty($_REQUEST['email'])){
                $query->where('t.email', 'LIKE', '%'.$_REQUEST['email'].'%');
            }
            if(!empty($_REQUEST['birthdate'])){
                $birthdate = date("Y-m-d", strtotime(str_replace('/','-',$_REQUEST['birthdate'])));
                $query->whereDate('t.birthdate', $birthdate);
            }
            if(!empty($_REQUEST['birthyear'])){
                $query->whereYear('t.birthdate', $_REQUEST['birthyear']);
            }
            if(!empty($_REQUEST['status_id'])){
                if(!in_array('', $_REQUEST['status_id'])){
                    $query->whereIn('t.status', $_REQUEST['status_id']);
                }
            }
            if(!empty($_REQUEST['call_status_id'])){
                $query->where('t.call_status', $_REQUEST['call_status_id']);
            }
            if(!empty($_REQUEST['source'])){
                $sources = $_REQUEST['source'];
                if(in_array('na', $sources)){
                    $query->whereRaw('t.id NOT IN (SELECT lead_id FROM fptu_lead_source)');
                }else{
                    $list_source_id = implode(',',$sources);
                    $query->whereRaw("t.id IN (SELECT fls.lead_id FROM fptu_lead_source fls WHERE fls.source_id IN($list_source_id) AND fls.id = (SELECT MIN(fls2.id) FROM fptu_lead_source fls2 WHERE fls2.lead_id = fls.lead_id AND fls2.deleted_at IS NULL))");
                }
            }else{
                if(!empty($_REQUEST['source_online'])){
                    if($_REQUEST['source_online'] == 'online') {
                        $query->whereRaw('t.id IN (SELECT lead_id FROM fptu_lead_source WHERE source_id IN (SELECT id FROM fptu_source WHERE online = 1))');
                    }else{
                        $query->whereRaw('t.id IN (SELECT lead_id FROM fptu_lead_source WHERE source_id IN (SELECT id FROM fptu_source WHERE online = 0))');
                    }
                }
                if(!empty($_REQUEST['source_type'])){
                    $types_str = "'" .implode("','", $_REQUEST['source_type']  ) . "'";
                    $query->whereRaw("t.id IN (SELECT lead_id FROM fptu_lead_source WHERE source_id IN (SELECT id FROM fptu_source WHERE type IN ($types_str)))");
                }
            }
            if(!empty($_REQUEST['school'])){
                $school = $_REQUEST['school'];
                $query->where("t.school IN (SELECT id FROM fptu_school WHERE name LIKE '%$school%')");
            }
            if(!empty($_REQUEST['major'])){
                $query->whereIn('t.major', $_REQUEST['major']);
            }
            if(!empty($_REQUEST['province'])){
                $query->whereIn('t.province_id', $_REQUEST['province']);
            }
            if(!empty($_REQUEST['department_id'])){
                $query->whereIn('t.department_id', $_REQUEST['department_id']);
            }
            if (!empty($_REQUEST['start_date'])) {
                $start_date = date('Y-m-d', strtotime(str_replace('/','-', $_REQUEST['start_date']))) . ' 00:00:00';
                $query->where('t.created_at', '>=', $start_date);
            }
            if (!empty($_REQUEST['end_date'])) {
                $end_date = date('Y-m-d', strtotime(str_replace('/','-', $_REQUEST['end_date']))) . ' 23:59:59';
                $query->where('t.created_at', '<=', $end_date);
            }
            if (!empty($_REQUEST['active_from'])) {
                $active_from = date('Y-m-d', strtotime(str_replace('/','-', $_REQUEST['active_from']))) . ' 00:00:00';
                $query->where('t.activated_at', '>=', $active_from);
            }
            if (!empty($_REQUEST['active_to'])) {
                $active_to = date('Y-m-d', strtotime(str_replace('/','-', $_REQUEST['active_to']))) . ' 23:59:59';
                $query->where('t.activated_at', '<=', $active_to);
            }
            if (!empty($_REQUEST['enrolled_from'])) {
                $enrolled_from = date('Y-m-d', strtotime(str_replace('/','-', $_REQUEST['enrolled_from']))) . ' 00:00:00';
                $query->where('t.enrolled_at', '>=', $enrolled_from);
            }
            if (!empty($_REQUEST['enrolled_to'])) {
                $enrolled_to = date('Y-m-d', strtotime(str_replace('/','-', $_REQUEST['enrolled_to']))) . ' 23:59:59';
                $query->where('t.enrolled_at', '<=', $enrolled_to);
            }
            if (!empty($_REQUEST['modified_from'])) {
                $modified_from = date('Y-m-d', strtotime(str_replace('/','-', $_REQUEST['modified_from']))) . ' 00:00:00';
                $query->where('t.updated_at', '>=', $modified_from);
            }
            if (!empty($_REQUEST['modified_to'])) {
                $modified_to = date('Y-m-d', strtotime(str_replace('/','-', $_REQUEST['modified_to']))) . ' 23:59:59';
                $query->where('t.updated_at', '<=', $modified_to);
            }
            if (!empty($_REQUEST['assigned_from'])) {
                $assigned_from = date('Y-m-d', strtotime(str_replace('/','-', $_REQUEST['assigned_from']))) . ' 00:00:00';
                $query->whereRaw("t.id IN (SELECT lead_id FROM fptu_lead_assignment WHERE deleted_at IS NULL AND created_at >= '$assigned_from')");

            }
            if (!empty($_REQUEST['assigned_to'])) {
                $assigned_to = date('Y-m-d', strtotime(str_replace('/','-', $_REQUEST['assigned_to']))) . ' 23:59:59';
                $query->whereRaw("t.id IN (SELECT lead_id FROM fptu_lead_assignment WHERE deleted_at IS NULL AND created_at <= '$assigned_to')");
            }
            if (!empty($_REQUEST['showup_school_from'])) {
                $showup_school_from = date('Y-m-d', strtotime(str_replace('/','-', $_REQUEST['showup_school_from']))) . ' 00:00:00';
                $query->where('t.showup_school_at', '>=', $showup_school_from);
            }
            if (!empty($_REQUEST['showup_school_to'])) {
                $showup_school_to = date('Y-m-d', strtotime(str_replace('/','-', $_REQUEST['showup_school_to']))) . ' 23:59:59';
                $query->where('t.showup_school_at', '<=', $showup_school_to);
            }
            if (!empty($_REQUEST['showup_seminor_from'])) {
                $showup_seminor_from = date('Y-m-d', strtotime(str_replace('/','-', $_REQUEST['showup_seminor_from']))) . ' 00:00:00';
                $query->where('t.showup_seminor_at', '>=', $showup_seminor_from);
            }
            if (!empty($_REQUEST['showup_seminor_to'])) {
                $showup_seminor_to = date('Y-m-d', strtotime(str_replace('/','-', $_REQUEST['showup_seminor_to']))) . ' 23:59:59';
                $query->where('t.showup_seminor_at', '<=', $showup_seminor_to);
            }
            if (!empty($_REQUEST['join_as_guest_from'])) {
                $join_as_guest_from = date('Y-m-d', strtotime(str_replace('/','-', $_REQUEST['join_as_guest_from']))) . ' 00:00:00';
                $query->where('t.join_as_guest_at', '>=', $join_as_guest_from);
            }
            if (!empty($_REQUEST['join_as_guest_to'])) {
                $join_as_guest_to = date('Y-m-d', strtotime(str_replace('/','-', $_REQUEST['join_as_guest_to']))) . ' 23:59:59';
                $query->where('t.join_as_guest_at', '<=', $join_as_guest_to);
            }
            if (!empty($_REQUEST['hot_lead_from'])) {
                $hot_lead_from = date('Y-m-d', strtotime(str_replace('/','-', $_REQUEST['hot_lead_from']))) . ' 00:00:00';
                $query->where('t.hot_lead_at', '>=', $hot_lead_from);
            }
            if (!empty($_REQUEST['hot_lead_to'])) {
                $hot_lead_to = date('Y-m-d', strtotime(str_replace('/','-', $_REQUEST['hot_lead_to']))) . ' 23:59:59';
                $query->where('t.hot_lead_at', '<=', $hot_lead_to);
            }
            if (!empty($_REQUEST['refuse_from'])) {
                $refuse_from = date('Y-m-d', strtotime(str_replace('/','-', $_REQUEST['refuse_from']))) . ' 00:00:00';
                $query->whereRaw("t.id IN (SELECT lead_id FROM fptu_lead_log_status WHERE status_new = 6 AND created_at >= '$refuse_from')");
            }
            if (!empty($_REQUEST['refuse_to'])) {
                $refuse_to = date('Y-m-d', strtotime(str_replace('/','-', $_REQUEST['refuse_to']))) . ' 23:59:59';
                $query->whereRaw("t.id IN (SELECT lead_id FROM fptu_lead_log_status WHERE status_new = 6 AND created_at <= '$refuse_to')");
            }
            if(!empty($_REQUEST['assignee'])){
                $assignees = $_REQUEST['assignee'];
                if(in_array('nobody', $assignees)){
                    $query->whereRaw('t.id NOT IN (SELECT lead_id FROM fptu_lead_assignment WHERE deleted_at IS NULL)');
                }else{
                    $list_assignee_id = implode(',',$assignees);
                    $query->whereRaw("t.id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id IN($list_assignee_id) AND deleted_at IS NULL)");
                }
            }
            if(!empty($_REQUEST['channel'])){
                $channels = $_REQUEST['channel'];
                $list_channel_id = implode(',',$channels);
                $query->whereRaw("t.id IN (SELECT lead_id FROM fptu_lead_channel WHERE channel_id IN($list_channel_id) AND deleted_at IS NULL)");

            }
            if(!empty($_REQUEST['campaign_category'])){
                $campaign_category = $_REQUEST['campaign_category'];
                $list_campaign_category_id = implode(',',$campaign_category);
                $query->whereRaw("t.id IN (SELECT lead_id FROM fptu_lead_campaign WHERE campaign_id IN (SELECT campaign_id FROM fptu_campaign_category_mapping WHERE category_id IN ($list_campaign_category_id) AND deleted_at IS NULL))");
            }
            if(!empty($_REQUEST['campaign'])){
                $campaigns = $_REQUEST['campaign'];
                $list_campaign_id = implode(',',$campaigns);
                $query->whereRaw("t.id IN (SELECT lead_id FROM fptu_lead_campaign WHERE campaign_id IN($list_campaign_id) AND deleted_at IS NULL)");

            }
            if(!empty($_REQUEST['keyword'])){
                $keywords = $_REQUEST['keyword'];
                $list_keyword_id = implode(',',$keywords);
                $query->whereRaw("t.id IN (SELECT lead_id FROM fptu_lead_keyword WHERE keyword_id IN($list_keyword_id) AND deleted_at IS NULL)");

            }
            if(!empty($_REQUEST['activator'])){
                $activators = $_REQUEST['activator'];
                $query->whereIn('activator', $activators);
            }
            if(!empty($_REQUEST['creator'])){
                $creators = $_REQUEST['creator'];
                $query->whereIn('created_by', $creators);
            }
            if(!empty($_REQUEST['rating'])){
                $ratings = $_REQUEST['rating'];
                $query->whereIn('rating', $ratings);
            }
            if(!empty($_REQUEST['notes'])){
                $query->where('notes', 'LIKE', '%'.$_REQUEST['notes'].'%');
            }

        }
        $countTotal = $query->count();

        $order_by = Input::get('order');
        $order_by_col_num = $order_by[0]['column'];
        $order_by_direction = $order_by[0]['dir'];
        if (!empty($order_by_col_num)) {
            if(Auth::user()->isSMISUser()){
                switch ($order_by_col_num){
                    case 1: $order_by_col_name = 't.name'; break;
                    case 2: $order_by_col_name = 'CONCAT(t.phone1, t.phone2)'; break;
                    case 3: $order_by_col_name = 't.student_name'; break;
                    case 7: $order_by_col_name = '(SELECT GROUP_CONCAT(DISTINCT username) FROM fptu_user WHERE id IN (SELECT user_id FROM fptu_lead_assignment WHERE lead_id = t.id AND deleted_at IS NULL))'; break;
                    case 8: $order_by_col_name = 't.created_at'; break;
                }
            }elseif(Auth::user()->isGISUser()){
                switch ($order_by_col_num){
                    case 1: $order_by_col_name = 't.name'; break;
                    case 2: $order_by_col_name = 'CONCAT(t.phone1, t.phone2)'; break;
                    case 3: $order_by_col_name = 't.student_name'; break;
                    case 4: $order_by_col_name = 'DATE_FORMAT(t.student_name, "%Y")'; break;
                    case 5: $order_by_col_name = 't.created_at'; break;
                    case 9: $order_by_col_name = '(SELECT GROUP_CONCAT(DISTINCT username) FROM fptu_user WHERE id IN (SELECT user_id FROM fptu_lead_assignment WHERE lead_id = t.id AND deleted_at IS NULL))'; break;
                }
            }elseif (Auth::user()->isASCUser()){
                switch ($order_by_col_num){
                    case 1: $order_by_col_name = 't.student_name'; break;
                    case 2: $order_by_col_name = 't.name'; break;
                    case 3: $order_by_col_name = 'CONCAT(t.phone1, t.phone2)'; break;
                }
            }else{
                switch ($order_by_col_num){
                    case 1: $order_by_col_name = 't.name'; break;
                    case 2: $order_by_col_name = 'CONCAT(t.phone1, t.phone2)'; break;
                    case 3: $order_by_col_name = 't.email'; break;
                    case 4: $order_by_col_name = 't.address'; break;
                    case 5: $order_by_col_name = 't.student_name'; break;
                    case 6: $order_by_col_name = 't.birthdate'; break;
                    case 7: $order_by_col_name = '(SELECT name FROM fptu_school WHERE id = t.school)'; break;
                    case 13: $order_by_col_name = '(SELECT GROUP_CONCAT(DISTINCT username) FROM fptu_user WHERE id IN (SELECT user_id FROM fptu_lead_assignment WHERE lead_id = t.id AND deleted_at IS NULL))'; break;
                    case 15: $order_by_col_name = '(SELECT name FROM fptu_info_source WHERE id = t.info_source)'; break;
                    case 19: $order_by_col_name = 't.department_id'; break;
                    case 20: $order_by_col_name = '(SELECT name FROM fptu_user WHERE id = t.created_by)'; break;
                    case 21: $order_by_col_name = 't.created_at'; break;
                }
            }
            if(!Auth::user()->isASCUser() && Auth::user()->hasRole('holding')){
                switch ($order_by_col_num){
                    case 1: $order_by_col_name = 't.name'; break;
                    case 2: $order_by_col_name = 'CONCAT(t.phone1, t.phone2)'; break;
                    case 3: $order_by_col_name = 't.email'; break;
                    case 4: $order_by_col_name = 't.address'; break;
                    case 5: $order_by_col_name = 't.student_name'; break;
                    case 6: $order_by_col_name = 't.birthdate'; break;
                    case 7: $order_by_col_name = '(SELECT name FROM fptu_school WHERE id = t.school)'; break;
                    case 13: $order_by_col_name = '(SELECT GROUP_CONCAT(DISTINCT username) FROM fptu_user WHERE id IN (SELECT user_id FROM fptu_lead_assignment WHERE lead_id = t.id AND deleted_at IS NULL))'; break;
                    case 15: $order_by_col_name = '(SELECT name FROM fptu_info_source WHERE id = t.info_source)'; break;
                    case 19: $order_by_col_name = 't.department_id'; break;
                    case 20: $order_by_col_name = '(SELECT name FROM fptu_user WHERE id = t.created_by)'; break;
                    case 21: $order_by_col_name = 't.created_at'; break;
                }
            }
            $query->orderBy($order_by_col_name, $order_by_direction);
        }else{
            $query->orderBy('t.created_at', 'DESC');
        }

        $length = Input::get('length');
        if ($length) $query->limit($length);
        $start = Input::get('start');
        if ($start) $query->offset($start);
        $data = $query->get();

        return array(
            'data' => $data,
            'recordsTotal' => $countTotal,
            'recordsFiltered' => $countTotal,
        );
    }

    public function searchMultiSource()
    {
        //echo 'Searching...';exit;
        $search_mode = Input::get('search_mode');
        if ($search_mode == 'advanced_search') {
            $settings = UserPref::firstOrNew(['key' => 'filtering_lead_source', 'user_id' => Auth::id()]);
            $settings->value = json_encode(Input::all());
            if (empty($settings->id))
                $settings->created_at = date('Y-m-d H:i:s');
            else
                $settings->updated_at = date('Y-m-d H:i:s');
            $settings->save();
        }

        $query = DB::table('fptu_lead AS t');
        $query->select('t.*',
            DB::raw('(SELECT name FROM fptu_province WHERE id = t.province_id) AS province'),
            DB::raw('(SELECT name FROM fptu_department WHERE id = t.department_id) AS department'),
            DB::raw('(SELECT name FROM fptu_major WHERE id = t.major) as major_name'),
            DB::raw('(SELECT GROUP_CONCAT(DISTINCT username) FROM fptu_user WHERE id IN (SELECT user_id FROM fptu_lead_assignment WHERE lead_id = t.id AND deleted_at IS NULL)) as assignee_list'),
            DB::raw('(SELECT username FROM fptu_user WHERE id = t.created_by) as creator'),
            DB::raw('CONCAT_WS(",", t.phone1, t.phone2) as phone'),
            DB::raw('(SELECT name FROM fptu_lead_status WHERE id = t.status) as lead_status_name'),
            DB::raw('(SELECT name FROM fptu_call_status WHERE id = t.call_status) as call_status_name'),
            DB::raw('(SELECT username FROM fptu_user WHERE id = t.activator) as activator_username'),
            DB::raw('(SELECT GROUP_CONCAT(DISTINCT name) FROM fptu_source WHERE id IN (SELECT source_id FROM fptu_lead_source WHERE lead_id = t.id AND deleted_at IS NULL)) as source_list'),
            DB::raw('(SELECT GROUP_CONCAT(DISTINCT name) FROM fptu_channel WHERE id IN (SELECT channel_id FROM fptu_lead_channel WHERE lead_id = t.id AND deleted_at IS NULL)) as channel_list'),
            DB::raw('(SELECT GROUP_CONCAT(DISTINCT name) FROM fptu_campaign WHERE id IN (SELECT campaign_id FROM fptu_lead_campaign WHERE lead_id = t.id AND deleted_at IS NULL)) as campaign_list'),
            DB::raw('(SELECT GROUP_CONCAT(DISTINCT name) FROM fptu_keyword WHERE id IN (SELECT keyword_id FROM fptu_lead_keyword WHERE lead_id = t.id AND deleted_at IS NULL)) as keyword_list'),
            DB::raw('(SELECT name FROM fptu_info_source WHERE id = t.info_source) as info_source_name'),
            DB::raw('(SELECT count(*) FROM edf_call_log WHERE lead_id = t.id) as total_log_call'),
            DB::raw('(SELECT notes FROM edf_call_log WHERE lead_id = t.id ORDER BY id DESC LIMIT 1) as last_log_call')
        );
        $query->whereNull('t.deleted_at');

        if (Auth::user()->hasRole('carer') && !Auth::user()->hasRole('admin') && !Auth::user()->hasRole('holding')) {
            $query->where('t.status', LeadStatus::STATUS_ENROLL);
        }
        if ((Auth::user()->hasRole('admin') && Auth::user()->department_id == Department::HOLDING) || Auth::user()->hasRole('holding')) {

        } else {
            $query->where(function ($sub_query) {
                $sub_query->where('t.department_id', Auth::user()->department_id)
                    ->orWhereRaw('t.id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = ? AND deleted_at IS NULL)', [Auth::id()]);
            });
        }

        if ($search_mode == 'global_search') {
            if (!empty($_REQUEST['gsearch'])) {
                $gsearch = $_REQUEST['gsearch'];
                $query->where(function ($sub_query) use ($gsearch) {
                    $sub_query->where('t.name', 'LIKE', "%$gsearch%")
                        ->orWhere('t.phone1', 'LIKE', "%$gsearch%")
                        ->orWhere('t.phone2', 'LIKE', "%$gsearch%")
                        ->orWhere('t.student_name', 'LIKE', "%$gsearch%")
                        ->orWhere('t.email', 'LIKE', "%$gsearch%")
                        ->orWhere('t.facebook', 'LIKE', "%$gsearch%");
                });
            }
        }

        if ($search_mode == 'advanced_search') {
            if (!empty($_REQUEST['name'])) {
                $query->where('t.name', 'LIKE', '%'.$_REQUEST['name'].'%');
            }
            if (!empty($_REQUEST['student_name'])) {
                $query->where('t.student_name', 'LIKE', '%'.$_REQUEST['student_name'].'%');
            }
            if (!empty($_REQUEST['phone'])) {
                $phone = $_REQUEST['phone'];
                $query->where(function ($sub_query) use ($phone) {
                    $sub_query->where('t.phone1', 'LIKE', '%'.$phone.'%')
                        ->orWhere('t.phone2', 'LIKE', '%'.$phone.'%');
                });
            }
            if (!empty($_REQUEST['email'])) {
                $query->where('t.email', 'LIKE', '%'.$_REQUEST['email'].'%');
            }
            if (!empty($_REQUEST['birthdate'])) {
                $birthdate = date("Y-m-d", strtotime(str_replace('/', '-', $_REQUEST['birthdate'])));
                $query->whereDate('t.birthdate', $birthdate);
            }
            if (!empty($_REQUEST['birthyear'])) {
                $query->whereYear('t.birthdate', $_REQUEST['birthyear']);
            }
            if (!empty($_REQUEST['status_id'])) {
                if (!in_array('', $_REQUEST['status_id'])) {
                    $query->whereIn('t.status', $_REQUEST['status_id']);
                }
            }
            if (!empty($_REQUEST['call_status_id'])) {
                $query->where('t.call_status', $_REQUEST['call_status_id']);
            }
            if (!empty($_REQUEST['source'])) {
                $sources = $_REQUEST['source'];
                if (in_array('na', $sources)) {
                    $query->whereRaw('t.id NOT IN (SELECT lead_id FROM fptu_lead_source)');
                } else {
                    $list_source_id = implode(',', $sources);
                    $query->whereRaw("t.id IN (SELECT lead_id FROM fptu_lead_source WHERE source_id IN($list_source_id))");
                }
            }
            if (!empty($_REQUEST['school'])) {
                $school = $_REQUEST['school'];
                $query->where("t.school IN (SELECT id FROM fptu_school WHERE name LIKE '%$school%')");
            }
            if (!empty($_REQUEST['major'])) {
                $query->whereIn('t.major', $_REQUEST['major']);
            }
            if (!empty($_REQUEST['province'])) {
                $query->whereIn('t.province_id', $_REQUEST['province']);
            }
            if (!empty($_REQUEST['department_id'])) {
                $query->whereIn('t.department_id', $_REQUEST['department_id']);
            }
            if (!empty($_REQUEST['start_date'])) {
                $start_date = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['start_date']))) . ' 00:00:00';
                $query->where('t.created_at', '>=', $start_date);
            }
            if (!empty($_REQUEST['end_date'])) {
                $end_date = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['end_date']))) . ' 23:59:59';
                $query->where('t.created_at', '<=', $end_date);
            }
            if (!empty($_REQUEST['active_from'])) {
                $active_from = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['active_from']))) . ' 00:00:00';
                $query->where('t.activated_at', '>=', $active_from);
            }
            if (!empty($_REQUEST['active_to'])) {
                $active_to = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['active_to']))) . ' 23:59:59';
                $query->where('t.activated_at', '<=', $active_to);
            }
            if (!empty($_REQUEST['enrolled_from'])) {
                $enrolled_from = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['enrolled_from']))) . ' 00:00:00';
                $query->where('t.enrolled_at', '>=', $enrolled_from);
            }
            if (!empty($_REQUEST['enrolled_to'])) {
                $enrolled_to = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['enrolled_to']))) . ' 23:59:59';
                $query->where('t.enrolled_at', '<=', $enrolled_to);
            }
            if (!empty($_REQUEST['modified_from'])) {
                $modified_from = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['modified_from']))) . ' 00:00:00';
                $query->where('t.updated_at', '>=', $modified_from);
            }
            if (!empty($_REQUEST['modified_to'])) {
                $modified_to = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['modified_to']))) . ' 23:59:59';
                $query->where('t.updated_at', '<=', $modified_to);
            }
            if (!empty($_REQUEST['assigned_from'])) {
                $assigned_from = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['assigned_from']))) . ' 00:00:00';
                $query->whereRaw("t.id IN (SELECT lead_id FROM fptu_lead_assignment WHERE deleted_at IS NULL AND created_at >= '$assigned_from')");

            }
            if (!empty($_REQUEST['assigned_to'])) {
                $assigned_to = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['assigned_to']))) . ' 23:59:59';
                $query->whereRaw("t.id IN (SELECT lead_id FROM fptu_lead_assignment WHERE deleted_at IS NULL AND created_at <= '$assigned_to')");
            }
            if (!empty($_REQUEST['showup_school_from'])) {
                $showup_school_from = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['showup_school_from']))) . ' 00:00:00';
                $query->where('t.showup_school_at', '>=', $showup_school_from);
            }
            if (!empty($_REQUEST['showup_school_to'])) {
                $showup_school_to = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['showup_school_to']))) . ' 23:59:59';
                $query->where('t.showup_school_at', '<=', $showup_school_to);
            }
            if (!empty($_REQUEST['showup_seminor_from'])) {
                $showup_seminor_from = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['showup_seminor_from']))) . ' 00:00:00';
                $query->where('t.showup_seminor_at', '>=', $showup_seminor_from);
            }
            if (!empty($_REQUEST['showup_seminor_to'])) {
                $showup_seminor_to = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['showup_seminor_to']))) . ' 23:59:59';
                $query->where('t.showup_seminor_at', '<=', $showup_seminor_to);
            }
            if (!empty($_REQUEST['join_as_guest_from'])) {
                $join_as_guest_from = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['join_as_guest_from']))) . ' 00:00:00';
                $query->where('t.join_as_guest_at', '>=', $join_as_guest_from);
            }
            if (!empty($_REQUEST['join_as_guest_to'])) {
                $join_as_guest_to = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['join_as_guest_to']))) . ' 23:59:59';
                $query->where('t.join_as_guest_at', '<=', $join_as_guest_to);
            }
            if (!empty($_REQUEST['hot_lead_from'])) {
                $hot_lead_from = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['hot_lead_from']))) . ' 00:00:00';
                $query->where('t.hot_lead_at', '>=', $hot_lead_from);
            }
            if (!empty($_REQUEST['hot_lead_to'])) {
                $hot_lead_to = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['hot_lead_to']))) . ' 23:59:59';
                $query->where('t.hot_lead_at', '<=', $hot_lead_to);
            }
            if (!empty($_REQUEST['assignee'])) {
                $assignees = $_REQUEST['assignee'];
                if (in_array('nobody', $assignees)) {
                    $query->whereRaw('t.id NOT IN (SELECT lead_id FROM fptu_lead_assignment WHERE deleted_at IS NULL)');
                } else {
                    $list_assignee_id = implode(',', $assignees);
                    $query->whereRaw("t.id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id IN($list_assignee_id) AND deleted_at IS NULL)");
                }
            }
            if (!empty($_REQUEST['channel'])) {
                $channels = $_REQUEST['channel'];
                $list_channel_id = implode(',', $channels);
                $query->whereRaw("t.id IN (SELECT lead_id FROM fptu_lead_channel WHERE channel_id IN($list_channel_id) AND deleted_at IS NULL)");

            }
            if(!empty($_REQUEST['campaign_category'])){
                $campaign_category = $_REQUEST['campaign_category'];
                $list_campaign_category_id = implode(',',$campaign_category);
                $query->whereRaw("t.id IN (SELECT lead_id FROM fptu_lead_campaign WHERE campaign_id IN (SELECT campaign_id FROM fptu_campaign_category_mapping WHERE category_id IN ($list_campaign_category_id) AND deleted_at IS NULL))");
            }
            if (!empty($_REQUEST['campaign'])) {
                $campaigns = $_REQUEST['campaign'];
                $list_campaign_id = implode(',', $campaigns);
                $query->whereRaw("t.id IN (SELECT lead_id FROM fptu_lead_campaign WHERE campaign_id IN($list_campaign_id) AND deleted_at IS NULL)");

            }
            if (!empty($_REQUEST['keyword'])) {
                $keywords = $_REQUEST['keyword'];
                $list_keyword_id = implode(',', $keywords);
                $query->whereRaw("t.id IN (SELECT lead_id FROM fptu_lead_keyword WHERE keyword_id IN($list_keyword_id) AND deleted_at IS NULL)");

            }
            if (!empty($_REQUEST['activator'])) {
                $activators = $_REQUEST['activator'];
                $query->whereIn('activator', $activators);
            }
            if (!empty($_REQUEST['creator'])) {
                $creators = $_REQUEST['creator'];
                $query->whereIn('created_by', $creators);
            }
            if (!empty($_REQUEST['rating'])) {
                $ratings = $_REQUEST['rating'];
                $query->whereIn('rating', $ratings);
            }
            if (!empty($_REQUEST['notes'])) {
                $query->where('notes', 'LIKE', '%'.$_REQUEST['notes'].'%');
            }

        }
        $countTotal = $query->count();

        $order_by = Input::get('order');
        $order_by_col_num = $order_by[0]['column'];
        $order_by_direction = $order_by[0]['dir'];
        if (!empty($order_by_col_num)) {
            if (Auth::user()->isSMISUser()) {
                switch ($order_by_col_num) {
                    case 1:
                        $order_by_col_name = 't.name';
                        break;
                    case 2:
                        $order_by_col_name = 'CONCAT(t.phone1, t.phone2)';
                        break;
                    case 3:
                        $order_by_col_name = 't.student_name';
                        break;
                    case 7:
                        $order_by_col_name = '(SELECT GROUP_CONCAT(DISTINCT username) FROM fptu_user WHERE id IN (SELECT user_id FROM fptu_lead_assignment WHERE lead_id = t.id AND deleted_at IS NULL))';
                        break;
                    case 8:
                        $order_by_col_name = 't.created_at';
                        break;
                }
            } elseif (Auth::user()->isGISUser()) {
                switch ($order_by_col_num) {
                    case 1:
                        $order_by_col_name = 't.name';
                        break;
                    case 2:
                        $order_by_col_name = 'CONCAT(t.phone1, t.phone2)';
                        break;
                    case 3:
                        $order_by_col_name = 't.student_name';
                        break;
                    case 4:
                        $order_by_col_name = 'DATE_FORMAT(t.student_name, "%Y")';
                        break;
                    case 5:
                        $order_by_col_name = 't.created_at';
                        break;
                    case 9:
                        $order_by_col_name = '(SELECT GROUP_CONCAT(DISTINCT username) FROM fptu_user WHERE id IN (SELECT user_id FROM fptu_lead_assignment WHERE lead_id = t.id AND deleted_at IS NULL))';
                        break;
                }
            } else {
                switch ($order_by_col_num) {
                    case 1:
                        $order_by_col_name = 't.name';
                        break;
                    case 2:
                        $order_by_col_name = 'CONCAT(t.phone1, t.phone2)';
                        break;
                    case 3:
                        $order_by_col_name = 't.email';
                        break;
                    case 4:
                        $order_by_col_name = 't.address';
                        break;
                    case 5:
                        $order_by_col_name = 't.student_name';
                        break;
                    case 6:
                        $order_by_col_name = 't.birthdate';
                        break;
                    case 7:
                        $order_by_col_name = '(SELECT name FROM fptu_school WHERE id = t.school)';
                        break;
                    case 13:
                        $order_by_col_name = '(SELECT GROUP_CONCAT(DISTINCT username) FROM fptu_user WHERE id IN (SELECT user_id FROM fptu_lead_assignment WHERE lead_id = t.id AND deleted_at IS NULL))';
                        break;
                    case 15:
                        $order_by_col_name = '(SELECT name FROM fptu_info_source WHERE id = t.info_source)';
                        break;
                    case 19:
                        $order_by_col_name = 't.department_id';
                        break;
                    case 20:
                        $order_by_col_name = '(SELECT name FROM fptu_user WHERE id = t.created_by)';
                        break;
                    case 21:
                        $order_by_col_name = 't.created_at';
                        break;
                }
            }
            $query->orderBy($order_by_col_name, $order_by_direction);
        } else {
            $query->orderBy('t.created_at', 'DESC');
        }

        $length = Input::get('length');
        if ($length) $query->limit($length);
        $start = Input::get('start');
        if ($start) $query->offset($start);
        $data = $query->get();

        return array(
            'data' => $data,
            'recordsTotal' => $countTotal,
            'recordsFiltered' => $countTotal,
        );
    }


    public function checkSpam()
    {
        $spam_keywords = array(
            'http',
            'https',
            'website',
            'websites',
        );
        $note = strtolower($this->notes);
        $flag = false;
        foreach ($spam_keywords as $keyword){
            if (strpos($note, $keyword) !== false) {
                $flag = true;
                break;
            }
        }
        if($flag){
            $this->department_id = Department::HOLDING;
        }
        return $flag;
    }

    public function getEnrollDesireDateDiff()
    {
        $diff = null;
        if(!empty($this->enroll_desire_at) && !empty($this->enroll_desire_register_at)){
            $enroll_desire_at = date('Y-m-d', strtotime(str_replace('/', '-', $this->enroll_desire_at)));
            $enroll_desire_register_at = date('Y-m-d', strtotime(str_replace('/', '-', $this->enroll_desire_register_at)));
            if($enroll_desire_at >= $enroll_desire_register_at){
                $date1=date_create($enroll_desire_at);
                $date2=date_create($enroll_desire_register_at);
                $diff = date_diff($date1,$date2)->format("%a");
            }else{
                $diff = 0;
            }
        }
        return $diff;
    }

    public function exchangeSuspect()
    {
        $accept_department_ids = array(
            1,   //SMIS Cầu Giấy - Hà Nội
            15,  //SMIS Dương Kinh - Hải Phòng
            18,  //SMIS THT
            2,   //GIS Cầu giấy
            8,   //GIS Hải Phòng
            16,  //GIS THT
            20,  //Aqua-tots Cầu Giấy, Hà Nội
            21,  //Aqua-tots Hải Phòng
            25,  //Aqua-tots THT
        );
        if(in_array($this->department_id, $accept_department_ids)){
            $suspect = new Suspect();
            $source = 6;
            switch ($this->department_id){
                case 1:
                case 2:
                    $suspect->department_id = 20; $suspect->assignee = 180;
                    break;
                case 8:
                case 15:
                    $suspect->department_id = 21;
                    break;
                case 16:
                case 18:
                    $suspect->department_id = 25;  break;
                    break;
                case 20: $suspect->department_id = 1;  break;
                case 21: $suspect->department_id = 15; break;
            }
            switch ($this->department_id){
                case 1:
                case 15:
                case 18:
                    $source = 31;
                    break;
                case 2:
                case 8:
                case 16:
                    $source = 32;
                    break;
            }
            $suspect->origin_lead_id    = $this->id;
            $suspect->name              = $this->name;
            $suspect->phone1            = $this->phone1;
            $suspect->phone2            = $this->phone2;
            $suspect->email             = $this->email;
            $suspect->student_name      = !empty($this->student_name) ? $this->student_name : $this->nick_name;
            $suspect->birthday          = $this->birthdate;
            $suspect->source            = $source;
            $suspect->status            = $this->status;
            $suspect->created_by        = $this->created_by;
            $suspect->note              = $this->notes;
            $suspect->utm_source        = $this->utm_source;
            $suspect->utm_campaign      = $this->utm_campaign;
            $suspect->utm_medium        = $this->utm_medium;
            $suspect->utm_content       = $this->utm_content;
            $suspect->utm_term          = $this->utm_term;
            $suspect->utm_link          = $this->utm_link;
            $suspect->save();

            if(in_array($suspect->department_id, [20,21,25])){
                $suspect->convertToLead(TRUE);
            }
        }

    }

    public function sendRequestOracleERP()
    {
        $accept_statuses = array(
            LeadStatus::STATUS_WAITLIST,
            LeadStatus::STATUS_NEW_SALE,
        );
        $accept_departments = array(
            5,
            8,
            12,
            15,
            17,
        );
        if(in_array($this->status, $accept_statuses) && in_array($this->department_id, $accept_departments)){

            $status_des = null;
            switch ($this->status){
                case LeadStatus::STATUS_WAITLIST: $status_des = 'WL'; break;
                case LeadStatus::STATUS_NEW_SALE: $status_des = 'NS'; break;
            }

            $method = 'POST';
            $url = env('URL_REQUEST_ORACLE_ERP');
            $header = array();
            $data = array(
                'lead_id'       => $this->id,
                'status'        => $status_des,
                'user_email'    => Auth::user()->email,
                'school_id'     => $this->department_id,
            );
            $response = Utils::callAPI($method,$url,$header,$data,$http_status);
        }
    }
}
