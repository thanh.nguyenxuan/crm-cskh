<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 24/11/2017
 * Time: 2:39 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $table = 'fptu_school';
    protected $fillable = array('name', 'type', 'address', 'province');


    public static function listData($province = null)
    {
        $data = (!empty($province))
            ? self::where('province', $province)->get()->pluck('name','id')
            : self::all()->pluck('name','id');
        return $data;
    }
}