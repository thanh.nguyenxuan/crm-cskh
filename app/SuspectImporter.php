<?php

namespace App;

use App\Suspect\Suspect;
use PHPExcel;
use PHPExcel_IOFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\ManualLeadAssigner;
use App\Major;
use App\LeadStatus;
use App\CallStatus;
use App\Province;
use App\District;


class SuspectImporter
{
    private static $marTViet = array("à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ", "ì", "í", "ị", "ỉ", "ĩ", "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ", "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ", "ỳ", "ý", "ỵ", "ỷ", "ỹ", "đ", "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ", "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ", "Ì", "Í", "Ị", "Ỉ", "Ĩ", "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ", "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ", "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ", "Đ");
    private static $marKoDau = array("a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "i", "i", "i", "i", "i", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "y", "y", "y", "y", "y", "d", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "I", "I", "I", "I", "I", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "Y", "Y", "Y", "Y", "Y", "D");
    private static $tohop = array("à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ", "ì", "í", "ị", "ỉ", "ĩ", "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ", "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ", "ỳ", "ý", "ỵ", "ỷ", "ỹ", "đ", "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ", "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ", "Ì", "Í", "Ị", "Ỉ", "Ĩ", "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ", "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ", "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ", "Đ");

    function proper_name($string, $encoding = 'UTF-8')
    {
        $return_string = mb_strtolower($string, $encoding);
        $words = explode(' ', $return_string);
        foreach ($words as $index => $word) {
            $words[$index] = mb_strtoupper(mb_substr($word, 0, 1, $encoding), $encoding) . mb_substr($word, 1, mb_strlen($word, $encoding), $encoding);
        }
        $return_string = implode(' ', $words);
        return $return_string;
    }

    function prepare_name($name)
    {
        $name = str_replace(self::$tohop, self::$marTViet, $name);
        $name = $this->proper_name(mb_strtolower($name, 'UTF-8'));
        return $name;
    }

    function validate_phong($phong)
    {
        $phong_list = array('HN', 'DN', 'HCM', 'TT', 'CT');
        if (in_array($phong, $phong_list))
            return true;
        return false;
    }

    function validate_phone($phone)
    {
        //$phone = $this->sanitize_phone($phone);
        if (strlen($phone) != 10 && strlen($phone) != 11)
            return false;
        return true;
    }

    function romanize_string($string)
    {
        $string = trim($string);
        $string = str_replace(self::$marTViet, self::$marKoDau, $string);
        $string = preg_replace('/\./', '', $string);
        return $string;
    }

    function legacy_romanize_string($string)
    {
        $string = trim($string);
        $string = str_replace(self::$marTViet, self::$marKoDau, $string);
        $string = preg_replace("/[^A-Za-z0-9 ]/", '', $string);
        return $string;
    }

    function sanitize_province($province)
    {
        $province = $this->romanize_string($province);
        $province = str_replace(' ', '', $province);
        $province = str_replace('-', '', $province);
        $province = str_replace('TP.', '', $province);
        switch ($province) {
            case 'TPHOCHIMINH':
                $province = 'HOCHIMINH';
                break;
            case 'VUNGTAU':
                $province = 'BARIAVUNGTAU';
                break;
            case 'HUE':
                $province = 'THUATHIENHUE';
                break;
            case 'DACLAC':
                $province = 'DAKLAK';
                break;
            case 'DACNONG':
                $province = 'DAKNONG';
                break;
        }
        return strtoupper($province);
    }

    function validate_district($district)
    {
        $district = trim($district);
        $count = DB::table('edf_district')->where('name', $district)->count();
        if ($count)
            return true;
        return false;
    }

    function validate_province($province)
    {
        ;
        $province = $this->sanitize_province($province);
        $count = DB::table('fptu_province')->where('name_upper', $province)->count();
        //var_dump(DB::table('fptu_province')->where('name_upper', $province)->toSql());
        //var_dump($province);
        if ($count)
            return true;
        return false;
    }

    function validate_source($source)
    {
        $source_check = DB::table('fptu_source')->where('name', $source);
        if ($source_check->count())
            return true;
        return false;
    }

    function validate_channel($channel)
    {
        $channel_check = DB::table('fptu_channel')->where('name', $channel);
        if ($channel_check->count())
            return true;
        return false;
    }

    function validate_campaign($campaign)
    {
        $campaign_check = DB::table('fptu_campaign')->where('name', $campaign);
        if ($campaign_check->count())
            return true;
        return false;
    }

    function validate_group($group_name)
    {
        $group_check = DB::table('fptu_group')->where('name', $group_name);
        if ($group_check->count())
            return true;
        return false;
    }

    function validate_campus($campus_code)
    {
        $campus_code = trim(strtoupper($campus_code));
        $check = DB::table('fptu_campus')->where('code', $campus_code)->count();
        if ($check)
            return true;
        return false;
    }

    function validate_assignee($assignee)
    {
        $count = DB::table('fptu_user')->where('username', $assignee)->count();
        if ($count)
            return true;
        return false;
    }

    function check_duplicate_phone($number)
    {
        $count = DB::table('fptu_lead')->where('phone1', $number)->orWhere('phone2', $number)->orWhere('phone3', $number)->orWhere('phone4', $number)->count();
        if ($count > 0)
            return true;
        return false;
    }

    function validate_date($date_string, $format = 'd/m/Y')
    {
        $day = 0;
        $month = 0;
        $year = 0;
        switch ($format) {
            case 'd/m/Y':
                $date_chunks = explode('/', $date_string);
                if (count($date_chunks) == 3) {
                    $day = intval(trim($date_chunks[0]));
                    $month = intval(trim($date_chunks[1]));
                    $year = intval(trim($date_chunks[2]));
                }
                break;
            case 'd-m-Y':
                $date_chunks = explode('-', $date_string);
                if (count($date_chunks) == 3) {
                    $day = intval(trim($date_chunks[0]));
                    $month = intval(trim($date_chunks[1]));
                    $year = intval(trim($date_chunks[2]));
                }
                break;
            case 'Y-m-d':
                $date_chunks = explode('-', $date_string);
                if (count($date_chunks) == 3) {
                    $day = intval(trim($date_chunks[2]));
                    $month = intval(trim($date_chunks[1]));
                    $year = intval(trim($date_chunks[0]));
                }
                break;
        }
        if ($year < 1000 || $year > 9999)
            return false;
        return checkdate($month, $day, $year);
    }

    function parse_date($date_string)
    {
        $day = 0;
        $month = 0;
        $year = 0;
        if (substr_count($date_string, '-') == 2) {
            $date_chunks = explode('-', $date_string);
            if (count($date_chunks) == 3) {
                $day = intval(trim($date_chunks[1]));
                $month = intval(trim($date_chunks[0]));
                $year = intval(trim($date_chunks[2]));
            }
        }
        if (substr_count($date_string, '/') == 2) {
            $date_chunks = explode('/', $date_string);
            if (count($date_chunks) == 3) {
                $day = intval(trim($date_chunks[0]));
                $month = intval(trim($date_chunks[1]));
                $year = intval(trim($date_chunks[2]));

            }
        }
        if ($year <= 99) {
            if ($year >= 45) {
                $year = 1900 + $year;
            } else {
                $year = 2000 + $year;
            }
        }
        if (checkdate($month, $day, $year))
            return $year . '-' . $month . '-' . $day;

        return '';

    }

    function import_row($row)
    {
        $created_at     = trim($row[1]);
        $department     = strtoupper(trim($row[2]));
        $source         = strtoupper(trim($row[3]));
        $name           = trim($row[4]);
        $phone1         = $this->sanitize_phone($row[5]);
        $phone2         = $this->sanitize_phone($row[6]);
        $email          = trim($row[7]);
        $suspect_status = trim($row[8]);
        $assignee       = trim($row[9]);

        $duplicate_message = '';
        $message = '';
        $status = 'ok';
        $data = array();

        $department_id = Department::where('name', $department)->first()->id;
        $status_id = (!empty($suspect_status)) ? LeadStatus::where('name', $suspect_status)->first()->id : 1;

        if (!empty($created_at)) {
            if (!Utils::validateDate($created_at, 'd/m/Y') && !Utils::validateDate($created_at, 'd/m/Y H:i:s')) {
                $message .= 'Ngày tạo lead \'<strong>' . $created_at . '</strong>\' không hợp lệ. ';
                $status = 'failed';
            } else {
                if (Utils::validateDate($created_at, 'd/m/Y'))
                    $created_at = Utils::convertDate($created_at, 'd/m/Y', 'Y-m-d H:i:s');
                elseif (Utils::validateDate($created_at, 'd/m/Y H:i:s'))
                    $created_at = Utils::convertDate($created_at, 'd/m/Y H:i:s', 'Y-m-d H:i:s');
            }
        }
        if (empty($source) || !$this->validate_source($source)) {
            $message .= 'Nguồn dẫn \'<strong>' . $source . '</strong>\' không hợp lệ. ';
            $status = 'failed';
        }
        if (empty($phone1) && empty($phone2)) {
            $message .= 'Thiếu cả 2 số điện thoại liên hệ. ';
            $status = 'failed';
        } elseif (!empty($phone1) && !$this->validate_phone($phone1)) {
            $message .= 'Số điện thoại 1 \'<strong>' . $phone1 . '</strong>\' không hợp lệ. ';
            $status = 'failed';
        } else if(!empty($phone2) && !$this->validate_phone($phone2)){
            $message .= 'Số điện thoại 2 \'<strong>' . $phone2 . '</strong>\' không hợp lệ. ';
            $status = 'failed';
        } else {
            if (!empty($phone1)) {
                if ($this->checkDuplicatePhone($phone1, $department_id)) {
                    $message .= 'Số điện thoại 1 \'<strong>' . $phone1 . '</strong>\' bị trùng. ';
                    $status = 'dup';
                }
            }else if (!empty($phone2)) {
                if ($this->checkDuplicatePhone($phone2, $department_id)) {
                    $message .= 'Số điện thoại 2 \'<strong>' . $phone2 . '</strong>\' bị trùng. ';
                    $status = 'dup';
                }
            }
        }
        if (!empty($assignee)) {
            if (!$this->validate_assignee($assignee)) {
                $message .= 'Tên đăng nhập của người phụ trách \'<strong>' . $assignee . '</strong>\' không hợp lệ. ';
                if ($status != 'dup')
                    $status = 'failed';
            }
        }

        if (!empty($message)) {
            $data['result'] = $status;
            $data['message'] = $message;
            $data['duplicate_message'] = $duplicate_message;
        } else {
            $data['result'] = 'ok';
            $data['message'] = 'Thành công!';
            $suspect = new Suspect();
            $suspect->name = $this->prepare_name($name);
            $suspect->department_id = $department_id;
            if (!empty($phone1))
                $suspect->phone1 = $phone1;
            if (!empty($phone2))
                $suspect->phone2 = $phone2;
            $suspect->email = $email;
            $suspect->status = $status_id;

            if (!empty($assignee)){
                $assignee_id = User::where('username', $assignee)->first()->id;
                $suspect->assignee = $assignee_id;
            }
            if(!empty($source)) {
                $source_id = Source::where('name', $source)->first()->id;
                $suspect->source = $source_id;
            }
            if (!empty($created_at)){
                $suspect->created_at = $created_at;
            }
            $suspect->created_by = Auth::user()->id;
            $suspect->save();
        }

        return $data;
    }


    function sanitize_phone($mobile)
    {
        $mobile = preg_replace('/[^\d]/', '', $mobile);
        if (substr($mobile, 0, 1) != '0')
            $mobile = '0' . $mobile;
        if ($mobile == '0')
            return '';
        return $mobile;
    }

    function dump_file($row_list = array(), $file_name)
    {
        $template_path = public_path() . '/templates/template_suspect.xlsx';
        $output_path = public_path() . '/uploads/suspect/';
        if (file_exists($template_path)) {
            $objPHPExcel = PHPExcel_IOFactory::load($template_path);
            $objPHPExcel->getActiveSheet()->fromArray($row_list, '', 'A2');
            //Storage::disk('local')->put('Failed_Import.xlsx', 'Contents');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
            $objWriter->save($output_path . $file_name);
        } else {
            echo '<div class="alert alert-danger">Lỗi không tìm thấy file template</div>';
        }
    }

    public
    function import($path)
    {
        $now_date = date('Y-m-d H:i:s');
        $now_string = date('YmdHis');
        if (!file_exists($path))
            return '<div class="alert alert-danger">Không tìm thấy file import!</div>';
        ini_set('display_errors', 0);
        //ini_set('error_reporting', E_ALL);
        error_reporting(E_ERROR | E_PARSE);
        $objPHPExcel = PHPExcel_IOFactory::load($path);
        $data = $objPHPExcel->getActiveSheet()->toArray();
        $result = array('message' => '', 'data' => array());
        $first_row = true;
        $row_number = 2;
        $success = 0;
        $failed = 0;
        $failed_row_list = array();
        $dup_row_list = array();
        $success_row_list = array();
        $duplicated = 0;
        $total = count($data) - 1;
        $message = '';
        foreach ($data as $row) {
            if ($first_row) {
                $first_row = false;
                continue;
            }
            $import_result = $this->import_row($row);
            //var_dump($import_result);
            $banner = 'Dòng ' . $row_number . ': ';
            if ($import_result['result'] == 'ok') {
                $success++;
                $message .= '<div class="alert alert-success">' . $banner . 'Thành công!</div>';
                $success_row_list[] = $row;

            } elseif ($import_result['result'] == 'dup') {
                $duplicated++;
                $row[] = strip_tags($import_result['message']);
                $row[] = strip_tags($import_result['duplicate_message']);
                $dup_row_list[] = $row;
                $message .= '<div class="alert alert-warning">' . $banner . $import_result['message'] . '</div>';
            } elseif ($import_result['result'] == 'failed') {
                $failed++;
                $row[] = strip_tags($import_result['message']);
                $failed_row_list[] = $row;
                $message .= '<div class="alert alert-danger">' . $banner . $import_result['message'] . '</div>';
            }
            $row_number++;
        }
        $original_file_name = 'Import_Original_' . $now_string . '.xlsx';
        $original_file_path = public_path() . '/uploads/suspect/' . $original_file_name;
        //var_dump($original_file_path);
        //var_dump($path);
        $archived = move_uploaded_file($path, $original_file_path);
        //$archived = true;
        //var_dump($archived);
        if (!$archived)
            $message .= '<div class="alert alert-danger">Đã có lỗi trong quá trình lưu trữ lại file gốc.</div>';
        if ($failed > 0) {
            $error_file_name = 'Import_Error_' . $now_string . '.xlsx';
            $this->dump_file($failed_row_list, $error_file_name);
        }
        if ($duplicated > 0) {
            $dup_file_name = 'Import_Duplicate_' . $now_string . '.xlsx';
            $this->dump_file($dup_row_list, $dup_file_name);
        }
        if ($success > 0) {
            $success_file_name = 'Import_Success_' . $now_string . '.xlsx';
            $this->dump_file($success_row_list, $success_file_name);
        }
        $result['data']['success'] = $success;
        if ($success)
            $result['data']['success_file_name'] = $success_file_name;
        $result['data']['failed'] = $failed;
        if ($failed)
            $result['data']['error_file_name'] = $error_file_name;
        $result['data']['dup'] = $duplicated;
        if ($duplicated)
            $result['data']['duplicate_file_name'] = $dup_file_name;
        $result['data']['total'] = $total;
        $result['message'] = $message;
        $import = new SuspectFileImport();
        $import->original_file_name = $original_file_name;
        $import->original_row_num = $total;
        if (!empty($error_file_name)) {
            $import->error_file_name = $error_file_name;
            $import->error_row_num = $failed;
        }
        if (!empty($dup_file_name)) {
            $import->duplicate_file_name = $dup_file_name;
            $import->duplicate_row_num = $duplicated;
        }
        if (!empty($success_file_name)) {
            $import->success_file_name = $success_file_name;
            $import->success_row_num = $success;
        }
        $import->created_at = $now_date;
        $import->created_by = Auth::id();
        $import->save();
        return $result;
    }



    public function checkDuplicatePhone($number, $department_id)
    {
        if (!empty($number)) {
            $count = DB::table('edf_suspect')
                ->where(function ($query) use ($number) {
                    $query->where('phone1', $number)
                        ->orWhere('phone2', $number);

                })
                ->whereNull('deleted_at')
                ->where('department_id', $department_id)
                ->count();
            if ($count > 0)
                return true;
        }
        return false;
    }
}