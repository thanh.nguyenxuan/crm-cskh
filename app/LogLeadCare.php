<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Excel;
use Illuminate\Support\Facades\DB;

class LogLeadCare extends Model
{
    public $table = 'log_lead_care';


    CONST CHANNEL_ZALO      = 'zalo';
    CONST CHANNEL_MESSAGE   = 'msg';
    CONST CHANNEL_EMAIL     = 'email';
    CONST CHANNEL_FACEBOOK  = 'facebook';

    public static function channelListData()
    {
        return array(
            self::CHANNEL_ZALO      => 'Zalo',
            self::CHANNEL_MESSAGE   => 'Tin nhắn',
            self::CHANNEL_EMAIL     => 'Email',
            self::CHANNEL_FACEBOOK  => 'Facebook',
        );
    }



}