<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $table = 'fptu_promotion';
    protected $fillable = array('code', 'name', 'percentage');
}
