<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadSource extends Model
{
    protected $table = 'fptu_lead_source';
    protected $fillable = array('lead_id', 'source_id');
}
