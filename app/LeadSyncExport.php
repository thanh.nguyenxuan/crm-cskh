<?php

namespace App;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class LeadSyncExport implements FromCollection, WithHeadings
{
    public function __construct($data)
    {
        $this->data = $data;
    }


    public function collection()
    {
        return $this->data;
    }

    public function headings() : array
    {
        return array(
            'id',
            'name',
            'phone',
            'email',
            'student_name',
            'birthday',
            'gender',
            'department',
            'status',
            'duplicate',
            'request_at',
            'utm_source',
            'utm_campaign',
            'url'
        );
    }
}