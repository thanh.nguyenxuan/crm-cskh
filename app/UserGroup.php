<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    protected $table = 'fptu_user_group';
	protected $fillable = array('user_id', 'group_id');
}
