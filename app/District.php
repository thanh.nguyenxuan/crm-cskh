<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'edf_district';

    public static function listData($province = null)
    {
        return (!empty($province))
            ? self::where('province', $province)->get()->pluck('name','id')
            : self::all()->pluck('name','id');
    }
}
