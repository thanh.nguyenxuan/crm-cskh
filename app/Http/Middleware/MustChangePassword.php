<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class MustChangePassword
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $current_path = $request->path();
        $change_password_path = 'user/' . Auth::id() . '/resetpassword';
        $logout_path = 'user/logout';
        if ($current_path != $change_password_path && $current_path != $logout_path && !empty(Auth::user()) && Auth::user()->password_expired) {
            return redirect($change_password_path);
        }

        return $next($request);
    }
}
