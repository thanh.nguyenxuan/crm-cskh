<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'leadMarketing/landingPage',
        'leadMarketing/website',
        'leadMarketing/facebook',
        'leadMarketing/tawkto',
        'leadMarketing/tawkto/offline',
        'leadMarketing/ahaChat',
        'leadMarketing/offline',
        'lead/updateStatus',
        'leadMarketing/lms/getCommonCrm',
        'leadMarketing/lms/getListRefCrm',
        'leadMarketing/lms/submitRef',
        'leadMarketing/lms/getRuleRef',
        'leadMarketing/lms/commons',
        'leadMarketing/lms/checkUpdate',
        'syncLead',
    ];
}
