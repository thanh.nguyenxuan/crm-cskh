<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 24/11/2017
 * Time: 2:39 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class OnlineController extends Model
{
    protected $table = 'fptu_online';
}