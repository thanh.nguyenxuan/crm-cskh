<?php

namespace App\Http\Controllers;

use App\Dashboard;
use App\RUserDepartment;
use App\User;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Shared\OLE\PPS;

class SiteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $date_range = !empty($request->date_range) ? $request->date_range : (date("01/m/Y", strtotime('- 3 months')). ' - ' . date("t/m/Y"));
        $date_arr = explode(' - ', $date_range);
        $dashboard = new Dashboard();
        $dashboard->start_date = $date_arr[0];
        $dashboard->end_date = $date_arr[1];
        $dashboard->department_id = $request->department_id;

        $tileStatsCounts = $dashboard->getStatCount();
        $conversionRate = $dashboard->getConversionRate();
        $sourceRate = $dashboard->getSourceRate();

        session()->flashInput($request->input());
        return view('site.index', [
            'tileStatsCounts' => $tileStatsCounts,
            'date_range' => $date_range,
            'conversionRate' => $conversionRate,
            'sourceRate' => $sourceRate,
            'start_date' => $dashboard->start_date,
            'end_date' => $dashboard->end_date,
        ]);
    }

}