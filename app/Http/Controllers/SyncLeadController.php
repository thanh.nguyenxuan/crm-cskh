<?php

namespace App\Http\Controllers;

use App\Lead;
use App\LeadSource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class SyncLeadController extends Controller
{

    public function index(Request $request)
    {
        $return = [
            'success' => TRUE,
            'message' => '',
            'data' => array()
        ];

        $lead = new Lead();

        $lead->name             = $request->name;
        $lead->student_name     = $request->student_name;
        $lead->birthdate        = $request->birthdate;
        $lead->nick_name        = $request->nick_name;
        $lead->gender           = $request->gender;
        $lead->twins            = $request->twins;
        $lead->address          = $request->address;
        $lead->phone1           = $request->phone1;
        $lead->phone2           = $request->phone2;
        $lead->email            = $request->email;
        $lead->email_2          = $request->email_2;
        $lead->facebook         = $request->facebook;
        $lead->status           = $request->status;
        $lead->call_status      = $request->call_status;
        $lead->department_id    = $request->department_id;
        $lead->showup_school_at = $request->showup_school_at;
        $lead->activated        = $request->activated;
        $lead->activated_at     = $request->activated_at;
        $lead->new_sale         = $request->new_sale;
        $lead->new_sale_at      = $request->new_sale_at;
        $lead->enrolled         = $request->enrolled;
        $lead->enrolled_at      = $request->enrolled_at;
        $lead->created_at       = !empty($request->created_at) ? $request->created_at : date('Y-m-d H:i:s');
        $lead->created_by       = 4;
        $lead->class_name       = $request->class_name;
        $lead->student_code     = $request->student_code;
        $lead->new_promoter     = 1;
        $lead->new_promoter_at  = $lead->enrolled_at;

        if(!$this->checkDuplicate($lead)){
            if($lead->save()){
                $return['data']['lead_id'] = $lead->id;

                $source_id = $request->source_id;
                if(!empty($source_id)){
                    $lead_source = new LeadSource;
                    $lead_source->lead_id = $lead->id;
                    $lead_source->source_id = $source_id;
                    $lead_source->created_by = $lead->created_by;
                    $lead_source->created_at = date('Y-m-d H:i:s');
                    $lead_source->save();
                }
            }else{
                $return['success'] = FALSE;
                $return['message'] = 'save_record_failed';
            }
        }else{
            $return['success'] = FALSE;
            $return['message'] = 'duplicate_lead';
        }

        echo json_encode($return);
        exit();
    }


    protected function checkDuplicate($lead)
    {
        if($lead->twins == 1){
            return FALSE;
        }
        $count = Lead::where('id', '!=', $lead->id)
            ->where(function ($count) use ($lead) {
                $count->where('phone1', $lead->phone1)
                    ->orWhere('phone2', $lead->phone1);
                if(!empty($lead->phone2)){
                    $count->orWhere('phone1', $lead->phone2)
                        ->orWhere('phone2', $lead->phone2);
                }
                if(!empty($lead->student_code)){
                    $count->orWhere('student_code', $lead->student_code);
                }
            })
            ->whereNull('deleted_at')
            ->where('department_id', $lead->department_id)
            ->whereRaw('leave_school IS NULL OR leave_school != 1')
            ->count();
        return ($count > 0);
    }

}