<?php

namespace App\Http\Controllers;

use App\CampaignCategoryMapping;
use Illuminate\Http\Request;
use App\Campaign;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CampaignController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $campaign_list = Campaign::select(
            'fptu_campaign.*',
            DB::raw("(SELECT GROUP_CONCAT(name) FROM fptu_campaign_category WHERE id IN (SELECT category_id FROM fptu_campaign_category_mapping WHERE campaign_id = fptu_campaign.id AND deleted_at IS NULL)) AS category_list")
        )->get();
        return view('campaign.index', ['campaign_list' => $campaign_list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('campaign.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = 0;
        $return_message = '';
        $found = Campaign::where('name', $request->name)
            ->count();
        $error = false;
        if ($found) {
            $return_message = 'Tên chiến dịch quảng cáo đã tồn tại.';
            $error = true;
        } else {
            $return_message = 'Tạo chiến dịch quảng cáo mới thành công!';
            $success = 1;
            $campaign = new Campaign();
            $campaign->name = trim($request->name);
            $saved = $campaign->save();
            if ($saved) {
                if(!empty($request->category)){
                    $campaignCategoryMapping = new CampaignCategoryMapping();
                    $campaignCategoryMapping->category_id = $request->category;
                    $campaignCategoryMapping->campaign_id = $campaign->id;
                    $campaignCategoryMapping->created_by = Auth::user()->id;
                    $campaignCategoryMapping->save();
                }
            }else{
                $return_message = 'Đã có lỗi trong quá trình lưu dữ liệu!';
                $success = 0;
                $error = true;
            }
        }
        $request->flash();
        if ($error) return view('campaign.create', ['success' => $success, 'message' => $return_message]);

        $category = CampaignCategoryMapping::whereNull("deleted_at")->where('campaign_id', $campaign->id)->first();
        return view('campaign.edit', ['campaign' => $campaign, 'category' => $category ,'success' => $success, 'message' => $return_message]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $campaign = Campaign::find($id);
        $category = CampaignCategoryMapping::whereNull("deleted_at")->where('campaign_id', $id)->first();

        return view('campaign.edit', ['campaign' => $campaign, 'category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $success = 0;
        $campaign = Campaign::find($id);
        $error = false;
        $return_message = '';

        if (!$error) {
            $update = array(
                'name' => $request->name
            );

            $updated = $campaign->update($update);
            if ($updated) {
                $return_message = 'Đã cập nhật dữ liệu thành công!';
                $success = 1;

                if(!empty($request->category)){
                    $categoryMapping = CampaignCategoryMapping::whereNull("deleted_at")
                        ->where('campaign_id', $campaign->id)
                        ->where('category_id', $request->category)
                        ->first();
                    if(!$categoryMapping){
                        CampaignCategoryMapping::whereNull("deleted_at")->where('campaign_id', $campaign->id)->delete();
                        $campaignCategoryMapping = new CampaignCategoryMapping();
                        $campaignCategoryMapping->category_id = $request->category;
                        $campaignCategoryMapping->campaign_id = $campaign->id;
                        $campaignCategoryMapping->created_by = Auth::user()->id;
                        $campaignCategoryMapping->save();

                    }
                }


            } else {
                $return_message = 'Đã có lỗi trong quá trình cập nhật dữ liệu.';
                $success = 0;
            }
        }
        //$user = User::find($id);
        $category = CampaignCategoryMapping::whereNull("deleted_at")->where('campaign_id', $id)->first();
        return view('campaign.edit', ['campaign' => $campaign, 'category' => $category, 'success' => $success, 'message' => $return_message]);
    }

    public function toggle($id)
    {
        $success = 0;
        $info_source = Campaign::find($id);
        $error = false;
        $return_message = '';
        $update = array();
        if ($info_source->hide == 1) {
            $info_source->hide = 0;
        } else {
            $info_source->hide = 1;
        }

        $updated = $info_source->save();
        if ($updated) {
            $return_message = 'Đã cập nhật trạng thái lớp thành công!';
            $success = 1;
        } else {
            $return_message = 'Đã có lỗi trong quá trình cập nhật dữ liệu.';
            $success = 0;
        }
        $info_source_list = array();
        $info_source_list = Campaign::all();
        return view('campaign.index', ['success' => $success, 'message' => $return_message, 'campaign_list' => $info_source_list]);
    }
}
