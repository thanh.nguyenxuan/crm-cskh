<?php

namespace App\Http\Controllers;

use App\RGroupUser;
use App\RUserDepartment;
use Illuminate\Http\Request;
use App\Role;
use App\User;
use App\Department;
use Illuminate\Support\Facades\Auth;
use Validator;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_list = array();
        if(Auth::user()->hasRole('admin')){
            $query = User::orderByRaw('active DESC, username ASC');
            if (!in_array(Department::HOLDING, Auth::user()->getManageDepartments()) && !Auth::user()->hasRole('holding')) {
                $departments_str = implode(',', Auth::user()->getManageDepartments());
                $query->whereRaw("(department_id IN ($departments_str)
                    OR id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                    OR id IN (SELECT user_id FROM group_user WHERE group_id IN (
                        SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                    ))
                )");
            }
            $user_list = $query->get();
        }


        $departments = Department::all()->pluck('name', 'id')->toArray();
        foreach ($user_list as $user) {
            if (!empty($user->department_id)) {
                $user->department = $departments[$user->department_id];
            }

        }
        return view('user.index', ['user_list' => $user_list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role_list = array();
        $department_list = array();
        if (Auth::user()->hasRole('admin')) {
            if(Auth::user()->hasRole('admin') && in_array(Department::HOLDING, Auth::user()->getManageDepartments()) || Auth::user()->hasRole('holding')){
                $role_list = Role::whereIn('id', [1,11,13,14])->get();
                $department_list = Department::pluck('name', 'id')->toArray();
            }else{
                if (Auth::user()->hasRole('teleteamleader')){
                    $role_list = array(Role::find(2));
                    $department_list = Department::whereIn('id', Auth::user()->getManageDepartments())->pluck('name', 'id')->toArray();
                }
            }
        }
        return view('user.create', ['role_list' => $role_list, 'department_list' => $department_list]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = 0;
        $role_list = array();
        $department_list = array();
        if (Auth::user()->hasRole('admin')) {
            if(Auth::user()->hasRole('admin') && in_array(Department::HOLDING, Auth::user()->getManageDepartments()) || Auth::user()->hasRole('holding')){
                $role_list = Role::whereIn('id', [1,11,13,14])->get();
                $department_list = Department::pluck('name', 'id')->toArray();
            }else{
                if (Auth::user()->hasRole('teleteamleader')){
                    $role_list = array(Role::find(2));
                    $department_list = Department::whereIn('id', Auth::user()->getManageDepartments())->pluck('name', 'id')->toArray();
                }
            }
        }
        $roles = $request->role;
        //var_dump($roles);
        $ext = trim($request->extension);

        $error = false;
        $return_message = '';
        $found = User::where('username', $request->username)
            ->orWhere('email', $request->email)
            ->count();
        if ($found) {
            $return_message = '<div>Tên đăng nhập hoặc email đã tồn tại.</div>';
            $error = true;
        }
        if(!empty($ext)){
            $ext_found = User::where('extension', $ext)
                ->count();
            if ($ext_found) {
                $return_message .= '<div>Số máy lẻ đã được sử dụng. Vui lòng chọn một số máy lẻ khác mà bạn đã được cấp.</div>';
                $error = true;
            }
        }
        if ($request->password != $request->password_confirmation) {
            $return_message .= '<div>Mật khẩu và xác nhận mật khẩu không giống nhau.</div>';
            $error = true;
        }
        if (empty($roles)) {
            $return_message .= '<div>Bạn phải cấp tối thiểu một quyền cho người dùng.</div>';
            $error = true;
        }
        if (!$error) {
            $user = User::create([
                'username' => $request->username,
                'password' => bcrypt($request->password),
                'password_expired' => 1,
                'full_name' => $request->name,
                'email' => $request->email,
                'active' => $request->active,
                'department_id' => $request->department,
                'extension' => $request->extension,
                'created_by' => Auth::user()->id,
            ]);
            $user->roles()->attach($roles);

            foreach ($request->departments as $department_id){
                $department_user = new RUserDepartment();
                $department_user->user_id = $user->id;
                $department_user->department_id = $department_id;
                $department_user->created_by = Auth::user()->id;
                $department_user->save();
            }

            if(!empty($request->groups)){
                foreach ($request->groups as $group_id){
                    $group_user = new RGroupUser();
                    $group_user->user_id = $user->id;
                    $group_user->group_id = $group_id;
                    $group_user->created_by = Auth::user()->id;
                    $group_user->save();
                }
            }

            $return_message = '';
            $success = 1;
        }
        $request->flash();
        return view('user.create', ['success' => $success, 'message' => $return_message, 'role_list' => $role_list, 'department_list' => $department_list]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $role_list = array();
        $user_roles = array();
        $department_list = array();
        //var_dump(Role::all());
        if (Auth::user()->hasRole('admin')) {
            if(Auth::user()->hasRole('admin') && in_array(Department::HOLDING, Auth::user()->getManageDepartments()) || Auth::user()->hasRole('holding')){
                $role_list = Role::whereIn('id', [1,11,13,14])->get();
                $department_list = Department::pluck('name', 'id')->toArray();
            }else{
                if (Auth::user()->hasRole('teleteamleader')){
                    $role_list = array(Role::find(2));
                    $department_list = Department::whereIn('id', Auth::user()->getManageDepartments())->pluck('name', 'id')->toArray();
                }
            }
        }
        if ($user->roles->count() > 0)
            $user_roles = $user->roles->pluck('id')->toArray();
        //var_dump($user->roles->count());
        if (!empty($user->department_id))
            $user->department = Department::find($user->department_id)->name;

        $user_departments = RUserDepartment::where('user_id', $user->id)->get()->pluck('department_id')->toArray();
        $user_groups = RGroupUser::where('user_id', $user->id)->get()->pluck('group_id')->toArray();
        return view('user.edit', [
            'user' => $user,
            'role_list' => $role_list,
            'department_list' => $department_list,
            'user_roles' => $user_roles,
            'user_departments' => $user_departments,
            'user_groups' => $user_groups
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $success = 0;
        $user = User::find($id);
        $role_list = array();
        $user_roles = array();
        $department_list = array();
        $user_departments = array();
        $user_groups = array();
        if (Auth::user()->hasRole('admin')) {
            if(Auth::user()->hasRole('admin') && in_array(Department::HOLDING, Auth::user()->getManageDepartments()) || Auth::user()->hasRole('holding')){
                $role_list = Role::whereIn('id', [1,11,13,14])->get();
                $department_list = Department::pluck('name', 'id')->toArray();
            }else{
                if (Auth::user()->hasRole('teleteamleader')){
                    $role_list = array(Role::find(2));
                    $department_list = Department::whereIn('id', Auth::user()->getManageDepartments())->pluck('name', 'id')->toArray();
                }
            }
        }
        $error = false;
        $return_message = '';
        $roles = $request->role;
        if ($request->password_change == 'change') {
            if ($request->password != $request->password_confirmation) {
                $return_message .= '<div>Mật khẩu và xác nhận mật khẩu không giống nhau.</div>';
                $error = true;
            }
            $validator = Validator::make($request->all(), [
                'password' => 'required|confirmed|min:6'
            ]);
//            $validator = Validator::make($request->all(), [
//                'password' => 'required|confirmed|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$/|'
//            ]);
            if ($validator->fails()) {
                $return_message .= '<div>Mật khẩu không hợp lệ.</div>';
                $error = true;
            }
        }
        $ext = trim($request->extension);
        if(!empty($ext)){
            $ext_found = User::where('extension', $ext)->where('id', '!=', $id)
                ->count();
            if ($ext_found) {
                $return_message .= '<div>Số máy lẻ đã được sử dụng. Vui lòng chọn một số máy lẻ khác mà bạn đã được cấp.</div>';
                $error = true;
            }
        }
        if (!$error) {
            $update = array(
                'full_name' => $request->name
            );
            if (Auth::user()->hasRole('admin')) {
                if (!empty($request->username))
                    $update['username'] = trim($request->username);
                $active = 0;
                if (!empty($request->active) && $request->active == 1)
                    $active = 1;
                $update['active'] = $active;
                if (!empty($request->email))
                    $update['email'] = trim($request->email);
                if (!empty($request->department))
                    $update['department_id'] = $request->department;
                if (!empty($request->extension)){
                    $update['extension'] = trim($request->extension);
                }else{
                    $update['extension'] = null;
                }
                if (empty($roles)) {
                    $return_message .= '<div>Bạn phải cấp tối thiểu một quyền cho người dùng.</div>';
                    $error = true;
                } else {
                    $user->roles()->sync($roles);
                }
            }
            if (!empty($request->password))
                $update['password'] = bcrypt($request->password);
            //var_dump($update);
            $updated = $user->update($update);
            if ($updated) {
                $return_message = '';
                $success = 1;

                if(Auth::user()->hasRole('admin'))
                {
                    RUserDepartment::where('user_id', $user->id)->delete();
                    if(!empty($request->departments)){
                        foreach ($request->departments as $department_id){
                            $department_user = new RUserDepartment();
                            $department_user->user_id = $user->id;
                            $department_user->department_id = $department_id;
                            $department_user->created_by = Auth::user()->id;
                            $department_user->save();
                        }
                    }
                    RGroupUser::where('user_id', $user->id)->delete();
                    if(!empty($request->groups)){
                        foreach ($request->groups as $group_id){
                            $group_user = new RGroupUser();
                            $group_user->user_id = $user->id;
                            $group_user->group_id = $group_id;
                            $group_user->created_by = Auth::user()->id;
                            $group_user->save();
                        }
                    }
                }

            } else {
                $return_message = '<div>Đã có lỗi trong quá trình cập nhật dữ liệu.</div>';
                $success = 0;
            }
        }
        $user = User::find($id);
        if ($user->roles->count() > 0)
            $user_roles = $user->roles->pluck('id')->toArray();
        if (!empty($user->department_id))
            $user->department = Department::find($user->department_id)->name;
        return view('user.edit', [
            'success' => $success,
            'user' => $user,
            'message' => $return_message,
            'role_list' => $role_list,
            'department_list' => $department_list,
            'user_roles' => $user_roles,
            'user_departments' => $user_departments,
            'user_groups' => $user_groups
        ]);
    }

    public function loginAs($user_id)
    {
        $loginAsUser = User::find($user_id);
        if (!empty($loginAsUser)) {
            session(['original_user_id' => Auth::id()]);
            $loginAsUser->full_name .= ' (Đăng nhập như)';
            Auth::login($loginAsUser);
            Auth::user()->getManageDepartments(TRUE);
        }
        return redirect('/');
    }

    public function logout(Request $request)
    {
        if (!empty(session('original_user_id'))) {
            $originalUser = User::find(session('original_user_id'));
            if (!empty($originalUser)) {
                $request->session()->forget('original_user_id');
                Auth::login($originalUser);
            }
        } else {
            Auth::logout();
        }
        return redirect('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        //
    }

    public
    function changePassword(Request $request)
    {
        echo 'Hello';
        //return resetPassword($request, Auth::id());
    }

    public
    function resetPassword(Request $request, $id = null)
    {
        $user = Auth::user();
        if (Auth::user()->hasRole('admin') && !empty($id)) {
            $user = User::find($id);
        }
        $success = 0;
        $message = '';
        if ($request->post('submit')) {
            $validator = Validator::make($request->all(), [
                'password' => 'required|confirmed|min:6'
            ]);
//            $validator = Validator::make($request->all(), [
//                'password' => 'required|confirmed|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$/|'
//            ]);
            if ($validator->fails()) {
                $message = 'Mật khẩu không hợp lệ.';
            } else {
                $user->password = bcrypt($request->password);
                if (!empty($user->password_expired)) {
                    $user->password_expired = 0;
                }
                $user->save();
                $success = 1;
            }
        } else {
            if ($user->password_expired) {
                $message = 'Mật khẩu cũ của bạn đã hết hạn sử dụng. Vui lòng đổi mật khẩu mới.';
            }
        }
        return view('user.resetpassword', ['success' => $success, 'user' => $user, 'message' => $message]);
    }

    public function getNotifications()
    {
        return view('user.notification');
    }


    public function getUserByDepartmentGroup(Request $request){
        $department_group = $request->get('department_group');
        $only_telesale = !empty($request->only_telesales) ? TRUE : FALSE;
        echo User::getOptionsUserByGroupDepartment(null, $department_group, $only_telesale);
    }

    public function getUserByDepartment(Request $request){
        $department_id = $request->get('department_id');
        $only_telesale = !empty($request->only_telesales) ? TRUE : FALSE;
        echo User::getOptionsUserByDepartment(null, $department_id, $only_telesale);
    }
}
