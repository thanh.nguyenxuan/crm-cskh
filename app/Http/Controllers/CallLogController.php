<?php

namespace App\Http\Controllers;

use App\Keyword;
use App\Lead;
use App\Lead\CallLog;
use App\User;
use App\Utils;
use App\Alarm;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class CallLogController extends Controller
{
    public function log()
    {
        $return = array(
            'success' => true,
            'message' => ''
        );

        $lead_id = Input::get('lead_id');
        $direction = Input::get('direction');
        $status = Input::get('status');
        $created_at = Input::get('created_at');
        $notes = Input::get('notes');
        $call_log = new Lead\CallLog();
        if (!empty($created_at)) {
            $created_at = Carbon::createFromFormat('d/m/Y H:i', $created_at)->format('Y-m-d H:i:00');
            $call_log->created_at = $created_at;
        }
        $call_log->lead_id = $lead_id;
        $call_log->direction = $direction;
        $call_log->status = $status;
        $call_log->created_at = $created_at;
        $call_log->notes = $notes;
        $call_log->created_by = Auth::id();

        $now = date("Y-m-d");
        $created_at = date("Y-m-d", strtotime($call_log->created_at));
        if($created_at == $now){
            $call_log->save();
        }else{
            $return['success'] = FALSE;
            $return['message'] = 'Thời điểm chăm sóc không hợp lệ!';
        }
        echo json_encode($return);
    }

    public function index(Request $request)
    {
        $from_date = $request->get('from_date', date('d/m/Y'));
        $to_date = $request->get('to_date', date('d/m/Y'));
        $direction = $request->get('direction');
        $created_by = $request->get('created_by');
        $status = $request->get('status');
        $tele_list = User::where('active', 1)->whereNull('deleted_at')->select(DB::raw("CONCAT(username,' - ', full_name) as username"), 'id')->orderBy('username', 'ASC')->pluck('username', 'id')->toArray();
        if(!empty($from_date)){
            $from_date = date('Y-m-d', strtotime(str_replace('/', '-', $from_date)));
        }
        if(!empty($to_date)){
            $to_date = date('Y-m-d', strtotime(str_replace('/', '-', $to_date)));
        }
        $query = CallLog::whereDate('edf_call_log.created_at', '>=', $from_date)
            ->whereDate('edf_call_log.created_at', '<=', $to_date)
            ->whereNull('edf_call_log.deleted_at');
        if (!Auth::user()->hasRole('admin') && !Auth::user()->hasRole('holding')) {
            $departments = Auth::user()->getManageDepartments();
            $departments_str = implode(',', $departments);
            $tele_list = User::whereIn('department_id', $departments)->where('active', 1)->whereNull('fptu_user.deleted_at')->select(DB::raw("CONCAT(username,' - ', full_name) as username"), 'id')->orderBy('username', 'ASC')->pluck('username', 'id')->toArray();
           $query->whereRaw("edf_call_log.created_by IN (SELECT id FROM fptu_user WHERE department_id IN ($departments_str))");
        }
        if(Auth::user()->hasRole('carer')){
            $query->whereRaw('lead_id IN (SELECT id FROM fptu_lead WHERE new_promoter = 1)');
        }
        if (!empty($direction)) {
            $query->where('edf_call_log.direction', $direction);
        }
        if (!empty($status)) {
            $query->where('edf_call_log.status', $status);
        }
        if (!empty($created_by)) {
            $query->where('edf_call_log.created_by', $created_by);
        }
        $call_history_list = $query->orderBy('edf_call_log.created_at', 'DESC')->get();
        $call_status_list = array(1 => 'Có nghe máy', 2 => 'Không nghe máy', 3 => 'Thuê bao', 4 => 'Sai số');
        return view('call_log.index', ['call_history_list' => $call_history_list, 'call_status_list' => $call_status_list, 'tele_list' => $tele_list, 'from_date' => $from_date, 'to_date' => $to_date]);
    }

    public function getCallLog($lead_id)
    {
        $call_history_list = CallLog::where('lead_id', $lead_id)
            ->whereNull('created_at')
            ->get()
            ->toArray();
        echo json_encode($call_history_list);
    }

    public function close($call_log_id)
    {
        $alarm_list = Input::get('alarm_list');
        if (!empty($alarm_list)) {
            foreach ($alarm_list as $alarm_id) {
                if (!empty($alarm_id)) {
                    $alarm = Alarm::find($alarm_id);
                    $alarm->closed = 1;
                    $alarm->save();
                }
            }
        }
    }
}