@include('includes.header')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Chỉnh sửa tài khoản người dùng</div>
                <div class="panel-body">
                    @if(!empty($message))
                        <div class="alert alert-danger" role="alert">{!! $message !!}</div>
                    @endif
                    @if(!empty($success))
                        <div class="alert alert-success" role="alert">Lưu dữ liệu thành công!</div>
                    @endif
                    <form class="form-horizontal" role="form" method="POST"
                          action="{{ route('user.update', $user->id) }}">
                        {!! method_field('patch') !!}
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">Tên đăng nhập</label>

                            <div class="col-md-6">
                                <label for="username" class="control-label">{{ $user->username }}</label>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Họ và tên</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name"
                                       value="{{ $user->full_name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">Email</label>

                            <div class="col-md-6">
                                @if(Auth::user()->hasRole('admin'))
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ $user->email }}" required>
                                @else
                                    {{ $user->email }}
                                @endif

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Đổi mật khẩu</label>

                            <div class="col-md-6">
                                {{ Form::checkbox('password_change', 'change', null, ['id' => 'password_change'])}}
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Mật khẩu</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required
                                       disabled>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Xác nhận mật khẩu</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control"
                                       name="password_confirmation" required disabled>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="department" class="col-md-4 control-label">Cơ sở</label>

                            <div class="col-md-6">
                                @if(Auth::user()->hasRole('admin'))
                                    {{
                                    Form::select('department', $department_list, !empty($request->department)?$request->department:$user->department_id, ['id' => 'department', 'placeholder' => 'Chọn phòng', 'required' => 'required']) }}
                                @else
                                    {{ $user->department }}
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="extension" class="col-md-4 control-label">Số máy lẻ</label>

                            <div class="col-md-6">
                                @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('teleteamleader') || Auth::user()->hasRole('promoterteamleader'))
                                    <input id="extension" type="text" class="form-control" name="extension"
                                           value="{{ $user->extension }}">
                                @else
                                    {{ $user->extension }}
                                @endif

                                @if ($errors->has('extension'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('extension') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        @if(Auth::user()->hasRole('admin'))
                            <div class="form-group">
                                <label for="password-confirm" class="col-md-4 control-label">Active</label>

                                <div class="col-md-6">
                                    {{ Form::checkbox('active', 1, $user->active)}}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="role" class="col-md-4 control-label">Quyền hạn</label>

                                <div class="col-md-6">
                                    @foreach ($role_list as $role)
                                        {{ Form::checkbox('role[]', $role->id, !empty($request->role)?in_array($role->id, $request->role):in_array($role->id, $user_roles)) }} {{ $role->display_name }}
                                        <br/>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Cập nhật
                                </button>
                                <a href="{{ route('user.resetpassword', $user->id) }}" class="btn btn-danger">Reset mật
                                    khẩu</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#password_change').change(function () {
//console.log($('#password').prop('disabled'));
            if ($('#password').prop('disabled')) {
                $('#password').prop('disabled', false);
                $('#password-confirm').prop('disabled', false);
            } else {
                $('#password').prop('disabled', true);
                $('#password-confirm').prop('disabled', true);
                $('#password').val('');
                $('#password-confirm').val('');
            }
        });
    });
</script>