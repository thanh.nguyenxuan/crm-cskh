<?php

namespace App\Http\Controllers\Report;

use App\CallStatus;
use App\Campaign;
use App\CampaignCategory;
use App\Channel;
use App\Group;
use App\Keyword;
use App\Lead;
use App\LeadStatus;
use App\Major;
use App\Province;
use App\Source;
use App\UserPref;
use Illuminate\Http\Request;
use App\Department;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use PHPExcel;
use PHPExcel_IOFactory;

class LeadSourceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request)
    {
        $source_list_data = Source::where('hide', 0)->orderBy('name', 'ASC')->get();
        $source_list = array();
        foreach ($source_list_data as $source_data) {
            $source_list[$source_data->id] = $source_data->name;
        }
        $channel_list = Channel::where('hide', 0)->orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $campaign_list = Campaign::where('hide', 0)->orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $campaign_category_list = CampaignCategory::where('status', 1)->pluck('name', 'id')->toArray();
        $keyword_list = Keyword::where('hide', 0)->orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $province_list_data = Province::orderBy('name', 'ASC')->get();
        $province_list = array('na' => 'Không xác định');
        foreach ($province_list_data as $province_data) {
            $province_list[$province_data->id] = $province_data->name;
        }
        $major_list = array();
        $major_list_data = Major::where('hide', 0)->get();
        foreach ($major_list_data as $major_data) {
            $major_list[$major_data->id] = $major_data->name;
        }
        $status_list = array('' => 'Trạng thái bất kỳ');
        $status_list_data = LeadStatus::where('hide', 0)->orderBy('sort_index', 'ASC')->get();
        foreach ($status_list_data as $status_data) {
            $status_list[$status_data->id] = $status_data->name;
        }
        $call_status_list_data = CallStatus::all();
        foreach ($call_status_list_data as $call_status_data) {
            $call_status_list[$call_status_data->id] = $call_status_data->name;
        }
        $department_list_data = Department::all();
        $department_list = array();

        foreach ($department_list_data as $department_data) {
            $department_list[$department_data->id] = $department_data->name;
        }

        $group_list = array('na' => 'Không thuộc nhóm nào');
        $group_list_data = Group::all();
        foreach ($group_list_data as $group_data) {
            $group_list[$group_data->id] = $group_data->name;
        }

        $user_data_list = User::whereIn('department_id',array_keys($department_list))->orderBy('username', 'ASC')->get();
        foreach ($user_data_list as $user_data) {
            $creator_list[$user_data->id] = $user_data->username . ' - ' . $user_data->full_name;
        }
        if (Auth::user()->hasRole('admin')) {
            $departments_str = implode(',', array_keys($department_list));
            $assignee_list_data = User::whereHas('roles', function ($q) {
                $q->where('name', 'telesales')
                    ->orWhere('name', 'teleteamleader');
            })->whereRaw("(department_id IN ($departments_str)
                    OR id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                    OR id IN (SELECT user_id FROM group_user WHERE group_id IN (
                        SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                    ))
                )")
                ->orderBy('username', 'ASC')->get();
        } elseif (Auth::user()->hasRole('holding')) {
            $departments_str = implode(',', array_keys($department_list));
            $assignee_list_data = User::whereHas('roles', function ($q) {
                $q->where('name', 'telesales')
                    ->orWhere('name', 'teleteamleader');
            })->whereRaw("(department_id IN ($departments_str)
                    OR id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                    OR id IN (SELECT user_id FROM group_user WHERE group_id IN (
                        SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                    ))
                )")
                ->orderBy('username', 'ASC')->get();
        } else {
            $departments_str = implode(',', Auth::user()->getManageDepartments());
            $assignee_list_data = User::whereHas('roles', function ($q) {
                $q->where('name', 'telesales')
                    ->orWhere('name', 'teleteamleader');
            })->whereRaw("(department_id IN ($departments_str)
                    OR id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                    OR id IN (SELECT user_id FROM group_user WHERE group_id IN (
                        SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                    ))
                )")
                ->orderBy('username', 'ASC')->get();
        }
        $assignee_list = array('nobody' => 'Chưa được chia');
        $activator_list = array();
        $caller_list = array();
        $rating_list = array(1 => 'Rất quan tâm', 2 => 'Quan tâm', 3 => 'Ít quan tâm');
        $assignee_tool_list = array();
        foreach ($assignee_list_data as $assignee_data) {
            $assignee_list[$assignee_data->id] = $assignee_data->username . ' - ' . $assignee_data->full_name;
            $assignee_tool_list[$assignee_data->id] = $assignee_data->username . ' - ' . $assignee_data->full_name;
            $caller_list[$assignee_data->id] = $assignee_data->username . ' - ' . $assignee_data->full_name;
            $activator_list[$assignee_data->id] = $assignee_data->username . ' - ' . $assignee_data->full_name;
        }
        $filtering = array();
        $user_pref = UserPref::where(['user_id' => Auth::id(), 'key' => 'filtering_lead_source'])->first();
        if (!empty($user_pref) && !empty($user_pref->value))
            $filtering = json_decode($user_pref->value);
        $trunk_list = DB::table('fptu_user_trunk')
            ->join('fptu_trunk', 'fptu_trunk.id', '=', 'fptu_user_trunk.trunk_id')
            ->where('fptu_user_trunk.user_id', Auth::id())->whereNull('fptu_user_trunk.deleted_at')->pluck('fptu_trunk.display_name', 'fptu_trunk.id');
        $trunk_id = 0;
        if (!empty(Auth::id()))
            $trunk_id = User::find(Auth::id())->trunk_id;

        $view = 'report.lead_source';
        if (Auth::check())
            return view($view, [
                'source_list' => $source_list,
                'campaign_list' => $campaign_list,
                'campaign_category_list' => $campaign_category_list,
                'channel_list' => $channel_list,
                'keyword_list' => $keyword_list,
                'province_list' => $province_list,
                'department_list' => $department_list,
                'group_list' => $group_list,
                'assignee_list' => $assignee_list,
                'activator_list' => $activator_list,
                'creator_list' => $creator_list,
                'assignee_tool_list' => $assignee_tool_list,
                'status_list' => $status_list,
                'call_status_list' => $call_status_list,
                'caller_list' => $caller_list,
                'rating_list' => $rating_list,
                'filtering' => $filtering,
                'trunk_list' => $trunk_list,
                'major_list' => $major_list,
                'trunk_id' => $trunk_id
            ]);
    }


    public function search(Request $request)
    {
        $model = new Lead();
        $data = $model->searchMultiSource();

        $export = Input::get('export');
        if (!empty($export)) {
            $exporter = new Lead\LeadExporter();
            $exporter->export($data['data']);
        }

        echo json_encode($data);
    }


}

