<?php

namespace App\Http\Controllers\Report;

use App\LeadLogStatus;
use App\LeadStatus;
use App\LogSms;
use Illuminate\Http\Request;
use App\Department;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use PHPExcel;
use PHPExcel_IOFactory;

class LogChangeStatusController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $model = new LeadLogStatus();

        $from_date = $request->get('from_date');
        $to_date = $request->get('to_date');
        $department_id = $request->get('department_id');

        if(!empty($from_date))          $model->start_date = $from_date;
        if(!empty($to_date))            $model->end_date = $to_date;
        if(!empty($department_id))      $model->department_id = $department_id;

        $data = $model->search();

        session()->flashInput($request->input());
        return view('report.log_change_status', [
            'data' => $data,
        ]);
    }


    public function export(Request $request)
    {
        $model = new LeadLogStatus();

        $from_date = $request->get('from_date');
        $to_date = $request->get('to_date');
        $department_id = $request->get('department_id');

        if(!empty($from_date))          $model->start_date = $from_date;
        if(!empty($to_date))            $model->end_date = $to_date;
        if(!empty($department_id))      $model->department_id = $department_id;

        $data = $model->search();

        if(!empty($data)){
            $file_name = "Lịch sử thay đổi trạng thái Lead từ $from_date đến $to_date.xlsx";

            $excel = new PHPExcel();
            $i=1;
            $excel->setActiveSheetIndex(0)->setTitle('Log Change Status')
                ->setCellValue("A$i", 'ID')
                ->setCellValue("B$i", 'Lead')
                ->setCellValue("C$i", 'Cơ sở')
                ->setCellValue("D$i", 'Ngày tạo')
                ->setCellValue("E$i", 'Mới')
                ->setCellValue("F$i", 'Đã tư vấn qua Điện Thoại')
                ->setCellValue("G$i", 'Sai thông tin/ Wrong information')
                ->setCellValue("H$i", 'Ko có nhu cầu/ No demand')
                ->setCellValue("I$i", 'Follow-up')
                ->setCellValue("J$i", 'Không chăm sóc nữa/ Dead')
                ->setCellValue("K$i", 'Đã đến trường/ show up')
                ->setCellValue("L$i", 'Đến hội thảo/ show up')
                ->setCellValue("M$i", 'Đặt cọc giữ chỗ/ Waiting list')
                ->setCellValue("N$i", 'Không nhập học')
                ->setCellValue("O$i", 'Nhập học/ NE')
                ->setCellValue("P$i", 'Bé làm khách')
                ->setCellValue("Q$i", 'Tiềm năng/ Potential')
                ->setCellValue("R$i", 'Không nghe máy')
            ;
            $departments = Department::all()->pluck('name','id')->toArray();
            foreach ($data as $lead_id => $lead_info){
                $i++;
                $excel->setActiveSheetIndex(0)
                    ->setCellValue("A$i", $lead_id)
                    ->setCellValue("B$i", $lead_info['lead']->name)
                    ->setCellValue("C$i", $departments[$lead_info['lead']->department_id])
                    ->setCellValue("D$i", !empty($lead_info['lead']->created_at) ? date('d/m/Y H:i:s', strtotime($lead_info['lead']->created_at)) : '')
                    ->setCellValue("E$i", !empty($lead_info['log_status'][1])  ? date('d/m/Y H:i:s', strtotime($lead_info['log_status'][1]))  : '')
                    ->setCellValue("F$i", !empty($lead_info['log_status'][2])  ? date('d/m/Y H:i:s', strtotime($lead_info['log_status'][2]))  : '')
                    ->setCellValue("G$i", !empty($lead_info['log_status'][3])  ? date('d/m/Y H:i:s', strtotime($lead_info['log_status'][3]))  : '')
                    ->setCellValue("H$i", !empty($lead_info['log_status'][4])  ? date('d/m/Y H:i:s', strtotime($lead_info['log_status'][4]))  : '')
                    ->setCellValue("I$i", !empty($lead_info['log_status'][5])  ? date('d/m/Y H:i:s', strtotime($lead_info['log_status'][5]))  : '')
                    ->setCellValue("J$i", !empty($lead_info['log_status'][6])  ? date('d/m/Y H:i:s', strtotime($lead_info['log_status'][6]))  : '')
                    ->setCellValue("K$i", !empty($lead_info['log_status'][7])  ? date('d/m/Y H:i:s', strtotime($lead_info['log_status'][7]))  : '')
                    ->setCellValue("L$i", !empty($lead_info['log_status'][8])  ? date('d/m/Y H:i:s', strtotime($lead_info['log_status'][8]))  : '')
                    ->setCellValue("M$i", !empty($lead_info['log_status'][9])  ? date('d/m/Y H:i:s', strtotime($lead_info['log_status'][9]))  : '')
                    ->setCellValue("N$i", !empty($lead_info['log_status'][10]) ? date('d/m/Y H:i:s', strtotime($lead_info['log_status'][14])) : '')
                    ->setCellValue("O$i", !empty($lead_info['log_status'][11]) ? date('d/m/Y H:i:s', strtotime($lead_info['log_status'][15])) : '')
                    ->setCellValue("P$i", !empty($lead_info['log_status'][12]) ? date('d/m/Y H:i:s', strtotime($lead_info['log_status'][16])) : '')
                    ->setCellValue("Q$i", !empty($lead_info['log_status'][13]) ? date('d/m/Y H:i:s', strtotime($lead_info['log_status'][17])) : '')
                    ->setCellValue("R$i", !empty($lead_info['log_status'][14]) ? date('d/m/Y H:i:s', strtotime($lead_info['log_status'][18])) : '')
                ;
            }

            header("Content-Type: application/vnd.ms-excel");
            header("Content-Disposition: attachment;filename=$file_name");
            header("Cache-Control: max-age=0");
            $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
            $writer->save('php://output');
        }else{
            die('Không có dữ liệu');
        }
    }

}
