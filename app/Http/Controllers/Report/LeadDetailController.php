<?php

namespace App\Http\Controllers\Report;

use App\Department;
use App\Http\Controllers\Controller;
use App\Lead;
use App\Report\LeadDetail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PHPExcel;
use PHPExcel_IOFactory;

class LeadDetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function smis(Request $request)
    {
        $url = env('APP_URL').'/report/leadDetail/smis';
        $url_params = array();

        $data = null;
        $current_page = (!empty($request->page) && intval($request->page) > 0) ? intval($request->page) : 1;
        $limit = 10;
        $offset = $limit*($current_page-1);
        $total = 0;
        $total_page = 0;

        if(!empty($request->from_date) && !empty($request->to_date)){
            $data = array();

            $url_params['from_date'] = $request->from_date;
            $url_params['to_date'] = $request->to_date;

            $from_date = date('Y-m-d', strtotime(str_replace('/','-',$request->from_date))) . ' 00:00:00';
            $to_date = date('Y-m-d', strtotime(str_replace('/','-',$request->to_date))) . ' 23:59:59';

            $query = DB::table('fptu_lead AS t')->whereNull('deleted_at')->whereRaw("created_at >= '$from_date' AND created_at <= '$to_date'");
            $query->select("t.*",
                DB::raw("CASE WHEN t.phone1 != '' AND t.phone1 IS NOT NULL THEN t.phone1 ELSE t.phone2 END AS phone"),
                DB::raw("(SELECT s.name FROM fptu_source s WHERE s.id = (SELECT ls.source_id FROM fptu_lead_source ls WHERE ls.lead_id = t.id AND ls.deleted_at IS NULL LIMIT 1)) AS source"),
                DB::raw("(SELECT c.name FROM fptu_campaign_category c WHERE c.id = (SELECT category_id FROM fptu_campaign_category_mapping WHERE campaign_id = (SELECT campaign_id FROM fptu_lead_campaign lc WHERE lc.lead_id = t.id AND lc.deleted_at IS NULL LIMIT 1))) AS campaign_category"),
                DB::raw("(SELECT la.user_id FROM fptu_lead_assignment la WHERE la.lead_id = t.id AND la.deleted_at IS NULL ORDER BY la.id DESC LIMIT 1) as assignee_id"),
                DB::raw("(SELECT la.user_id FROM fptu_lead_assignment la WHERE la.lead_id = t.id ORDER BY la.id DESC LIMIT 1 OFFSET 1) as old_assignee_id")
            );

            if(!empty($request->department_id)){
                $query->where('department_id', $request->department_id);
                $url_params['department_id'] = $request->department_id;
            }else{
                $query->whereIn('department_id', Department::getListDepartmentId('SMIS'));
            }
            if(!empty($request->assignee_id)){
                $query->whereRaw("id IN (SELECT lead_id FROM fptu_lead_assignment WHERE deleted_at IS NULL AND user_id = '$request->assignee_id')");
                $url_params['assignee_id'] = $request->assignee_id;
            }

            $total = $query->count();
            $total_page = ceil($total/$limit);
            $query->offset($offset);
            $query->limit($limit);
            $data = $query->get();
        }

        session()->flashInput($request->input());

        return view('report.lead_detail.smis', array(
            'data' => $data,
            'total' => $total,
            'limit' => $limit,
            'offset' => $offset,
            'pagination_total_page' => $total_page,
            'pagination_current_page' => $current_page,
            'pagination_url' => $url,
            'pagination_url_params' => $url_params,
        ));

    }

    public function exportSMIS(Request $request)
    {
        if(!empty($request->from_date) && !empty($request->to_date)){
            $from_date = date('Y-m-d', strtotime(str_replace('/','-',$request->from_date))) . ' 00:00:00';
            $to_date = date('Y-m-d', strtotime(str_replace('/','-',$request->to_date))) . ' 23:59:59';

            $query = DB::table('fptu_lead AS t')->whereNull('deleted_at')->whereRaw("created_at >= '$from_date' AND created_at <= '$to_date'");
            $query->select("t.*",
                DB::raw("CASE WHEN t.phone1 != '' AND t.phone1 IS NOT NULL THEN t.phone1 ELSE t.phone2 END AS phone"),
                DB::raw("(SELECT s.name FROM fptu_source s WHERE s.id = (SELECT ls.source_id FROM fptu_lead_source ls WHERE ls.lead_id = t.id AND ls.deleted_at IS NULL LIMIT 1)) AS source"),
                DB::raw("(SELECT c.name FROM fptu_campaign_category c WHERE c.id = (SELECT category_id FROM fptu_campaign_category_mapping WHERE campaign_id = (SELECT campaign_id FROM fptu_lead_campaign lc WHERE lc.lead_id = t.id AND lc.deleted_at IS NULL LIMIT 1))) AS campaign_category"),
                DB::raw("(SELECT la.user_id FROM fptu_lead_assignment la WHERE la.lead_id = t.id AND la.deleted_at IS NULL ORDER BY la.id DESC LIMIT 1) as assignee_id"),
                DB::raw("(SELECT la.user_id FROM fptu_lead_assignment la WHERE la.lead_id = t.id ORDER BY la.id DESC LIMIT 1 OFFSET 1) as old_assignee_id")
            );

            if(!empty($request->department_id)){
                $query->where('department_id', $request->department_id);
            }else{
                $query->whereIn('department_id', Department::getListDepartmentId('SMIS'));
            }
            if(!empty($request->assignee_id)){
                $query->whereRaw("id IN (SELECT lead_id FROM fptu_lead_assignment WHERE deleted_at IS NULL AND user_id = '$request->assignee_id')");
            }
            $data = $query->get();

            if(!empty($data)){
                $file_name = "Báo cáo chi tiết Lead - SMIS từ ".$request->from_date." đến ".$request->to_date." - ".date('YmdHis').".xlsx";

                $excel = new PHPExcel();
                $excel->setActiveSheetIndex(0)->setTitle('Report Lead - SMIS')
                    ->setCellValue("A1", 'Stt')->mergeCells('A1:A2')
                    ->setCellValue('B1', 'Lead')->mergeCells('B1:P1')
                    ->setCellValue('B2', 'ID')
                    ->setCellValue('C2', 'Họ và tên PHHS')
                    ->setCellValue('D2', 'Họ và tên HS')
                    ->setCellValue('E2', 'Số điện thoại')
                    ->setCellValue('F2', 'Ngày sinh')
                    ->setCellValue('G2', 'Nhóm lớp')
                    ->setCellValue('H2', 'Hệ học mong muốn')
                    ->setCellValue('I2', 'Trạng thái Lead')
                    ->setCellValue('J2', 'Nguồn dẫn')
                    ->setCellValue('K2', 'Nhóm chiến dịch')
                    ->setCellValue('L2', 'Người tạo')
                    ->setCellValue('M2', 'Ngày tạo')
                    ->setCellValue('N2', 'Tháng tạo')
                    ->setCellValue('O2', 'Người phụ trách hiện tại')
                    ->setCellValue('P2', 'Người phụ trách trước')
                    ->setCellValue('Q1', 'Showup')->mergeCells('Q1:R1')
                    ->setCellValue('Q2', 'Ngày show up đến trường')
                    ->setCellValue('R2', 'Tháng show up đến trường')

                    ->setCellValue('S1', 'Waiting List')->mergeCells('S1:X1')
                    ->setCellValue('S2', 'Ngày giữ chỗ')
                    ->setCellValue('T2', 'Tháng giữ chỗ')
                    ->setCellValue('U2', 'Nhóm lớp giữ chỗ')
                    ->setCellValue('V2', 'Hệ học giữ chỗ')
                    ->setCellValue('W2', 'Phiếu thu tiền/báo có giữ chỗ (Có/không)')
                    ->setCellValue('X2', 'Mã học sinh')

                    ->setCellValue('Y1', 'NE')->mergeCells('Y1:AJ1')
                    ->setCellValue('Y2', 'Ngày tham gia CT bé làm khách')
                    ->setCellValue('Z2', 'Tháng tham gia CT bé làm khách')
                    ->setCellValue('AA2', 'Lớp học CT Bé làm khách')
                    ->setCellValue('AB2', 'Nhóm lớp CT bé làm khách')
                    ->setCellValue('AC2', 'Hệ học CT Bé làm khách')
                    ->setCellValue('AD2', 'Phiếu thu tiền/báo có Phí CT bé làm khách (Có/không)')
                    ->setCellValue('AE2', 'Ngày nhập học chính thức')
                    ->setCellValue('AF2', 'Tháng nhập học')
                    ->setCellValue('AG2', 'Tên lớp nhập học chính thức')
                    ->setCellValue('AH2', 'Nhóm lớp chính thức')
                    ->setCellValue('AI2', 'Hệ học chính thức')
                    ->setCellValue('AJ2', 'Phiếu thu tiền/báo hoàn thành CS tài chính (Có/không)')

                    ->getStyle('A1:AJ2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                ;

                $statusListData = \App\LeadStatus::all()->pluck('name', 'id')->toArray();
                $userListData = \App\User::all()->pluck('username', 'id')->toArray();
                $desireClassListData = \App\Major::all()->pluck('name', 'id')->toArray();
                $guestClassListData = \App\GuestClass::all()->pluck('name', 'id')->toArray();
                $officialClassListData = \App\OfficialClass::all()->pluck('name', 'id')->toArray();
                $learningSystemDesireListData = \App\LearningSystemDesire::all()->pluck('name', 'id')->toArray();
                $learningSystemGuestListData = \App\LearningSystemGuest::all()->pluck('name', 'id')->toArray();
                $learningSystemOfficialListData = \App\LearningSystemOfficial::all()->pluck('name', 'id')->toArray();

                $i = 3;
                $stt = 0;
                foreach ($data as $item){
                    $excel->getActiveSheet()
                        ->setCellValue("A$i", ++$stt)
                        ->setCellValue("B$i", $item->id)
                        ->setCellValue("C$i", $item->name)
                        ->setCellValue("D$i", $item->student_name)
                        ->setCellValue("E$i", $item->phone1)
                        ->setCellValue("F$i", (!empty($item->birthdate) ? date('d/m/Y', strtotime($item->birthdate)) : ''))
                        ->setCellValue("G$i", (!empty($item->major) ? $desireClassListData[$item->major] : ''))
                        ->setCellValue("H$i", (!empty($item->learning_system_desire) ? $learningSystemDesireListData[$item->learning_system_desire] : ''))
                        ->setCellValue("I$i", (!empty($item->status) ? $statusListData[$item->status] : ''))
                        ->setCellValue("J$i", $item->source)
                        ->setCellValue("K$i", $item->campaign_category)
                        ->setCellValue("L$i", (!empty($userListData[$item->created_by]) ? $userListData[$item->created_by] : ''))
                        ->setCellValue("M$i", (!empty($item->created_at) ? date('d/m/Y', strtotime($item->created_at)) : ''))
                        ->setCellValue("N$i", (!empty($item->created_at) ? date('m', strtotime($item->created_at)) : ''))
                        ->setCellValue("O$i", (!empty($userListData[$item->assignee_id]) ? $userListData[$item->assignee_id] : ''))
                        ->setCellValue("P$i", (!empty($userListData[$item->old_assignee_id]) ? $userListData[$item->old_assignee_id] : ''))
                        ->setCellValue("Q$i", (!empty($item->showup_school_at) ? date('d/m/Y', strtotime($item->showup_school_at)) : ''))
                        ->setCellValue("R$i", (!empty($item->showup_school_at) ? date('m', strtotime($item->showup_school_at)) : ''))
                        ->setCellValue("S$i", (!empty($item->activated_at) ? date('d/m/Y', strtotime($item->activated_at)) : ''))
                        ->setCellValue("T$i", (!empty($item->activated_at) ? date('m', strtotime($item->activated_at)) : ''))
                        ->setCellValue("U$i", (!empty($item->waitlist_class) ? $officialClassListData[$item->waitlist_class] : ''))
                        ->setCellValue("V$i", (!empty($item->learning_system_waitlist) ? $learningSystemOfficialListData[$item->learning_system_waitlist] : ''))
                        ->setCellValue("W$i", $item->status_waitlist_number)
                        ->setCellValue("X$i", $item->student_code)
                        ->setCellValue("Y$i", (!empty($item->join_as_guest_at) ? date('d/m/Y', strtotime($item->join_as_guest_at)) : ''))
                        ->setCellValue("Z$i", (!empty($item->join_as_guest_at) ? date('m', strtotime($item->join_as_guest_at)) : ''))
                        ->setCellValue("AA$i", $item->guest_class_name)
                        ->setCellValue("AB$i", (!empty($item->guest_class) ? $guestClassListData[$item->guest_class] : ''))
                        ->setCellValue("AC$i", (!empty($item->learning_system_guest) ? $learningSystemGuestListData[$item->learning_system_guest] : ''))
                        ->setCellValue("AD$i", $item->status_guest_number)
                        ->setCellValue("AE$i", (!empty($item->enrolled_at) ? date('d/m/Y', strtotime($item->enrolled_at)) : ''))
                        ->setCellValue("AF$i", (!empty($item->enrolled_at) ? date('m', strtotime($item->enrolled_at)) : ''))
                        ->setCellValue("AG$i", $item->class_name)
                        ->setCellValue("AH$i", (!empty($item->official_class) ? $officialClassListData[$item->official_class] : ''))
                        ->setCellValue("AI$i", (!empty($item->learning_system_official) ? $learningSystemOfficialListData[$item->learning_system_official] : ''))
                        ->setCellValue("AJ$i", $item->receipt_number)
                    ;
                    $i++;
                }

                header("Content-Type: application/vnd.ms-excel");
                header("Content-Disposition: attachment;filename=$file_name");
                header("Cache-Control: max-age=0");
                $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
                $writer->save('php://output');
            }else{
                die('Không có dữ liệu');
            }
        }else{
            die('Thiếu dữ liệu truyền vào');
        }
    }


    public function gis(Request $request)
    {
        $url = env('APP_URL').'/report/leadDetail/gis';
        $url_params = array();

        $data = null;
        $current_page = (!empty($request->page) && intval($request->page) > 0) ? intval($request->page) : 1;
        $limit = 10;
        $offset = $limit*($current_page-1);
        $total = 0;
        $total_page = 0;

        if(!empty($request->from_date) && !empty($request->to_date)){
            $data = array();

            $url_params['from_date'] = $request->from_date;
            $url_params['to_date'] = $request->to_date;

            $from_date = date('Y-m-d', strtotime(str_replace('/','-',$request->from_date))) . ' 00:00:00';
            $to_date = date('Y-m-d', strtotime(str_replace('/','-',$request->to_date))) . ' 23:59:59';

            $query = DB::table('fptu_lead AS t')->whereNull('deleted_at')->whereRaw("created_at >= '$from_date' AND created_at <= '$to_date'");
            $query->select("t.*",
                DB::raw("CASE WHEN t.phone1 != '' AND t.phone1 IS NOT NULL THEN t.phone1 ELSE t.phone2 END AS phone"),
                DB::raw("(SELECT s.name FROM fptu_source s WHERE s.id = (SELECT ls.source_id FROM fptu_lead_source ls WHERE ls.lead_id = t.id AND ls.deleted_at IS NULL LIMIT 1)) AS source"),
                DB::raw("CASE WHEN t.utm_campaign IS NOT NULL AND t.utm_campaign != '' THEN t.utm_campaign ELSE (SELECT c.name FROM fptu_campaign c WHERE c.id = (SELECT campaign_id FROM fptu_lead_campaign lc WHERE lc.lead_id = t.id AND lc.deleted_at IS NULL LIMIT 1)) END AS campaign"),
                DB::raw("(SELECT la.user_id FROM fptu_lead_assignment la WHERE la.lead_id = t.id AND la.deleted_at IS NULL ORDER BY la.id DESC LIMIT 1) as assignee_id"),
                DB::raw("(SELECT la.user_id FROM fptu_lead_assignment la WHERE la.lead_id = t.id ORDER BY la.id DESC LIMIT 1 OFFSET 1) as old_assignee_id")
            );

            if(!empty($request->department_id)){
                $query->where('department_id', $request->department_id);
                $url_params['department_id'] = $request->department_id;
            }else{
                $query->whereIn('department_id', Department::getListDepartmentId('GIS'));
            }
            if(!empty($request->assignee_id)){
                $query->whereRaw("id IN (SELECT lead_id FROM fptu_lead_assignment WHERE deleted_at IS NULL AND user_id = '$request->assignee_id')");
                $url_params['assignee_id'] = $request->assignee_id;
            }

            $total = $query->count();
            $total_page = ceil($total/$limit);
            $query->offset($offset);
            $query->limit($limit);
            $data = $query->get();
        }

        session()->flashInput($request->input());

        return view('report.lead_detail.gis', array(
            'data' => $data,
            'total' => $total,
            'limit' => $limit,
            'offset' => $offset,
            'pagination_total_page' => $total_page,
            'pagination_current_page' => $current_page,
            'pagination_url' => $url,
            'pagination_url_params' => $url_params,
        ));
    }

    public function exportGIS(Request $request)
    {
        if(!empty($request->from_date) && !empty($request->to_date)){
            $from_date = date('Y-m-d', strtotime(str_replace('/','-',$request->from_date))) . ' 00:00:00';
            $to_date = date('Y-m-d', strtotime(str_replace('/','-',$request->to_date))) . ' 23:59:59';

            $query = DB::table('fptu_lead AS t')->whereNull('deleted_at')->whereRaw("created_at >= '$from_date' AND created_at <= '$to_date'");
            $query->select("t.*",
                DB::raw("CASE WHEN t.phone1 != '' AND t.phone1 IS NOT NULL THEN t.phone1 ELSE t.phone2 END AS phone"),
                DB::raw("(SELECT s.name FROM fptu_source s WHERE s.id = (SELECT ls.source_id FROM fptu_lead_source ls WHERE ls.lead_id = t.id AND ls.deleted_at IS NULL LIMIT 1)) AS source"),
                DB::raw("CASE WHEN t.utm_campaign IS NOT NULL AND t.utm_campaign != '' THEN t.utm_campaign ELSE (SELECT c.name FROM fptu_campaign c WHERE c.id = (SELECT campaign_id FROM fptu_lead_campaign lc WHERE lc.lead_id = t.id AND lc.deleted_at IS NULL LIMIT 1)) END AS campaign"),
                DB::raw("(SELECT la.user_id FROM fptu_lead_assignment la WHERE la.lead_id = t.id AND la.deleted_at IS NULL ORDER BY la.id DESC LIMIT 1) as assignee_id"),
                DB::raw("(SELECT la.user_id FROM fptu_lead_assignment la WHERE la.lead_id = t.id ORDER BY la.id DESC LIMIT 1 OFFSET 1) as old_assignee_id")
            );

            if(!empty($request->department_id)){
                $query->where('department_id', $request->department_id);
            }else{
                $query->whereIn('department_id', Department::getListDepartmentId('GIS'));
            }
            if(!empty($request->assignee_id)){
                $query->whereRaw("id IN (SELECT lead_id FROM fptu_lead_assignment WHERE deleted_at IS NULL AND user_id = '$request->assignee_id')");
            }
            $data = $query->get();

            if(!empty($data)){
                $file_name = "Báo cáo chi tiết Lead - GIS từ ".$request->from_date." đến ".$request->to_date." - ".date('YmdHis').".xlsx";

                $excel = new PHPExcel();
                $excel->setActiveSheetIndex(0)->setTitle('Report Lead - GIS')
                    ->setCellValue("A1", 'Stt')->mergeCells('A1:A2')
                    ->setCellValue('B1', 'Lead')->mergeCells('B1:R1')
                    ->setCellValue('B2', 'ID')
                    ->setCellValue('C2', 'Họ và tên PHHS')
                    ->setCellValue('D2', 'Họ và tên HS')
                    ->setCellValue('E2', 'Số điện thoại')
                    ->setCellValue('F2', 'Ngày sinh')
                    ->setCellValue('G2', 'Hệ học')
                    ->setCellValue('H2', 'Cấp học')
                    ->setCellValue('I2', 'Khối')
                    ->setCellValue('J2', 'Loại Lead')
                    ->setCellValue('K2', 'Trạng thái Lead')
                    ->setCellValue('L2', 'Nguồn dẫn')
                    ->setCellValue('M2', 'Chiến dịch')
                    ->setCellValue('N2', 'Người tạo')
                    ->setCellValue('O2', 'Ngày tạo')
                    ->setCellValue('P2', 'Tháng tạo')
                    ->setCellValue('Q2', 'Người phụ trách hiện tại')
                    ->setCellValue('R2', 'Người phụ trách trước')
                    ->setCellValue('S1', 'Showup')->mergeCells('S1:T1')
                    ->setCellValue('S2', 'Ngày')
                    ->setCellValue('T2', 'Tháng')
                    ->setCellValue('U1', 'Waiting List')->mergeCells('U1:X1')
                    ->setCellValue('U2', 'Ngày')
                    ->setCellValue('V2', 'Tháng')
                    ->setCellValue('W2', 'Số phiếu thu tiền/báo có giữ chỗ')
                    ->setCellValue('X2', 'Mã học sinh')
                    ->setCellValue('Y1', 'NS')->mergeCells('Y1:AA1')
                    ->setCellValue('Y2', 'Ngày')
                    ->setCellValue('Z2', 'Tháng')
                    ->setCellValue('AA2', 'Số liên lai/báo có')
                    ->setCellValue('AB1', 'NE')->mergeCells('AB1:AE1')
                    ->setCellValue('AB2', 'Ngày')
                    ->setCellValue('AC2', 'Tháng')
                    ->setCellValue('AD2', 'Tên lớp nhập học chính thức')
                    ->setCellValue('AE2', 'Khối lớp nhập học chính thức')
                    ->getStyle('A1:AE2')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
                ;

                $statusListData = \App\LeadStatus::all()->pluck('name', 'id')->toArray();
                $userListData = \App\User::all()->pluck('username', 'id')->toArray();
                $leadTypeListData = \App\LeadType::all()->pluck('name', 'id')->toArray();
                $desireClassListData = \App\Major::all()->pluck('name', 'id')->toArray();
                $officialClassListData = \App\OfficialClass::all()->pluck('name', 'id')->toArray();
                $learningSystemDesireListData = \App\LearningSystemDesire::all()->pluck('name', 'id')->toArray();

                $i = 3;
                $stt = 0;
                foreach ($data as $item){
                    $excel->getActiveSheet()
                        ->setCellValue("A$i", ++$stt)
                        ->setCellValue("B$i", $item->id)
                        ->setCellValue("C$i", $item->name)
                        ->setCellValue("D$i", $item->student_name)
                        ->setCellValue("E$i", $item->phone)
                        ->setCellValue("F$i", (!empty($item->birthdate) ? date('d/m/Y', strtotime($item->birthdate)) : ''))
                        ->setCellValue("G$i", (!empty($item->learning_system_desire) ? $learningSystemDesireListData[$item->learning_system_desire] : ''))
                        ->setCellValue("H$i", (!empty($item->major) ? \App\Major::getSchoolLevel($item->major) : ''))
                        ->setCellValue("I$i", (!empty($item->major) ? $desireClassListData[$item->major] : ''))
                        ->setCellValue("J$i", (!empty($item->lead_type) ? $leadTypeListData[$item->lead_type] : ''))
                        ->setCellValue("K$i", (!empty($item->status) ? $statusListData[$item->status] : ''))
                        ->setCellValue("L$i", $item->source)
                        ->setCellValue("M$i", $item->campaign)
                        ->setCellValue("N$i", (!empty($userListData[$item->created_by]) ? $userListData[$item->created_by] : ''))
                        ->setCellValue("O$i", (!empty($item->created_at) ? date('d/m/Y', strtotime($item->created_at)) : ''))
                        ->setCellValue("P$i", (!empty($item->created_at) ? date('m', strtotime($item->created_at)) : ''))
                        ->setCellValue("Q$i", (!empty($userListData[$item->assignee_id]) ? $userListData[$item->assignee_id] : ''))
                        ->setCellValue("R$i", (!empty($userListData[$item->old_assignee_id]) ? $userListData[$item->old_assignee_id] : ''))
                        ->setCellValue("S$i", (!empty($item->showup_school_at) ? date('d/m/Y', strtotime($item->showup_school_at)) : ''))
                        ->setCellValue("T$i", (!empty($item->showup_school_at) ? date('m', strtotime($item->showup_school_at)) : ''))
                        ->setCellValue("U$i", (!empty($item->activated_at) ? date('d/m/Y', strtotime($item->activated_at)) : ''))
                        ->setCellValue("V$i", (!empty($item->activated_at) ? date('m', strtotime($item->activated_at)) : ''))
                        ->setCellValue("W$i", $item->status_waitlist_number)
                        ->setCellValue("X$i", $item->student_code)
                        ->setCellValue("Y$i", (!empty($item->new_sale_at) ? date('d/m/Y', strtotime($item->new_sale_at)) : ''))
                        ->setCellValue("Z$i", (!empty($item->new_sale_at) ? date('m', strtotime($item->new_sale_at)) : ''))
                        ->setCellValue("AA$i",$item->receipt_number)
                        ->setCellValue("AB$i",(!empty($item->enrolled_at) ? date('d/m/Y', strtotime($item->enrolled_at)) : ''))
                        ->setCellValue("AC$i",(!empty($item->enrolled_at) ? date('m', strtotime($item->enrolled_at)) : ''))
                        ->setCellValue("AD$i",$item->class_name)
                        ->setCellValue("AE$i",(!empty($item->official_class) ? $officialClassListData[$item->official_class] : ''))
                    ;
                    $i++;
                }

                header("Content-Type: application/vnd.ms-excel");
                header("Content-Disposition: attachment;filename=$file_name");
                header("Cache-Control: max-age=0");
                $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
                $writer->save('php://output');
            }else{
                die('Không có dữ liệu');
            }
        }else{
            die('Thiếu dữ liệu truyền vào');
        }
    }

    public function getUserByDepartment(Request $request){
        $department_id = $request->get('department_id');
        $type = $request->get('type');
        if($type == 'SMIS'){
            echo User::getOptionsUserSMIS(null, $department_id);
        }else if($type == 'GIS'){
            echo User::getOptionsUserGIS(null, $department_id);
        }
    }

}