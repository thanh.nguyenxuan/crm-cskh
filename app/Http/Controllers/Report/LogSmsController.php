<?php

namespace App\Http\Controllers\Report;

use App\LogSms;
use Illuminate\Http\Request;
use App\Department;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LogSmsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $model = new LogSms();

        $from_date = $request->get('from_date');
        $to_date = $request->get('to_date');
        $type = $request->get('type');
        $status = $request->get('status');
        $department_id = $request->get('department_id');
        $department_level = $request->get('department_level');
        $user_id = $request->get('user_id');
        $role_id = $request->get('role_id');
        $lead_id = $request->get('lead_id');
        $brand_name = $request->get('brand_name');

        if(!empty($from_date))          $model->start_date = $from_date;
        if(!empty($to_date))            $model->end_date = $to_date;
        if(!empty($type))               $model->type = $type;
        if(!empty($status))             $model->status = $status;
        if(!empty($department_id))      $model->department_id = $department_id;
        if(!empty($department_level))   $model->department_level = $department_level;
        if(!empty($user_id))            $model->user_id = $user_id;
        if(!empty($role_id))            $model->role_id = $role_id;
        if(!empty($lead_id))            $model->lead_id = $lead_id;
        if(!empty($brand_name))         $model->brand_name = $brand_name;

        $data = $model->search();

        session()->flashInput($request->input());
        return view('report.log_sms', [
            'data' => $data,
        ]);
    }


    public function detail(Request $request)
    {
        $return = array(
            'success' => TRUE,
            'message' => '',
            'data' => array()
        );
        $log_sms = LogSms::find($request->sms_id);
        if($log_sms){
            if(!empty($log_sms->lead_id)){
                $return['data']['lead_ids'] = explode(',',$log_sms->lead_id);
            }
        }

        return json_encode($return);
    }


}
