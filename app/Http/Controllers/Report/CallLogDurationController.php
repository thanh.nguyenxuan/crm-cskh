<?php

namespace App\Http\Controllers\Report;

use App\Department;
use App\Http\Controllers\Controller;
use App\Lead\CallLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PHPExcel;
use PHPExcel_IOFactory;

class CallLogDurationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request)
    {
        $data = null;

        if(!empty($request->from_date) && !empty($request->to_date)){

            $from_date = date('Y-m-d', strtotime(str_replace('/','-',$request->from_date))) . ' 00:00:00';
            $to_date = date('Y-m-d', strtotime(str_replace('/','-',$request->to_date))) . ' 23:59:59';

            $query = DB::table('edf_call_log AS t')
                ->select("t.*")
                ->leftJoin('fptu_user AS u', 't.created_by', '=', 'u.id')
                ->whereNull('t.deleted_at')
                ->whereRaw("t.created_at >= '$from_date' AND t.created_at <= '$to_date'")
            ;

            if (!in_array(Department::HOLDING, Auth::user()->getManageDepartments()) && !Auth::user()->hasRole('holding')) {
                $departments_str = implode(',', Auth::user()->getManageDepartments());
                $query->whereRaw("(u.department_id IN ($departments_str)
                    OR u.id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                    OR u.id IN (SELECT user_id FROM group_user WHERE group_id IN (
                        SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                    ))
                )");
            }else{
                if(!empty($request->department_group)){
                    $departments_str = implode(',', Department::getListDepartmentId($request->department_group));
                    $query->whereRaw("(u.department_id IN ($departments_str)
                        OR u.id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                        OR u.id IN (SELECT user_id FROM group_user WHERE group_id IN (
                            SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                        ))
                    )");
                }
            }
            if(!empty($request->department_ids)){
                $departments_str = implode(',', $request->department_ids);
                $query->whereRaw("(u.department_id IN ($departments_str)
                    OR u.id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                    OR u.id IN (SELECT user_id FROM group_user WHERE group_id IN (
                        SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                    ))
                )");
            }
            if(!empty($request->assignee_id)){
                $query->where('t.created_by', $request->assignee_id);
            }

            $callLogs = $query->get();

            $init_item_structure = array(
                'success'   => array('total' => 0, 'under_1' => 0, 'between_1_3' => 0, 'above_3' => 0),
                'fail'      => array('total' => 0),
            );

            for($i=0; $i<24; $i++){
                $data[$i] = $init_item_structure;
            }

            foreach ($callLogs as $callLog)
            {
                $index = intval(date('H', strtotime($callLog->created_at)));
                if($callLog->status == CallLog::STATUS_ANSWERED){
                    $data[$index]['success']['total']++;

                    if(!empty($callLog->data)){
                        $callLogData = json_decode($callLog->data);
                        if(!empty($callLogData->talk_time)){
                            if($callLogData->talk_time < 60){
                                $data[$index]['success']['under_1']++;
                            }else if($callLogData->talk_time >= 60 && $callLogData->talk_time <= 180){
                                $data[$index]['success']['between_1_3']++;
                            }else if($callLogData->talk_time > 180){
                                $data[$index]['success']['above_3']++;
                            }
                        }
                    }
                }else{
                    $data[$index]['fail']['total']++;
                }
            }
        }

        session()->flashInput($request->input());
        return view('report.call_log_duration', array(
            'data' => $data,
        ));
    }

    public function export(Request $request)
    {
        $data = null;

        if(!empty($request->from_date) && !empty($request->to_date)){

            $from_date = date('Y-m-d', strtotime(str_replace('/','-',$request->from_date))) . ' 00:00:00';
            $to_date = date('Y-m-d', strtotime(str_replace('/','-',$request->to_date))) . ' 23:59:59';

            $query = DB::table('edf_call_log AS t')
                ->select("t.*")
                ->leftJoin('fptu_user AS u', 't.created_by', '=', 'u.id')
                ->whereNull('t.deleted_at')
                ->whereRaw("t.created_at >= '$from_date' AND t.created_at <= '$to_date'")
            ;

            if (!in_array(Department::HOLDING, Auth::user()->getManageDepartments()) && !Auth::user()->hasRole('holding')) {
                $departments_str = implode(',', Auth::user()->getManageDepartments());
                $query->whereRaw("(u.department_id IN ($departments_str)
                    OR u.id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                    OR u.id IN (SELECT user_id FROM group_user WHERE group_id IN (
                        SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                    ))
                )");
            }else{
                if(!empty($request->department_group)){
                    $departments_str = implode(',', Department::getListDepartmentId($request->department_group));
                    $query->whereRaw("(u.department_id IN ($departments_str)
                        OR u.id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                        OR u.id IN (SELECT user_id FROM group_user WHERE group_id IN (
                            SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                        ))
                    )");
                }
            }
            if(!empty($request->department_ids)){
                $departments_str = implode(',', $request->department_ids);
                $query->whereRaw("(u.department_id IN ($departments_str)
                    OR u.id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                    OR u.id IN (SELECT user_id FROM group_user WHERE group_id IN (
                        SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                    ))
                )");
            }
            if(!empty($request->assignee_id)){
                $query->where('t.created_by', $request->assignee_id);
            }

            $callLogs = $query->get();

            $init_item_structure = array(
                'success'   => array('total' => 0, 'under_1' => 0, 'between_1_3' => 0, 'above_3' => 0),
                'fail'      => array('total' => 0),
            );

            for($i=0; $i<24; $i++){
                $data[$i] = $init_item_structure;
            }

            foreach ($callLogs as $callLog)
            {
                $index = intval(date('H', strtotime($callLog->created_at)));
                if($callLog->status == CallLog::STATUS_ANSWERED){
                    $data[$index]['success']['total']++;

                    if(!empty($callLog->data)){
                        $callLogData = json_decode($callLog->data);
                        if(!empty($callLogData->talk_time)){
                            if($callLogData->talk_time < 60){
                                $data[$index]['success']['under_1']++;
                            }else if($callLogData->talk_time >= 60 && $callLogData->talk_time <= 180){
                                $data[$index]['success']['between_1_3']++;
                            }else if($callLogData->talk_time > 180){
                                $data[$index]['success']['above_3']++;
                            }
                        }
                    }
                }else{
                    $data[$index]['fail']['total']++;
                }
            }

            if(!empty($data)){
                $file_name = date('YmdHis')." Báo cáo chỉ số cuộc gọi từ ".date('Y-m-d', strtotime($from_date))." đến ".date('Y-m-d', strtotime($to_date)).".xlsx";

                $excel = new PHPExcel();
                $i=1;
                $excel->setActiveSheetIndex(0)->setTitle('Call Log Duration')
                    ->setCellValue("A$i", 'Giờ (00:00 -> 59:59)')
                    ->setCellValue("B$i", 'Thành công')
                    ->setCellValue("C$i", 'Dưới 1 phút')
                    ->setCellValue("D$i", 'Từ 1 đến 3 phút')
                    ->setCellValue("E$i", 'Trên 3 phút')
                    ->setCellValue("F$i", 'Thất bại')
                ;
                foreach ($data as $key => $item){
                    $i++;

                    $excel->setActiveSheetIndex(0)
                        ->setCellValueExplicit("A$i", ($key < 10) ? "0$key" : $key)
                        ->setCellValue("B$i", $item['success']['total'])
                        ->setCellValue("C$i", $item['success']['under_1'])
                        ->setCellValue("D$i", $item['success']['between_1_3'])
                        ->setCellValue("E$i", $item['success']['above_3'])
                        ->setCellValue("F$i", $item['fail']['total'])
                    ;
                }

                header("Content-Type: application/vnd.ms-excel");
                header("Content-Disposition: attachment;filename=$file_name");
                header("Cache-Control: max-age=0");
                $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
                $writer->save('php://output');
            }else{
                die('Không có dữ liệu');
            }
        }
    }

}
