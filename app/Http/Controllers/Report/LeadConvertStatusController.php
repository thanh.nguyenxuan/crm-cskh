<?php

namespace App\Http\Controllers\Report;


use App\Account\Status;
use App\Department;
use App\Http\Controllers\Controller;
use App\LeadStatus;
use App\Source;
use App\User;
use App\Utils;
use Illuminate\Http\Request;
use App\Lead;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use PHPExcel;
use PHPExcel_IOFactory;

class LeadConvertStatusController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function getLeads(Request $request)
    {
        if(!empty($request->from_date)
            && !empty($request->to_date)
            && !empty($request->status_old)
            && !empty($request->status_new)
        ) {
            $from_date = date('Y-m-d', strtotime(str_replace('/','-',$request->from_date))) . ' 00:00:00';
            $to_date = date('Y-m-d', strtotime(str_replace('/','-',$request->to_date))) . ' 23:59:59';
            $query = DB::table('fptu_lead AS t')
                ->select('t.*',
                    DB::raw("d.name AS department"),
                    DB::raw("(SELECT user_id FROM fptu_lead_assignment WHERE lead_id = t.id AND deleted_at IS NULL LIMIT 1) AS assignee"),
                    DB::raw("(SELECT source_id FROM fptu_lead_source WHERE lead_id = t.id ORDER BY id ASC LIMIT 1) AS source_id")
                )
                ->leftJoin('fptu_department AS d', 'd.id', '=', 't.department_id')
                ->whereNull('t.deleted_at')
                ->whereRaw("t.created_at >= '$from_date' AND t.created_at <= '$to_date'")
                ->orderBy('d.sort_index', 'ASC')
            ;

            if (!in_array(Department::HOLDING, Auth::user()->getManageDepartments()) && !Auth::user()->hasRole('holding')) {
                $departments_str = implode(',', Auth::user()->getManageDepartments());
                $query->whereRaw("(t.department_id IN ($departments_str)
                    OR t.id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                    OR t.id IN (SELECT user_id FROM group_user WHERE group_id IN (
                        SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                    ))
                )");
            }else{
                if(!empty($request->departments)){
                    $departments_str = implode(',', $request->departments);
                    $query->whereRaw("(t.department_id IN ($departments_str)
                        OR t.id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                        OR t.id IN (SELECT user_id FROM group_user WHERE group_id IN (
                            SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                        ))
                    )");
                }
                if(!empty($request->department_id)){
                    $query->whereRaw("(t.department_id = $request->department_id
                        OR t.id IN (SELECT user_id FROM user_department WHERE department_id = $request->department_id)   
                        OR t.id IN (SELECT user_id FROM group_user WHERE group_id IN (
                            SELECT group_id FROM group_department WHERE department_id = $request->department_id
                        ))
                    )");
                }

            }
            if(!empty($request->assignee_id)){
                $query->whereRaw("t.id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = $request->assignee_id AND deleted_at IS NULL)");
            }
            if(!empty($request->sources)){
                $sources_str = implode(',', $request->sources);
                $query->whereRaw("t.id IN (SELECT lead_id FROM fptu_lead_source ls1 
                    WHERE source_id IN ($sources_str)
                    AND ls1.id = (SELECT min(id) FROM fptu_lead_source ls2 WHERE ls1.lead_id = ls2.lead_id)
                )");
            }
            return $query->get();
        }
        return null;
    }

    protected function convertToData(Request $request, $leads)
    {
        $data = null;
        if(!empty($leads)) {
            $data = array();

            $users = User::all()->pluck('username', 'id')->toArray();

            $date_origin_column = 'created_at';
            $date_convert_column = 'created_at';

            switch ($request->status_old){
                case LeadStatus::STATUS_QUALIFIED:      $date_origin_column = 'qualified_at';       break;
                case LeadStatus::STATUS_HOT_LEAD:       $date_origin_column = 'hot_lead_at';        break;
                case LeadStatus::STATUS_SHOWUP_SCHOOL:  $date_origin_column = 'showup_school_at';   break;
                case LeadStatus::STATUS_WAITLIST:       $date_origin_column = 'activated_at';       break;
                case LeadStatus::STATUS_NEW_SALE:       $date_origin_column = 'new_sale_at';        break;
                case LeadStatus::STATUS_ENROLL:         $date_origin_column = 'enrolled_at';        break;
            }

            switch ($request->status_new){
                case LeadStatus::STATUS_QUALIFIED:      $date_convert_column = 'qualified_at';       break;
                case LeadStatus::STATUS_HOT_LEAD:       $date_convert_column = 'hot_lead_at';       break;
                case LeadStatus::STATUS_SHOWUP_SCHOOL:  $date_convert_column = 'showup_school_at';  break;
                case LeadStatus::STATUS_WAITLIST:       $date_convert_column = 'activated_at';      break;
                case LeadStatus::STATUS_NEW_SALE:       $date_convert_column = 'new_sale_at';       break;
                case LeadStatus::STATUS_ENROLL:         $date_convert_column = 'enrolled_at';       break;
            }

            foreach ($leads as $lead){
                if(empty($lead->$date_origin_column)){
                    continue;
                }

                $datediff = null;
                if(!empty($lead->$date_convert_column)){
                    $datediff = (strtotime($lead->$date_convert_column) - strtotime($lead->$date_origin_column)) / (60*60*24);
                }

                if(!isset($data[$lead->department_id])){
                    $data[$lead->department_id] = array(
                        'name' => $lead->department,
                        'total' => 0,
                        'not_convert' => 0,
                        'convert' => 0,
                        '1_week_convert' => 0,
                        '2_week_convert' => 0,
                        '3_week_convert' => 0,
                        '4_week_convert' => 0,
                        'above_4_week_convert' => 0,
                        '1_month_convert' => 0,
                        '2_month_convert' => 0,
                        '3_month_convert' => 0,
                        '4_month_convert' => 0,
                        'above_4_month_convert' => 0,
                        'detail' => array(),
                    );
                }
                $assignee = !empty($lead->assignee) ? $lead->assignee : $lead->created_by;
                if(!isset($data[$lead->department_id]['detail'][$assignee])){
                    $data[$lead->department_id]['detail'][$assignee] = array(
                        'name' => $users[$assignee],
                        'total' => 0,
                        'not_convert' => 0,
                        'convert' => 0,
                        '1_week_convert' => 0,
                        '2_week_convert' => 0,
                        '3_week_convert' => 0,
                        '4_week_convert' => 0,
                        'above_4_week_convert' => 0,
                        '1_month_convert' => 0,
                        '2_month_convert' => 0,
                        '3_month_convert' => 0,
                        '4_month_convert' => 0,
                        'above_4_month_convert' => 0,
                    );
                }
                $data[$lead->department_id]['total']++;
                $data[$lead->department_id]['detail'][$assignee]['total']++;
                if(isset($datediff)){
                    $data[$lead->department_id]['convert']++;
                    $data[$lead->department_id]['detail'][$assignee]['convert']++;
                    //week
                    if($datediff <= 7){
                        $data[$lead->department_id]['1_week_convert']++;
                        $data[$lead->department_id]['detail'][$assignee]['1_week_convert']++;
                    }
                    if($datediff > 7 && $datediff <= 14){
                        $data[$lead->department_id]['2_week_convert']++;
                        $data[$lead->department_id]['detail'][$assignee]['2_week_convert']++;
                    }
                    if($datediff > 14 && $datediff <= 21){
                        $data[$lead->department_id]['3_week_convert']++;
                        $data[$lead->department_id]['detail'][$assignee]['3_week_convert']++;
                    }
                    if($datediff > 21 && $datediff <= 28){
                        $data[$lead->department_id]['4_week_convert']++;
                        $data[$lead->department_id]['detail'][$assignee]['4_week_convert']++;
                    }
                    if($datediff > 28){
                        $data[$lead->department_id]['above_4_week_convert']++;
                        $data[$lead->department_id]['detail'][$assignee]['above_4_week_convert']++;
                    }
                    //month
                    if($datediff <= 30){
                        $data[$lead->department_id]['1_month_convert']++;
                        $data[$lead->department_id]['detail'][$assignee]['1_month_convert']++;
                    }
                    if($datediff > 30 && $datediff <= 60){
                        $data[$lead->department_id]['2_month_convert']++;
                        $data[$lead->department_id]['detail'][$assignee]['2_month_convert']++;
                    }
                    if($datediff > 60 && $datediff <= 90){
                        $data[$lead->department_id]['3_month_convert']++;
                        $data[$lead->department_id]['detail'][$assignee]['3_month_convert']++;
                    }
                    if($datediff > 90 && $datediff <= 120){
                        $data[$lead->department_id]['4_month_convert']++;
                        $data[$lead->department_id]['detail'][$assignee]['4_month_convert']++;
                    }
                    if($datediff > 120){
                        $data[$lead->department_id]['above_4_month_convert']++;
                        $data[$lead->department_id]['detail'][$assignee]['above_4_month_convert']++;
                    }
                }else{
                    $data[$lead->department_id]['not_convert']++;
                    $data[$lead->department_id]['detail'][$assignee]['not_convert']++;
                }

            }
        }

        return $data;
    }

    public function index(Request $request)
    {
        $lead = $this->getLeads($request);
        $data = $this->convertToData($request , $lead);
        session()->flashInput($request->input());
        return view('report.lead_convert_status', array(
            'data' => $data,
        ));
    }


    public function export(Request $request)
    {
        $leads = $this->getLeads($request);
        $data = $this->convertToData($request , $leads);
        $typeListData = array(
            'week' => 'tuần',
            'month' => 'tháng',
        );
        $users = User::all()->pluck('username', 'id')->toArray();
        $statusListData = LeadStatus::listData();
        $statusListData[LeadStatus::STATUS_QUALIFIED] = LeadStatus::STATUS_QUALIFIED;
        $sourceListData = Source::listData();

        $from_date = date('Y-m-d', strtotime(str_replace('/','-',$request->from_date))) . ' 00:00:00';
        $to_date = date('Y-m-d', strtotime(str_replace('/','-',$request->to_date))) . ' 23:59:59';

        if(!empty($data)){
            $file_name = "Báo cáo chuyển đổi trạng thái Lead "
                .$statusListData[$request->status_old]
                ." - "
                .$statusListData[$request->status_new]
                ." từ $request->from_date to $request->to_date (".$typeListData[$request->type].").xlsx"
            ;

            $excel = new PHPExcel();

            //tổng quan
            $i=1;
            $excel->setActiveSheetIndex(0)
                ->setCellValue("A$i", 'Cơ sở')
                ->setCellValue("B$i", 'TVTS')
                ->setCellValue("C$i", 'Tổng số Lead')
                ->setCellValue("D$i", 'Chưa chuyển đổi')
                ->setCellValue("E$i", 'Đã chuyển đổi')
                ->setCellValue("F$i", 'Chuyển đổi trong 1 '.$typeListData[$request->type])
                ->setCellValue("G$i", 'Chuyển đổi trong 2 '.$typeListData[$request->type])
                ->setCellValue("H$i", 'Chuyển đổi trong 3 '.$typeListData[$request->type])
                ->setCellValue("I$i", 'Chuyển đổi trong 4 '.$typeListData[$request->type])
                ->setCellValue("J$i", 'Chuyển đổi hơn 4 '.$typeListData[$request->type])
            ;
            $excel->getActiveSheet()->setTitle('Tổng quan');
            $excel->getActiveSheet()->getStyle("A$i:J$i")->getFont()->setBold(TRUE);

            foreach ($data as $key => $item){
                if(!empty($item['detail'])){
                    foreach ($item['detail'] as $sub_key => $sub_item){
                        $i++;
                        $excel->setActiveSheetIndex(0)
                            ->setCellValue("A$i", $item['name'])
                            ->setCellValue("B$i", $sub_item['name'])
                            ->setCellValue("C$i", $sub_item['total'])
                            ->setCellValue("D$i", $sub_item['not_convert'])
                            ->setCellValue("E$i", $sub_item['convert'])
                            ->setCellValue("F$i", $sub_item['1_'.$request->type.'_convert'])
                            ->setCellValue("G$i", $sub_item['2_'.$request->type.'_convert'])
                            ->setCellValue("H$i", $sub_item['3_'.$request->type.'_convert'])
                            ->setCellValue("I$i", $sub_item['3_'.$request->type.'_convert'])
                            ->setCellValue("J$i", $sub_item['above_4_'.$request->type.'_convert'])
                        ;
                    }
                }
                $i++;
                $excel->setActiveSheetIndex(0)
                    ->setCellValue("A$i", $item['name'])
                    ->setCellValue("B$i", 'Tổng')
                    ->setCellValue("C$i", $item['total'])
                    ->setCellValue("D$i", $item['not_convert'])
                    ->setCellValue("E$i", $item['convert'])
                    ->setCellValue("F$i", $item['1_'.$request->type.'_convert'])
                    ->setCellValue("G$i", $item['2_'.$request->type.'_convert'])
                    ->setCellValue("H$i", $item['3_'.$request->type.'_convert'])
                    ->setCellValue("I$i", $item['3_'.$request->type.'_convert'])
                    ->setCellValue("J$i", $item['above_4_'.$request->type.'_convert'])
                ;
                $excel->getActiveSheet()->getStyle("A$i:J$i")->getFont()->setBold(TRUE);
            }

            foreach(range('A','J') as $columnID) {
                $excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
            }


            //chi tiết
            $date_origin_column = 'created_at';
            $date_convert_column = 'created_at';
            switch ($request->status_old){
                case LeadStatus::STATUS_QUALIFIED:      $date_origin_column = 'qualified_at';       break;
                case LeadStatus::STATUS_HOT_LEAD:       $date_origin_column = 'hot_lead_at';        break;
                case LeadStatus::STATUS_SHOWUP_SCHOOL:  $date_origin_column = 'showup_school_at';   break;
                case LeadStatus::STATUS_WAITLIST:       $date_origin_column = 'activated_at';       break;
                case LeadStatus::STATUS_NEW_SALE:       $date_origin_column = 'new_sale_at';        break;
                case LeadStatus::STATUS_ENROLL:         $date_origin_column = 'enrolled_at';        break;
            }

            switch ($request->status_new){
                case LeadStatus::STATUS_QUALIFIED:      $date_convert_column = 'qualified_at';      break;
                case LeadStatus::STATUS_HOT_LEAD:       $date_convert_column = 'hot_lead_at';       break;
                case LeadStatus::STATUS_SHOWUP_SCHOOL:  $date_convert_column = 'showup_school_at';  break;
                case LeadStatus::STATUS_WAITLIST:       $date_convert_column = 'activated_at';      break;
                case LeadStatus::STATUS_NEW_SALE:       $date_convert_column = 'new_sale_at';       break;
                case LeadStatus::STATUS_ENROLL:         $date_convert_column = 'enrolled_at';       break;
            }


            $i=1;
            $sheet = $excel->createSheet();
            $excel->setActiveSheetIndex(1)
                ->setCellValue("A$i", 'ID')
                ->setCellValue("B$i", 'Lead')
                ->setCellValue("C$i", 'Cơ sở')
                ->setCellValue("D$i", 'Người phụ trách')
                ->setCellValue("E$i", 'Trạng thái ban đầu')
                ->setCellValue("F$i", 'Thạng thái chuyển đổi')
                ->setCellValue("G$i", 'Thời gian chuyển đổi (ngày)')
                ->setCellValue("H$i", 'Ngày tạo')
                ->setCellValue("I$i", 'Qualified')
                ->setCellValue("J$i", 'Hot Lead')
                ->setCellValue("K$i", 'Show Up (SU)')
                ->setCellValue("L$i", 'Đặt cọc (WL)')
                ->setCellValue("M$i", 'New Sale (NS)')
                ->setCellValue("N$i", 'Nhập học (NE)')
                ->setCellValue("O$i", 'Nguồn dẫn')
            ;
            $excel->getActiveSheet()->setTitle('Chi tiết');
            $excel->getActiveSheet()->getStyle("A$i:O$i")->getFont()->setBold(TRUE);

            foreach ($leads as $lead){
                if(empty($lead->$date_origin_column)){
                    continue;
                }

                $datediff = null;
                if(!empty($lead->$date_convert_column)){
                    $datediff = (strtotime($lead->$date_convert_column) - strtotime($lead->$date_origin_column)) / (60*60*24);
                }

                $i++;
                $excel->setActiveSheetIndex(1)
                    ->setCellValue("A$i", $lead->id)
                    ->setCellValue("B$i", $lead->name)
                    ->setCellValue("C$i", $lead->department)
                    ->setCellValue("D$i", !empty($users[$lead->assignee]) ? $users[$lead->assignee] : '')
                    ->setCellValue("E$i", !empty($statusListData[$request->status_old]) ? $statusListData[$request->status_old] : '')
                    ->setCellValue("F$i", !empty($statusListData[$request->status_new]) ? $statusListData[$request->status_new] : '')
                    ->setCellValue("G$i", isset($datediff) ? round($datediff, 2) : '')
                    ->setCellValue("H$i", $lead->created_at)
                    ->setCellValue("I$i", $lead->qualified_at)
                    ->setCellValue("J$i", $lead->hot_lead_at)
                    ->setCellValue("K$i", $lead->showup_school_at)
                    ->setCellValue("L$i", $lead->activated_at)
                    ->setCellValue("M$i", $lead->new_sale_at)
                    ->setCellValue("N$i", $lead->enrolled_at)
                    ->setCellValue("O$i", !empty($sourceListData[$lead->source_id]) ? $sourceListData[$lead->source_id] : '')
                ;

                foreach(range('A','O') as $columnID) {
                    $excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
                }
            }


            $excel->setActiveSheetIndex(0);

            header("Content-Type: application/vnd.ms-excel");
            header("Content-Disposition: attachment;filename=$file_name");
            header("Cache-Control: max-age=0");
            $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
            $writer->save('php://output');
        }else{
            die('Không có dữ liệu');
        }
    }
}