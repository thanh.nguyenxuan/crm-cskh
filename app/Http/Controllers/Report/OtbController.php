<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use App\Department;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use PHPExcel;
use PHPExcel_IOFactory;

class OtbController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $url = env('APP_URL').'/report/otb';
        $url_params = array();

        $data = null;
        $current_page = (!empty($request->page) && intval($request->page) > 0) ? intval($request->page) : 1;
        $limit = 10;
        $offset = $limit*($current_page-1);
        $total = 0;
        $total_page = 0;

        if(!empty($request->from_date) && !empty($request->to_date)){
            $data = array();
            $conditions = array();

            $conditions[] = "deleted_at IS NULL";

            $from_date = date('Y-m-d', strtotime(str_replace('/','-',$request->from_date))) . ' 00:00:00';
            $conditions[] = "created_at >= '$from_date'";
            $url_params['from_date'] = $request->from_date;

            $to_date = date('Y-m-d', strtotime(str_replace('/','-',$request->to_date))) . ' 23:59:59';
            $conditions[] = "created_at <= '$to_date'";
            $url_params['to_date'] = $request->to_date;

            if(!empty($request->department_id)){
                $conditions[] = "department_id = '$request->department_id'";
                $url_params['department_id'] = $request->department_id;
            }

            if(!empty($request->status)){
                $conditions[] = "status = '$request->status'";
                $url_params['status'] = $request->status;
            }

            $condition = implode(' AND ',$conditions);
            $select = "
                t.id,
                t.name,
                t.student_name,
                CASE WHEN t.phone1 != '' AND t.phone1 IS NOT NULL THEN t.phone1 ELSE t.phone2 END AS phone,
                t.birthdate as birthday,
                CASE WHEN t.major IS NOT NULL AND t.major > 0 THEN (SELECT m.name FROM fptu_major m WHERE m.id = t.major) ELSE NULL END AS class_group_desire,
                CASE WHEN t.learning_system_desire IS NOT NULL AND t.learning_system_desire > 0 THEN (SELECT lsd.name FROM learning_system_desire lsd WHERE lsd.id = t.learning_system_desire) ELSE NULL END AS learning_system_desire,
                (SELECT ls.name FROM fptu_lead_status ls WHERE ls.id = t.status) AS status,
                (SELECT s.name FROM fptu_source s WHERE s.id = (SELECT ls.source_id FROM fptu_lead_source ls WHERE ls.lead_id = t.id AND ls.deleted_at IS NULL LIMIT 1)) AS source,
                CASE WHEN t.utm_campaign IS NOT NULL AND t.utm_campaign != '' THEN utm_campaign ELSE (SELECT c.name FROM fptu_campaign c WHERE c.id = (SELECT campaign_id FROM fptu_lead_campaign lc WHERE lc.lead_id = t.id AND lc.deleted_at IS NULL LIMIT 1)) END AS campaign,
                (SELECT u.username FROM fptu_user u WHERE u.id = t.created_by) as created_by,
                t.created_at,
                DATE_FORMAT(t.created_at, \"%m\") as created_at_month,
                (SELECT u.username FROM fptu_user u WHERE u.id = (SELECT la.user_id FROM fptu_lead_assignment la WHERE la.lead_id = t.id AND la.deleted_at IS NULL ORDER BY la.id DESC LIMIT 1)) as assignee,
                t.total_call_log,
                CASE WHEN total_call_log > 10 THEN (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at DESC LIMIT 1 OFFSET 9) ELSE (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at ASC LIMIT 1 OFFSET 0) END AS call_log_1,
                CASE WHEN total_call_log > 10 THEN (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at DESC LIMIT 1 OFFSET 8) ELSE (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at ASC LIMIT 1 OFFSET 1) END AS call_log_2,
                CASE WHEN total_call_log > 10 THEN (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at DESC LIMIT 1 OFFSET 7) ELSE (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at ASC LIMIT 1 OFFSET 2) END AS call_log_3,
                CASE WHEN total_call_log > 10 THEN (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at DESC LIMIT 1 OFFSET 6) ELSE (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at ASC LIMIT 1 OFFSET 3) END AS call_log_4,
                CASE WHEN total_call_log > 10 THEN (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at DESC LIMIT 1 OFFSET 5) ELSE (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at ASC LIMIT 1 OFFSET 4) END AS call_log_5,
                CASE WHEN total_call_log > 10 THEN (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at DESC LIMIT 1 OFFSET 4) ELSE (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at ASC LIMIT 1 OFFSET 5) END AS call_log_6,
                CASE WHEN total_call_log > 10 THEN (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at DESC LIMIT 1 OFFSET 3) ELSE (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at ASC LIMIT 1 OFFSET 6) END AS call_log_7,
                CASE WHEN total_call_log > 10 THEN (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at DESC LIMIT 1 OFFSET 2) ELSE (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at ASC LIMIT 1 OFFSET 7) END AS call_log_8,
                CASE WHEN total_call_log > 10 THEN (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at DESC LIMIT 1 OFFSET 1) ELSE (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at ASC LIMIT 1 OFFSET 8) END AS call_log_9,
                CASE WHEN total_call_log > 10 THEN (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at DESC LIMIT 1 OFFSET 0) ELSE (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at ASC LIMIT 1 OFFSET 9) END AS call_log_10,
                t.showup_school_at,
                t.showup_seminor_at,
                t.activated_at as waiting_list_at,
                DATE_FORMAT(t.activated_at, \"%m\") as waiting_list_at_month,
                t.status_waitlist_number as waiting_list_receipt_number,
                t.student_code,
                t.join_as_guest_at,
                t.guest_class_name as class_name_guest,
                CASE WHEN t.guest_class IS NOT NULL AND t.guest_class > 0 THEN (SELECT gc.name FROM guest_class gc WHERE gc.id = t.guest_class) ELSE NULL END AS class_group_guest,
                CASE WHEN t.learning_system_guest IS NOT NULL AND t.learning_system_guest > 0 THEN (SELECT lsg.name FROM learning_system_guest lsg WHERE lsg.id = t.learning_system_guest) ELSE NULL END AS learning_system_guest,
                t.status_guest_number as guest_receipt_number,
                t.enrolled_at,
                DATE_FORMAT(t.enrolled_at, \"%m\") as enrolled_at_month,
                class_name as class_name_official,
                CASE WHEN t.official_class IS NOT NULL AND t.official_class > 0 THEN (SELECT oc.name FROM official_class oc WHERE oc.id = t.official_class) ELSE NULL END AS class_group_official,
                CASE WHEN t.learning_system_official IS NOT NULL AND t.learning_system_official > 0 THEN (SELECT lso.name FROM learning_system_official lso WHERE lso.id = t.learning_system_official) ELSE NULL END AS learning_system_official,
                receipt_number
            ";
            $from_total = "
            (SELECT 
                *,
                (SELECT count(*) FROM edf_call_log WHERE edf_call_log.lead_id = fptu_lead.id AND edf_call_log.deleted_at IS NULL) as total_call_log
                FROM fptu_lead 
                WHERE $condition
            ) AS t
            ";
            $from_data = "
            (SELECT 
                *,
                (SELECT count(*) FROM edf_call_log WHERE edf_call_log.lead_id = fptu_lead.id AND edf_call_log.deleted_at IS NULL) as total_call_log
                FROM fptu_lead 
                WHERE $condition
                LIMIT $limit OFFSET $offset
            ) AS t
            ";

            $total = (DB::select(DB::raw("SELECT count(*) AS count FROM $from_total")))[0]->count;
            $total_page = ceil($total/$limit);
            $data = DB::select(DB::raw("SELECT $select FROM $from_data"));
        }

        session()->flashInput($request->input());

        return view('report.otb', array(
            'data' => $data,
            'total' => $total,
            'limit' => $limit,
            'offset' => $offset,
            'pagination_total_page' => $total_page,
            'pagination_current_page' => $current_page,
            'pagination_url' => $url,
            'pagination_url_params' => $url_params,
        ));
    }


    public function export(Request $request)
    {
        if(!empty($request->from_date) && !empty($request->to_date)){
            $data = array();
            $conditions = array();

            $conditions[] = "deleted_at IS NULL";

            $from_date = date('Y-m-d', strtotime(str_replace('/','-',$request->from_date))) . ' 00:00:00';
            $conditions[] = "created_at >= '$from_date'";
            $url_params['from_date'] = $request->from_date;

            $to_date = date('Y-m-d', strtotime(str_replace('/','-',$request->to_date))) . ' 23:59:59';
            $conditions[] = "created_at <= '$to_date'";
            $url_params['to_date'] = $request->to_date;

            if(!empty($request->department_id)){
                $conditions[] = "department_id = '$request->department_id'";
                $url_params['department_id'] = $request->department_id;
            }

            if(!empty($request->status)){
                $conditions[] = "status = '$request->status'";
                $url_params['status'] = $request->status;
            }

            $condition = implode(' AND ',$conditions);
            $select = "
                t.id,
                t.name,
                t.student_name,
                CASE WHEN t.phone1 != '' AND t.phone1 IS NOT NULL THEN t.phone1 ELSE t.phone2 END AS phone,
                t.birthdate as birthday,
                CASE WHEN t.major IS NOT NULL AND t.major > 0 THEN (SELECT m.name FROM fptu_major m WHERE m.id = t.major) ELSE NULL END AS class_group_desire,
                CASE WHEN t.learning_system_desire IS NOT NULL AND t.learning_system_desire > 0 THEN (SELECT lsd.name FROM learning_system_desire lsd WHERE lsd.id = t.learning_system_desire) ELSE NULL END AS learning_system_desire,
                (SELECT ls.name FROM fptu_lead_status ls WHERE ls.id = t.status) AS status,
                (SELECT s.name FROM fptu_source s WHERE s.id = (SELECT ls.source_id FROM fptu_lead_source ls WHERE ls.lead_id = t.id AND ls.deleted_at IS NULL LIMIT 1)) AS source,
                CASE WHEN t.utm_campaign IS NOT NULL AND t.utm_campaign != '' THEN utm_campaign ELSE (SELECT c.name FROM fptu_campaign c WHERE c.id = (SELECT campaign_id FROM fptu_lead_campaign lc WHERE lc.lead_id = t.id AND lc.deleted_at IS NULL LIMIT 1)) END AS campaign,
                (SELECT u.username FROM fptu_user u WHERE u.id = t.created_by) as created_by,
                t.created_at,
                DATE_FORMAT(t.created_at, \"%m\") as created_at_month,
                (SELECT u.username FROM fptu_user u WHERE u.id = (SELECT la.user_id FROM fptu_lead_assignment la WHERE la.lead_id = t.id AND la.deleted_at IS NULL ORDER BY la.id DESC LIMIT 1)) as assignee,
                t.total_call_log,
                CASE WHEN total_call_log > 10 THEN (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at DESC LIMIT 1 OFFSET 9) ELSE (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at ASC LIMIT 1 OFFSET 0) END AS call_log_1,
                CASE WHEN total_call_log > 10 THEN (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at DESC LIMIT 1 OFFSET 8) ELSE (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at ASC LIMIT 1 OFFSET 1) END AS call_log_2,
                CASE WHEN total_call_log > 10 THEN (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at DESC LIMIT 1 OFFSET 7) ELSE (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at ASC LIMIT 1 OFFSET 2) END AS call_log_3,
                CASE WHEN total_call_log > 10 THEN (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at DESC LIMIT 1 OFFSET 6) ELSE (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at ASC LIMIT 1 OFFSET 3) END AS call_log_4,
                CASE WHEN total_call_log > 10 THEN (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at DESC LIMIT 1 OFFSET 5) ELSE (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at ASC LIMIT 1 OFFSET 4) END AS call_log_5,
                CASE WHEN total_call_log > 10 THEN (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at DESC LIMIT 1 OFFSET 4) ELSE (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at ASC LIMIT 1 OFFSET 5) END AS call_log_6,
                CASE WHEN total_call_log > 10 THEN (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at DESC LIMIT 1 OFFSET 3) ELSE (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at ASC LIMIT 1 OFFSET 6) END AS call_log_7,
                CASE WHEN total_call_log > 10 THEN (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at DESC LIMIT 1 OFFSET 2) ELSE (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at ASC LIMIT 1 OFFSET 7) END AS call_log_8,
                CASE WHEN total_call_log > 10 THEN (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at DESC LIMIT 1 OFFSET 1) ELSE (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at ASC LIMIT 1 OFFSET 8) END AS call_log_9,
                CASE WHEN total_call_log > 10 THEN (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at DESC LIMIT 1 OFFSET 0) ELSE (SELECT cl.created_at FROM edf_call_log cl WHERE cl.lead_id = t.id ORDER BY cl.created_at ASC LIMIT 1 OFFSET 9) END AS call_log_10,
                t.showup_school_at,
                t.showup_seminor_at,
                t.activated_at as waiting_list_at,
                DATE_FORMAT(t.activated_at, \"%m\") as waiting_list_at_month,
                t.status_waitlist_number as waiting_list_receipt_number,
                t.student_code,
                t.join_as_guest_at,
                t.guest_class_name as class_name_guest,
                CASE WHEN t.guest_class IS NOT NULL AND t.guest_class > 0 THEN (SELECT gc.name FROM guest_class gc WHERE gc.id = t.guest_class) ELSE NULL END AS class_group_guest,
                CASE WHEN t.learning_system_guest IS NOT NULL AND t.learning_system_guest > 0 THEN (SELECT lsg.name FROM learning_system_guest lsg WHERE lsg.id = t.learning_system_guest) ELSE NULL END AS learning_system_guest,
                t.status_guest_number as guest_receipt_number,
                t.enrolled_at,
                DATE_FORMAT(t.enrolled_at, \"%m\") as enrolled_at_month,
                class_name as class_name_official,
                CASE WHEN t.official_class IS NOT NULL AND t.official_class > 0 THEN (SELECT oc.name FROM official_class oc WHERE oc.id = t.official_class) ELSE NULL END AS class_group_official,
                CASE WHEN t.learning_system_official IS NOT NULL AND t.learning_system_official > 0 THEN (SELECT lso.name FROM learning_system_official lso WHERE lso.id = t.learning_system_official) ELSE NULL END AS learning_system_official,
                receipt_number
            ";
            $from = "
            (SELECT 
                *,
                (SELECT count(*) FROM edf_call_log WHERE edf_call_log.lead_id = fptu_lead.id AND edf_call_log.deleted_at IS NULL) as total_call_log
                FROM fptu_lead 
                WHERE $condition
            ) AS t
            ";

            $data = DB::select(DB::raw("SELECT $select FROM $from"));

            if(!empty($data)){
                $file_name = "Báo cáo tổng hợp Lead - OTB $from_date đến $to_date.xlsx";

                $excel = new PHPExcel();
                $i=1;
                $excel->setActiveSheetIndex(0)->setTitle('Report CRM - OTB')
                    ->setCellValue("A$i", 'Stt')
                    ->setCellValue("B$i", 'ID')
                    ->setCellValue("C$i", 'Họ tên PH')
                    ->setCellValue("D$i", 'Họ tên HS')
                    ->setCellValue("E$i", 'Điện thoại')
                    ->setCellValue("F$i", 'Ngày sinh')
                    ->setCellValue("G$i", 'Nhóm lớp mong muốn')
                    ->setCellValue("H$i", 'Hệ học mong muốn')
                    ->setCellValue("I$i", 'Trạng thái')
                    ->setCellValue("J$i", 'Nguồn dẫn')
                    ->setCellValue("K$i", 'Chiến dịch')
                    ->setCellValue("L$i", 'Người tạo')
                    ->setCellValue("M$i", 'Ngày tạo')
                    ->setCellValue("N$i", 'Tháng tạo')
                    ->setCellValue("O$i", 'Người phụ trách')
                    ->setCellValue("P$i", 'Ngày call log 1')
                    ->setCellValue("Q$i", 'Ngày call log 2')
                    ->setCellValue("R$i", 'Ngày call log 3')
                    ->setCellValue("S$i", 'Ngày call log 4')
                    ->setCellValue("T$i", 'Ngày call log 5')
                    ->setCellValue("U$i", 'Ngày call log 6')
                    ->setCellValue("V$i", 'Ngày call log 7')
                    ->setCellValue("W$i", 'Ngày call log 8')
                    ->setCellValue("X$i", 'Ngày call log 9')
                    ->setCellValue("Y$i", 'Ngày call log 10')
                    ->setCellValue("Z$i", 'Ngày show up đến trường')
                    ->setCellValue("AA$i", 'Ngày show up hội thảo')
                    ->setCellValue("AB$i", 'Ngày giữ chỗ')
                    ->setCellValue("AC$i", 'Tháng giữ chỗ')
                    ->setCellValue("AD$i", 'Số phiếu thu tiền/báo có giữ chỗ')
                    ->setCellValue("AE$i", 'Mã học sinh')
                    ->setCellValue("AF$i", 'Ngày tham gia CT bé làm khách')
                    ->setCellValue("AG$i", 'Lớp học CT bé làm khách')
                    ->setCellValue("AH$i", 'Nhóm lớp CT bé làm khách')
                    ->setCellValue("AI$i", 'Hệ học CT bé làm khách')
                    ->setCellValue("AJ$i", 'Số phiếu thu tiền/báo có CT bé làm khách')
                    ->setCellValue("AK$i", 'Ngày nhập học')
                    ->setCellValue("AL$i", 'Tháng nhập học')
                    ->setCellValue("AM$i", 'Tên lớp chính thức')
                    ->setCellValue("AN$i", 'Nhóm lớp chính thức')
                    ->setCellValue("AO$i", 'Hệ học chính thức')
                    ->setCellValue("AP$i", 'Số biên lai/báo có')
                ;
                foreach ($data as $item){
                    $i++;
                    $excel->setActiveSheetIndex(0)
                        ->setCellValue("A$i", $i)
                        ->setCellValue("B$i", $item->id)
                        ->setCellValue("C$i", $item->name)
                        ->setCellValue("D$i", $item->student_name)
                        ->setCellValue("E$i", $item->phone)
                        ->setCellValue("F$i", !empty($item->birthday) ? date('d/m/Y', strtotime($item->birthday)) : null)
                        ->setCellValue("G$i", $item->class_group_desire)
                        ->setCellValue("H$i", $item->learning_system_desire)
                        ->setCellValue("I$i", $item->status)
                        ->setCellValue("J$i", $item->source)
                        ->setCellValue("K$i", $item->campaign)
                        ->setCellValue("L$i", $item->created_by)
                        ->setCellValue("M$i", !empty($item->created_at) ? date('d/m/Y H:i:s', strtotime($item->created_at)) : null)
                        ->setCellValue("N$i", $item->created_at_month)
                        ->setCellValue("O$i", $item->assignee)
                        ->setCellValue("P$i", !empty($item->call_log_1) ? date('d/m/Y H:i:s', strtotime($item->call_log_1)) : null)
                        ->setCellValue("Q$i", !empty($item->call_log_2) ? date('d/m/Y H:i:s', strtotime($item->call_log_2)) : null)
                        ->setCellValue("R$i", !empty($item->call_log_3) ? date('d/m/Y H:i:s', strtotime($item->call_log_3)) : null)
                        ->setCellValue("S$i", !empty($item->call_log_4) ? date('d/m/Y H:i:s', strtotime($item->call_log_4)) : null)
                        ->setCellValue("T$i", !empty($item->call_log_5) ? date('d/m/Y H:i:s', strtotime($item->call_log_5)) : null)
                        ->setCellValue("U$i", !empty($item->call_log_6) ? date('d/m/Y H:i:s', strtotime($item->call_log_6)) : null)
                        ->setCellValue("V$i", !empty($item->call_log_7) ? date('d/m/Y H:i:s', strtotime($item->call_log_7)) : null)
                        ->setCellValue("W$i", !empty($item->call_log_8) ? date('d/m/Y H:i:s', strtotime($item->call_log_8)) : null)
                        ->setCellValue("X$i", !empty($item->call_log_9) ? date('d/m/Y H:i:s', strtotime($item->call_log_9)) : null)
                        ->setCellValue("Y$i", !empty($item->call_log_10) ? date('d/m/Y H:i:s', strtotime($item->call_log_10)) : null)
                        ->setCellValue("Z$i", !empty($item->showup_school_at) ? date('d/m/Y H:i:s', strtotime($item->showup_school_at)) : null)
                        ->setCellValue("AA$i", !empty($item->showup_seminor_at) ? date('d/m/Y H:i:s', strtotime($item->showup_seminor_at)) : null)
                        ->setCellValue("AB$i", !empty($item->waiting_list_at) ? date('d/m/Y H:i:s', strtotime($item->waiting_list_at)) : null)
                        ->setCellValue("AC$i", $item->waiting_list_at_month)
                        ->setCellValue("AD$i", $item->waiting_list_receipt_number)
                        ->setCellValue("AE$i", $item->student_code)
                        ->setCellValue("AF$i", !empty($item->join_as_guest_at) ? date('d/m/Y H:i:s', strtotime($item->join_as_guest_at)) : null)
                        ->setCellValue("AG$i", $item->class_name_guest)
                        ->setCellValue("AH$i", $item->class_group_guest)
                        ->setCellValue("AI$i", $item->learning_system_guest)
                        ->setCellValue("AJ$i", $item->guest_receipt_number)
                        ->setCellValue("AK$i", !empty($item->enrolled_at) ? date('d/m/Y H:i:s', strtotime($item->enrolled_at)) : null)
                        ->setCellValue("AL$i", $item->enrolled_at_month)
                        ->setCellValue("AM$i", $item->class_name_official)
                        ->setCellValue("AN$i", $item->class_group_official)
                        ->setCellValue("AO$i", $item->learning_system_official)
                        ->setCellValue("AP$i", $item->receipt_number)
                    ;
                }

                header("Content-Type: application/vnd.ms-excel");
                header("Content-Disposition: attachment;filename=$file_name");
                header("Cache-Control: max-age=0");
                $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
                $writer->save('php://output');
            }else{
                die('Không có dữ liệu');
            }
        }else{
            die('Thiếu dữ liệu truyền vào');
        }
    }

}
