<?php

namespace App\Http\Controllers\Report;

use App\LeadLogStatus;
use App\Report\Cdr;
use Illuminate\Http\Request;
use App\Department;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Utils;
use Illuminate\Support\Facades\Auth;
use App\Lead;
use App\UserPref;

class CdrController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $model = new Cdr();
        $data = array();
        $message = null;
        $result = null;

        $start_date     = $request->get('start_date');
        $end_date       = $request->get('end_date');
        $type           = $request->get('type');
        $status         = $request->get('status');
        $phone_number   = $request->get('phone_number');
        $extension      = $request->get('extension');

        if(!empty($start_date) && !empty($end_date)){

            $extra_params = array();
            if(!empty($type)){
                $extra_params['type'] = $type;
            }
            if(!empty($status)){
                $extra_params['status'] = $status;
            }
            if(!empty($phone_number)){
                $extra_params['phone_number'] = $phone_number;
            }

            $raw_data = $model->getReportCdr($start_date,$end_date,$extra_params);
            if($raw_data){
                $message = $raw_data['message'];
                $result = $raw_data['result'];
            }

            if(!empty($raw_data['data'])){
                $data = $raw_data['data'];
                if(!empty($extension)) {
                    $filter_ext_data = array();
                    foreach ($data as $cdr) {
                        if($cdr->extension == $extension){
                            $filter_ext_data[] = $cdr;
                        }
                    }
                    $data = $filter_ext_data;
                }
            }
        }

        session()->flashInput($request->input());
        return view('report.cdr', [
            'message'       => $message,
            'result'        => $result,
            'data'          => $data,
        ]);
    }

    public function getUserByDepartment(Request $request){
        $department_id = $request->get('department_id');
        echo Cdr::getOptionsUser(null, $department_id);
    }

    public function getPbxConfig()
    {
        /*$config = array(
            'host' => 'https://cxcs001.cloudpbx.vn:8083',
            'token' => '$1$/u28EV27$kT766.Wt3qSMlOZGRybgI1',
            'organization' => 'demo02.cloudpbx.vn'
        );*/

        $config = array(
            'host' => Cdr::HOST,
            'token' => Cdr::TOKEN,
            'organization' => Cdr::ORGANIZATION,
        );

        echo json_encode($config);
    }

    public function saveCallLog(Request $request)
    {
        $return = array(
            'success'=> true,
            'message'=> '',
            'data' => array(),
        );
        if(!empty($request->call_session)){
            $start_date = $end_date =  date('Y-m-d');

            $model = new Cdr();
            try{
                $data = $model->getReportCdr($start_date, $end_date, array('call_session' => $request->call_session));
            }catch (\Exception $e){
                $return['success'] = false;
                $return['message'] = 'Lấy dữ liệu từ tổng đài thất bại';
            }

            if(!empty($data['data'])){
                $callLog = new Lead\CallLog();
                $callLog->lead_id = $request->lead_id;
                $callLog->direction = Lead\CallLog::DIRECTION_OUT;
                $callLog->status = Cdr::convertStatusForCallLog($data['data'][0]->status);
                $callLog->notes = $request->note;
                $callLog->created_at = $data['data'][0]->date_time;
                $callLog->created_by = Auth::user()->id;
                $callLog->data = json_encode($data['data'][0]);
                $callLog->save();

                if(!empty($request->lead_status)){
                    $lead = Lead::where('id', $request->lead_id)->first();
                    if($lead){
                        if($lead->status != $request->lead_status){
                            $old_status = $lead->status;
                            $lead->status = $request->lead_status;
                            if($lead->save()){
                                $leadLogStatus = new LeadLogStatus();
                                $leadLogStatus->lead_id = $lead->id;
                                $leadLogStatus->status_old = $old_status;
                                $leadLogStatus->status_new = $lead->status;
                                $leadLogStatus->lead_id = $lead->id;
                                $leadLogStatus->created_at = date('Y-m-d H:i:s');
                                $leadLogStatus->created_by = Auth::user()->id;
                                $leadLogStatus->save();
                            }
                        }
                    }
                }

                if(!empty($request->lead_call_status)){
                    $lead = Lead::where('id', $request->lead_id)->first();
                    if($lead) {
                        if ($lead->call_status != $request->lead_call_status) {
                            $old_status = $lead->call_status;
                            $lead->call_status = $request->lead_call_status;
                            if($lead->save()){
                                $leadLogStatus = new LeadLogStatus();
                                $leadLogStatus->lead_id = $lead->id;
                                $leadLogStatus->status_old = $old_status;
                                $leadLogStatus->status_new = $lead->call_status;
                                $leadLogStatus->lead_id = $lead->id;
                                $leadLogStatus->created_at = date('Y-m-d H:i:s');
                                $leadLogStatus->created_by = Auth::user()->id;
                                $leadLogStatus->type = LeadLogStatus::TYPE_CALL_STATUS;
                                $leadLogStatus->save();
                            }
                        }
                    }
                }

                $return['data']['cdr'] = $data['data'][0];
                $return['data']['call_log'] = $callLog->toArray();

                $callLogStatusListData = Lead\CallLog::getListStatus();
                $callLogDirectionListData = Lead\CallLog::getListDirection();
                $html = '<tr>' .
                    '<td>*</td>' .
                    '<td>'.$callLogDirectionListData[$callLog->direction].'</td>' .
                    '<td>'.$callLogStatusListData[$callLog->status].'</td>' .
                    '<td>'.$callLog->notes.'</td>' .
                    '<td>'.Auth::user()->username.'</td>' .
                    '<td>'.date('d/m/Y H:i:s',strtotime($callLog->created_at)).'</td>';
                if(!empty($data['data'][0]->talk_time) && $data['data'][0]->talk_time > 0 && !empty( $data['data'][0]->recordingfile)){
                    $html.='<td>'.$data['data'][0]->talk_time.'</td>';
                    $html.='<td><div><a onclick="loadRecordingFile($(this).closest(\'div\'),\''. $data['data'][0]->recordingfile .'\')"><i class="fa fa-file-audio-o"></i></a> File ghi âm</div></td>';
                    $html.='<td><div><a onclick="downloadRecordingFile(\''. $data['data'][0]->download_url .'\')"><i class="fa fa-download"></i></a></div></td>';
                }else{
                    $html.='<td>'.(isset($data['data'][0]->talk_time)?$data['data'][0]->talk_time:'').'</td>';
                    $html.='<td colspan="2">Không có file ghi âm</td>';
                }
                $html.='</tr>';
                $return['data']['html'] = $html;
            }else{
                $return['success'] = false;
                $return['message'] = 'Không có dữ liệu trên hệ thống tổng đài';
            }
        }else{
            $return['success'] = false;
            $return['message'] = 'Không có dữ liệu call session';
        }

        echo json_encode($return);
    }

}
