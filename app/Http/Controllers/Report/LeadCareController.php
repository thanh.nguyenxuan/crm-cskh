<?php

namespace App\Http\Controllers\Report;


use App\Department;
use App\Http\Controllers\Controller;
use App\Utils;
use Illuminate\Http\Request;
use App\Lead;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use PHPExcel;
use PHPExcel_IOFactory;

class LeadCareController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request)
    {
        $data = null;
        if(!empty($request->from_date) && !empty($request->to_date)){
            $key = __CLASS__.'_'.__FUNCTION__
                .'_'.$request->from_date
                .'_'.$request->to_date
                .'_'.$request->department_group
                .'_'.$request->assignee_id
                .'_'.(is_array($request->department_ids) ? implode(',',$request->department_ids) : $request->department_ids)
                .'_'.Auth::user()->id;
            ;
//            $data =  Cache::store('file')->get($key);
            if(!$data){
                $from_date = date('Y-m-d', strtotime(str_replace('/','-',$request->from_date))) . ' 00:00:00';
                $to_date = date('Y-m-d', strtotime(str_replace('/','-',$request->to_date))) . ' 23:59:59';
                $count_call_log_day = count(Utils::removeDayInRangeByWeekDayName(Utils::getListDateInRange($from_date, $to_date), array('sat', 'sun')));
                $query = DB::table('fptu_user AS t')
                    ->whereNull('deleted_at')
                    ->where('active', 1)
                    ->whereRaw("t.id IN (SELECT user_id FROM fptu_role_user WHERE role_id IN (SELECT id FROM fptu_role WHERE name IN ('telesales','teleteamleader')))")
                ;

                $query->join('fptu_department AS d', 'd.id', '=', 't.department_id');
                $query->orderByRaw('d.sort_index ASC, t.username ASC');
                $query->select(
                    't.id',
                    DB::raw('d.name AS department'),
                    't.username',
                    DB::raw('(SELECT count(*) FROM fptu_lead WHERE deleted_at IS NULL AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL)) AS count_all_lead'),
                    DB::raw('(SELECT count(*) FROM fptu_lead WHERE deleted_at IS NULL AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL) AND status IN (1,5,7,17,18)) AS count_all_lead_available'),
                    DB::raw("(SELECT count(*) FROM fptu_lead WHERE deleted_at IS NULL AND status IN (3, 4, 6) AND id IN (SELECT l.lead_id FROM fptu_lead_log_status l WHERE l.created_at >= '$from_date' AND l.created_at <= '$to_date' AND l.status_new IN (3, 4, 6) ) AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL)) AS total_lead_dead"),
                    DB::raw("(SELECT count(*) FROM fptu_lead WHERE deleted_at IS NULL AND created_at BETWEEN '$from_date' AND '$to_date' AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL)) AS total_lead_new"),
                    DB::raw("(SELECT count(*) FROM fptu_lead WHERE deleted_at IS NULL AND hot_lead_at BETWEEN '$from_date' AND '$to_date' AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL)) AS total_lead_hot_lead"),
                    DB::raw("(SELECT count(*) FROM fptu_lead WHERE deleted_at IS NULL AND showup_school_at BETWEEN '$from_date' AND '$to_date' AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL)) AS total_lead_showup"),
                    DB::raw("(SELECT count(*) FROM fptu_lead WHERE deleted_at IS NULL AND activated_at BETWEEN '$from_date' AND '$to_date' AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL)) AS total_lead_waitlist"),
//                    DB::raw("(SELECT count(*) FROM fptu_lead WHERE deleted_at IS NULL AND showup_school_at IS NOT NULL AND showup_school_at BETWEEN '$from_date' AND '$to_date' AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL)) AS count_all_lead_showup"),
                    DB::raw("(SELECT count(*) FROM edf_call_log WHERE created_at BETWEEN '$from_date' AND '$to_date' AND created_by = t.id AND status = '".Lead\CallLog::STATUS_ANSWERED."' AND deleted_at IS NULL) AS count_all_log_call_answered"),
                    DB::raw("(SELECT round(count(*)/$count_call_log_day,1) FROM edf_call_log WHERE created_at BETWEEN '$from_date' AND '$to_date' AND status = '".Lead\CallLog::STATUS_ANSWERED."' AND created_by = t.id AND deleted_at IS NULL) AS average_log_call_by_day"),
                    DB::raw("(SELECT count(*) FROM log_lead_care WHERE deleted_at IS NULL AND created_by = t.id AND created_at BETWEEN '$from_date' AND '$to_date') AS total_log_lead_care"),
                    DB::raw("(SELECT count(*) FROM fptu_lead f WHERE deleted_at IS NULL AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL) AND status IN (1,5,7,17,18) AND (SELECT count(*) FROM edf_call_log WHERE lead_id = f.id AND created_by = t.id AND created_at <= '$to_date' AND status = '".Lead\CallLog::STATUS_ANSWERED."' AND deleted_at IS NULL) = 1) AS count_all_lead_take_care_1"),
                    DB::raw("(SELECT count(*) FROM fptu_lead f WHERE deleted_at IS NULL AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL) AND status IN (1,5,7,17,18) AND (SELECT count(*) FROM edf_call_log WHERE lead_id = f.id AND created_by = t.id AND created_at <= '$to_date' AND status = '".Lead\CallLog::STATUS_ANSWERED."' AND deleted_at IS NULL) = 2) AS count_all_lead_take_care_2"),
                    DB::raw("(SELECT count(*) FROM fptu_lead f WHERE deleted_at IS NULL AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL) AND status IN (1,5,7,17,18) AND (SELECT count(*) FROM edf_call_log WHERE lead_id = f.id AND created_by = t.id AND created_at <= '$to_date' AND status = '".Lead\CallLog::STATUS_ANSWERED."' AND deleted_at IS NULL) = 3) AS count_all_lead_take_care_3"),
                    DB::raw("(SELECT count(*) FROM fptu_lead f WHERE deleted_at IS NULL AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL) AND status IN (1,5,7,17,18) AND (SELECT count(*) FROM edf_call_log WHERE lead_id = f.id AND created_by = t.id AND created_at <= '$to_date' AND status = '".Lead\CallLog::STATUS_ANSWERED."' AND deleted_at IS NULL) > 3) AS count_all_lead_take_care_more")
                );
                if (!in_array(Department::HOLDING, Auth::user()->getManageDepartments()) && !Auth::user()->hasRole('holding')) {
                    $departments_str = implode(',', Auth::user()->getManageDepartments());
                    $query->whereRaw("(t.department_id IN ($departments_str)
                        OR t.id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                        OR t.id IN (SELECT user_id FROM group_user WHERE group_id IN (
                            SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                        ))
                    )");
                }else{
                    if(!empty($request->department_group)){
                        $departments_str = implode(',', Department::getListDepartmentId($request->department_group));
                        $query->whereRaw("(t.department_id IN ($departments_str)
                            OR t.id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                            OR t.id IN (SELECT user_id FROM group_user WHERE group_id IN (
                                SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                            ))
                        )");
                    }
                }
                if(!empty($request->department_ids)){
                    $departments_str = implode(',', $request->department_ids);
                    $query->whereRaw("(t.department_id IN ($departments_str)
                        OR t.id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                        OR t.id IN (SELECT user_id FROM group_user WHERE group_id IN (
                            SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                        ))
                    )");
                }
                if(!empty($request->assignee_id)){
                    $query->where('t.id', $request->assignee_id);
                }
                $data = $query->get();
//                Cache::store('file')->put($key, $data, 60);
            }
        }
        session()->flashInput($request->input());
        return view('report.lead_care', array(
            'data' => $data,
        ));
    }


    public function export(Request $request)
    {
        $data = null;
        if(!empty($request->from_date) && !empty($request->to_date)){
            $key = __CLASS__.'_'.__FUNCTION__
                .'_'.$request->from_date
                .'_'.$request->to_date
                .'_'.$request->department_group
                .'_'.$request->assignee_id
                .'_'.(is_array($request->department_ids) ? implode(',',$request->department_ids) : $request->department_ids)
                .'_'.Auth::user()->id;
            ;

//            $data =  Cache::store('file')->get($key);
            if(!$data){
                $from_date = date('Y-m-d', strtotime(str_replace('/','-',$request->from_date))) . ' 00:00:00';
                $to_date = date('Y-m-d', strtotime(str_replace('/','-',$request->to_date))) . ' 23:59:59';
                $count_call_log_day = count(Utils::removeDayInRangeByWeekDayName(Utils::getListDateInRange($from_date, $to_date), array('sat', 'sun')));
                $query = DB::table('fptu_user AS t')
                    ->whereNull('deleted_at')
                    ->where('active', 1)
                    ->whereRaw("t.id IN (SELECT user_id FROM fptu_role_user WHERE role_id IN (SELECT id FROM fptu_role WHERE name IN ('telesales','teleteamleader')))")
                ;
                $query->join('fptu_department AS d', 'd.id', '=', 't.department_id');
                $query->orderByRaw('d.sort_index ASC, t.username ASC');
                $query->select(
                    't.id',
                    DB::raw('d.name AS department'),
                    't.username',
                    DB::raw('(SELECT count(*) FROM fptu_lead WHERE deleted_at IS NULL AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL)) AS count_all_lead'),
                    DB::raw('(SELECT count(*) FROM fptu_lead WHERE deleted_at IS NULL AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL) AND status IN (1,5,7,17,18)) AS count_all_lead_available'),
                    DB::raw("(SELECT count(*) FROM fptu_lead WHERE deleted_at IS NULL AND status IN (3, 4, 6) AND id IN (SELECT l.lead_id FROM fptu_lead_log_status l WHERE l.created_at >= '$from_date' AND l.created_at <= '$to_date' AND l.status_new IN (3, 4, 6) ) AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL)) AS total_lead_dead"),
                    DB::raw("(SELECT count(*) FROM fptu_lead WHERE deleted_at IS NULL AND created_at BETWEEN '$from_date' AND '$to_date' AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL)) AS total_lead_new"),
                    DB::raw("(SELECT count(*) FROM fptu_lead WHERE deleted_at IS NULL AND hot_lead_at BETWEEN '$from_date' AND '$to_date' AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL)) AS total_lead_hot_lead"),
                    DB::raw("(SELECT count(*) FROM fptu_lead WHERE deleted_at IS NULL AND showup_school_at BETWEEN '$from_date' AND '$to_date' AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL)) AS total_lead_showup"),
                    DB::raw("(SELECT count(*) FROM fptu_lead WHERE deleted_at IS NULL AND activated_at BETWEEN '$from_date' AND '$to_date' AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL)) AS total_lead_waitlist"),
//                    DB::raw("(SELECT count(*) FROM fptu_lead WHERE deleted_at IS NULL AND showup_school_at IS NOT NULL AND showup_school_at BETWEEN '$from_date' AND '$to_date' AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL)) AS count_all_lead_showup"),
                    DB::raw("(SELECT count(*) FROM edf_call_log WHERE created_at BETWEEN '$from_date' AND '$to_date' AND created_by = t.id AND status = '".Lead\CallLog::STATUS_ANSWERED."' AND deleted_at IS NULL) AS count_all_log_call_answered"),
                    DB::raw("(SELECT round(count(*)/$count_call_log_day,1) FROM edf_call_log WHERE created_at BETWEEN '$from_date' AND '$to_date' AND status = '".Lead\CallLog::STATUS_ANSWERED."' AND created_by = t.id AND deleted_at IS NULL) AS average_log_call_by_day"),
                    DB::raw("(SELECT count(*) FROM log_lead_care WHERE deleted_at IS NULL AND created_by = t.id AND created_at BETWEEN '$from_date' AND '$to_date') AS total_log_lead_care"),
                    DB::raw("(SELECT count(*) FROM fptu_lead f WHERE deleted_at IS NULL AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL) AND status IN (1,5,7,17,18) AND (SELECT count(*) FROM edf_call_log WHERE lead_id = f.id AND created_by = t.id AND created_at <= '$to_date' AND status = '".Lead\CallLog::STATUS_ANSWERED."' AND deleted_at IS NULL) = 1) AS count_all_lead_take_care_1"),
                    DB::raw("(SELECT count(*) FROM fptu_lead f WHERE deleted_at IS NULL AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL) AND status IN (1,5,7,17,18) AND (SELECT count(*) FROM edf_call_log WHERE lead_id = f.id AND created_by = t.id AND created_at <= '$to_date' AND status = '".Lead\CallLog::STATUS_ANSWERED."' AND deleted_at IS NULL) = 2) AS count_all_lead_take_care_2"),
                    DB::raw("(SELECT count(*) FROM fptu_lead f WHERE deleted_at IS NULL AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL) AND status IN (1,5,7,17,18) AND (SELECT count(*) FROM edf_call_log WHERE lead_id = f.id AND created_by = t.id AND created_at <= '$to_date' AND status = '".Lead\CallLog::STATUS_ANSWERED."' AND deleted_at IS NULL) = 3) AS count_all_lead_take_care_3"),
                    DB::raw("(SELECT count(*) FROM fptu_lead f WHERE deleted_at IS NULL AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = t.id AND deleted_at IS NULL) AND status IN (1,5,7,17,18) AND (SELECT count(*) FROM edf_call_log WHERE lead_id = f.id AND created_by = t.id AND created_at <= '$to_date' AND status = '".Lead\CallLog::STATUS_ANSWERED."' AND deleted_at IS NULL) > 3) AS count_all_lead_take_care_more")
                );
                if (!in_array(Department::HOLDING, Auth::user()->getManageDepartments()) && !Auth::user()->hasRole('holding')) {
                    $departments_str = implode(',', Auth::user()->getManageDepartments());
                    $query->whereRaw("(t.department_id IN ($departments_str)
                        OR t.id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                        OR t.id IN (SELECT user_id FROM group_user WHERE group_id IN (
                            SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                        ))
                    )");
                }else{
                    if(!empty($request->department_group)){
                        $departments_str = implode(',', Department::getListDepartmentId($request->department_group));
                        $query->whereRaw("(t.department_id IN ($departments_str)
                            OR t.id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                            OR t.id IN (SELECT user_id FROM group_user WHERE group_id IN (
                                SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                            ))
                        )");
                    }
                }
                if(!empty($request->department_ids)){
                    $departments_str = implode(',', $request->department_ids);
                    $query->whereRaw("(t.department_id IN ($departments_str)
                        OR t.id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                        OR t.id IN (SELECT user_id FROM group_user WHERE group_id IN (
                            SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                        ))
                    )");
                }
                if(!empty($request->assignee_id)){
                    $query->where('t.id', $request->assignee_id);
                }
                $data = $query->get();
//                Cache::store('file')->put($key, $data, 60);
            }

            if(!empty($data)){
                $file_name = "Báo cáo chăm sóc lead_".date('YmdHis').".xlsx";

                $excel = new PHPExcel();
                $i=1;
                $excel->setActiveSheetIndex(0)->setTitle('Lead Interactivity')
                    ->setCellValue("A$i", 'Stt')
                    ->setCellValue("B$i", 'Cơ sở')
                    ->setCellValue("C$i", 'TVTS')
                    ->setCellValue("D$i", 'Tổng số Lead')
                    ->setCellValue("E$i", 'Tổng số Lead còn chăm sóc được')
                    ->setCellValue("F$i", 'Tổng số Lead dead')
                    ->setCellValue("G$i", 'Tổng số Lead mới')
                    ->setCellValue("H$i", 'Hot Lead')
                    ->setCellValue("I$i", 'Show up')
                    ->setCellValue("J$i", 'WL')
                    ->setCellValue("K$i", 'Tổng số cuộc gọi có trả lời')
                    ->setCellValue("L$i", 'Trung bình logcall/ngày')
                    ->setCellValue("M$i", 'Tổng số ghi chú qua các kênh khác')
                    ->setCellValue("N$i", 'Số lead chăm sóc 1 lần')
                    ->setCellValue("O$i", 'Số lead chăm sóc 2 lần')
                    ->setCellValue("P$i", 'Số lead chăm sóc 3 lần')
                    ->setCellValue("Q$i", 'Số lead chăm sóc trên 3 lần')
                ;
                foreach ($data as $item){
                    $i++;

                    $excel->setActiveSheetIndex(0)
                        ->setCellValue("A$i", $i-1)
                        ->setCellValue("B$i", $item->department)
                        ->setCellValue("C$i", $item->username)
                        ->setCellValue("D$i", $item->count_all_lead)
                        ->setCellValue("E$i", $item->count_all_lead_available)
                        ->setCellValue("F$i", $item->total_lead_dead)
                        ->setCellValue("G$i", $item->total_lead_new)
                        ->setCellValue("H$i", $item->total_lead_hot_lead)
                        ->setCellValue("I$i", $item->total_lead_showup)
                        ->setCellValue("J$i", $item->total_lead_waitlist)
                        ->setCellValue("K$i", $item->count_all_log_call_answered)
                        ->setCellValue("L$i", $item->average_log_call_by_day)
                        ->setCellValue("M$i", $item->total_log_lead_care)
                        ->setCellValue("N$i", $item->count_all_lead_take_care_1)
                        ->setCellValue("O$i", $item->count_all_lead_take_care_2)
                        ->setCellValue("P$i", $item->count_all_lead_take_care_3)
                        ->setCellValue("Q$i", $item->count_all_lead_take_care_more)
                    ;
                }

                header("Content-Type: application/vnd.ms-excel");
                header("Content-Disposition: attachment;filename=$file_name");
                header("Cache-Control: max-age=0");
                $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
                $writer->save('php://output');
            }else{
                die('Không có dữ liệu');
            }
        }
    }
}