<?php

namespace App\Http\Controllers\Report;

use App\Lead\CallLog;
use App\Mail\Mailer;
use App\Role;
use Illuminate\Http\Request;
use App\Department;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Utils;
use Illuminate\Support\Facades\Auth;
use App\Lead;
use App\UserPref;
use Illuminate\Support\Facades\Mail;

class CallLogReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $department_list = null;
        $tele_list = null;
        $role_list = null;

        $from_date = $request->get('from_date', date('d/m/Y'));
        $to_date = $request->get('to_date', date('d/m/Y'));
        $direction = $request->get('direction');
        $status = $request->get('status');
        $department_id = $request->get('department_id');
        $user_id = $request->get('user_id');
        $role_id = $request->get('role_id');
        $department_level = $request->get('department_level');

        $query = CallLog::select('created_by', DB::raw('COUNT(*) as call_count'))->groupBy('created_by')->where(DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d')"), '>=', Utils::convertDate($from_date))->where(DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d')"), '<=', Utils::convertDate($to_date));
        if(!empty($direction)){
            $query->where('direction', $direction);
        }
        if(!empty($status)){
            $query->where('status', $status);
        }
        if(!empty($department_id) && empty($user_id)){
            $query->whereIn('created_by', function($sub_query) use($department_id){
                $sub_query->select('id')
                    ->from(with(new User())->getTable())
                    ->where('department_id', $department_id);
            });
        }
        if(!empty($user_id)){
            $query->where('created_by', $user_id);
        }
        if(!empty($role_id)){
            $query->whereIn('created_by', function($sub_query) use($role_id){
                $sub_query->select('user_id')
                    ->from('fptu_role_user')
                    ->where('role_id', $role_id);
            });
        }
        if(!empty($department_level)){
            $query->whereIn('created_by', function($sub_query) use ($department_level){
                $sub_query->select('id')
                    ->from(with(new User())->getTable())
                    ->whereIn('department_id', function($sub_query_2) use ($department_level){
                        $sub_query_2->select('id')
                            ->from(with(new Department())->getTable())
                            ->where('name', 'like', "$department_level%");
                    });
            });
        }

        $call_log_stats = $query->pluck('call_count', 'created_by')->toArray();


        if (!Auth::user()->hasRole('admin') && !Auth::user()->hasRole('holding')) {
            $department_list = Department::whereIn('id', Auth::user()->getManageDepartments())->orderBy('sort_index','ASC')->get();
            $departments_str = implode(',', Auth::user()->getManageDepartments());
            $tele_query = User::whereRaw("(department_id IN ($departments_str)
                    OR id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                    OR id IN (SELECT user_id FROM group_user WHERE group_id IN (
                        SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                    ))
                )")
                ->where('active', 1)->whereNull('deleted_at')->select(DB::raw("CONCAT(username,' - ', full_name) as username"), 'id')->orderBy('username', 'ASC');
        } else {
            $department_list = Department::orderBy('sort_index','ASC')->get();
            $tele_query = User::where('active', 1)->whereNull('deleted_at')->select(DB::raw("CONCAT(username,' - ', full_name) as username"), 'id')->orderBy('username', 'ASC');
        }
        $role_list = Role::all();

        if(!empty($department_id) && empty($user_id)){
            $tele_query = $tele_query->where('department_id', $department_id);
        }
        if(!empty($user_id)){
            $tele_query = $tele_query->where('id', $user_id);
        }
        if(!empty($role_id)){
            $tele_query = $tele_query->whereIn('id', function($sub_query) use($role_id){
                $sub_query->select('user_id')
                    ->from('fptu_role_user')
                    ->where('role_id', $role_id);
            });
        }
        if(!empty($department_level)){
            $tele_query = $tele_query->whereIn('department_id', function($sub_query) use($department_level){
                $sub_query->select('id')
                    ->from(with(new Department())->getTable())
                    ->where('name', 'like', "$department_level%");
            });
        }
        $tele_list = $tele_query->get();

        session()->flashInput($request->input());
        return view('report.call_log', ['department_list' => $department_list, 'tele_list' => $tele_list, 'role_list' => $role_list, 'call_log_stats' => $call_log_stats]);
    }

    public function getUserByDepartment(Request $request){
        $department_id = $request->get('department_id');
        if(!empty($request->only_telesales)){
            echo CallLog::getOptionsUserTVTS(null, $department_id);
        }else{
            echo CallLog::getOptionsUser(null, $department_id);
        }
    }


    public function test(Request $request)
    {

//        Mail::to('thanh.nguyenxuan@edufit.vn')->send(new Mailer("test1"));

//        $to_name = 'Thành';
//        $to_email = 'htxuanthanh.star.move.vn@gmail.com';
//        $subject = 'Test2';
//        $data = array('name' => 'TestOK', 'body' => 'Test mail1');
//        Mail::send('email.default', $data, function($message) use ($to_name, $to_email, $subject) {
//            $message->to($to_email, $to_name)
//                ->subject($subject);
//        $message->from('htxuanthanh.star.move.vn@gmail.com','Test MAIL2');
//        });

        /*$from       = 'CRM';
        $to         = 'htxuanthanh.star.move.vn@gmail.com';
        $subject    = 'TEST';
        $data       = array('name'=>'Thành', 'body' => 'OK');
        $template   = 'email.default';
        Utils::sendMail($from, $to, $subject, $data, $template);*/

        /*phpinfo();
        echo "<pre>".var_export($_SERVER)."</pre>";
        die();*/

        /*$filtering = array();
        $user_pref = UserPref::where(['user_id' => Auth::id(), 'key' => 'filtering'])->first();
        if (!empty($user_pref) && !empty($user_pref->value))
            $filtering = json_decode($user_pref->value);
        dd($filtering);

        die("1");*/
    }


}
