<?php

namespace App\Http\Controllers\Report;


use App\Department;
use App\Http\Controllers\Controller;
use App\User;
use App\Utils;
use Illuminate\Http\Request;
use App\Lead;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use PHPExcel;
use PHPExcel_IOFactory;

class LeadConvertController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request)
    {
        $data = null;
        if(!empty($request->from_date) && !empty($request->to_date)){
            $data = array();
            $from_date = date('Y-m-d', strtotime(str_replace('/','-',$request->from_date))) . ' 00:00:00';
            $to_date = date('Y-m-d', strtotime(str_replace('/','-',$request->to_date))) . ' 23:59:59';
            $query = DB::table('fptu_lead AS t')
                ->select('t.*',
                    DB::raw("d.name AS department"),
                    DB::raw("(SELECT user_id FROM fptu_lead_assignment WHERE lead_id = t.id AND deleted_at IS NULL LIMIT 1) AS assignee"),
                    DB::raw("TIMESTAMPDIFF(DAY, t.created_at, t.enrolled_at) AS datediff")
                )
                ->leftJoin('fptu_department AS d', 'd.id', '=', 't.department_id')
                ->whereNull('deleted_at')
                ->whereRaw("enrolled_at >= '$from_date' AND enrolled_at <= '$to_date'")
                ->orderBy('d.sort_index', 'ASC')
            ;

            if (!in_array(Department::HOLDING, Auth::user()->getManageDepartments()) && !Auth::user()->hasRole('holding')) {
                $departments_str = implode(',', Auth::user()->getManageDepartments());
                $query->whereRaw("(t.department_id IN ($departments_str)
                    OR t.id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                    OR t.id IN (SELECT user_id FROM group_user WHERE group_id IN (
                        SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                    ))
                )");
            }else{
                if(!empty($request->department_id)){
                    $query->whereRaw("(t.department_id = $request->department_id
                        OR t.id IN (SELECT user_id FROM user_department WHERE department_id = $request->department_id)   
                        OR t.id IN (SELECT user_id FROM group_user WHERE group_id IN (
                            SELECT group_id FROM group_department WHERE department_id = $request->department_id
                        ))
                    )");
                }
            }
            if(!empty($request->assignee_id)){
                $query->whereRaw("t.id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = $request->assignee_id AND deleted_at IS NULL)");
            }
            $leads = $query->get();
            $users = User::all()->pluck('username', 'id')->toArray();
            if(!empty($leads)){
                foreach ($leads as $lead){
                    if(!isset($data[$lead->department_id])){
                        $data[$lead->department_id] = array(
                            'name' => $lead->department,
                            'total' => 0,
                            '1_week_convert' => 0,
                            '2_week_convert' => 0,
                            '3_week_convert' => 0,
                            'above_3_week_convert' => 0,
                            'detail' => array(),
                        );
                    }
                    $assignee = !empty($lead->assignee) ? $lead->assignee : $lead->created_by;
                    if(!isset($data[$lead->department_id]['detail'][$assignee])){
                        $data[$lead->department_id]['detail'][$assignee] = array(
                            'name' => $users[$assignee],
                            'total' => 0,
                            '1_week_convert' => 0,
                            '2_week_convert' => 0,
                            '3_week_convert' => 0,
                            'above_3_week_convert' => 0,
                        );
                    }
                    $data[$lead->department_id]['total']++;
                    $data[$lead->department_id]['detail'][$assignee]['total']++;
                    if($lead->datediff <= 7){
                        $data[$lead->department_id]['1_week_convert']++;
                        $data[$lead->department_id]['detail'][$assignee]['1_week_convert']++;
                    }
                    if($lead->datediff > 7 && $lead->datediff <= 14){
                        $data[$lead->department_id]['2_week_convert']++;
                        $data[$lead->department_id]['detail'][$assignee]['2_week_convert']++;
                    }
                    if($lead->datediff > 14 && $lead->datediff <= 21){
                        $data[$lead->department_id]['3_week_convert']++;
                        $data[$lead->department_id]['detail'][$assignee]['3_week_convert']++;
                    }
                    if($lead->datediff > 21){
                        $data[$lead->department_id]['above_3_week_convert']++;
                        $data[$lead->department_id]['detail'][$assignee]['above_3_week_convert']++;
                    }
                }
            }
        }
        session()->flashInput($request->input());
        return view('report.lead_convert', array(
            'data' => $data,
        ));
    }


    public function export(Request $request)
    {
        if(!empty($request->from_date) && !empty($request->to_date)){
            $data = array();
            $from_date = date('Y-m-d', strtotime(str_replace('/','-',$request->from_date))) . ' 00:00:00';
            $to_date = date('Y-m-d', strtotime(str_replace('/','-',$request->to_date))) . ' 23:59:59';
            $query = DB::table('fptu_lead AS t')
                ->select('t.*',
                    DB::raw("d.name AS department"),
                    DB::raw("(SELECT user_id FROM fptu_lead_assignment WHERE lead_id = t.id AND deleted_at IS NULL LIMIT 1) AS assignee"),
                    DB::raw("TIMESTAMPDIFF(DAY, t.created_at, t.enrolled_at) AS datediff")
                )
                ->leftJoin('fptu_department AS d', 'd.id', '=', 't.department_id')
                ->whereNull('deleted_at')
                ->whereRaw("enrolled_at >= '$from_date' AND enrolled_at <= '$to_date'")
                ->orderBy('d.sort_index', 'ASC')
            ;

            if (!in_array(Department::HOLDING, Auth::user()->getManageDepartments()) && !Auth::user()->hasRole('holding')) {
                $departments_str = implode(',', Auth::user()->getManageDepartments());
                $query->whereRaw("(t.department_id IN ($departments_str)
                    OR t.id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                    OR t.id IN (SELECT user_id FROM group_user WHERE group_id IN (
                        SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                    ))
                )");
            }else{
                if(!empty($request->department_id)){
                    $query->whereRaw("(t.department_id = $request->department_id
                        OR t.id IN (SELECT user_id FROM user_department WHERE department_id = $request->department_id)   
                        OR t.id IN (SELECT user_id FROM group_user WHERE group_id IN (
                            SELECT group_id FROM group_department WHERE department_id = $request->department_id
                        ))
                    )");
                }
            }
            if(!empty($request->assignee_id)){
                $query->whereRaw("t.id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = $request->assignee_id AND deleted_at IS NULL)");
            }
            $leads = $query->get();
            $users = User::all()->pluck('username', 'id')->toArray();
            if(!empty($leads)){
                foreach ($leads as $lead){
                    if(!isset($data[$lead->department_id])){
                        $data[$lead->department_id] = array(
                            'name' => $lead->department,
                            'total' => 0,
                            '1_week_convert' => 0,
                            '2_week_convert' => 0,
                            '3_week_convert' => 0,
                            'above_3_week_convert' => 0,
                            'detail' => array(),
                        );
                    }
                    $assignee = !empty($lead->assignee) ? $lead->assignee : $lead->created_by;
                    if(!isset($data[$lead->department_id]['detail'][$assignee])){
                        $data[$lead->department_id]['detail'][$assignee] = array(
                            'name' => $users[$assignee],
                            'total' => 0,
                            '1_week_convert' => 0,
                            '2_week_convert' => 0,
                            '3_week_convert' => 0,
                            'above_3_week_convert' => 0,
                        );
                    }
                    $data[$lead->department_id]['total']++;
                    $data[$lead->department_id]['detail'][$assignee]['total']++;
                    if($lead->datediff <= 7){
                        $data[$lead->department_id]['1_week_convert']++;
                        $data[$lead->department_id]['detail'][$assignee]['1_week_convert']++;
                    }
                    if($lead->datediff > 7 && $lead->datediff <= 14){
                        $data[$lead->department_id]['2_week_convert']++;
                        $data[$lead->department_id]['detail'][$assignee]['2_week_convert']++;
                    }
                    if($lead->datediff > 14 && $lead->datediff <= 21){
                        $data[$lead->department_id]['3_week_convert']++;
                        $data[$lead->department_id]['detail'][$assignee]['3_week_convert']++;
                    }
                    if($lead->datediff > 21){
                        $data[$lead->department_id]['above_3_week_convert']++;
                        $data[$lead->department_id]['detail'][$assignee]['above_3_week_convert']++;
                    }
                }
            }

            if(!empty($data)){
                $file_name = "Báo cáo chuyển đổi lead NE từ $from_date to $to_date ".date('YmdHis').".xlsx";

                $excel = new PHPExcel();
                $i=1;
                $excel->setActiveSheetIndex(0)
                    ->setCellValue("A$i", 'Cơ sở')
                    ->setCellValue("B$i", 'TVTS')
                    ->setCellValue("C$i", 'Tổng số Lead NE')
                    ->setCellValue("D$i", 'Chuyển đổi NE trong 1 tuần')
                    ->setCellValue("E$i", 'Chuyển đổi NE trong 2 tuần')
                    ->setCellValue("F$i", 'Chuyển đổi NE trong 3 tuần')
                    ->setCellValue("G$i", 'Chuyển đổi NE hơn 3 tuần')
                ;
                $excel->getActiveSheet()->getStyle("A$i:G$i")->getFont()->setBold(TRUE);
                $excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                $excel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
                $excel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
                $excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
                $excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);

                foreach ($data as $key => $item){
                    if(!empty($item['detail'])){
                        foreach ($item['detail'] as $sub_key => $sub_item){
                            $i++;
                            $excel->setActiveSheetIndex(0)
                                ->setCellValue("A$i", $item['name'])
                                ->setCellValue("B$i", $sub_item['name'])
                                ->setCellValue("C$i", $sub_item['total'])
                                ->setCellValue("D$i", $sub_item['1_week_convert'])
                                ->setCellValue("E$i", $sub_item['2_week_convert'])
                                ->setCellValue("F$i", $sub_item['3_week_convert'])
                                ->setCellValue("G$i", $sub_item['above_3_week_convert'])
                            ;
                        }
                    }
                    $i++;
                    $excel->setActiveSheetIndex(0)
                        ->setCellValue("A$i", $item['name'])
                        ->setCellValue("B$i", 'Tổng')
                        ->setCellValue("C$i", $item['total'])
                        ->setCellValue("D$i", $item['1_week_convert'])
                        ->setCellValue("E$i", $item['2_week_convert'])
                        ->setCellValue("F$i", $item['3_week_convert'])
                        ->setCellValue("G$i", $item['above_3_week_convert'])
                    ;
                    $excel->getActiveSheet()->getStyle("A$i:G$i")->getFont()->setBold(TRUE);
                }

                header("Content-Type: application/vnd.ms-excel");
                header("Content-Disposition: attachment;filename=$file_name");
                header("Cache-Control: max-age=0");
                $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
                $writer->save('php://output');
            }else{
                die('Không có dữ liệu');
            }
        }
    }
}