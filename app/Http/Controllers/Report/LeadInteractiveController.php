<?php

namespace App\Http\Controllers\Report;

use App\Lead;
use App\Lead\CallLog;
use App\Report\LeadInteractive;
use Illuminate\Http\Request;
use App\Department;
use App\User;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use PHPExcel;
use PHPExcel_IOFactory;

class LeadInteractiveController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $model = new LeadInteractive();

        $from_date = $request->get('from_date');
        $to_date = $request->get('to_date');
        $department_id = $request->get('department_id');
        $user_id = $request->get('user_id');
        $assignee_id = $request->get('assignee_id');
        $lead_id = $request->get('lead_id');
        $interact = $request->get('interact');

        if(!empty($from_date))          $model->start_date = $from_date;
        if(!empty($to_date))            $model->end_date = $to_date;
        if(!empty($department_id))      $model->department_id = $department_id;
        if(!empty($user_id))            $model->user_id = $user_id;
        if(!empty($assignee_id))        $model->assignee_id = $assignee_id;
        if(!empty($lead_id))            $model->lead_id = $lead_id;
        if(!empty($interact))           $model->interact = $interact;

        $data = $model->search();

        session()->flashInput($request->input());
        return view('report.lead_interactive', [
            'data' => $data,
        ]);
    }

    public function export(Request $request)
    {
        $model = new LeadInteractive();

        $from_date = $request->get('from_date');
        $to_date = $request->get('to_date');
        $department_id = $request->get('department_id');
        $user_id = $request->get('user_id');
        $assignee_id = $request->get('assignee_id');
        $lead_id = $request->get('lead_id');
        $interact = $request->get('interact');

        if(!empty($from_date))          $model->start_date = $from_date;
        if(!empty($to_date))            $model->end_date = $to_date;
        if(!empty($department_id))      $model->department_id = $department_id;
        if(!empty($user_id))            $model->user_id = $user_id;
        if(!empty($assignee_id))        $model->assignee_id = $assignee_id;
        if(!empty($lead_id))            $model->lead_id = $lead_id;
        if(!empty($interact))           $model->interact = $interact;

        $data = $model->search();

        if(!empty($data)){
            $file_name = "Chỉ số thời gian chăm sóc Lead.xlsx";
            $user_list = User::all()->pluck('username', 'id');
            $department_list = Department::all()->pluck('name', 'id');

            $excel = new PHPExcel();
            $i=1;
            $excel->setActiveSheetIndex(0)->setTitle('Lead Interactivity')
                ->setCellValue("A$i", 'Stt')
                ->setCellValue("B$i", 'Lead')
                ->setCellValue("C$i", 'SĐT')
                ->setCellValue("D$i", 'Người tạo')
                ->setCellValue("E$i", 'Người phụ trách')
                ->setCellValue("F$i", 'Cơ sở')
                ->setCellValue("G$i", 'Ngày tạo')
                ->setCellValue("H$i", 'Ngày đầu tiên tương tác')
                ->setCellValue("I$i", 'Thời gian tương tác (ngày)')
            ;
            foreach ($data as $lead){
                $i++;
                $name = $lead->name ? $lead->name : ($lead->student_name ? $lead->student_name : $lead->nick_name);
                $phone = $lead->phone1 ? $lead->phone1 : $lead->phone2;
                $created_by = isset($user_list[$lead->created_by]) ? $user_list[$lead->created_by] : '';
                $assignee = isset($user_list[$lead->assignee]) ? $user_list[$lead->assignee] : '';
                $department = isset($department_list[$lead->department_id]) ? $department_list[$lead->department_id] : '';
                $created_at = $lead->created_at;
                $first_interact = $lead->first_call_log_time;
                $diff_day = $lead->diff_day;

                $excel->setActiveSheetIndex(0)
                    ->setCellValue("A$i", $i-1)
                    ->setCellValue("B$i", $name)
                    ->setCellValue("C$i", $phone)
                    ->setCellValue("D$i", $created_by)
                    ->setCellValue("E$i", $assignee)
                    ->setCellValue("F$i", $department)
                    ->setCellValue("G$i", $created_at)
                    ->setCellValue("H$i", $first_interact)
                    ->setCellValue("I$i", $diff_day)
                ;
            }

            header("Content-Type: application/vnd.ms-excel");
            header("Content-Disposition: attachment;filename=$file_name");
            header("Cache-Control: max-age=0");
            $writer = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
            $writer->save('php://output');
        }else{
            die('Không có dữ liệu');
        }
    }

}
