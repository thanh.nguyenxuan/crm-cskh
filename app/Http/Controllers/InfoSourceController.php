<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InfoSource;

class InfoSourceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $info_source_list = array();
        $info_source_list = InfoSource::all();
        return view('info_source.index', ['info_source_list' => $info_source_list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('info_source.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = 0;
        $return_message = '';
        $found = InfoSource::where('name', $request->name)
            ->count();
        $error = false;
        if ($found) {
            $return_message = 'Tên nguồn thông tin đã tồn tại.';
            $error = true;
        } else {
            $return_message = 'Tạo nguồn thông tin mới thành công!';
            $success = 1;
            $info_source = new InfoSource();
            $info_source->name = trim($request->name);
            $saved = $info_source->save();
            if (!$saved) {
                $return_message = 'Đã có lỗi trong quá trình lưu dữ liệu!';
                $success = 0;
                $error = true;
            }
        }
        $request->flash();
        if ($error)
            return view('info_source.create', ['success' => $success, 'message' => $return_message]);
        return view('info_source.edit', ['info_source' => $info_source, 'success' => $success, 'message' => $return_message]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $info_source = InfoSource::find($id);
        return view('info_source.edit', ['info_source' => $info_source]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $success = 0;
        $info_source = InfoSource::find($id);
        $error = false;
        $return_message = '';

        if (!$error) {
            $update = array(
                'name' => $request->name
            );

            $updated = $info_source->update($update);
            if ($updated) {
                $return_message = 'Đã cập nhật dữ liệu thành công!';
                $success = 1;
            } else {
                $return_message = 'Đã có lỗi trong quá trình cập nhật dữ liệu.';
                $success = 0;
            }
        }
        //$user = User::find($id);
        return view('info_source.edit', ['success' => $success, 'info_source' => $info_source, 'message' => $return_message]);
    }

    public function toggle($id)
    {
        $success = 0;
        $info_source = InfoSource::find($id);
        $error = false;
        $return_message = '';
        $update = array();
        if ($info_source->hide == 1) {
            $info_source->hide = 0;
        } else {
            $info_source->hide = 1;
        }

        $updated = $info_source->save();
        if ($updated) {
            $return_message = 'Đã cập nhật trạng thái lớp thành công!';
            $success = 1;
        } else {
            $return_message = 'Đã có lỗi trong quá trình cập nhật dữ liệu.';
            $success = 0;
        }
        $info_source_list = array();
        $info_source_list = InfoSource::all();
        return view('info_source.index', ['success' => $success, 'message' => $return_message, 'info_source_list' => $info_source_list]);
    }
}
