<?php

namespace App\Http\Controllers;

use App\District;
use App\Utils;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    public function getDistrictByProvince(Request $request)
    {
        if(!empty($request->province)){
            $data = District::listData($request->province);
        }else{
            $data = array();
        }
        echo Utils::generateOptions($data, null, 'Chọn quận/huyện');
    }
}
