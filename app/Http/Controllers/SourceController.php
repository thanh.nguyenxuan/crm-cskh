<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Source;

class SourceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $source_list = array();
        $source_list = Source::all();
        return view('source.index', ['source_list' => $source_list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('source.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = 0;
        $return_message = '';
        $found = Source::where('name', $request->name)
            ->count();
        $error = false;
        if ($found) {
            $return_message = '<div>Tên nguồn dẫn đã tồn tại.</div>';
            $error = true;
        } else {
            $return_message = 'Tạo nguồn dẫn mới thành công!';
            $success = 1;
            $source = new Source();
            $source->name = trim($request->name);
            $saved = $source->save();
            if (!$saved) {
                $return_message = 'Đã có lỗi trong quá trình lưu dữ liệu!';
                $success = 0;
                $error = true;
            }
        }
        $request->flash();
        if ($error)
            return view('source.create', ['success' => $success, 'message' => $return_message]);
        return view('source.edit', ['source' => $source, 'success' => $success, 'message' => $return_message]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $source = Source::find($id);
        return view('source.edit', ['source' => $source]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $success = 0;
        $source = Source::find($id);
        $error = false;
        $return_message = '';

        if (!$error) {
            $source->name = trim($request->name);
            $updated = $source->save();
            if ($updated) {
                $return_message = 'Đã cập nhật dữ liệu thành công.';
                $success = 1;
            } else {
                $return_message = 'Đã có lỗi trong quá trình cập nhật dữ liệu.';
                $success = 0;
            }
        }
        //$user = User::find($id);
        return view('source.edit', ['success' => $success, 'source' => $source, 'message' => $return_message]);
    }

    public function toggle($id)
    {
        $success = 0;
        $source = Source::find($id);
        $error = false;
        $return_message = '';
        $update = array();
        if ($source->hide == 1) {
            $source->hide = 0;
        } else {
            $source->hide = 1;
        }
        $updated = $source->save();
        if ($updated) {
            $return_message = 'Đã cập nhật trạng thái lớp thành công!';
            $success = 1;
        } else {
            $return_message = '<div>Đã có lỗi trong quá trình cập nhật dữ liệu.</div>';
            $success = 0;
        }
        $source_list = array();
        $source_list = Source::all();
        return view('source.index', ['success' => $success, 'message' => $return_message, 'source_list' => $source_list]);
    }
}
