<?php

namespace App\Http\Controllers\EmailMarketing;

use App\EmailMarketing\EmailAttachment;
use App\EmailMarketing\EmailTemplate;
use App\EmailMarketing\MailMerger;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\EmailMarketing\EmailCampaign;
use App\EmailMarketing\Email;
use App\EmailMarketing\EmailImporter;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailSender;
use PDF;
use Dompdf\Dompdf;
use Dompdf\Options;
use Illuminate\Support\Facades\Auth;

class EmailCampaignController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = EmailCampaign::all();
        foreach ($list as $item) {
            $item->count = Email::where('campaign_id', $item->id)->count();
        }
        //var_dump($list);
        return view('email.campaign.index', ['list' => $list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function import(Request $request, $id)
    {
        $list = EmailCampaign::find($id);
        if ($request->isMethod('get')) {
            return view('email.campaign.import', ['list' => $list]);
        }
        if ($request->isMethod('post')) {
            $importer = new EmailImporter;
            $result = $importer->import($id, $request->file('import')->path());
            //var_dump($result);
            return view('email.campaign.import', ['list' => $list, 'result' => $result]);
        }
    }

    public function show($id)
    {
        $detail = EmailCampaign::find($id);
        //var_dump($detail);
        $has_image = 0;
        $valid_email = 0;
        if (!empty($detail)) {
            $detail->count = Email::where('campaign_id', $id)->count();
            $detail->sent_count = Email::where('campaign_id', $id)->where('status', 'sent')->whereNull('deleted_at')->count();
            $detail->opened_count = Email::where('campaign_id', $id)->whereNotNull('opened_at')->whereNull('deleted_at')->count();
            $detail->last_sent = Email::where('campaign_id', $id)->where('status', 'sent')->orderBy('sent_at', 'DESC')->pluck('sent_at')->first();
            $email_list = Email::where('campaign_id', $id)->orderBy('sent_at', 'DESC')->orderBy('status', 'DESC')->get();
            //var_dump($email_list);
            foreach ($email_list as $email) {
                $attachments = EmailAttachment::where('email_campaign_id', $id)->get();
                for ($i = 0; $i < count($attachments); $i++) {
                    $attachment = $attachments[$i];
                    if ($attachment->personalized) {
                        $file_path = public_path() . '/files/attachments/' . DIRECTORY_SEPARATOR . $attachment->email_campaign_id . DIRECTORY_SEPARATOR . $email->id . DIRECTORY_SEPARATOR . $attachment->id . '.' . $attachment->file_type;
                        $attachment->file_path = env('APP_URL') . '/files/attachments/' . DIRECTORY_SEPARATOR . $attachment->email_campaign_id . DIRECTORY_SEPARATOR . $email->id . DIRECTORY_SEPARATOR . $attachment->id . '.' . $attachment->file_type;
                    } else {
                        $file_path = public_path() . '/files/attachments/' . DIRECTORY_SEPARATOR . $attachment->email_campaign_id . DIRECTORY_SEPARATOR . $attachment->file_name;
                        $attachment->file_path = env('APP_URL') . '/files/attachments/' . DIRECTORY_SEPARATOR . $attachment->email_campaign_id . DIRECTORY_SEPARATOR . $attachment->file_name;
                    }
                    if (file_exists($file_path))
                        $attachment->status = 'found';
                    else
                        $attachment->status = 'notfound';
                }
                $email->attachments = $attachments;
            }
            //var_dump($email_list);
            foreach ($email_list as $email) {
                if (!empty(trim($email->image)))
                    $has_image++;
                // if(!empty(trim($email->image)) && !empty(trim($email->id_passport)) && !empty(trim($email->email)))
                // $valid_email++;
            }
            $detail->email_list = $email_list;
            //var_dump($email_list);
        }
        return view('email.campaign.show', ['id' => $id, 'detail' => $detail, 'has_image' => $has_image]);
    }

    public function showPdf($id)
    {
        $email = Email::find($id);
        //var_dump($id);
        //var_dump(Email::find($id));
        // switch($email->branch) {
        // case 'HN':
        // $email->place = 'Hà Nội';
        // break;
        // case 'HCM':
        // $email->place = 'Hồ Chí Minh';
        // break;
        // case 'DN':
        // $email->place = 'Đà Nẵng';
        // break;
        // }
        return view('email.templates.2018.ThuBaoKQXetTuyenPdf', ['data' => json_decode($email->data)]);
    }

    public function genAllPdf($id)
    {
        set_time_limit(-1);
        $email_list = Email::where('campaign_id', $id)->get();
        foreach ($email_list as $email) {
            if (empty($email->sent_at))
                $this->genPdf($email->id);
        }
    }

    public function regenAllPdf($id)
    {
        set_time_limit(-1);
        $email_list = Email::where('campaign_id', $id)->get();
        foreach ($email_list as $email) {
            $this->genPdf($email->id, true);
        }
    }

    public function genPdf($id, $regen = false)
    {
        set_time_limit(-1);
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $email = Email::find($id);
        // switch($email->branch) {
        // case 'HN':
        // $email->place = 'Hà Nội';
        // break;
        // case 'HCM':
        // $email->place = 'Hồ Chí Minh';
        // break;
        // case 'DN':
        // $email->place = 'Đà Nẵng';
        // break;
        // }
        // $pdf = PDF::loadView('email.templates.TBGiayChungNhanKQThi_DoXetHB', ['email' => $email], array(), 'UTF-8');
        // PDF::setOptions(['defaultFont' => 'serif', 'isFontSubsettingEnabled' => true]);
        if (!empty($email) && !empty($email->campaign_id)) {
            //$campaign = EmailCampaign::find($email->campaign_id);
            $attachments = EmailAttachment::where('email_campaign_id', $email->campaign_id)->get();
            foreach ($attachments as $attachment) {
                if ($attachment->personalized == 1) {
                    $export_dir = $attachment->path . '/' . $email->campaign_id . DIRECTORY_SEPARATOR . $email->id;
                    $export_path = $export_dir . '/' . $attachment->id . '.' . $attachment->file_type;
                    var_dump($export_path);
                    if ($attachment->file_type == 'pdf' && (!file_exists($export_path) || $regen)) {
                        echo 'Gen file PDF....';
                        $options = new Options();
                        $options->set('defaultFont', 'Times');
                        $dompdf = new Dompdf($options);
                        $template = null;
                        if (!empty($attachment->email_template_id)) {
                            $template = EmailTemplate::find($attachment->email_template_id);
                            $template = $template->getLatestRevision();
                        }
                        //var_dump($template);
                        //var_dump($attachment->email_template_id);
                        if (!empty($template)) {
                            $emailMerger = new MailMerger();
                            $template_content = '<html><head><link rel="stylesheet" type="text/css" href="/var/www/html/public/js/ckeditor/contents.css"></head><body>';
                            $template_content .= $template->content;
                            $template_content .= '</body></html>';
                            $dompdf->loadHtml($emailMerger->parse($template_content, json_decode($email->data)), 'UTF-8');
                            $dompdf->setPaper('A4');
                            $dompdf->render();
                            $output = $dompdf->output();
                            if (!file_exists($export_dir)) {
                                mkdir($export_dir, 0777, true);
                            }
                            if (file_exists($export_dir) && is_writable($export_dir)) {
                                file_put_contents($export_path, $output);
                                echo 'Đã tạo thành công file đính kèm cho email ' . $email->email . '<br />';
                            } else {
                                echo 'Lỗi ghi file!';
                            }
                        }
                    }
                }
            }
        }

    }

    public function showEmail($id)
    {
        $template_content = '';
        $email = Email::find($id);
        //return new EmailSender($email);
        $campaign = EmailCampaign::find($email->campaign_id);
        //var_dump($email->campaign_id);
        if (!empty($campaign->template)) {
            $latest_template = $campaign->getLatestTemplateRevision();
            if (!empty($latest_template) && !empty($latest_template->id))
                $template_content = EmailTemplate::find($latest_template->id)->content;
        }
        $data = get_object_vars(json_decode($email->data));
        //var_dump($email->data);
        $mailMerger = new MailMerger();
        echo $mailMerger->parse($template_content, $data);
    }

    public function sendPrivateEmail($id, $test_email = '')
    {
        $email = Email::find($id);
        //$file = '/var/www/html/public/files/export/' . $email->list_id . '/' . $email->excel_order . '.pdf';
//        if (!file_exists($file))
//            $this->genPdf($id);
        //var_dump($id);
        //var_dump(Email::find($id));
        //return view('email.templates.GiayBaoDuThiEmail', ['email' => $email]);
        //$email = Email::find($id);
        //var_dump($email);
        $personal_emails = explode('/', $email->email);
        $bcc = array();
        $campaign = EmailCampaign::find($email->campaign_id);
        if (!empty($campaign) && !empty($campaign->id)) {
            if ($campaign->type == 'marketing') {
                //$bcc[] = 'hoangld@fpt.edu.vn';
            } elseif ($campaign->type == 'counselor') {
                $bcc[] = 'daihocfpt@fpt.edu.vn';
            } else {
                //$bcc[] = 'hoangld@fpt.edu.vn';
            }
            $reply_to = 'daihocfpt@fpt.edu.vn';
            if (!empty($email->reply_to)) {
                $reply_to = $email->reply_to;
            }
            foreach ($personal_emails as $e) {
                if (!empty($test_email))
                    $e = $test_email;
                else
                    $e = trim($e);
                if ($email->status != 'sent' || !empty($test_email)) {
                    $test = false;
                    if (!empty($test_email))
                        $test = true;
                    $mailable = new EmailSender($email, $test);
                    $mailable->replyTo($reply_to);
                    if (!empty($bcc))
                        $mailable->bcc($bcc);
                    Mail::to($e, $email->name)->queue($mailable);
                }
            }
        }
        //echo 'Đã gửi!';
        return view('email.campaign.sendemail');
    }

    public function resend($id)
    {
        set_time_limit(-1);
        $email = Email::find($id);
        //$file = '/var/www/html/public/files/export/' . $email->list_id . '/' . $email->excel_order . '.pdf';
//        if (!file_exists($file))
//            $this->genPdf($id);
        //var_dump($id);
        //var_dump(Email::find($id));
        //return view('email.templates.GiayBaoDuThiEmail', ['email' => $email]);
        //$email = Email::find($id);
        //var_dump($email);
        $personal_emails = explode('/', $email->email);
        $reply_to = 'daihocfpt@fpt.edu.vn';
        if (!empty($email->reply_to)) {
            $reply_to = $email->reply_to;
        }
        $mailable = new EmailSender($email);
        $mailable->replyTo($reply_to);
        foreach ($personal_emails as $e) {
            Mail::to($e, $email->name)->bcc('daihocfpt@fpt.edu.vn')->queue($mailable);
            echo 'Đã gửi lại email cho ' . $email->name . ' tại địa chỉ ' . $e . '<br />';
        }
        //echo 'Đã gửi!';
        return view('email.campaign.sendemail');
    }

    public function progress(Request $request, $id)
    {
        $email_sent = Email::where('list_id', $id)->where('status', 'sent')->count();
        echo '<p style="font-size: 2em; font-weight: bold;">' . $email_sent . ' sent!</p>';
    }

    public function editEmailTemplate(Request $request, $id)
    {
        $template = '';
        $saved = $request->get('save_button');
        if (!empty($id)) {
            $parent_template = EmailTemplate::find($id);
            if (!empty($parent_template) && !empty($parent_template->id))
                $latest_revision = EmailTemplate::where('parent', $id)
                    ->orderBy('created_at', 'DESC')
                    ->first();
            if (!empty($latest_revision) && !empty($latest_revision->id)) {
                $template = $latest_revision;
            } else {
                $template = $parent_template;
            }
        }
        if ($saved == 'save') {
            $content = $request->get('template_content');
            if (!empty($template) && !empty($template->id) && $content != $template->content) {
                $new_template = new EmailTemplate();
                $new_template->name = $template->name;
                $new_template->type = $template->type;
                if (empty($template->parent))
                    $new_template->parent = $template->id;
                else
                    $new_template->parent = $template->parent;
                $new_template->content = $content;
                $new_template->created_by = Auth::id();
                $new_template->created_at = date('Y-m-d H:i:s');
                $new_template->updated_at = date('Y-m-d H:i:s');
                $new_template->save();
                $template = $new_template;
            }
        }
        return view('email.campaign.edit_template', ['template' => $template]);
    }

    public function sendEmail($id)
    {
        set_time_limit(-1);
        //$email = Email::find($id);
        //var_dump($id);
        //var_dump(Email::find($id));
        //return view('email.templates.GiayBaoDuThiEmail', ['email' => $email]);
        //$email = Email::find($id);
        $campaign = EmailCampaign::find($id);
        $campaign_id = $campaign->list;
        $email_list = Email::where('campaign_id', $id)
            ->get();
        //var_dump($email_list);
        //return view('email.campaign.sendemail');
        //var_dump(new EmailSender($email));
        $stt = 0;
        foreach ($email_list as $email) {
            if ($email->status != 'sent') {
                $this->sendPrivateEmail($email->id);
                echo $stt++ . ': ' . $email->email . ' - ' . $email->name . '<br />';
                $stt++;
            }
            //Mail::to($email->email, $email->name)->send(new EmailSender($email));
            //$this->sendPrivateEmail($email->id);
        }


        echo 'Đã gửi!';
        return view('email.campaign.sendemail');
    }

    public function sendTestEmail($id)
    {
        set_time_limit(-1);
        $email = Email::find($id);
        $campaign = EmailCampaign::find($email->campaign_id);
        //$email_list = Email::where('campaign_id', $id)
        //->get();
        //var_dump($email_list);
        //return view('email.campaign.sendemail');
        //var_dump(new EmailSender($email));
        //$stt = 0;
        //foreach ($email_list as $email) {
        // if(!empty($email->image) && $email->status != 'sent') {
        // $personal_emails = explode(',', $email->email);
        // foreach($personal_emails as $e) {
        // if(!empty($e)) {
        // echo $stt. ': ' . $e.' | '. $email->name . '<br />';
        // Mail::to($e, $email->name)->send(new EmailSender($email));
        // }
        // }
        // $stt++;
        // }
        //Mail::to($email->email, $email->name)->send(new EmailSender($email));
        //echo $stt++ . ': ' . $email->email . '<br />';
        return $this->sendPrivateEmail($email->id, Auth::user()->email);
        //}

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $email_list = Email::where('campaign_id', $id)->get();
        foreach ($email_list as $email) {
            $email->delete();
        }
        return $this->show($id);
    }
}
