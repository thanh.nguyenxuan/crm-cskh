<?php

namespace App\Http\Controllers\EmailMarketing;

use App\EmailMarketing\Email;
use App\EmailMarketing\EmailHistory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;

class EmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function open(Request $request, $id)
    {
        header("Content-Type: image/png"); // it will return image
        $pixel_file = public_path('images/1px.png');
        //var_dump(file_exists($pixel_file));
        $email = Email::find($id);
        $email->opened_at = date('Y-m-d H:i:s');
        $email->save();
        $history = new EmailHistory();
        $history->email_id = $id;
        $history->event = 'open';
        $data = array();
        $ua = $request->server('HTTP_USER_AGENT');
        $data['user_agent'] = $ua;
        $history->data = json_encode($data);
        $history->created_at = date('Y-m-d H:i:s');
        $history->save();
        //return $response;
        return response()->file($pixel_file);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
