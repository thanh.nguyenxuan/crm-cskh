<?php

namespace App\Http\Controllers\EmailMarketing;

use App\EmailMarketing\EmailCampaign;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\EmailMarketing\EmailList;
use App\EmailMarketing\Email;
use App\EmailMarketing\EmailImporter;

class EmailListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$list = EmailList::all();
		foreach($list as $item) {
			$item->number = Email::where('list_id', $item->id)->count();
		}
        return view('email.list.index', ['list' => $list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
		$detail = EmailList::find($id);
		if(!empty($detail)) {
			$detail->count = Email::where('list_id', $id)->count();
			$detail->email_list = Email::where('list_id', $id)->get();
		}
        return view('email.list.show', ['id' => $id, 'detail' => $detail]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

	
	public function import(Request $request, $id)
    {
		$list = EmailCampaign::find($id);
		if($request->isMethod('get')) {
			return view('email.campaign.import', ['list' => $list]);
		}
		if($request->isMethod('post')) {
			$importer = new EmailImporter;
			$result = $importer->import($id, $request->file('import')->path());
			//var_dump($result);
			return view('email.campaign.import', ['list' => $list, 'result' => $result]);
		}		
    }
	
	public function export(Request $request, $id) {
		//$list = EmailList::find($id)->first();
		$importer = new EmailImporter;
		$email_list = Email::where('list_id', $id)->get();
		$list = array();
		foreach($email_list as $email) {
			$contact = array();
			$contact[] = $email->name;
			$contact[] = $email->birthdate;
			$contact[] = $email->id_passport;
			$contact[] = $email->id_passport;
			$contact[] = $email->phone1;
			$contact[] = $email->phone2;
			$contact[] = $email->address;
			$contact[] = $email->email;
			$list[] = $contact;
		}
		$file_name = 'Email_Export_' . $id . '.xlsx';
		$importer->dump_file($list, $file_name);
		$path = public_path() . '/uploads/' . $file_name;
		return response()->download($path);
	}
	
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
