<?php

namespace App\Http\Controllers\LeadMarketing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FacebookController extends Controller
{

    public function index(Request $request)
    {
        if (isset($request->hub_verify_token)) {
            if ($request->hub_verify_token === 'htxuanthanh_facebook_message_test') {
                echo $request->hub_challenge;
                return;
            } else {
                echo 'Invalid Verify Token';
                return;
            }
        }

        /* receive and send messages */
        $input = json_decode(file_get_contents('php://input'), true);
        if (isset($input['entry'][0]['messaging'][0]['sender']['id'])) {

            $sender = $input['entry'][0]['messaging'][0]['sender']['id']; //sender facebook id
            $message = $input['entry'][0]['messaging'][0]['message']['text']; //text that user sent
            $page_access_token = 'EAAKlmVNJd7YBAL4PGclwa4zZCCZBOKvEgfvDs4F0HHzPlS5Oue4luIG4AakJxNB7UboZBwZBVZB7voo6UWB3rO7LFBtZA3ellGyzybuSi65Cck505IYLrrvYQe7UrtHk97vt8QMCBlwjXJRWlhNZCtPg9u1uXxzXI2CDENhV4ZAVokvETEOPMSOo';

            $url = "https://graph.facebook.com/v2.6/me/messages?access_token=$page_access_token";

            /*initialize curl*/
            $ch = curl_init($url);
            /*prepare response*/
            $jsonData = '{
            "recipient":{
                "id":"' . $sender . '"
                },
                "message":{
                    "text":"You said, ' . $message . '"
                }
            }';
            /* curl setting to send a json post data */
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            if (!empty($message)) {
                $result = curl_exec($ch); // user will get the message
            }
        }
    }
}