<?php

namespace App\Http\Controllers\LeadMarketing;

use App\ChannelUtmMapping;
use App\Department;
use App\Http\Controllers\Controller;
use App\Lead;
use App\LeadCampaign;
use App\LeadChannel;
use App\LeadSource;
use App\LogSyncLead;
use App\ManualLeadAssigner;
use App\Notify;
use App\User;
use App\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class WebsiteController extends Controller
{

    public function index(Request $request)
    {
        $return = array(
            'success' => TRUE,
            'message' => 'OK',
            'data' => array(),
        );

        if(!empty($request->source)){
            if($request->source == LogSyncLead::SOURCE_SMIS){
                $this->syncLeadSMIS($request);
            }
            if($request->source == LogSyncLead::SOURCE_GIS){
                $this->syncLeadGIS($request);
            }
            if($request->source == LogSyncLead::SOURCE_IKIDS){
                $this->syncLeadIKIDS($request);
            }
            if($request->source == LogSyncLead::SOURCE_ASC){
                $this->syncLeadASC($request);
            }
            if($request->source == LogSyncLead::SOURCE_DEWEY){
                $this->syncLeadDewey($request);
            }
        }

        echo json_encode($return);
    }


    protected function syncLeadSMIS(Request $request)
    {
        $holding_department_id = Department::HOLDING;
        $department_mapping = array(
            1 => 'SMIS - Cầu Giấy, Hà Nội',
            3 => 'SMIS - Thụy Khuê, Ba Đình, Hà Nội',
            4 => 'SMIS - Lương Yên, Hai Bà Trưng, Hà Nội',
            5 => 'SMIS - Hà Đông, Hà Nội',
            6 => 'SMIS - Lê Hồng Phong, Hải Phòng',
            15=> 'SMIS - Dương Kinh, Hải Phòng',
            11=> 'SMIS - Hạ Long',
            12=> 'SMIS - Quận 2, Tp. Hồ Chí Minh',
            17=> 'SMIS - Thanh Xuân, Hà Nội',
            18=> 'SMIS - Tây Hồ Tây, Hà Nội',
            13=> 'Ikids - Thái Bình',
            19=> 'Ikids - Nam Từ Liêm, Hà Nội',

        );

        $department_mapping_popup = array(
            1 => 'SMIS - Cầu Giấy, HN',
            3 => 'SMIS - Thụy Khuê, Ba Đình, HN',
            4 => 'SMIS - Lương Yên, Hai Bà Trưng, HN',
            5 => 'SMIS - Hà Đông, HN',
            15=> 'SMIS - Dương Kinh, Hải Phòng',
            11=> 'SMIS - Hạ Long, Quảng Ninh',
            12=> 'SMIS - Quận 2, Tp. Hồ Chí Minh',
            17=> 'SMIS - Thanh Xuân, Hà Nội',
            18=> 'SMIS - Tây Hồ Tây, Hà Nội',
            13=> 'Ikids - Thái Bình',
            19=> 'Ikids - Nam Từ Liêm, HN',
        );

        $name           = $request->name;
        $phone          = $request->phone;
        $email          = $request->email;
        $department     = $request->department;
        $student_name   = $request->student_name;
        $birthday       = $request->birthday;
        $gender         = $request->gender;
        $address        = $request->address;
        $note           = $request->note;
        $source         = $request->source;
        $utm_source     = $request->utm_source;
        $utm_campaign   = $request->utm_campaign;
        $utm_medium     = $request->utm_medium;
        $utm_content    = $request->utm_content;
        $utm_term       = $request->utm_term;

        $department_id = $holding_department_id;
        if(!empty($department)){
            foreach ($department_mapping as $key => $value){
                if($department == $value){
                    $department_id = $key;
                    break;
                }
            }
            foreach ($department_mapping_popup as $key => $value){
                if($department == $value){
                    $department_id = $key;
                    break;
                }
            }
        }

        $lead = new Lead();
        $lead->name          = $name;
        $lead->phone1        = Utils::sanitizePhoneV2($phone);
        $lead->email         = $email;
        $lead->department_id = $department_id;
        $lead->student_name  = $student_name;
        $lead->birthdate     = !empty($birthday) ? date('Y-m-d', strtotime(str_replace('/','-',$birthday))) : null;
        $lead->address       = $address;
        $lead->notes         = $note;
        $lead->source        = $source;
        $lead->utm_source    = $utm_source;
        $lead->utm_campaign  = $utm_campaign;
        $lead->utm_medium    = $utm_medium;
        $lead->utm_content   = $utm_content;
        $lead->utm_term      = $utm_term;

        $this->syncLead($lead, $request);
    }


    protected function syncLeadGIS(Request $request)
    {
        $holding_department_id = Department::HOLDING;
        $department_mapping = array(
            2 => '[:vi]Gateway - Cầu Giấy, Hà Nội[:en]Gateway - Cau Giay, Ha Noi[:]',
            8 => '[:vi]Gateway - Dương Kinh, Hải Phòng[:en]Gateway - Duong Kinh, Hai Phong[:]',
        );
        $department_mapping_vi = array(
            2 => 'Gateway - Cầu Giấy, Hà Nội',
            8 => 'Gateway - Dương Kinh, Hải Phòng',
        );
        $department_mapping_en = array(
            2 => 'Gateway - Cau Giay, Ha Noi',
            8 => 'Gateway - Duong Kinh, Hai Phong',
        );

        $name           = $request->name;
        $phone          = $request->phone;
        $email          = $request->email;
        $department     = $request->department;
        $student_name   = $request->student_name;
        $birthday       = $request->birthday;
        $gender         = $request->gender;
        $address        = $request->address;
        $note           = $request->note;
        $source         = $request->source;
        $utm_source     = $request->utm_source;
        $utm_campaign   = $request->utm_campaign;
        $utm_medium     = $request->utm_medium;
        $utm_content    = $request->utm_content;
        $utm_term       = $request->utm_term;

        $department_id = $holding_department_id;
        if(!empty($department)){
            foreach ($department_mapping as $key => $value){
                if($department == $value){
                    $department_id = $key;
                    break;
                }
            }
            foreach ($department_mapping_vi as $key => $value){
                if($department == $value){
                    $department_id = $key;
                    break;
                }
            }
            foreach ($department_mapping_en as $key => $value){
                if($department == $value){
                    $department_id = $key;
                    break;
                }
            }
        }

        $lead = new Lead();
        $lead->name          = $name;
        $lead->phone1        = Utils::sanitizePhoneV2($phone);
        $lead->email         = $email;
        $lead->department_id = $department_id;
        $lead->student_name  = $student_name;
        $lead->birthdate     = !empty($birthday) ? date('Y-m-d', strtotime(str_replace('/','-',$birthday))) : null;
        $lead->address       = $address;
        $lead->notes         = $note;
        $lead->source        = $source;
        $lead->utm_source    = $utm_source;
        $lead->utm_campaign  = $utm_campaign;
        $lead->utm_medium    = $utm_medium;
        $lead->utm_content   = $utm_content;
        $lead->utm_term      = $utm_term;

        $this->syncLead($lead, $request);
    }


    protected function syncLeadIKIDS(Request $request)
    {
        $holding_department_id = Department::HOLDING;
        $department_mapping = array(
            19 => 'Ikids Montessori Nam Từ Liêm, Hà Nội',
            13 => 'Ikids Montessori Thái Bình',
        );

        $name           = $request->name;
        $phone          = $request->phone;
        $email          = $request->email;
        $department     = $request->department;
        $student_name   = $request->student_name;
        $birthday       = $request->birthday;
        $gender         = $request->gender;
        $address        = $request->address;
        $note           = $request->note;
        $source         = $request->source;
        $utm_source     = $request->utm_source;
        $utm_campaign   = $request->utm_campaign;
        $utm_medium     = $request->utm_medium;
        $utm_content    = $request->utm_content;
        $utm_term       = $request->utm_term;

        $department_id = $holding_department_id;
        if(!empty($department)){
            foreach ($department_mapping as $key => $value){
                if($department == $value){
                    $department_id = $key;
                    break;
                }
            }
        }

        $lead = new Lead();
        $lead->name          = $name;
        $lead->phone1        = Utils::sanitizePhoneV2($phone);
        $lead->email         = $email;
        $lead->department_id = $department_id;
        $lead->student_name  = $student_name;
        $lead->birthdate     = !empty($birthday) ? date('Y-m-d', strtotime(str_replace('/','-',$birthday))) : null;
        if(!empty($gender)){
            if($gender == 'Bé trai'){
                $lead->gender = 'm';
            }else if($gender == 'Bé gái'){
                $lead->gender = 'f';
            }
        }
        $lead->address       = $address;
        $lead->notes         = $note;
        $lead->source        = $source;
        $lead->utm_source    = $utm_source;
        $lead->utm_campaign  = $utm_campaign;
        $lead->utm_medium    = $utm_medium;
        $lead->utm_content   = $utm_content;
        $lead->utm_term      = $utm_term;

        $this->syncLead($lead, $request);
    }

    protected function syncLeadASC(Request $request)
    {
        $holding_department_id = Department::HOLDING;
        $department_mapping = array(
            20 => 'Aqua-tots Cầu Giấy, Hà Nội',
            21 => 'Aqua-tots Dương Kinh, Hải Phòng',
            25 => 'Aqua-tots Tây Hồ, Hà Nội',
        );

        $name           = $request->name;
        $phone          = $request->phone;
        $email          = $request->email;
        $department     = $request->department;
        $student_name   = $request->student_name;
        $birthday       = $request->birthday;
        $gender         = $request->gender;
        $address        = $request->address;
        $note           = $request->note;
        $source         = $request->source;
        $utm_source     = $request->utm_source;
        $utm_campaign   = $request->utm_campaign;
        $utm_medium     = $request->utm_medium;
        $utm_content    = $request->utm_content;
        $utm_term       = $request->utm_term;

        $department_id = $holding_department_id;
        if(!empty($department)){
            foreach ($department_mapping as $key => $value){
                if($department == $value){
                    $department_id = $key;
                    break;
                }
            }
        }

        $lead = new Lead();
        $lead->name          = $name;
        $lead->phone1        = Utils::sanitizePhoneV2($phone);
        $lead->email         = $email;
        $lead->department_id = $department_id;
        $lead->student_name  = $student_name;
        $lead->birthdate     = !empty($birthday) ? date('Y-m-d', strtotime(str_replace('/','-',$birthday))) : null;
        if(!empty($gender)){
            if($gender == 'Bé trai'){
                $lead->gender = 'm';
            }else if($gender == 'Bé gái'){
                $lead->gender = 'f';
            }
        }
        $lead->address       = $address;
        $lead->notes         = $note;
        $lead->source        = $source;
        $lead->utm_source    = $utm_source;
        $lead->utm_campaign  = $utm_campaign;
        $lead->utm_medium    = $utm_medium;
        $lead->utm_content   = $utm_content;
        $lead->utm_term      = $utm_term;

        $this->syncLead($lead, $request);
    }

    protected function syncLeadDewey(Request $request)
    {
        $holding_department_id = Department::HOLDING;
        $department_mapping = array(
            2   => 'dewey-cau-giay-ha-noi',
            16  => 'dewey-tay-ho-tay-ha-noi',
        );

        $name           = $request->name;
        $phone          = $request->phone;
        $email          = $request->email;
        $department     = $request->department;
        $student_name   = $request->student_name;
        $birthday       = $request->birthday;
        $gender         = $request->gender;
        $address        = $request->address;
        $note           = $request->note;
        $source         = $request->source;
        $utm_source     = $request->utm_source;
        $utm_campaign   = $request->utm_campaign;
        $utm_medium     = $request->utm_medium;
        $utm_content    = $request->utm_content;
        $utm_term       = $request->utm_term;

        $department_id = $holding_department_id;
        if(!empty($department)){
            foreach ($department_mapping as $key => $value){
                if($department == $value){
                    $department_id = $key;
                    break;
                }
            }
        }

        $lead = new Lead();
        $lead->name          = $name;
        $lead->phone1        = Utils::sanitizePhoneV2($phone);
        $lead->email         = $email;
        $lead->department_id = $department_id;
        $lead->student_name  = $student_name;
        $lead->birthdate     = !empty($birthday) ? date('Y-m-d', strtotime(str_replace('/','-',$birthday))) : null;
        $lead->address       = $address;
        $lead->notes         = $note;
        $lead->source        = $source;
        $lead->utm_source    = $utm_source;
        $lead->utm_campaign  = $utm_campaign;
        $lead->utm_medium    = $utm_medium;
        $lead->utm_content   = $utm_content;
        $lead->utm_term      = $utm_term;

        $this->syncLead($lead, $request);
    }


    protected function syncLead(Lead $lead, Request $request)
    {
        $logSyncLead = new LogSyncLead();
        $logSyncLead->data = json_encode($request->all());
        $logSyncLead->source = $lead->source;
        $logSyncLead->created_at = date('Y-m-d H:i:s');
        $number = $lead->phone1;
        $source_id = 1; //website
        $lead->checkSpam();
        if(!$lead->checkDuplicate($number)){
            $logSyncLead->duplicate = 0;
            $user = $lead->getAssignee();
            $lead->status = 1; //mới
            $lead->created_at = date('Y-m-d H:i:s');
            $lead->created_by = 4; // hệ thống
            if($lead->save()){
                $lead_source = new LeadSource;
                $lead_source->lead_id = $lead->id;
                $lead_source->source_id = $source_id;
                $lead_source->created_by = $lead->created_by;
                $lead_source->created_at = date('Y-m-d H:i:s');
                $lead_source->save();

                $assigner = new ManualLeadAssigner();
                $assigner->user_id = $user->id;
                $assigner->lead_id = $lead->id;
                $assigner->created_by = $lead->created_by;
                $assigner->save();

                $campaign_id = 1; //TS
                if($campaign_id){
                    $lead_campaign = new LeadCampaign();
                    $lead_campaign->lead_id = $lead->id;
                    $lead_campaign->campaign_id =$campaign_id;
                    $lead_campaign->created_at = date('Y-m-d H:i:s');
                    $lead_campaign->save();
                }

                if(!empty($lead->utm_source)){
                    $channel_id = ChannelUtmMapping::getChannelIdByUtm($lead->utm_source);
                    if (!empty($channel_id)) {
                        $lead_channel = new LeadChannel();
                        $lead_channel->lead_id = $lead->id;
                        $lead_channel->channel_id = $channel_id;
                        $lead_channel->created_by = $lead->created_by;;
                        $lead_channel->created_at = date('Y-m-d H:i:s');
                        $lead_channel->save();
                    }
                }

                $logSyncLead->status = LogSyncLead::STATUS_SUCCESS;
                $logSyncLead->lead_id = $lead->id;

                $lead->exchangeSuspect();

            }else{
                $logSyncLead->status = LogSyncLead::STATUS_FAIL;
            }
        }else{
            $logSyncLead->status = LogSyncLead::STATUS_SUCCESS;
            $logSyncLead->duplicate = 1;
            $lead_duplicate = Lead::where(function ($query) use ($number) {
                $query->where('phone1', $number)->orWhere('phone2', $number)->orWhere('phone3', $number)->orWhere('phone4', $number);
            })
            ->whereNull('deleted_at')
            ->where('department_id', $lead->department_id)
            ->first();

            if(!empty($lead->notes)){
                if (strpos($lead_duplicate->notes, $lead->notes) === false) {
                    $lead_duplicate->notes .= "\n". date('Y-m-d H:i:s') .': '.$lead->notes;
                    $lead_duplicate->save();
                }
            }

            $lead_source = LeadSource::whereNull('deleted_at')
                ->where('lead_id', $lead_duplicate->id)
                ->where('source_id', $source_id)
                ->first();
            if(!$lead_source){
                $lead_source = new LeadSource();
                $lead_source->lead_id = $lead_duplicate->id;
                $lead_source->source_id = $source_id;
                $lead_source->created_by = $lead_duplicate->created_by;
                $lead_source->created_at = date('Y-m-d H:i:s');
                $lead_source->save();
            }

            $campaign_id = 1;
            if($campaign_id){
                $lead_campaign = LeadCampaign::whereNull('deleted_at')
                    ->where('lead_id',$lead_duplicate->id)
                    ->first();
                if(!$lead_campaign){
                    $lead_campaign = new LeadCampaign();
                    $lead_campaign->lead_id = $lead_duplicate->id;
                    $lead_campaign->campaign_id = $campaign_id;
                    $lead_campaign->created_at = date('Y-m-d H:i:s');
                    $lead_campaign->save();
                };
            }

            $logSyncLead->lead_id = $lead_duplicate->id;
            $user = User::getLeadAssignee($lead_duplicate->id);
        }

        $logSyncLead->save();

        if($logSyncLead->status != LogSyncLead::STATUS_FAIL){
            if($user && $user->active == 1){
                $notify = new Notify();
                $notify->content = ($logSyncLead->duplicate == 0)
                    ? "Lead \"$lead->name\" mới đăng ký từ website $logSyncLead->source"
                    : "Lead \"$lead->name\" trùng, vừa đăng ký từ website $logSyncLead->source";
                $notify->content.= "<br/>Chi tiết: " .env('APP_URL') ."/lead/edit/$logSyncLead->lead_id";
                $notify->user_id = $user->id;
                $notify->status = Notify::STATUS_NEW;
                $notify->created_at = date('Y-m-d H:i:s');
                $notify->lead_id = $logSyncLead->lead_id;
                $notify->lead_duplicate = $logSyncLead->duplicate;
                $notify->type = Notify::TYPE_SYNC_LEAD;
                $notify->save();

                //mail
                if($logSyncLead->duplicate == 0 && !empty($user->email))
                {
                    $from       = 'CRM';
                    $to         = $user->email;
                    $subject    = "Thông báo Lead mới";
                    $data       = array(
                        'title'         => $subject,
                        'name'          => $lead->name,
                        'phone'         => $lead->phone1,
                        'created_at'    => date("d/m/Y H:i", strtotime($lead->created_at)),
                        'source'        => 'website ' . $logSyncLead->source,
                        'url'           => env('APP_URL').'/lead/edit/'.$lead->id,
                    );
                    $template   = 'email.lead.lead_new';

                    Mail::send($template, $data, function($message) use ($from, $to, $subject) {
                        $message
                            ->from('crm@edufit.vn',$from)
                            ->to($to)
                            ->subject($subject);
                    });
                }
            }
        }
    }
}