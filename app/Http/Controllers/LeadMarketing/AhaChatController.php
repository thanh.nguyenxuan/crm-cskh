<?php

namespace App\Http\Controllers\LeadMarketing;

use App\Campaign;
use App\Http\Controllers\Controller;
use App\Lead;
use App\LeadCampaign;
use App\LeadSource;
use App\LogSyncLead;
use App\ManualLeadAssigner;
use App\Notify;
use App\User;
use App\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class AhaChatController extends Controller
{

    public function index(Request $request)
    {
        $return = array(
            'success' => true,
            'message' => 'OK',
            'data' => array(),
        );

        $this->syncLead($request);

        echo json_encode($return);
    }


    protected function syncLead(Request $request)
    {
        $name               = !empty($request->name) ? $request->name : '';
        $phone              = !empty($request->phone) ? Utils::sanitizePhoneV2($request->phone) : '';
        $email              = !empty($request->email) ? $request->email : '';
        $note               = !empty($request->note) ? $request->note : '';
        $page_name          = !empty($request->page_name) ? $request->page_name : '';
        $department_id      = !empty($request->department_id) ? $request->department_id : '';

        $utm_source         = 'AhaChat';

        if(strpos($phone, '+84') !== FALSE){
            $phone = str_replace('+84', '0', $phone);
        }

        if(empty($page_name) && empty($department_id)){
           return;
        }

        if(empty($department_id)) {
            $department_id = 14; //Mặc định Holding
            if(!empty($page_name)){
                $mapping = array(
//                '' => 'Sakura Montessori International School',
//                '' => 'Ikids Montessori School',
                    1   => 'Sakura Montessori International School - Cầu Giấy, Hà Nội',
                    3   => 'Sakura Montessori International School - Thụy Khuê, Tây Hồ',
                    4   => 'Sakura Montessori International School - Lương Yên, Hai Bà Trưng, Hà Nội',
                    5   => 'Sakura Montessori International School - Hà Đông, Hà Nội',
                    15  => 'Sakura Montessori International School - Hải Phòng',
                    11  => 'Sakura Montessori International School - Hạ Long',
                    12  => 'Sakura Montessori International School - Tp.Hồ Chí Minh',
                    17  => 'Sakura Montessori International School - Thanh Xuân, Hà Nội',
                    18  => 'Sakura Montessori International School - Tây Hồ Tây, Hà Nội',
                    13  => 'Ikids Montessori School - Thái Bình',
                    19  => 'Ikids Montessori School - Nam Từ Liêm, Hà Nội',
                );
                $mapping_dewey = array(
                    2   => 'The Dewey Schools',
                );
                foreach ($mapping as $key => $value){
                    if($value == $request->page_name){
                        $department_id = $key;
                        break;
                    }
                }
                foreach ($mapping_dewey as $key => $value){
                    if($value == $request->page_name){
                        $department_id = $key;
                        break;
                    }
                }
            }
        }

        $lead = new Lead();
        $lead->name          = $name;
        $lead->phone1        = $phone;
        $lead->email         = $email;
        $lead->department_id = $department_id;
        $lead->notes         = $note;
        $lead->utm_source    = $utm_source;

        $logSyncLead = new LogSyncLead();
        $logSyncLead->data = json_encode($request->all());
        $logSyncLead->source = $lead->utm_source;
        $logSyncLead->campaign = $lead->utm_campaign;
        $logSyncLead->created_at = date('Y-m-d H:i:s');
        $number = $lead->phone1;
        $source_id = 20; //FB Inbox/Comment
        $lead->checkSpam();
        if(!$lead->checkDuplicate($number)){
            $logSyncLead->duplicate = 0;
            $user = $lead->getAssignee();
            $lead->status = 1; //mới
            $lead->created_at = date('Y-m-d H:i:s');
            $lead->created_by = 4; // hệ thống
            if(!empty($lead->name) && !empty($lead->phone1)){
                if($lead->save()){
                    $lead_source = new LeadSource;
                    $lead_source->lead_id = $lead->id;
                    $lead_source->source_id = $source_id;
                    $lead_source->created_by = $lead->created_by;
                    $lead_source->created_at = date('Y-m-d H:i:s');
                    $lead_source->save();

                    $assigner = new ManualLeadAssigner();
                    $assigner->user_id = $user->id;
                    $assigner->lead_id = $lead->id;
                    $assigner->created_by = $lead->created_by;
                    $assigner->save();

                    $logSyncLead->status = LogSyncLead::STATUS_SUCCESS;
                    $logSyncLead->lead_id = $lead->id;

                    $lead->exchangeSuspect();

                }else{
                    $logSyncLead->status = LogSyncLead::STATUS_FAIL;
                }
            }else{
                $logSyncLead->status = LogSyncLead::STATUS_FAIL;
            }
        }else{
            $logSyncLead->status = LogSyncLead::STATUS_SUCCESS;
            $logSyncLead->duplicate = 1;
            $lead_duplicate = Lead::where(function ($query) use ($number) {
                $query->where('phone1', $number)->orWhere('phone2', $number)->orWhere('phone3', $number)->orWhere('phone4', $number);
            })
                ->whereNull('deleted_at')
                ->where('department_id', $lead->department_id)
                ->first();

            if(!empty($lead->notes)){
                if (strpos($lead_duplicate->notes, $lead->notes) === false) {
                    $lead_duplicate->notes .= "\n". date('Y-m-d H:i:s') .': '.$lead->notes;
                    $lead_duplicate->save();
                }
            }

            $lead_source = LeadSource::whereNull('deleted_at')
                ->where('lead_id', $lead_duplicate->id)
                ->where('source_id', $source_id)
                ->first();
            if(!$lead_source){
                $lead_source = new LeadSource();
                $lead_source->lead_id = $lead_duplicate->id;
                $lead_source->source_id = $source_id;
                $lead_source->created_by = $lead_duplicate->created_by;
                $lead_source->created_at = date('Y-m-d H:i:s');
                $lead_source->save();
            }

            $logSyncLead->lead_id = $lead_duplicate->id;
            $user = User::getLeadAssignee($lead_duplicate->id);
        }

        $logSyncLead->save();

        if($logSyncLead->status != LogSyncLead::STATUS_FAIL){
            if($user && $user->active == 1){
                $notify = new Notify();
                $notify->content = ($logSyncLead->duplicate == 0)
                    ? "Lead \"$lead->name\" mới đăng ký từ tin nhắn Facebook"
                    : "Lead \"$lead->name\" trùng, vừa đăng ký từ tin nhắn Facebook";
                $notify->content.= "<br/>Chi tiết: " .env('APP_URL') ."/lead/edit/$logSyncLead->lead_id";
                $notify->user_id = $user->id;
                $notify->status = Notify::STATUS_NEW;
                $notify->created_at = date('Y-m-d H:i:s');
                $notify->lead_id = $logSyncLead->lead_id;
                $notify->lead_duplicate = $logSyncLead->duplicate;
                $notify->type = Notify::TYPE_SYNC_LEAD;
                $notify->save();

                //mail
                if($logSyncLead->duplicate == 0 && !empty($user->email))
                {
                    $from       = 'CRM';
                    $to         = $user->email;
                    $subject    = "Thông báo Lead mới";
                    $data       = array(
                        'title'         => $subject,
                        'name'          => $lead->name,
                        'phone'         => $lead->phone1,
                        'created_at'    => date("d/m/Y H:i", strtotime($lead->created_at)),
                        'source'        => 'AhaChat ' . $page_name,
                        'url'           => env('APP_URL').'/lead/edit/'.$lead->id,
                    );
                    $template   = 'email.lead.lead_new';

                    Mail::send($template, $data, function($message) use ($from, $to, $subject) {
                        $message
                            ->from('crm@edufit.vn',$from)
                            ->to($to)
                            ->subject($subject);
                    });
                }
            }
        }

    }

}