<?php

namespace App\Http\Controllers\LeadMarketing;

use App\AppBonusPolicy;
use App\AppBonusPolicyArea;
use App\AppClue;
use App\AppClueStaff;
use App\AppCommon;
use App\AppFeature;
use App\AppIntroduce;
use App\AppSpecialOffer;
use App\AppSpecialOfferProgram;
use App\AppTutorial;
use App\Campaign;
use App\ChannelUtmMapping;
use App\Department;
use App\Http\Controllers\Controller;
use App\Lead;
use App\LeadCampaign;
use App\LeadChannel;
use App\LeadSource;
use App\LeadStatus;
use App\LearningSystemDesire;
use App\LogSyncLead;
use App\ManualLeadAssigner;
use App\Notify;
use App\ReferralRule;
use App\User;
use App\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class LmsController extends Controller
{

    protected function getAppLoginEmail()
    {
        $token = null;
        $header = getallheaders();
        if(is_array($header)){
            $header = array_change_key_case($header, CASE_LOWER);
        }
        if(!empty($header['email'])){
            return $header['email'];
        }
        return null;
    }


    protected function getListStatus()
    {
        return array(
            'NL' => 'Đã nhận thông tin',
            'QL' => 'Đang chăm sóc',
            'RF' => 'Phụ huynh từ chối',
            'WL' => 'Đặt cọc giữ chỗ',
            'NE' => 'Đã nhập học'
        );
    }

    protected function convertStatus($status)
    {
        switch ($status) {
            case LeadStatus::STATUS_NEW:
                return 'NL';
            case LeadStatus::STATUS_NO_DEMAND:
            case LeadStatus::STATUS_REFUSE:
                return 'RF';
            case LeadStatus::STATUS_WAITLIST:
            case LeadStatus::STATUS_NEW_SALE:
                return 'WL';
            case LeadStatus::STATUS_ENROLL:
                return 'NE';
            default:
                return 'QL';
        }
    }



    public function getCommonCrm(Request $request)
    {
        $return = array(
            'success' => TRUE,
            'message' => '',
            'data' => array(
                'department' => array(),
                'status' => array(),
                'studyType' => array(),
            )
        );

        $departments = Department::where('hide', 0)->whereNotNull('lms_school_id')->orderBy('sort_index_app','ASC')->get();
        foreach ($departments as $department){
            $return['data']['department'][] = array(
                'id' => strval($department->id),
                'name' => $department->description,
                'schoolId' => strval($department->lms_school_id)
            );
        }

        $statuses = $this->getListStatus();
        foreach ($statuses as $key => $value){
            $return['data']['status'][] = array(
                'id' => $key,
                'name' => $value
            );
        }

        $learning_system_desires = LearningSystemDesire::where('status',1)->orderBy('sort_index_app','ASC')->get();
        foreach ($learning_system_desires as $learning_system_desire){
            $return['data']['studyType'][] = array(
                'id' => strval($learning_system_desire->id),
                'name' => $learning_system_desire->name,
            );
        }

        header('Content-Type: application/json');
        echo json_encode($return);
    }

    public function getListRefCrm(Request $request)
    {
        $return = array(
            'success' => TRUE,
            'message' => '',
            'data' => array()
        );

        if(!empty($request->userId)){
            $leads = Lead::where('referrer_id_lms', $request->userId)->orderBy('id', 'DESC')->get();
            $departments = Department::all()->pluck('description','id')->toArray();
            foreach ($leads as $lead){
                $return['data'][] = array(
                    'id' => strval($lead->id),
                    'name' => $lead->name,
                    'department' => $departments[$lead->department_id],
                    'status' => $this->convertStatus($lead->status),
                    'createdAt' => date('Y-m-d\TH:i:s.v\Z', (strtotime($lead->created_at) - 7*60*60))
                );
            }
        }else{
            $return['success'] = FALSE;
            $return['message'] = 'Empty User ID';
        }

        header('Content-Type: application/json');
        echo json_encode($return);
    }

    public function submitRef(Request $request)
    {
        $return = array(
            'success' => TRUE,
            'message' => '',
            'data' => array()
        );

        $name               = !empty($request->name)                ? $request->name : '';
        $phone              = !empty($request->phoneNumber)         ? Utils::sanitizePhoneV2($request->phoneNumber) : '';
        $email              = !empty($request->email)               ? $request->email : '';
        $note               = !empty($request->note)                ? $request->note : '';
        $referrer_name      = !empty($request->userName)            ? $request->userName : '';
        $referrer_phone     = !empty($request->userPhoneNumber)     ? $request->userPhoneNumber : '';
        $referrer_id_lms    = !empty($request->userId)              ? $request->userId : '';
        $learning_system_desire_ids = !empty($request->studyType)   ? $request->studyType : '';

        if(empty($utm_source)){
            $utm_source = 'LMS';
        }

        $department_id = 14; //Mặc định Holding
        if(!empty($request->department)){
            $department_id = $request->department;
        }

        $lead = new Lead();
        $lead->name          = $name;
        $lead->phone1        = $phone;
        $lead->email         = $email;
        $lead->department_id = $department_id;
        $lead->notes         = $note;
        $lead->utm_source    = $utm_source;
        $lead->referrer_name        = $referrer_name;
        $lead->referrer_phone       = $referrer_phone;
        $lead->referrer_id_lms      = $referrer_id_lms;

        if(!empty($learning_system_desire_ids)){
            $learning_system_desires = LearningSystemDesire::whereIn('id', explode(',',$learning_system_desire_ids))->get()->pluck('name')->toArray();
            if(!empty($learning_system_desires)){
                $lead->notes = "Hệ học quan tâm: ". implode(", ",$learning_system_desires);
            }
        }

        $logSyncLead = new LogSyncLead();
        $logSyncLead->data = json_encode($request->all());
        $logSyncLead->source = $lead->utm_source;
        $logSyncLead->campaign = $lead->utm_campaign;
        $logSyncLead->created_at = date('Y-m-d H:i:s');
        $number = $lead->phone1;
        $source_id = 11; //PH được giới thiệu bởi PH TDS
        $lead->checkSpam();
        if(!$lead->checkDuplicate($number)){
            $logSyncLead->duplicate = 0;
            $user = $lead->getAssignee();
            $lead->status = 1; //mới
            $lead->created_at = date('Y-m-d H:i:s');
            $lead->created_by = 4; // hệ thống
            if(!empty($lead->name) && !empty($lead->phone1)){
                if($lead->save()){
                    $lead_source = new LeadSource;
                    $lead_source->lead_id = $lead->id;
                    $lead_source->source_id = $source_id;
                    $lead_source->created_by = $lead->created_by;
                    $lead_source->created_at = date('Y-m-d H:i:s');
                    $lead_source->save();

                    $assigner = new ManualLeadAssigner();
                    $assigner->user_id = $user->id;
                    $assigner->lead_id = $lead->id;
                    $assigner->created_by = $lead->created_by;
                    $assigner->save();

                    if(!empty($utm_source)){
                        $channel_id = ChannelUtmMapping::getChannelIdByUtm($utm_source);
                        if (!empty($channel_id)) {
                            $lead_channel = new LeadChannel();
                            $lead_channel->lead_id = $lead->id;
                            $lead_channel->channel_id = $channel_id;
                            $lead_channel->created_by = $lead->created_by;;
                            $lead_channel->created_at = date('Y-m-d H:i:s');
                            $lead_channel->save();
                        }
                    }

                    $logSyncLead->status = LogSyncLead::STATUS_SUCCESS;
                    $logSyncLead->lead_id = $lead->id;

                }else{
                    $logSyncLead->status = LogSyncLead::STATUS_FAIL;
                }
            }else{
                $logSyncLead->status = LogSyncLead::STATUS_FAIL;
            }

            if($logSyncLead->status == LogSyncLead::STATUS_FAIL){
                $return['success'] = FALSE;
                $return['message'] = 'create_lead_fail';
            }

        }else{
            $logSyncLead->status = LogSyncLead::STATUS_SUCCESS;
            $logSyncLead->duplicate = 1;

            $return['success'] = FALSE;
            $return['message'] = 'duplicate_lead';
        }

        $logSyncLead->save();

        header('Content-Type: application/json');
        echo json_encode($return);
    }


    public function getRuleRef(Request $request)
    {
        $return = array(
            'success' => TRUE,
            'message' => '',
            'data' => array(
                'timeRule' => '',
                'rule' => array()
            )
        );

        if(!empty($request->schoolId)){
            $timeRule = ReferralRule::where('type', 'time')->where('school_id', $request->schoolId)->first();
            $rules = ReferralRule::where('type', 'rule')->where('school_id', $request->schoolId)->get();
            if(!empty($timeRule)){
                $return['data']['timeRule'] = $timeRule->content;
            }
            if(!empty($rules)){
                foreach ($rules as $rule){
                    $return['data']['rule'][] = array(
                        'id' => intval($rule->id),
                        'title' => $rule->name,
                        'content' => $rule->content,
                    );
                }
            }

        }

        $response = json_encode($return);
        $response = str_replace('\r\n', "", $response);

        header('Content-Type: application/json');
        echo $response;
    }


    public function checkUpdate(Request $request)
    {
        $return = array(
            'success' => TRUE,
            'message' => '',
            'data'    => array(
                'android' => array(
                    'version' => '1.0',
                    'buildNumber' => 1,
                    'isForceUpdate' => false,
                    'linkStore' => ''
                ),
                'ios' => array(
                    'version' => '1.0',
                    'buildNumber' => 1,
                    'isForceUpdate' => false,
                    'linkStore' => ''
                )
            )
        );

        header('Content-Type: application/json');
        $response = json_encode($return);
        echo $response;
    }

    public function commons(Request $request)
    {
        $return = array(
            'success' => TRUE,
            'message' => '',
            'data' => array(
                'role'              => 'guest',
                'letterContent'     => '',
                'bonusPolicy'       => array(),
                'specialOffers'     => array(),
                'tutorials'         => array(),
                'clues'             => array(),
                'commons'           => array(),
                'features'          => array(),

            )
        );

        //role
        $email = $this->getAppLoginEmail();
        if(!empty($email)){
            $user = User::where('active', 1)->where('email', $email)->first();
            if($user){
                $return['data']['role'] = 'staff';
            }
        }

        //letterContent
        $introduce = AppIntroduce::where('status', 1)->latest()->first();
        if($introduce){
            $return['data']['letterContent'] = $introduce->content;
        }

        //bonusPolicy
        $bonusPolicies = AppBonusPolicy::where('status', 1)->orderBy('sort_index', 'ASC')->get();
        if(!empty($bonusPolicies)){
            $bonusPolicyAreas = AppBonusPolicyArea::where('status', 1)->orderBy('sort_index', 'ASC')->get();
            foreach ($bonusPolicies as $bonusPolicy){
                $item = array(
                    'id'                => strval($bonusPolicy->id),
                    'departmentName'    => $bonusPolicy->name,
                    'area'              => array()
                );
                foreach ($bonusPolicyAreas as $bonusPolicyArea){
                    if($bonusPolicyArea->bonus_policy_id == $bonusPolicy->id){
                        $item['area'][] = array(
                            'id'        => strval($bonusPolicyArea->id),
                            'name'      => $bonusPolicyArea->name,
                            'staff'     => $bonusPolicyArea->staff,
                            'parent'    => $bonusPolicyArea->parent
                        );
                    }
                }
                $return['data']['bonusPolicy'][] = $item;
            }
        }

        //specialOffers
        $specialOffers = AppSpecialOffer::where('status', 1)->orderBy('sort_index', 'ASC')->get();
        if(!empty($specialOffers)){
            $specialOfferPrograms = AppSpecialOfferProgram::where('status', 1)->orderBy('sort_index', 'ASC')->get();
            foreach ($specialOffers as $specialOffer){
                $item = array(
                    'id'        => strval($specialOffer->id),
                    'title'     => $specialOffer->name,
                    'programs'  => array()
                );
                foreach ($specialOfferPrograms as $specialOfferProgram){
                    if($specialOfferProgram->special_offer_id == $specialOffer->id){
                        $item['programs'][] = array(
                            'id'        => strval($specialOfferProgram->id),
                            'imageUrl'  => $specialOfferProgram->image_url,
                            'title'     => $specialOfferProgram->name,
                            'time'      => date('d/m/Y', strtotime($specialOfferProgram->start_date)) . ' - ' . date('d/m/Y', strtotime($specialOfferProgram->end_date)),
                            'content'   => !empty($specialOfferProgram->content) ? $specialOfferProgram->content : ''
                        );
                    }
                }
                $return['data']['specialOffers'][] = $item;
            }
        }

        //tutorials
        $tutorials = AppTutorial::where('status', 1)->orderBy('sort_index', 'ASC')->get();
        if(!empty($tutorials)){
            foreach ($tutorials as $tutorial){
                $return['data']['tutorials'][] = array(
                    'id'        => strval($tutorial->id),
                    'title'     => $tutorial->name,
                    'content'   => !empty($tutorial->content) ? $tutorial->content : ''
                );
            }
        }

        //clues
        $clues = AppClue::where('status', 1)->orderBy('sort_index', 'ASC')->get();
        if(!empty($clues)){
            $clueStaffs = AppClueStaff::where('status', 1)->orderBy('sort_index', 'ASC')->get();
            foreach ($clues as $clue){
                $item = array(
                    'id'        => strval($clue->id),
                    'title'     => $clue->name,
                    'staffs'    => array()
                );
                foreach ($clueStaffs as $clueStaff){
                    if($clueStaff->clue_id == $clue->id){
                        $item['staffs'][] = array(
                            'id'                => strval($clueStaff->id),
                            'departmentName'    => !empty($clueStaff->department) ? $clueStaff->department : '',
                            'name'              => $clueStaff->name,
                            'phoneNumber'       => $clueStaff->phone,
                            'line'              => $clueStaff->line,
                            'email'             => $clueStaff->email
                        );
                    }
                }
                $return['data']['clues'][] = $item;
            }
        }

        //commons
        $commons = AppCommon::where('status', 1)->orderBy('sort_index', 'ASC')->get();
        if(!empty($commons)){
            foreach ($commons as $common){
                $return['data']['commons'][] = array(
                    'id'    => strval($common->id),
                    'name'  => $common->name,
                    'url'   => !empty($common->url) ? env('APP_URL').$common->url : ''
                );
            }
        }

        //features
        $features = AppFeature::where('status', 1)->orderBy('sort_index', 'ASC')->get();
        if(!empty($features)){
            foreach ($features as $feature){
                $return['data']['features'][] = array(
                    'id'            => strval($feature->id),
                    'title'         => $feature->name,
                    'type'          => $feature->type,
                    'isHighlight'   => ($feature->is_highlight == 1)
                );
            }
        }

        header('Content-Type: application/json');
        $response = json_encode($return);
        echo $response;
    }

}