<?php

namespace App\Http\Controllers\LeadMarketing;

use App\Http\Controllers\Controller;
use App\Lead;
use App\LeadSource;
use App\LogSyncLead;
use App\ManualLeadAssigner;
use App\Notify;
use App\User;
use App\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class WebsiteController extends Controller
{

    public function index(Request $request)
    {
        $return = array(
            'success' => TRUE,
            'message' => 'OK',
            'data' => array(),
        );

        if(!empty($request->utm_source)){
            if($request->utm_source == LogSyncLead::SOURCE_SMIS){
                $this->syncLeadSMIS($request);
            }
            if($request->utm_source == LogSyncLead::SOURCE_GIS){
                $this->syncLeadGIS($request);
            }
            if($request->utm_source == LogSyncLead::SOURCE_IKIDS){
                $this->syncLeadIKIDS($request);
            }
        }

        echo json_encode($return);
    }


    protected function syncLeadSMIS(Request $request)
    {
        $holding_department_id = 14;
        $department_mapping = array(
            1 => 'SMIS - Cầu Giấy, Hà Nội',
            3 => 'SMIS - Thụy Khuê, Ba Đình, Hà Nội',
            4 => 'SMIS - Lương Yên, Hai Bà Trưng, Hà Nội',
            5 => 'SMIS - Hà Đông, Hà Nội',
            6 => 'SMIS - Lê Hồng Phong, Hải Phòng',
            15=> 'SMIS - Dương Kinh, Hải Phòng',
            11=> 'SMIS - Hạ Long',
            12=> 'SMIS - Quận 2, Tp. Hồ Chí Minh',
            13=> 'Ikids - Thái Bình',
        );

        $department_mapping_popup = array(
            1 => 'SMIS - Cầu Giấy, HN',
            3 => 'SMIS - Thụy Khuê, Ba Đình, HN',
            4 => 'SMIS - Lương Yên, Hai Bà Trưng, HN',
            5 => 'SMIS - Hà Đông, HN',
            15=> 'SMIS - Dương Kinh, Hải Phòng',
            11=> 'SMIS - Hạ Long, Quảng Ninh',
            12=> 'SMIS - Quận 2, Tp. Hồ Chí Minh',
            13=> 'Ikids - Thái Bình',
        );

        $name           = $request->name;
        $phone          = $request->phone;
        $email          = $request->email;
        $department     = $request->department;
        $student_name   = $request->student_name;
        $birthday       = $request->birthday;
        $gender         = $request->gender;
        $address        = $request->address;
        $note           = $request->note;
        $utm_source     = $request->utm_source;

        $department_id = $holding_department_id;
        if(!empty($department)){
            foreach ($department_mapping as $key => $value){
                if($department == $value){
                    $department_id = $key;
                    break;
                }
            }
            foreach ($department_mapping_popup as $key => $value){
                if($department == $value){
                    $department_id = $key;
                    break;
                }
            }
        }

        $lead = new Lead();
        $lead->name          = $name;
        $lead->phone1        = Utils::sanitizePhoneV2($phone);
        $lead->email         = $email;
        $lead->department_id = $department_id;
        $lead->student_name  = $student_name;
        $lead->birthdate     = !empty($birthday) ? date('Y-m-d', strtotime(str_replace('/','-',$birthday))) : null;
        $lead->address       = $address;
        $lead->notes         = $note;
        $lead->utm_source    = $utm_source;

        $this->syncLead($lead, $request);
    }


    protected function syncLeadGIS(Request $request)
    {
        $holding_department_id = 14;
        $department_mapping = array(
            2 => '[:vi]Gateway - Cầu Giấy, Hà Nội[:en]Gateway - Cau Giay, Ha Noi[:]',
            8 => '[:vi]Gateway - Dương Kinh, Hải Phòng[:en]Gateway - Duong Kinh, Hai Phong[:]',
        );

        $name           = $request->name;
        $phone          = $request->phone;
        $email          = $request->email;
        $department     = $request->department;
        $student_name   = $request->student_name;
        $birthday       = $request->birthday;
        $gender         = $request->gender;
        $address        = $request->address;
        $note           = $request->note;
        $utm_source     = $request->utm_source;

        $department_id = $holding_department_id;
        if(!empty($department)){
            foreach ($department_mapping as $key => $value){
                if($department == $value){
                    $department_id = $key;
                    break;
                }
            }
        }

        $lead = new Lead();
        $lead->name          = $name;
        $lead->phone1        = Utils::sanitizePhoneV2($phone);
        $lead->email         = $email;
        $lead->department_id = $department_id;
        $lead->student_name  = $student_name;
        $lead->birthdate     = !empty($birthday) ? date('Y-m-d', strtotime(str_replace('/','-',$birthday))) : null;
        $lead->address       = $address;
        $lead->notes         = $note;
        $lead->utm_source    = $utm_source;

        $this->syncLead($lead, $request);
    }


    protected function syncLeadIKIDS(Request $request)
    {
        $ikids_department_id = 13;

        $name           = $request->name;
        $phone          = $request->phone;
        $email          = $request->email;
        $department     = $request->department;
        $student_name   = $request->student_name;
        $birthday       = $request->birthday;
        $gender         = $request->gender;
        $address        = $request->address;
        $note           = $request->note;
        $utm_source     = $request->utm_source;

        $department_id = $ikids_department_id;

        $lead = new Lead();
        $lead->name          = $name;
        $lead->phone1        = Utils::sanitizePhoneV2($phone);
        $lead->email         = $email;
        $lead->department_id = $department_id;
        $lead->student_name  = $student_name;
        $lead->birthdate     = !empty($birthday) ? date('Y-m-d', strtotime(str_replace('/','-',$birthday))) : null;
        if(!empty($gender)){
            if($gender == 'Bé trai'){
                $lead->gender = 'm';
            }else if($gender == 'Bé gái'){
                $lead->gender = 'f';
            }
        }
        $lead->address       = $address;
        $lead->notes         = $note;
        $lead->utm_source    = $utm_source;

        $this->syncLead($lead, $request);
    }

    protected function syncLead(Lead $lead, Request $request)
    {
        $logSyncLead = new LogSyncLead();
        $logSyncLead->data = json_encode($request->all());
        $logSyncLead->source = $lead->utm_source;
        $logSyncLead->created_at = date('Y-m-d H:i:s');

        $flag = true;
        $number = $lead->phone1;
        $list_lead_duplicate = $lead->getAllDuplicate($number);
        if(!empty($list_lead_duplicate)){
            $logSyncLead->duplicate = 1;
            foreach ($list_lead_duplicate as $lead_duplicate){
                if($lead_duplicate->department_id == $lead->department_id){
                    $flag = false;
                    $logSyncLead->lead_id = $lead_duplicate->id;
                }
                $user = User::getLeadAssignee($lead_duplicate->id);
                $notify = new Notify();
                $notify->content = "Lead \"$lead->name\" trùng, vừa đăng ký từ website $logSyncLead->source";
                $notify->content.= "<br/>Chi tiết: " .env('APP_URL') ."/lead/edit/$lead_duplicate->id";
                $notify->user_id = $user->id;
                $notify->status = Notify::STATUS_NEW;
                $notify->created_at = date('Y-m-d H:i:s');
                $notify->lead_id = $logSyncLead->lead_id;
                $notify->lead_duplicate = $logSyncLead->duplicate;
                $notify->save();
            }
        }else{
            $logSyncLead->duplicate = 0;
        }

        if($flag){
            $user = $lead->getAssignee();
            $lead->status = 1; //mới
            $lead->created_at = date('Y-m-d H:i:s');
            $lead->created_by = 4; // hệ thống
            if($lead->save()){
                $lead_source = new LeadSource;
                $lead_source->lead_id = $lead->id;
                $lead_source->source_id = 15; //Online
                $lead_source->created_by = $lead->created_by;
                $lead_source->created_at = date('Y-m-d H:i:s');
                $lead_source->save();

                $assigner = new ManualLeadAssigner();
                $assigner->user_id = $user->id;
                $assigner->lead_id = $lead->id;
                $assigner->created_by = $lead->created_by;
                $assigner->save();

                $logSyncLead->status = LogSyncLead::STATUS_SUCCESS;
                $logSyncLead->lead_id = $lead->id;

                $notify = new Notify();
                $notify->content = "Lead \"$lead->name\" mới đăng ký từ website $logSyncLead->source";
                $notify->content.= "<br/>Chi tiết: " .env('APP_URL') ."/lead/edit/$logSyncLead->lead_id";
                $notify->user_id = $user->id;
                $notify->status = Notify::STATUS_NEW;
                $notify->created_at = date('Y-m-d H:i:s');
                $notify->lead_id = $logSyncLead->lead_id;
                $notify->lead_duplicate = $logSyncLead->duplicate;
                $notify->save();

                if(!empty($user->email)){
                    $from       = 'CRM';
//                    $to         = $user->email;
                    $to         = 'thanh.nguyenxuan@educo.edu.vn';
                    $subject    = "Thông báo Lead mới";
                    $data       = array(
                        'title'         => $subject,
                        'name'          => $lead->name,
                        'phone'         => $lead->phone1,
                        'created_at'    => date("d/m/Y H:i", strtotime($lead->created_at)),
                        'source'        => 'website ' . $logSyncLead->source,
                        'url'           => env('APP_URL').'/lead/edit/'.$lead->id,
                    );
                    $template   = 'email.lead.lead_new';

                    Mail::send($template, $data, function($message) use ($from, $to, $subject) {
                        $message
                            ->from('crm@edufit.vn',$from)
                            ->to($to)
                            ->subject($subject);
                    });
                }

            }else{
                $logSyncLead->status = LogSyncLead::STATUS_FAIL;
            }
        }else{
            $logSyncLead->status = LogSyncLead::STATUS_FAIL;
        }
        $logSyncLead->save();
    }
}