<?php

namespace App\Http\Controllers\LeadMarketing;

use App\Campaign;
use App\ChannelUtmMapping;
use App\Http\Controllers\Controller;
use App\Lead;
use App\LeadCampaign;
use App\LeadChannel;
use App\LeadSource;
use App\LogSyncLead;
use App\ManualLeadAssigner;
use App\Notify;
use App\User;
use App\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class OfflineController extends Controller
{

    public function index(Request $request)
    {
        $return = array(
            'success' => true,
            'message' => 'OK',
            'data' => array(),
        );

        $this->syncLead($request);

        echo json_encode($return);
    }


    protected function syncLead(Request $request)
    {
        $name               = !empty($request->name)                ? $request->name : '';
        $phone              = !empty($request->phone)               ? Utils::sanitizePhoneV2($request->phone) : '';
        $referrer_name      = !empty($request->referrer_name)       ? $request->referrer_name : '';
        $referrer_phone     = !empty($request->referrer_phone)      ? Utils::sanitizePhoneV2($request->referrer_phone) : '';
        $referrer_source    = !empty($request->referrer_source)     ? $request->referrer_source : '';
        $study_program      = !empty($request->study_program)       ? $request->study_program : '';
        $consultation_time  = !empty($request->consultation_time)   ? Utils::sanitizePhoneV2($request->consultation_time) : '';

        $campaign           = !empty($request->campaign)            ? $request->campaign : '';

        $utm_source         = !empty($request->utm_source)          ? $request->utm_source : '';
        $utm_campaign       = !empty($request->utm_campaign)        ? $request->utm_campaign : '';
        $utm_medium         = !empty($request->utm_medium)          ? $request->utm_medium : '';
        $utm_content        = !empty($request->utm_content)         ? $request->utm_content : '';
        $utm_term           = !empty($request->utm_term)            ? $request->utm_term : '';
        $utm_link           = !empty($request->variant_url)         ? $request->variant_url : '';

        if(empty($campaign)){
            return;
        }
        if(empty($utm_source)){
            $utm_source = 'Offline';
        }
        if(empty($utm_campaign)){
            $utm_campaign = $campaign;
        }
        if(!empty($request->link) && empty($utm_link)){
            $utm_link = $request->link;
        }

        $department_id = 14; //Mặc định Holding
        if(!empty($request->department_id)){
            $department_id = $request->department_id;
        }
        if(!empty($request->department)){
            $mapping_dewey = array(
                2 => 'Dewey Cầu Giấy, Hà Nội',
                16 => 'Dewey Tây Hồ Tây, Hà Nội'
            );
            foreach ($mapping_dewey as $key => $value){
                if($value == $request->department){
                    $department_id = $key;
                    break;
                }
            }
        }


        $lead = new Lead();
        $lead->name             = $name;
        $lead->phone1           = $phone;
        $lead->referrer_name    = $referrer_name;
        $lead->referrer_phone   = $referrer_phone;
        $lead->department_id    = $department_id;
        $lead->utm_source       = $utm_source;
        $lead->utm_campaign     = $utm_campaign;
        $lead->utm_medium       = $utm_medium;
        $lead->utm_content      = $utm_content;
        $lead->utm_term         = $utm_term;
        $lead->utm_link         = $utm_link;

        $note_items = array();
        if(!empty($study_program)) {
            if(is_array($study_program)){
                $study_program_str = "Quan tâm chương trình học:\n" . implode("\n",$study_program);
            }else{
                $study_program_str = "Quan tâm chương trình học: " . $study_program;
            }
            $note_items[] = $study_program_str;
        }
        if(!empty($consultation_time)){
            $consultation_time_str = 'Thời gian nhận tư vấn: ' . $consultation_time;
            $note_items[] = $consultation_time_str;
        }
        if(!empty($note_items)){
            $lead->notes = implode("\n",$note_items);
        }

        $source_id = 35; // Đối tác cá nhân
        if(!empty($referrer_source)){
            switch ($referrer_source){
                case "PH được giới thiệu bởi CBNV ": $source_id = 36; break;
                case "PH được giới thiệu bới PH TDS": $source_id = 11; break;
                case "PH được giới thiệu bới PH SMIS": $source_id = 12; break;
            }
        }

        $logSyncLead = new LogSyncLead();
        $logSyncLead->data = json_encode($request->all());
        $logSyncLead->source = $lead->utm_source;
        $logSyncLead->campaign = $lead->utm_campaign;
        $logSyncLead->created_at = date('Y-m-d H:i:s');
        $number = $lead->phone1;

        $lead->checkSpam();
        if(!$lead->checkDuplicate($number)){
            $logSyncLead->duplicate = 0;
            $user = $lead->getAssignee();
            $lead->status = 1; //mới
            $lead->created_at = date('Y-m-d H:i:s');
            $lead->created_by = 4; // hệ thống
            if(!empty($lead->name) && !empty($lead->phone1)){
                if($lead->save()){
                    $lead_source = new LeadSource;
                    $lead_source->lead_id = $lead->id;
                    $lead_source->source_id = $source_id;
                    $lead_source->created_by = $lead->created_by;
                    $lead_source->created_at = date('Y-m-d H:i:s');
                    $lead_source->save();

                    $assigner = new ManualLeadAssigner();
                    $assigner->user_id = $user->id;
                    $assigner->lead_id = $lead->id;
                    $assigner->created_by = $lead->created_by;
                    $assigner->save();

                    $campaign_id = Campaign::getIdByName($campaign);
                    if($campaign_id){
                        $lead_campaign = new LeadCampaign();
                        $lead_campaign->lead_id = $lead->id;
                        $lead_campaign->campaign_id =$campaign_id;
                        $lead_campaign->created_at = date('Y-m-d H:i:s');
                        $lead_campaign->save();
                    }

                    if(!empty($utm_source)){
                        $channel_id = ChannelUtmMapping::getChannelIdByUtm($utm_source);
                        if (!empty($channel_id)) {
                            $lead_channel = new LeadChannel();
                            $lead_channel->lead_id = $lead->id;
                            $lead_channel->channel_id = $channel_id;
                            $lead_channel->created_by = $lead->created_by;;
                            $lead_channel->created_at = date('Y-m-d H:i:s');
                            $lead_channel->save();
                        }
                    }

                    $logSyncLead->status = LogSyncLead::STATUS_SUCCESS;
                    $logSyncLead->lead_id = $lead->id;

                    $lead->exchangeSuspect();

                }else{
                    $logSyncLead->status = LogSyncLead::STATUS_FAIL;
                }
            }else{
                $logSyncLead->status = LogSyncLead::STATUS_FAIL;
            }
        }else{
            $logSyncLead->status = LogSyncLead::STATUS_SUCCESS;
            $logSyncLead->duplicate = 1;
            $lead_duplicate = Lead::where(function ($query) use ($number) {
                $query->where('phone1', $number)->orWhere('phone2', $number)->orWhere('phone3', $number)->orWhere('phone4', $number);
            })
                ->whereNull('deleted_at')
                ->where('department_id', $lead->department_id)
                ->first();

            if(!empty($lead->notes)){
                if (strpos($lead_duplicate->notes, $lead->notes) === false) {
                    $lead_duplicate->notes .= "\n". date('Y-m-d H:i:s') .': '.$lead->notes;
                    $lead_duplicate->save();
                }
            }

            $lead_source = LeadSource::whereNull('deleted_at')
                ->where('lead_id', $lead_duplicate->id)
                ->where('source_id', $source_id)
                ->first();
            if(!$lead_source){
                $lead_source = new LeadSource();
                $lead_source->lead_id = $lead_duplicate->id;
                $lead_source->source_id = $source_id;
                $lead_source->created_by = $lead_duplicate->created_by;
                $lead_source->created_at = date('Y-m-d H:i:s');
                $lead_source->save();
            }

            $campaign_id = Campaign::getIdByName($campaign);
            if($campaign_id){
                $lead_campaign = LeadCampaign::whereNull('deleted_at')
                    ->where('lead_id',$lead_duplicate->id)
                    ->where('campaign_id',$campaign_id)
                    ->first();
                if(!$lead_campaign){
                    $lead_campaign = new LeadCampaign();
                    $lead_campaign->lead_id = $lead_duplicate->id;
                    $lead_campaign->campaign_id = $campaign_id;
                    $lead_campaign->created_at = date('Y-m-d H:i:s');
                    $lead_campaign->save();
                };
            }

            $logSyncLead->lead_id = $lead_duplicate->id;
            $user = User::getLeadAssignee($lead_duplicate->id);
        }

        $logSyncLead->save();

        if($logSyncLead->status != LogSyncLead::STATUS_FAIL){
            if($user && $user->active == 1){
                $notify = new Notify();
                $notify->content = ($logSyncLead->duplicate == 0)
                    ? "Lead \"$lead->name\" mới đăng ký từ LandingPage chiến dịch $logSyncLead->campaign"
                    : "Lead \"$lead->name\" trùng, vừa đăng ký từ LandingPage chiến dịch $logSyncLead->campaign";
                $notify->content.= "<br/>Chi tiết: " .env('APP_URL') ."/lead/edit/$logSyncLead->lead_id";
                $notify->user_id = $user->id;
                $notify->status = Notify::STATUS_NEW;
                $notify->created_at = date('Y-m-d H:i:s');
                $notify->lead_id = $logSyncLead->lead_id;
                $notify->lead_duplicate = $logSyncLead->duplicate;
                $notify->type = Notify::TYPE_SYNC_LEAD;
                $notify->save();

                //mail
                if($logSyncLead->duplicate == 0 && !empty($user->email))
                {
                    $from       = 'CRM';
                    $to         = $user->email;
                    $subject    = "Thông báo Lead mới";
                    $data       = array(
                        'title'         => $subject,
                        'name'          => $lead->name,
                        'phone'         => $lead->phone1,
                        'created_at'    => date("d/m/Y H:i", strtotime($lead->created_at)),
                        'source'        => 'Ladipage ' . $logSyncLead->campaign,
                        'url'           => env('APP_URL').'/lead/edit/'.$lead->id,
                    );
                    $template   = 'email.lead.lead_new';

                    Mail::send($template, $data, function($message) use ($from, $to, $subject) {
                        $message
                            ->from('crm@edufit.vn',$from)
                            ->to($to)
                            ->subject($subject);
                    });
                }
            }
        }

    }

}