<?php

namespace App\Http\Controllers\LeadMarketing;

use App\Http\Controllers\Controller;
use App\Lead;
use App\LeadCampaign;
use App\LeadSource;
use App\LogSyncLead;
use App\ManualLeadAssigner;
use App\Notify;
use App\User;
use App\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class TawktoController extends Controller
{
    protected function verifySignature ($body, $signature)
    {
        $secret_keys = array(
//            '1873904cf61e3aaa6bb5eb8cf130e9f80f178d1b67accbc5d1b7c8d8de342e4ff0546baecca98589e5bf897b74a471d4', //test
            LogSyncLead::SOURCE_GIS     => '63551a4e28abc22d1b28f0fa148e89642aac1ba971fe05fa22dedd5b7e96001b59382606b87efd740becaf7b729f089c',
            LogSyncLead::SOURCE_SMIS    => 'd185dee8cf69de3bad67d7c734419b721e8fdb7f0f0d6474b5f5af76c9a80d27bfc2195caafc3fc6207365051bbd9c1a',
            LogSyncLead::SOURCE_IKIDS   => '93c51e8157a594804c7bbfc837619d47f84d3d319343868b27743973b8b67e9ec5c2547a26e4cd3c0d95375907045759',
            LogSyncLead::SOURCE_ASC     => '31f787f82a4e1749e69e31d0e77892e99f1b2c3d2e34a6b0f970fc9dfb84ae407ddbc514cfb4bdb78802fb834a45afdd',
        );
        foreach ($secret_keys as $key => $value){
            if(hash_hmac('sha1', $body, $value) == $signature){
                return $key;
            }
        }
        return NULL;
    }

    protected function verifyDepartmentType($department_type)
    {
        $department_types = array(
            LogSyncLead::SOURCE_GIS,
            LogSyncLead::SOURCE_SMIS,
            LogSyncLead::SOURCE_IKIDS,
            LogSyncLead::SOURCE_ASC,
        );
        return in_array($department_type, $department_types);
    }

    public function index(Request $request)
    {
        $return = array(
            'success' => false,
            'message' => '',
            'data' => array()
        );

        $body = file_get_contents('php://input');
        $signature = isset($_SERVER['HTTP_X_TAWK_SIGNATURE']) ? $_SERVER['HTTP_X_TAWK_SIGNATURE'] : "";
        if(!empty($signature)){
            $key = $this->verifySignature($body, $signature);
            if (!empty($key)) {
                $data = json_decode($body);
                if($data->message->sender->type == 'visitor'){
                    $name = !empty($data->visitor->name) ? $data->visitor->name : "";
                    $email = !empty($data->visitor->email) ? $data->visitor->email : "";
                    $phone = null;
                    $department = null;
                    $note = null;

                    $message_data = explode("\r\n", $data->message->text);
                    if(count($message_data) > 1){

                        switch ($key){
                            case LogSyncLead::SOURCE_GIS:
                                $utm_source = 'tawkto_online_GIS';
                                if(!empty($message_data[2])){
                                    $phone_data = explode(' : ', $message_data[2]);
                                    if(count($phone_data) > 1){
                                        $phone = Utils::sanitizePhoneV2($phone_data[1]);
                                    }
                                }
                                if(!empty($message_data[3])){
                                    $department_data = explode(' : ', $message_data[3]);
                                    if(count($department_data) > 1){
                                        $department = $department_data[1];
                                    }
                                }
                                if(!empty($message_data[4])){
                                    $note = !empty($message_data[4]);
                                }
                                break;
                            case LogSyncLead::SOURCE_SMIS:
                                $utm_source = 'tawkto_online_SMIS';
                                if(!empty($message_data[1])){
                                    $phone_data = explode(' : ', $message_data[1]);
                                    if(count($phone_data) > 1){
                                        $phone = Utils::sanitizePhoneV2($phone_data[1]);
                                    }
                                }
                                if(!empty($message_data[2])){
                                    $department_data = explode(' : ', $message_data[2]);
                                    if(count($department_data) > 1){
                                        $department = $department_data[1];
                                    }
                                }
                                break;
                            case LogSyncLead::SOURCE_IKIDS:
                                $utm_source = 'tawkto_online_IKIDS';
                                if(!empty($message_data[1])){
                                    $phone_data = explode(' : ', $message_data[1]);
                                    if(count($phone_data) > 1){
                                        $phone = Utils::sanitizePhoneV2($phone_data[1]);
                                    }
                                }
                                if(!empty($message_data[2])){
                                    $department_data = explode(' : ', $message_data[2]);
                                    if(count($department_data) > 1){
                                        $department = $department_data[1];
                                    }
                                }
                                break;
                            case LogSyncLead::SOURCE_ASC:
                                $utm_source = 'tawkto_online_ASC';
                                if(!empty($message_data[1])){
                                    $phone_data = explode(' : ', $message_data[1]);
                                    if(count($phone_data) > 1){
                                        $phone = Utils::sanitizePhoneV2($phone_data[1]);
                                    }
                                }
                                if(!empty($message_data[2])){
                                    $department_data = explode(' : ', $message_data[2]);
                                    if(count($department_data) > 1){
                                        $department = $department_data[1];
                                    }
                                }
                                break;
                        }
                    }
                    $department_id = 14;
                    $mapping = array(
                        8   => 'GIS Dương Kinh - Hải Phòng',
                        2   => 'GIS Cầu Giấy - Hà Nội',
                        16  => 'GIS Tây Hồ Tây - Hà Nội',
                        1   => 'SMIS Cầu Giấy - Hà Nội',
                        3   => 'SMIS Thụy Khuê, Ba Đình - Hà Nội',
                        4   => 'SMIS Lương Yên, Hai Bà Trưng - Hà Nội',
                        5   => 'SMIS Hà Đông - Hà Nội',
                        11  => 'SMIS Hạ Long - Quảng Ninh',
                        12  => 'SMIS Quận 2 - Hồ Chí Minh',
                        17  => 'SMIS Thanh Xuân - Hà Nội',
                        18  => 'SMIS Tây Hồ Tây - Hà Nội',
                        15  => 'SMIS Dương Kinh - Hải Phòng',
                        13  => 'IKIDS Thái Bình',
                        19  => 'Ikids Nam Từ Liêm',
                    );

                    $mapping_asc = array(
                        20 => 'Cầu Giấy, Hà Nội',
                        25 => 'Tây Hồ Tây, Hà Nội',
                        21 => 'Dương Kinh, Hải Phòng',
                    );

                    if($request->department_type == LogSyncLead::SOURCE_ASC){
                        foreach ($mapping_asc as $key => $value){
                            if($value == $department){
                                $department_id = $key;
                                break;
                            }
                        }
                    }

                    foreach ($mapping as $key => $value){
                        if($value == $department){
                            $department_id = $key;
                            break;
                        }
                    }

                    if(!empty($name) && !empty($phone)){
                        $lead = new Lead();
                        $lead->name = $name;
                        $lead->phone1 = $phone;
                        $lead->email = $email;
                        $lead->department_id = $department_id;
                        $lead->notes = $note;
                        $lead->utm_source = $utm_source;

                        $return['success'] = $this->syncLead($lead, $request);
                    }else{
                        $return['message'] = 'missing data';
                    }
                }else{
                    $return['message'] = 'messageType invalid';
                }
            }else{
                $return['message'] = 'verifySignature fail';
            }
        }else{
            $return['message'] = 'verifySignature empty signature';
        }

        echo json_encode($return);
    }


    public function offline(Request $request)
    {
        $return = array(
            'success' => false,
            'message' => '',
            'data' => array()
        );

        $name = null;
        $email = null;
        $phone = null;
        $department = null;
        $note = null;
        $utm_source = 'tawkto_offline';

        if(!empty($request->department_type) && $this->verifyDepartmentType($request->department_type)){

            $name = !empty($request->name) ? $request->name : "";
            $email = !empty($request->email) ? $request->email : "";

            switch ($request->department_type){
                case LogSyncLead::SOURCE_GIS:
                    if(!empty($request->questions) && is_array($request->questions)){
                        $phone = Utils::sanitizePhoneV2($request->questions[3]['answer']);
                        $department = $request->questions[4]['answer'];
                        $note = $request->questions[5]['answer'];
                        $utm_source.= '_GIS';
                    }
                    break;
                case LogSyncLead::SOURCE_SMIS:
                    if(!empty($request->questions) && is_array($request->questions)){
                        $phone = Utils::sanitizePhoneV2($request->questions[3]['answer']);
                        $department = $request->questions[4]['answer'];
                        $note = $request->questions[5]['answer'];
                        $utm_source.= '_SMIS';
                    }
                    break;
                case LogSyncLead::SOURCE_IKIDS:
                    if(!empty($request->questions) && is_array($request->questions)){
                        $phone = Utils::sanitizePhoneV2($request->questions[3]['answer']);
                        $department = $request->questions[4]['answer'];
                        $note = $request->questions[5]['answer'];
                        $utm_source.= '_IKIDS';
                    }
                    break;
                case LogSyncLead::SOURCE_ASC:
                    if(!empty($request->questions) && is_array($request->questions)){
                        $phone = Utils::sanitizePhoneV2($request->questions[2]['answer']);
                        $department = $request->questions[6]['answer'];
                        $note_arr = [];
                        $note_arr[] = $request->questions[5]['answer'];
                        $note_arr[] = $request->questions[4]['answer'];
                        $note = implode("\n", $note_arr);
                        $utm_source.= '_ASC';
                    }
                    break;
                default:
                    $return['message'] = 'department type invalid';
            }
        }else{
            $return['message'] = 'missing department type';
        }

        $department_id = 14;
        $mapping = array(
            8   => 'GIS Dương Kinh - Hải Phòng',
            2   => 'GIS Cầu Giấy - Hà Nội',
            16  => 'GIS Tây Hồ - Hà Nội',
            1   => 'SMIS Cầu Giấy - Hà Nội',
            3   => 'SMIS Thụy Khuê, Ba Đình - Hà Nội',
            4   => 'SMIS Lương Yên, Hai Bà Trưng - Hà Nội',
            5   => 'SMIS Hà Đông - Hà Nội',
            11  => 'SMIS Hạ Long - Quảng Ninh',
            12  => 'SMIS Quận 2 - Hồ Chí Minh',
            13  => 'IKIDS Thái Bình',
            15  => 'SMIS Dương Kinh - Hải Phòng',
            17  => 'SMIS Thanh Xuân - Hà Nội',
            18  => 'SMIS Tây Hồ - Hà Nội',
            19  => 'Ikids Nam Từ Liêm',
        );

        $mapping_asc = array(
            20 => 'Cầu Giấy, Hà Nội',
            25 => 'Tây Hồ Tây, Hà Nội',
            21 => 'Dương Kinh, Hải Phòng',
        );

        foreach ($mapping as $key => $value){
            if($value == $department){
                $department_id = $key;
                break;
            }
        }

        if($request->department_type == LogSyncLead::SOURCE_ASC){
            foreach ($mapping_asc as $key => $value){
                if($value == $department){
                    $department_id = $key;
                    break;
                }
            }
        }

        if(!empty($name) && !empty($phone)){
            $lead = new Lead();
            $lead->name = $name;
            $lead->phone1 = $phone;
            $lead->email = $email;
            $lead->department_id = $department_id;
            $lead->notes = $note;
            $lead->utm_source = $utm_source;

            $return['success'] = $this->syncLead($lead, $request);
        }else{
            $return['message'] = 'missing data';
        }

        echo json_encode($return);
    }

    protected function syncLead(Lead $lead, Request $request)
    {
        $logSyncLead = new LogSyncLead();
        $logSyncLead->data = json_encode($request->all());
        $logSyncLead->source = $lead->utm_source;
        $logSyncLead->created_at = date('Y-m-d H:i:s');
        $number = $lead->phone1;
        $source_id = 2; //live chat;
        $lead->checkSpam();
        if(!$lead->checkDuplicate($number)){
            $logSyncLead->duplicate = 0;
            $user = $lead->getAssignee();
            $lead->status = 1; //mới
            $lead->created_at = date('Y-m-d H:i:s');
            $lead->created_by = 4; // hệ thống
            if($lead->save()){
                $lead_source = new LeadSource;
                $lead_source->lead_id = $lead->id;
                $lead_source->source_id = $source_id;
                $lead_source->created_by = $lead->created_by;
                $lead_source->created_at = date('Y-m-d H:i:s');
                $lead_source->save();

                $assigner = new ManualLeadAssigner();
                $assigner->user_id = $user->id;
                $assigner->lead_id = $lead->id;
                $assigner->created_by = $lead->created_by;
                $assigner->save();

                $campaign_id = 1; //TS
                if($campaign_id){
                    $lead_campaign = new LeadCampaign();
                    $lead_campaign->lead_id = $lead->id;
                    $lead_campaign->campaign_id =$campaign_id;
                    $lead_campaign->created_at = date('Y-m-d H:i:s');
                    $lead_campaign->save();
                }

                $logSyncLead->status = LogSyncLead::STATUS_SUCCESS;
                $logSyncLead->lead_id = $lead->id;

                $lead->exchangeSuspect();

            }else{
                $logSyncLead->status = LogSyncLead::STATUS_FAIL;
            }
        }else{
            $logSyncLead->status = LogSyncLead::STATUS_SUCCESS;
            $logSyncLead->duplicate = 1;
            $lead_duplicate = Lead::where(function ($query) use ($number) {
                $query->where('phone1', $number)->orWhere('phone2', $number)->orWhere('phone3', $number)->orWhere('phone4', $number);
            })
                ->whereNull('deleted_at')
                ->where('department_id', $lead->department_id)
                ->first();

            if(!empty($lead->notes)){
                if (strpos($lead_duplicate->notes, $lead->notes) === false) {
                    $lead_duplicate->notes .= "\n". date('Y-m-d H:i:s') .': '.$lead->notes;
                    $lead_duplicate->save();
                }
            }

            $lead_source = LeadSource::whereNull('deleted_at')
                ->where('lead_id', $lead_duplicate->id)
                ->where('source_id', $source_id)
                ->first();
            if(!$lead_source){
                $lead_source = new LeadSource();
                $lead_source->lead_id = $lead_duplicate->id;
                $lead_source->source_id = $source_id;
                $lead_source->created_by = $lead_duplicate->created_by;
                $lead_source->created_at = date('Y-m-d H:i:s');
                $lead_source->save();
            }

            $campaign_id = 1;
            if($campaign_id){
                $lead_campaign = LeadCampaign::whereNull('deleted_at')
                    ->where('lead_id',$lead_duplicate->id)
                    ->first();
                if(!$lead_campaign){
                    $lead_campaign = new LeadCampaign();
                    $lead_campaign->lead_id = $lead_duplicate->id;
                    $lead_campaign->campaign_id = $campaign_id;
                    $lead_campaign->created_at = date('Y-m-d H:i:s');
                    $lead_campaign->save();
                };
            }

            $logSyncLead->lead_id = $lead_duplicate->id;
            $user = User::getLeadAssignee($lead_duplicate->id);
        }

        $logSyncLead->save();

        if($logSyncLead->status != LogSyncLead::STATUS_FAIL){
            if($user && $user->active == 1){
                $notify = new Notify();
                $notify->content = ($logSyncLead->duplicate == 0)
                    ? "Lead \"$lead->name\" mới liên hệ từ live chat TawkTo"
                    : "Lead \"$lead->name\" trùng, liên hệ từ live chat TawkTo";
                $notify->content.= "<br/>Chi tiết: " .env('APP_URL') ."/lead/edit/$logSyncLead->lead_id";
                $notify->user_id = $user->id;
                $notify->status = Notify::STATUS_NEW;
                $notify->created_at = date('Y-m-d H:i:s');
                $notify->lead_id = $logSyncLead->lead_id;
                $notify->lead_duplicate = $logSyncLead->duplicate;
                $notify->type = Notify::TYPE_SYNC_LEAD;
                $notify->save();

                //mail
                if($logSyncLead->duplicate == 0 && !empty($user->email))
                {
                    $from       = 'CRM';
                    $to         = $user->email;
                    $subject    = "Thông báo Lead mới";
                    $data       = array(
                        'title'         => $subject,
                        'name'          => $lead->name,
                        'phone'         => $lead->phone1,
                        'created_at'    => date("d/m/Y H:i", strtotime($lead->created_at)),
                        'source'        => 'LiveChat Tawkto',
                        'url'           => env('APP_URL').'/lead/edit/'.$lead->id,
                    );
                    $template   = 'email.lead.lead_new';

                    Mail::send($template, $data, function($message) use ($from, $to, $subject) {
                        $message
                            ->from('crm@edufit.vn',$from)
                            ->to($to)
                            ->subject($subject);
                    });
                }
            }
        }

        return ($logSyncLead->status != LogSyncLead::STATUS_FAIL);
    }

}