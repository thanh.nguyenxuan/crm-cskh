<?php

namespace App\Http\Controllers;

use App\Keyword;
use App\Lead;
use App\User;
use Carbon\Carbon;
use App\Sms;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;

class SmsController extends Controller
{

    public function getUnopenedSmsList($user_id)
    {
        $sms_list = DB::table('fptu_sms')
            ->join('fptu_lead', function ($join) {
                $join->on('fptu_lead.phone1', '=', 'fptu_sms.to')
                    ->orOn('fptu_lead.phone2', '=', 'fptu_sms.to')
                    ->orOn('fptu_lead.phone3', '=', 'fptu_sms.to')
                    ->orOn('fptu_lead.phone4', '=', 'fptu_sms.to');
            })
            ->join('fptu_lead_assignment', 'fptu_lead_assignment.lead_id', '=', 'fptu_lead.id')
            ->where('fptu_lead_assignment.user_id', $user_id)
            ->where(function ($query) {
                $query->where('fptu_sms.closed', 0)
                    ->orWhereNull('fptu_sms.closed');
            })
            ->where('fptu_sms.type', 2)
            ->select('fptu_sms.*', 'fptu_lead.name as lead_name', 'fptu_lead.id as lead_id')
            ->orderBy('fptu_sms.created_at', 'DESC')
            ->get();
            //->toSql();
        //var_dump($sms_list);
        echo json_encode($sms_list);
    }

    public function stopSmsNotification()
    {
        $sms_list = Input::get('sms_list');
        if (!empty($sms_list)) {
            foreach ($sms_list as $sms_id) {
                if (!empty($sms_id)) {
                    $sms = Sms::find($sms_id);
                    $sms->closed = 1;
                    $sms->save();
                }
            }
        }
    }
}