<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\School;
use App\SchoolType;
use App\Province;
use App\Department;
use Illuminate\Support\Facades\Auth;

class SchoolController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $school_list = array();
        if (Auth::user()->hasRole('admin'))
            $school_list = School::orderBy('type', 'asc')->orderBy('name', 'asc')->get();
        else
            $school_list = School::where('province', Department::find(Auth::user()->department_id)->province)->orderBy('type', 'asc')->orderBy('name', 'asc')->get();
        $province_list = Province::all()->pluck('name', 'id');
        $school_type_list = SchoolType::all()->pluck('name', 'id');
        return view('school.index', ['school_list' => $school_list, 'province_list' => $province_list, 'school_type_list' => $school_type_list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $school_type_list = SchoolType::all()->pluck('name', 'id');
        $province_list = Province::all()->pluck('name', 'id');
        return view('school.create', ['province_list' => $province_list, 'school_type_list' => $school_type_list]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = 0;
        $return_message = '';
        $province_list = Province::all()->pluck('name', 'id');
        $school_type_list = SchoolType::all()->pluck('name', 'id');
        $found = School::where('name', $request->name)
            ->count();
        $error = false;
        if ($found) {
            $return_message = 'Tên trường đã tồn tại.';
            $error = true;
        } else {
            $return_message = 'Tạo trường mới thành công!';
            $success = 1;
            $school = new School();
            $school->name = trim($request->name);
            $school->type = trim($request->type);
            $school->province = $request->province;
            $school->address = trim($request->address);
            $saved = $school->save();
            if (!$saved) {
                $return_message = 'Đã có lỗi trong quá trình lưu dữ liệu!';
                $success = 0;
                $error = true;
            }
        }
        $request->flash();
        if ($error)
            return view('school.create', ['success' => $success, 'message' => $return_message]);
        return view('school.edit', ['school' => $school, 'province_list' => $province_list, 'success' => $success, 'message' => $return_message, 'school_type_list' => $school_type_list]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $school = School::find($id);
        $province_list = Province::all()->pluck('name', 'id');
        $school_type_list = SchoolType::all()->pluck('name', 'id');
        return view('school.edit', ['school' => $school, 'province_list' => $province_list, 'school_type_list' => $school_type_list]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $success = 0;
        $school = School::find($id);
        $error = false;
        $return_message = '';
        $school->name = trim($request->name);
        $school->type = trim($request->type);
        $school->address = trim($request->address);
        $school->province = $request->province;
        $province_list = Province::all()->pluck('name', 'id');
        $school_type_list = SchoolType::all()->pluck('name', 'id');
        $updated = $school->save();
        if ($updated) {
            $return_message = 'Đã cập nhật dữ liệu thành công!';
            $success = 1;
        } else {
            $return_message = '<div>Đã có lỗi trong quá trình cập nhật dữ liệu.</div>';
            $success = 0;
        }
        return view('school.edit', ['success' => $success, 'school' => $school, 'province_list' => $province_list, 'message' => $return_message, 'school_type_list' => $school_type_list]);
    }

    public function toggle($id)
    {
        $success = 0;
        $school = School::find($id);
        $error = false;
        $return_message = '';
        $update = array();
        $province_list = Province::all()->pluck('name', 'id');
        $school_type_list = SchoolType::all()->pluck('name', 'id');
        if ($school->hide == 1) {
            $school->hide = 0;
        } else {
            $school->hide = 1;
        }
        $updated = $school->save();
        if ($updated) {
            $return_message = 'Đã cập nhật trạng thái lớp thành công!';
            $success = 1;
        } else {
            $return_message = 'Đã có lỗi trong quá trình cập nhật dữ liệu.';
            $success = 0;
        }
//        $school_list = array();
//        $school_list = School::all();
//        return view('school.index', ['success' => $success, 'message' => $return_message, 'school_list' => $school_list, 'province_list' => $province_list, 'school_type_list' => $school_type_list]);
        return redirect('school');
    }
}
