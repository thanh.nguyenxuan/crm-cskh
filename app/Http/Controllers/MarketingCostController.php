<?php

namespace App\Http\Controllers;

use App\Keyword;
use App\Lead;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class MarketingCostController extends Controller
{
    public function index()
    {
        $keyword_list = Keyword::orderBy('cost_per_lead', 'DESC')->get();
        foreach ($keyword_list as $keyword) {
            if (!empty($keyword->updated_by))
                $keyword->updater = User::find($keyword->updated_by)->username;
            if (!empty($keyword->person_in_charge))
                $keyword->person_in_charge = User::find($keyword->person_in_charge)->username;
        }
        $last_updated = Keyword::select('updated_at')->whereNotNull('person_in_charge')->orderBy('updated_at', 'DESC')->first();
        //var_dump($last_updated['updated_at']);
        return view('cost/index', ['keyword_list' => $keyword_list, 'last_updated' => $last_updated['updated_at']]);

    }

    public function store()
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $input_list = Input::all();
        //$update_array = array();
        foreach ($input_list['marketing_cost'] as $keyword_id => $cost) {
            //echo 'Updating cost ' . $keyword_id . ' with ' . $cost . '<br />';\
            //$current_cost = Keyword::find($keyword_id)->marketing_cost;
            DB::table('fptu_keyword')->where('id', $keyword_id)->where('marketing_cost', '!=', $cost)->update(['marketing_cost' => $cost, 'updated_at' => date('Y-m-d H:i:s'), 'updated_by' => Auth::user()->id]);
        }
        DB::table('fptu_lead')->update(['marketing_cost' => 0]);
        $keyword_list = DB::table('fptu_keyword')
            ->join(DB::raw('(SELECT DISTINCT(lead_id), keyword_id FROM fptu_lead_keyword) as lead_keyword'), 'fptu_keyword.id', '=', 'lead_keyword.keyword_id')
            ->join('fptu_lead', 'fptu_lead.id', '=', 'lead_keyword.lead_id')
            ->where('fptu_lead.department_id', 4)
            ->whereNull('fptu_lead.deleted_at')
            ->select('lead_keyword.keyword_id', 'fptu_keyword.marketing_cost', DB::raw('count(*) as lead_count, count(fptu_lead.converted) as account_count'))
            ->groupBy('lead_keyword.keyword_id')
            ->get();
        $keyword_account_array = DB::table('fptu_keyword')
            ->join(DB::raw('(SELECT DISTINCT(lead_id), keyword_id FROM fptu_lead_keyword) as lead_keyword'), 'fptu_keyword.id', '=', 'lead_keyword.keyword_id')
            ->join('fptu_lead', 'fptu_lead.id', '=', 'lead_keyword.lead_id')
            ->where('fptu_lead.department_id', 4)
            ->whereNull('fptu_lead.deleted_at')
            ->where('fptu_lead.converted', 1)
            ->select('lead_keyword.keyword_id', DB::raw('count(fptu_lead.converted) as account_count'))
            ->groupBy('lead_keyword.keyword_id')
            ->pluck('account_count', 'keyword_id')
            ->toArray();
        foreach ($keyword_list as $keyword) {
            $keyword_id = $keyword->keyword_id;
            //$keyword_name = $keyword->name;
            $keyword_marketing_cost = $keyword->marketing_cost;
            //echo 'Name: ' . $keyword_name . ' | ';
            //echo 'Marketing cost: ' . $keyword_marketing_cost . '<br />';
            $lead_count = $keyword->lead_count;
            //echo $keyword_name . ': ' . $lead_count . '<br />';
            $account_count = !empty($keyword_account_array[$keyword_id]) ? $keyword_account_array[$keyword_id] : 0;
            DB::table('fptu_keyword')->where('id', $keyword_id)->update(['lead_count' => $lead_count, 'account_count' => $account_count]);
            if (($lead_count > 0) && ($keyword_marketing_cost > 0)) {
                $lead_cost = $keyword_marketing_cost / $lead_count;
                DB::table('fptu_keyword')->where('id', $keyword_id)->update(['lead_count' => $lead_count, 'cost_per_lead' => $lead_cost]);
                //var_dump($lead_cost);
                if ($lead_cost > 0) {
                    //DB::connection()->enableQueryLog();
                    $leads = DB::table('fptu_lead')
                        ->join('fptu_lead_keyword', 'fptu_lead.id', '=', 'fptu_lead_keyword.lead_id')
                        ->where('fptu_lead_keyword.keyword_id', $keyword_id)
                        ->where('fptu_lead.department_id', 4)
                        ->update(['fptu_lead.marketing_cost' => DB::raw('fptu_lead.marketing_cost+' . $lead_cost)]);
                    //dd(DB::getQueryLog());
                }
            }
        }
        $last_updated = Keyword::select('updated_at')->whereNotNull('person_in_charge')->orderBy('updated_at', 'DESC')->first();
        $keyword_list = Keyword::orderBy('cost_per_lead', 'DESC')->get();
        foreach ($keyword_list as $keyword) {
            if (!empty($keyword->updated_by))
                $keyword->updater = User::find($keyword->updated_by)->username;
            if (!empty($keyword->person_in_charge))
                $keyword->person_in_charge = User::find($keyword->person_in_charge)->username;
        }
        return view('cost/index', ['keyword_list' => $keyword_list, 'success' => true, 'last_updated' => $last_updated['updated_at']]);
    }
}