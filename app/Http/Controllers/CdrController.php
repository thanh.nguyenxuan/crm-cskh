<?php

namespace App\Http\Controllers;

use App\PbxTrunk;
use App\Utils;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Rest;

class CdrController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $from_date_original = Input::get('from_date');
        $to_date_original = Input::get('to_date');
        $from_date = date('Y-m-d');
        $to_date = date('Y-m-d');
        $trunk = Input::get('trunk');
        $direction = Input::get('direction');
        $tele = Input::get('tele');
        $trunk_name = 'Edufit Test';
        $direction_list = array(
            'outgoing' => 'Gọi đi',
            'incoming' => 'Gọi đến'
        );
        $trunk_list = PbxTrunk::get()->pluck('display_name', 'id');
        if (!empty($from_date_original))
            $from_date = Utils::toDbDate($from_date_original);
        else {
            $from_date_original = date('d/m/Y');
        }
        if (!empty($to_date_original))
            $to_date = Utils::toDbDate($to_date_original);
        else {
            $to_date_original = date('d/m/Y');
        }
        $ext = Auth::user()->extension;
        $ext_list = array();
        $ext_username_list = array();
        if (Auth::user()->hasRole('admin'))
            $user_list = DB::table('fptu_user')
                ->join('fptu_role_user', 'fptu_user.id', '=', 'fptu_role_user.user_id')
                ->where('fptu_role_user.role_id', 2)
                ->pluck('fptu_user.username', 'fptu_user.extension')
                ->toArray();
        else
            $user_list = DB::table('fptu_user')
                ->join('fptu_role_user', 'fptu_user.id', '=', 'fptu_role_user.user_id')
                ->where('fptu_user.department_id', Auth::user()->department_id)
                ->where('fptu_role_user.role_id', 2)
                ->pluck('fptu_user.username', 'fptu_user.extension')
                ->toArray();
        $telesales_list = DB::table('fptu_user')
            ->join('fptu_role_user', 'fptu_user.id', '=', 'fptu_role_user.user_id')
            ->where('fptu_user.department_id', Auth::user()->department_id)
            ->where('fptu_role_user.role_id', 2)
            ->pluck('fptu_user.username', 'fptu_user.username')
            ->toArray();
        foreach ($user_list as $extension => $username) {
            if (!empty($extension)) {
                $ext_list[] = $extension;
                $ext_username_list[$extension] = $username;
                if (!empty($tele) && $tele == $username) {
                    $ext = $extension;
                }
            }
        }
        if ((Auth::user()->hasRole('teleteamleader') || Auth::user()->hasRole('holding') || Auth::user()->hasRole('admin')) && empty($tele)) {

//            $user_list = DB::table('fptu_user')
//                ->join('fptu_role_user', 'fptu_user.id', '=', 'fptu_role_user.user_id')
//                ->join('fptu_user_group', 'fptu_user.id', '=', 'fptu_user_group.user_id')
//                ->where('fptu_role_user.role_id', 2)
//                ->where('fptu_user_group.group_id', 2)
//                ->pluck('fptu_user.username', 'fptu_user.extension')
//                ->toArray();
            if (!empty($ext_list)) {
                $ext = implode(',', $ext_list);
            }
        }
        $param_string = '';
        if (!empty($trunk)) {
            $param_string .= '&trunk=' . $trunk;
        }
        if (!empty($direction)) {
            $param_string .= '&direction=' . $direction;
        }
        if (!empty($ext)) {
            $url = env('APP_URL') . '/api/call_details.php?status=ANSWERED&ext=' . $ext . '&from_date=' . $from_date . '&to_date=' . $to_date . $param_string;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $data = curl_exec($ch);
            curl_close($ch);
            $data = json_decode($data);
            //var_dump($data);
        }
        if(empty($data)) $data = array();
        for ($i = 0; $i < count($data); $i++) {
            $phone = '';
            if (!empty($data[$i]->direction)) {
                if ($data[$i]->direction == 'outgoing') {
                    $phone = trim($data[$i]->to);
                }
                if ($data[$i]->direction == 'incoming') {
                    $phone = trim($data[$i]->from);
                }
            }
            if (!empty($phone)) {
                $lead = DB::table('fptu_lead')
                    ->where('phone1', $phone)
                    ->orWhere('phone2', $phone)
                    ->orWhere('phone3', $phone)
                    ->orWhere('phone4', $phone)
                    ->whereNull('deleted_at')
                    ->first();
                //var_dump($lead);
                if (!empty($lead) && !empty($lead->id)) {
                    $data[$i]->lead_id = $lead->id;
                    $data[$i]->lead_name = $lead->name;
                }
            }
            switch ($data[$i]->status) {
                case 'ANSWERED':
                    $data[$i]->status = 'Có nghe máy';
            }
            if (!empty($data[$i]->trunk)) {
                $trunk_name = 'Edufit Test';
                $data[$i]->trunk_name = $trunk_name;
            }
            if (!empty($data[$i]->ext) && !empty($ext_username_list[$data[$i]->ext]))
                $data[$i]->caller = $ext_username_list[$data[$i]->ext];
        }

        return view('cdr.index', ['trunk_list' => $trunk_list, 'direction_list' => $direction_list, 'data' => $data, 'telesales_list' => $telesales_list, 'from_date' => $from_date_original, 'to_date' => $to_date_original]);
    }

    public function getCallLog($phone)
    {

    }

    public function getCallRecordingFile($id)
    {
        $url = env('APP_URL') . '/api/recording/' . $id;
        $recording_file = Rest::get($url);
        return $recording_file;
    }
}
