<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Notify;

class NotifyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function markAllAsRead(Request $request)
    {
        $return = array(
            'success' => TRUE,
            'message' => '',
            'data' => array()
        );
        DB::table('notify')
            ->where('user_id', Auth::id())
            ->whereIn('status', array(Notify::STATUS_NEW, Notify::STATUS_PENDING))
            ->update(array('status' => Notify::STATUS_SEEN));
        echo json_encode($return);
    }

    public function markNewAsPending(Request $request)
    {
        $return = array(
            'success' => TRUE,
            'message' => '',
            'data' => array()
        );
        DB::table('notify')
            ->where('user_id', Auth::id())
            ->where('status', Notify::STATUS_NEW)
            ->update(array('status' => Notify::STATUS_PENDING));
        echo json_encode($return);
    }

    public function markAsRead(Request $request)
    {
        $return = array(
            'success' => TRUE,
            'message' => '',
            'data' => array()
        );
        DB::table('notify')
            ->where('id', $request->notify_id)
            ->where('user_id', Auth::id())
            ->update(array('status' => Notify::STATUS_SEEN));
        echo json_encode($return);
    }

    public function markAsUnRead(Request $request)
    {
        $return = array(
            'success' => TRUE,
            'message' => '',
            'data' => array()
        );
        DB::table('notify')
            ->where('id', $request->notify_id)
            ->where('user_id', Auth::id())
            ->update(array('status' => Notify::STATUS_PENDING));
        echo json_encode($return);
    }

    public function getNewNotify(Request $request)
    {
        $return = array(
            'success' => TRUE,
            'message' => '',
            'data' => array(
                'total' => 0,
                'notify' => array()
            )
        );
        $total = DB::table('notify')
            ->where('user_id', Auth::id())
            ->where('status', Notify::STATUS_NEW)
            ->count();
        $last_notify = DB::table('notify')
            ->select('notify.*',
                DB::raw('DATE_FORMAT(notify.created_at,\'%d/%m/%Y %H:%i\') as notify_at')
            )
            ->where('user_id', Auth::id())
            ->orderBy('id', 'desc')
            ->limit(10)
            ->get();
        $return['data']['total'] = $total;
        $return['data']['notify'] = $last_notify;
        echo json_encode($return);
    }


    public function index(Request $request)
    {
        $url = env('APP_URL').'/notify';
        $url_params = array();
        $query = DB::table('notify')
            ->where('user_id', Auth::id())
            ->orderBy('id', 'desc')
            ->whereNull('deleted_at');
        if(!empty($request->from_date)){
            $from_date = date('Y-m-d', strtotime(str_replace('/','-',$request->from_date))) . ' 00:00:00';
            $query->where('created_at','>=',$from_date);
            $url_params['from_date'] = $request->from_date;
        }
        if(!empty($request->to_date)){
            $to_date = date('Y-m-d', strtotime(str_replace('/','-',$request->to_date))) . ' 23:59:59';
            $query->where('created_at','<=',$to_date);
            $url_params['to_date'] = $request->to_date;
        }
        if(!empty($request->status)){
            if($request->status == Notify::STATUS_NEW){
                $query->whereIn('status',array(Notify::STATUS_NEW, Notify::STATUS_PENDING));
            }else{
                $query->where('status',$request->status);
            }
            $url_params['status'] = "$request->status";
        }
        if(!empty($request->key)){
            $query->where('content', 'LIKE', "%$request->key%");
            $url_params['key'] = $request->key;
        }
        if(isset($request->is_duplicate) && $request->is_duplicate !== ''){
            $query->where('lead_duplicate', "$request->is_duplicate");
            $url_params['is_duplicate'] = $request->is_duplicate;
        }
        $current_page = (!empty($request->page) && intval($request->page) > 0) ? intval($request->page) : 1;
        $limit = 10;
        $offset = $limit*($current_page-1);
        $total = $query->count();
        $total_page = ceil($total/$limit);
        $data = $query->offset($offset)->limit($limit)->get();

        session()->flashInput($request->input());

        return view('notify.index', array(
            'data' => $data,
            'pagination_total_page' => $total_page,
            'pagination_current_page' => $current_page,
            'pagination_url' => $url,
            'pagination_url_params' => $url_params,
        ));
    }

    function checkNotifyLeadNewAndNoAnswer()
    {
        $return = array(
            'success' => false,
            'message' => null,
            'data' => null,
        );

        $notify = Notify::whereIn('type', array(
            Notify::TYPE_ALARM_LEAD_CARE,
            Notify::TYPE_ALARM_LEAD_DAILY_KPI,
        ))
            ->where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->first();
        if($notify && $notify->status == Notify::STATUS_NEW){
            if($notify->type == Notify::TYPE_ALARM_LEAD_CARE){
                $return['success'] = TRUE;
                $return['data'] = $notify;
                $data = json_decode($notify->data);
                $message = '<b>Xin chào!</b>';
                $message.= '<br/>Hiện bạn đang có:';
                $message.= '<br/>'.count($data->new). ' Lead trạng thái "Mới"';
                $message.= '<br/>'.count($data->no_answer). ' Lead trạng thái "Không nghe máy"';
                $return['message'] = $message;
            }else{
                $return['success'] = TRUE;
                $return['data'] = $notify;
                $return['message'] = $notify->content;
            }
        }
        echo json_encode($return);
    }

    function detail($id)
    {

    }

}
