<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CallStatus;

class CallStatusController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $call_status_list = array();
        $call_status_list = CallStatus::all();
        return view('call_status.index', ['call_status_list' => $call_status_list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('call_status.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = 0;
        $return_message = '';
        $found = CallStatus::where('name', $request->name)
            ->count();
        $error = false;
        if ($found) {
            $return_message = 'Tên nguồn thông tin đã tồn tại.';
            $error = true;
        } else {
            $return_message = 'Tạo nguồn thông tin mới thành công!';
            $success = 1;
            $call_status = new CallStatus();
            $call_status->name = trim($request->name);
            $saved = $call_status->save();
            if (!$saved) {
                $return_message = 'Đã có lỗi trong quá trình lưu dữ liệu!';
                $success = 0;
                $error = true;
            }
        }
        $request->flash();
        if ($error)
            return view('call_status.create', ['success' => $success, 'message' => $return_message]);
        return view('call_status.edit', ['call_status' => $call_status, 'success' => $success, 'message' => $return_message]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $call_status = CallStatus::find($id);
        return view('call_status.edit', ['call_status' => $call_status]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $success = 0;
        $call_status = CallStatus::find($id);
        $error = false;
        $return_message = '';

        if (!$error) {
            $update = array(
                'name' => $request->name
            );

            $updated = $call_status->update($update);
            if ($updated) {
                $return_message = 'Đã cập nhật dữ liệu thành công!';
                $success = 1;
            } else {
                $return_message = 'Đã có lỗi trong quá trình cập nhật dữ liệu.';
                $success = 0;
            }
        }
        //$user = User::find($id);
        return view('call_status.edit', ['success' => $success, 'call_status' => $call_status, 'message' => $return_message]);
    }

    public function toggle($id)
    {
        $success = 0;
        $call_status = CallStatus::find($id);
        $error = false;
        $return_message = '';
        $update = array();
        if ($call_status->hide == 1) {
            $call_status->hide = 0;
        } else {
            $call_status->hide = 1;
        }

        $updated = $call_status->save();
        if ($updated) {
            $return_message = 'Đã cập nhật trạng thái lớp thành công!';
            $success = 1;
        } else {
            $return_message = 'Đã có lỗi trong quá trình cập nhật dữ liệu.';
            $success = 0;
        }
        $call_status_list = array();
        $call_status_list = CallStatus::all();
        return view('call_status.index', ['success' => $success, 'message' => $return_message, 'call_status_list' => $call_status_list]);
    }
}
