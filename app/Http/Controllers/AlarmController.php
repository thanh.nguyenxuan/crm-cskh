<?php

namespace App\Http\Controllers;

use App\Department;
use App\Lead;
use App\Utils;
use App\Alarm;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class AlarmController extends Controller
{
    public function set()
    {
        $alarm_at = Input::get('alarm_at');
        $alarm_to = Input::get('alarm_to');
        $object_type = Input::get('object_type');
        $object_id = Input::get('object_id');
        $alarm_before = Input::get('alarm_before');
        $type = Input::get('type');
        $notes = Input::get('notes');
        if(empty($notes)){
            $notes = Alarm::getTypeListData()[$type];
        }
        $alarm = new Alarm;
        if (!empty($alarm_at)) {
            $alarm_at = Carbon::createFromFormat('d/m/Y H:i', $alarm_at)->format('Y-m-d H:i:00');
            $alarm->alarm_at = $alarm_at;
        }
        $alarm->alarm_to = $alarm_to;
        $alarm->object_type = $object_type;
        $alarm->object_id = $object_id;
        $alarm->alarm_before = $alarm_before;
        $alarm->type = $type;
        $alarm->notes = $notes;
        $ok = $alarm->save();
        if ($ok) {
            echo 'ok';
        } else {
            echo 'error';
        }
    }

    public function index(Request $request)
    {
        $from_date = $request->get('from_date', date('d/m/Y'));
        $to_date = $request->get('to_date', date('d/m/Y'));
        $date_type = $request->get('date_type', 'created_at');
        $assignee = $request->get('assignee');

        $alarm_list = Alarm::whereDate($date_type, '>=', Utils::convertDate($from_date))->whereDate($date_type, '<=', Utils::convertDate($to_date));
        if (!Auth::user()->hasRole('admin') && !Auth::user()->hasRole('holding') && !in_array(Department::HOLDING, Auth::user()->getManageDepartments())){
            $alarm_list->where('alarm_to', Auth::id());
        }elseif(!in_array(Department::HOLDING, Auth::user()->getManageDepartments())){
            $userIds = Auth::user()->getManageUsers();
            $alarm_list->whereIn('alarm_to', $userIds);
        }
        if(!empty($assignee)){
            $alarm_list->where('alarm_to', $assignee);
        }

        $alarm_list = $alarm_list->get();
        foreach ($alarm_list as $alarm) {
            if (!empty($alarm->object_id) && !empty(Lead::find($alarm->object_id))) {
                $lead = Lead::find($alarm->object_id);
                $alarm->lead_id = $alarm->object_id;
                $alarm->lead_name = $lead->name;
            }
        }

        session()->flashInput($request->input());
        return view('alarm.index', [
            'alarm_list' => $alarm_list,
            'from_date' => $from_date,
            'to_date' => $to_date,
            'date_type' => $date_type,
        ]);
    }

    public function getAlarmList($user_id)
    {
        $now = date('Y-m-d H:i:s');
        $alarm_list = DB::table('fptu_alarm')
            ->join('fptu_lead', 'fptu_alarm.object_id', '=', 'fptu_lead.id')
            ->where('fptu_alarm.object_type', 'lead')
            ->where('fptu_alarm.alarm_to', $user_id)
            ->where('fptu_alarm.closed', 0)
            ->whereRaw("fptu_alarm.alarm_at <= ('$now' + INTERVAL fptu_alarm.alarm_before MINUTE)")
            ->select('fptu_alarm.*', 'fptu_lead.name as lead_name', 'fptu_lead.id as lead_id', 'fptu_lead.phone1 as lead_phone')
            ->get()
            ->toArray();
        echo json_encode($alarm_list);
    }

    public function stopAlarm()
    {
        $alarm_list = Input::get('alarm_list');
        if (!empty($alarm_list)) {
            foreach ($alarm_list as $alarm_id) {
                if (!empty($alarm_id)) {
                    $alarm = Alarm::find($alarm_id);
                    $alarm->closed = 1;
                    $alarm->save();
                }
            }
        }
    }
}