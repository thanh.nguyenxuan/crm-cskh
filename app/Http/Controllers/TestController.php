<?php

namespace App\Http\Controllers;

use App\Sms\Sms;
use App\Sms\SmsFetcher;
use App\User;
use App\Utils;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use PHPExcel;
use PHPExcel_IOFactory;
use Google_Client;
use Google_Service_Gmail;
use Google_Service_Gmail_Message;
use App\Mail\SmsNotification;
use App\Lead;
use App\DiemThiTHPT\Grade;

//use Google_Service_Gmail_Message;

class TestController extends Controller
{
    function validate_date($date_string, $format = 'd/m/Y')
    {
        $day = 0;
        $month = 0;
        $year = 0;
        switch ($format) {
            case 'd/m/Y':
                $date_chunks = explode('/', $date_string);
                if (count($date_chunks) == 3) {
                    $day = intval(trim($date_chunks[0]));
                    $month = intval(trim($date_chunks[1]));
                    $year = intval(trim($date_chunks[2]));
                }
                break;
            case 'd-m-Y':
                $date_chunks = explode('-', $date_string);
                if (count($date_chunks) == 3) {
                    $day = intval(trim($date_chunks[0]));
                    $month = intval(trim($date_chunks[1]));
                    $year = intval(trim($date_chunks[2]));
                }
                break;
            case 'Y-m-d':
                $date_chunks = explode('-', $date_string);
                if (count($date_chunks) == 3) {
                    $day = intval(trim($date_chunks[2]));
                    $month = intval(trim($date_chunks[1]));
                    $year = intval(trim($date_chunks[0]));
                }
                break;
        }
        if ($year < 1000 || $year > 9999)
            return false;
        return checkdate($month, $day, $year);
    }

    public
    function test()
    {
        return 'ok';
    }

    public
    function testx()
    {

        /* connect to gmail */
        $hostname = '{imap.gmail.com:993/imap/ssl}INBOX';
        $username = 'smsbot.tstt@gmail.com';
        $password = 'sms@2018@4444!';

        /* try to connect */
        $inbox = imap_open($hostname, $username, $password) or die('Cannot connect to Gmail: ' . imap_last_error());

        /* grab emails */
        $today = date('d M Y');
        var_dump($today);
        $emails = imap_search($inbox, 'FROM "smsbot.tstt@gmail.com" SINCE "' . $today . '"');

        /* if emails are returned, cycle through each... */
        if ($emails) {

            /* begin output var */
            $output = '';

            /* put the newest emails on top */
            rsort($emails);

            /* for every email... */
            foreach ($emails as $email_number) {

                /* get information specific to this email */
                $overview = imap_fetch_overview($inbox, $email_number, 0);
                $message = imap_fetchbody($inbox, $email_number, 1);
                $meta = $overview[0];
                $sms_existing = Sms::where('uid', $meta->uid)->count();
                $phone = $this->parseMessage($meta->subject);
                $sms = null;
                if ($sms_existing == 0 && !empty($phone)) {
                    $sms = new Sms();

                    $sms->uid = $meta->uid;
                    $received_date = date('Y-m-d H:i:s', strtotime($meta->date));
                    $sms->created_at = $received_date;
                    $sms->to = $phone; // from
                    $sms->type = 2; // incoming SMS
                    $sms->created_by = 1; // system
                    $sms->content = $message;
                    $sms->port = substr($meta->subject, strlen($meta->subject) - 1, 1);
                    $sms->save();
                } else {
                    $sms = Sms::where('uid', $meta->uid)->first();
                }
                if (!empty($sms) && empty($sms->notified)) {
                    $this->notify($sms);
                }
                //var_dump($overview[0]);

                /* output the email header information */
                $output .= $overview[0]->uid . '<br />';
                $output .= '<div class="toggler ' . ($overview[0]->seen ? 'read' : 'unread') . '">';
                $output .= '<span class="subject">' . $overview[0]->subject . '</span> ';
                $output .= '<span class="from">' . $overview[0]->from . '</span>';
                $output .= '<span class="date">on ' . $overview[0]->date . '</span>';
                $output .= '<span class="phone">. Phone: ' . $this->parseMessage($overview[0]->subject) . '</span>';
                //$date_received = date('d/m/Y H:i:s', strtotime($overview[0]->date));
                //var_dump($date_received);
                $output .= '</div>';

                /* output the email body */
                $output .= '<div class="body">' . $message . '</div>';
            }

            echo $output;
        }

        /* close the connection */
        imap_close($inbox);
    }

    function notify($sms)
    {
        $phone = $sms->to;
        $lead_name = '';
        $to_email = '';
        $cc = array('hoangld.tstt@gmail.com');
        if (!empty($phone)) {
            $lead = Lead::where('phone1', $phone)->orWhere('phone2', $phone)->orWhere('phone3', $phone)->orWhere('phone4', $phone)->first();
            if (!empty($lead) && !empty($lead->id)) {
                $lead_id = $lead->id;
                $lead_name = $lead->name;
                if (!empty($lead->activator)) {
                    $activator = User::where('id', $lead->activator)->first();
                    $to_email = $activator->email;
                } else {
                    $assignee_list = DB::table('fptu_lead_assignment')->where('lead_id', $lead->id)->whereNull('deleted_at')->get();
                    $to_email = array();
                    foreach ($assignee_list as $assignee) {
                        $to_email = array();
                        $assignee_user = User::find($assignee->user_id);
                        if (!empty($assignee_user) && !empty($assignee_user->id)) {
                            $to_email[] = $assignee_user->email;
                        }
                    }
                }
            }
        }
        if ($sms->port == 7) {
            $cc[] = 'chinhvt.tstt@gmail.com';
        }
        Mail::to($to_email)->cc($cc)->queue(new SmsNotification($sms));
    }

    function listMessages($service, $userId)
    {
        $pageToken = NULL;
        $messages = array();
        $opt_param = array();
        //do {
        try {
            //if ($pageToken) {
            $opt_param['q'] = ' from:(smsbot.tstt@gmail.com) after:2018/5/3';
            //}
            $messagesResponse = $service->users_messages->listUsersMessages($userId, $opt_param);
            if ($messagesResponse->getMessages()) {
                $messages = array_merge($messages, $messagesResponse->getMessages());
                //$pageToken = $messagesResponse->getNextPageToken();
            }
        } catch (Exception $e) {
            print 'An error occurred: ' . $e->getMessage();
        }
        //} while ($pageToken);

        foreach ($messages as $message) {
            $messageId = $message->getId();
            $message_data = $service->users_messages->get($userId, $messageId);
            var_dump($message_data);
        }

        //return $messages;
    }

    public
    function parseMessage($message)
    {
        $matches = array();
        preg_match('/\+84\d+/', $message, $matches);
        if (!empty($matches)) {
            $phone = '0' . substr($matches[0], 3, strlen($matches[0]));
            return ($phone);
        }
        return '';
    }

    public
    function test3()
    {
        //session_start();
        //var_dump(session('access_token'));
        if (!empty(session('access_token'))) {
            $service_account_file = storage_path('app/oauth_key.json');
            //var_dump(file_exists(storage_path('app/service_account_key.json')));exit;
            //putenv('GOOGLE_APPLICATION_CREDENTIALS=' . $service_account_file);
            $client = new Google_Client();
            $client->setAuthConfig($service_account_file);
            $client->addScope(Google_Service_Gmail::GMAIL_READONLY);
            //$client->useApplicationDefaultCredentials();
            $client->setAccessToken(session('access_token'));
            $client->setAccessType('offline');
            $client->setLoginHint('smsbot.tstt@gmail.com');
            $client->setApprovalPrompt('consent');
            $gmailService = new Google_Service_Gmail($client);
            //var_dump($gmailService);
            $messages = $this->listMessages($gmailService, 'me');
            //var_dump($messages);
        } else {
            return redirect()->route('test.authorize');
        }
        //return 'Error!';
    }

    public
    function authorizeGoogle()
    {
        //echo 1;exit;
        //session_start();
        //var_dump($_GET['code']);
        $client = new Google_Client();
        $service_account_file = storage_path('app/oauth_key.json');
        $client->setAuthConfig($service_account_file);
        $client->addScope(Google_Service_Gmail::GMAIL_READONLY);
        $client->setRedirectUri('http://localhost/authorize');
        $client->setAccessType('offline');        // offline access
        $client->setIncludeGrantedScopes(true);   // incremental auth
        $client->setLoginHint('smsbot.tstt@gmail.com');
        //$client->setApprovalPrompt('consent');
        if (empty($_GET['code'])) {
            $auth_url = $client->createAuthUrl();
            //var_dump($auth_url);
            return redirect()->away(filter_var($auth_url, FILTER_SANITIZE_URL));
        } else {
            $client->authenticate($_GET['code']);
            session(['access_token' => $client->getAccessToken()]);
            //var_dump($_SESSION['access_token']);
            return redirect()->route('test.test');
        }
        return 'Error!';
    }

    public
    function parse_grade()
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $grade_list = Grade::all();
        $grade_name_array = array('toan', 'van', 'anh', 'nga', 'phap', 'trung', 'nhat', 'ly', 'hoa', 'sinh', 'khtn', 'su', 'dia', 'gdcd', 'khxh', 'ngoaingu');
        foreach ($grade_list as $grade) {
            $grade_array = array();
            $diem = null;
            $number_of_grades = 0;
            $sbd = trim($grade->sbd);
            $diem = trim($grade->diem);
            $diem = preg_replace('!\s+!', ' ', $diem);
            $diem = str_replace('Toán:', 'toan', $diem);
            $diem = str_replace('Ngữ văn:', 'van', $diem);
            $diem = str_replace('Tiếng Anh:', 'anh', $diem);
            $diem = str_replace('Tiếng Nga:', 'nga', $diem);
            $diem = str_replace('Tiếng Pháp:', 'phap', $diem);
            $diem = str_replace('Tiếng Trung:', 'trung', $diem);
            $diem = str_replace('Tiếng Nhật:', 'nhat', $diem);
            $diem = str_replace('Vật lí:', 'ly', $diem);
            $diem = str_replace('Hóa học:', 'hoa', $diem);
            $diem = str_replace('Sinh học:', 'sinh', $diem);
            $diem = str_replace('KHTN:', 'khtn', $diem);
            $diem = str_replace('Lịch sử:', 'su', $diem);
            $diem = str_replace('Địa lí:', 'dia', $diem);
            $diem = str_replace('GDCD:', 'gdcd', $diem);
            $diem = str_replace('KHXH:', 'khxh', $diem);
            $diem = str_replace('Ngoại ngữ:', 'khxh', $diem);
            if (!empty($diem)) {
                $grade_array = explode(' ', $diem);
                for ($i = 0; $i < count($grade_array); $i++) {
                    $grade_name = $grade_array[$i];
                    if (in_array($grade_name, $grade_name_array)) {
                        $diem_mon = $grade_array[$i + 1];
                        $diem_mon = floatval($diem_mon);
                        $grade->$grade_name = $diem_mon;
                        $number_of_grades++;
                    }
                }

                if (($number_of_grades * 2) != count($grade_array)) {
                    echo 'ERR! ' . $sbd . '<br />';
                    echo 'Num of grades: ' . $number_of_grades . '<br />';
                    echo 'Grade array count: ' . count($grade_array) . '<br />';
                    var_dump($grade_array);
                    break;
                }
                if ($grade->save()) {
                    echo $grade->sbd . '<br />';
                }
            }
        }
    }
}