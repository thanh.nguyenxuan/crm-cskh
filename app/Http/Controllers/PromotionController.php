<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Promotion;

class PromotionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promotion_list = array();
        $promotion_list = Promotion::all();
        return view('promotion.index', ['promotion_list' => $promotion_list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('promotion.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = 0;
        $return_message = '';
        $found = Promotion::where('name', $request->name)
            ->count();
        $error = false;
        if ($found) {
            $return_message = '<div>Tên nguồn thông tin đã tồn tại.</div>';
            $error = true;
        } else {
            $return_message = '';
            $success = 1;
        }
        $promotion = new Promotion();
        $promotion->code = trim($request->code);
        $promotion->name = trim($request->name);
        $promotion->percentage = trim($request->percentage);
        $promotion->save();
        $request->flash();
        return view('promotion.edit', ['promotion' => $promotion, 'success' => $success, 'message' => $return_message]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $promotion = Promotion::find($id);
        return view('promotion.edit', ['promotion' => $promotion]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $success = 0;
        $promotion = Promotion::find($id);
        $error = false;
        $return_message = '';

        if (!$error) {
            $update = array(
                'code' => $request->code,
                'name' => $request->name,
                'percentage' => $request->percentage
            );

            $updated = $promotion->update($update);
            if ($updated) {
                $return_message = '';
                $success = 1;
            } else {
                $return_message = '<div>Đã có lỗi trong quá trình cập nhật dữ liệu.</div>';
                $success = 0;
            }
        }
        //$user = User::find($id);
        return view('promotion.edit', ['success' => $success, 'promotion' => $promotion, 'message' => $return_message]);
    }

    public function toggle($id)
    {
        $success = 0;
        $promotion = Promotion::find($id);
        $error = false;
        $return_message = '';
        $update = array();
        if ($promotion->hide == 1) {
                $promotion->hide = 0;
        } else {
            $promotion->hide = 1;
        }

        $updated = $promotion->save();
        if ($updated) {
            $return_message = 'Đã cập nhật trạng thái promotion thành công!';
            $success = 1;
        } else {
            $return_message = '<div>Đã có lỗi trong quá trình cập nhật dữ liệu.</div>';
            $success = 0;
        }
        $promotion_list = array();
        $promotion_list = Promotion::all();
        return view('promotion.index', ['success' => $success, 'message' => $return_message, 'promotion_list' => $promotion_list]);
    }
}
