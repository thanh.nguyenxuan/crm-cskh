<?php

namespace App\Http\Controllers;

use App\Alarm;
use App\CampaignCategory;
use App\CampaignCategoryMapping;
use App\ComplainReason;
use App\InfoSource;
use App\LeadCampaign;
use App\LeadChannel;
use App\LeadComplain;
use App\LeadLogStatus;
use App\LeadObjective;
use App\LeadParentsConcern;
use App\LeadStudentChallenge;
use App\LogLeadCare;
use App\LogSms;
use App\Major;
use App\ManualLeadAssigner;
use App\Notify;
use App\Pbx;
use App\Promotion;
use App\Report\Cdr;
use App\School;
use App\Sms;
use App\Telcom;
use Illuminate\Http\Request;
//use Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;

use App\Lead;
use App\Account;


use App\User;
use App\Role;
use App\UserPref;

use App\Importer;

use App\Source;

use App\Province;
use App\District;
use App\Department;
use App\Area;
use App\LeadStatus;
use App\CallStatus;
use App\Group;
use App\Campus;
use App\DataList;
use App\Channel;
use App\Campaign;
use App\Keyword;
use App\LeadSource;
use App\Merging;
use App\Objective;
use App\ParentsConcern;
use App\StudentChallenge;


use App\Account\Status as AccountStatus;

use SSP;

use PAMI\Client\Impl\ClientImpl as PamiClient;
use PAMI\Message\Event\EventMessage;
use PAMI\Message\Event\OriginateResponseEvent;
use PAMI\Listener\IEventListener;
use PAMI\Message\Action\OriginateAction;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\ChangeLog;
use App\Utils;

class LeadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        $source_list = Source::listDataByUserDepartment();
        $channel_list = Channel::where('hide', 0)->orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $campaign_list = Campaign::where('hide', 0)->orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $campaign_category_list = CampaignCategory::where('status', 1)->pluck('name', 'id')->toArray();
        $keyword_list = Keyword::where('hide', 0)->orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $province_list_data = Province::orderBy('name', 'ASC')->get();
        $province_list = array('na' => 'Không xác định');
        foreach ($province_list_data as $province_data) {
            $province_list[$province_data->id] = $province_data->name;
        }
        $major_list = array();
        $major_list_data = Major::where('hide', 0)->get();
        foreach ($major_list_data as $major_data) {
            $major_list[$major_data->id] = $major_data->name;
        }
        $status_list = array('' => 'Trạng thái bất kỳ');
        $status_list_data = LeadStatus::where('hide', 0)->orderBy('sort_index', 'ASC')->get();
        foreach ($status_list_data as $status_data) {
            $status_list[$status_data->id] = $status_data->name;
        }
        $call_status_list_data = CallStatus::all();
        foreach ($call_status_list_data as $call_status_data) {
            $call_status_list[$call_status_data->id] = $call_status_data->name;
        }
        $department_list = Department::where('hide', 0)->orderBy('sort_index', 'ASC')->pluck('name', 'id')->toArray();

        $group_list = array('na' => 'Không thuộc nhóm nào');
        $group_list_data = Group::all();
        foreach ($group_list_data as $group_data) {
            $group_list[$group_data->id] = $group_data->name;
        }

        $user_data_list = User::whereIn('department_id',array_keys($department_list))->orderBy('username', 'ASC')->get();
        foreach ($user_data_list as $user_data) {
            $creator_list[$user_data->id] = $user_data->username . ' - ' . $user_data->full_name;
        }
        if (Auth::user()->hasRole('holding') || in_array(Department::HOLDING, Auth::user()->getManageDepartments())) {
            $departments_str = implode(',', array_keys($department_list));
            $assignee_list_data = User::whereHas('roles', function ($q) {
                $q->where('name', 'telesales')
                    ->orWhere('name', 'teleteamleader');
            })->whereRaw("(department_id IN ($departments_str)
                    OR id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                    OR id IN (SELECT user_id FROM group_user WHERE group_id IN (
                        SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                    ))
                )")
                ->orderBy('username', 'ASC')->get();
        } else {
            $departments_str = implode(',', Auth::user()->getManageDepartments());
            $assignee_list_data = User::whereHas('roles', function ($q) {
                $q->where('name', 'telesales')
                    ->orWhere('name', 'teleteamleader');
            })->whereRaw("(department_id IN ($departments_str)
                    OR id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                    OR id IN (SELECT user_id FROM group_user WHERE group_id IN (
                        SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                    ))
                )")
                ->orderBy('username', 'ASC')->get();
        }
        $assignee_list = array('nobody' => 'Chưa được chia');
        $activator_list = array();
        $caller_list = array();
        $rating_list = array(1 => 'Rất quan tâm', 2 => 'Quan tâm', 3 => 'Ít quan tâm');
        $assignee_tool_list = array();
        foreach ($assignee_list_data as $assignee_data) {
            $assignee_list[$assignee_data->id] = $assignee_data->username . ' - ' . $assignee_data->full_name;
            $caller_list[$assignee_data->id] = $assignee_data->username . ' - ' . $assignee_data->full_name;
            $activator_list[$assignee_data->id] = $assignee_data->username . ' - ' . $assignee_data->full_name;
            if($assignee_data->active == 1){
                if(in_array(Department::HOLDING, Auth::user()->getManageDepartments()) || Auth::user()->hasRole('holding')){
                    $assignee_tool_list[$assignee_data->id] = $assignee_data->username . ' - ' . $assignee_data->full_name . ' - ' . $department_list[$assignee_data->department_id];
                }else{
                    $assignee_tool_list[$assignee_data->id] = $assignee_data->username . ' - ' . $assignee_data->full_name;
                }
            }
        }
        $filtering = array();
        if(empty($request->clear_filter)){
            $user_pref = UserPref::where(['user_id' => Auth::id(), 'key' => 'filtering'])->first();
            if (!empty($user_pref) && !empty($user_pref->value)) {
                $filtering = json_decode($user_pref->value);
            }
        }
        $trunk_list = DB::table('fptu_user_trunk')
            ->join('fptu_trunk', 'fptu_trunk.id', '=', 'fptu_user_trunk.trunk_id')
            ->where('fptu_user_trunk.user_id', Auth::id())->whereNull('fptu_user_trunk.deleted_at')->pluck('fptu_trunk.display_name', 'fptu_trunk.id');
        $trunk_id = 0;
        if (!empty(Auth::id()))
            $trunk_id = User::find(Auth::id())->trunk_id;

        $view = 'lead.index';
        if(Auth::user()->isSMISUser()){
            $view = 'lead.index_smis';
        }elseif(Auth::user()->isGISUser()){
            $view = 'lead.index_gis';
        }elseif(Auth::user()->isASCUser()){
            $view = 'lead.index_asc';
        }elseif(!Auth::user()->isASCUser() && Auth::user()->hasRole('holding')){
            $view = 'lead.index';
        }

        if (Auth::user()->hasRole('carer')) {
            $view = 'lead.index_carer';
        }

        if (Auth::check())
            return view($view, [
                'source_list' => $source_list,
                'campaign_list' => $campaign_list,
                'campaign_category_list' => $campaign_category_list,
                'channel_list' => $channel_list,
                'keyword_list' => $keyword_list,
                'province_list' => $province_list,
                'department_list' => $department_list,
                'group_list' => $group_list,
                'assignee_list' => $assignee_list,
                'activator_list' => $activator_list,
                'creator_list' => $creator_list,
                'assignee_tool_list' => $assignee_tool_list,
                'status_list' => $status_list,
                'call_status_list' => $call_status_list,
                'caller_list' => $caller_list,
                'rating_list' => $rating_list,
                'filtering' => $filtering,
                'trunk_list' => $trunk_list,
                'major_list' => $major_list,
                'trunk_id' => $trunk_id
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lead = new Lead;
        $status_list = array();
        $status_list_data = LeadStatus::where('hide', 0)->orderBy('sort_index', 'ASC')->get();
        foreach ($status_list_data as $status_data) {
            $status_list[$status_data->id] = $status_data->name;
        }
        $call_status_list = array(0 => 'Chưa chăm sóc');
        $call_status_list_data = CallStatus::all();
        foreach ($call_status_list_data as $call_status_data) {
            $call_status_list[$call_status_data->id] = $call_status_data->name;
        }
        $source_list = Source::listDataByUserDepartment();
        $objective_list = array();
        $objective_list = Objective::all();
        $parents_concern_list = array();
        $parents_concern_list = ParentsConcern::all();
        $student_challenge_list = array();
        $student_challenge_list = StudentChallenge::all();
        $department_list = Department::where('hide', 0)->orderBy('sort_index', 'ASC')->pluck('name', 'id')->toArray();
        $info_source_list = array();
        $info_source_list_data = InfoSource::all();
        foreach ($info_source_list_data as $info_source_data) {
            $info_source_list[$info_source_data->id] = $info_source_data->name;
        }
        $promotion_list = array();
        $promotion_list_data = Promotion::all();
        foreach ($promotion_list_data as $promotion_data) {
            $promotion_list[$promotion_data->id] = $promotion_data->name;
        }
        $province_list = array();
        $district_list = array();
        $district_list = District::get()->pluck('name', 'id');
        $province_list_data = Province::all();
        foreach ($province_list_data as $province_data) {
            $province_list[$province_data->id] = $province_data->name;
        }
        $assignee_list = array();
        $assignee_list_data = User::all();
        foreach ($assignee_list_data as $assignee_data) {
            $assignee_list[$assignee_data->id] = $assignee_data->username;
        }
        $campus_list = array();
        $campus_list_data = Campus::all();
        foreach ($campus_list_data as $campus_data) {
            $campus_list[$campus_data->id] = $campus_data->name;
        }
        $channel_list = array();
        $channel_list_data = Channel::orderBy('name', 'ASC')->get();
        foreach ($channel_list_data as $channel_data) {
            $channel_list[$channel_data->id] = $channel_data->name;
        }
        $exam_place_list = array();
        $exam_place_list_data = Campus::all();
        foreach ($exam_place_list_data as $exam_place_data) {
            $exam_place_list[$exam_place_data->id] = $exam_place_data->name;
        }
        $major_list = array();
        $major_list_data = Major::where('hide', 0)->get();
        foreach ($major_list_data as $major_data) {
            $major_list[$major_data->id] = $major_data->name;
        }
        $rating_list = array(1 => 'Rất quan tâm', 2 => 'Quan tâm', 3 => 'Ít quan tâm');

        $request = array();
        return view('includes.lead.create', ['lead' => $lead, 'status_list' => $status_list, 'source_list' => $source_list, 'department_list' => $department_list, 'info_source_list' => $info_source_list, 'promotion_list' => $promotion_list, 'province_list' => $province_list, 'district_list' => $district_list, 'assignee_list' => $assignee_list, 'campus_list' => $campus_list, 'exam_place_list' => $exam_place_list, 'major_list' => $major_list, 'rating_list' => $rating_list, 'request' => $request, 'channel_list' => $channel_list, 'objective_list' => $objective_list, 'student_challenge_list' => $student_challenge_list, 'parents_concern_list' => $parents_concern_list]);
    }

    public function import(Request $request)
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        if ($request->isMethod('post')) {
            if(!empty($request->file('import'))){
                $importer = new Importer;
                $result = $importer->import($request->file('import')->path());
                return view('lead.import', ['result' => $result]);
            }
        }
        return view('lead.import');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = false;
        $return_message = '';
        $sendRequestOracleERP = FALSE;
        $lead = new Lead;
        //var_dump($lead);
        $lead->name = $request->name;
        $lead->nick_name = $request->nick_name;
        $lead->gender = $request->gender;
        $lead->student_name = $request->student_name;
        $lead->email = $request->email;
        $lead->email_2 = $request->email_2;
        $lead->facebook = $request->facebook;
        $source_id = $request->source;
        $channel_id = $request->channel;
        $campaign = $request->utm_campaign;
        if (!empty($request->birthdate))
            $lead->birthdate = Utils::convertDate($request->birthdate);
        $lead->phone1 = Utils::sanitizePhoneV2($request->phone1);
        $lead->phone2 = Utils::sanitizePhoneV2($request->phone2);
        $lead->status = $request->status;
        if (empty($request->status))
            $lead->status = LeadStatus::STATUS_NEW;
        else {
            switch ($request->status) {
                case LeadStatus::STATUS_SHOWUP_SCHOOL:
                    $lead->showup_school_at = date('Y-m-d H:i:s');
                    break;
                case LeadStatus::STATUS_WAITLIST:
                    $lead->activated = 1;
                    $lead->activator = Auth::id();
                    $lead->activated_at = date('Y-m-d H:i:s');
                    $sendRequestOracleERP = TRUE;
                    break;
                case LeadStatus::STATUS_ENROLL:
                    $lead->enrolled = 1;
                    $lead->enrolled_at = date('Y-m-d H:i:s');
                    break;
                case LeadStatus::STATUS_NEW_SALE:
                    $lead->new_sale = 1;
                    $lead->new_sale_at = date('Y-m-d H:i:s');
                    $sendRequestOracleERP = TRUE;
                    break;
                case LeadStatus::STATUS_HOT_LEAD:
                    $lead->hot_lead_at = date('Y-m-d H:i:s');
                    break;
            }
        }
        //$lead->source = $request->source;

        if(!empty($request->department_id)){
            $lead->department_id = $request->department_id;
        }else{
            $lead->department_id = Auth::user()->getManageDepartments()[0];
        }
//        $lead->utm_source = $request->utm_source;
        $lead->utm_campaign = $request->utm_campaign;
//        $lead->utm_medium = $request->utm_source;
        $lead->info_source = $request->info_source;
        if ($request->twins == 1)
            $lead->twins = $request->twins;
        $lead->promotion = $request->promotion;
        $lead->province_id = $request->province_id;
        $lead->address = $request->address;
        if (!empty($request->school)){
            if($request->school == Utils::OTHER_OPTION && !empty($request->other_school)){
                if(!empty($request->other_school)){
                    $school = School::where('name', $request->other_school)->first();
                    if(!$school){
                        $school = new School();
                        $school->name = $request->other_school;
                        $school->province = 0;
                        $school->save();
                    }
                    $lead->school = $school->id;
                }
            }else{
                $lead->school = $request->school;
            }
        }
        if (!empty($request->district))
            $lead->district = $request->district;
        $lead->want_study = $request->want_study;
        $lead->want_exam = $request->want_exam;
        $lead->objective_others = $request->objective_others;
        $lead->parents_concern_others = $request->parents_concern_others;
        $lead->student_challenge_others = $request->student_challenge_others;
        $lead->major = $request->major;
        $lead->url_referrer = $request->url_referrer;
        $lead->url_request = $request->url_request;
        $lead->ga_client_id = $request->ga_client_id;
        $lead->rating = $request->rating;
        $lead->notes = $request->notes;
        $lead->created_by = Auth::id();
        $lead->class_name = $request->class_name;
        $lead->learning_system_desire           = $request->learning_system_desire;
        $lead->lead_type                        = $request->lead_type;
        if(!empty($request->enroll_desire_at)){
            $lead->enroll_desire_at = Utils::convertDate($request->enroll_desire_at);
            $lead->enroll_desire_register_at = date("Y-m-d H:i:s");
        }

        if(empty($request->name)){
            $success = false;
            $return_message = 'Họ tên phụ huynh không được phép rỗng';
        }elseif(empty($request->phone1)){
            $success = false;
            $return_message = 'Số điện thoại không được phép rỗng';
        }elseif(empty($request->student_name)){
            $success = false;
            $return_message = 'Họ tên học sinh không được phép rỗng';
        }elseif(!isset($request->status)){
            $success = false;
            $return_message = 'Trạng thái lead không được phép rỗng';
        }elseif (empty($request->source)) {
            $success = false;
            $return_message = 'Nguồn lead không được để trống!';
        } elseif (empty($lead->phone1) && empty($lead->phone2)) {
            $success = false;
            $return_message = 'Bạn cần nhập tối thiểu một số điện thoại!';
        } elseif ((!$lead->checkDuplicate($lead->phone1) && !$lead->checkDuplicate($lead->phone2)) || $request->twins == 1) {
            $lead_created = $lead->save();
            if ($lead_created) {
                if (!empty($source_id)) {
                    $lead_source = new LeadSource;
                    $lead_source->lead_id = $lead->id;
                    $lead_source->source_id = $source_id;
                    $lead_source->created_by = Auth::id();
                    $lead_source->created_at = date('Y-m-d H:i:s');
                    $lead_source->save();
                }
                if (!empty($channel_id)) {
                    $lead_channel = new LeadChannel();
                    $lead_channel->lead_id = $lead->id;
                    $lead_channel->channel_id = $channel_id;
                    $lead_channel->created_by = Auth::id();
                    $lead_channel->created_at = date('Y-m-d H:i:s');
                    $lead_channel->save();
                }
                if(!empty($campaign)){
                    $campaign_id = Campaign::where('name', $campaign)->first()->id;
                    if($campaign_id){
                        $lead_campaign = new LeadCampaign();
                        $lead_campaign->lead_id = $lead->id;
                        $lead_campaign->campaign_id = $campaign_id;
                        $lead_campaign->created_at = date('Y-m-d H:i:s');
                        $lead_campaign->save();
                    }
                }
                if(!empty($request->objective)){
                    foreach ($request->objective as $objective){
                        if($objective != Objective::OTHER_OPTION){
                            $lead_objective = new LeadObjective();
                            $lead_objective->lead_id = $lead->id;
                            $lead_objective->objective_id = $objective;
                            $lead_objective->save();
                        }
                    }
                }
                if(!empty($request->parents_concern)){
                    foreach ($request->parents_concern as $parents_concern){
                        if($parents_concern != ParentsConcern::OTHER_OPTION){
                            $lead_parents_concern = new LeadParentsConcern();
                            $lead_parents_concern->lead_id = $lead->id;
                            $lead_parents_concern->parents_concern_id = $parents_concern;
                            $lead_parents_concern->save();
                        }
                    }
                }
                if(!empty($request->student_challenge)){
                    foreach ($request->student_challenge as $student_challenge){
                        if($student_challenge != StudentChallenge::OTHER_OPTION){
                            $lead_student_challenge = new LeadStudentChallenge();
                            $lead_student_challenge->lead_id = $lead->id;
                            $lead_student_challenge->student_challenge_id = $student_challenge;
                            $lead_student_challenge->save();
                        }
                    }
                }

                if($sendRequestOracleERP){
                    $lead->sendRequestOracleERP();
                }
            }
            //$assignee = $request->assignee;
            $assignee = Auth::id();
            //$existing = ManualLeadAssigner::where(['lead_id' => $lead->id, 'user_id' => $assignee])->count("id");
            //if (empty($existing)) {
            $assigner = new ManualLeadAssigner();
            $assigner->user_id = $assignee;
            $assigner->lead_id = $lead->id;
            $assigner->created_by = Auth::id();
            $assigned = $assigner->save();
            if ($lead_created && $assigned) {
                $success = true;
                $return_message = 'Tạo lead thành công';
                $request->session()->flash('success', $success);
                $request->session()->flash('return_message', $return_message);
                UserPref::updateOrCreate(
                    ['user_id' => Auth::id(), 'key' => 'force_reload'],
                    ['value' => 1]
                );
                return $this->edit($lead->id);
            } else {
                $success = false;
                $return_message = 'Đã có lỗi trong quá trình tạo lead!';
            }
        } else {
            $success = false;
            $return_message = 'Lead trùng số điện thoại!';

            //notify
            $number = $lead->phone1;
            $lead_duplicate = Lead::where(function ($query) use ($number) {
                $query->where('phone1', $number)->orWhere('phone2', $number);
            })
                ->whereNull('deleted_at')
                ->where('department_id', $lead->department_id)
                ->first();
            $user = User::getLeadAssignee($lead_duplicate->id);
            if($user && $user->active == 1){
                $notify = new Notify();
                $notify->content = "Lead \"{$lead->name}\" trùng, vừa đăng ký trở lại";
                $notify->content.= "<br/>Chi tiết: " .env('APP_URL') ."/lead/edit/$lead_duplicate->id";
                $notify->user_id = $user->id;
                $notify->status = Notify::STATUS_NEW;
                $notify->created_at = date('Y-m-d H:i:s');
                $notify->lead_id = $lead_duplicate->id;
                $notify->lead_duplicate = 1;
                $notify->type = Notify::TYPE_ASSIGN_LEAD;
                $notify->save();
            }
        }
        //return view('includes.lead.create', ['lead' => $lead, 'status_list' => $status_list, 'source_list' => $source_list, 'department_list' => $department_list, 'group_list' => $group_list, 'province_list' => $province_list, 'assignee_list' => $assignee_list, 'campus_list' => $campus_list, 'exam_place_list' => $exam_place_list, 'major_list' => $major_list]);
        $request->session()->flash('success', $success);
        $request->session()->flash('return_message', $return_message);
        $request->flash();
        return $this->create();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('lead.edit', $id);
    }


    public function assignLead(Request $request)
    {
        $selected_leads = $request->input('selected');
        $assignee = $request->input('assignee');
        $department = $request->input('department');
        $auth_id = Auth::id();
        $accept_auth_assign_ne_lead = array(
            84, //myhao.lai
            70, //thu
            29, //huyen.dam
            174, //hang
        );
        $user = User::where('id', $assignee)->first();

        foreach ($selected_leads as $lead_id) {
            $lead = Lead::find($lead_id);
            $accept = FALSE;
            if($lead->status == 15){
                if(in_array($auth_id, $accept_auth_assign_ne_lead)){
                    $accept = TRUE;
                }
            }else{
                $accept = TRUE;
            }
            if($accept){
                ManualLeadAssigner::where(['lead_id' => $lead_id])->delete();
                $assigner = new ManualLeadAssigner();
                $assigner->user_id = $assignee;
                $assigner->lead_id = $lead_id;
                $assigner->created_by = $auth_id;
                $assigner->save();

                $update_department = FALSE;
                if((in_array(Department::HOLDING, Auth::user()->getManageDepartments()) || Auth::user()->hasRole('holding'))
                    && $user->department_id != $lead->department_id
                ) {
                    $lead->department_id = $user->department_id;
                    $update_department = TRUE;
                }
                if(!empty($department) && $lead->department_id != $department){
                    $lead->department_id = $department;
                    $update_department = TRUE;
                }
                if($update_department){
                    $lead->save();
                }

                if($user && $user->active == 1){
                    $notify = new Notify();
                    $notify->content = "Lead \"{$lead->name}\" mới được phân công cho bạn bởi ".Auth::user()->username;
                    $notify->content.= "<br/>Chi tiết: " .env('APP_URL') ."/lead/edit/$lead->id";
                    $notify->user_id = $assigner->user_id;
                    $notify->status = Notify::STATUS_NEW;
                    $notify->created_at = date('Y-m-d H:i:s');
                    $notify->lead_id = $lead->id;
                    $notify->type = Notify::TYPE_ASSIGN_LEAD;
                    $notify->save();

                    if($user && !empty($user->email)){
                        $from       = 'CRM';
                        $to         = $user->email;
                        $subject    = "Thông báo Lead mới";
                        $data       = array(
                            'title'         => $subject,
                            'name'          => $lead->name,
                            'phone'         => $lead->phone1,
                            'created_at'    => date("d/m/Y H:i", strtotime($lead->created_at)),
                            'source'        => Auth::user()->username,
                            'url'           => env('APP_URL').'/lead/edit/'.$lead->id,
                        );
                        $template   = 'email.lead.lead_assign';

                        Mail::send($template, $data, function($message) use ($from, $to, $subject) {
                            $message
                                ->from('crm@edufit.vn',$from)
                                ->to($to)
                                ->subject($subject);
                        });
                    }
                }
            }
        }
    }


    public function search()
    {
        $model = new Lead();
        $data = $model->search();

        $export = Input::get('export');
        if (!empty($export)) {
            $exporter = new Lead\LeadExporter();
            $exporter->export($data['data']);
        }

        echo json_encode($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lead = Lead::withTrashed()->where('id', $id)->first();
        $lead_channel = LeadChannel::whereNull('deleted_at')->where('lead_id',$lead->id)->first();
        if($lead_channel){
            $lead->channel = $lead_channel->channel_id;
        }
//        if (!empty($lead->birthdate))
//            $lead->birthdate = Carbon::createFromFormat('Y-m-d', $lead->birthdate)->format('d/m/Y');
        //var_dump($request->birthdate);
        $status_list = $lead->getAvailableStatusListData();
        $source_list_array = array();
        $source_list_data = DB::table('fptu_lead_source')
            ->select('fptu_source.name')
            ->join('fptu_source', 'fptu_lead_source.source_id', '=', 'fptu_source.id')
            ->where('fptu_lead_source.lead_id', '=', $id)
            ->orderBy('fptu_source.name', 'ASC')
            ->distinct()
            ->get();
        foreach ($source_list_data as $source_data) {
            $source_list_array[] = $source_data->name;
        }
        $source_list_string = implode(', ', $source_list_array);
        //var_dump
        $source_list = Source::where('hide', 0)->orderBy('sort_index', 'ASC')->pluck('name', 'id')->toArray();
        $department_list = Department::where('hide', 0)->orderBy('sort_index', 'ASC')->pluck('name', 'id')->toArray();
        $info_source_list = InfoSource::all()->pluck('name', 'id');
        $promotion_list = Promotion::all()->pluck('name', 'id');
        $province_list = array();
        $phone_list = array();
        if (!empty($lead->phone1))
            $phone_list[$lead->phone1] = $lead->phone1;
        if (!empty($lead->phone2))
            $phone_list[$lead->phone2] = $lead->phone2;
        $district_list = District::get()->pluck('name', 'id');
        $province_list_data = Province::all();
        foreach ($province_list_data as $province_data) {
            $province_list[$province_data->id] = $province_data->name;
        }
        $assignment = DB::table('fptu_lead_assignment');
        $assignment_list_data =
            $assignment->join('fptu_user', 'fptu_user.id', '=', 'fptu_lead_assignment.user_id')
                ->select('fptu_user.*')
                ->where('fptu_lead_assignment.lead_id', '=', $id)
                ->whereNull('fptu_lead_assignment.deleted_at')
                ->get();
        $assignee_list = '';
        $assignee_list_data = array();
        $counter = 0;
        foreach ($assignment_list_data as $assignment_data) {
            //var_dump($assignment_data);
            $assignee_list .= $assignment_data->username;
            if (++$counter < count($assignment_list_data))
                $assignee_list .= ', ';
            $assignee_list_data[] = $assignment_data->id;
        }
        $account_status_list = array();
        $account_status_list = array('Thiếu', 'Đủ');
        //$method_normal_list = DB::table('fptu_method_normal')->pluck('id', 'name')->get();
        //$method_scholarship_list = DB::table('fptu_method_scholarship')->pluck('id', 'name')->get();
//        foreach ($account_status_data_list as $account_data_list) {
//            $account_status_list[$account_data_list->id] = $account_data_list->name;
//        }
        $lead->assignee_list = $assignee_list;
        $lead->assignee_list_data = $assignee_list_data;
        $department_name = '';
        if (!empty($lead->department_id)) {
            $department_name = Department::find($lead->department_id)->name;
        } else {
            $department_name = 'Không xác định';
        }
//        $school_required = false;
//        if (!empty($lead->school_other) && empty($lead->school))
//            $school_required = true;
        $account = Account::firstOrCreate(['lead_id' => $lead->id]);
        $major_list = array();
        $major_list_data = Major::where('hide', 0)->get();
        foreach ($major_list_data as $major_data) {
            $major_list[$major_data->id] = $major_data->name;
        }
        $objective_list = Objective::all();
        $parents_concern_list = array();
        $parents_concern_list = ParentsConcern::all();
        $student_challenge_list = array();
        $student_challenge_list = StudentChallenge::all();
        $campus_list = array();
        $campus_list_data = Campus::all();
        foreach ($campus_list_data as $campus_data) {
            $campus_list[$campus_data->id] = $campus_data->name;
        }
        $exam_place_list = array();
        $exam_place_list_data = Campus::all();
        foreach ($exam_place_list_data as $exam_place_data) {
            $exam_place_list[$exam_place_data->id] = $exam_place_data->name;
        }
        //$call_status_list = array('na' => 'Không xác định');
        $call_status_list_data = CallStatus::all();
        foreach ($call_status_list_data as $call_status_data) {
            $call_status_list[$call_status_data->id] = $call_status_data->name;
        }
        $call_log_status_list = Lead\CallLog::getListStatus();
        $receiving_place_list = array(1 => 'HN', 2 => 'DN', 3 => 'CT', 4 => 'HCM');
        $campaign_list = array(1 => 'Đợt 1', 2 => 'Đợt 2', 3 => 'Đợt 3', 4 => 'Đợt 4');
        $account_financial_status_list = array('' => 'Chọn trạng thái', 1 => 'Đợt 1', 2 => 'Đợt 2', 3 => 'Đợt 3', 4 => 'Đợt 4');
        $payment_method_list = array('' => 'Chọn hình thức nộp tiền', 1 => '', 2 => '');
        $rating_list = array(1 => 'Rất quan tâm', 2 => 'Quan tâm', 3 => 'Ít quan tâm');
        $change_log = ChangeLog::where('object', 'lead')
            ->where('object_id', $lead->id)
            ->orWhere('object', 'account')
            ->where('object_id', $id)
            ->orWhere('object_id', $account->id)
            ->get();
        foreach ($change_log as $log) {
            $changer = User::find($log->changer);
            if (!empty($changer) && !empty($changer->username)) {
                $log->changer = User::find($log->changer)->username . ' - ' . User::find($log->changer)->full_name;
            } else {
                $log->changer = 'Deleted user';
            }
            $content = '';
            if ($log->type == 'create') {
                $content .= "Nhập mới trường <strong>{$log->field_display_name}</strong> với giá trị <strong>'" . $log->value_after . '\'</strong>';
            }
            if ($log->type == 'edit') {
                $content .= "Chỉnh sửa trường <strong>{$log->field_display_name}</strong> từ '<strong>{$log->value_before_description}</strong>' thành '<strong>{$log->value_after_description}</strong>'";
            }
            $log->log .= $content;
        }
        $assignee_array = ManualLeadAssigner::where('lead_id', $id)->whereNull('deleted_at')->distinct('user_id')->pluck('user_id')->toArray();
        //dd($assignee_list);
        $duplicate_warning = array(
            'warning' => false,
            'message' => ''
        );
        if(!empty($lead->email)){
            $duplicate_found = DB::table('fptu_lead')->whereNull('deleted_at')->whereRaw("trim(email) = ?", array($lead->email))->get()->count();
            if ($duplicate_found > 1) {
                $duplicate_warning['warning'] = true;
                $duplicate_warning['message'] .= 'Email <strong>' . trim(strtolower($lead->email)) . '</strong> đã trùng với lead khác! ';
            }
        }
        if (!empty($lead->name) && !empty($lead->birthdate) && !empty($lead->province_id)) {
            $duplicate_found_2 = DB::table('fptu_lead')->whereNull('deleted_at')->whereRaw('trim(name) = ? AND birthdate = ? AND province_id = ?', array($lead->name, $lead->birthdate, $lead->province_id))->get()->count();
            if ($duplicate_found_2 > 1) {
                $duplicate_warning['warning'] = true;
                $duplicate_warning['message'] .= 'Có một lead khác đã trùng tên, ngày sinh & tỉnh với lead này! ';
            }
        }
        $sms_list = Sms::whereIn('to', $phone_list)->get();
        $call_history_list = Lead\CallLog::where('lead_id', $id)->whereNull('deleted_at')->orderBy('created_at', 'DESC')->get();
        $log_status_list = LeadLogStatus::where('lead_id', $id)->whereNull('deleted_at')->orderBy('created_at', 'DESC')->get();
        $log_sms_list = LogSms::findByLead($id);
        $log_alarm_list = Alarm::where('object_id', $id)->where('object_type',Alarm::OBJECT_TYPE_LEAD)->orderByRaw('alarm_at DESC, alarm_before ASC')->get();
        $user_list = User::all()->pluck('username', 'id');
        $channel_list = Channel::orderBy('name', 'ASC')->get()->pluck('name', 'id');
        $list_lead_objective = LeadObjective::where('lead_id', $lead->id)->whereNull('deleted_at')->pluck('objective_id')->toArray();
        $list_lead_parents_concern = LeadParentsConcern::where('lead_id', $lead->id)->whereNull('deleted_at')->pluck('parents_concern_id')->toArray();
        $list_lead_student_challenge = LeadStudentChallenge::where('lead_id', $lead->id)->whereNull('deleted_at')->pluck('student_challenge_id')->toArray();
        $log_lead_care_list = LogLeadCare::where('lead_id', $id)->whereNull('deleted_at')->orderBy('created_at', 'DESC')->get();

        return view('includes.lead.edit', [
            'id' => $id,
            'lead' => $lead,
            'account' => $account,
            'status_list' => $status_list,
            'source_list' => $source_list,
            'source_list_string' => $source_list_string,
            'source_list_array' => $source_list_array,
            'department_list' => $department_list,
            'info_source_list' => $info_source_list,
            'promotion_list' => $promotion_list,
            'province_list' => $province_list,
            'district_list' => $district_list,
            'account_status_list' => $account_status_list,
            'major_list' => $major_list,
            'campaign' => $campaign_list,
            'account_financial_status_list' => $account_financial_status_list,
            'payment_method_list' => $payment_method_list,
            'receiving_place_list' => $receiving_place_list,
            'change_log' => $change_log,
            'exam_place_list' => $exam_place_list,
            'campus_list' => $campus_list,
            'call_status_list' => $call_status_list,
            'call_log_status_list' => $call_log_status_list,
            'rating_list' => $rating_list,
            'duplicate_warning' => $duplicate_warning,
            'phone_list' => $phone_list,
            'sms_list' => $sms_list,
            'call_history_list' => $call_history_list,
            'department_name' => $department_name,
            'objective_list' => $objective_list,
            'student_challenge_list' => $student_challenge_list,
            'parents_concern_list' => $parents_concern_list,
            'assignee_array' => $assignee_array,
            'log_status_list' => $log_status_list,
            'log_sms_list' => $log_sms_list,
            'log_alarm_list' => $log_alarm_list,
            'user_list' => $user_list,
            'list_lead_objective' => $list_lead_objective,
            'list_lead_parents_concern' => $list_lead_parents_concern,
            'list_lead_student_challenge' => $list_lead_student_challenge,
            'channel_list' => $channel_list,
            'log_lead_care_list' => $log_lead_care_list
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    private
    function log($change_type, $object_id, $object_type, $field_name, $field_display_name, $before_value, $before_value_description = '', $after_value, $after_value_description = '')
    {
        // var_dump($request->$field_name);
        // var_dump($lead->$field_name);
        // if(empty($before_value)) {
        // $before_value = $lead->$field_name;
        // $after_value = $request->$field_name;
        // }''
        if ($before_value != $after_value) {
            $changer = Auth::user()->id;
            $is_disciple = null;
            if (!empty(Auth::user()->sifu)) {
                $changer = Auth::user()->sifu;
                $is_disciple = 1;
            }
            ChangeLog::create(['type' => $change_type, 'object' => $object_type, 'object_id' => $object_id, 'field_name' => $field_name, 'field_display_name' => $field_display_name, 'value_before' => $before_value, 'value_after' => $after_value, 'changer' => $changer, 'disciple' => $is_disciple]);
        }
    }

    public
    function update(Request $request, $id)
    {
        $sendRequestOracleERP = FALSE;
        $lead = Lead::find($id);
        $lead_channel = LeadChannel::whereNull('deleted_at')->where('lead_id',$lead->id)->first();
        if($lead_channel){
            $lead->channel = $lead_channel->channel_id;
        }
        $old_status = $lead->status;
        $old_call_status = $lead->call_status;
        $old_care_status = $lead->care_status;
        $source_list_array = array();
        $source_list_data = DB::table('fptu_lead_source')
            ->select('fptu_source.name')
            ->join('fptu_source', 'fptu_lead_source.source_id', '=', 'fptu_source.id')
            ->where('fptu_lead_source.lead_id', '=', $id)
            ->distinct()
            ->get();
        foreach ($source_list_data as $source_data) {
            $source_list_array[] = $source_data->name;
        }
        $source_list_string = implode(', ', $source_list_array);
        $source_list = Source::where('hide', 0)->orderBy('sort_index', 'ASC')->pluck('name', 'id')->toArray();
        $department_list = Department::where('hide', 0)->orderBy('sort_index', 'ASC')->pluck('name', 'id')->toArray();
        $phone_list = array();
        if (!empty($lead->phone1))
            $phone_list[$lead->phone1] = $lead->phone1;
        if (!empty($lead->phone2))
            $phone_list[$lead->phone2] = $lead->phone2;
        $info_source_list = array();
        $info_source_list_data = InfoSource::all();
        foreach ($info_source_list_data as $info_source_data) {
            $info_source_list[$info_source_data->id] = $info_source_data->name;
        }
        $promotion_list = array();
        $promotion_list_data = Promotion::all();
        foreach ($promotion_list_data as $promotion_data) {
            $promotion_list[$promotion_data->id] = $promotion_data->name;
        }
        $department_name = '';
        if (!empty($lead->department_id)) {
            $department_name = Department::find($lead->department_id)->name;
        } else {
            $department_name = 'Không xác định';
        }
        $objective_list = array();
        $objective_list = Objective::all();
        $parents_concern_list = array();
        $parents_concern_list = ParentsConcern::all();
        $student_challenge_list = array();
        $student_challenge_list = StudentChallenge::all();
        $district_list = District::get()->pluck('name', 'id');
        $province_list = array();
        $province_list_data = Province::all();
        foreach ($province_list_data as $province_data) {
            $province_list[$province_data->id] = $province_data->name;
        }
        $assignment = DB::table('fptu_lead_assignment');
        $assignment_list_data =
            $assignment->join('fptu_user', 'fptu_user.id', '=', 'fptu_lead_assignment.user_id')
                ->select('fptu_user.*')
                ->where('fptu_lead_assignment.lead_id', '=', $id)
                ->whereNull('fptu_lead_assignment.deleted_at')
                ->get();
        $assignee_list = '';
        $assignee_list_data = array();
        $assignment_count = 0;
        foreach ($assignment_list_data as $assignment_data) {
            $assignee_list .= $assignment_data->username;
            if ($assignment_count > 0)
                $assignee_list .= '<br />';
            $assignee_list_data[] = $assignment_data->id;
            $assignment_count++;
        }
        $account_status_list = array();
        $account_status_data_list = AccountStatus::all();
        foreach ($account_status_data_list as $account_data_list) {
            $account_status_list[$account_data_list->id] = $account_data_list->name;
        }
        $account = Account::firstOrCreate(['lead_id' => $lead->id]);
        $major_list = array();
        $major_list_data = Major::where('hide', 0)->get();
        foreach ($major_list_data as $major_data) {
            $major_list[$major_data->id] = $major_data->name;
        }
        $campus_list = array();
        $campus_list_data = Campus::all();
        foreach ($campus_list_data as $campus_data) {
            $campus_list[$campus_data->id] = $campus_data->name;
        }
        $exam_place_list = array();
        $exam_place_list_data = DataList::getItems(1);
        foreach ($exam_place_list_data as $exam_place_data) {
            $exam_place_list[$exam_place_data->id] = $exam_place_data->display_name;
        }
        $call_status_list_data = CallStatus::all();
        foreach ($call_status_list_data as $call_status_data) {
            $call_status_list[$call_status_data->id] = $call_status_data->name;
        }
        $rating_list = array(1 => 'Rất quan tâm', 2 => 'Quan tâm', 3 => 'Ít quan tâm');
        $call_log_status_list = Lead\CallLog::getListStatus();
        $change_log = ChangeLog::where('object', 'lead')
            ->where('object_id', $lead->id)
            ->orWhere('object', 'account')
            ->where('object_id', $id)
            ->orWhere('object_id', $account->id)
            ->get();


        $return_message = '';
        $success = true;
        $duplicate = false;

        if(empty($request->name)){
            $success = false;
            $return_message = 'Họ tên phụ huynh không được phép rỗng';
        }
        if(empty($request->phone1)){
            $success = false;
            $return_message = 'Số điện thoại không được phép rỗng';
        }
        if(empty($request->student_name)){
            $success = false;
            $return_message = 'Họ tên học sinh không được phép rỗng';
        }
        if(!isset($request->status)){
            $success = false;
            $return_message = 'Trạng thái lead không được phép rỗng';
        }

        $phone1 = Utils::sanitizePhoneV2($request->phone1);
        $phone2 = Utils::sanitizePhoneV2($request->phone2);
        if(!empty($request->phone1) && empty($phone1)){
            $success = false;
            $return_message = 'Số điện thoại 1 không hợp lệ';
        }
        if(!empty($request->phone2) && empty($phone2)){
            $success = false;
            $return_message = 'Số điện thoại 2 không hợp lệ';
        }
        if (!empty($request->birthdate) && !Utils::validateDisplayBirthdate($request->birthdate)) {
            $success = false;
            $return_message = 'Ngày sinh không hợp lệ!';
        }
        /*if (($request->status == 7) && ($request->department == Auth::user()->department_id)) {
            $success = false;
            $return_message = 'Cần xác định tên phòng cần chuyển đi đối với lead chuyển cơ sở!';
        }*/

        if(!empty($request->leave_school) && empty($request->leave_school_reason)){
            $success = FALSE;
            $return_message = 'Lý do thôi học không được phép rỗng!';
        }
        if(!empty($request->leave_school_reason) && empty($request->leave_school)){
            $success = FALSE;
            $return_message = 'Hộp chọn thôi học chưa được tích!';
        }

        if($success){
            if (!empty($phone1) && $lead->checkDuplicate($phone1) && empty($request->twins)) {
                $duplicate = true;
                $return_message = 'Số điện thoại 1 ' . $phone1 . ' bị trùng!';
            }
            if (!empty($phone2) && $lead->checkDuplicate($phone2) && empty($request->twins)) {
                $duplicate = true;
                $return_message = "Số điện thoại 2 " . $phone2 . " bị trùng!";
            }
            if ($duplicate) {
                $phone_list = array();
                $success = false;
                if (!empty($phone1))
                    $phone_list[] = $phone1;
                if (!empty($phone2))
                    $phone_list[] = $phone2;
                $dup_lead = null;
                foreach ($phone_list as $phone) {
                    $dup_lead = Lead::where('phone1', $phone)
                        ->orWhere('phone2', $phone)
                        ->get();
                    //var_dump($dup_lead);
                }
            }
        }

        if (!empty($lead) && $success) {
//            $this->log('edit', $lead->id, 'lead', 'name', 'Họ và tên', $lead->name, $lead->name, $request->name, $request->name);
            $lead->name = $request->name;
            $lead->nick_name = $request->nick_name;
            $lead->gender = $request->gender;
            if ($request->twins == 1)
                $lead->twins = $request->twins;
            $lead->siblings_info = $request->siblings_info;
            $lead->address = $request->address;
            $lead->student_name = $request->student_name;
            $lead->objective_others = $request->objective_others;
            $lead->parents_concern_others = $request->parents_concern_others;
            $lead->student_challenge_others = $request->student_challenge_others;
//            $this->log('edit', $lead->id, 'lead', 'email', 'Email', $lead->email, $lead->email, $request->email, $request->email);
            //$this->log($lead, $request, 'email', 'Email');
            $lead->email = $request->email;
            $lead->email_2 = $request->email_2;
//            $this->log('edit', $lead->id, 'lead', 'facebook', 'Facebook', $lead->facebook, $lead->facebook, $request->facebook, $request->facebook);
            //$this->log($lead, $request, 'facebook', 'Facebook');
            $lead->facebook = $request->facebook;
//            $this->log('edit', $lead->id, 'lead', 'birthdate', 'Ngày sinh', $lead->birthdate, $lead->birthdate, $request->birthdate, $request->birthdate);
            //$this->log($lead, $request, 'birthdate', 'Ngày sinh');
            if (!empty($request->birthdate) && Utils::validateDisplayBirthdate($request->birthdate))
                $lead->birthdate = Carbon::createFromFormat('d/m/Y', $request->birthdate)->format('Y-m-d');

//            $this->log('edit', $lead->id, 'lead', 'phone1', 'SĐT 1', $lead->phone1, $lead->phone1, $request->phone1, $request->phone1);
            $lead->phone1 = $phone1;
//            $this->log('edit', $lead->id, 'lead', 'phone2', 'SĐT 2', $lead->phone2, $lead->phone2, $request->phone2, $request->phone2);
            $lead->phone2 = $phone2;

            $status_before_value_desc = '';
            if (!empty($lead->status) && !empty(LeadStatus::find($lead->status)))
                $status_before_value_desc = LeadStatus::find($lead->status)->name;
            $status_after_value_desc = '';
            if (!empty($request->status) && !empty(LeadStatus::find($lead->status)))
                $status_after_value_desc = LeadStatus::find($request->status)->name;

            if($request->status != $old_status){
                $lead->status = $request->status;
            }
            /* lưu vết thời gian WL*/
            if ($request->activated == 1 && $lead->activated != 1) {
                $lead->activator = Auth::user()->id;
                $lead->activated_at = date('Y-m-d H:i:s');
                $lead->activated = 1;
                if(!in_array($lead->status, array(
                    LeadStatus::STATUS_ENROLL,
                    LeadStatus::STATUS_NEW_SALE,
                ))){
                    $lead->status = LeadStatus::STATUS_WAITLIST;
                    $sendRequestOracleERP = TRUE;
                }
                if (!in_array($lead->activator, $assignee_list_data)) {
                    $manual_assigner = new ManualLeadAssigner;
                    $manual_assigner->user_id = $lead->activator;
                    $manual_assigner->lead_id = $lead->id;
                    $manual_assigner->save();
                }
            }
            /* Lưu vết thời gian NS*/
            if ($request->new_sale == 1 && $lead->new_sale != 1) {
                $lead->new_sale_at = date('Y-m-d H:i:s');
                $lead->new_sale = 1;
                if(!in_array($lead->status, array(LeadStatus::STATUS_ENROLL))){
                    $lead->status = LeadStatus::STATUS_NEW_SALE;
                    $sendRequestOracleERP = TRUE;
                }
            }

            /* lưu vết thời gian nhập học*/
            if ($request->enrolled == 1 && $lead->enrolled != 1) {
                $lead->enrolled_at = date('Y-m-d H:i:s');
                $lead->enrolled = 1;
                $lead->status = LeadStatus::STATUS_ENROLL;
            }

            /* lưu vết thời gian đến trường*/
            if($request->status == LeadStatus::STATUS_SHOWUP_SCHOOL && empty($lead->showup_school_at)){
                $lead->showup_school_at = date('Y-m-d H:i:s');
            }

            /* lưu vết thời gian đến hội thảo*/
            /*if($request->status == 8 && empty($lead->showup_seminor_at)){
                $lead->showup_seminor_at = date('Y-m-d H:i:s');
            }*/

            /* Lưu vết bé làm khách */
            if($request->status == LeadStatus::STATUS_GUEST && empty($lead->join_as_guest_at)){
                $lead->join_as_guest_at = date('Y-m-d H:i:s');
            }

            /* Lưu vết hot lead */
            if($request->status == LeadStatus::STATUS_HOT_LEAD && empty($lead->hot_lead_at)){
                $lead->hot_lead_at = date('Y-m-d H:i:s');
            }

            /* Lưu vết qualified lead */
            if($request->call_status_id == CallStatus::STATUS_QUALIFIED_LEAD && empty($lead->qualified_at)){
                $lead->qualified_at = date('Y-m-d H:i:s');
            }

            /* Lưu vết new promoter */
            if($request->new_promoter){
                $lead->new_promoter = 1;
                if(empty($lead->new_promoter_at)){
                    $lead->new_promoter_at = date('Y-m-d H:i:s');
                }
            }

//            $this->log('edit', $lead->id, 'lead', 'status', 'Trạng thái lead', $old_status, $status_before_value_desc, $lead->status, $status_after_value_desc);
            //$source_value = '';
//            if (!empty($lead->source)) {
//                $source = Source::where('name', $request->source)->first();
//                if (!empty($source)) {
//                    $source_value = $source->name;
//                    $lead_source = new LeadSource;
//                    $lead_source->lead_id = $id;
//                    $lead_source->source_id = $source->id;
//                    $lead_source->save();
//                } else {
//                    $success = false;
//                    $return_message = 'Nguồn lead không hợp lệ!';
//                }
//            }
            //if (!empty($lead->source))
            //$this->log('edit', $lead->id, 'lead', 'source', 'Nguồn lead', $source_value, '', Source::find($request->source)->name, '');
            //$this->log($lead, $request, 'source', 'Nguồn lead');
            //$lead->source = $request->source;
            //this->log($lead, $request, 'department', 'Phòng');
            $source_value = '';
//            if (!empty($lead->department))
//                $this->log('edit', $lead->id, 'lead', 'department', 'Phòng', Department::find($lead->department_id)->name, '', Department::find($request->department)->name, '');
            if (!empty($request->department))
                $lead->department_id = $request->department;
            //$this->log($lead, $request, 'utm_source', 'Kênh quảng cáo');
//            $this->log('edit', $lead->id, 'lead', 'utm_source', 'Kênh quảng cáo', $lead->utm_source, '', $request->utm_source, '');
//            $lead->utm_source = $request->utm_source;
//            //$this->log($lead, $request, 'utm_campaign', 'Chiến dịch quảng cáo');
//            $this->log('edit', $lead->id, 'lead', 'utm_campaign', 'Chiến dịch quảng cáo', $lead->utm_campaign, '', $request->utm_campaign, '');
            $lead->utm_campaign = $request->utm_campaign;
//            $this->log('edit', $lead->id, 'lead', 'utm_medium', 'Từ khóa quảng cáo', $lead->utm_medium, '', $request->utm_medium, '');
////			  $this->log($lead, $request, 'utm_medium', 'Từ khóa quảng cáo');
//            $lead->utm_medium = $request->utm_medium;
            $lead->info_source = $request->info_source;
            $lead->promotion = $request->promotion;
            $lead->province_id = $request->province_id;
//            $this->log('edit', $lead->id, 'lead', 'school', 'Trường', $lead->school, '', $request->school, '');
            if (!empty($request->school)){
                if($request->school == Utils::OTHER_OPTION && !empty($request->other_school)){
                    if(!empty($request->other_school)){
                        $school = School::where('name', $request->other_school)->first();
                        if(!$school){
                            $school = new School();
                            $school->name = $request->other_school;
                            $school->province = 0;
                            $school->save();
                        }
                        $lead->school = $school->id;
                    }
                }else{
                    $lead->school = $request->school;
                }
            }
            $lead->district = $request->district;
            //$this->log($lead, $request, 'want_study', 'Mong muốn học');
            $lead->want_study = $request->want_study;
            //$this->log($lead, $request, 'want_exam', 'Mong muốn thi');
            $lead->want_exam = $request->want_exam;
            //$this->log($lead, $request, 'major', 'Ngành đăng ký');
            $lead->major = $request->major;
            //$this->log($lead, $request, 'url_referrer', 'URL Referrer');
            $lead->url_referrer = $request->url_referrer;
            //$this->log($lead, $request, 'url_request', 'URL Request');
            $lead->url_request = $request->url_request;
            //$this->log($lead, $request, 'ga_client_id', 'GA Client ID');
            $lead->ga_client_id = $request->ga_client_id;
//            $this->log('edit', $lead->id, 'lead', 'notes', 'Ghi chú', $lead->notes, $lead->notes, $request->notes, $request->notes);
            //$this->log($lead, $request, 'notes', 'Ghi chú');
            $lead->notes = $request->notes;
            $lead->study_place = $request->study_place;
            $lead->exam_place = $request->exam_place;
//            $this->log('edit', $lead->id, 'lead', 'call_status', 'Trạng thái chăm sóc', $lead->call_status, '', $request->call_status_id, '');
            $lead->call_status = $request->call_status_id;
            $lead->rating = $request->rating;

            $lead->call_status_no_concern_reason    = $request->call_status_no_concern_reason;
            $lead->learning_system_desire           = $request->learning_system_desire;
            $lead->lead_type                        = $request->lead_type;

            $lead->status_waitlist_number           = $request->status_waitlist_number;
            $lead->receipt_number                   = $request->receipt_number;
            $lead->student_code                     = $request->student_code;
            $lead->class_name                       = $request->class_name;
            $lead->official_class                   = $request->official_class;
            $lead->learning_system_official         = $request->learning_system_official;

            $lead->guest_class_name                 = $request->guest_class_name;
            $lead->guest_class                      = $request->guest_class;
            $lead->learning_system_guest            = $request->learning_system_guest;
            $lead->status_guest_number              = $request->status_guest_number;

            $lead->waitlist_class                   = $request->waitlist_class;
            $lead->learning_system_waitlist         = $request->learning_system_waitlist;

            $lead->care_status                      = $request->care_status;

            if(!empty($request->enroll_desire_at)){
                $lead->enroll_desire_at = Utils::convertDate($request->enroll_desire_at);
                if(empty($lead->enroll_desire_register_at)){
                    $lead->enroll_desire_register_at = date("Y-m-d H:i:s");
                }
            }

            if(!empty($request->leave_school)){
                $lead->leave_school = $request->leave_school;
                $lead->leave_school_reason = $request->leave_school_reason;
                if(empty($lead->leave_school_register_at)){
                    $lead->leave_school_register_at = date('Y-m-d H:i:s');
                }
            }

            if ($success !== FALSE) {
                $lead->new_duplicate = 0;
                unset($lead->channel);
                $saved = $lead->save();
                    if ($saved) {

                        if (!empty($request->source)) {
                            $lead_source = new LeadSource;
                            $lead_source->lead_id = $lead->id;
                            $lead_source->source_id = $request->source;
                            $lead_source->created_by = Auth::id();
                            $lead_source->created_at = date('Y-m-d H:i:s');
                            $lead_source->save();
                        }

                        if($lead->status != $old_status){
                            $leadLogStatus = new LeadLogStatus();
                            $leadLogStatus->lead_id = $lead->id;
                            $leadLogStatus->status_old = $old_status;
                            $leadLogStatus->status_new = $lead->status;
                            $leadLogStatus->lead_id = $lead->id;
                            $leadLogStatus->created_at = date('Y-m-d H:i:s');
                            $leadLogStatus->created_by = Auth::user()->id;
                            $leadLogStatus->type = LeadLogStatus::TYPE_LEAD_STATUS;
                            $leadLogStatus->save();
                        }

                        if($lead->call_status != $old_call_status){
                            $leadLogStatus = new LeadLogStatus();
                            $leadLogStatus->lead_id = $lead->id;
                            $leadLogStatus->status_old = $old_call_status;
                            $leadLogStatus->status_new = $lead->call_status;
                            $leadLogStatus->lead_id = $lead->id;
                            $leadLogStatus->created_at = date('Y-m-d H:i:s');
                            $leadLogStatus->created_by = Auth::user()->id;
                            $leadLogStatus->type = LeadLogStatus::TYPE_CALL_STATUS;
                            $leadLogStatus->save();
                        }

                        if($lead->care_status != $old_care_status){
                            $leadLogStatus = new LeadLogStatus();
                            $leadLogStatus->lead_id = $lead->id;
                            $leadLogStatus->status_old = $old_care_status;
                            $leadLogStatus->status_new = $lead->care_status;
                            $leadLogStatus->lead_id = $lead->id;
                            $leadLogStatus->created_at = date('Y-m-d H:i:s');
                            $leadLogStatus->created_by = Auth::user()->id;
                            $leadLogStatus->type = LeadLogStatus::TYPE_CARE_STATUS;
                            $leadLogStatus->save();
                        }

                        $success = true;
                        $return_message = 'Đã lưu thành công!';
                        UserPref::updateOrCreate(
                            ['user_id' => Auth::id(), 'key' => 'force_reload'],
                            ['value' => 1]
                        );

                        if($sendRequestOracleERP){
                            $lead->sendRequestOracleERP();
                        }

                        $request->session()->forget('status');

                    } else {
                    $success = true;
                    $return_message = 'Đã có lỗi trong quá trình lưu dữ liệu!';
                }
            }
        }
        $lead->assignee_list = $assignee_list;
        $lead->assignee_list_data = $assignee_list_data;
        $assignee_array = ManualLeadAssigner::where('lead_id', $id)->whereNull('deleted_at')->distinct('user_id')->pluck('user_id')->toArray();
        $status_list = $lead->getAvailableStatusListData();
        $request->session()->flash('success', $success);
        $request->session()->flash('return_message', $return_message);
        $request->flash();
        $sms_list = Sms::whereIn('to', $phone_list)->get();
        $call_history_list = Lead\CallLog::where('lead_id', $id)->whereNull('deleted_at')->orderBy('created_at', 'DESC')->get();
        $log_status_list = LeadLogStatus::where('lead_id', $id)->whereNull('deleted_at')->orderBy('created_at', 'DESC')->get();
        $log_sms_list = LogSms::findByLead($id);
        $log_alarm_list = Alarm::where('object_id', $id)->where('object_type',Alarm::OBJECT_TYPE_LEAD)->orderByRaw('alarm_at DESC, alarm_before ASC')->get();
        $user_list = User::all()->pluck('username', 'id');
        $channel_list = Channel::orderBy('name', 'ASC')->get()->pluck('name', 'id');
        $list_lead_objective = LeadObjective::where('lead_id', $lead->id)->whereNull('deleted_at')->pluck('objective_id')->toArray();
        $list_lead_parents_concern = LeadParentsConcern::where('lead_id', $lead->id)->whereNull('deleted_at')->pluck('parents_concern_id')->toArray();
        $list_lead_student_challenge = LeadStudentChallenge::where('lead_id', $lead->id)->whereNull('deleted_at')->pluck('student_challenge_id')->toArray();
        $log_lead_care_list = LogLeadCare::where('lead_id', $id)->whereNull('deleted_at')->orderBy('created_at', 'DESC')->get();

        return view('includes.lead.edit', [
            'id' => $id,
            'lead' => $lead,
            'account' => $account,
            'status_list' => $status_list,
            'source_list' => $source_list,
            'source_list_array' => $source_list_array,
            'info_source_list' => $info_source_list,
            'promotion_list' => $promotion_list,
            'source_list_string' => $source_list_string,
            'department_list' => $department_list,
            'province_list' => $province_list,
            'district_list' => $district_list,
            'account_status_list' => $account_status_list,
            'major_list' => $major_list,
            'change_log' => $change_log,
            'exam_place_list' => $exam_place_list,
            'campus_list' => $campus_list,
            'assignee_list' => $assignee_list,
            'assignee_array' => $assignee_array,
            'call_status_list' => $call_status_list,
            'rating_list' => $rating_list,
            'success' => $success,
            'call_log_status_list' => $call_log_status_list,
            'return_message' => $return_message,
            'phone_list' => $phone_list,
            'sms_list' => $sms_list,
            'call_history_list' => $call_history_list,
            'department_name' => $department_name,
            'objective_list' => $objective_list,
            'student_challenge_list' => $student_challenge_list,
            'parents_concern_list' => $parents_concern_list,
            'log_status_list' => $log_status_list,
            'log_sms_list' => $log_sms_list,
            'log_alarm_list' => $log_alarm_list,
            'user_list' => $user_list,
            'list_lead_objective' => $list_lead_objective,
            'list_lead_parents_concern' => $list_lead_parents_concern,
            'list_lead_student_challenge' => $list_lead_student_challenge,
            'channel_list' => $channel_list,
            'log_lead_care_list' => $log_lead_care_list
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {

    }


    public
    function clearFiltering()
    {
        $filtering = UserPref::where(['user_id' => Auth::id(), 'key' => 'filtering'])->first();
        if (!empty($filtering))
            $deleted = UserPref::where(['user_id' => Auth::id(), 'key' => 'filtering'])->delete();
        else
            return 'ok';
        if ($deleted)
            return 'ok';
        return 'error';
    }

    public
    function call($phone, $trunk = '')
    {
        $pbx = new Pbx;
        $pbx->dial($phone, $trunk);
    }

    public
    function call_test($phone, $trunk = '')
    {
        $pbx = new Pbx;
        $pbx->dial_test($phone, $trunk);
    }

    /**
     * Send SMS
     */
    public function sendSms(Request $request)
    {
        $return = array(
            'success' => false,
            'message' => '',
            'data' => array(),
            'dataHtml' => '',
        );
        if(!empty($request->brandname)){
            $brandname = $request->brandname;
        }else{
            $brandname = Telcom::getAuthBrandName();
        }
        $content = Utils::utf8convert(trim($request->sms_content));
        $lead_id = $request->lead_id;
        $lead = Lead::find($lead_id);
        $target = $lead->getSMSTarget();
        $params = [
            'targets' => $target,
            'content' => $content,
            'brandname' => $brandname
        ];

        if(!empty($target) && !empty($content) && !empty($brandname)){
            $log_sms = new LogSms();
            $log_sms->brand_name = $brandname;
            $log_sms->target = $target;
            $log_sms->content = $content;
            $log_sms->created_by = Auth::user()->id;
            $log_sms->created_at = date('Y-m-d H:i:s');
            $log_sms->lead_id = $lead_id;
            $log_sms->type = LogSms::TYPE_SINGLE;
            if (!empty(session('original_user_id'))) {
                $log_sms->original_user_id = session('original_user_id');
            }

            $response = json_decode(Utils::sendSms($target, $content, $brandname));
            $log_sms->send_at = date('Y-m-d H:i:s');
            $log_sms->sent = 1;
            $item['response'] = $response;
            if((!empty($response) && is_object($response) && $response->CODE == 0) || $response === 0){
                $log_sms->status = LogSms::STATUS_SUCCESS;
                $return['success'][] = $item;
            }else{
                $log_sms->status = LogSms::STATUS_FAIL;
                $item['message'] = LogSms::getResponseDescription($response);
                $return['fail'][] = $item;
            }
            $log_sms->request = json_encode($params);
            $log_sms->response = json_encode($response);
            $log_sms->save();

            $smsListStatus = LogSms::getAllStatus();
            $return['data'] = $log_sms->getAttributes();
            $return['dataHtml'] = "<tr>
                    <td>*</td>
                    <td>$brandname</td>
                    <td>$target</td>
                    <td>$content</td>
                    <td>{$smsListStatus[$log_sms->status]}</td>
                    <td>".date('d/m/Y H:i:s', strtotime($log_sms->send_at))."</td>
                    <td>".date('d/m/Y H:i:s', strtotime($log_sms->created_at))."</td>
                    <td>".Auth::user()->username."</td>
                </tr>";

        }else{
            if(empty($target)){
                $return['message'].= "Số điện thoại rỗng\n";
            }
            if(empty($content)){
                $return['message'].= "Nội dung tin nhắn rỗng\n";
            }
            if(empty($brandname)){
                $return['message'].= "Brandname rỗng\n";
            }
        }

        echo json_encode($return);
    }

    /**
     * Send SMS Multiple
     */
    public function sendSmsMultiple(Request $request)
    {
        $return = array(
            'success' => array(),
            'fail' => array(),
        );
        if(!empty($request->brandname)){
            $brandname = $request->brandname;
        }else{
            $brandname = Telcom::getAuthBrandName();
        }
        $content = Utils::utf8convert($request->sms_content);
        $list_lead_id = $request->selected;
        $leads = Lead::whereIn('id', $list_lead_id)->get();
        $targets = array();
        $lead_ids = array();

        foreach ($leads as $lead){
            $lead_ids[] = $lead->id;
            $target = $lead->getSMSTarget();
            if(!empty($target) && !in_array($target, $targets)){
                $targets[] = $target;
            }
        }

        $str_targets = implode(',',$targets);
        $str_lead_ids = implode(',',$lead_ids);

        $item = array(
            'id' => $lead_ids,
            'response' => '',
            'message' => '',
        );

        if(!empty($targets) && !empty($content) && !empty($brandname)){
            $log_sms = new LogSms();
            $log_sms->brand_name = $brandname;
            $log_sms->target = $str_targets;
            $log_sms->content = $content;
            $log_sms->created_by = Auth::user()->id;
            $log_sms->created_at = date('Y-m-d H:i:s');
            $log_sms->lead_id = $str_lead_ids;
            $log_sms->type = LogSms::TYPE_MULTIPLE;
            if (!empty(session('original_user_id'))) {
                $log_sms->original_user_id = session('original_user_id');
            }

            $params = [
                'targets' => $str_targets,
                'content' => $content,
                'brandname' => $brandname
            ];
            $response = json_decode(Utils::sendSms($str_targets, $content, $brandname));
            $log_sms->send_at = date('Y-m-d H:i:s');
            $log_sms->sent = 1;
            $item['response'] = $response;
            if((!empty($response) && is_object($response) && $response->CODE == 0) || $response === 0){
                $log_sms->status = LogSms::STATUS_SUCCESS;
                $return['success'][] = $item;
            }else{
                $log_sms->status = LogSms::STATUS_FAIL;
                $item['message'] = LogSms::getResponseDescription($response);
                $return['fail'][] = $item;
            }
            $log_sms->request = json_encode($params);
            $log_sms->response = json_encode($response);
            $log_sms->save();
        }else{
            if(empty($targets)){
                $item['message'].= "Số điện thoại rỗng\n";
            }
            if(empty($content)){
                $item['message'].= "Nội dung tin nhắn rỗng\n";
            }
            if(empty($brandname)){
                $item['message'].= "Brandname rỗng\n";
            }
            $return['fail'][] = $item;
        }

        echo json_encode($return);
    }

    public function getUserByDepartment(Request $request)
    {
        $return = array(
            'option' => array(
                'creator' => '',
                'assignee' => '',
                'activator' => '',
            ),
        );
        $departments = $request->departments;
        $department_list = Department::where('hide', 0)->orderBy('sort_index', 'ASC')->pluck('name', 'id')->toArray();
        if(!empty($departments)){
            $users = DB::table('fptu_user')->orderBy('username', 'ASC')->whereIn('department_id', $departments)->get();
        }else{
            $users = DB::table('fptu_user')->whereIn('department_id',array_keys($department_list))->orderBy('username', 'ASC')->get();
        }
        if(!empty($users)) {
            foreach ($users as $user) {
                $return['option']['creator'] .= "<option value='$user->id'>$user->username - $user->full_name</option>";
            }
        }

        if (Auth::user()->hasRole('admin')) {
            $assignee_list_data = User::whereHas('roles', function ($q) {
                $q->where('name', 'telesales')
                    ->orWhere('name', 'teleteamleader');
            })->whereIn('department_id',array_keys($department_list))->orderBy('username', 'ASC');
        } elseif (Auth::user()->hasRole('holding')) {
            $assignee_list_data = User::whereHas('roles', function ($q) {
                $q->where('name', 'telesales')
                    ->orWhere('name', 'teleteamleader');
            })->whereIn('department_id',array_keys($department_list))->orderBy('username', 'ASC');
        } else {
            $assignee_list_data = User::whereHas('roles', function ($q) {
                $q->where('name', 'telesales')
                    ->orWhere('name', 'teleteamleader');
            })->whereIn('department_id', Auth::user()->getManageDepartments())->orderBy('username', 'ASC');
        }

        if(!empty($departments)){
            $assignee_list_data = $assignee_list_data->whereIn('department_id', $departments)->get();
        }else{
            $assignee_list_data = $assignee_list_data->get();
        }

        $return['option']['assignee'].= "<option value='nobody'>Chưa được chia</option>";
        foreach ($assignee_list_data as $assignee_data) {
            $return['option']['assignee'].= "<option value='$assignee_data->id'>$assignee_data->username - $assignee_data->full_name</option>";
            $return['option']['activator'].= "<option value='$assignee_data->id'>$assignee_data->username - $assignee_data->full_name</option>";
        }

        echo json_encode($return);
    }


    public function getCampaignByCategory(Request $request)
    {
        $return = array(
            'option' => array(
                'campaign' => '',
            ),
        );
        $campaign_categories = $request->campaign_categories;
        if(!empty($campaign_categories)){
            $campaign_categories_query = implode(',',$campaign_categories);
            $campaigns = DB::table('fptu_campaign')->where('hide', 0)->whereRaw("id IN (SELECT campaign_id FROM fptu_campaign_category_mapping WHERE category_id IN ($campaign_categories_query) AND deleted_at IS NULL)")->get();
        }else{
            $campaigns = DB::table('fptu_campaign')->where('hide', 0)->get();
        }


        if(!empty($campaigns)) {
            foreach ($campaigns as $campaign) {
                $return['option']['campaign'] .= "<option value='$campaign->id'>$campaign->name</option>";
            }
        }
        echo json_encode($return);
    }

    public function createLogLeadCare(Request $request)
    {
        $return = array(
            'success' => false,
            'message' => '',
            'data' => array(),
        );

        if(!empty($request->lead_id) && !empty($request->channel) && !empty($request->log_content)){
            $model = new LogLeadCare();
            $model->lead_id = $request->lead_id;
            $model->channel = $request->channel;
            $model->content = $request->log_content;
            $model->created_by = Auth::user()->id;
            if($model->save()){
                $return['success'] = TRUE;
                $return['data'] = array(
                    'channel' => LogLeadCare::channelListData()[$model->channel],
                    'content' => $model->content,
                    'created_at' => date('d/m/Y', strtotime($model->created_at)),
                    'created_by' => Auth::user()->username,
                );
            };
        }

        echo json_encode($return);
    }


    public function createComplain(Request $request)
    {
        $return = array(
            'success' => false,
            'message' => '',
            'data' => array(),
            'dataHtml' => '',
        );

        if(!empty($request->lead_id) && !empty($request->reason)){
            $model = new LeadComplain();
            $model->lead_id     = $request->lead_id;
            $model->complain_id = $request->reason;
            $model->note        = $request->note;
            $model->status      = LeadComplain::STATUS_RECEIVED;
            $model->created_by  = Auth::user()->id;
            if($model->save()){
                $return['success'] = TRUE;
                $return['data'] = $model->getAttributes();
                $return['dataHtml'] = "<tr data-id='{$model->id}'>
                    <td>*</td>
                    <td><select name='reason' class='form-control'>".Utils::generateOptions(ComplainReason::listData(), $model->complain_id)."</select></td>
                    <td><textarea name='note' class='form-control'>{$model->note}</textarea></td>
                    <td>".Auth::user()->username."</td>
                    <td>".Carbon::parse($model->created_at)->format('d/m/Y H:i:s')."</td>
                    <td><select name='status' class='form-control'>".Utils::generateOptions(LeadComplain::statusListData(), $model->status)."</select></td>
                    <td>
                        <a class='btn btn-primary btnSave' onclick='updateComplain(".$model->id.")'>Lưu</a>
                        <span class='loading spin'><i class='fa fa-spinner'></i></span>
                    </td>
                </tr>";
            }
        }

        echo json_encode($return);
    }


    public function updateComplain(Request $request)
    {
        $return = array(
            'success' => false,
            'message' => '',
            'data' => array(),
        );

        if(!empty($request->complain_id)){
            $model = LeadComplain::find($request->complain_id);
            $model->complain_id = $request->reason;
            $model->note        = $request->note;
            $model->status      = $request->status;
            if($model->save()){
                $return['success'] = TRUE;
                $return['data'] = $model->getAttributes();
            };
        }

        echo json_encode($return);
    }
}
