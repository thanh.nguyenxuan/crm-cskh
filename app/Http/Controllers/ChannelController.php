<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Channel;

class ChannelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $channel_list = array();
        $channel_list = Channel::all();
        return view('channel.index', ['channel_list' => $channel_list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('channel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = 0;
        $return_message = '';
        $found = Channel::where('name', $request->name)
            ->count();
        $error = false;
        if ($found) {
            $return_message = 'Tên kênh quảng cáo đã tồn tại.';
            $error = true;
        } else {
            $return_message = 'Tạo kênh quảng cáo mới thành công!';
            $success = 1;
            $channel = new Channel();
            $channel->name = trim($request->name);
            $saved = $channel->save();
            if (!$saved) {
                $return_message = 'Đã có lỗi trong quá trình lưu dữ liệu!';
                $success = 0;
                $error = true;
            }
        }
        $request->flash();
        if ($error)
            return view('channel.create', ['success' => $success, 'message' => $return_message]);
        return view('channel.edit', ['channel' => $channel, 'success' => $success, 'message' => $return_message]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $channel = Channel::find($id);
        return view('channel.edit', ['channel' => $channel]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $success = 0;
        $channel = Channel::find($id);
        $error = false;
        $return_message = '';

        if (!$error) {
            $update = array(
                'name' => $request->name
            );

            $updated = $channel->update($update);
            if ($updated) {
                $return_message = 'Đã cập nhật dữ liệu thành công!';
                $success = 1;
            } else {
                $return_message = 'Đã có lỗi trong quá trình cập nhật dữ liệu.';
                $success = 0;
            }
        }
        //$user = User::find($id);
        return view('channel.edit', ['success' => $success, 'channel' => $channel, 'message' => $return_message]);
    }

    public function toggle($id)
    {
        $success = 0;
        $info_source = Channel::find($id);
        $error = false;
        $return_message = '';
        $update = array();
        if ($info_source->hide == 1) {
            $info_source->hide = 0;
        } else {
            $info_source->hide = 1;
        }

        $updated = $info_source->save();
        if ($updated) {
            $return_message = 'Đã cập nhật trạng thái lớp thành công!';
            $success = 1;
        } else {
            $return_message = 'Đã có lỗi trong quá trình cập nhật dữ liệu.';
            $success = 0;
        }
        $info_source_list = array();
        $info_source_list = Channel::all();
        return view('channel.index', ['success' => $success, 'message' => $return_message, 'channel_list' => $info_source_list]);
    }
}
