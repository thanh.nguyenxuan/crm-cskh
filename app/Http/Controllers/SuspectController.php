<?php

namespace App\Http\Controllers;

use App\Department;
use App\Lead;
use App\LeadSource;
use App\LeadStatus;
use App\ManualLeadAssigner;
use App\Source;
use App\Suspect\Suspect;
use App\SuspectImporter;
use App\SuspectStatus;
use App\User;
use App\Utils;
use Illuminate\Http\Request;
use App\Channel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SuspectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $suspect_list = Suspect::search();

        $departmentListData = Department::all()->pluck('name', 'id');
        $userListData       = User::all()->pluck('username', 'id');
        $sourceListData     = Source::all()->pluck('name', 'id');;
        $statusListData     = SuspectStatus::all()->pluck('name', 'id');;

        session()->flashInput($request->input());
        return view('suspect.index', [
            'suspect_list'          => $suspect_list,
            'departmentListData'    => $departmentListData,
            'userListData'          => $userListData,
            'sourceListData'        => $sourceListData,
            'statusListData'        => $statusListData,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('suspect.create');
    }

    public function show($id)
    {
        return redirect()->route('suspect.edit', $id);
    }

    public function import(Request $request)
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        if ($request->isMethod('get')) {
            return view('suspect.import');
        }
        if ($request->isMethod('post')) {
            $importer = new SuspectImporter();
            $result = $importer->import($request->file('import')->path());
            return view('suspect.import', ['result' => $result]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    private function checkIfPhoneDuplicate($number, $department_id)
    {
        if (!empty($number)) {
            $count = Suspect::where(function ($query) use ($number) {
                $query->where('phone1', $number)
                    ->orWhere('phone2', $number);
            })
                ->whereNull('deleted_at')
                ->where('department_id', $department_id)
                ->count();
            if ($count > 0)
                return true;
        }
        return false;
    }

    public function store(Request $request)
    {
        $success = 0;
        $return_message = '';
        $error = false;
        $name = $request->name;
        $phone1 = Utils::sanitizePhone($request->phone1);
        $phone2 = Utils::sanitizePhone($request->phone2);
        if (empty($phone1) || (strlen($phone1) != 10 && strlen($phone1) != 11)) {
            $error = true;
            $return_message .= 'Số điện thoại 1 không hợp lệ! ';
        }
        $email = $request->email;
        $source = $request->source;
        $note = $request->note;
        $status = $request->status;
        $assignee = $request->assignee;
        if(Auth::user()->hasRole('holding') || (Auth::user()->hasRole('admin') && in_array(Department::HOLDING, Auth::user()->getManageDepartments()))){
            $department_id = $request->get('department_id');
        }else{
            $department_id = Auth::user()->getManageDepartments()[0];
        }
        if ($this->checkIfPhoneDuplicate($phone1, $department_id)) {
            $error = true;
            $return_message .= 'Số điện thoại 1 bị trùng! ';
        }
        if (!empty($phone2) && $this->checkIfPhoneDuplicate($phone2, $department_id)) {
            $error = true;
            $return_message .= 'Số điện thoại 2 bị trùng! ';
        }
        $request->flash();
        if ($error)
            return view('suspect.create', ['success' => $success, 'message' => $return_message]);
        $success = true;
        $suspect = new Suspect();
        $suspect->name = $name;
        $suspect->phone1 = $phone1;
        $suspect->phone2 = $phone2;
        $suspect->email = $email;
        $suspect->source = $source;
        $suspect->status = $status;
        $suspect->department_id = $department_id;
        $suspect->assignee = $assignee;
        $suspect->note = $note;
        $suspect->save();
        $return_message = 'Dữ liệu data đã được lưu!';
        return view('suspect.edit', ['suspect' => $suspect, 'success' => $success, 'message' => $return_message]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $suspect = Suspect::find($id);
        return view('suspect.edit', ['suspect' => $suspect]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $success = false;
        $suspect = Suspect::find($id);
        $error = false;
        $return_message = '';

        $name = $request->name;
        $phone1 = Utils::sanitizePhone($request->phone1);
        $phone2 = Utils::sanitizePhone($request->phone2);
        if (empty($phone1) || (strlen($phone1) != 10 && strlen($phone1) != 11)) {
            $error = true;
            $return_message .= 'Số điện thoại 1 không hợp lệ! ';
        }
        $email = $request->email;
        $source = $request->source;
        $note = $request->note;
        $status = $request->status;
        $assignee = $request->assignee;
        if(!empty($request->get('department_id'))){
            $department_id = $request->get('department_id');
        }else{
            $department_id = $suspect->department_id;
        }
        if ($suspect->checkDuplicate($phone1, $department_id)) {
            $error = true;
            $return_message .= 'Số điện thoại 1 bị trùng! ';
        }
        if (!empty($phone2) && $suspect->checkDuplicate($phone2, $department_id)) {
            $error = true;
            $return_message .= 'Số điện thoại 2 bị trùng! ';
        }
        $request->flash();
        if (!$error) {
            $success = true;
            $suspect->name = $name;
            $suspect->phone1 = $phone1;
            $suspect->phone2 = $phone2;
            $suspect->email = $email;
            $suspect->source = $source;
            $suspect->status = $status;
            $suspect->department_id = $department_id;
            $suspect->assignee = $assignee;
            $suspect->note = $note;
            $suspect->save();
            $return_message = 'Dữ liệu data đã được lưu!';
        }
        return view('suspect.edit', ['success' => $success, 'suspect' => $suspect, 'message' => $return_message]);
    }


    public function convertToLead(Request $request)
    {
        $return = array(
            'success' => TRUE,
            'message' => '',
            'data'    => []
        );

        if(!empty($request->suspect_id)){

            $suspect = Suspect::find($request->suspect_id);

            $lead = new Lead();
            $lead->name                 = $suspect->name;
            $lead->phone1               = $suspect->phone1;
            $lead->phone2               = $suspect->phone2;
            $lead->email                = $suspect->email;
            $lead->department_id        = $suspect->department_id;
            $lead->status               = LeadStatus::STATUS_TAKING_CARE;

            if($lead->checkDuplicate($lead->phone1) || $lead->checkDuplicate($lead->phone2)){
                $return['success'] = FALSE;
                $return['message'] = 'Lead đã tồn tại ở cơ sở này';
            }else{
                if($lead->save()){
                    $lead_source = new LeadSource;
                    $lead_source->lead_id = $lead->id;
                    $lead_source->source_id = 6; //Mass Data
                    $lead_source->created_by = Auth::id();
                    $lead_source->created_at = date('Y-m-d H:i:s');
                    $lead_source->save();

                    $assigner = new ManualLeadAssigner();
                    $assigner->user_id = !empty($suspect->assignee) ? $suspect->assignee : Auth::id();
                    $assigner->lead_id = $lead->id;
                    $assigner->created_by = Auth::id();
                    $assigner->save();

                    $suspect->status = SuspectStatus::STATUS_CONVERTED;
                    $suspect->convert = 1;
                    $suspect->converted_at = date('Y-m-d H:i:s');
                    $suspect->lead_id = $lead->id;
                    $suspect->save();
                }else{
                    $return['success'] = FALSE;
                    $return['message'] = 'Chuyển đổi thất bại';
                };
            }
        }else{
            $return['success'] = FALSE;
            $return['message'] = 'Không tìm thấy dữ liệu';
        }


        echo json_encode($return);
    }
}
