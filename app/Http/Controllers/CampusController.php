<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Campus;

class CampusController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $campus_list = array();
        $campus_list = Campus::all();
        return view('campus.index', ['campus_list' => $campus_list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('campus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = 0;
        $return_message = '';
        $found = Campus::where('name', $request->name)
            ->count();
        $error = false;
        if ($found) {
            $return_message = 'Tên nguồn thông tin đã tồn tại.';
            $error = true;
        } else {
            $return_message = 'Tạo nguồn thông tin mới thành công!';
            $success = 1;
            $campus = new Campus();
            $campus->code = trim($request->code);
            $campus->name = trim($request->name);
            $saved = $campus->save();
            if (!$saved) {
                $return_message = 'Đã có lỗi trong quá trình lưu dữ liệu!';
                $success = 0;
                $error = true;
            }
        }
        $request->flash();
        if ($error)
            return view('campus.create', ['success' => $success, 'message' => $return_message]);
        return view('campus.edit', ['campus' => $campus, 'success' => $success, 'message' => $return_message]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $campus = Campus::find($id);
        return view('campus.edit', ['campus' => $campus]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $success = 0;
        $campus = Campus::find($id);
        $error = false;
        $return_message = '';
        $campus->code = trim($request->code);
        $campus->name = trim($request->name);
        $updated = $campus->save();
        if ($updated) {
            $return_message = 'Đã cập nhật dữ liệu thành công!';
            $success = 1;
        } else {
            $return_message = '<div>Đã có lỗi trong quá trình cập nhật dữ liệu.</div>';
            $success = 0;
        }
        return view('campus.edit', ['success' => $success, 'campus' => $campus, 'message' => $return_message]);
    }

    public function toggle($id)
    {
        $success = 0;
        $campus = Campus::find($id);
        $error = false;
        $return_message = '';
        $update = array();
        if ($campus->hide == 1) {
            $campus->hide = 0;
        } else {
            $campus->hide = 1;
        }
        $updated = $campus->save();
        if ($updated) {
            $return_message = 'Đã cập nhật trạng thái lớp thành công!';
            $success = 1;
        } else {
            $return_message = 'Đã có lỗi trong quá trình cập nhật dữ liệu.';
            $success = 0;
        }
        $campus_list = array();
        $campus_list = Campus::all();
        return view('campus.index', ['success' => $success, 'message' => $return_message, 'campus_list' => $campus_list]);
    }
}
