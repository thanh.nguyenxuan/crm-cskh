<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 08/12/2017
 * Time: 10:33 AM
 */

namespace App\Http\Controllers;

//use Illuminate\Http\Request;
use App\Subiz;
use App\Utils;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Lead;
use App\LeadSource;
use App\Province;

class SubizController extends Controller
{

    public function getResponse(Request $request)
    {
        $raw = Input::all();
        if (!empty($raw) && isset($raw['secret']) && $raw['secret'] == 'tstt2018') {
            $subiz = new Subiz;
            $subiz->event = $raw['event_name'];
            if (!empty($raw['data']['department']) && !empty($raw['data']['department']['name']))
                $subiz->province = $raw['data']['department']['name'];
            $subiz->country_code = $raw['data']['session']['country_code'];
            $subiz->country_name = $raw['data']['session']['country_name'];
            $subiz->email = trim(strtolower($raw['data']['visitor']['email']));
            $subiz->name = $raw['data']['visitor']['name'];
            $phone = $raw['data']['visitor']['phone'];
            $phone = Utils::sanitizePhone($phone);
            $subiz->phone = $phone;
            $subiz->raw = var_export($raw, true);
            $subiz->save();
            if (!empty($phone) && !Utils::checkDuplicatePhone($phone)) {
                $this->generateLead($subiz);
            } else {
                $subiz->result = 2; // Duplicate
                $subiz->save();
            }
        }
    }

    public function generateLead($subiz)
    {
        $lead = new Lead;
        $lead->name = $subiz->name;
        $lead->email = $subiz->email;
        $lead->phone1 = $subiz->phone;
        $message = '';
        $province = Utils::sanitize_province($subiz->province);
        if (!empty($province) && !Utils::validate_province($province)) {
            $message .= 'Tỉnh: ' . $province;

        }
        $province_data = null;
        if (!empty($province)) {
            $province_data = Province::where('name_upper', $province)->first();
            if (!empty($province_data)) {
                $lead->department_id = $province_data->department_id;
                $lead->province_id = $province_data->id;
                $lead->area_id = $province_data->area;
            }
        }
        $lead->notes .= $message;
        $lead->created_by = 1; // System
        $lead->created_at = date('Y-m-d H:i:s');
        $lead_created = $lead->save();
        if ($lead_created) {
            $lead_source = new LeadSource;
            $lead_source->lead_id = $lead->id;
            $lead_source->source_id = 2; //SB
            $lead_source->created_by = 1; // System
            $lead_source->created_at = date('Y-m-d H:i:s');
            $lead_source->save();
            $subiz->lead_id = $lead->id;
            $subiz->result = 1;
            $subiz->save();
        }
        return true;
    }
}