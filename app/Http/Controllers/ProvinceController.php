<?php

namespace App\Http\Controllers;

use App\Department;
use App\Province;

class ProvinceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $province_list = Province::all();
        foreach ($province_list as $province) {
            $province->department = Department::find($province->department_id)->name;
        }
        return view('province.index', ['province_list' => $province_list]);
    }
}