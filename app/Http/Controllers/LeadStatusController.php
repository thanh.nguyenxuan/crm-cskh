<?php

namespace App\Http\Controllers;

use App\Lead;
use Illuminate\Http\Request;
use App\LeadStatus;

class LeadStatusController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lead_status_list = array();
        $lead_status_list = LeadStatus::all();
        return view('lead_status.index', ['lead_status_list' => $lead_status_list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('lead_status.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = 0;
        $return_message = '';
        $found = LeadStatus::where('name', $request->name)
            ->count();
        $error = false;
        if ($found) {
            $return_message = 'Tên trạng thái lead đã tồn tại.';
            $error = true;
        } else {
            $return_message = 'Tạo trạng thái lead mới thành công!';
            $success = 1;
            $lead_status = new LeadStatus();
            $lead_status->name = trim($request->name);
            $saved = $lead_status->save();
            if (!$saved) {
                $return_message = 'Đã có lỗi trong quá trình lưu dữ liệu!';
                $success = 0;
                $error = true;
            }
        }
        $request->flash();
        if ($error)
            return view('lead_status.create', ['success' => $success, 'message' => $return_message]);
        return view('lead_status.edit', ['lead_status' => $lead_status, 'success' => $success, 'message' => $return_message]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $lead_status = LeadStatus::find($id);
        return view('lead_status.edit', ['lead_status' => $lead_status]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $success = 0;
        $lead_status = LeadStatus::find($id);
        $error = false;
        $return_message = '';

        if (!$error) {
            $update = array(
                'name' => $request->name
            );

            $updated = $lead_status->update($update);
            if ($updated) {
                $return_message = 'Đã cập nhật dữ liệu thành công!';
                $success = 1;
            } else {
                $return_message = 'Đã có lỗi trong quá trình cập nhật dữ liệu.';
                $success = 0;
            }
        }
        //$user = User::find($id);
        return view('lead_status.edit', ['success' => $success, 'lead_status' => $lead_status, 'message' => $return_message]);
    }

    public function toggle($id)
    {
        $success = 0;
        $lead_status = LeadStatus::find($id);
        $error = false;
        $return_message = '';
        $update = array();
        if ($lead_status->hide == 1) {
            $lead_status->hide = 0;
        } else {
            $lead_status->hide = 1;
        }

        $updated = $lead_status->save();
        if ($updated) {
            $return_message = 'Đã cập nhật trạng thái lớp thành công!';
            $success = 1;
        } else {
            $return_message = 'Đã có lỗi trong quá trình cập nhật dữ liệu.';
            $success = 0;
        }
        $lead_status_list = array();
        $lead_status_list = LeadStatus::all();
        return view('lead_status.index', ['success' => $success, 'message' => $return_message, 'lead_status_list' => $lead_status_list]);
    }
}
