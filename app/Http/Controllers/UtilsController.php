<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 24/11/2017
 * Time: 2:32 PM
 */

namespace App\Http\Controllers;


use App\Sms;
use App\UserPref;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Weight;
use App\User;
use App\DataList;
use App\ManualLeadAssigner;
use App\Campus;
use App\Lead;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class UtilsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function getSchools(Request $request, $province, $selected = null)
    {
        //$province = $request->input('province');
        $query = $request->term;
        //var_dump($query);
        //echo $province;
        //var_dump($request);
        $where = array(
            ['province', '=', $province]
        );
        if (!empty($query))
            $where[] = ['name', 'like', '%' . $query . '%'];
        //if (!empty($selected))
        //$where[] = ['id', '=', $selected];
        $school_list = \App\School::where($where)->pluck('name', 'id')->all();
        //$return = a;
        $data = array();
        foreach ($school_list as $id => $school) {
            $item = new \stdClass();
            $item->id = $id;
            $item->text = $school;
            if ($item->id == $selected)
                $item->selected = true;
            $data[] = $item;
        }
        $return = new \stdClass();
        $return->results = $data;
        echo json_encode($return);
    }

    public function getDistricts(Request $request, $province, $selected = null)
    {
        //$province = $request->input('province');
        $query = $request->term;
        //var_dump($query);
        //echo $province;
        //var_dump($request);
        $where = array(
            ['province', '=', $province]
        );
        if (!empty($query))
            $where[] = ['name', 'like', '%' . $query . '%'];
//        if (!empty($selected))
//            $where[] = ['id', '=', $selected];
        $district_list = \App\District::where($where)->pluck('name', 'id')->all();
        //$return = a;
        $data = array();
        foreach ($district_list as $id => $district) {
            $item = new \stdClass();
            $item->id = $id;
            $item->text = $district;
            if ($item->id == $selected)
                $item->selected = true;
            $data[] = $item;
        }
        $return = new \stdClass();
        $return->results = $data;
        echo json_encode($return);
    }

    public function getOnline($user_id)
    {
        $online = \App\Online::where('user_id', $user_id)->pluck('minutes')->first();
        return $online;
    }

    public function setOnline($user_id, $plus_extra_minutes = true)
    {
        //$online = \App\Online::where('user_id', $user_id)->pluck('minutes')->first();
        $last_online = \App\Online::where('user_id', $user_id)->pluck('last_online')->first();
        $minutes = $this->getOnline($user_id);
        $online = \App\Online::where('user_id', $user_id)->pluck('last_online')->first();
        $allowed_time = strtotime($online) + env('ONLINE_CHECK_INTERVAL') / 1000 + env('ONLINE_CHECK_TIMEOUT') / 1000;
        $now = time();
        if ($allowed_time < $now)
            $plus_extra_minutes = false;
        if ($plus_extra_minutes) {
            $minutes += (ceil(env('ONLINE_CHECK_INTERVAL') / 60000));
            $last_online = date('Y-m-d H:i:s', strtotime($last_online) + env('ONLINE_CHECK_INTERVAL') / 1000);
        } else {
            $last_online = date('Y-m-d H:i:s');
        }
        \App\Online::where('user_id', $user_id)->update(['minutes' => $minutes, 'last_online' => $last_online]);
        return $last_online;
    }

    public function checkCost()
    {

        echo $this->getLeadCost(4, 'DKDT', 'Google', 56);
    }

    public function getLeadCost($user_id = '', $registration_type = '', $lead_source = '', $province = '', $school = '', $birthyear = '', $channel = '', $campaign = '', $keyword = '')
    {
        //\App\Online::where('user_id', $user_id, $registration_type, $lead_source, $lead_source, $school, $birthyear, $channel, $campaign, $keyword)->
        $cost = 0;
        //$cost += Online::getMinutes();
        $cost += Weight::getWeight('registration_type', $registration_type);
        $cost += Weight::getWeight('lead_source', $lead_source);
        $cost += Weight::getWeight('province', $province);
        $cost += Weight::getWeight('school', $school);
        $cost += Weight::getWeight('birthyear', $birthyear);
        $cost += Weight::getWeight('channel', $channel);
        $cost += Weight::getWeight('campaign', $campaign);
        $cost += Weight::getWeight('keyword', $keyword);
        //var_dump($cost);
        return $cost;
    }

    public static function getOnlineStatus($user_id)
    {
        $online = \App\Online::where('user_id', $user_id)->pluck('last_online')->first();
        $allowed_time = strtotime($online) + env('ONLINE_CHECK_INTERVAL') / 1000 + env('ONLINE_CHECK_TIMEOUT') / 1000;
        $now = time();
        $is_online = ($allowed_time > $now);
        //var_dump($online);
//        echo 'Last Online: ' . date('d/m/Y H:i:s', strtotime($online)) . '<br />';
//        echo 'Now: ' . date('d/m/Y H:i:s') . '<br />';
//        echo 'Maximum Allowed: ' . date('d/m/Y H:i:s', $allowed_time) . '<br />';
//        echo 'Online Status: ' . ($is_online ? 'Online' : 'Offline');
        return $is_online;
    }

    public function getWeight(Request $request)
    {
        $weight = Weight::getWeight($request->input('name'), $request->input('value'));
        var_dump($weight);
    }

    public function checkIfPhoneExists($phone)
    {
        $existing = Lead::where('phone1', $phone)
            ->whereOr('phone2', $phone)
            ->whereOr('phone3', $phone)
            ->whereOr('phone4', $phone)->count();
        echo $existing;
    }

    public function getOnlineUsers()
    {
        $users = User::all();
        foreach ($users as $user) {
            if ($this->getOnlineStatus($user->id)) {
                echo $user->username . ' is online.<br />';
            }
        }
    }

    public function checkList()
    {
        //var_dump(DataList::getItems(1));
        var_dump(Campus::all());
    }

    public function fix_assignment()
    {
        $leads = DB::table('fptu_lead')->where('department_id', 4)
            ->whereNotNull('activator')->where('status', 5)
            ->whereRaw(" activator NOT IN (SELECT user_id FROM fptu_lead_assignment WHERE lead_id = fptu_lead.id) ")
            ->get();
        foreach ($leads as $lead) {
            $activator = $lead->activator;
            $activated_at = $lead->activated_at;
            if (!empty($activator) && !empty($activated_at)) {
                $manual_assigner = new ManualLeadAssigner;
                $manual_assigner->user_id = $activator;
                $manual_assigner->lead_id = $lead->id;
                $manual_assigner->created_at = $activated_at;
                $manual_assigner->save();
                echo 'Fixed ' . $lead->id . '<br />';
            }
        }
    }

    public function sendSms()
    {
        $host = '118.70.133.195';
        $port = '5038';
        $username = 'tsttfptu';
        $password = 'tstt@2017$';
        $dest = trim(Input::get('sms_to'));
        $message = trim(Input::get('sms_content'));
        $id = rand(1, 1000);
        $trunk = intval(Input::get('sms_port')) + 1;
        $socket = stream_socket_client("tcp://$host:$port");
        if ($socket) {
            echo "Connected to socket, sending authentication request.\n";
            // Prepare authentication request
            $authenticationRequest = "Action: Login\r\n";
            $authenticationRequest .= "Username: $username\r\n";
            $authenticationRequest .= "Secret: $password\r\n\r\n";
            // Send authentication request
            $authenticate = stream_socket_sendto($socket, $authenticationRequest);
            //var_dump($authenticate);
            if ($authenticate > 0) {
                // Wait for server response
                usleep(200000);
                // Read server response
                $authenticateResponse = fread($socket, 40096);
                // Check if authentication was successful
                if (strpos($authenticateResponse, 'Success') !== false) {
                    echo "Authenticated to AMI. Sending SMS.\n";
                    // Prepare originate request
                    $smsCommand = "Action: smscommand\r\n";
                    $message = str_replace("\r\n", " ", $message);
                    $message = str_replace("\n", " ", $message);
                    $smsCommand .= "command: gsm send sms $trunk $dest \"$message\" $id\r\n\r\n";
                    echo $smsCommand;
                    // Send originate request
                    $originate = stream_socket_sendto($socket, $smsCommand);
                    if ($originate > 0) {
                        // Wait for server response
                        usleep(200000);
                        // Read server response
                        $originateResponse = fread($socket, 60096);
                        // Check if originate was successful
                        echo "SMS sent.";
                        $sms = new Sms;
                        $sms->port = $trunk - 1;
                        $sms->to = $dest;
                        $sms->type = 1;
                        $sms->content = $message;
                        $sms->created_by = Auth::id();
                        $sms->created_at = date('Y-m-d H:i:s');
                        $sms->save();
                    }
                } else {
                    echo 'Authentication failed!';
                    //var_dump($authenticateResponse);
                }
            } else {
                echo 'Cannot authenticate!';
            }
        }

    }

    public function forceReload()
    {
        $force_reload = UserPref::updateOrCreate(
            ['user_id' => Auth::id(), 'key' => 'force_reload'],
            ['value' => 1]
        );
        if (!empty($force_reload) && $force_reload->id) {
            return 'ok';
        }
        return 'error';
    }

    public function getForceReloadStatus()
    {
        $need_reload = UserPref::where('user_id', Auth::id())->where('key', 'force_reload')->pluck('value')->first();
        UserPref::updateOrCreate(
            ['user_id' => Auth::id(), 'key' => 'force_reload'],
            ['value' => 0]
        );
        if (!empty($need_reload)) {
            return 1;
        }
        return 0;
    }


}