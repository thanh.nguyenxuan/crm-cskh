<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Utils;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Auth;
use PAMI\Client\Impl\ClientImpl as PamiClient;
use PAMI\Message\Event\EventMessage;
use PAMI\Message\Event\OriginateResponseEvent;
use PAMI\Listener\IEventListener;
use PAMI\Message\Action\OriginateAction;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function Dmr()
    {
        return view('report.dmr');
    }

    public function showOnlineStatus()
    {
        //echo Utils::toDbDate('23/05/2000');
        $user_list = User::select()->orderBy('username', 'ASC')->get();
        return view('report.online', ['user_list' => $user_list]);
    }

    function get_call_summary_report($from_date, $to_date)
    {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => 'http://' . env('PBX_HOST') . '/api/cdr.php?from_date=' . $from_date . '&to_date=' . $to_date
        ));
        $resp = curl_exec($curl);
        curl_close($curl);
        $data = array();
        if (!empty($resp))
            $data = json_decode($resp);
        return $data;
    }

    function showCallSummaryReport()
    {
        $from_date = Input::get('from_date');
        $to_date = Input::get('to_date');
        if (!empty($from_date))
            $from_date = Utils::convertDate($from_date);
        else
            $from_date = date('Y-m-d');
        if (!empty($to_date))
            $to_date = Utils::convertDate($to_date);
        else
            $to_date = date('Y-m-d');
        $tthn_exts = array(
            1014, 1020, 1021, 1022, 1023, 1024, 1025, 1026, 1027, 1028, 1029, 1030, 1048, 1049, 1050, 1053, 1054, 1070, 1071, 1072, 1073, 1077, 1078, 1079, 4001, 4002, 4003, 4004, 4005, 4006, 4007, 4008, 4009, 4010
        );
        $tthcm_exts = array(
            1031, 1032, 1033, 1034, 1035, 1036, 1037, 1038, 1039, 1040, 1041, 1042, 1051, 1052, 1060, 1061, 1062, 1063, 1064, 1065, 1066, 1067, 1068, 1069, 1074, 1075, 1076, 8001, 8002, 8003
        );
        $data = $this->get_call_summary_report($from_date, $to_date);
        $hn = array();
        $hcm = array();
        foreach ($data as $ext => $details) {
            if (in_array($ext, $tthn_exts)) {
                $hn[$ext] = $details;
            }
            if (in_array($ext, $tthcm_exts)) {
                $hcm[$ext] = $details;
            }
        }
        ksort($hn);
        ksort($hcm);
        Input::flash();
        return view('report.call_summary', ['hn' => $hn, 'hcm' => $hcm]);
    }

    public function showLeadReport(Request $request)
    {
        $from_date = trim($request->get('from-date'));
        $to_date = trim($request->get('to-date'));
        $user_list = DB::table('fptu_user')
            ->join('fptu_user_group', 'fptu_user_group.user_id', '=', 'fptu_user.id')
            ->join('fptu_group', 'fptu_user_group.group_id', '=', 'fptu_group.id')
            ->join('fptu_role_user', 'fptu_user.id', '=', 'fptu_role_user.user_id')
            ->select('fptu_user.username as username', 'fptu_group.name as group_name')
            ->where('fptu_role_user.role_id', 2)
            ->orderBy('fptu_user_group.group_id', 'ASC')
            ->orderBy('fptu_user.username', 'ASC')->get();
        if (!empty($from_date))
            $from_date = Utils::convertDate($from_date);
        else {
            $from_date = date('Y-m-d');
        }
        if (!empty($to_date))
            $to_date = Utils::convertDate($to_date);
        else {
            $to_date = date('Y-m-d');
        }
        $lead_data = DB::table('fptu_lead')
            ->join('fptu_lead_assignment', 'fptu_lead.id', '=', 'fptu_lead_assignment.lead_id')
            ->join('fptu_user', 'fptu_lead_assignment.user_id', '=', 'fptu_user.id')
            ->select(DB::raw('fptu_user.username, count(*) as `count`'))
            ->whereNull('fptu_lead.deleted_at')
            ->groupBy('fptu_user.username');
        $assignment_data = DB::table('fptu_lead')
            ->join('fptu_lead_assignment', 'fptu_lead.id', '=', 'fptu_lead_assignment.lead_id')
            ->join('fptu_user', 'fptu_lead_assignment.user_id', '=', 'fptu_user.id')
            ->select(DB::raw('fptu_user.username, count(DISTINCT(fptu_lead.id)) as `count`'))
            ->whereNull('fptu_lead.deleted_at')
            ->groupBy('fptu_user.username');
        $modified_lead_data = DB::table('fptu_lead')
            //->join('fptu_lead_assignment', 'fptu_lead.id', '=', 'fptu_lead_assignment.lead_id')
            ->join('fptu_change_log', 'fptu_change_log.object_id', '=', 'fptu_lead.id')
            ->join('fptu_user', 'fptu_change_log.changer', '=', 'fptu_user.id')
            ->select(DB::raw('fptu_user.username as username, count(DISTINCT(fptu_change_log.object_id)) as `count`'))
            ->whereNull('fptu_lead.deleted_at')
            ->groupBy('fptu_user.username');
        $converted_data = DB::table('fptu_lead')
            ->join('fptu_user', 'fptu_lead.activator', '=', 'fptu_user.id')
            ->select(DB::raw('fptu_user.username, count(DISTINCT(fptu_lead.id)) as `count`'))
            ->whereNull('fptu_lead.deleted_at')
            ->groupBy('fptu_user.username');
        $active_data = clone $assignment_data;
        $call_later_lead_data = clone $modified_lead_data;
        $no_answer_lead_data = clone $modified_lead_data;
        $wrong_number_lead_data = clone $modified_lead_data;
        $dont_care_lead_data = clone $modified_lead_data;
        $transfered_lead_data = clone $modified_lead_data;
        $duplicate_lead_data = clone $modified_lead_data;
        $mistargeted_lead_data = clone $modified_lead_data;
        if (!empty($from_date) && !empty($to_date)) {
            $lead_data = $lead_data->whereDate('fptu_lead.created_at', '>=', $from_date)
                ->whereDate('fptu_lead.created_at', '<=', $to_date);
            $assignment_data = $assignment_data
                ->whereDate('fptu_lead_assignment.created_at', '>=', $from_date)
                ->whereDate('fptu_lead_assignment.created_at', '<=', $to_date);
            $modified_lead_data = $modified_lead_data->whereDate('fptu_change_log.created_at', '>=', $from_date)
                ->whereDate('fptu_change_log.created_at', '<=', $to_date);
            $active_data = $active_data->whereDate('fptu_lead.activated_at', '>=', $from_date)
                ->where('fptu_lead.department_id', 4)
                ->whereDate('fptu_lead.activated_at', '<=', $to_date);
            $converted_data = $converted_data
                //->where('fptu_lead.department_id', 4)
                ->whereDate('fptu_lead.converted_at', '>=', $from_date)
                ->whereDate('fptu_lead.converted_at', '<=', $to_date);
            $called_lead_data = $this->get_call_summary_report($from_date, $to_date);
            //var_dump($called_lead_data);
            $call_later_lead_data = $call_later_lead_data->whereDate('fptu_change_log.created_at', '>=', $from_date)
                ->whereDate('fptu_change_log.created_at', '<=', $to_date)
                ->where('fptu_lead.department_id', 4)
                ->where('fptu_change_log.field_name', 'status')
                ->where('fptu_change_log.value_after', 2);
            $no_answer_lead_data = $no_answer_lead_data->whereDate('fptu_change_log.created_at', '>=', $from_date)
                ->where('fptu_lead.department_id', 4)
                ->whereDate('fptu_change_log.created_at', '<=', $to_date)
                ->where('fptu_change_log.field_name', 'status')
                ->where('fptu_change_log.value_after', 3);
            $wrong_number_lead_data = $wrong_number_lead_data->whereDate('fptu_change_log.created_at', '>=', $from_date)
                ->where('fptu_lead.department_id', 4)
                ->whereDate('fptu_change_log.created_at', '<=', $to_date)
                ->where('fptu_change_log.field_name', 'status')
                ->where('fptu_change_log.value_after', 4);
            $dont_care_lead_data = $dont_care_lead_data->whereDate('fptu_change_log.created_at', '>=', $from_date)
                ->where('fptu_lead.department_id', 4)
                ->whereDate('fptu_change_log.created_at', '<=', $to_date)
                ->where('fptu_change_log.field_name', 'status')
                ->where('fptu_change_log.value_after', 6);
            $transfered_lead_data = $transfered_lead_data->whereDate('fptu_change_log.created_at', '>=', $from_date)
                ->whereDate('fptu_change_log.created_at', '<=', $to_date)
                ->where('fptu_change_log.field_name', 'status')
                ->where('fptu_change_log.value_after', 7);
            $duplicate_lead_data = $duplicate_lead_data
                ->whereDate('fptu_change_log.created_at', '>=', $from_date)
                ->whereDate('fptu_change_log.created_at', '<=', $to_date)
                ->where('fptu_lead.department_id', 4)
                ->where('fptu_lead.call_status', 19);
            $mistargeted_lead_data = $mistargeted_lead_data
                ->whereDate('fptu_change_log.created_at', '>=', $from_date)
                ->whereDate('fptu_change_log.created_at', '<=', $to_date)
                ->where('fptu_lead.department_id', 4)
                ->where('fptu_lead.status', 8);
        }
        $tele_user_list = User::whereHas('roles', function ($q) {
            $q->where('name', 'telesales')
                ->orWhere('name', 'teleteamleader');
        })->get()->toArray();
        //var_dump($tele_user_list);
        $no_tele_user_list = User::whereDoesntHave('roles', function ($q) {
            $q->where('name', 'telesales')
                ->Where('name', 'teleteamleader');
        })->get();
        //var_dump($tele_user_list);

        $active_data = $active_data->where('fptu_lead.status', '5');
        $data = array(
            'summary' => array(
                'total' => 0,
                'quality' => 0,
                'active' => 0,
                'application' => 0,
                'modified' => 0,
                'called' => 0,
                'duration' => 0,
                'call_later' => 0,
                'wrong_number' => 0,
                'no_answer' => 0,
                'dont_care' => 0,
                'transfered' => 0,
                'duplicate' => 0,
                'mistargeted' => 0
            )
        );
        $called_list = array();
        $duration_list = array();
        $assignment_list = array();
        $quality_list = array();
        $active_list = array();
        $modified_list = array();
        $duplicate_list = array();
        $mistargeted_list = array();
        foreach ($assignment_data->get() as $assignment) {
            $assignment_list[$assignment->username] = $assignment;
            //$user_id = $assignment->
        }
        foreach ($active_data->get() as $active) {
            $active_list[$active->username] = $active;
        }
        foreach ($converted_data->get() as $converted) {
            $converted_list[$converted->username] = $converted;
        }
        foreach ($modified_lead_data->get() as $modified) {
            $modified_list[$modified->username] = $modified;
        }
        //var_dump($called_lead_data);exit;
        foreach ($called_lead_data as $ext => $called) {
            //var_dump($called);
            foreach ($tele_user_list as $tele) {
                //var_dump($called);
                if ($tele['extension'] == $ext) {
                    $call = new \stdClass();
                    $call->username = $tele['username'];
                    $call->count = $called->calls;
                    $duration = new \stdClass();
                    $duration->username = $tele['username'];
                    $duration->count = $called->duration;
                    $called_list[$tele['username']] = $call;
                    $duration_list[$tele['username']] = $duration;
                }
            }
        }
        foreach ($call_later_lead_data->get() as $called) {
            $call_later_list[$called->username] = $called;
        }
        foreach ($wrong_number_lead_data->get() as $called) {
            $wrong_number_list[$called->username] = $called;
        }
        foreach ($no_answer_lead_data->get() as $called) {
            $no_answer_list[$called->username] = $called;
        }
        foreach ($dont_care_lead_data->get() as $called) {
            $dont_care_list[$called->username] = $called;
        }
        foreach ($transfered_lead_data->get() as $called) {
            $transfered_list[$called->username] = $called;
        }
        foreach ($duplicate_lead_data->get() as $duplicate) {
            $duplicate_list[$duplicate->username] = $duplicate;
        }
        foreach ($mistargeted_lead_data->get() as $duplicate) {
            $mistargeted_list[$duplicate->username] = $duplicate;
        }
        $user_group_list = array();
        foreach ($user_list as $user) {
            $user_name = $user->username;
            $user_group_list[$user_name] = $user->group_name;
            if (!isset($data[$user_name])) {
                $data[$user_name] = array(
                    'total' => 0,
                    'quality' => 0,
                    'active' => 0,
                    'application' => 0,
                    'modified' => 0,
                    'called' => 0,
                    'duration' => 0,
                    'call_later' => 0,
                    'wrong_number' => 0,
                    'no_answer' => 0,
                    'dont_care' => 0,
                    'transfered' => 0,
                    'duplicate' => 0,
                    'mistargeted' => 0
                );
            }
            if (isset($assignment_list[$user_name]) && $assignment_list[$user_name]->count) {
                $data[$user_name]['total'] = $assignment_list[$user_name]->count;
                $data['summary']['total'] += $data[$user_name]['total'];
            }
            if (isset($active_list[$user_name]) && $active_list[$user_name]->count) {
                $data[$user_name]['active'] = $active_list[$user_name]->count;
                $data['summary']['active'] += $data[$user_name]['active'];
            }
            if (isset($converted_list[$user_name]) && $converted_list[$user_name]->count) {
                $data[$user_name]['application'] = $converted_list[$user_name]->count;
                $data['summary']['application'] += $data[$user_name]['application'];
            }
            if (isset($modified_list[$user_name]) && $modified_list[$user_name]->count) {
                $data[$user_name]['modified'] = $modified_list[$user_name]->count;
                $data['summary']['modified'] += $data[$user_name]['modified'];
            }
            if (isset($called_list[$user_name]) && $called_list[$user_name]->count) {
                $data[$user_name]['called'] = $called_list[$user_name]->count;
                $data['summary']['called'] += $data[$user_name]['called'];
            }
            if (isset($duration_list[$user_name]) && $duration_list[$user_name]->count) {
                $data[$user_name]['duration'] = $duration_list[$user_name]->count;
                $data['summary']['duration'] += $data[$user_name]['duration'];
            }
            if (isset($call_later_list[$user_name]) && $call_later_list[$user_name]->count) {
                $data[$user_name]['call_later'] = $call_later_list[$user_name]->count;
                $data['summary']['call_later'] += $data[$user_name]['call_later'];
            }
            if (isset($wrong_number_list[$user_name]) && $wrong_number_list[$user_name]->count) {
                $data[$user_name]['wrong_number'] = $wrong_number_list[$user_name]->count;
                $data['summary']['wrong_number'] += $data[$user_name]['wrong_number'];
            }
            if (isset($no_answer_list[$user_name]) && $no_answer_list[$user_name]->count) {
                $data[$user_name]['no_answer'] = $no_answer_list[$user_name]->count;
                $data['summary']['no_answer'] += $data[$user_name]['no_answer'];
            }
            if (isset($dont_care_list[$user_name]) && $dont_care_list[$user_name]->count) {
                $data[$user_name]['dont_care'] = $dont_care_list[$user_name]->count;
                $data['summary']['dont_care'] += $data[$user_name]['dont_care'];
            }
            if (isset($transfered_list[$user_name]) && $transfered_list[$user_name]->count) {
                $data[$user_name]['transfered'] = $transfered_list[$user_name]->count;
                $data['summary']['transfered'] += $data[$user_name]['transfered'];
            }
            if (isset($duplicate_list[$user_name]) && $duplicate_list[$user_name]->count) {
                $data[$user_name]['duplicate'] = $duplicate_list[$user_name]->count;
                $data['summary']['duplicate'] += $data[$user_name]['duplicate'];
            }
            if (isset($mistargeted_list[$user_name]) && $mistargeted_list[$user_name]->count) {
                $data[$user_name]['mistargeted'] = $mistargeted_list[$user_name]->count;
                $data['summary']['mistargeted'] += $data[$user_name]['mistargeted'];
            }
            $data[$user_name]['quality'] = $data[$user_name]['total'] - $data[$user_name]['wrong_number'] - $data[$user_name]['duplicate'] - $data[$user_name]['transfered'] - $data[$user_name]['mistargeted'];
            $data['summary']['quality'] += $data[$user_name]['quality'];
        }
        $request->flash();

        return view('report.lead', ['user_list' => $user_list, 'user_group_list' => $user_group_list, 'assignment_data' => $assignment_data->get(), 'active_data' => $active_data->get(), 'data' => $data]);
    }

    public function showDmrReport(Request $request)
    {

        $from_date = trim($request->get('from_date'));
        $to_date = trim($request->get('to_date'));
        if (!empty($from_date))
            $from_date = Utils::convertDate($from_date);
        else {
            $from_date = date('Y-m-d');
        }
        if (!empty($to_date))
            $to_date = Utils::convertDate($to_date);
        else {
            $to_date = date('Y-m-d');
        }

        $account_list = DB::table('fptu_account')
            ->join('fptu_account_assessment', 'fptu_account.id', '=', 'fptu_account_assessment.account_id')
            ->whereDate('fptu_account.created_at', '>=', $from_date)
            ->whereDate('fptu_account.created_at', '<=', $to_date)
            ->whereNull('deleted_at')
            ->get();

        $data = array();
        $data['ict']['total'] = 0;
        $data['biz']['total'] = 0;
        $data['cl']['total'] = 0;
        $data['aa']['total'] = 0;
        $data['total']['total'] = 0;
        $app_type_list = DB::table('fptu_app_type')->get();
        $major_list = DB::table('fptu_major')->get();
        foreach ($app_type_list as $app_type) {
            foreach ($major_list as $major) {
                $data[$app_type->id][$major->id] = 0;
                $data[$major->id]['total'] = 0;
            }
            $data[$app_type->id]['ict'] = 0;
            $data[$app_type->id]['biz'] = 0;
            $data[$app_type->id]['cl'] = 0;
            $data[$app_type->id]['aa'] = 0;
            $data[$app_type->id]['total'] = 0;
        }
        foreach ($account_list as $account) {
            $major = 0;
            if ($account->app_type == 2 || $account->app_type == 4 || $account->app_type == 5) {
                $major = $account->major_exam;
                $data[$account->app_type][$major]++;
            }
            if ($account->app_type == 1 || $account->app_type == 3) {
                $major = $account->major_no_exam;
                $data[$account->app_type][$major]++;
            }
            $data[$account->app_type]['total']++;
            $data[$major]['total']++;
            $ict_major = array(4, 5, 6);
            $biz_major = array(2, 3, 11);
            $cl_major = array(7, 8, 9, 13);
            $aa_major = array(1);
            if (in_array($major, $ict_major)) {
                $data[$account->app_type]['ict']++;
            }
            if (in_array($major, $biz_major)) {
                $data[$account->app_type]['biz']++;
            }
            if (in_array($major, $cl_major)) {
                $data[$account->app_type]['cl']++;
            }
            if (in_array($major, $aa_major)) {
                $data[$account->app_type]['aa']++;
            }
            $data['total']['total']++;
        }
        return view('report.dmr', ['data' => $data, 'from_date' => $from_date, 'to_date' => $to_date]);
    }

    public function test()
    {
        $options = array(
            'host' => env('PBX_HOST'),
            'scheme' => env('PBX_SCHEME'),
            'port' => env('PBX_PORT'),
            'username' => env('PBX_USERNAME'),
            'secret' => env('PBX_PASSWORD'),
            'connect_timeout' => env('PBX_CONNECT_TIMEOUT'),
            'read_timeout' => env('PBX_READ_TIMEOUT')
        );
        $client = new PamiClient($options);
        $client->open();
        $originateMsg = new OriginateAction('SIP/6789');
        $originateMsg->setContext('default');
        $originateMsg->setPriority(1);
        $originateMsg->setExtension('6789');
        $originateMsg->setApplication('Dial');
        $originateMsg->setData('SIP/TSTT_HN/3288000989663230');
        $originateMsg->setCallerId('02473001866');
        var_dump($client);
        var_dump($client->send($originateMsg));
        $client->close();
    }
}
