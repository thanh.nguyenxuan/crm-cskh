<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Lead;
use App\Account;

use PHPExcel;

use PHPExcel_IOFactory;

use App\User;
use App\Role;

use App\Importer;

use App\Source;

use App\Province;
use App\Department;
use App\Area;
use App\LeadStatus;
use App\Group;

use App\Account\Status as AccountStatus;

use SSP;

use PAMI\Client\Impl\ClientImpl as PamiClient;
use PAMI\Message\Event\EventMessage;
use PAMI\Message\Event\OriginateResponseEvent;
use PAMI\Listener\IEventListener;
use PAMI\Message\Action\OriginateAction;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\ChangeLog;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $province_list_data = Province::all();
        $province_list = array();
        foreach ($province_list_data as $province_data) {
            $province_list[$province_data->id] = $province_data->name;
        }
        $area_list_data = Area::all();
        $area_list = array();
        foreach ($area_list_data as $area_data) {
            $area_list[$area_data->id] = $area_data->name;
        }
        $department_list_data = Department::all();
        $department_list = array();
        foreach ($department_list_data as $department_data) {
            $department_list[$department_data->id] = $department_data->name;
        }
        if (Auth::user()->hasRole('admin'))
            $assignee_list_data = User::orderBy('username', 'ASC')->get();
        else
            $assignee_list_data = User::where('department_id', Auth::user()->department_id)->orderBy('username', 'ASC')->get();
        $assignee_list = array('nobody' => 'Chưa được chia');
        $activator_list = array();
        $creator_list = array();
        foreach ($assignee_list_data as $assignee_data) {
            $assignee_list[$assignee_data->id] = $assignee_data->username . ' - ' . $assignee_data->full_name;
            $activator_list[$assignee_data->id] = $assignee_data->username . ' - ' . $assignee_data->full_name;
            $creator_list[$assignee_data->id] = $assignee_data->username . ' - ' . $assignee_data->full_name;
        }
        if (Auth::check())
            return view('account.index', ['province_list' => $province_list, 'department_list' => $department_list, 'area_list' => $area_list, 'assignee_list' => $assignee_list, 'activator_list' => $activator_list, 'creator_list' => $creator_list]);
        else
            return redirect('/');
    }


    public function listAll()
    {
        $current_user_id = Auth::user()->id;
        $where_telesales = '';
        $where_assignee = '';
        if (isset($_REQUEST['gsearch']))
            $gsearch = $_REQUEST['gsearch'];
        $search_mode = Input::get('search_mode');
        $data = DB::table('fptu_account')
            ->leftJoin('fptu_lead', 'fptu_lead.id', '=', 'fptu_account.lead_id')
            ->leftJoin('fptu_province', 'fptu_account.province', '=', 'fptu_province.id')
            ->leftJoin('fptu_province as issue_province', 'fptu_account.issue_province', '=', 'issue_province.id')
            ->leftJoin('fptu_department', 'fptu_lead.department_id', '=', 'fptu_department.id')
            ->leftJoin('fptu_user as telesales', 'fptu_account.telesales', '=', 'telesales.id')
            ->leftJoin('fptu_user as creator', 'fptu_account.created_by', '=', 'creator.id')
            ->leftJoin('fptu_account_assessment', 'fptu_account.id', '=', 'fptu_account_assessment.account_id')
            ->leftJoin('fptu_assessment', 'fptu_account_assessment.assessment_id', '=', 'fptu_assessment.id')
            ->leftJoin('fptu_app_type', 'fptu_account_assessment.app_type', '=', 'fptu_app_type.id')
            ->leftJoin('fptu_method_normal', 'fptu_account_assessment.method_normal', '=', 'fptu_method_normal.id')
            ->leftJoin('fptu_method_scholarship', 'fptu_account_assessment.method_scholarship', '=', 'fptu_method_scholarship.id')
            ->leftJoin('fptu_school', 'fptu_account.school', '=', 'fptu_school.id')
            ->leftJoin('fptu_campus', 'fptu_account.campus', '=', 'fptu_campus.id')
            ->leftJoin('fptu_exam_place', 'fptu_account_assessment.exam_place', '=', 'fptu_exam_place.id')
            ->leftJoin('fptu_major as no_exam_major', 'fptu_account_assessment.major_no_exam', '=', 'no_exam_major.id')
            ->leftJoin('fptu_major as exam_major', 'fptu_account_assessment.major_exam', '=', 'exam_major.id')
            ->leftJoin('fptu_subject_group', 'fptu_account_assessment.subject_group', '=', 'fptu_subject_group.id')
            ->leftJoin('fptu_submission_channel', 'fptu_account.submission_channel', '=', 'fptu_submission_channel.id')
            ->leftJoin('fptu_academic_record', 'fptu_account.id', '=', 'fptu_academic_record.account_id')
        ;
        $assignee = Input::get('assignee');
        $nobody_found = false;
        if (!empty($assignee) && in_array('nobody', $assignee)) {
            $nobody_found = array_search('nobody', $assignee);
            //var_dump($nobody_found);
            if ($nobody_found !== FALSE) {
                unset($assignee[$nobody_found]);
                if (empty($assignee))
                    $where_assignee .= " AND a.user_id IS NULL ";
                else
                    $where_assignee .= " AND ((a.user_id IS NULL) OR ";
            }
        }
        if (!empty($assignee)) {
            $assignee_list_string = "'" . implode("','", $assignee) . "'";
            if ($nobody_found)
                $where_assignee .= " (a.user_id IN ($assignee_list_string)) ";
            else
                $where_assignee .= " AND a.user_id IN ($assignee_list_string) ";
        }
        if (Auth::user()->hasRole('telesales') && !Auth::user()->hasRole('teleleader') && !Auth::user()->hasRole('manager') && !Auth::user()->hasRole('admin'))
            $data = $data->join(DB::raw("(SELECT a.lead_id, GROUP_CONCAT(b.username) as `assignee_list` FROM `fptu_lead_assignment` a INNER JOIN fptu_user b ON a.user_id = b.id WHERE a.user_id = '$current_user_id' $where_assignee GROUP BY a.lead_id) as assignee"), 'assignee.lead_id', '=', 'fptu_lead.id');
        else {
            if (!empty($assignee)) {
                $data = $data->join(DB::raw("(SELECT a.lead_id, GROUP_CONCAT(b.username) as `assignee_list` FROM `fptu_lead_assignment` a INNER JOIN fptu_user b ON a.user_id = b.id WHERE 1=1 $where_assignee GROUP BY a.lead_id) as assignee"), 'assignee.lead_id', '=', 'fptu_lead.id');
            } else {
                $data = $data->leftJoin(DB::raw("(SELECT a.lead_id, GROUP_CONCAT(b.username) as `assignee_list` FROM `fptu_lead_assignment` a INNER JOIN fptu_user b ON a.user_id = b.id WHERE 1=1 $where_assignee GROUP BY a.lead_id) as assignee"), 'assignee.lead_id', '=', 'fptu_lead.id');
            }
        }
        $data = $data->select(
            'fptu_account.id as app_id',
            'fptu_account.*',
            'fptu_province.name as province',
            'fptu_department.name as department',
            'assignee.assignee_list',
            'creator.username as creator',
            'telesales.username as tele_username',
            'fptu_assessment.name as assessment_name',
            'fptu_app_type.name as app_type_name',
            'fptu_exam_place.name as exam_place_name',
            'fptu_method_normal.name as method_normal_name',
            'fptu_school.name as school_name',
            'fptu_province.name as province_name',
            'fptu_campus.code as campus_name',
            'fptu_method_scholarship.name as method_scholarship_name',
            'no_exam_major.name as major_no_exam_name',
            'exam_major.name as major_exam_name',
            'fptu_subject_group.name as subject_group_name',
            'issue_province.name as issue_province_name',
            'fptu_submission_channel.name as submission_channel_name',
            'fptu_academic_record.*',
            DB::raw("CONCAT_WS(',', fptu_lead.phone1, fptu_lead.phone2, fptu_lead.phone3 , fptu_lead.phone4) as phone"), 'assignee.assignee_list'
        );
        if (!Auth::user()->hasRole('admin')) {
            $department_id = Auth::user()->department_id;
            $data = $data->where('fptu_lead.department_id', '=', $department_id);
        }
        if ($search_mode == 'global_search' && !empty($gsearch)) {
            $data = $data
                ->where('fptu_lead.name', 'LIKE', "%$gsearch%")
                ->orWhere('fptu_lead.phone1', 'LIKE', "%$gsearch%")
                ->orWhere('fptu_lead.phone2', 'LIKE', "%$gsearch%")
                ->orWhere('fptu_lead.phone3', 'LIKE', "%$gsearch%")
                ->orWhere('fptu_lead.phone4', 'LIKE', "%$gsearch%")
                ->orWhere('fptu_lead.email', 'LIKE', "%$gsearch%")
                ->orWhere('fptu_lead.facebook', 'LIKE', "%$gsearch%");
        } elseif ($search_mode == 'advanced_search') {
            $name = Input::get('name');
            if (!empty($name)) {
                $data = $data->where('fptu_lead.name', 'LIKE', "%$name%");
            }
            $phone = Input::get('phone');
            if (!empty($phone)) {
                $data = $data
                    ->where('fptu_lead.phone1', 'LIKE', "%$phone%")
                    ->orWhere('fptu_lead.phone2', 'LIKE', "%$phone%")
                    ->orWhere('fptu_lead.phone3', 'LIKE', "%$phone%")
                    ->orWhere('fptu_lead.phone4', 'LIKE', "%$phone%");
            }
            $email = Input::get('email');
            if (!empty($email)) {
                $data = $data->where('fptu_lead.email', 'LIKE', "%$email%");
            }
            $source = Input::get('source');
            if (!empty($source)) {
                if (in_array('na', $source)) {
                    $na_index = array_search('na', $source);
                    if (FALSE !== $na_index) {
                        unset($source[$na_index]);
                    }
                    $data = $data->whereNull('fptu_lead.source')
                        ->orWhere('fptu_lead.source', '=', 0);
                    if (count($source) > 0)
                        $data = $data->orWhereIn('fptu_lead.source', $source);
                } else {
                    $data = $data->whereIn('fptu_lead.source', $source);
                }
            }
            $school = Input::get('school');
            if (!empty($school)) {
                $data = $data->where('fptu_lead.school', 'LIKE', "%$school%");
            }
            $province = Input::get('province');
            //var_dump($province);
            if (!empty($province)) {
                if (in_array('na', $province)) {
                    $na_index = array_search('na', $province);
                    if (FALSE !== $na_index) {
                        unset($province[$na_index]);
                    }
                    $data = $data->whereNull('fptu_lead.province_id')
                        ->orWhere('fptu_lead.province_id', '=', 0);
                    if (count($province) > 0)
                        $data = $data->orWhereIn('fptu_lead.province_id', $province);
                } else {
                    $data = $data->whereIn('fptu_lead.province_id', $province);
                }
            }
            $department = Input::get('department');
            if (!empty($department)) {
                $data = $data->where('fptu_lead.department_id', 'LIKE', "%$department%");
            }
            $area = Input::get('area');
            if (!empty($area)) {
                if (in_array('na', $area)) {
                    $na_index = array_search('na', $area);
                    if (FALSE !== $na_index) {
                        unset($area[$na_index]);
                    }
                    $data = $data->whereNull('fptu_lead.area_id')
                        ->orWhere('fptu_lead.area_id', '=', 0);
                    if (count($area) > 0)
                        $data = $data->orWhereIn('fptu_lead.area_id', $area);
                } else {
                    $data = $data->whereIn('fptu_lead.area_id', $area);
                }
            }
            $import_count = Input::get('import_count');
            $import_count_operator = Input::get('import_count_operator');
            if (!empty($import_count) && !empty($import_count_operator)) {
                $operator = '';
                switch ($import_count_operator) {
                    case 'ge':
                        $operator = '>=';
                        break;
                    case 'gt':
                        $operator = '>';
                        break;
                    case 'le':
                        $operator = '<=';
                        break;
                    case 'lt':
                        $operator = '<';
                        break;
                    default:
                        $operator = '>=';
                        break;
                }
                $data = $data->where('fptu_lead.import_count', $operator, $import_count);
            }

            $notes = Input::get('notes');
            if (!empty($notes)) {
                $data = $data->where('fptu_lead.notes', 'LIKE', "%$notes%");
            }
            $activator = Input::get('activator');
            if (!empty($activator)) {
                $data = $data->whereIn('fptu_lead.activator', $activator);
            }
            $creator = Input::get('creator');
            if (!empty($creator)) {
                $data = $data->whereIn('fptu_lead.created_by', $creator);
            }
            $start_date = Input::get('start_date');
            if (!empty($start_date)) {
                $start_date = Carbon::createFromFormat('d/m/Y', $start_date)->format('Y-m-d 00:00:00');
                $data = $data->where('fptu_lead.created_at', '>=', "$start_date");
            }
            $end_date = Input::get('end_date');
            if (!empty($end_date)) {
                $end_date = Carbon::createFromFormat('d/m/Y', $end_date)->format('Y-m-d 23:59:59');
                $data = $data->where('fptu_lead.created_at', '<=', "$end_date");
            }
            $active_from = Input::get('active_from');
            if (!empty($active_from)) {
                $active_from = Carbon::createFromFormat('d/m/Y', $active_from)->format('Y-m-d 00:00:00');
                $data = $data->where('fptu_lead.activated_at', '>=', "$active_from");
            }
            $active_to = Input::get('active_to');
            if (!empty($active_to)) {
                $active_to = Carbon::createFromFormat('d/m/Y', $active_to)->format('Y-m-d 23:59:59');
                $data = $data->where('fptu_lead.activated_at', '<=', "$active_to");
            }
        }
        //var_dump($data->toSql());

        $countTotal = $data->count();
        $data = $data->whereDate('fptu_account.created_at', '<=', '2018-04-10');
        $data = $data->orderBy('fptu_account.created_at', 'DESC');

        //var_dump($data->toSql());
        $data = array(
            'data' => $data->get(),
            'recordsTotal' => $countTotal,
            'recordsFiltered' => $countTotal,
        );
        echo json_encode($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$account->birthdate = Carbon::createFromFormat('Y-m-d', $account->birthdate)->format('d/m/Y');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
