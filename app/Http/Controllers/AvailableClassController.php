<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AvailableClass;

class AvailableClassController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $class_list = array();
        $class_list = AvailableClass::all();
        return view('class.index', ['class_list' => $class_list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('class.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = 0;
        $return_message = '';
        $found = AvailableClass::where('name', $request->name)
            ->count();
        $error = false;
        if ($found) {
            $return_message = '<div>Tên lớp đã tồn tại.</div>';
            $error = true;
        } else {
            $return_message = 'Tạo lớp mới thành công!';
            $success = 1;
            $class = new AvailableClass();
            $class->name = trim($request->name);
            $saved = $class->save();
            if (!$saved) {
                $return_message = 'Đã có lỗi trong quá trình lưu dữ liệu!';
                $success = 0;
                $error = true;
            }
        }
        $request->flash();
        if ($error)
            return view('class.create', ['success' => $success, 'message' => $return_message]);
        return view('class.edit', ['class' => $class, 'success' => $success, 'message' => $return_message]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $class = AvailableClass::find($id);
        return view('class.edit', ['class' => $class]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $success = 0;
        $class = AvailableClass::find($id);
        $error = false;
        $return_message = '';

        if (!$error) {
            $update = array(
                'name' => $request->name
            );

            $updated = $class->update($update);
            if ($updated) {
                $return_message = '';
                $success = 1;
            } else {
                $return_message = '<div>Đã có lỗi trong quá trình cập nhật dữ liệu.</div>';
                $success = 0;
            }
        }
        //$user = User::find($id);
        return view('class.edit', ['success' => $success, 'class' => $class, 'message' => $return_message]);
    }

    public function toggle($id)
    {
        $success = 0;
        $class = AvailableClass::find($id);
        $error = false;
        $return_message = '';
        $update = array();
        if ($class->hide == 1) {
            $class->hide = 0;
        } else {
            $class->hide = 1;
        }
        $updated = $class->save();
        if ($updated) {
            $return_message = 'Đã cập nhật trạng thái lớp thành công!';
            $success = 1;
        } else {
            $return_message = '<div>Đã có lỗi trong quá trình cập nhật dữ liệu.</div>';
            $success = 0;
        }
        $class_list = array();
        $class_list = AvailableClass::all();
        return view('class.index', ['success' => $success, 'message' => $return_message, 'class_list' => $class_list]);
    }
}