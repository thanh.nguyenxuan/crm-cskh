<?php

namespace App\Http\Controllers;

use App\RGroup;
use App\RGroupDepartment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Utils;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class RGroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $model = new RGroup();

        return view('rGroup.index', ['model' => $model]);
    }


    public function create()
    {
        return view('rGroup.create');
    }


    public function store(Request $request)
    {
        $success = false;
        $validate = true;
        $message = '';
        $m_arr = array();

        if(empty($request->name)){
            $m_arr[] = 'Tên nhóm rỗng';
            $validate = false;
        }
        if(empty($request->departments)){
            $m_arr[] = 'Cơ sở rỗng';
            $validate = false;
        }
        if(!empty($request->name)){
            $exist = RGroup::where('name', $request->name)->first();
            if($exist){
                $m_arr[] = 'Tên nhóm đã tồn tại';
                $validate = false;
            }
        }
        if($validate){
            $model = new RGroup();
            $model->name = $request->name;
            $model->status = 1;
            $model->created_by = Auth::user()->id;
            if($model->save()){
                $success = TRUE;
                $message = 'Tạo nhóm thành công';

                $selected_departments = array();
                foreach ($request->departments as $department_id){
                    $selected_departments[] = $department_id;
                    $group_department = new RGroupDepartment();
                    $group_department->group_id = $model->id;
                    $group_department->department_id = $department_id;
                    $group_department->created_by = Auth::user()->id;
                    $group_department->save();
                }

                return view('rGroup.edit', [
                    'success' => $success,
                    'message' => $message,
                    'model' => $model,
                    'selected_departments' => $selected_departments
                ]);
            }
        }

        if(!empty($m_arr)){
            $message = implode('<br/>', $m_arr);
        }
        $request->flash();
        return view('rGroup.create', ['success' => $success, 'message' => $message]);
    }


    public function edit($id)
    {
        $model = RGroup::find($id);
        $selected_departments = RGroupDepartment::where('group_id', $model->id)->get()->pluck('department_id')->toArray();

        return view('rGroup.edit', ['model' => $model, 'selected_departments' => $selected_departments]);
    }


    public function update(Request $request, $id)
    {
        $model = RGroup::find($id);
        $selected_departments = RGroupDepartment::where('group_id', $model->id)->get()->pluck('department_id')->toArray();

        $success = false;
        $validate = true;
        $message = '';
        $m_arr = array();

        if(empty($request->name)){
            $m_arr[] = 'Tên nhóm rỗng';
            $validate = false;
        }
        if(empty($request->departments)){
            $m_arr[] = 'Cơ sở rỗng';
            $validate = false;
        }
        if(!empty($request->name)){
            $exist = RGroup::where('id', '!=', $model->id)->where('name', $request->name)->first();
            if($exist){
                $m_arr[] = 'Tên nhóm đã tồn tại';
                $validate = false;
            }
        }

        if($validate){
            $model->name = $request->name;
            if($model->save()){
                $success = TRUE;
                $message = 'Cập nhật nhóm thành công';

                RGroupDepartment::where('group_id', $model->id)->delete();
                $selected_departments = array();
                foreach ($request->departments as $department_id){
                    $selected_departments[] = $department_id;
                    $group_department = new RGroupDepartment();
                    $group_department->group_id = $model->id;
                    $group_department->department_id = $department_id;
                    $group_department->created_by = Auth::user()->id;
                    $group_department->save();
                }

            }
        }

        return view('rGroup.edit', [
            'success' => $success,
            'message' => $message,
            'model' => $model,
            'selected_departments' => $selected_departments
        ]);
    }


    public function toggle($id)
    {
        $model = RGroup::find($id);
        if($model){
            if ($model->status == 1) {
                $model->status = 0;
            } else {
                $model->status = 1;
            }
            $model->save();
        }
        return redirect('rGroup');
    }

}