<?php

namespace App\Http\Controllers;

use App\AdminControlModel\AdminControlSyncLeadERP;
use App\Lead\CallLog;
use App\Report\Cdr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class AdminControl extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        echo 'AdminControl';
    }


    public function syncLeadERP(Request $request)
    {
        set_time_limit(0);
        ini_set('memory_limit', '-1');
        $model = new AdminControlSyncLeadERP();

        if ($request->isMethod('post')) {
            if(!empty($request->file('filepath'))){
                $model->filepath = $request->file('filepath')->path();
                $result = $model->sync();
                dd($result);
            }
        }

        return view('admin_control.sync_lead_erp', [
            'model' => $model
        ]);

    }

    public function dailySync(Request $request)
    {
        CallLog::dailySync();
    }

    public function getCallLogPBX(Request $request)
    {
        if(!empty($request->call_session)
            && !empty($request->lead_id)
            && !empty($request->user_id)
            && !empty($request->date)
        ) {
            $start_date = $end_date =  date('Y-m-d', strtotime($request->date));
            $model = new Cdr();
            $data = $model->getReportCdr($start_date, $end_date, array('call_session' => $request->call_session));

            $callLog = new CallLog();
            $callLog->lead_id = $request->lead_id;
            $callLog->direction = CallLog::DIRECTION_OUT;
            $callLog->status = Cdr::convertStatusForCallLog($data['data'][0]->status);
            $callLog->notes = $request->note;
            $callLog->created_at = $data['data'][0]->date_time;
            $callLog->created_by = $request->user_id;
            $callLog->data = json_encode($data['data'][0]);
            $callLog->save();

            dd($callLog->id);
        }


    }

}