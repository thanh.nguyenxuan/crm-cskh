<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\DB;


class AsteriskController extends Controller
{
    public function getNameFromNumber($number)
    {
        $lead_name = '';
        $return = '';
        $department_name = '';
        $activator_username = '';
        $assignee = '';
        $lead = DB::table('fptu_lead')
            ->where('phone1', $number)->orWhere('phone2', $number)->orWhere('phone3', $number)->orWhere('phone4', $number)->first();
        if (!empty($lead)) {
            $lead_name = $lead->name;
        } else {
            $lead_name = 'NEW-' . $number;
        }
        $return .= $lead_name;
        return $return;
    }

    public function dial($phone, $trunk_id = '')
    {
        $data = '';
        $trunk = null;
        if (empty($trunk_id)) {
            $default_trunk = Auth::user()->default_trunk;
            $trunk = PbxTrunk::find($default_trunk);
        } else {
            $trunk = PbxTrunk::find($trunk_id);
        }
        $ext = Auth::user()->extension;
        if (!empty($trunk) && !empty($ext) && !empty($phone)) {
            $tech = strtoupper($trunk->tech);
            $client = $this->connect();
            $originateMsg = new OriginateAction($tech . '/' . $ext);
            $originateMsg->setContext('default');
            $originateMsg->setPriority(1);
            $originateMsg->setExtension($ext);
            $originateMsg->setApplication('Dial');
            $originateMsg->setData($tech . '/' . $trunk->name . '/' . $trunk->prefix . $phone);
            //var_dump($tech . '/' . $trunk->name . '/' . $trunk->prefix . $phone);
            //var_dump(Auth::user()->toArray());
            $originateMsg->setCallerId($trunk->caller_id);
            //var_dump($client);
            $client->send($originateMsg);
            $client->close();
        }
    }
    
    public function getExtFromNumber($number)
    {

        $activator_username = '';
        $lead = DB::table('fptu_lead')
            ->where('phone1', $number)->orWhere('phone2', $number)->orWhere('phone3', $number)->orWhere('phone4', $number)->first();
        if (!empty($lead)) {
            $lead_name = $lead->name;
            if (!empty($lead->activator)) {
                $activator = User::find($lead->activator);
                if (!empty($activator) && !empty($activator->extension)) {
                    $activator_ext = $activator->extension;
                    return $activator_ext;
                }
            } else {
                $assignment_list = DB::table('fptu_lead_assignment')
                    ->join('fptu_user', 'fptu_user.id', '=', 'fptu_lead_assignment.user_id')
                    ->where('fptu_lead_assignment.lead_id', $lead->id)->whereNull('fptu_lead_assignment.deleted_at')->get();
                if (!empty($assignment_list)) {
                    if (count($assignment_list) == 1) {
                        $assignee = $assignment_list[0];
                        if (!empty($assignee) && !empty($assignee->extension)) {
                            if (!empty($assignee->sifu)) {
                                $sifu = User::find($assignee->sifu);
                                if (!empty($sifu) && !empty($sifu->extension)) {
                                    return $sifu->extension;
                                } else
                                    return $assignee->extension;
                            }
                            return $assignee->extension;
                        }
                    } elseif (count($assignment_list) == 2) {
                        if (!empty($assignment_list[0]->extension) && !empty($assignment_list[1]->extension)) {
                            if ($assignment_list[0]->sifu == $assignment_list[1]->id) {
                                return $assignment_list[1]->extension;
                            } elseif ($assignment_list[1]->sifu == $assignment_list[0]->id) {
                                return $assignment_list[0]->extension;
                            }
                        }
                    }
                }
            }
        }
        return "";
    }
}
