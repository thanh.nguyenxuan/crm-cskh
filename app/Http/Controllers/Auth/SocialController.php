<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Config;

use Illuminate\Support\Facades\Input;

use Socialite;

use App\Http\Controllers\Controller;

use App\User;

use Illuminate\Support\Facades\Auth;

class SocialController extends Controller
{

    public function getSocialRedirect($provider)
    {

        $providerKey = Config::get('services.' . $provider);

        if (empty($providerKey)) {

            return view('/')
                ->with('message', 'Không tìm thấy provider.');

        }

        return Socialite::driver($provider)->redirect();

    }

    public function getSocialHandle($provider)
    {
        if (Input::get('denied') != '') {

            return redirect()->to('/login')
                ->with('message', 'Bạn không thể đăng nhập nếu từ chối chia sẻ dữ liệu tài khoản.');

        }

        $user = Socialite::driver($provider)->user();

        $socialUser = null;

        $socialUser = User::where('email', '=', $user->email)->where('active', 1)->first();

        if (!empty($socialUser)) {
            Auth::login($socialUser, true);
            return redirect()->to('/');
        }
        return redirect()->to('login')->with('message', 'Không tìm thấy tài khoản tương ứng với email đăng nhập của bạn.');
    }
}
