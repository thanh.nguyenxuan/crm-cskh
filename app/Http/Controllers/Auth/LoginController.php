<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\UtilsController;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function username()
    {
        return 'username';
    }

    protected function redirectTo()
    {
        if (Auth::user()->password_expired)
            $this->redirectTo = '/user/resetpassword';
        return $this->redirectTo;
    }

//    public function setOnline($user_id, $plus_extra_minutes = true)
//    {
//        //$online = \App\Online::where('user_id', $user_id)->pluck('minutes')->first();
//        $last_online = \App\Online::where('user_id', $user_id)->pluck('last_online')->first();
//        $minutes = $this->getOnline($user_id);
//        if ($plus_extra_minutes) {
//            $minutes += (ceil(env('ONLINE_CHECK_INTERVAL') / 60000));
//            $last_online = strtotime($last_online) + env('ONLINE_CHECK_INTERVAL') / 1000;
//        } else {
//            $last_online = date('Y-m-d H:i:s');
//        }
//        \App\Online::where('user_id', $user_id)->update(['minutes' => $minutes, 'last_online' => $last_online]);
//        return $last_online;
//    }

    public function postLogin(Request $request)
    {
        //var_dump($auth);
        //$this->setOnline(, false);

    }

    protected function authenticated(Request $request, $user)
    {
        Auth::user()->getManageDepartments(TRUE);
    }

}
