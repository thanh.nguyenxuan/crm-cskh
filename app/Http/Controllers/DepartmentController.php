<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\Province;

class DepartmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $department_list = array();
        $department_list = Department::all();
        $province_list = Province::all()->pluck('name', 'id');
        return view('department.index', ['department_list' => $department_list, 'province_list' => $province_list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $province_list = Province::all()->pluck('name', 'id');
        return view('department.create', ['province_list' => $province_list]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = 0;
        $return_message = '';
        $province_list = Province::all()->pluck('name', 'id');
        $found = Department::where('name', $request->name)
            ->count();
        $error = false;
        if ($found) {
            $return_message = 'Tên nguồn thông tin đã tồn tại.';
            $error = true;
        } else {
            $return_message = 'Tạo nguồn thông tin mới thành công!';
            $success = 1;
            $department = new Department();
            $department->name = trim($request->name);
            $department->description = trim($request->description);
            $department->province = $request->province;
            $saved = $department->save();
            if (!$saved) {
                $return_message = 'Đã có lỗi trong quá trình lưu dữ liệu!';
                $success = 0;
                $error = true;
            }
        }
        $request->flash();
        if ($error)
            return view('department.create', ['success' => $success, 'message' => $return_message]);
        return view('department.edit', ['department' => $department, 'province_list' => $province_list, 'success' => $success, 'message' => $return_message]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::find($id);
        $province_list = Province::all()->pluck('name', 'id');
        return view('department.edit', ['department' => $department, 'province_list' => $province_list]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $success = 0;
        $department = Department::find($id);
        $error = false;
        $return_message = '';
        $department->name = trim($request->name);
        $department->description = trim($request->description);
        $department->province = $request->province;
        $province_list = Province::all()->pluck('name', 'id');
        $updated = $department->save();
        if ($updated) {
            $return_message = 'Đã cập nhật dữ liệu thành công!';
            $success = 1;
        } else {
            $return_message = '<div>Đã có lỗi trong quá trình cập nhật dữ liệu.</div>';
            $success = 0;
        }
        return view('department.edit', ['success' => $success, 'department' => $department, 'province_list' => $province_list, 'message' => $return_message]);
    }

    public function toggle($id)
    {
        $success = 0;
        $department = Department::find($id);
        $error = false;
        $return_message = '';
        $update = array();
        if ($department->hide == 1) {
            $department->hide = 0;
        } else {
            $department->hide = 1;
        }
        $updated = $department->save();
        if ($updated) {
            $return_message = 'Đã cập nhật trạng thái lớp thành công!';
            $success = 1;
        } else {
            $return_message = 'Đã có lỗi trong quá trình cập nhật dữ liệu.';
            $success = 0;
        }
//        $department_list = array();
//        $department_list = Department::all();
//        $province_list = Province::all()->pluck('name', 'id');
        return redirect('department');
//        return view('department.index', ['success' => $success, 'message' => $return_message, 'department_list' => $department_list, 'province_list' => $province_list]);
    }
}
