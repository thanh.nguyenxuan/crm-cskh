<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Group;
use App\User;
use App\Department;
use App\UserGroup;
use Illuminate\Support\Facades\Auth;

class GroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $group_list = array();
        if (Auth::user()->hasRole('admin'))
            $group_list = Group::all();
        else {
            if (Auth::user()->hasRole('teleteamleader') || Auth::user()->hasRole('promoterteamleader'))
                $group_list = Group::where('team_leader', Auth::id())->get();
            if (Auth::user()->hasRole('manager'))
                $group_list = Group::where('department', Auth::user()->department_id)->get();
        }
        foreach ($group_list as $group) {
            if (!empty($group->team_leader)) {
                $team_leader = User::find($group->team_leader);
                if (!empty($team_leader))
                    $group->team_leader = $team_leader->username;
            }
            if (!empty($group->department))
                $group->department = Department::find($group->department)->name;
            if ($group->type == 1)
                $group->type = 'Telesales';
            if ($group->type == 2)
                $group->type = 'Promoter';
            $member_list = UserGroup::where('group_id', $group->id)->get()->pluck('user_id')->toArray();
            $members = array();
            foreach ($member_list as $member_id) {
                $members[] = User::find($member_id);
            }
            $group->members = $members;
        }
        return view('group.index', ['group_list' => $group_list]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_list = User::orderBy('username', 'ASC')->get()->pluck('username', 'id')->toArray();
        $group_type_list = array(1 => 'Telesales', 2 => 'Promoter');
        $department_list = Department::all()->pluck('name', 'id')->toArray();
        return view('group.create', ['department_list' => $department_list, 'user_list' => $user_list, 'group_type_list' => $group_type_list]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = '';
        $success = 0;
        $group_count = Group::where(['name' => $request->name])->count();
        $user_list = User::all()->pluck('username', 'id')->toArray();
        $group_type_list = array(1 => 'Telesales', 2 => 'Promoter');
        $department_list = Department::all()->pluck('name', 'id')->toArray();
        if ($group_count > 0) {
            $message = 'Tên nhóm đã tồn tại';
        } else {
            $group = new Group;
            $group->name = $request->name;
            $group->type = $request->type;
            $group->team_leader = $request->team_leader;
            $group->department = $request->department;
            $group->save();
            foreach ($request->member_list as $member_id) {
                UserGroup::firstOrCreate(['user_id' => $member_id, 'group_id' => $group->id]);
            }
            $success = 1;
        }
        return view('group.create', ['success' => $success, 'message' => $message, 'department_list' => $department_list, 'user_list' => $user_list, 'group_type_list' => $group_type_list]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $group = Group::find($id);
        $user_list = User::all()->pluck('username', 'id')->toArray();
        $group_type_list = array(1 => 'Telesales', 2 => 'Promoter');
        $department_list = Department::all()->pluck('name', 'id')->toArray();
        $group->members = UserGroup::where('group_id', $id)->get()->pluck('user_id')->toArray();
        return view('group.edit', ['group' => $group, 'department_list' => $department_list, 'user_list' => $user_list, 'group_type_list' => $group_type_list]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $message = '';
        $success = 0;
        $group_count = Group::where(['name' => $request->name])->count();
        $user_list = User::all()->pluck('username', 'id')->toArray();
        $group_type_list = array(1 => 'Telesales', 2 => 'Promoter');
        $department_list = Department::all()->pluck('name', 'id')->toArray();
        $group = Group::find($id);
        $group->name = $request->name;
        $group->type = $request->type;
        $group->team_leader = $request->team_leader;
        $group->department = $request->department;
        $group->save();
        //var_dump($request->member_list);
        $member_rel_list = UserGroup::where('group_id', $id)->get()->pluck('id')->toArray();
        UserGroup::destroy($member_rel_list);
        if (!empty($request->member_list))
            foreach ($request->member_list as $member_id) {
                UserGroup::create(['user_id' => $member_id, 'group_id' => $id]);
            }
        $group->members = $request->member_list;
        $success = 1;
        return view('group.edit', ['group' => $group, 'success' => $success, 'message' => $message, 'department_list' => $department_list, 'user_list' => $user_list, 'group_type_list' => $group_type_list]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
