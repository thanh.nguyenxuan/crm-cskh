<?php
class Ami {
	public function call() {
		$options = array(
		'host' => '210.245.80.34',
		'scheme' => 'udp://',
		'port' => 5060,
		'username' => '1002',
		'secret' => 'hang123',
		'connect_timeout' => 10,
		'read_timeout' => 10
		);
	$client = new PamiClient($options);
	$client->open();

		// Registering a closure
	//use PAMI\Message\Event\OriginateResponseEvent;
	$client->registerEventListener(
	    function (OriginateResponseEvent $event) {
			$reason = $event->getReason();
	        switch ($reason) {
				var_dump($reason);
	        }
	    },
	    function (EventMessage $event) {
	        return instanceof OriginateResponseEvent;
	    }
	);
	// Needed to process and deliver events and responses
	$running = true;
	while($running) {
	    $client->process();  
	    usleep(1000);  
	}

	$client->close();
	}
}