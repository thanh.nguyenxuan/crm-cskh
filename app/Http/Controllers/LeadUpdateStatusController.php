<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Lead;
use App\LeadLogStatus;
use App\LeadStatus;
use App\ManualLeadAssigner;
use Illuminate\Http\Request;

class LeadUpdateStatusController extends Controller{

    public function index(Request $request)
    {
        $return = array(
            'success' => TRUE,
            'message' => '',
            'data' => array(),
        );
        if(!empty($request->lead_id) && !empty($request->status))
        {
            $accept_status = array(
                LeadStatus::STATUS_NEW,
                LeadStatus::STATUS_WAITLIST,
                LeadStatus::STATUS_NEW_SALE,
                LeadStatus::STATUS_ENROLL,
            );
            if(!in_array($request->status, $accept_status)){
                $return['success'] = FALSE;
                $return['message'] = 'Invalid status';
            }
            $lead = Lead::find($request->lead_id);
            if(!$lead){
                $return['success'] = FALSE;
                $return['message'] = 'Lead not found';
            }
            if($return['success']){
                if($lead->status != $request->status){
                    $update = FALSE;
                    $old_status = $lead->status;
                    $now = date('Y-m-d H:i:s');

                    switch ($request->status){
                       /* case LeadStatus::STATUS_NEW:
                            $update = TRUE;
                            $lead_log_old_status = LeadLogStatus::where('lead_id', $lead->id)->where('type',LeadLogStatus::TYPE_LEAD_STATUS)->whereNotIn('status_old', $accept_status)->orderBy('id', 'DESC')->first();
                            if($lead_log_old_status && !empty($lead_log_old_status->status_old)){
                                $lead->status = $lead_log_old_status->status_old;
                            }else{
                                $lead->status = LeadStatus::STATUS_NEW;
                            }
                            break;*/
                        case LeadStatus::STATUS_WAITLIST:
                            if(!in_array($lead->status, [LeadStatus::STATUS_NEW_SALE, LeadStatus::STATUS_ENROLL])){
                                $update = TRUE;
                                $lead->status = LeadStatus::STATUS_WAITLIST;
                                if(empty($lead->activated_at)){
                                    $update = TRUE;
                                    $lead->activated_at = $now;
                                    $lead->activated = 1;
                                    $assignment = ManualLeadAssigner::where('lead_id', $lead->id)->whereNull('deleted_at')->orderBy('id', 'DESC')->first();
                                    if($assignment){
                                        $lead->activator = $assignment->user_id;
                                    }
                                }
                            }else{
                                if($lead->status == LeadStatus::STATUS_NEW_SALE){
                                    $update = TRUE;
                                    $lead->status = LeadStatus::STATUS_WAITLIST;
                                }
                            }
                            break;
                        case LeadStatus::STATUS_NEW_SALE:
                            $update = TRUE;
                            if($lead->status != LeadStatus::STATUS_ENROLL){
                                $lead->status = LeadStatus::STATUS_NEW_SALE;
                                if(empty($lead->new_sale_at)){
                                    $lead->new_sale_at = $now;
                                    $lead->new_sale = 1;
                                }
                            }else{
                                $lead->status = LeadStatus::STATUS_NEW_SALE;
                            }
                            break;
                        case LeadStatus::STATUS_ENROLL:
                            $update = TRUE;
                            $lead->status = LeadStatus::STATUS_ENROLL;
                            if(empty($lead->enrolled_at)){
                                $lead->enrolled_at = $now;
                                $lead->enrolled = 1;
                            }
                            break;
                    }

                    if($request->status == LeadStatus::STATUS_WAITLIST) {
                        if(!in_array($lead->status, $accept_status)){
                            if(empty($lead->activated_at)){
                                $update = TRUE;
                                $lead->status = LeadStatus::STATUS_WAITLIST;
                                $lead->activated_at = $now;
                                $lead->activated = 1;
                            }
                        }else{
                            if($lead->status == LeadStatus::STATUS_NEW_SALE){
                                $update = TRUE;
                                $lead->status = LeadStatus::STATUS_WAITLIST;
                            }
                        }
                    }
                    if($request->status == LeadStatus::STATUS_NEW_SALE) {
                        if(!in_array($lead->status, $accept_status) || $lead->status == LeadStatus::STATUS_WAITLIST){
                            if(empty($lead->new_sale_at)){
                                $update = TRUE;
                                $lead->status = LeadStatus::STATUS_NEW_SALE;
                                $lead->new_sale_at = $now;
                                $lead->new_sale = 1;
                            }
                        }
                    }

                    if($update){
                        if($lead->save()){
                            $lead_log_status = new LeadLogStatus();
                            $lead_log_status->lead_id = $lead->id;
                            $lead_log_status->status_old = $old_status;
                            $lead_log_status->status_new = $lead->status;
                            $lead_log_status->created_at = $now;
                            $lead_log_status->created_by = 4; //crm
                            $lead_log_status->save();
                        }else{
                            $return['success'] = FALSE;
                            $return['message'] = 'Save fail';
                        }
                    }
                }
            }
        }
        echo json_encode($return);
    }
}
