<?php

namespace App\Http\Controllers;

use App\FileImport;
use App\User;
use Illuminate\Support\Facades\Auth;

class FileImportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->hasRole('admin'))
            $import_list = FileImport::orderBy('created_at', 'DESC')->get();
        else
            $import_list = FileImport::where('creator', Auth::id());
        $upload_url = env('APP_PATH');
        foreach ($import_list as $import) {
            $creator = User::find($import->created_by);
            if (!empty($creator) && !empty($creator->username))
                $import->creator = $creator->username;
            $import->original_file_url = $upload_url . '/uploads/' . $import->original_file_name;
            if (!empty($import->success_file_name))
                $import->success_file_url = $upload_url . '/uploads/' . $import->success_file_name;
            if (!empty($import->duplicate_file_name))
                $import->duplicate_file_url = $upload_url . '/uploads/' . $import->duplicate_file_name;
            if (!empty($import->error_file_name))
                $import->error_file_url = $upload_url . '/uploads/' . $import->error_file_name;
        }
        return view('import.index', ['import_list' => $import_list]);
    }
}