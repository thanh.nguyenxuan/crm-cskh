<?php

namespace App\Http\Controllers;

use App\ManualLeadAssigner;
use Illuminate\Http\Request;
//use Request;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Input;

use App\Lead;
use App\Account;

use PHPExcel;

use PHPExcel_IOFactory;

use App\User;
use App\Role;
use App\UserPref;

use App\Importer;

use App\Source;

use App\Province;
use App\Department;
use App\Area;
use App\LeadStatus;
use App\CallStatus;
use App\Group;
use App\Campus;
use App\DataList;
use App\Channel;
use App\Campaign;
use App\Keyword;
use App\LeadSource;


use App\Account\Status as AccountStatus;

use SSP;

use PAMI\Client\Impl\ClientImpl as PamiClient;
use PAMI\Message\Event\EventMessage;
use PAMI\Message\Event\OriginateResponseEvent;
use PAMI\Listener\IEventListener;
use PAMI\Message\Action\OriginateAction;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\ChangeLog;
use App\Utils;

class LeadController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
        //$leads = DB::select('select * from lead where deleted = 0');
        //$leads = Lead::all();
        //var_dump(Auth::user()->hasRole('admin'));
        $source_list_data = Source::orderBy('name', 'ASC')->get();
        $source_list = array('na' => 'Không xác định');
        foreach ($source_list_data as $source_data) {
            $source_list[$source_data->id] = $source_data->name;
        }
        $channel_list_data = Channel::orderBy('name', 'ASC')->get();
        $channel_list = array();
        foreach ($channel_list_data as $channel_data) {
            $channel_list[$channel_data->id] = $channel_data->name;
        }
        $campaign_list_data = Campaign::orderBy('name', 'ASC')->get();
        $campaign_list = array();
        foreach ($campaign_list_data as $campaign_data) {
            $campaign_list[$campaign_data->id] = $campaign_data->name;
        }
        $keyword_list_data = Keyword::orderBy('name', 'ASC')->get();
        $keyword_list = array();
        foreach ($keyword_list_data as $keyword_data) {
            $keyword_list[$keyword_data->id] = $keyword_data->name;
        }
        $province_list_data = Province::orderBy('name', 'ASC')->get();
        $province_list = array('na' => 'Không xác định');
        foreach ($province_list_data as $province_data) {
            $province_list[$province_data->id] = $province_data->name;
        }
        $area_list_data = Area::all();
        $area_list = array('na' => 'Không xác định');
        foreach ($area_list_data as $area_data) {
            $area_list[$area_data->id] = $area_data->name;
        }
        $status_list = array('' => 'Trạng thái bất kỳ');
        $status_list_data = LeadStatus::all();
        foreach ($status_list_data as $status_data) {
            $status_list[$status_data->id] = $status_data->name;
        }
        //$call_status_list = array('na' => 'Không xác định');
        $call_status_list_data = CallStatus::all();
        foreach ($call_status_list_data as $call_status_data) {
            $call_status_list[$call_status_data->id] = $call_status_data->name;
        }
        $department_list_data = Department::all();
        $department_list = array();

        foreach ($department_list_data as $department_data) {
//            if (Auth::user()->hasRole('admin')) {
            $department_list[$department_data->id] = $department_data->name;
//            } else {
//                if (Auth::user()->department_id === $department_data->id)
//                    $department_list[$department_data->id] = $department_data->name;
//            }
        }

        $user_data_list = User::orderBy('username', 'ASC')->get();
        foreach ($user_data_list as $user_data) {
            $creator_list[$user_data->id] = $user_data->username . ' - ' . $user_data->full_name;
        }
        if (Auth::user()->hasRole('admin'))
            $assignee_list_data = User::orderBy('username', 'ASC')->get();
        else {
            if (Auth::user()->hasRole('teleteamleader') || (Auth::user()->hasRole('promoterteamleader')))
                $assignee_list_data = Auth::user()->getTeamMembers();
            else
                $assignee_list_data = User::where('department_id', Auth::user()->department_id)->orderBy('username', 'ASC')->get();
        }
        $assignee_list = array('nobody' => 'Chưa được chia');
        $activator_list = array();
        $caller_list = array();
        $rating_list = array(1 => 'Hot', 2 => 'Warm', 3 => 'Cold');
        foreach ($assignee_list_data as $assignee_data) {
            $assignee_list[$assignee_data->id] = $assignee_data->username . ' - ' . $assignee_data->full_name;
            $caller_list[$assignee_data->id] = $assignee_data->username . ' - ' . $assignee_data->full_name;
            $activator_list[$assignee_data->id] = $assignee_data->username . ' - ' . $assignee_data->full_name;
        }
        $filtering = array();
        $user_pref = UserPref::where(['user_id' => Auth::id(), 'key' => 'filtering'])->first();
        if (!empty($user_pref) && !empty($user_pref->value))
            $filtering = json_decode($user_pref->value);
        if (Auth::check())
            return view('lead.index', ['source_list' => $source_list, 'campaign_list' => $campaign_list, 'channel_list' => $channel_list, 'keyword_list' => $keyword_list, 'province_list' => $province_list, 'department_list' => $department_list, 'area_list' => $area_list, 'assignee_list' => $assignee_list, 'activator_list' => $activator_list, 'creator_list' => $creator_list, 'status_list' => $status_list, 'call_status_list' => $call_status_list, 'caller_list' => $caller_list, 'rating_list' => $rating_list, 'filtering' => $filtering]);
//        else
//            return redirect('/');
//        $options = array(
//            'host' => '210.245.80.34',
//            'scheme' => 'tcp://',
//            'port' => 5038,
//            'username' => 'crm',
//            'secret' => 'lehoang267@123',
//            'connect_timeout' => 10000,
//            'read_timeout' => 10000
//        );
//        $client = new PamiClient($options);
//        $client->open();
//        $originateMsg = new OriginateAction('SIP/1005');
//        //$originateMsg->setContext('default');
//        // $originateMsg->setPriority(1);
//        // $originateMsg->setExtension('1001');
//        $originateMsg->setApplication('Dial');
//        $originateMsg->setData('SIP/SIP07107301866/3320000989663230');
//        $originateMsg->setCallerId('Lê Đại Hoàng <1001>');
//        var_dump($client->send($originateMsg));
        // $client->registerEventListener(
        // function(OriginateResponseEvent $event){
        // $reason = $event->getReason();
        // },
        // function(EventMessage $event) {
        // return ($event instanceof OriginateResponseEvent);
        // });
        // $running = true;
        // while($running) {
        // $client->process();
        // usleep(1000);
        // }

//        $client->close();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $lead = new Lead;
        $status_list = array();
        $status_list_data = LeadStatus::all();
        foreach ($status_list_data as $status_data) {
            $status_list[$status_data->id] = $status_data->name;
        }
        $call_status_list = array(0 => 'Chưa chăm sóc');
        $call_status_list_data = CallStatus::all();
        foreach ($call_status_list_data as $call_status_data) {
            $call_status_list[$call_status_data->id] = $call_status_data->name;
        }
        $source_list = array();
        $source_list_data = Source::all();
        foreach ($source_list_data as $source_data) {
            $source_list[$source_data->id] = $source_data->name;
        }
        $department_list = array();
        $department_list_data = Department::all();
        foreach ($department_list_data as $department_data) {
            $department_list[$department_data->id] = $department_data->name;
        }
        $group_list = array();
        $group_list_data = Group::all();
        foreach ($group_list_data as $group_data) {
            $group_list[$group_data->id] = $group_data->name;
        }
        $province_list = array();
//        if(Auth::user()->hasRole('admin'))
//            $province_list_data = Province::all();
//        else
        $province_list_data = Province::where('department_id', Auth::user()->department_id)->get();
        foreach ($province_list_data as $province_data) {
            $province_list[$province_data->id] = $province_data->name;
        }
        $assignee_list = array();
        $assignee_list_data = User::all();
        foreach ($assignee_list_data as $assignee_data) {
            $assignee_list[$assignee_data->id] = $assignee_data->username;
        }
        $campus_list = array();
        $campus_list_data = Campus::all();
        foreach ($campus_list_data as $campus_data) {
            $campus_list[$campus_data->id] = $campus_data->name;
        }
        $exam_place_list = array();
        $exam_place_list_data = DataList::getItems(1);
        foreach ($exam_place_list_data as $exam_place_data) {
            $exam_place_list[$exam_place_data->id] = $exam_place_data->display_name;
        }
        $major_list = array();
        $major_list_data = DataList::getItems(2);
        foreach ($major_list_data as $major_data) {
            $major_list[$major_data->id] = $major_data->display_name;
        }
        $rating_list = array(1 => 'Hot', 2 => 'Warm', 3 => 'Cold');

        $request = array();
        return view('includes.lead.create', ['lead' => $lead, 'status_list' => $status_list, 'source_list' => $source_list, 'department_list' => $department_list, 'group_list' => $group_list, 'province_list' => $province_list, 'assignee_list' => $assignee_list, 'campus_list' => $campus_list, 'exam_place_list' => $exam_place_list, 'major_list' => $major_list, 'rating_list' => $rating_list, 'request' => $request]);
    }

    public function import(Request $request)
    {
        if ($request->isMethod('get')) {
            return view('lead.import');
        }
        if ($request->isMethod('post')) {
            $importer = new Importer;
            $result = $importer->import($request->file('import')->path());
            return view('lead.import', ['result' => $result]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $success = false;
        $return_message = '';
        $lead = new Lead;
        //var_dump($lead);
        $lead->name = $request->name;
        $lead->email = $request->email;
        $lead->facebook = $request->facebook;
        $source_id = $request->source;
        if (!empty($request->birthdate))
            $lead->birthdate = Utils::toDbDate($request->birthdate);
        $lead->phone1 = Utils::sanitizePhone($request->phone1);
        $lead->phone2 = Utils::sanitizePhone($request->phone2);
        $lead->phone3 = Utils::sanitizePhone($request->phone3);
        $lead->phone4 = Utils::sanitizePhone($request->phone4);
        if ($request->status == 5) {
            $lead->activator = Auth::id();
            $lead->activated_at = date('Y-m-d H:i:s');
        }
        $lead->status = $request->status;
        if (empty($request->status))
            $lead->status = 1;
        //$lead->source = $request->source;
        $lead->department_id = $request->department;
//        $lead->utm_source = $request->utm_source;
//        $lead->utm_campaign = $request->utm_campaign;
//        $lead->utm_medium = $request->utm_source;
        $lead->group_id = $request->group;
        $lead->province_id = $request->province;
        $lead->school = $request->school;
        $lead->want_study = $request->want_study;
        $lead->want_exam = $request->want_exam;
        $lead->major = $request->major;
        $lead->url_referrer = $request->url_referrer;
        $lead->url_request = $request->url_request;
        $lead->ga_client_id = $request->ga_client_id;
        $lead->rating = $request->rating;
        $lead->notes = $request->notes;
        $lead->created_by = Auth::id();
        if (!Utils::checkDuplicatePhone($lead->phone1) && !Utils::checkDuplicatePhone($lead->phone2) && !Utils::checkDuplicatePhone($lead->phone3) && !Utils::checkDuplicatePhone($lead->phone4)) {
            $lead_created = $lead->save();
            if ($lead_created) {
                if (!empty($source_id)) {
                    $lead_source = new LeadSource;
                    $lead_source->lead_id = $lead->id;
                    $lead_source->source_id = $source_id;
                    $lead_source->created_by = Auth::id();
                    $lead_source->save();
                }
            }
            $assignee = $request->assignee;
            //$existing = ManualLeadAssigner::where(['lead_id' => $lead->id, 'user_id' => $assignee])->count("id");
            //if (empty($existing)) {
            $assigner = new ManualLeadAssigner();
            $assigner->user_id = $assignee;
            $assigner->lead_id = $lead->id;
            $assigned = $assigner->save();
            if ($lead_created && $assigned) {
                $success = true;
                $return_message = 'Tạo lead thành công';
            } else {
                $success = false;
                $return_message = 'Đã có lỗi trong quá trình tạo lead!';
            }
        } else {
            $success = false;
            $return_message = 'Lead trùng số điện thoại!';
        }
        //return view('includes.lead.create', ['lead' => $lead, 'status_list' => $status_list, 'source_list' => $source_list, 'department_list' => $department_list, 'group_list' => $group_list, 'province_list' => $province_list, 'assignee_list' => $assignee_list, 'campus_list' => $campus_list, 'exam_place_list' => $exam_place_list, 'major_list' => $major_list]);
        return $this->create()->with('return_message', $return_message)->with('success', $success)->with('request', $request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function assignLead(Request $request)
    {
        $selected_leads = $request->input('selected');
        $assignee = $request->input('assignee');
        $additional_assignment = $request->input('additional_assignment');
        foreach ($selected_leads as $lead) {
            $new = false;
            if (empty($additional_assignment)) {
                ManualLeadAssigner::where(['lead_id' => $lead])->delete();
                $new = true;
            } else {
                $existing = ManualLeadAssigner::where(['lead_id' => $lead, 'user_id' => $assignee])->count("id");
                if (empty($existing)) {
                    $new = true;
                }
            }
            if ($new) {
                $assigner = new ManualLeadAssigner();
                $assigner->user_id = $assignee;
                $assigner->lead_id = $lead;
                $assigner->save();
            }
        }
    }

    public function listAll()
    {
        $search_mode = Input::get('search_mode');
        if ($search_mode == 'advanced_search') {
            $settings = UserPref::firstOrNew(['key' => 'filtering', 'user_id' => Auth::id()]);
            $settings->value = json_encode(Input::all());
            if (empty($settings->id))
                $settings->created_at = date('Y-m-d H:i:s');
            else
                $settings->updated_at = date('Y-m-d H:i:s');
            $settings->save();
        }
        $start = Input::get('start');
        $length = Input::get('length');
        $current_user_id = Auth::user()->id;
        $department_id = Auth::user()->department_id;
        $phone = trim(Input::get('phone'));
        $where_telesales = '';
        $where_assignee = '';
        $exact_phone_found = false;
        $source = Input::get('source');
        $where_source = '';
        $channel = Input::get('channel');
        $where_channel = '';
        $campaign = Input::get('campaign');
        $where_campaign = '';
        $keyword = Input::get('keyword');
        $where_keyword = '';
        if (isset($_REQUEST['gsearch']))
            $gsearch = $_REQUEST['gsearch'];
        $data = DB::table('fptu_lead')
            ->leftJoin('fptu_province', 'fptu_lead.province_id', '=', 'fptu_province.id')
            ->leftJoin('fptu_department', 'fptu_lead.department_id', '=', 'fptu_department.id')
            ->leftJoin('fptu_user as creator', 'fptu_lead.created_by', '=', 'creator.id')
            ->leftJoin('fptu_user as activator', 'fptu_lead.activator', '=', 'activator.id')
            ->leftJoin('fptu_lead_status as lead_status', 'fptu_lead.status', '=', 'lead_status.id')
            ->leftJoin('fptu_call_status', 'fptu_lead.call_status', '=', 'fptu_call_status.id')
            ->leftJoin('fptu_school', 'fptu_lead.school', '=', 'fptu_school.id');
        if ($search_mode == 'advanced_search') {
            $assignee = Input::get('assignee');
            //var_dump($assignee);
            if (!empty($assignee)) {
                $nobody_found = array_search('nobody', $assignee);
                if ($nobody_found && (count($assignee) == 1)) {
                    //$where_assignee .= " AND a.user_id IS NULL ";
                } else {
                    if ($nobody_found) {
                        unset($assignee[$nobody_found]);
                    }
                    if (!empty($assignee)) {
                        $assignee_list_string = "'" . implode("','", $assignee) . "'";
                        $where_assignee .= " AND a.user_id IN ($assignee_list_string) ";
                    }
                }
            }
            $where_caller = '';
            $caller = Input::get('caller');
            if (!empty($caller)) {
                $where_caller = " AND b.changer IN ('" . implode("','", $caller) . "') ";
                $data = $data->join(DB::raw("(SELECT DISTINCT a.id as lead_id, b.changer as caller FROM fptu_lead a INNER JOIN fptu_change_log b ON a.id = b.object_id AND b.object = 'lead' WHERE 1=1 $where_caller) as caller_list"), 'caller_list.lead_id', '=', 'fptu_lead.id');
            }
        }
        if ($search_mode == 'global_search' && !empty($gsearch)) {
            $search_phone = $gsearch;
        } else {
            $search_phone = $phone;
        }
        if (substr(trim($search_phone), 0, 1) != 0)
            $search_phone = '0' . $search_phone;
        $data = $data->leftJoin(DB::raw("(SELECT a.lead_id, GROUP_CONCAT(b.username) as `assignee_list` FROM `fptu_lead_assignment` a INNER JOIN fptu_user b ON a.user_id = b.id WHERE 1=1 $where_assignee GROUP BY a.lead_id) as assignee_info"), 'assignee_info.lead_id', '=', 'fptu_lead.id');
        $exact_phone_found = DB::table('fptu_lead')->where('phone1', 'LIKE', $search_phone)
            ->orWhere('phone2', 'LIKE', $search_phone)
            ->orWhere('phone3', 'LIKE', $search_phone)
            ->orWhere('phone4', 'LIKE', $search_phone)->count();
//var_dump(ManualLeadAssigner::distinct()->pluck('lead_id')->get());
        if (!$exact_phone_found) {
            if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('onlinemarketer')) {
                if (!empty($assignee)) {
                    if ($assignee == array('nobody'))
                        $data = $data->whereNotIn('fptu_lead.id', ManualLeadAssigner::select('lead_id')->distinct()->get());
                    else
                        $data = $data->join(DB::raw("(SELECT a.lead_id, GROUP_CONCAT(b.username) as `assignee_list` FROM `fptu_lead_assignment` a INNER JOIN fptu_user b ON a.user_id = b.id WHERE 1=1 $where_assignee GROUP BY a.lead_id) as assignee"), 'assignee.lead_id', '=', 'fptu_lead.id');
                }
            } elseif (Auth::user()->hasRole('teleteamleader') || Auth::user()->hasRole('promoterteamleader')) {
                $team_members = Auth::user()->getTeamMembers();
                $member_user_list = array($current_user_id);
                foreach ($team_members as $member) {
                    $member_user_list[] = $member->id;
                }
                $where_group_members = implode("','", $member_user_list);
                $data = $data->join(DB::raw("(SELECT a.lead_id, GROUP_CONCAT(b.username) as `assignee_list` FROM `fptu_lead_assignment` a INNER JOIN fptu_user b ON a.user_id = b.id WHERE a.user_id IN ('$where_group_members') $where_assignee GROUP BY a.lead_id) as assignee"), 'assignee.lead_id', '=', 'fptu_lead.id');
            } else
                $data = $data->join(DB::raw("(SELECT a.lead_id, GROUP_CONCAT(b.username) as `assignee_list` FROM `fptu_lead_assignment` a INNER JOIN fptu_user b ON a.user_id = b.id WHERE a.user_id = '$current_user_id' $where_assignee GROUP BY a.lead_id) as assignee"), 'assignee.lead_id', '=', 'fptu_lead.id');
        }
        if ($source == array('na')) {
            $data = $data->whereNotIn('fptu_lead.id', LeadSource::select('lead_id')->distinct()->get());
        } else {
            if (!empty($source)) {
                $na_found = array_search('na', $source);
                //var_dump($na_found);
                if (FALSE !== $na_found)
                    unset($source[$na_found]);
                if (!empty($source)) {
                    $source_list_string = implode(',', $source);
                    $where_source = ' AND a.source_id IN (' . $source_list_string . ') ';
                }
                //var_dump($source);
                if (!empty($where_source))
                    $data = $data->join(DB::raw("(SELECT a.lead_id, GROUP_CONCAT(DISTINCT b.name) as `source_list` FROM `fptu_lead_source` a INNER JOIN fptu_source b ON a.source_id = b.id WHERE 1=1 $where_source GROUP BY a.lead_id) as lead_source_filter"), 'lead_source_filter.lead_id', '=', 'fptu_lead.id');
            }
        }
        $data = $data->leftJoin(DB::raw("(SELECT a.lead_id, GROUP_CONCAT(DISTINCT b.name) as `source_list` FROM `fptu_lead_source` a INNER JOIN fptu_source b ON a.source_id = b.id GROUP BY a.lead_id) as lead_source"), 'lead_source.lead_id', '=', 'fptu_lead.id');

        if (!empty($channel)) {
            $channel_list_string = implode(',', $channel);
            $where_channel = ' AND a.channel_id IN (' . $channel_list_string . ') ';
        }

        if (!empty($where_channel))
            $data = $data->join(DB::raw("(SELECT a.lead_id, GROUP_CONCAT(DISTINCT b.name) as `channel_list` FROM `fptu_lead_channel` a INNER JOIN fptu_channel b ON a.channel_id = b.id WHERE 1=1 $where_channel GROUP BY a.lead_id) as lead_channel_filter"), 'lead_channel_filter.lead_id', '=', 'fptu_lead.id');
        $data = $data->leftJoin(DB::raw("(SELECT a.lead_id, GROUP_CONCAT(DISTINCT b.name) as `channel_list` FROM `fptu_lead_channel` a INNER JOIN fptu_channel b ON a.channel_id = b.id GROUP BY a.lead_id) as lead_channel"), 'lead_channel.lead_id', '=', 'fptu_lead.id');

        if (!empty($campaign)) {
            $campaign_list_string = implode(',', $campaign);
            $where_campaign = ' AND a.campaign_id IN (' . $campaign_list_string . ') ';
        }
        if (!empty($where_campaign))
            $data = $data->join(DB::raw("(SELECT a.lead_id, GROUP_CONCAT(DISTINCT b.name) as `campaign_list` FROM `fptu_lead_campaign` a INNER JOIN fptu_campaign b ON a.campaign_id = b.id WHERE 1=1 $where_campaign GROUP BY a.lead_id) as lead_campaign_filter"), 'lead_campaign_filter.lead_id', '=', 'fptu_lead.id');
        $data = $data->leftJoin(DB::raw("(SELECT a.lead_id, GROUP_CONCAT(DISTINCT b.name) as `campaign_list` FROM `fptu_lead_campaign` a INNER JOIN fptu_campaign b ON a.campaign_id = b.id GROUP BY a.lead_id) as lead_campaign"), 'lead_campaign.lead_id', '=', 'fptu_lead.id');

        if (!empty($keyword)) {
            $keyword_list_string = implode(',', $keyword);
            $where_keyword = ' AND a.keyword_id IN (' . $keyword_list_string . ') ';
        }
        if (!empty($where_keyword))
            $data = $data->join(DB::raw("(SELECT a.lead_id, GROUP_CONCAT(DISTINCT b.name) as `keyword_list` FROM `fptu_lead_keyword` a INNER JOIN fptu_keyword b ON a.keyword_id = b.id WHERE 1=1 $where_keyword GROUP BY a.lead_id) as lead_keyword_filter"), 'lead_keyword_filter.lead_id', '=', 'fptu_lead.id');
        $data = $data->leftJoin(DB::raw("(SELECT a.lead_id, GROUP_CONCAT(DISTINCT b.name) as `keyword_list` FROM `fptu_lead_keyword` a INNER JOIN fptu_keyword b ON a.keyword_id = b.id GROUP BY a.lead_id) as lead_keyword"), 'lead_keyword.lead_id', '=', 'fptu_lead.id');

        $data = $data->select('fptu_lead.*', 'fptu_province.name as province', 'fptu_department.name as department', 'assignee_info.assignee_list', 'creator.username as creator', DB::raw("CONCAT_WS(',', fptu_lead.phone1, fptu_lead.phone2, fptu_lead.phone3 , fptu_lead.phone4) as phone"), 'lead_status.name as lead_status_name', 'fptu_school.name as school_name', 'fptu_call_status.name as call_status_name', 'activator.username as activator_username', 'lead_source.source_list', 'lead_channel.channel_list', 'lead_campaign.campaign_list', 'lead_keyword.keyword_list');
//        if (!Auth::user()->hasRole('admin') && empty($exact_phone_found)) {
//            $data = $data->where('fptu_lead.department_id', '=', $department_id);
//        }
        if ($search_mode == 'global_search' && !empty($gsearch)) {
            $data = $data
                ->where('fptu_lead.name', 'LIKE', "%$gsearch%")
                ->orWhere('fptu_lead.phone1', 'LIKE', "%$gsearch%")
                ->orWhere('fptu_lead.phone2', 'LIKE', "%$gsearch%")
                ->orWhere('fptu_lead.phone3', 'LIKE', "%$gsearch%")
                ->orWhere('fptu_lead.phone4', 'LIKE', "%$gsearch%")
                ->orWhere('fptu_lead.email', 'LIKE', "%$gsearch%")
                ->orWhere('fptu_lead.facebook', 'LIKE', "%$gsearch%")
                ->orWhere('fptu_lead.notes', 'LIKE', "%$gsearch%");
        } elseif
        ($search_mode == 'advanced_search') {
            $name = Input::get('name');
            if (!empty($name)) {
                $data = $data->where('fptu_lead.name', 'LIKE', "%$name%");
            }
            if (!empty($phone)) {
                $data = $data
                    ->where(function ($data) use ($phone) {
                        $data->where('fptu_lead.phone1', 'LIKE', "%$phone%")
                            ->orWhere('fptu_lead.phone2', 'LIKE', "%$phone%")
                            ->orWhere('fptu_lead.phone3', 'LIKE', "%$phone%")
                            ->orWhere('fptu_lead.phone4', 'LIKE', "%$phone%");
                    });
            }
            $email = Input::get('email');
            if (!empty($email)) {
                $data = $data->where('fptu_lead.email', 'LIKE', "%$email%");
            }
            $status = Input::get('status_id');
//var_dump($status);
            if (!empty($status) && !empty($status[0])) {
                $data = $data->whereIn('fptu_lead.status', $status);
            }
            $rating = Input::get('rating');
            if (!empty($rating)) {
                $data = $data->whereIn('fptu_lead.rating', $rating);
            }
            $call_status = Input::get('call_status_id');
//var_dump($status);
            if (!empty($call_status)) {
                $data = $data->whereIn('fptu_lead.call_status', $call_status);
            }
            $birthyear = Input::get('birthyear');
            if (!empty($birthyear)) {
                $data = $data->whereRaw('YEAR(fptu_lead.birthdate) = ?', [$birthyear]);
            }
            $birthdate = Input::get('birthdate');
            if (!empty($birthdate)) {
                $birthdate = Utils::toDbDate($birthdate);
                $data = $data->where('fptu_lead.birthdate', '=', $birthdate);
            }
            $school = Input::get('school');
            if (!empty($school)) {
                $data = $data->where('fptu_lead.school', '=', $school);
            }
            $province = Input::get('province');
//var_dump($province);
            if (!empty($province)) {
                if (in_array('na', $province)) {
                    $na_index = array_search('na', $province);
                    if (FALSE !== $na_index) {
                        unset($province[$na_index]);
                    }
                    $data = $data->whereNull('fptu_lead.province_id')
                        ->orWhere('fptu_lead.province_id', '=', 0);
                    if (count($province) > 0)
                        $data = $data->orWhereIn('fptu_lead.province_id', $province);
                } else {
                    $data = $data->whereIn('fptu_lead.province_id', $province);
                }
            }
            $department = Input::get('department_id');
            if (!empty($department)) {
                $data = $data->whereIn('fptu_lead.department_id', $department);
            }
            $area = Input::get('area');
            if (!empty($area)) {
                if (in_array('na', $area)) {
                    $na_index = array_search('na', $area);
                    if (FALSE !== $na_index) {
                        unset($area[$na_index]);
                    }
                    $data = $data->whereNull('fptu_lead.area_id')
                        ->orWhere('fptu_lead.area_id', '=', 0);
                    if (count($area) > 0)
                        $data = $data->orWhereIn('fptu_lead.area_id', $area);
                } else {
                    $data = $data->whereIn('fptu_lead.area_id', $area);
                }
            }
            $import_count = Input::get('import_count');
            $import_count_operator = Input::get('import_count_operator');
            if (!empty($import_count) && !empty($import_count_operator)) {
                $operator = '';
                switch ($import_count_operator) {
                    case 'ge':
                        $operator = '>=';
                        break;
                    case 'gt':
                        $operator = '>';
                        break;
                    case 'le':
                        $operator = '<=';
                        break;
                    case 'lt':
                        $operator = '<';
                        break;
                    default:
                        $operator = '>=';
                        break;
                }
                $data = $data->where('fptu_lead.import_count', $operator, $import_count);
            }

            $notes = Input::get('notes');
            if (!empty($notes)) {
                $data = $data->where('fptu_lead.notes', 'LIKE', "%$notes%");
            }
            $activator = Input::get('activator');
            if (!empty($activator)) {
                $data = $data->whereIn('fptu_lead.activator', $activator);
            }
            $creator = Input::get('creator');
            if (!empty($creator)) {
                $data = $data->whereIn('fptu_lead.created_by', $creator);
            }
            $start_date = Input::get('start_date');
            if (!empty($start_date)) {
                $start_date = Carbon::createFromFormat('d/m/Y', $start_date)->format('Y-m-d 00:00:00');
                $data = $data->where('fptu_lead.created_at', '>=', "$start_date");
            }
            $end_date = Input::get('end_date');
            if (!empty($end_date)) {
                $end_date = Carbon::createFromFormat('d/m/Y', $end_date)->format('Y-m-d 23:59:59');
                $data = $data->where('fptu_lead.created_at', '<=', "$end_date");
            }
            $active_from = Input::get('active_from');
            if (!empty($active_from)) {
                $active_from = Carbon::createFromFormat('d/m/Y', $active_from)->format('Y-m-d 00:00:00');
                $data = $data->where('fptu_lead.activated_at', '>=', "$active_from");
            }
            $active_to = Input::get('active_to');
            if (!empty($active_to)) {
                $active_to = Carbon::createFromFormat('d/m/Y', $active_to)->format('Y-m-d 23:59:59');
                $data = $data->where('fptu_lead.activated_at', '<=', "$active_to");
            }
        }
//echo $data->toSql();

        $countTotal = $data->count();
        $order_by = Input::get('order');
        $order_by_col_num = $order_by[0]['column'];
        $order_by_direction = $order_by[0]['dir'];
        if (!empty($order_by_col_num)) {
            switch ($order_by_col_num) {
                case 1:
                    $order_by_col_name = 'fptu_lead.name';
                    break;
                case 5:
                    $order_by_col_name = 'activator.username';
                    break;
                case 6:
                    $order_by_col_name = 'fptu_lead.email';
                    break;
                case 8:
                    $order_by_col_name = 'fptu_source.name';
                    break;
                case 10:
                    $order_by_col_name = 'fptu_province.name';
                    break;
                case 11:
                    $order_by_col_name = 'fptu_department.name';
                    break;
                case 12:
                    $order_by_col_name = 'lead_status.name';
                    break;
                case 15:
                    $order_by_col_name = 'fptu_lead.import_count';
                    break;
                case 16:
                    $order_by_col_name = 'creator.username';
                    break;
                case 17:
                    $order_by_col_name = 'fptu_lead.created_at';
                    break;
                default:
                    $order_by_col_name = 'fptu_lead.created_at';
                    break;
            }
            $data = $data->orderBy($order_by_col_name, $order_by_direction);
        } else
            $data = $data->orderBy('fptu_lead.import_count', 'DESC')->orderBy('fptu_lead.created_at', 'DESC');
        if ($length)
            $data = $data->limit($length);

        if ($start)
            $data = $data->offset($start);

        $data = array(
            'data' => $data->get(),
            'recordsTotal' => $countTotal,
            'recordsFiltered' => $countTotal,
        );
        echo json_encode($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function edit($id)
    {
        $lead = Lead::find($id);
//        if (!empty($lead->birthdate))
//            $lead->birthdate = Carbon::createFromFormat('Y-m-d', $lead->birthdate)->format('d/m/Y');
        //var_dump($request->birthdate);
        $status_list = array();
        $status_list_data = LeadStatus::all();
        foreach ($status_list_data as $status_data) {
            $status_list[$status_data->id] = $status_data->name;
        }
        $source_list_array = array();
        $source_list_data = DB::table('fptu_lead_source')
            ->select('fptu_source.name')
            ->join('fptu_source', 'fptu_lead_source.source_id', '=', 'fptu_source.id')
            ->where('fptu_lead_source.lead_id', '=', $id)
            ->orderBy('fptu_source.name', 'ASC')
            ->distinct()
            ->get();
        foreach ($source_list_data as $source_data) {
            $source_list_array[] = $source_data->name;
        }
        $source_list = implode(', ', $source_list_array);
        $department_list = array();
        $department_list_data = Department::all();
        foreach ($department_list_data as $department_data) {
            $department_list[$department_data->id] = $department_data->name;
        }
        $group_list = array();
        $group_list_data = Group::all();
        foreach ($group_list_data as $group_data) {
            $group_list[$group_data->id] = $group_data->name;
        }
        $province_list = array();
//        if(Auth::user()->hasRole('admin'))
//            $province_list_data = Province::all();
//        else
        $province_list_data = Province::where('department_id', Auth::user()->department_id)->get();
        foreach ($province_list_data as $province_data) {
            $province_list[$province_data->id] = $province_data->name;
        }
        $assignment = DB::table('fptu_lead_assignment');
        $assignment_list_data =
            $assignment->join('fptu_user', 'fptu_user.id', '=', 'fptu_lead_assignment.user_id')
                ->select('fptu_user.*')
                ->where('fptu_lead_assignment.lead_id', '=', $id)->get();
        $assignee_list = '';
        $assignee_list_data = array();
        foreach ($assignment_list_data as $assignment_data) {
            //var_dump($assignment_data);
            $assignee_list .= $assignment_data->username . '<br />';
            $assignee_list_data[] = $assignment_data->id;
        }
        $account_status_list = array();
        $account_status_data_list = AccountStatus::all();
        foreach ($account_status_data_list as $account_data_list) {
            $account_status_list[$account_data_list->id] = $account_data_list->name;
        }
        $lead->assignee_list = $assignee_list;
        $lead->assignee_list_data = $assignee_list_data;
//        $school_required = false;
//        if (!empty($lead->school_other) && empty($lead->school))
//            $school_required = true;
        $account = Account::firstOrCreate(['lead_id' => $lead->id]);
        $major_list = array();
        $major_list_data = DataList::getItems(2);
        foreach ($major_list_data as $major_data) {
            $major_list[$major_data->id] = $major_data->display_name;
        }
        $campus_list = array();
        $campus_list_data = Campus::all();
        foreach ($campus_list_data as $campus_data) {
            $campus_list[$campus_data->id] = $campus_data->name;
        }
        $exam_place_list = array();
        $exam_place_list_data = DataList::getItems(1);
        foreach ($exam_place_list_data as $exam_place_data) {
            $exam_place_list[$exam_place_data->id] = $exam_place_data->display_name;
        }
        //$call_status_list = array('na' => 'Không xác định');
        $call_status_list_data = CallStatus::all();
        foreach ($call_status_list_data as $call_status_data) {
            $call_status_list[$call_status_data->id] = $call_status_data->name;
        }
        $receiving_place_list = array(1 => 'HN', 2 => 'DN', 3 => 'CT', 4 => 'HCM');
        $campaign_list = array(1 => 'Đợt 1', 2 => 'Đợt 2', 3 => 'Đợt 3', 4 => 'Đợt 4');
        $account_financial_status_list = array('' => 'Chọn trạng thái', 1 => 'Đợt 1', 2 => 'Đợt 2', 3 => 'Đợt 3', 4 => 'Đợt 4');
        $payment_method_list = array('' => 'Chọn hình thức nộp tiền', 1 => '', 2 => '');
        $rating_list = array(1 => 'Hot', 2 => 'Warm', 3 => 'Cold');
        $change_log = ChangeLog::where('object', 'lead')
            ->where('object_id', $lead->id)
            ->orWhere('object', 'account')
            ->where('object_id', $id)
            ->orWhere('object_id', $account->id)
            ->get();
        foreach ($change_log as $log) {
            $log->changer = User::find($log->changer)->username . ' - ' . User::find($log->changer)->full_name;
            $content = '';
            if ($log->type == 'create') {
                $content .= "Nhập mới trường <strong>{$log->field_display_name}</strong> với giá trị <strong>'" . $log->value_after . '\'</strong>';
            }
            if ($log->type == 'edit') {
                $content .= "Chỉnh sửa trường <strong>{$log->field_display_name}</strong> từ '<strong>{$log->value_before_description}</strong>' thành '<strong>{$log->value_after_description}</strong>'";
            }
            $log->log .= $content;
        }
        return view('includes.lead.edit', ['id' => $id, 'lead' => $lead, 'account' => $account, 'status_list' => $status_list, 'source_list' => $source_list, 'department_list' => $department_list, 'group_list' => $group_list, 'province_list' => $province_list, 'account_status_list' => $account_status_list, 'major_list' => $major_list, 'campaign' => $campaign_list, 'account_financial_status_list' => $account_financial_status_list, 'payment_method_list' => $payment_method_list, 'receiving_place_list' => $receiving_place_list, 'change_log' => $change_log, 'exam_place_list' => $exam_place_list, 'campus_list' => $campus_list, 'call_status_list' => $call_status_list, 'rating_list' => $rating_list]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    private
    function log($change_type, $object_id, $object_type, $field_name, $field_display_name, $before_value, $before_value_description = '', $after_value, $after_value_description = '')
    {
        // var_dump($request->$field_name);
        // var_dump($lead->$field_name);
        // if(empty($before_value)) {
        // $before_value = $lead->$field_name;
        // $after_value = $request->$field_name;
        // }''
        if ($before_value != $after_value) {
            ChangeLog::create(['type' => $change_type, 'object' => $object_type, 'object_id' => $object_id, 'field_name' => $field_name, 'field_display_name' => $field_display_name, 'value_before' => $before_value, 'value_after' => $after_value, 'changer' => Auth::user()->id]);
        }
    }

    public
    function update(Request $request, $id)
    {
        $lead = Lead::find($id);
        $status_list = array();
        $status_list_data = LeadStatus::all();
        foreach ($status_list_data as $status_data) {
            $status_list[$status_data->id] = $status_data->name;
        }
        $source_list_array = array();
        $source_list_data = DB::table('fptu_lead_source')
            ->select('fptu_source.name')
            ->join('fptu_source', 'fptu_lead_source.source_id', '=', 'fptu_source.id')
            ->where('fptu_lead_source.lead_id', '=', $id)
            ->distinct()
            ->get();
        foreach ($source_list_data as $source_data) {
            $source_list_array[] = $source_data->name;
        }
        $source_list = implode(', ', $source_list_array);
        $department_list = array();
        $department_list_data = Department::all();
        foreach ($department_list_data as $department_data) {
            $department_list[$department_data->id] = $department_data->name;
        }
        $group_list = array();
        $group_list_data = Group::all();
        foreach ($group_list_data as $group_data) {
            $group_list[$group_data->id] = $group_data->name;
        }
        $province_list = array();
//        if(Auth::user()->hasRole('admin'))
//            $province_list_data = Province::all();
//        else
        $province_list_data = Province::where('department_id', Auth::user()->department_id)->get();
        foreach ($province_list_data as $province_data) {
            $province_list[$province_data->id] = $province_data->name;
        }
        $assignment = DB::table('fptu_lead_assignment');
        $assignment_list_data =
            $assignment->join('fptu_user', 'fptu_user.id', '=', 'fptu_lead_assignment.user_id')
                ->select('fptu_user.*')
                ->where('fptu_lead_assignment.lead_id', '=', $id)->get();
        $assignee_list = '';
        $assignee_list_data = array();
        foreach ($assignment_list_data as $assignment_data) {
            $assignee_list .= $assignment_data->username . '<br />';
            $assignee_list_data[] = $assignment_data->id;
        }
        $account_status_list = array();
        $account_status_data_list = AccountStatus::all();
        foreach ($account_status_data_list as $account_data_list) {
            $account_status_list[$account_data_list->id] = $account_data_list->name;
        }
        //$lead->assignee_list = $assignee_list;
        //$lead->assignee_list_data = $assignee_list_data;
        $account = Account::firstOrCreate(['lead_id' => $lead->id]);
        $major_list = array();
        $major_list_data = DataList::getItems(2);
        foreach ($major_list_data as $major_data) {
            $major_list[$major_data->id] = $major_data->display_name;
        }
        $campus_list = array();
        $campus_list_data = Campus::all();
        foreach ($campus_list_data as $campus_data) {
            $campus_list[$campus_data->id] = $campus_data->name;
        }
        $exam_place_list = array();
        $exam_place_list_data = DataList::getItems(1);
        foreach ($exam_place_list_data as $exam_place_data) {
            $exam_place_list[$exam_place_data->id] = $exam_place_data->display_name;
        }
        $call_status_list_data = CallStatus::all();
        foreach ($call_status_list_data as $call_status_data) {
            $call_status_list[$call_status_data->id] = $call_status_data->name;
        }
        $receiving_place_list = array(1 => 'HN', 2 => 'DN', 3 => 'CT', 4 => 'HCM');
        $campaign_list = array(1 => 'Đợt 1', 2 => 'Đợt 2', 3 => 'Đợt 3', 4 => 'Đợt 4');
        $account_financial_status_list = array('' => 'Chọn trạng thái', 1 => 'Đợt 1', 2 => 'Đợt 2', 3 => 'Đợt 3', 4 => 'Đợt 4');
        $payment_method_list = array('' => 'Chọn hình thức nộp tiền', 1 => '', 2 => '');
        $rating_list = array(1 => 'Hot', 2 => 'Warm', 3 => 'Cold');
        $change_log = ChangeLog::where('object', 'lead')
            ->where('object_id', $lead->id)
            ->orWhere('object', 'account')
            ->where('object_id', $id)
            ->orWhere('object_id', $account->id)
            ->get();


        $return_message = '';
        $success = true;
        $duplicate = false;
        $phone1 = Utils::sanitizePhone($request->phone1);
        $phone2 = Utils::sanitizePhone($request->phone2);
        $phone3 = Utils::sanitizePhone($request->phone3);
        $phone4 = Utils::sanitizePhone($request->phone4);
        if (!empty($phone1) && $lead->checkDuplicate($phone1)) {
            $duplicate = true;
            $return_message = 'Số điện thoại 1 ' . $phone1 . ' bị trùng!';
        }
        if (!empty($phone2) && $lead->checkDuplicate($phone2)) {
            $duplicate = true;
            $return_message = "Số điện thoại 2 " . $phone2 . " bị trùng!";
        }
        if (!empty($phone3) && $lead->checkDuplicate($phone3)) {
            $duplicate = true;
            $return_message = 'Số điện thoại 3 ' . $phone3 . ' bị trùng!';

        }
        if (!empty($phone4) && $lead->checkDuplicate($phone4)) {
            $duplicate = true;
            $return_message = 'Số điện thoại 4 ' . $phone4 . ' bị trùng!';
        }
        if ($duplicate) {
            $phone_list = array();
            $success = false;
            if (!empty($phone1))
                $phone_list[] = $phone1;
            if (!empty($phone2))
                $phone_list[] = $phone2;
            if (!empty($phone3))
                $phone_list[] = $phone3;
            if (!empty($phone4))
                $phone_list[] = $phone4;
            $dup_lead = null;
            foreach ($phone_list as $phone) {
                $dup_lead = Lead::where('phone1', $phone)
                    ->orWhere('phone2', $phone)
                    ->orWhere('phone3', $phone)
                    ->orWhere('phone4', $phone)
                    ->get();
                var_dump($dup_lead);
            }
        }
        if (!empty($lead) && empty($duplicate)) {
            $this->log('edit', $lead->id, 'lead', 'name', 'Họ và tên', $lead->name, $lead->name, $request->name, $request->name);
            $lead->name = $request->name;
            $this->log('edit', $lead->id, 'lead', 'email', 'Email', $lead->email, $lead->email, $request->email, $request->email);
            //$this->log($lead, $request, 'email', 'Email');
            $lead->email = $request->email;
            $this->log('edit', $lead->id, 'lead', 'facebook', 'Facebook', $lead->facebook, $lead->facebook, $request->facebook, $request->facebook);
            //$this->log($lead, $request, 'facebook', 'Facebook');
            $lead->facebook = $request->facebook;
            $this->log('edit', $lead->id, 'lead', 'birthdate', 'Ngày sinh', $lead->birthdate, $lead->birthdate, $request->birthdate, $request->birthdate);
            //$this->log($lead, $request, 'birthdate', 'Ngày sinh');
            if (!empty($request->birthdate) && Utils::validateDisplayBirthdate($request->birthdate))
                $lead->birthdate = Carbon::createFromFormat('d/m/Y', $request->birthdate)->format('Y-m-d');
            $this->log('edit', $lead->id, 'lead', 'phone1', 'SĐT 1', $lead->phone1, $lead->phone1, $request->phone1, $request->phone1);
            $this->log('edit', $lead->id, 'lead', 'phone2', 'SĐT 2', $lead->phone2, $lead->phone2, $request->phone2, $request->phone2);
            //$this->log($lead, $request, 'phone1', 'SĐT 2');
            $lead->phone2 = $request->phone2;
            $this->log('edit', $lead->id, 'lead', 'phone3', 'SĐT 3', $lead->phone3, $lead->phone3, $request->phone3, $request->phone3);
            //$this->log($lead, $request, 'phone1', 'SĐT 3');
            $lead->phone3 = $request->phone3;
            $this->log('edit', $lead->id, 'lead', 'phone4', 'SĐT 4', $lead->phone4, $lead->phone4, $request->phone4, $request->phone4);
            //$this->log($lead, $request, 'phone1', 'SĐT 4');
            $lead->phone4 = $request->phone4;
            $status_before_value_desc = '';
            if (!empty($lead->status))
                $status_before_value_desc = LeadStatus::find($lead->status)->name;
            $status_after_value_desc = '';
            if (!empty($request->status))
                $status_after_value_desc = LeadStatus::find($request->status)->name;
            if ($request->status == 5 && empty($lead->activator) && ($lead->status != $request->status)) {
                $lead->activator = Auth::user()->id;
                $lead->activated_at = date('Y-m-d H:i:s');
            }
            $this->log('edit', $lead->id, 'lead', 'status', 'Trạng thái lead', $lead->status, $status_before_value_desc, $request->status, $status_after_value_desc);
            $lead->status = $request->status;
            //$source_value = '';
//            if (!empty($lead->source)) {
//                $source = Source::where('name', $request->source)->first();
//                if (!empty($source)) {
//                    $source_value = $source->name;
//                    $lead_source = new LeadSource;
//                    $lead_source->lead_id = $id;
//                    $lead_source->source_id = $source->id;
//                    $lead_source->save();
//                } else {
//                    $success = false;
//                    $return_message = 'Nguồn lead không hợp lệ!';
//                }
//            }
            //if (!empty($lead->source))
            //$this->log('edit', $lead->id, 'lead', 'source', 'Nguồn lead', $source_value, '', Source::find($request->source)->name, '');
            //$this->log($lead, $request, 'source', 'Nguồn lead');
            //$lead->source = $request->source;
            //this->log($lead, $request, 'department', 'Phòng');
            $source_value = '';
            if (!empty($lead->department))
                $this->log('edit', $lead->id, 'lead', 'department', 'Phòng', Department::find($lead->department_id)->name, '', Department::find($request->department)->name, '');
            $lead->department_id = $request->department;
            //$this->log($lead, $request, 'utm_source', 'Kênh quảng cáo');
//            $this->log('edit', $lead->id, 'lead', 'utm_source', 'Kênh quảng cáo', $lead->utm_source, '', $request->utm_source, '');
//            $lead->utm_source = $request->utm_source;
//            //$this->log($lead, $request, 'utm_campaign', 'Chiến dịch quảng cáo');
//            $this->log('edit', $lead->id, 'lead', 'utm_campaign', 'Chiến dịch quảng cáo', $lead->utm_campaign, '', $request->utm_campaign, '');
//            $lead->utm_campaign = $request->utm_campaign;
//            $this->log('edit', $lead->id, 'lead', 'utm_medium', 'Từ khóa quảng cáo', $lead->utm_medium, '', $request->utm_medium, '');
////			  $this->log($lead, $request, 'utm_medium', 'Từ khóa quảng cáo');
//            $lead->utm_medium = $request->utm_medium;
            $lead->group_id = $request->group;
            $lead->province_id = $request->province;
            $this->log('edit', $lead->id, 'lead', 'school', 'Trường', $lead->school, '', $request->school, '');
            $lead->school = $request->school;
            //$this->log($lead, $request, 'want_study', 'Mong muốn học');
            $lead->want_study = $request->want_study;
            //$this->log($lead, $request, 'want_exam', 'Mong muốn thi');
            $lead->want_exam = $request->want_exam;
            //$this->log($lead, $request, 'major', 'Ngành đăng ký');
            $lead->major = $request->major;
            //$this->log($lead, $request, 'url_referrer', 'URL Referrer');
            $lead->url_referrer = $request->url_referrer;
            //$this->log($lead, $request, 'url_request', 'URL Request');
            $lead->url_request = $request->url_request;
            //$this->log($lead, $request, 'ga_client_id', 'GA Client ID');
            $lead->ga_client_id = $request->ga_client_id;
            $this->log('edit', $lead->id, 'lead', 'notes', 'Ghi chú', $lead->notes, $lead->notes, $request->notes, $request->notes);
            //$this->log($lead, $request, 'notes', 'Ghi chú');
            $lead->notes = $request->notes;
            $lead->study_place = $request->study_place;
            $lead->exam_place = $request->exam_place;
            $lead->call_status = $request->call_status_id;
            $lead->rating = $request->rating;
            if ($request->converted) {
                $this->log('edit', $lead->id, 'lead', 'converted', 'Đã nộp hồ sơ', 'Chưa nộp', 'Chưa nộp', 'Đã nộp', 'Đã nộp');
                //$this->log($lead, $request,
                $lead->converted = $request->converted;
                $account = Account::firstOrNew(['lead_id' => $id]);
                //$account->lead_id = $lead->id;
                //var_dump($account);return;
                $account->name = $lead->name;
                $account->province = $lead->province_id;
                $account->birthdate = $lead->birthdate;
                $account->status = $lead->status;
                $account->save();
            } else
                $lead->converted = 0;
            if (!empty($request->birthdate) && !Utils::validateDisplayBirthdate($request->birthdate)) {
                $success = false;
                $return_message = 'Ngày sinh không hợp lệ!';
            }
            if ($success !== FALSE) {
                $saved = $lead->save();
                if ($saved) {
                    $success = true;
                    $return_message = 'Đã lưu thành công!';
                } else {
                    $success = true;
                    $return_message = 'Đã có lỗi trong quá trình lưu dữ liệu!';
                }
            }
        }
//        else {
//            $account = Account::firstOrNew(['lead_id' => $id]);
//            $account->name = $request->name;
//            $account->province = $request->province;
//            $account->gender = $request->gender;
//            $account->birthdate = Carbon::createFromFormat('d/m/Y', $request->birthdate)->format('Y-m-d');
//            $account->national_id = $request->national_id;
//            $account->status = $request->status;
//            $account->save();
//        }
        return view('includes.lead.edit', ['id' => $id, 'lead' => $lead, 'account' => $account, 'status_list' => $status_list, 'source_list' => $source_list, 'department_list' => $department_list, 'group_list' => $group_list, 'province_list' => $province_list, 'account_status_list' => $account_status_list, 'major_list' => $major_list, 'campaign' => $campaign_list, 'account_financial_status_list' => $account_financial_status_list, 'payment_method_list' => $payment_method_list, 'receiving_place_list' => $receiving_place_list, 'change_log' => $change_log, 'exam_place_list' => $exam_place_list, 'campus_list' => $campus_list, 'assignee_list' => $assignee_list, 'call_status_list' => $call_status_list, 'rating_list' => $rating_list, 'success' => $success, 'return_message' => $return_message]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {

    }

    public
    function export()
    {
        $objPHPExcel = new PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
            ->setLastModifiedBy("Maarten Balliauw")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Test result file");


        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Hello')
            ->setCellValue('B2', 'world!')
            ->setCellValue('C1', 'Hello')
            ->setCellValue('D2', 'world!');

        // Miscellaneous glyphs, UTF-8
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A4', 'Miscellaneous glyphs')
            ->setCellValue('A5', 'Nguyễn Thảnh Nghiềng');

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Simple');


        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="01simple.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }

    public
    function getNameFromNumber($number)
    {
        $lead = DB::table('fptu_lead')->where('phone1', $number)->orWhere('phone2', $number)->orWhere('phone3', $number)->orWhere('phone4', $number)->first();
        if (!empty($lead))
            return $lead->name;
        else
            return $number;
    }

    public
    function clearFiltering()
    {
        $filtering = UserPref::where(['user_id' => Auth::id(), 'key' => 'filtering'])->first();
        if (!empty($filtering))
            $deleted = UserPref::where(['user_id' => Auth::id(), 'key' => 'filtering'])->delete();
        else
            return 'ok';
        if ($deleted)
            return 'ok';
        return 'error';
    }

    public
    function testDelete()
    {
        $lead = Lead::find(270);
        if (!empty($phone1))
            $phone_list[] = $phone1;
        if (!empty($phone2))
            $phone_list[] = $phone2;
        if (!empty($phone3))
            $phone_list[] = $phone3;
        if (!empty($phone4))
            $phone_list[] = $phone4;
        $dup_lead = null;
        $phone_list = array('0989645143');
        foreach ($phone_list as $phone) {
            $dup_lead = Lead::where('phone1', $phone)
                ->orWhere('phone2', $phone)
                ->orWhere('phone3', $phone)
                ->orWhere('phone4', $phone)
                ->get();
        }
        if (!empty($dup_lead) && ($dup_lead->count() > 0)) {
            if ($lead->status == LeadStatus::ACTIVE_LEAD && $dup_lead->status != LeadStatus::ACTIVE_LEAD) {
                $dup_lead->master_id = $lead->id;
                $lead->dup_id = $
                $dup_lead->delete();
            }
        }
    }


//    public function assignLead($lead_id, $user_id)
//    {
//        $existing = ManualLeadAssigner::where($lead_id, $user_id)->count("id");
//        if (empty($existing)) {
//            $assigner = new ManualLeadAssigner();
//            $assigner->user_id = $user_id;
//            $assigner->lead_id = $lead_id;
//            $assigner->save();
//        }
//    }


}
