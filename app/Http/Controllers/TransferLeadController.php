<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 24/03/2017
 * Time: 2:32 PM
 */

namespace App\Http\Controllers;


use Illuminate\Http\Request;

use App\Weight;
use App\User;
use App\DataList;
use App\Campus;
use App\Lead;
use Illuminate\Support\Facades\DB;

class TransferLeadController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showList($department_id)
    {
        $leads = DB::table('fptu_lead')
            ->leftJoin('fptu_province', 'fptu_lead.province_id', '=', 'fptu_province.id')
            ->leftJoin('fptu_school', 'fptu_lead.school', '=', 'fptu_school.id')
            ->select('fptu_lead.name', 'fptu_lead.phone1', 'fptu_lead.email', 'fptu_lead.facebook', 'fptu_lead.birthdate', 'fptu_province.name as province_name', 'fptu_lead.school_other', 'fptu_school.name as school_name')
            ->where('fptu_lead.department_id', $department_id)
            ->whereNull('fptu_lead.deleted_at')
            ->where('fptu_lead.notified', 0)
            ->get();
        ?>
        <style type="text/css">
            table, th, td {
                border: 1px solid #000;
                border-collapse: collapse;
            }
        </style>
        <table>
            <thead>
            <th>STT</th>
            <th>Họ & tên</th>
            <th>SĐT</th>
            <th>Email</th>
            <th>Facebook</th>
            <th>Ngày sinh</th>
            <th>Tỉnh</th>
            <th>Trường</th>
            </thead>
            <tbody>
            <?php
            $stt = 0;
            foreach ($leads as $lead):
                ?>
                <tr>
                    <td><?php echo ++$stt; ?></td>
                    <td><?php echo !empty($lead->name) ? $lead->name : ''; ?></td>
                    <td><?php echo $lead->phone1; ?></td>
                    <td><?php echo !empty($lead->email) ? $lead->email : ''; ?></td>
                    <td><?php echo !empty($lead->facebook) ? $lead->facebook : ''; ?></td>
                    <td><?php echo !empty($lead->birthdate) ? $lead->birthdate : ''; ?></td>
                    <td><?php echo !empty($lead->province_name) ? $lead->province_name : ''; ?></td>
                    <td>
                        <?php echo !empty($lead->school_name) ? $lead->school_name : $lead->school_other; ?>
                    </td>
                </tr>
                <?php
            endforeach;
            ?>
            </tbody>
        </table>
        <form method="post" action="<?php echo route('transfer.notified'); ?>">
            <?php echo csrf_field(); ?>
            <button type="submit" name="send" value="sent">Gửi</button>
        </form>
        <?php
    }
}
