<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AvailableClass extends Model
{
    protected $table = 'fptu_major';
    protected $fillable = [
        'name', 'hide'
    ];
}
