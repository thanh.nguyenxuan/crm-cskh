<?php

namespace App\Suspect;

use App\Department;
use App\Lead;
use App\LeadSource;
use App\LeadStatus;
use App\ManualLeadAssigner;
use App\SuspectStatus;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Suspect extends Model
{
    protected $table = 'edf_suspect';

    public function checkDuplicate($number, $department_id)
    {
        if (!empty($number)) {
            $count = Suspect::where('id', '!=', $this->id)
                ->where(function ($count) use ($number) {
                    $count->where('phone1', 'LIKE', $number)->orWhere('phone2', 'LIKE', $number);
                })
                ->where('department_id', $department_id)
                ->count();
            if ($count > 0)
                return true;
        }
        return false;
    }

    public static function search()
    {
        $query = DB::table('edf_suspect AS t');

        if(Auth::user()->hasRole('holding') || (Auth::user()->hasRole('admin') && in_array(Department::HOLDING, Auth::user()->getManageDepartments()))){

        }else{
            $query->where('department_id', Auth::user()->getManageDepartments());
        }

        if(!empty($_REQUEST['name'])){
            $query->where('t.name', 'LIKE', '%'.$_REQUEST['name'].'%');
        }
        if(!empty($_REQUEST['phone'])){
            $phone = $_REQUEST['phone'];
            $query->where(function ($sub_query) use ($phone){
                $sub_query->where('t.phone1', 'LIKE', '%'.$phone.'%')
                    ->orWhere('t.phone2', 'LIKE', '%'.$phone.'%')
                ;
            });
        }
        if(!empty($_REQUEST['email'])){
            $query->where('t.email', 'LIKE', '%'.$_REQUEST['email'].'%');
        }
        if(!empty($_REQUEST['source'])){
            $query->where('t.source', $_REQUEST['source']);
        }
        if(!empty($_REQUEST['status'])){
            $query->where('t.status', $_REQUEST['status']);
        }
        if(!empty($_REQUEST['department_id'])){
            $query->where('t.department_id', $_REQUEST['department_id']);
        }
        if(!empty($_REQUEST['assignee'])){
            if($_REQUEST['assignee'] == -1){
                $query->whereNull('t.assignee');
            }else{
                $query->where('t.assignee', $_REQUEST['assignee']);
            }
        }
        if (!empty($_REQUEST['start_date'])) {
            $start_date = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['start_date']))) . ' 00:00:00';
            $query->where('t.created_at', '>=', $start_date);
        }
        if (!empty($_REQUEST['end_date'])) {
            $end_date = date('Y-m-d', strtotime(str_replace('/', '-', $_REQUEST['end_date']))) . ' 23:59:59';
            $query->where('t.created_at', '<=', $end_date);
        }
        $query->orderBy('t.id', 'DESC');

        return $query->get();
    }

    public function convertToLead()
    {
        $return = array(
            'success' => TRUE,
            'message' => '',
            'data'    => []
        );

        if($this->status != SuspectStatus::STATUS_CONVERTED){

            $lead = new Lead();
            $lead->name                 = $this->name;
            $lead->phone1               = $this->phone1;
            $lead->phone2               = $this->phone2;
            $lead->email                = $this->email;
            $lead->department_id        = $this->department_id;
            $lead->status               = LeadStatus::STATUS_NEW;

            if($lead->checkDuplicate($lead->phone1) || $lead->checkDuplicate($lead->phone2)){
                $return['success'] = FALSE;
                $return['message'] = 'Lead đã tồn tại ở cơ sở này';
            }else{
                if($lead->save()){
                    $lead_source = new LeadSource;
                    $lead_source->lead_id = $lead->id;
                    $lead_source->source_id = $this->source;
                    $lead_source->created_by = 4;
                    $lead_source->created_at = date('Y-m-d H:i:s');
                    $lead_source->save();

                    if(empty($this->assignee) && in_array($lead->department_id, [25])){
                        $this->assignee = $lead->getAssignee();
                    }

                    if(!empty($this->assignee)){
                        $assigner = new ManualLeadAssigner();
                        $assigner->user_id = $this->assignee;
                        $assigner->lead_id = $lead->id;
                        $assigner->created_by = 4;
                        $assigner->save();
                    }

                    $this->status = SuspectStatus::STATUS_CONVERTED;
                    $this->convert = 1;
                    $this->converted_at = date('Y-m-d H:i:s');
                    $this->lead_id = $lead->id;
                    $this->save();
                }else{
                    $return['success'] = FALSE;
                    $return['message'] = 'Chuyển đổi thất bại';
                };
            }
        }else{
            $return['success'] = FALSE;
            $return['message'] = 'Không tìm thấy dữ liệu';
        }
        echo json_encode($return);
    }
}
