<?php

namespace App\DiemThiTHPT;

use Illuminate\Database\Eloquent\Model;

class Grade extends Model
{
    protected $table = 'nvfpt';
    protected $primaryKey = 'id';
}
