<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComplainReason extends Model
{
    protected $table = 'complain_reason';

    public function isChecked($requestData, $leadData)
    {
        $checked = false;
        if(!empty($requestData)){
            if(in_array($this->id, $requestData)){
                $checked = TRUE;
            }
        }else if(!empty($leadData)){
            if(in_array($this->id, $leadData)){
                $checked = TRUE;
            }
        }
        return $checked;
    }

    public static function listData()
    {
        return self::all()->pluck('name', 'id')->toArray();
    }
}
