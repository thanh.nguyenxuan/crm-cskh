<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Major extends Model
{
    protected $table = 'fptu_major';

    public static function getSchoolLevel($id)
    {
        if(!empty($id)){
            if(in_array($id, array(1,2))){
                return 'Mầm non';
            }
            if(in_array($id, array(4,5,6,7,8))){
                return 'Tiểu học';
            }
            if(in_array($id, array(9,10,11,12))){
                return 'Trung học cơ sở';
            }
            if(in_array($id, array(13,14,15))){
                return 'Trung học phổ thông';
            }
        }
        return '';
    }
}
