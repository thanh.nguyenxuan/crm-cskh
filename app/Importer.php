<?php

namespace App;

use App\LeadStatus;
use Illuminate\Support\Facades\Mail;
use PHPExcel;
use PHPExcel_IOFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\ManualLeadAssigner;
use App\Major;
use App\CallStatus;
use App\Province;
use App\District;


class Importer
{
    private static $marTViet = array("à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ", "ì", "í", "ị", "ỉ", "ĩ", "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ", "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ", "ỳ", "ý", "ỵ", "ỷ", "ỹ", "đ", "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ", "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ", "Ì", "Í", "Ị", "Ỉ", "Ĩ", "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ", "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ", "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ", "Đ");
    private static $marKoDau = array("a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "i", "i", "i", "i", "i", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "y", "y", "y", "y", "y", "d", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "I", "I", "I", "I", "I", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "Y", "Y", "Y", "Y", "Y", "D");
    private static $tohop = array("à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ", "ì", "í", "ị", "ỉ", "ĩ", "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ", "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ", "ỳ", "ý", "ỵ", "ỷ", "ỹ", "đ", "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ", "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ", "Ì", "Í", "Ị", "Ỉ", "Ĩ", "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ", "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ", "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ", "Đ");

    function proper_name($string, $encoding = 'UTF-8')
    {
        $return_string = mb_strtolower($string, $encoding);
        $words = explode(' ', $return_string);
        foreach ($words as $index => $word) {
            $words[$index] = mb_strtoupper(mb_substr($word, 0, 1, $encoding), $encoding) . mb_substr($word, 1, mb_strlen($word, $encoding), $encoding);
        }
        $return_string = implode(' ', $words);
        return $return_string;
    }

    function prepare_name($name)
    {
        $name = str_replace(self::$tohop, self::$marTViet, $name);
        $name = $this->proper_name(mb_strtolower($name, 'UTF-8'));
        return $name;
    }

    function validate_phong($phong)
    {
        $phong_list = array('HN', 'DN', 'HCM', 'TT', 'CT');
        if (in_array($phong, $phong_list))
            return true;
        return false;
    }

    function validate_phone($phone)
    {
        if(!empty($phone)){
            $pattern = '/^[\d]{8,15}$/';
            if(!preg_match($pattern, $phone)){
                return FALSE;
            }
        }
        return TRUE;
    }

    function romanize_string($string)
    {
        $string = trim($string);
        $string = str_replace(self::$marTViet, self::$marKoDau, $string);
        $string = preg_replace('/\./', '', $string);
        return $string;
    }

    function legacy_romanize_string($string)
    {
        $string = trim($string);
        $string = str_replace(self::$marTViet, self::$marKoDau, $string);
        $string = preg_replace("/[^A-Za-z0-9 ]/", '', $string);
        return $string;
    }

    function sanitize_province($province)
    {
        $province = $this->romanize_string($province);
        $province = str_replace(' ', '', $province);
        $province = str_replace('-', '', $province);
        $province = str_replace('TP.', '', $province);
        switch ($province) {
            case 'TPHOCHIMINH':
                $province = 'HOCHIMINH';
                break;
            case 'VUNGTAU':
                $province = 'BARIAVUNGTAU';
                break;
            case 'HUE':
                $province = 'THUATHIENHUE';
                break;
            case 'DACLAC':
                $province = 'DAKLAK';
                break;
            case 'DACNONG':
                $province = 'DAKNONG';
                break;
        }
        return strtoupper($province);
    }

    function validate_district($district)
    {
        $district = trim($district);
        $count = DB::table('edf_district')->where('name', $district)->count();
        if ($count)
            return true;
        return false;
    }

    function validate_province($province)
    {
        ;
        $province = $this->sanitize_province($province);
        $count = DB::table('fptu_province')->where('name_upper', $province)->count();
        //var_dump(DB::table('fptu_province')->where('name_upper', $province)->toSql());
        //var_dump($province);
        if ($count)
            return true;
        return false;
    }

    function validate_source($source)
    {
        $source_check = DB::table('fptu_source')->where('name', $source);
        if ($source_check->count())
            return true;
        return false;
    }

    function validate_channel($channel)
    {
        $channel_check = DB::table('fptu_channel')->where('name', $channel);
        if ($channel_check->count())
            return true;
        return false;
    }

    function validate_campaign($campaign)
    {
        $campaign_check = DB::table('fptu_campaign')->where('name', $campaign);
        if ($campaign_check->count())
            return true;
        return false;
    }

    function validate_group($group_name)
    {
        $group_check = DB::table('fptu_group')->where('name', $group_name);
        if ($group_check->count())
            return true;
        return false;
    }

    function validate_campus($campus_code)
    {
        $campus_code = trim(strtoupper($campus_code));
        $check = DB::table('fptu_campus')->where('code', $campus_code)->count();
        if ($check)
            return true;
        return false;
    }

    function validate_assignee($assignee)
    {
        $count = DB::table('fptu_user')->where('username', $assignee)->count();
        if ($count)
            return true;
        return false;
    }

    function check_duplicate_phone($number)
    {
        $count = DB::table('fptu_lead')->where('phone1', $number)->orWhere('phone2', $number)->count();
        if ($count > 0)
            return true;
        return false;
    }

    function validate_date($date_string, $format = 'd/m/Y')
    {
        $day = 0;
        $month = 0;
        $year = 0;
        switch ($format) {
            case 'd/m/Y':
                $date_chunks = explode('/', $date_string);
                if (count($date_chunks) == 3) {
                    $day = intval(trim($date_chunks[0]));
                    $month = intval(trim($date_chunks[1]));
                    $year = intval(trim($date_chunks[2]));
                }
                break;
            case 'd-m-Y':
                $date_chunks = explode('-', $date_string);
                if (count($date_chunks) == 3) {
                    $day = intval(trim($date_chunks[0]));
                    $month = intval(trim($date_chunks[1]));
                    $year = intval(trim($date_chunks[2]));
                }
                break;
            case 'Y-m-d':
                $date_chunks = explode('-', $date_string);
                if (count($date_chunks) == 3) {
                    $day = intval(trim($date_chunks[2]));
                    $month = intval(trim($date_chunks[1]));
                    $year = intval(trim($date_chunks[0]));
                }
                break;
        }
        if ($year < 1000 || $year > 9999)
            return false;
        return checkdate($month, $day, $year);
    }

    function parse_date($date_string)
    {
        $day = 0;
        $month = 0;
        $year = 0;
        if (substr_count($date_string, '-') == 2) {
            $date_chunks = explode('-', $date_string);
            if (count($date_chunks) == 3) {
                $day = intval(trim($date_chunks[1]));
                $month = intval(trim($date_chunks[0]));
                $year = intval(trim($date_chunks[2]));
            }
        }
        if (substr_count($date_string, '/') == 2) {
            $date_chunks = explode('/', $date_string);
            if (count($date_chunks) == 3) {
                $day = intval(trim($date_chunks[0]));
                $month = intval(trim($date_chunks[1]));
                $year = intval(trim($date_chunks[2]));

            }
        }
        if ($year <= 99) {
            if ($year >= 45) {
                $year = 1900 + $year;
            } else {
                $year = 2000 + $year;
            }
        }
        if (checkdate($month, $day, $year))
            return $year . '-' . $month . '-' . $day;

        return '';

    }

    function import_row($row)
    {
        $message = '';
        $status = 'ok';
        $data = array();
        $created_at = trim($row[1]);
        if (!empty($created_at)) {
            if (!Utils::validateDate($created_at, 'd/m/Y') && !Utils::validateDate($created_at, 'd/m/Y H:i:s')) {
                $message .= 'Ngày tạo lead \'<strong>' . $created_at . '</strong>\' không hợp lệ. ';
                $status = 'failed';
            } else {
                if (Utils::validateDate($created_at, 'd/m/Y'))
                    $created_at = Utils::convertDate($created_at, 'd/m/Y', 'Y-m-d H:i:s');
                elseif (Utils::validateDate($created_at, 'd/m/Y H:i:s'))
                    $created_at = Utils::convertDate($created_at, 'd/m/Y H:i:s', 'Y-m-d H:i:s');
            }
        }
        //dd($created_at);
        $department = strtoupper(trim($row[2]));
        $duplicate_message = '';
        $source = strtoupper(trim($row[3]));

        if (empty($source) || !$this->validate_source($source)) {
            $message .= 'Nguồn dẫn \'<strong>' . $source . '</strong>\' không hợp lệ. ';
            $status = 'failed';
        }
        $channel = trim($row[4]);
        if (!empty($channel) && !$this->validate_channel($channel)) {
            $message .= 'Campaign \'<strong>' . $channel . '</strong>\' không hợp lệ. ';
            $status = 'failed';
        }
        $campaign = trim($row[5]);
        if (!empty($campaign) && !$this->validate_campaign($campaign)) {
            $message .= 'Chiến dịch quảng cáo \'<strong>' . $campaign . '</strong>\' không hợp lệ. ';
            $status = 'failed';
        }
        $keyword_string = trim($row[6]);
        $keyword_id = 0;
        if (!empty($keyword_string)) {
            $keyword = Keyword::firstOrCreate(['name' => $keyword_string]);
            $keyword->created_by = Auth::id();
            $keyword_id = $keyword->id;
        }
        $name = trim($row[7]);
        $student_name = trim($row[8]);
        $nick_name = trim($row[9]);
        $gender = strtolower(trim($row[10]));
        switch ($gender) {
            case 'nam':
            case 'male':
                $gender = 'm';
                break;
            case 'nữ':
            case 'female':
                $gender = 'f';
                break;
        }
        $birthdate = trim($row[11]);
        if (!empty($birthdate)) {
            if (!Utils::validateDate($birthdate, 'd/m/Y')) {
                $message .= 'Ngày sinh \'<strong>' . $birthdate . '</strong>\' không hợp lệ. ';
                $status = 'failed';
            } else {
                $birthdate = Utils::convertDate($birthdate, 'd/m/Y', 'Y-m-d');
            }
        }
        $mobile = Utils::sanitizePhoneV2($row[12]);
        $parent1_phone = Utils::sanitizePhoneV2($row[13]);
        $twins = (trim($row[28]) == "Có") ? 1 : 0;
        $department_id = Department::where('name', $department)->first()->id;
        if(!in_array(Department::HOLDING, Auth::user()->getManageDepartments())
            && !Auth::user()->hasRole('holding')
            && !in_array($department_id, Auth::user()->getManageDepartments())
        ){
            $message .= 'Cơ sở không chính xác. ';
            $status = 'failed';
        }
        if(empty($name)){
            $message .= 'Thiếu họ tên phụ huynh. ';
            $status = 'failed';
        }
        if (empty($mobile) && empty($parent1_phone)) {
            $message .= 'Thiếu cả 2 số điện thoại liên hệ. ';
            $status = 'failed';
        } elseif (!$this->validate_phone($mobile)) {
            $message .= 'SĐT 1 <strong>' . $mobile . '</strong> không hợp lệ!';
            $status = 'failed';
        } elseif (!$this->validate_phone($parent1_phone)){
            $message .= 'SĐT 2 <strong>' . $parent1_phone . '</strong> không hợp lệ!';
            $status = 'failed';
        } else {
            $number = null;
            if (!empty($mobile) && empty($twins)) {
                if (Utils::checkDuplicatePhone($mobile, $department_id)) {
                    $message .= 'Số điện thoại chính \'<strong>' . $mobile . '</strong>\' bị trùng. ';
                    $status = 'dup';
                    $number = $mobile;
                }
            }
            if (!empty($parent1_phone) && empty($twins)) {
                if (Utils::checkDuplicatePhone($parent1_phone, $department_id)) {
                    $message .= 'Số điện thoại phụ \'<strong>' . $parent1_phone . '</strong>\' bị trùng. ';
                    $status = 'dup';
                    $number = $parent1_phone;
                }
            }

            if($status == 'dup'){
                //notify
                $lead_duplicate = Lead::where(function ($query) use ($number) {
                    $query->where('phone1', $number)
                        ->orWhere('phone2', $number);
                })
                    ->whereNull('deleted_at')
                    ->where('department_id', $department_id)
                    ->first();
                $user = User::getLeadAssignee($lead_duplicate->id);
                if($user && $user->active == 1){
                    $notify = new Notify();
                    $notify->content = "Lead \"{$name}\" trùng, vừa được import bởi ".Auth::user()->username;
                    $notify->content.= "<br/>Chi tiết: " .env('APP_URL') ."/lead/edit/$lead_duplicate->id";
                    $notify->user_id = $user->id;
                    $notify->status = Notify::STATUS_NEW;
                    $notify->created_at = date('Y-m-d H:i:s');
                    $notify->lead_id = $lead_duplicate->id;
                    $notify->lead_duplicate = 1;
                    $notify->type = Notify::TYPE_IMPORT_LEAD_ASSIGNMENT;
                    $notify->save();
                }
            }

            $phone_list = array();
            if (!empty($mobile))
                $phone_list[] = $mobile;
            if (!empty($parent1_phone))
                $phone_list[] = $parent1_phone;
            //$lead_check = Lead::whereIn('phone1', $phone_list)->orWhereIn('phone2', $phone_list);
            $lead_check = Lead::where(function ($query) use ($phone_list) {
                $query->whereIn('phone1', $phone_list)
                    ->orWhereIn('phone2', $phone_list);
                })
                ->whereNull('deleted_at')
                ->where('department_id', $department_id)
                ->whereRaw('leave_school IS NULL OR leave_school != 1');
            if ($lead_check->count() > 0 && empty($twins)) {
                $lead_check = $lead_check->first();
                // get assignment list
                if ($lead_check->status == 5) {
                    if (!empty($lead_check->activator)) {
                        $activator_username = User::find($lead_check->activator)->username;
                        $duplicate_message = $activator_username;
                    }
                } else {
                    $assignment_list = DB::table('fptu_user')
                        ->join('fptu_lead_assignment', 'fptu_user.id', '=', 'fptu_lead_assignment.user_id')
                        ->where('fptu_lead_assignment.lead_id', $lead_check->id)
                        ->groupBy('fptu_lead_assignment.lead_id')
                        ->select(DB::raw('group_concat(fptu_user.username) as user_list'))
                        ->whereNull('fptu_user.deleted_at')
                        ->whereNull('fptu_lead_assignment.deleted_at')
                        ->first();
                    if (!empty($assignment_list) || !empty($assignment_list->user_list)) {
                        $duplicate_message = $assignment_list->user_list;
                    }
                }
                //var_dump($lead->import_count);
                /*if (!empty($lead_check->import_count)) {
                    $lead_check->import_count = $lead_check->import_count + 1;
                    //var_dump($lead_check->import_count);
                } else
                    $lead_check->import_count = 1;*/
                if (!empty($source)) {
                    $source_object = Source::where('name', $source)->first();
                    if (!empty($source_object) && !empty($source_object->id)) {
                        $source_id = $source_object->id;
                    }
                }
                if (!empty($channel)) {
                    $channel_id = Channel::where('name', $channel)->first();
                    if (!empty($channel_id) && $channel_id->id) {
                        $channel_id = $channel_id->id;
                    } else {
                        $channel_id = '';
                    }
                }
                if (!empty($campaign) && !empty(Campaign::where('name', $campaign)->first()->id))
                    $campaign_id = Campaign::where('name', $campaign)->first()->id;
                if (!empty($source_id)) {
                    $lead_source = new LeadSource;
                    $lead_source->lead_id = $lead_check->id;
                    $lead_source->source_id = $source_id;
                    $lead_source->created_by = Auth::id();
                    $lead_source->save();
                }
                if (!empty($channel_id)) {
                    $lead_channel = new LeadChannel;
                    $lead_channel->lead_id = $lead_check->id;
                    $lead_channel->channel_id = $channel_id;
                    //$lead_channel->created_by = Auth::id();
                    $lead_channel->save();
                }
                if (!empty($campaign_id)) {
                    $lead_campaign = new LeadCampaign;
                    $lead_campaign->lead_id = $lead_check->id;
                    $lead_campaign->campaign_id = $campaign_id;
                    //$lead_campaign->created_by = Auth::id();
                    $lead_campaign->save();
                }else{
                    if(!empty($source_id) && in_array($source_id, Source::onlineSource())){
                        $campaign_id = 1;
                        if($campaign_id){
                            $lead_campaign = LeadCampaign::whereNull('deleted_at')
                                ->where('lead_id',$lead_check->id)
                                ->first();
                            if(!$lead_campaign){
                                $lead_campaign = new LeadCampaign();
                                $lead_campaign->lead_id = $lead_check->id;
                                $lead_campaign->campaign_id = $campaign_id;
                                $lead_campaign->created_at = date('Y-m-d H:i:s');
                                $lead_campaign->save();
                            };
                        }
                    }
                }
                if (!empty($keyword_id)) {
                    $lead_keyword = new LeadKeyword;
                    $lead_keyword->lead_id = $lead_check->id;
                    $lead_keyword->keyword_id = $keyword_id;
                    $lead_keyword->created_by = Auth::id();
                    $lead_keyword->save();
                }
                if ((empty($lead_check->email) || (strtolower($lead_check->email) == 'chưa có')) && !empty($row[14])) {
                    $lead_check->email = trim($row[14]);
                }
                if ((empty($lead_check->facebook) || (strtolower($lead_check->facebook) == 'chưa có')) && !empty($row[18])) {
                    $lead_check->facebook = trim($row[18]);
                }
                $lead_check->last_duplicate = date('Y-m-d H:i:s');
                $lead_check->new_duplicate = 1;
                $lead_check->notes .= trim($row[25]);
                $lead_check->save();
            }
        }
        $district = trim($row[20]);
        if (!empty($district) && !$this->validate_district($district)) {
            $message .= 'Tên quận/huyện \'<strong>' . $district . '</strong>\' không hợp lệ. ';
            if ($status != 'dup')
                $status = 'failed';

        }
        $province = $this->sanitize_province(trim($row[21]));
        if (!empty($province) && !$this->validate_province($province)) {
            $message .= 'Tên tỉnh \'<strong>' . $province . '</strong>\' không hợp lệ. ';
            if ($status != 'dup')
                $status = 'failed';

        }
        $assignee = trim($row[22]);
        if (!empty($assignee)) {
            if (!$this->validate_assignee($assignee)) {
                $message .= 'Tên đăng nhập của người phụ trách \'<strong>' . $assignee . '</strong>\' không hợp lệ. ';
                if ($status != 'dup')
                    $status = 'failed';
            }
        }
        $district_data = null;
        if (!empty($district)) {
            $district_data = District::where('name', $district)->first();
        }
        $province_data = null;
        if (!empty($province)) {
            $province_data = Province::where('name_upper', $province)->first();
        }
        if (!empty($message)) {
            $data['result'] = $status;
            $data['message'] = $message;
            $data['duplicate_message'] = $duplicate_message;
        } else {
            $data['result'] = 'ok';
            $data['message'] = 'Thành công!';
            $lead = new Lead;
            $lead->name = $this->prepare_name($name);
            $lead->student_name = $this->prepare_name($student_name);
            $lead->nick_name = $this->prepare_name($nick_name);
            if ($gender == 'm' || $gender == 'f')
                $lead->gender = $gender;
            if (!empty($mobile))
                $lead->phone1 = $mobile;
            if (!empty($parent1_phone))
                $lead->phone2 = $parent1_phone;
            $lead->email = $row[14];
            $lead->email_2 = $row[15];
            $status = trim($row[16]);
            if (!empty($status))
                $status_id = LeadStatus::where('name', $status)->first()->id;
            else
                $status_id = 1;
            $lead->status = $status_id;
            $call_status = trim($row[17]);
            if (!empty($call_status))
                $call_status_id = CallStatus::where('name', $call_status)->first()->id;
            $lead->call_status = $call_status_id;
            $lead->facebook = trim($row[18]);
            $lead->address = trim($row[19]);
            $source_id = Source::where('name', $source)->first()->id;
            if (!empty($channel))
                $channel_id = Channel::where('name', $channel)->first()->id;
            if (!empty($campaign))
                $campaign_id = Campaign::where('name', $campaign)->first()->id;
            if (!empty($birthdate))
                $lead->birthdate = $birthdate;
            if (!empty($assignee))
                $user = User::where('username', $assignee)->first();
            switch ($status_id) {
                case LeadStatus::STATUS_SHOWUP_SCHOOL: // show up đến trường
                    $lead->showup_school_at = date('Y-m-d H:i:s');
                    break;
                /*case 8:
                    $lead->showup_seminor_at = date('Y-m-d H:i:s');
                    break;*/
                case LeadStatus::STATUS_WAITLIST: // Đặt cọc giữ chỗ
                    $lead->activator = !empty($user) ? $user->id : Auth::id();
                    $lead->activated_at = date('Y-m-d H:i:s');
                    $lead->activated = 1;
                    $lead->sendRequestOracleERP(LeadStatus::STATUS_WAITLIST);
                    break;
                case LeadStatus::STATUS_ENROLL: // Nhập học
                    $lead->enrolled = 1;
                    $lead->enrolled_at = date('Y-m-d H:i:s');
                    break;
                case LeadStatus::STATUS_NEW_SALE:
                    $lead->new_sale_at = date('Y-m-d H:i:s');
                    $lead->new_sale = 1;
                    $lead->sendRequestOracleERP(LeadStatus::STATUS_NEW_SALE);
                    break;
                case LeadStatus::STATUS_HOT_LEAD:
                    $lead->hot_lead_at = date('Y-m-d H:i:s');
                    break;
            }
            if (!empty($district_data))
                $lead->province_id = $district_data->id;
            if (!empty($province_data))
                $lead->province_id = $province_data->id;
            $lead->department_id = $department_id;
            $major = trim($row[23]);
            if (!empty($major))
                $major = Major::where('name', $major)->first();
            if (!empty($major))
                $lead->major = $major->id;
            $school_name = trim($row[24]);
            if(!empty($school_name)){
                $school = School::where('name', $school_name)->first();
                if(!$school){
                    $school = new School();
                    $school->name = $school_name;
                    $school->province = 0;
                    $school->save();
                }
                $lead->school = $school->id;
            }
            $lead->notes = $row[25];
            $lead->twins = $twins;
            $lead->url_referrer = $row[26];
            $lead->url_request = $row[27];
            //dd($created_at . ' ' . date('H:i:s'));
            if (!empty($created_at))
                $lead->created_at = $created_at;
            $lead->created_by = Auth::user()->id;
            if(!empty($campaign_id)){
                $lead->utm_campaign = $campaign;
            }
            $lead->import_at = date('Y-m-d H:i:s');
            $lead->import_count = 1;
            $lead->save();
            if (!empty($source_id)) {
                $lead_source = new LeadSource;
                $lead_source->lead_id = $lead->id;
                $lead_source->source_id = $source_id;
                $lead_source->created_by = Auth::id();
                $lead_source->save();
            }
            if (!empty($channel_id)) {
                $lead_channel = new LeadChannel;
                $lead_channel->lead_id = $lead->id;
                $lead_channel->channel_id = $channel_id;
                //$lead_channel->created_by = Auth::id();
                $lead_channel->save();
            }
            if (!empty($campaign_id)) {
                $lead_campaign = new LeadCampaign;
                $lead_campaign->lead_id = $lead->id;
                $lead_campaign->campaign_id = $campaign_id;
                //$lead_campaign->created_by = Auth::id();
                $lead_campaign->save();
            }else{
                if(!empty($source_id) && in_array($source_id, Source::onlineSource())){
                    $campaign_id = 1;
                    if($campaign_id){
                        $lead_campaign = new LeadCampaign();
                        $lead_campaign->lead_id = $lead->id;
                        $lead_campaign->campaign_id =$campaign_id;
                        $lead_campaign->created_at = date('Y-m-d H:i:s');
                        $lead_campaign->save();
                    }
                }
            }
            if (!empty($keyword_id)) {
                $lead_keyword = new LeadKeyword;
                $lead_keyword->lead_id = $lead->id;
                $lead_keyword->keyword_id = $keyword_id;
                $lead_keyword->created_by = Auth::id();
                $lead_keyword->save();
            }

            if (!empty($assignee)){
                if (!empty($user) && !empty($lead->id)) {
                    $manual_assigner = new ManualLeadAssigner;
                    $manual_assigner->user_id = $user->id;
                    $manual_assigner->lead_id = $lead->id;
                    $manual_assigner->created_at = date('Y-m-d H:i:s');
                    $manual_assigner->save();
                }
            }else{
                if(!empty($lead->id)){
                    $user = $lead->getAssignee();
                    $manual_assigner = new ManualLeadAssigner;
                    $manual_assigner->user_id = $user->id;
                    $manual_assigner->lead_id = $lead->id;
                    $manual_assigner->created_at = date('Y-m-d H:i:s');
                    $manual_assigner->save();
                }
            }

            $lead->exchangeSuspect();

            if($user && $user->id != Auth::user()->id){
                $notify = new Notify();
                $notify->content = "Lead \"{$lead->name}\" mới được phân công cho bạn bởi ".Auth::user()->username;
                $notify->content.= "<br/>Chi tiết: " .env('APP_URL') ."/lead/edit/$lead->id";
                $notify->user_id = $user->id;
                $notify->status = Notify::STATUS_NEW;
                $notify->created_at = date('Y-m-d H:i:s');
                $notify->lead_id = $lead->id;
                $notify->type = Notify::TYPE_IMPORT_LEAD_ASSIGNMENT;
                $notify->save();

                if(!empty($user->email)){
                    $from       = 'CRM';
                    $to         = $user->email;
                    $subject    = "Thông báo Lead mới";
                    $mail_data       = array(
                        'title'         => $subject,
                        'name'          => $lead->name,
                        'phone'         => $lead->phone1,
                        'created_at'    => date("d/m/Y H:i", strtotime($lead->created_at)),
                        'source'        => Auth::user()->username,
                        'url'           => env('APP_URL').'/lead/edit/'.$lead->id,
                    );
                    $template   = 'email.lead.lead_assign';

                    Mail::send($template, $mail_data, function($message) use ($from, $to, $subject) {
                        $message
                            ->from('crm@edufit.vn',$from)
                            ->to($to)
                            ->subject($subject);
                    });
                }
            }

        }

        return $data;
    }


    function sanitize_phone($mobile)
    {
        $mobile = preg_replace('/[^\d]/', '', $mobile);
        if (substr($mobile, 0, 1) != '0')
            $mobile = '0' . $mobile;
        if ($mobile == '0')
            return '';
        return $mobile;
    }

    function dump_file($row_list = array(), $file_name)
    {
        $template_path = public_path() . '/templates/template_lead_20200426.xlsx';
        $output_path = public_path() . '/uploads/';
        if (file_exists($template_path)) {
            $objPHPExcel = PHPExcel_IOFactory::load($template_path);
            $objPHPExcel->getActiveSheet()->fromArray($row_list, '', 'A2');
            //Storage::disk('local')->put('Failed_Import.xlsx', 'Contents');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
            $objWriter->save($output_path . $file_name);
        } else {
            echo '<div class="alert alert-danger">Lỗi không tìm thấy file template</div>';
        }
    }

    public
    function import($path)
    {
        $now_date = date('Y-m-d H:i:s');
        $now_string = date('YmdHis');
        if (!file_exists($path))
            return '<div class="alert alert-danger">Không tìm thấy file import!</div>';
        ini_set('display_errors', 0);
        //ini_set('error_reporting', E_ALL);
        error_reporting(E_ERROR | E_PARSE);
        $objPHPExcel = PHPExcel_IOFactory::load($path);
        $data = $objPHPExcel->getActiveSheet()->toArray();
        $result = array('message' => '', 'data' => array());
        $first_row = true;
        $row_number = 2;
        $success = 0;
        $failed = 0;
        $failed_row_list = array();
        $dup_row_list = array();
        $success_row_list = array();
        $duplicated = 0;
        $total = count($data) - 1;
        $message = '';
        foreach ($data as $row) {
            if ($first_row) {
                $first_row = false;
                continue;
            }
            $import_result = $this->import_row($row);
            //var_dump($import_result);
            $banner = 'Dòng ' . $row_number . ': ';
            if ($import_result['result'] == 'ok') {
                $success++;
                $message .= '<div class="alert alert-success">' . $banner . 'Thành công!</div>';
                $success_row_list[] = $row;

            } elseif ($import_result['result'] == 'dup') {
                $duplicated++;
                $row[] = strip_tags($import_result['message']);
                $row[] = strip_tags($import_result['duplicate_message']);
                $dup_row_list[] = $row;
                $message .= '<div class="alert alert-warning">' . $banner . $import_result['message'] . '</div>';
            } elseif ($import_result['result'] == 'failed') {
                $failed++;
                $row[] = strip_tags($import_result['message']);
                $failed_row_list[] = $row;
                $message .= '<div class="alert alert-danger">' . $banner . $import_result['message'] . '</div>';
            }
            $row_number++;
        }
        $original_file_name = 'Import_Original_' . $now_string . '.xlsx';
        $original_file_path = public_path() . '/uploads/' . $original_file_name;
        //var_dump($original_file_path);
        //var_dump($path);
        $archived = move_uploaded_file($path, $original_file_path);
        //$archived = true;
        //var_dump($archived);
        if (!$archived)
            $message .= '<div class="alert alert-danger">Đã có lỗi trong quá trình lưu trữ lại file gốc.</div>';
        if ($failed > 0) {
            $error_file_name = 'Import_Error_' . $now_string . '.xlsx';
            $this->dump_file($failed_row_list, $error_file_name);
        }
        if ($duplicated > 0) {
            $dup_file_name = 'Import_Duplicate_' . $now_string . '.xlsx';
            $this->dump_file($dup_row_list, $dup_file_name);
        }
        if ($success > 0) {
            $success_file_name = 'Import_Success_' . $now_string . '.xlsx';
            $this->dump_file($success_row_list, $success_file_name);
        }
        $result['data']['success'] = $success;
        if ($success)
            $result['data']['success_file_name'] = $success_file_name;
        $result['data']['failed'] = $failed;
        if ($failed)
            $result['data']['error_file_name'] = $error_file_name;
        $result['data']['dup'] = $duplicated;
        if ($duplicated)
            $result['data']['duplicate_file_name'] = $dup_file_name;
        $result['data']['total'] = $total;
        $result['message'] = $message;
        $import = new FileImport;
        $import->original_file_name = $original_file_name;
        $import->original_row_num = $total;
        if (!empty($error_file_name)) {
            $import->error_file_name = $error_file_name;
            $import->error_row_num = $failed;
        }
        if (!empty($dup_file_name)) {
            $import->duplicate_file_name = $dup_file_name;
            $import->duplicate_row_num = $duplicated;
        }
        if (!empty($success_file_name)) {
            $import->success_file_name = $success_file_name;
            $import->success_row_num = $success;
        }
        $import->created_at = $now_date;
        $import->created_by = Auth::id();
        $import->save();
        return $result;
    }
}