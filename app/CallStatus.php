<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CallStatus extends Model
{
    protected $table = 'fptu_call_status';
    protected $fillable = array('name');

    CONST STATUS_NO_CONCERN = 6;
    CONST STATUS_QUALIFIED_LEAD = 20;
}
