<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareStatus extends Model
{
    protected $table = 'care_status';


    public static function listData()
    {
        return self::all()->pluck('name', 'id')->toArray();
    }
}
