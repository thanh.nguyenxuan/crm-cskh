<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuspectStatus extends Model
{
    protected $table = 'edf_suspect_status';

    CONST STATUS_NEW            = 1;       //Mới
    CONST STATUS_POTENTIAL      = 2;       //Tiềm năng
    CONST STATUS_WRONG_INFO     = 4;       //Sai thông tin
    CONST STATUS_TAKING_CARE    = 5;       //Đang chăm sóc
    CONST STATUS_REFUSE         = 6;       //Từ chối
    CONST STATUS_CONVERTED      = 10;      //Đã chuyển đổi

    public static function getAllStatus(){
        return LeadStatus::all()->pluck('name', 'id');
    }

    public static function listData()
    {
        return self::where('hide', 0)->whereNull('deleted_at')->orderBy('sort_index', 'ASC')->get()->pluck('name', 'id');
    }

    public static function generateOptions($data, $selected_value, $empty_text)
    {
        $html = "";
        if(!empty($empty_text)){
            $html.= "<option value=''>{$empty_text}</option>";
        }
        foreach ($data as $key => $value){
            $selected = (!empty($selected_value) && $key == $selected_value) ? "selected" : "";
            $color = '';
            /*switch ($key) {
                case self::STATUS_NEW:              $color = '#28a745'; break;
                case self::STATUS_POTENTIAL:        $color = '#007bff'; break;
                case self::STATUS_WRONG_INFO:       $color = '#ffc107'; break;
                case self::STATUS_TAKING_CARE:      $color = '#6c757d'; break;
                case self::STATUS_REFUSE:           $color = '#6c757d'; break;
                case self::STATUS_CONVERTED:        $color = '#6c757d'; break;
            }*/
            $html.= "<option ".(!empty($color) ? "style='color: $color' " : "")." value='{$key}' {$selected}>{$value}</option>";
        }
        return $html;
    }
}
