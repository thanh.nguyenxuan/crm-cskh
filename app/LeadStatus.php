<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use function JmesPath\search;

class LeadStatus extends Model
{
    protected $table = 'fptu_lead_status';
    protected $fillable = array('name');
    const ACTIVE_LEAD = 5;

    CONST STATUS_NEW            = 1;       //Mới
    CONST STATUS_WRONG_INFO     = 3;       //Sai thông tin
    CONST STATUS_NO_DEMAND      = 4;       //Ko có nhu cầu
    CONST STATUS_TAKING_CARE    = 5;       //Đang chăm sóc
    CONST STATUS_REFUSE         = 6;       //Từ chối
    CONST STATUS_SHOWUP_SCHOOL  = 7;       //Đến trường
    CONST STATUS_WAITLIST       = 9;       //Đặt cọc giữ chỗ
    CONST STATUS_CANCEL_DEPOSIT = 14;      //Bỏ cọc
    CONST STATUS_ENROLL         = 15;      //Nhập học
    CONST STATUS_GUEST          = 16;      //Bé làm khách
    CONST STATUS_POTENTIAL      = 17;      //Tiềm năng
    CONST STATUS_NO_ANSWER      = 18;      //Không nghe máy
    CONST STATUS_NEW_SALE       = 19;      //New Sale
    CONST STATUS_ENTRANCE_EXAM_FAIL = 20;  //Không đạt đầu vào
    CONST STATUS_HOT_LEAD       = 21;      //hot lead

    CONST STATUS_QUALIFIED      = 'QL';    //mapping với Trạng thái chăm sóc

    public static function getAllStatus(){
        return LeadStatus::all()->pluck('name', 'id');
    }

    public static function listData()
    {
        return self::where('hide', 0)->whereNull('deleted_at')->orderBy('sort_index', 'ASC')->get()->pluck('name', 'id');
    }

    public static function generateOptions($data, $selected_value, $empty_text)
    {
        $html = "";
        if(!empty($empty_text)){
            $html.= "<option value=''>{$empty_text}</option>";
        }
        foreach ($data as $key => $value){
            $selected = (!empty($selected_value) && $key == $selected_value) ? "selected" : "";
            $color = '';
            switch ($key) {
                case self::STATUS_NEW:              $color = '#28a745'; break;
                case self::STATUS_POTENTIAL:        $color = '#007bff'; break;
                case self::STATUS_WAITLIST:         $color = '#ffc107'; break;
                case self::STATUS_ENROLL:           $color = '#dc3545'; break;
                case self::STATUS_REFUSE:           $color = '#6c757d'; break;
                case self::STATUS_NEW_SALE:         $color = '#6f42c1'; break;
                CASE self::STATUS_HOT_LEAD:         $color = '#d92934'; break;
            }
            $html.= "<option ".(!empty($color) ? "style='color: $color' " : "")." value='{$key}' {$selected}>{$value}</option>";
        }
        return $html;
    }


    public static function listDataLead()
    {
        return self::where('hide', 0)
            ->whereNull('deleted_at')
            ->whereNotIn('id', [
                self::STATUS_HOT_LEAD,
                self::STATUS_SHOWUP_SCHOOL,
                self::STATUS_WAITLIST,
                self::STATUS_NEW_SALE,
                self::STATUS_ENROLL,
            ])
            ->orderBy('sort_index', 'ASC')
            ->get()->pluck('name', 'id');
    }
}
