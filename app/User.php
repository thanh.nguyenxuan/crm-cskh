<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'fptu_user';

    protected $fillable = [
        'username', 'full_name', 'email', 'password', 'extension', 'department_id', 'online', 'active', 'password_expired'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function getLeadAssignee($lead_id)
    {
        $user = User::whereRaw("id IN (SELECT user_id FROM fptu_lead_assignment WHERE lead_id = '$lead_id' AND deleted_at IS NULL)")
            ->where('active', 1)
            ->first();
        if(!$user){
            $user = User::whereRaw("id = (SELECT created_by FROM fptu_lead WHERE id = '$lead_id' AND deleted_at IS NULL)")
                ->where('active', 1)
                ->first();
        }
        return $user;
    }

    public function isSMISUser()
    {
        $departments = $this->getManageDepartments();
        if(in_array(Department::HOLDING, $departments)){
            return FALSE;
        }
        foreach ($departments as $department_id){
            if(in_array($department_id, array(
                1,  //SMIS-HN-CG-DV
                3,  //SMIS-HN-BĐ-TK
                4,  //SMIS-HN-HBT-LY
                5,  //SMIS-HN-HĐ-CA
                6,  //SMIS-HP-NQ-LHP
                7,  //SMIS-HP-NQ-VC
                11, //SMIS-QN-HL-NTH
                12, //SMIS-HCM-Q2-AK
                13, //IMS-TB-TL-HTC
                15, //SMIS-HP-DK-AD
                17, //SMIS-HN-TX-NT
                18, //SMIS-HN-TL-THT
                19, //IMS-HN-NTL-TH
            ))) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function isGISUser()
    {
        $departments = $this->getManageDepartments();
        if(in_array(Department::HOLDING, $departments)){
            return FALSE;
        }
        foreach ($departments as $department_id){
            if(in_array($department_id, array(
                2,  //GIS-HN-CG-DV
                8,  //GIS-HP-DK-AD
                16, //GIS-HN-TL-THT
            ))) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function isASCUser()
    {
        $departments = $this->getManageDepartments();
        if(in_array(Department::HOLDING, $departments)){
            return FALSE;
        }
        foreach ($departments as $department_id){
            if(in_array($department_id, array(
                20,  //ASC-AquaTots-CG
                21,  //ASC-AquaTots-DK
                22,  //ASC-Jacpa
                25,  //ASC-AquaTots-THT
            ))) {
                return TRUE;
            }
        }
        return FALSE;
    }


    public static function getListUserSMIS($department_id = null)
    {
        $list_department_id = array(
            1,  //SMIS-HN-CG-DV
            3,  //SMIS-HN-BĐ-TK
            4,  //SMIS-HN-HBT-LY
            5,  //SMIS-HN-HĐ-CA
            6,  //SMIS-HP-NQ-LHP
            7,  //SMIS-HP-NQ-VC
            11, //SMIS-QN-HL-NTH
            12, //SMIS-HCM-Q2-AK
            13, //IMS-TB-TL-HTC
            15, //SMIS-HP-DK-AD
            17, //SMIS-HN-TX-NT
            18, //SMIS-HN-TL-THT
            19, //IMS-HN-NTL-TH
        );
        $query = User::where('active', 1)->whereNull('deleted_at')
            ->select(DB::raw("CONCAT(username,' - ', full_name) as username"), 'id')
            ->orderBy('username', 'ASC');
        if(!empty($department_id)){
            $query->where('department_id', $department_id);
        }else{
            $query->whereIn('department_id', $list_department_id);
        }
        $data = $query->pluck('username', 'id')->toArray();

        return $data;
    }

    public static function getListUserGIS($department_id = null)
    {
        $list_department_id = array(
            2,  //GIS-HN-CG-DV
            8,  //GIS-HP-DK-AD
            16, //GIS-HN-TL-THT
        );
        $query = User::where('active', 1)->whereNull('deleted_at')
            ->select(DB::raw("CONCAT(username,' - ', full_name) as username"), 'id')
            ->orderBy('username', 'ASC');
        if(!empty($department_id)){
            $query->where('department_id', $department_id);
        }else{
            $query->whereIn('department_id', $list_department_id);
        }
        $data = $query->pluck('username', 'id')->toArray();

        return $data;
    }

    public static function getOptionsUserSMIS($selected_value = null, $department_id = null){
        return Utils::generateOptions(self::getListUserSMIS($department_id), $selected_value, 'Tất cả');
    }

    public static function getOptionsUserGIS($selected_value = null, $department_id = null){
        return Utils::generateOptions(self::getListUserGIS($department_id), $selected_value, 'Tất cả');
    }

    public static function getListUserByDepartmentGroup($department_group, $only_telesale = FALSE){
        $data = array();
        $list_department = Department::getListDepartmentId($department_group);
        if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding')) {
            if(!empty($list_department)){
                $departments_str = implode(',', $list_department);
                $query = User::where('active', 1)->whereNull('deleted_at')
                    ->whereRaw("(department_id IN ($departments_str)
                        OR id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                        OR id IN (SELECT user_id FROM group_user WHERE group_id IN (
                            SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                        ))
                    )");
            }else{
                $query = User::where('active', 1)->whereNull('deleted_at');
            }
        }else{
            $departments_str = implode(',', Auth::user()->getManageDepartments());
            $query = User::where('active', 1)->whereNull('deleted_at')
                ->whereRaw("(department_id IN ($departments_str)
                    OR id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                    OR id IN (SELECT user_id FROM group_user WHERE group_id IN (
                        SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                    ))
                )");
        }
        if($only_telesale){
            $query->whereHas('roles', function ($q) {
                $q->whereIn('name', array('telesales','teleteamleader'));
            });
        }
        $data = $query->select(DB::raw("CONCAT(username,' - ', full_name) as username"), 'id')->orderBy('username', 'ASC')->pluck('username', 'id')->toArray();

        return $data;
    }

    public static function getOptionsUserByGroupDepartment($selected_value = null, $department_group = null, $only_telesales = TRUE){
        return Utils::generateOptions(self::getListUserByDepartmentGroup($department_group, $only_telesales), $selected_value, 'Tất cả');
    }

    public static function getListUserByDepartment($department_id, $only_telesale = FALSE){
        $data = array();
        $query = User::where('active', 1)->whereNull('deleted_at');
        if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding')) {
            if($department_id){
                $query->whereRaw("(department_id = $department_id
                    OR id IN (SELECT user_id FROM user_department WHERE department_id = $department_id)
                    OR id IN (SELECT user_id FROM group_user WHERE group_id IN (
                        SELECT group_id FROM group_department WHERE department_id = $department_id
                    ))
                )");
            }
        }else{
            $departments_str = implode(',', Auth::user()->getManageDepartments());
            $query->whereRaw("(department_id IN ($departments_str)
                OR id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                OR id IN (SELECT user_id FROM group_user WHERE group_id IN (
                    SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                ))
            )");
        }
        if($only_telesale){
            $query->whereHas('roles', function ($q) {
                $q->whereIn('name', array('telesales','teleteamleader'));
            });
        }
        $data = $query->select(DB::raw("CONCAT(username,' - ', full_name) as username"), 'id')->orderBy('username', 'ASC')->pluck('username', 'id')->toArray();

        return $data;
    }

    public static function getOptionsUserByDepartment($selected_value = null, $department_id = null, $only_telesales = TRUE){
        return Utils::generateOptions(self::getListUserByDepartment($department_id, $only_telesales), $selected_value, 'Tất cả');
    }


    public function hasRoleInList($roles)
    {
        foreach ($roles as $role){
            if(Auth::user()->hasRole($role)){
                return TRUE;
            }
        }
        return FALSE;
    }

    public static function listData($no_assign = FALSE, $inManage = FALSE)
    {
        $assignee_list = array();
        if($no_assign){
            $assignee_list['-1'] = 'Chưa được chia';
        }
        $query = User::where('active',1)->orderBy('username', 'ASC');
        if($inManage){
            $query->whereIn('department_id', Auth::user()->getManageDepartments());
        }
        $users = $query->get();
        foreach ($users as $user){
            $assignee_list[$user->id] = $user->username;
        }

        return $assignee_list;
    }

    public function getManageDepartments($clearCache = FALSE)
    {
        $cache_key = __CLASS__ . '_' . __FUNCTION__
            . '_user_' . $this->id
        ;
        $data = Cache::store('file')->get($cache_key);
        $cache_timeout = Cache::store('file')->get($cache_key.'_timeout');
        $timeout = 12*60*60;

        if(!$cache_timeout || time() >= $cache_timeout || $clearCache){
            $data = null;
        }
        if(!$data){
            $departments = array();
            /**
             * user ASC quản lý các cơ sở ASC
             * 169 - loan.dinhthanh
             * 203 - linh.nguyenphuong
             * 214 - huong.bachthilan
             * 216 - phuong.ngolan
             *
             */
            if(in_array($this->id, [203, 169, 214, 216])){
                $departments[] = 20; //ASC-AquaTots-CG
                $departments[] = 21; //ASC-AquaTots-DK
                $departments[] = 22; //ASC-Jacpa
                $departments[] = 25; //ASC-AquaTots-THT
            }
            if(!in_array($this->department_id, $departments)){
                $departments[] = $this->department_id;
            }

            $user_departments = RUserDepartment::where('user_id', $this->id)->get()->pluck('department_id')->toArray();
            if(!empty($user_departments)){
                foreach ($user_departments as $department_id){
                    $departments[] = $department_id;
                }
            }

            $user_groups_departments = RGroupDepartment::whereRaw("group_id IN (SELECT group_id FROM group_user WHERE user_id = '$this->id')")->get()->pluck('department_id')->toArray();
            if(!empty($user_groups_departments)){
                foreach ($user_groups_departments as $department_id){
                    $departments[] = $department_id;
                }
            }
            $data = $departments;
            Cache::store('file')->put($cache_key, $data, $timeout);
            Cache::store('file')->put($cache_key.'_timeout', time()+$timeout, $timeout);
        }

        return $data;
    }


    public function getManageDepartmentsListData()
    {
        $query = DB::table('fptu_department')->where('hide', 0);
        if(!in_array(Department::HOLDING, $this->getManageDepartments()) && !$this->hasRole('holding')){
            $query->whereIn('id', $this->getManageDepartments());
        }
        $query->orderBy('sort_index','ASC');
        return $query->get()->pluck('name', 'id');
    }

    public function getManageUsers($clearCache = FALSE)
    {
        $cache_key = __CLASS__ . '_' . __FUNCTION__
            . '_user_' . $this->id
        ;
        $data = Cache::store('file')->get($cache_key);
        $cache_timeout = Cache::store('file')->get($cache_key.'_timeout');
        $timeout = 12*60*60;

        if(!$cache_timeout || time() >= $cache_timeout || $clearCache){
            $data = null;
        }
        if(!$data){
            $departments_str = implode(',', $this->getManageDepartments());
            $users = User::whereHas('roles', function ($q) {
                $q->where('name', 'telesales')
                    ->orWhere('name', 'teleteamleader');
            })->whereRaw("(department_id IN ($departments_str)
                    OR id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                    OR id IN (SELECT user_id FROM group_user WHERE group_id IN (
                        SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                    ))
                )")
                ->orderBy('username', 'ASC')->get();

            $data = $users->pluck('id')->toArray();
            Cache::store('file')->put($cache_key, $data, $timeout);
            Cache::store('file')->put($cache_key.'_timeout', time()+$timeout, $timeout);
        }

        return $data;
    }

}
