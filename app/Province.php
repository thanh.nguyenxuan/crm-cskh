<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'fptu_province';


    public static function listData()
    {
        return self::all()->pluck('name','id');
    }
}
