<?php

namespace App\Console;

use App\Lead\CallLog;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /*$log_file = storage_path('/logs/sms.log');
        $schedule->call(function () {
            $fetcher = new SmsFetcher();
            $fetcher->fetch();
        })->everyMinute()->sendOutputTo($log_file, true);*/

        //gửi mail thống kê danh sách lead đồng bộ online
//        $schedule->call(function(){LogSyncLead::reportSyncLeadDaily();})->dailyAt('15:00');

        //thống kê danh sách lead tồn đọng chưa chăm sóc -> notify
//        $schedule->call(function(){Notify::notifyLeadNewAndNoAnswer();})->dailyAt('08:00');
//        $schedule->call(function(){Notify::notifyLeadNewAndNoAnswer();})->dailyAt('14:00');

        //thông báo lead kpi hàng ngày
//        $schedule->call(function(){Notify::notifyLeadDailyKPI();})->weekdays()->dailyAt('09:00');
//        $schedule->call(function(){Notify::notifyLeadDailyKPI();})->weekdays()->dailyAt('12:00');
//        $schedule->call(function(){Notify::notifyLeadDailyKPI();})->weekdays()->dailyAt('15:00');
//        $schedule->call(function(){Notify::notifyLeadDailyKPI();})->weekdays()->dailyAt('17:00');

        //đồng bộ file ghi âm call log
        //$schedule->call(function(){CallLog::dailySync();});


        //đồng bộ call log không có file ghi âm
        $schedule->call(function(){CallLog::syncEmptyFileDaily();})->dailyAt('12:00');
        $schedule->call(function(){CallLog::syncEmptyFileDaily();})->dailyAt('23:59');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
