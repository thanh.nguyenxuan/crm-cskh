<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $table = 'fptu_campaign';
    protected $fillable = array('name');

    public static function getIdByName($name)
    {
        $model = Campaign::where('name', $name)->first();
        if($model) {
            return $model->id;
        }else{
            return null;
        }
    }
}
