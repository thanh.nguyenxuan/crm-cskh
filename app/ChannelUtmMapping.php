<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChannelUtmMapping extends Model
{
    protected $table = 'channel_utm_mapping';

    public static function getChannelIdByUtm($utm_source)
    {
        $model = ChannelUtmMapping::where('utm_source', $utm_source)->first();
        if($model) {
            return $model->channel_id;
        }else{
            return null;
        }
    }
}
