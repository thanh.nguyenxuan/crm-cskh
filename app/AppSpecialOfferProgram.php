<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppSpecialOfferProgram extends Model
{
    protected $table = 'app_special_offer_program';

}
