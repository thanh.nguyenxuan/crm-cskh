<?php

namespace App;

use PAMI\Client\Impl\ClientImpl as PamiClient;
use PAMI\Message\Event\EventMessage;
use PAMI\Message\Event\OriginateResponseEvent;
use PAMI\Listener\IEventListener;
use PAMI\Message\Action\OriginateAction;
use Illuminate\Support\Facades\Auth;

class Pbx
{
    public function connect()
    {
        $options = array(
            'host' => env('PBX_HOST'),
            'scheme' => env('PBX_SCHEME'),
            'port' => env('PBX_PORT'),
            'username' => env('PBX_USERNAME'),
            'secret' => env('PBX_PASSWORD'),
            'connect_timeout' => env('PBX_CONNECT_TIMEOUT'),
            'read_timeout' => env('PBX_READ_TIMEOUT')
        );
        //var_dump($options);
        $client = new PamiClient($options);
        $client->open();
        return $client;
    }

    public function dial($phone, $trunk_id = '')
    {
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $data = '';
        $trunk = null;
        if (empty($trunk_id)) {
            $department_id = Auth::user()->department_id;
            if (!empty($department_id)) {
                $default_trunk = Department::find($department_id)->default_trunk;
                if (!empty($default_trunk))
                    $trunk = PbxTrunk::find($default_trunk);
            }
        } else {
            $trunk = PbxTrunk::find($trunk_id);
        }
        $ext = Auth::user()->extension;
        if (!empty($trunk) && !empty($ext) && !empty($phone)) {
            $tech = strtoupper($trunk->tech);
            $client = $this->connect();
            $originateMsg = new OriginateAction($tech . '/' . $ext);
            $originateMsg->setContext('default');
            $originateMsg->setPriority(1);
            $originateMsg->setExtension($ext);
            $originateMsg->setApplication('Dial');
            $originateMsg->setData($tech . '/' . $trunk->name . '/' . $trunk->prefix . $phone);
            var_dump($tech . '/' . $trunk->name . '/' . $trunk->prefix . $phone);
            var_dump(Auth::user()->toArray());
            $originateMsg->setCallerId($trunk->caller_id);
            var_dump($client);
            $output = $client->send($originateMsg);
            var_dump($output);
            $client->close();
        }
    }

    public function dial_test($phone, $trunk_id = '')
    {
        $data = '';
        $trunk = null;
        if (empty($trunk_id)) {
            $default_trunk = Auth::user()->default_trunk;
            $trunk = PbxTrunk::find($default_trunk);
        } else {
            $trunk = PbxTrunk::find($trunk_id);
        }
        $ext = Auth::user()->extension;
        if (!empty($trunk) && !empty($ext) && !empty($phone)) {
            $tech = strtoupper($trunk->tech);
            $client = $this->connect();
            $originateMsg = new OriginateAction($tech . '/' . $ext);
            $originateMsg->setContext('from-internal');
            $originateMsg->setPriority(1);
            $originateMsg->setExtension($ext);
            $originateMsg->setApplication('Dial');
            $originateMsg->setData($tech . '/' . $trunk->name . '/' . $trunk->prefix . $phone . ',300,T');
            //var_dump($tech . '/' . $trunk->name . '/' . $trunk->prefix . $phone);
            //var_dump(Auth::user()->toArray());
            $originateMsg->setCallerId($trunk->caller_id);
            //var_dump($client);
            $client->send($originateMsg);
            $client->close();
        }
    }
}

?>
