<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ManualLeadAssigner extends Model
{
    use SoftDeletes;
    protected $table = 'fptu_lead_assignment';
}