<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    protected $table = 'fptu_keyword';
    protected $fillable = ['name'];
}
