<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class ChangeLog extends Model
{
    protected $table = 'fptu_change_log';
	protected $fillable = array('type', 'changer', 'value_before', 'value_before_description', 'value_after', 'value_after_description', 'object', 'object_id', 'field_name' ,'field_display_name');
}