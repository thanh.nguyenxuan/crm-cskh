<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadParentsConcern extends Model
{
    protected $table = 'fptu_lead_parents_concern';

}
