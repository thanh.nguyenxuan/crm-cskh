<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ParentsConcern extends Model
{
    protected $table = 'edf_parents_concern';

    CONST OTHER_OPTION = 99;

    public function isChecked($requestData, $leadData, $other)
    {
        $checked = false;
        if(!empty($requestData)){
            if(in_array($this->id, $requestData)){
                $checked = TRUE;
            }
        }else if(!empty($leadData)){
            if(in_array($this->id, $leadData)){
                $checked = TRUE;
            }else{
                if($this->id == self::OTHER_OPTION && $other){
                    $checked = TRUE;
                }
            }
        }
        return $checked;
    }
}
