<?php

namespace App\EmailMarketing;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model
{
    protected $table = 'fptu_email_template';

    public function getLatestRevision()
    {
        $latest_template = EmailTemplate::where('parent', $this->id)
            ->orderBy('created_at', 'DESC')
            ->first();
        if (!empty($latest_template) && !empty($latest_template->id))
            return $latest_template;
        return $this;
    }
}
