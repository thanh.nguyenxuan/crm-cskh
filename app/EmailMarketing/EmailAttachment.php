<?php

namespace App\EmailMarketing;

use Illuminate\Database\Eloquent\Model;

class EmailAttachment extends Model
{
    protected $table = 'fptu_email_campaign_attachment';
}
