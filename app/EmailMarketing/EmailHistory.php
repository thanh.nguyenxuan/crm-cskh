<?php

namespace App\EmailMarketing;

use Illuminate\Database\Eloquent\Model;

class EmailHistory extends Model
{
    protected $table = 'fptu_email_history';
}
