<?php

namespace App\EmailMarketing;

class MailMerger
{
    public function parse($template_content, $data)
    {
        foreach ($data as $placeholder => $value) {
            $placeholder = '[' . $placeholder . ']';
            //var_dump($placeholder);
            $template_content = str_ireplace($placeholder, $value, $template_content);
        }
        return $template_content;
    }
}
