<?php

namespace App\EmailMarketing;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmailCampaign extends Model
{
    use SoftDeletes;
    protected $table = 'fptu_email_campaign';

    public function getLatestTemplateRevision()
    {
        $latest = null;
        $latest = EmailTemplate::where('parent', $this->template)
            ->orderBy('created_at', 'DESC')
            ->first();
        if (empty($latest) || empty($latest->id)) {
            $latest = EmailTemplate::find($this->template);
        }
        return $latest;
    }
}
