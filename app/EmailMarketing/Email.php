<?php

namespace App\EmailMarketing;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $table = 'fptu_email';
	protected $dates = ['birthdate'];
}
