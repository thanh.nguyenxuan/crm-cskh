<?php

namespace App\EmailMarketing;

use Illuminate\Database\Eloquent\Model;

class EmailList extends Model
{
    protected $table = 'fptu_email_list';
}
