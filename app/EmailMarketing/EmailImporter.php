<?php

namespace App\EmailMarketing;

use PHPExcel;
use PHPExcel_IOFactory;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class EmailImporter
{
    var $id;

    public function import($id, $path)
    {
        $this->id = $id;
        if (!file_exists($path))
            return '<div class="alert alert-danger">Không tìm thấy file import!</div>';
        ini_set('display_errors', 1);
        ini_set('error_reporting', E_ALL);
        $objPHPExcel = PHPExcel_IOFactory::load($path);
        $data = $objPHPExcel->getActiveSheet()->toArray();
        $result = array('message' => '', 'data' => array());
        $first_row = true;
        $row_number = 2;
        $success = 0;
        $failed = 0;
        $failed_row_list = array();
        $dup_row_list = array();
        $success_row_list = array();
        $duplicated = 0;
        $total = count($data) - 1;
        $message = '';
        foreach ($data as $row) {
            if ($first_row) {
                $header = $row;
                $first_row = false;
                continue;
            }
            $import_result = $this->import_row3($row, $header);
            //var_dump($import_result);
            $banner = 'Dòng ' . $row_number . ': ';
            if ($import_result['result'] == 'ok') {
                $success++;
                $message .= '<div class="alert alert-success">' . $banner . 'Thành công!</div>';
                $success_row_list[] = $row;

            } elseif ($import_result['result'] == 'dup') {
                $duplicated++;
                $row[] = strip_tags($import_result['message']);
                $dup_row_list[] = $row;
                $message .= '<div class="alert alert-warning">' . $banner . $import_result['message'] . '</div>';
            } elseif ($import_result['result'] == 'failed') {
                $failed++;
                $row[] = strip_tags($import_result['message']);
                $failed_row_list[] = $row;
                $message .= '<div class="alert alert-danger">' . $banner . $import_result['message'] . '</div>';
            }
            $row_number++;
        }
        if ($failed > 0) {
            //var_dump($failed_row_list);
            $this->dump_file($failed_row_list, 'Import_Failed.xlsx');
        }
        if ($duplicated > 0) {
            $this->dump_file($dup_row_list, 'Import_Duplicate.xlsx');
        }
        if ($success > 0) {
            $this->dump_file($success_row_list, 'Import_Success.xlsx');
        }
        $result['data']['success'] = $success;
        $result['data']['failed'] = $failed;
        $result['data']['dup'] = $duplicated;
        $result['data']['total'] = $total;
        $result['message'] = $message;
        return $result;
    }

    function validate_email($email)
    {
        return true;
        $email_list = array();
        if (strpos($email, ',') !== false) {
            $email_list = explode(",", $email);
        } else {
            $email_list[] = $email;
        }
        if (empty($email_list))
            return false;
        $ok = true;
        foreach ($email_list as $email) {
            $email = trim(strtolower($email));
            if (!filter_var($email, FILTER_VALIDATE_EMAIL))
                $ok = false;
        }
        if ($ok)
            return true;
        return false;
    }

    function dump_file($row_list = array(), $file_name)
    {
        $template_path = public_path() . '/templates/email_template.xlsx';
        $output_path = public_path() . '/uploads/';
        if (file_exists($template_path)) {
            $objPHPExcel = PHPExcel_IOFactory::load($template_path);
            $objPHPExcel->getActiveSheet()->fromArray($row_list, '', 'A2');
            //Storage::disk('local')->put('Failed_Import.xlsx', 'Contents');
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save($output_path . $file_name);
        } else {
            echo '<div class="alert alert-danger">Lỗi không tìm thấy file template</div>';
        }
    }

    function sanitize_phone($mobile)
    {
        $mobile = preg_replace('/[^\d]/', '', $mobile);
        if (substr($mobile, 0, 1) != '0')
            $mobile = '0' . $mobile;
        return $mobile;
    }

    function validate_phone($phone)
    {
        //$phone = $this->sanitize_phone($phone);
        if (strlen($phone) != 10 && strlen($phone) != 11)
            return false;
        return true;
    }

    function validate_national_id($id)
    {
        if (empty($id))
            return '';
        $id = preg_replace('/\s+/', '', $id);
        if (strlen($id) == 8) {
            $id = '0' . $id;
        }
        if (strlen($id) == 10) {
            if (substr($id, 0, 1) == 0)
                $id = substr($id, 1, 9);
            else
                $id = '00' . $id;
        }
        if (strlen($id) == 11) {
            if (substr($id, 0, 1) != 0)
                $id = '0' . $id;
        }
        if (strlen($id) != 9 && strlen($id) != 12)
            return false;
        return $id;
    }

    function get_image_file($stt)
    {
        $path = '/var/www/html/public/uploads/students_2/' . $stt . '.';
        $allow_exts = array('jpg', 'png', 'jpeg', 'JPG', 'JPEG', 'gif', 'GIF');
        foreach ($allow_exts as $ext) {
            $path_check = $path . $ext;
            //var_dump($path_check);
            if (file_exists($path_check))
                return $stt . '.' . $ext;
        }
        return null;
    }

    function import_row($row)
    {
        $data = array();
        $message = '';
        $status = 'ok';
        $name = trim($row[1]);
        $stt = trim($row[0]);
        $image = $this->get_image_file($stt);
        $birthdate = trim($row[2]);
        if (!empty($birthdate)) {
            try {
                $birthdate = Carbon::createFromFormat('d/m/Y', $birthdate)->format('Y-m-d');;
            } catch (\InvalidArgumentException $exception) {
                $message .= 'Ngày sinh \'<strong>' . $birthdate . '</strong>\' không hợp lệ. ';
                $status = 'failed';
            }
        }
        $id_passport = trim($row[3]);
        $id_passport = $this->validate_national_id($id_passport);
        // if(!$id_passport) {
        // $message .= 'CMND \'<strong>'. $id_passport .'</strong>\' không hợp lệ. CMND phải có 9 hoặc 12 số.';
        // $status = 'failed';
        // }
        if (!empty($row[4])) {
            $phone1 = $this->sanitize_phone($row[4]);
            if (!empty($phone1) && !$this->validate_phone($phone1)) {
                $message .= 'Số điện thoại \'<strong>' . $phone1 . '</strong>\' không hợp lệ. ';
                //$status = 'failed';
            }
        } else
            $phone1 = '';
        if (!empty($row[5])) {
            $phone2 = $this->sanitize_phone($row[5]);
            if (!empty($phone2) && !$this->validate_phone($phone2)) {
                $message .= 'Số điện thoại \'<strong>' . $phone2 . '</strong>\' không hợp lệ. ';
                //$status = 'failed';
            }
        } else
            $phone2 = '';
        $address = trim($row[6]);
        $email = trim($row[7]);
        if (!$this->validate_email($email)) {
            $message .= 'Email \'<strong>' . $email . '</strong>\' không hợp lệ. ';
            $status = 'failed';
        }
        // else {
        // $email_count = Email::where('email', $email)->count();
        // if(!empty($email_count)) {
        // $message .= 'Email \'<strong>'. $email .'</strong>\' đã tồn tại trong danh sách. ';
        // $status = 'dup';
        // }
        // }
        if ($status != 'ok') {
            $data['result'] = $status;
            $data['message'] = $message;
        } else {
            $data['result'] = 'ok';
            $data['message'] = 'Thành công!';
            $contact = new Email;
            $contact->excel_order = $stt;
            $contact->name = $name;
            if (!empty($birthdate))
                $contact->birthdate = $birthdate;
            $contact->id_passport = $id_passport;
            if (!empty($image))
                $contact->image = $image;
            $contact->phone1 = $phone1;
            $contact->phone2 = $phone2;
            $contact->list_id = $this->id;
            $contact->address = $address;
            if (!empty($email))
                $contact->email = $email;
            $contact->save();
        }

        return $data;
    }

    function import_row2($row)
    {
        $data = array();
        $message = '';
        $status = 'ok';
        $stt = trim($row[0]);
        $name = trim($row[1]);
        $contact_name = trim($row[2]);
        $address = trim($row[3]);
        $phone1 = trim($row[4]);
        $phone2 = trim($row[5]);
        $email = trim($row[6]);
        $major = trim($row[7]);
        $subject_group = trim($row[8]);
        $grade = 0;
        if (!empty($row[9]))
            $grade = trim($row[9]);
        //var_dump($row[9]);
        $branch = trim($row[10]);
        if (!$this->validate_email($email)) {
            $message .= 'Email \'<strong>' . $email . '</strong>\' không hợp lệ. ';
            $status = 'failed';
        }
        // else {
        // $email_count = Email::where('email', $email)->count();
        // if(!empty($email_count)) {
        // $message .= 'Email \'<strong>'. $email .'</strong>\' đã tồn tại trong danh sách. ';
        // $status = 'dup';
        // }
        // }
        if ($status != 'ok') {
            $data['result'] = $status;
            $data['message'] = $message;
        } else {
            $data['result'] = 'ok';
            $data['message'] = 'Thành công!';
            $contact = new Email;
            $contact->excel_order = $stt;
            $contact->name = $name;
            $contact->contact_name = $name;
            $contact->address = $address;
            $contact->phone1 = $phone1;
            $contact->phone2 = $phone2;
            $contact->list_id = $this->id;
            if (!empty($email))
                $contact->email = $email;
            $contact->major = $major;
            $contact->subject_group = $subject_group;
            $contact->grade = $grade;
            $contact->branch = $branch;
            $contact->save();
        }

        return $data;
    }

    function import_row3($row, $header)
    {
        $data = array();
        $content = array();
        $message = '';
        $status = 'ok';
        $email_col = null;
        $name_col = null;
        $reply_to_col = null;
        $email = '';
        $name = '';
        for ($i = 0; $i < count($header); $i++) {
            $heading = strtolower($header[$i]);
            if ($heading == 'email' || $heading == 'e-mail' || $heading == 'mail' || $heading == 'gmail')
                $email_col = $i;
            if ($heading == 'name' || $heading == 'ho_ten' || $heading == 'ho_va_ten' || $heading == 'full_name' || $heading == 'Họ và tên')
                $name_col = $i;
            if ($heading == 'reply' || $heading == 'reply_to' || $heading == 'replyto' || $heading == 'reply_email'|| $heading == 'reply to')
                $reply_to_col = $i;
        }

        //var_dump($email_col);
        if ($email_col === FALSE) {
            $message .= 'Không cột nào có tiêu đề là email.';
            $status = 'failed';
        }
        if (!empty($row[$email_col])) {
            $email = $row[$email_col];
            if (!$this->validate_email($email)) {
                $message .= 'Email \'<strong>' . $email . '</strong>\' không hợp lệ. ';
                $status = 'failed';
            }
        }
        if (!empty($row[$name_col]))
            $name = $row[$name_col];
        if (!empty($row[$reply_to_col]))
            $reply_to = $row[$reply_to_col];
        foreach ($row as $col => $value) {
            if ($col != $email_col) {
                $content[$header[$col]] = $value;
                //var_dump($value);
            }
        }
        if ($status != 'ok') {
            $data['result'] = $status;
            $data['message'] = $message;
        } else {
            $data['result'] = 'ok';
            $data['message'] = 'Thành công!';
            $contact = new Email;
            if (!empty($email))
                $contact->email = trim(strtolower($email));
            $contact->name = trim($name);
            if (!empty($reply_to))
                $contact->reply_to = $reply_to;
            $contact->data = json_encode($content);
            $contact->campaign_id = $this->id;
            $contact->created_by = Auth::id();
            $contact->save();
        }

        return $data;
    }
}