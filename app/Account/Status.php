<?php

namespace App\Account;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $table = 'fptu_account_status';
}
