<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignCategory extends Model
{
    protected $table = 'fptu_campaign_category';


    public static function listData()
    {
        return self::whereNull('deleted_at')->get()->pluck('name', 'id')->toArray();
    }
}
