<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    protected $table = 'fptu_channel';
    protected $fillable = array('name');
}
