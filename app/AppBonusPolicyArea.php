<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppBonusPolicyArea extends Model
{
    protected $table = 'app_bonus_policy_area';

}
