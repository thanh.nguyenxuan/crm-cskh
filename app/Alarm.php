<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alarm extends Model
{
    CONST OBJECT_TYPE_LEAD = 'lead';

    protected $table = 'fptu_alarm';

    CONST TYPE_CALLBACK         = 'callback';       //hẹn gọi lại
    CONST TYPE_SEE_AT_SCHOOL    = 'see_at_school';  //hẹn đến trường

    public static function getAllStatus()
    {
        return array(
            0 => 'Đang chờ',
            1 => 'Đã hoàn thành',
        );
    }

    public static function getTypeListData()
    {
        return array(
            self::TYPE_CALLBACK => 'Hẹn gọi lại',
            self::TYPE_SEE_AT_SCHOOL => 'Hẹn đến trường',
        );
    }
}
