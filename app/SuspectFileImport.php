<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuspectFileImport extends Model
{
    protected $table = 'fptu_import_suspect';
}
