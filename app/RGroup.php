<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RGroup extends Model
{
    use SoftDeletes;

    protected $table = 'group';


    public static function listData()
    {
        return self::where('status', 1)->orderBy('sort_index','ASC')->get()->pluck('name','id');
    }

    public function search()
    {
        return self::all();

    }

}