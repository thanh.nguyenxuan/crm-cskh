<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LearningSystemOfficial extends Model
{
    protected $table = 'learning_system_official';

    public static function listData()
    {
        return self::where('status',1)->get()->pluck('name', 'id');
    }
}
