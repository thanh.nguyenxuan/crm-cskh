<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Weight extends Model
{
    protected $table = 'fptu_weight';
    public static function getWeight($name, $value) {
        if(empty($value))
            $value = 'default';
        $weight = Weight::where(
            [
                ['name', 'like', $name],
                ['value', 'like', $value]
            ]
        )->pluck('weight')->first();
        return $weight;
    }
}
