<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class LeadLogStatus extends Model
{
    protected $table = 'fptu_lead_log_status';

    CONST TYPE_LEAD_STATUS = 'lead_status';
    CONST TYPE_CALL_STATUS = 'call_status';
    CONST TYPE_CARE_STATUS = 'care_status';

    public $start_date;
    public $end_date;
    public $department_id;



    public function search()
    {
        $data = array();
        if(!empty($this->start_date) && !empty($this->end_date))
        {
            $start_date = date('Y-m-d', strtotime(str_replace('/','-',$this->start_date))) . ' 00:00:00';
            $end_date = date('Y-m-d', strtotime(str_replace('/','-',$this->end_date))) . ' 23:59:59';
            $departments = array();
            $lead_query = Lead::whereNull('deleted_at')
                ->where('created_at', '>=' , $start_date)
                ->where('created_at', '<=' , $end_date)
                ->orderBy('id', 'ASC');
            if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding')){
                if(!empty($this->department_id)){
                    $departments[] = $this->department_id;
                }
            }else{
                $departments = Auth::user()->getManageDepartments();
            }
            if(!empty($departments)){
                $lead_query->whereIn('department_id', $departments);
            }
            $list_lead = $lead_query->get();
            $list_status = LeadStatus::where('hide', 0)->orderBy('id', 'ASC')->get();

            //init structure log status
            $log_status = array();
            foreach ($list_status as $status){
                $log_status[$status->id] = '';
            }

            //init structure data
            foreach ($list_lead as $lead){
                $data[$lead->id] = array(
                    'lead' => $lead,
                    'log_status' => $log_status
                );
            }

            $lead_log_status_conditions = array();
            $lead_log_status_conditions[] = 'deleted_at IS NULL';
            $lead_log_status_conditions[] = "created_at >= '$start_date' AND created_at <= '$end_date'";
            if(!empty($departments)){
                $lead_log_status_conditions[] = "department_id IN (".implode(',',$departments).")";
            }

            $condition = implode(' AND ', $lead_log_status_conditions);
            $list_lead_log_status = LeadLogStatus::whereNull('deleted_at')
                ->whereRaw("lead_id IN (SELECT id FROM fptu_lead WHERE $condition)")
                ->orderBy('id','ASC')
                ->get();

            foreach ($list_lead_log_status as $lead_log_status){
                if($lead_log_status->status_old = 1){ //mới
                    $data[$lead_log_status->lead_id]['log_status'][1] = $data[$lead_log_status->lead_id]['lead']->created_at;
                }
                $data[$lead_log_status->lead_id]['log_status'][$lead_log_status->status_new] = $lead_log_status->created_at;
            }
        }
        return $data;


    }
}