<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Excel;
use Illuminate\Support\Facades\DB;

class LogSyncLead extends Model
{
    public $table = 'log_sync_lead';

    CONST STATUS_FAIL = 4;
    CONST STATUS_SUCCESS = 10;

    CONST SOURCE_GIS = 'GIS';
    CONST SOURCE_SMIS = 'SMIS';
    CONST SOURCE_IKIDS = 'IKIDS';
    CONST SOURCE_ASC = 'ASC';
    CONST SOURCE_DEWEY = 'DEWEY';

    public static function reportSyncLeadDaily()
    {
        //OTS,Marketing,Sale
        $date = date("d/m/Y");
        $today = date('Y-m-d');
        $from_date = date('Y-m-d', strtotime($today . ' -1 days')). ' 15:00:00';
        $to_date = $today . ' 14:59:59';

        $data = LogSyncLead::select(
                'log_sync_lead.id AS id',
                'fptu_lead.name AS name',
                'fptu_lead.phone1 AS phone',
                'fptu_lead.email AS email',
                'fptu_lead.student_name AS student_name',
                'fptu_lead.birthdate AS birthday',
                'fptu_lead.gender AS gender',
                'fptu_department.name AS department',
                'fptu_lead_status.name AS status',
                DB::raw('CASE log_sync_lead.duplicate WHEN 1 then "có" ELSE "không" END AS duplicate'),
                'log_sync_lead.created_at AS request_at',
                'log_sync_lead.source AS utm_source',
                'log_sync_lead.campaign AS utm_campaign',
                DB::raw('CONCAT("'.env('APP_URL').'/lead/edit/",log_sync_lead.lead_id) AS url')
            )
            ->leftJoin('fptu_lead', 'log_sync_lead.lead_id', '=', 'fptu_lead.id')
            ->leftJoin('fptu_lead_status', 'fptu_lead.status', '=', 'fptu_lead_status.id')
            ->leftJoin('fptu_department', 'fptu_lead.department_id', '=', 'fptu_department.id')
            ->whereNull('log_sync_lead.deleted_at')
            ->where('log_sync_lead.status', self::STATUS_SUCCESS)
            ->where('log_sync_lead.created_at', '>=', $from_date)
            ->where('log_sync_lead.created_at', '<=', $to_date)
            ->get();

        $count_total_new = 0;
        $count_total_duplicate = 0;
        foreach ($data as $item){
            if($item->duplicate == 'có'){
                $count_total_duplicate++;
            }else{
                $count_total_new++;
            }
        }

        //loại bỏ các lead mới khỏi file
        foreach ($data as $key => $item){
            if($item->duplicate != 'có'){
                unset($data[$key]);
            }
        }

        $file = \Excel::download(new LeadSyncExport($data), 'LogSyncLead.xlsx');

        $from       = 'CRM';
        $to         = array(
            'thanh.nguyenxuan@educo.edu.vn',
            'sales.smis@edufit.vn',
            'maitrang.nguyen@edufit.vn',
            'duc.nguyenviet@educo.edu.vn',
        );
        $subject    = "Cập nhật tình hình thu lead ngày $date";
        $data       = array(
            'title' => $subject . " (từ $from_date đến $to_date)",
            'count_total_new' => $count_total_new,
            'count_total_duplicate' => $count_total_duplicate
        );
        $template   = 'email.lead.report_ots';
        $attachment = array(
            'path' => $file->getFile()->getPathName(),
            'name' => $subject.'.xlsx'
        );

        Mail::send($template, $data, function($message) use ($from, $to, $subject, $attachment) {
            $message
                ->from('crm@edufit.vn',$from)
                ->to($to)
                ->subject($subject);
            if(!empty($attachment)){
                $message->attach($attachment['path'], ['as' => $attachment['name']]);
            }
        });


    }

}
