<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppFeature extends Model
{
    protected $table = 'app_feature';

}
