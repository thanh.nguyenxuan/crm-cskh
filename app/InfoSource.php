<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfoSource extends Model
{
    protected $table = 'fptu_info_source';
    protected $fillable = array('name');
}
