<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReferralRule extends Model
{
    protected $table = 'referral_rules';
}
