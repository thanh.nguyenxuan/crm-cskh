<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentChallenge extends Model
{
    protected $table = 'edf_student_challenge';

    CONST OTHER_OPTION = 5;

    public function isChecked($requestData, $leadData, $other)
    {
        $checked = false;
        if(!empty($requestData)){
            if(in_array($this->id, $requestData)){
                $checked = TRUE;
            }
        }else if(!empty($leadData)){
            if(in_array($this->id, $leadData)){
                $checked = TRUE;
            }else{
                if($this->id == self::OTHER_OPTION && $other){
                    $checked = TRUE;
                }
            }
        }
        return $checked;
    }
}
