<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LearningSystemDesire extends Model
{
    protected $table = 'learning_system_desire';


    public static function listData()
    {
        return self::where('status',1)->get()->pluck('name', 'id');
    }
}
