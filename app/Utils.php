<?php

namespace App;

use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class Utils
{
    CONST OTHER_OPTION = -1;

    private static $marTViet = array("à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ", "ì", "í", "ị", "ỉ", "ĩ", "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ", "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ", "ỳ", "ý", "ỵ", "ỷ", "ỹ", "đ", "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ", "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ", "Ì", "Í", "Ị", "Ỉ", "Ĩ", "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ", "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ", "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ", "Đ");
    private static $marKoDau = array("a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "i", "i", "i", "i", "i", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "y", "y", "y", "y", "y", "d", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "I", "I", "I", "I", "I", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "Y", "Y", "Y", "Y", "Y", "D");
    private static $tohop = array("à", "á", "ạ", "ả", "ã", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ", "è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ", "ì", "í", "ị", "ỉ", "ĩ", "ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ", "ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ", "ỳ", "ý", "ỵ", "ỷ", "ỹ", "đ", "À", "Á", "Ạ", "Ả", "Ã", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ", "È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ", "Ì", "Í", "Ị", "Ỉ", "Ĩ", "Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ", "Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ", "Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ", "Đ");

    public static function toDbDate($date, $format = 'd/m/Y', $has_time = false)
    {
        if (empty($date))
            return '';
        return Carbon::createFromFormat($format, $date)->format('Y-m-d H:i:s');
    }

    public static function convertDate($date, $fromFormat = 'd/m/Y', $toFormat = 'Y-m-d')
    {
        if(!empty($date)){
            if($fromFormat == 'd/m/Y'){
                $datePattern = '/^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/';
                if(preg_match($datePattern, $date)){
                    return Carbon::createFromFormat($fromFormat, $date)->format($toFormat);
                }
            }else{
                return Carbon::createFromFormat($fromFormat, $date)->format($toFormat);
            }
        }
        return null;
    }

    public static function toDisplayDate($date, $has_time = false)
    {
        if (empty($date))
            return '';
        if (!$has_time)
            return Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y');
        else
            return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    public static function checkDuplicatePhone($number, $department_id)
    {
        if (!empty($number)) {
            $count = DB::table('fptu_lead')
                ->where(function ($query) use ($number) {
                    $query->where('phone1', $number)
                        ->orWhere('phone2', $number)
                        ->orWhere('phone3', $number)
                        ->orWhere('phone4', $number);
                })
                ->whereNull('deleted_at')
                ->where('department_id', $department_id)
                ->whereRaw('leave_school IS NULL OR leave_school != 1')
                ->count();
            if ($count > 0)
                return true;
        }
        return false;
    }

    public static function checkDuplicateLead($number, $birthdate, $department_id)
    {
        if (!empty($number)) {
            $count = DB::table('fptu_lead')
                ->where(function ($query) use ($number) {
                    $query->where('phone1', $number)
                        ->orWhere('phone2', $number)
                        ->orWhere('phone3', $number)
                        ->orWhere('phone4', $number);
                })
                ->whereNull('deleted_at')
                ->where('department_id', $department_id)
                ->count();
            if ($count > 0)
                return true;
        }
        return false;
    }

    public
    static function sanitizePhone($mobile)
    {
        $mobile = preg_replace('/[^\d]/', '', $mobile);
        if (empty($mobile))
            return null;
        if (substr($mobile, 0, 2) == '84')
            $mobile = substr($mobile, 2, strlen($mobile) - 2);
        if (substr($mobile, 0, 1) != '0')
            $mobile = '0' . $mobile;
        $mobile = Utils::change_telco_prefix($mobile);
        return $mobile;
    }

    public
    static function romanize_string($string)
    {
        $string = trim($string);
        $string = str_replace(self::$marTViet, self::$marKoDau, $string);
        $string = preg_replace('/\./', '', $string);
        $string = preg_replace('/\-/', '', $string);
        return $string;
    }

    public
    static function validateDisplayBirthdate($birthdate)
    {
        $birthdate = trim($birthdate);
        $date_chunks = explode('/', $birthdate);
        if (count($date_chunks) == 3) {
            if (checkdate($date_chunks[1], $date_chunks[0], $date_chunks[2]))
                return true;
        }
        return false;
    }

    public
    static function sanitize_province($province)
    {
        $province = self::romanize_string($province);
        $province = str_replace(' ', '', $province);
        $province = strtoupper($province);
        switch ($province) {
            case 'TPHOCHIMINH':
                $province = 'HOCHIMINH';
                break;
            case 'VUNGTAU':
                $province = 'BARIAVUNGTAU';
                break;
            case 'HUE':
                $province = 'THUATHIENHUE';
                break;
            case 'DACLAC':
                $province = 'DAKLAK';
                break;
            case 'DACNONG':
                $province = 'DAKNONG';
                break;
        }
        return $province;
    }

    public
    static function validate_province($province)
    {
        $province = self::sanitize_province($province);
        $count = DB::table('fptu_province')->where('name_upper', $province)->count();
        //var_dump(DB::table('fptu_province')->where('name_upper', $province)->toSql());
        //var_dump($province);
        if ($count)
            return true;
        return false;
    }

    public static function validateDate($dateString, $format)
    {
        $date = \DateTime::createFromFormat($format, $dateString);
        return $date && \DateTime::getLastErrors()["warning_count"] == 0 && \DateTime::getLastErrors()["error_count"] == 0;
    }

    public
    static function change_telco_prefix($number)
    {
        $new_prefix = '';
        if (strlen($number) == 11) {
            $prefix = substr($number, 0, 4);
            $remaining = substr($number, 4, 7);
            switch ($prefix) {
                // Viettel
                case '0169':
                    $new_prefix = '039';
                    break;
                case '0168':
                    $new_prefix = '038';
                    break;
                case '0167':
                    $new_prefix = '037';
                    break;
                case '0166':
                    $new_prefix = '036';
                    break;
                case '0165':
                    $new_prefix = '035';
                    break;
                case '0164':
                    $new_prefix = '034';
                    break;
                case '0163':
                    $new_prefix = '033';
                    break;
                case '0162':
                    $new_prefix = '032';
                    break;
                // MobiFone
                case '0120':
                    $new_prefix = '070';
                    break;
                case '0121':
                    $new_prefix = '079';
                    break;
                case '0122':
                    $new_prefix = '077';
                    break;
                case '0126':
                    $new_prefix = '076';
                    break;
                case '0128':
                    $new_prefix = '078';
                    break;
                // Vinaphone
                case '0123':
                    $new_prefix = '083';
                    break;
                case '0124':
                    $new_prefix = '084';
                    break;
                case '0125':
                    $new_prefix = '085';
                    break;
                case '0127':
                    $new_prefix = '081';
                    break;
                case '0129':
                    $new_prefix = '082';
                    break;
                // Gmobile
                case '0199':
                    $new_prefix = '059';
                    break;
            }
        }
        if (!empty($new_prefix)) {
            $new_number = $new_prefix . $remaining;
            return $new_number;
        }
        return $number;
    }

    public static function sendSms($phone,$message, $brandname = Telcom::BRAND_NAME_EDUFIT)
    {
        //lay thong tin cau hinh telcom
        $telcomConfig = DB::table('telcom')->where('brandname', $brandname)->first();
        $username = $telcomConfig->username;
        $password = $telcomConfig->password;
        $url = $telcomConfig->url;
        $type = 1;
        $post = array(
            "username"  => $username,
            "password"  => $password,
            "brandname" => $brandname,
            "message"   => $message,
            "phones"    => $phone,
            "type"      => $type
        );
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    public static function sendSMSv2($phones, $mesage, $brandname = Telcom::BRAND_NAME_EDUFIT)
    {
        $telcomConfig = DB::table('telcom')->where('brandname', $brandname)->first();
        $username = $telcomConfig->username;
        $password = $telcomConfig->password;
        $url = 'http://cms.telcomedia.com.vn/api2/send';
        $secret_key = 'slF08YlPh@!s6T';
        $post = array(
            'uid'           => '113',
            'userCms'       => 'edufit',
            'passCms'       => 'ee1a2579c83fd00befea6fe88bfcacf9',
            'brandname'     => $brandname,
            'username'      => $username,
            'password'      => $password,
            'message'       => $mesage,
            'phones'        => $phones,
            'type'          => '1',
            'timeSchedule'  => '',
        );

        $signature = md5(implode('@', array_merge(array_values($post), array($secret_key))));
        $post['signature'] = $signature;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    public static function unsign_string($str, $separator = '-', $keep_special_chars = FALSE)
    {
        $str = str_replace(array("à", "á", "ạ", "ả", "ã", "ă", "ằ", "ắ", "ặ", "ẳ", "ẵ", "â", "ầ", "ấ", "ậ", "ẩ", "ẫ"), "a", $str);
        $str = str_replace(array("À", "Á", "Ạ", "Ả", "Ã", "Ă", "Ằ", "Ắ", "Ặ", "Ẳ", "Ẵ", "Â", "Ầ", "Ấ", "Ậ", "Ẩ", "Ẫ"), "A", $str);
        $str = str_replace(array("è", "é", "ẹ", "ẻ", "ẽ", "ê", "ề", "ế", "ệ", "ể", "ễ"), "e", $str);
        $str = str_replace(array("È", "É", "Ẹ", "Ẻ", "Ẽ", "Ê", "Ề", "Ế", "Ệ", "Ể", "Ễ"), "E", $str);
        $str = str_replace("đ", "d", $str);
        $str = str_replace("Đ", "D", $str);
        $str = str_replace(array("ỳ", "ý", "ỵ", "ỷ", "ỹ", "ỹ"), "y", $str);
        $str = str_replace(array("Ỳ", "Ý", "Ỵ", "Ỷ", "Ỹ"), "Y", $str);
        $str = str_replace(array("ù", "ú", "ụ", "ủ", "ũ", "ư", "ừ", "ứ", "ự", "ử", "ữ"), "u", $str);
        $str = str_replace(array("Ù", "Ú", "Ụ", "Ủ", "Ũ", "Ư", "Ừ", "Ứ", "Ự", "Ử", "Ữ"), "U", $str);
        $str = str_replace(array("ì", "í", "ị", "ỉ", "ĩ"), "i", $str);
        $str = str_replace(array("Ì", "Í", "Ị", "Ỉ", "Ĩ"), "I", $str);
        $str = str_replace(array("ò", "ó", "ọ", "ỏ", "õ", "ô", "ồ", "ố", "ộ", "ổ", "ỗ", "ơ", "ờ", "ớ", "ợ", "ở", "ỡ"), "o", $str);
        $str = str_replace(array("Ò", "Ó", "Ọ", "Ỏ", "Õ", "Ô", "Ồ", "Ố", "Ộ", "Ổ", "Ỗ", "Ơ", "Ờ", "Ớ", "Ợ", "Ở", "Ỡ"), "O", $str);
        if ($keep_special_chars == FALSE) {
            $str = str_replace(array('–', '…', '“', '”', "~", "!", "@", "#", "$", "%", "^", "&", "*", "/", "\\", "?", "<", ">", "'", "\"", ":", ";", "{", "}", "[", "]", "|", "(", ")", ",", ".", "`", "+", "=", "-"), $separator, $str);
            $str = preg_replace("/[^_A-Za-z0-9- ]/i", '', $str);
        }

        $str = str_replace(' ', $separator, $str);

        return trim(strtolower($str), "-");
    }

    public static function utf8convert($str, $keep_upper_char = FALSE)
    {
        if(!$str) return false;
        if($keep_upper_char){
            $utf8 = array(
                'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
                'd'=>'đ',
                'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
                'i'=>'í|ì|ỉ|ĩ|ị',
                'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
                'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
                'y'=>'ý|ỳ|ỷ|ỹ|ỵ',
                'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
                'D'=>'Đ',
                'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
                'I'=>'Í|Ì|Ỉ|Ĩ|Ị',
                'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
                'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
                'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
            );
        }else{
            $utf8 = array(
                'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
                'd'=>'đ|Đ',
                'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
                'i'=>'í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị',
                'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
                'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
                'y'=>'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',
            );
        }


        foreach($utf8 as $ascii=>$uni) $str = preg_replace("/($uni)/i",$ascii,$str);

        return $str;
    }


    public static function sanitizePhoneV2($phone)
    {
        $value = preg_replace('/[^\d]/', '', $phone);
        if(!empty($value)){
            $pattern = '/^[\d]{8,15}$/';
            if(!preg_match($pattern, $value)){
                $value = null;
            }
        }
        return $value;
    }

    public static function generateOptions($data, $selected_value = null, $empty_text = null, $other = false, $other_text = null, $multiple = false)
    {
        $html = "";
        if($multiple){
            foreach ($data as $key => $value){
                $selected = (!empty($selected_value) && in_array($key, $selected_value)) ? "selected" : "";
                $html.= "<option value='{$key}' {$selected}>{$value}</option>";
            }
        }else{
            if(!empty($empty_text)){
                $html.= "<option value=''>{$empty_text}</option>";
            }
            if($other){
                if(empty($other_text)) $other_text = 'Khác';
                $selected = (!empty($selected_value) && self::OTHER_OPTION == $selected_value) ? "selected" : "";
                $html.= "<option value='".self::OTHER_OPTION."' {$selected}>{$other_text}</option>";
            }
            foreach ($data as $key => $value){
                $selected = (!empty($selected_value) && $key == $selected_value) ? "selected" : "";
                $html.= "<option value='{$key}' {$selected}>{$value}</option>";
            }
        }
        return $html;
    }

    public static function getDayOfWeekShortName($date = null){
        if(empty($date)) $date = date('Y-m-d');
        return strtolower(substr(date('l', strtotime($date)),0,3));
    }

    public static function getListDateInRange($start_date, $end_date)
    {
        $arr_date = array();
        $start_date = date('Y-m-d', strtotime(str_replace('/','-',$start_date))) . ' 00:00:00';
        $end_date = date('Y-m-d', strtotime(str_replace('/','-',$end_date))) . ' 23:59:59';
        $period = new DatePeriod(
            new DateTime($start_date),
            new DateInterval('P1D'),
            new DateTime($end_date)
        );
        foreach ($period as $key => $value){
            $arr_date[] = $value->format('Y-m-d');
        }
        return $arr_date;
    }

    /**
     * @param $list_date array
     * @param $removeWeekDayNames array
     * ext:
     * $removeWeekDayNames: ['sat','sun']
     * @return array
     */
    public static function removeDayInRangeByWeekDayName($list_date, $removeWeekDayNames)
    {
        $list_filtered_date = array();
        if(!empty($list_date) && !empty($removeWeekDayNames)){
            foreach ($list_date as $date){
                if(!in_array(self::getDayOfWeekShortName($date), $removeWeekDayNames)){
                    $list_filtered_date[] = $date;
                }
            }
        }
        return $list_filtered_date;
    }

    public static function ultimate_trim($str)
    {
        return trim(preg_replace("/\s+/", " ", $str));
    }

    public static function convertExcelDateToPHP($excel_date, $format = 'Y-m-d')
    {
        if(is_numeric($excel_date)){
            $php_date = date($format, \PHPExcel_Shared_Date::ExcelToPHP($excel_date));
        }else{
            $php_date = date($format, strtotime(str_replace('/','-', $excel_date)));
        }
        return $php_date;
    }

    public static function prettyTime($time)
    {
        $return = '';
        $minute = intval($time/60);
        $second = $time%60;
        if($minute > 0){
            $return.= $minute.'m';
        }
        if($second > 0){
            $return.= $second.'s';
        }
        return $return;
    }

    public static function callAPI($method, $url, $header = array(), $data = array(), &$http_status = 200){
        $curl = curl_init();

        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl   , CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
        $result = curl_exec($curl);
        $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if(!$result){die("Connection Failure");}
        curl_close($curl);
        return $result;
    }
}