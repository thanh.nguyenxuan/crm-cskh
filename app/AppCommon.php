<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppCommon extends Model
{
    protected $table = 'app_common';

}
