<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 08/05/2018
 * Time: 3:09 AM
 */

namespace App\Sms;

use App\Sms\Sms;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\SmsNotification;
use App\Lead;


class SmsFetcher
{
    public function fetch()
    {

        /* connect to gmail */
        $hostname = '{imap.gmail.com:993/imap/ssl}INBOX';
        $username = 'smsbot.tstt@gmail.com';
        $password = 'sms@2018@4444!';

        /* try to connect */
        $inbox = imap_open($hostname, $username, $password) or die('Cannot connect to Gmail: ' . imap_last_error());

        /* grab emails */
        $today = date('d M Y');
        $emails = imap_search($inbox, 'FROM "smsbot.tstt@gmail.com" SINCE "' . $today . '"');

        /* if emails are returned, cycle through each... */
        if ($emails) {

            /* for every email... */
            foreach ($emails as $email_number) {

                /* get information specific to this email */
                $overview = imap_fetch_overview($inbox, $email_number, 0);
                $message = imap_fetchbody($inbox, $email_number, 1);
                $meta = $overview[0];
                $sms_existing = Sms::where('uid', $meta->uid)->count();
                $phone = $this->parseMessage($meta->subject);
                $sms = null;
                if ($sms_existing == 0 && !empty($phone)) {
                    $sms = new Sms();

                    $sms->uid = $meta->uid;
                    $received_date = date('Y-m-d H:i:s', strtotime($meta->date));
                    $sms->created_at = $received_date;
                    $sms->to = $phone; // from
                    $sms->type = 2; // incoming SMS
                    $sms->created_by = 1; // system
                    $sms->content = $message;
                    $sms->port = substr($meta->subject, strlen($meta->subject) - 1, 1);
                    $sms->save();
                } else {
                    $sms = Sms::where('uid', $meta->uid)->first();
                }
                if (!empty($sms) && empty($sms->notified)) {
                    $this->notify($sms);
                }
            }
        }

        /* close the connection */
        imap_close($inbox);
    }

    function notify($sms)
    {
        $phone = $sms->to;
        $lead_name = '';
        $to_email = '';
        $cc = array('hoangld.tstt@gmail.com');
        if (!empty($phone)) {
            $lead = Lead::where('phone1', $phone)->orWhere('phone2', $phone)->orWhere('phone3', $phone)->orWhere('phone4', $phone)->first();
            if (!empty($lead) && !empty($lead->id)) {
                $lead_id = $lead->id;
                $lead_name = $lead->name;
                if (!empty($lead->activator)) {
                    $activator = User::where('id', $lead->activator)->first();
                    if (!empty($activator) && $activator->email)
                        $to_email = $activator->email;
                } else {
                    $assignee_list = DB::table('fptu_lead_assignment')->where('lead_id', $lead->id)->whereNull('deleted_at')->get();
                    $to_email = array();
                    foreach ($assignee_list as $assignee) {
                        $to_email = array();
                        $assignee_user = User::find($assignee->user_id);
                        if (!empty($assignee_user) && !empty($assignee_user->id)) {
                            $to_email[] = $assignee_user->email;
                        }
                    }
                }
            }
        }
        if (!empty($to_email) && !empty($cc))
            Mail::to($to_email)->cc($cc)->queue(new SmsNotification($sms));
    }


    public function parseMessage($message)
    {
        $matches = array();
        preg_match('/\+84\d+/', $message, $matches);
        if (!empty($matches)) {
            $phone = '0' . substr($matches[0], 3, strlen($matches[0]));
            return ($phone);
        }
        return '';
    }
}