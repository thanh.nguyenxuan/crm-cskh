<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 24/11/2017
 * Time: 2:39 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campus extends Model
{
    protected $table = 'fptu_campus';
    protected $fillable = array('name');

    public function getCampusId()
    {
        if (empty($this->code))
            return false;
        $campus_id = Campus::where('code', $this->code)->first();
        //var_dump(Campus::where('code', $this->code));
        return $campus_id['id'];
    }
}
