<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Telcom extends Model
{
    CONST BRAND_NAME_EDUFIT     = 'EDUFIT';
    CONST BRAND_NAME_GATEWAY    = 'GATEWAY';
    CONST BRAND_NAME_SAKURA     = 'MN SAKURA';
    CONST BRAND_NAME_DEWEY      = 'DEWEY';

    protected $table = 'telcom';

    public static function getListBrandNameOptions()
    {
        $data = array(
            self::BRAND_NAME_EDUFIT     => self::BRAND_NAME_EDUFIT,
            self::BRAND_NAME_GATEWAY    => self::BRAND_NAME_GATEWAY,
            self::BRAND_NAME_SAKURA     => self::BRAND_NAME_SAKURA,
            self::BRAND_NAME_DEWEY      => self::BRAND_NAME_DEWEY,
        );
        return Utils::generateOptions($data, null, 'Chọn');
    }


    public static function getAuthBrandName()
    {
        $user = User::find(Auth::user()->id);
        $departments = $user->getManageDepartments();
        $department = Department::find($departments[0]);
        $department_name = strtoupper($department->name);
        if(in_array($department->id, [2, 16])){ //GIS-CG , GIS-THT
            $brand_name = self::BRAND_NAME_DEWEY;
        }else if(preg_match('/^SMIS-/', $department_name) || preg_match('/^IMS-/', $department_name)){
            $brand_name = self::BRAND_NAME_SAKURA;
        }else if(preg_match('/^GIS-/', $department_name)){
            $brand_name = self::BRAND_NAME_GATEWAY;
        }else{
            $brand_name = null;
        }
        return $brand_name;
    }

}