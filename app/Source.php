<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Source extends Model
{
    protected $table = 'fptu_source';
    protected $fillable = array('name');

    CONST TYPE_ORGANIC          = 'organic';
    CONST TYPE_OTHER            = 'other';
    CONST TYPE_BRAND            = 'brand';
    CONST TYPE_REFERRED         = 'referred';
    CONST TYPE_EMPLOYEE_CHILD   = 'employee_child';
    CONST TYPE_CHANGING         = 'changing';
    CONST TYPE_PARTNER          = 'partner';
    CONST TYPE_PARENTS          = 'parents';
    CONST TYPE_ADVERTISE        = 'advertise';

    public static function onlineSource()
    {
        $query = self::where('online', 1)->where('hide', 0);

        if(Auth::user()->isSMISUser()){
            $query->where('smis', 1);
        }elseif(Auth::user()->isGISUser()){
            $query->where('gis', 1);
        }elseif(Auth::user()->isASCUser()){
            $query->where('asc', 1);
        }

        return $query->get()->pluck('id')->toArray();
    }

    public static function listData()
    {
        return self::where('hide', 0)->orderBy('sort_index', 'ASC')->get()->pluck('name', 'id');
    }

    public static function listDataByUserDepartment()
    {
        $query = Source::where('hide', 0)->orderBy('sort_index', 'ASC');
        if(Auth::user()->isSMISUser()){
            $query->where('smis', 1);
        }else if(Auth::user()->isGISUser()){
            $query->where('gis', 1);
        }else if(Auth::user()->isASCUser()){
            $query->where('asc', 1);
        }
        return $query->pluck('name', 'id')->toArray();
    }

    public static function getTypeLabel($type)
    {
        $label = '';
        switch ($type){
            case self::TYPE_ORGANIC         : $label = 'Organic';             break;
            case self::TYPE_BRAND           : $label = 'Thương hiệu';         break;
            case self::TYPE_REFERRED        : $label = 'Giới thiệu';          break;
            case self::TYPE_ADVERTISE       : $label = 'Quảng cáo';           break;
            case self::TYPE_PARTNER         : $label = 'Đối tác';             break;
            case self::TYPE_EMPLOYEE_CHILD  : $label = 'Con em CBNV';         break;
            case self::TYPE_CHANGING        : $label = 'Chuyển đổi';          break;
            case self::TYPE_PARENTS         : $label = 'Phụ Huynh GIS/SMIS';  break;
            case self::TYPE_OTHER           : $label = 'Khác';                break;
        }
        return $label;
    }

    public static function getTypeListData($online = null)
    {
        $data = array();
        if(isset($online)){
            if($online == 'online' || $online == 1){
                $types = array(
                    self::TYPE_ORGANIC,
                    self::TYPE_BRAND,
                    self::TYPE_ADVERTISE,
                    self::TYPE_PARTNER,
                    self::TYPE_OTHER,
                );
            }else{
                $types = array(
                    self::TYPE_BRAND,
                    self::TYPE_REFERRED,
                    self::TYPE_PARTNER,
                    self::TYPE_EMPLOYEE_CHILD,
                    self::TYPE_CHANGING,
                    self::TYPE_PARENTS,
                    self::TYPE_OTHER,
                );
            }
        }else{
            $types = array(
                self::TYPE_ORGANIC,
                self::TYPE_BRAND,
                self::TYPE_REFERRED,
                self::TYPE_ADVERTISE,
                self::TYPE_PARTNER,
                self::TYPE_EMPLOYEE_CHILD,
                self::TYPE_CHANGING,
                self::TYPE_PARENTS,
                self::TYPE_OTHER,
            );
        }

        foreach ($types as $type){
            $data[$type] = self::getTypeLabel($type);
        }
        return $data;
    }


}
