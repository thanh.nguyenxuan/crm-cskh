<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CampaignCategoryMapping extends Model
{
    use SoftDeletes;
    protected $table = 'fptu_campaign_category_mapping';

}
