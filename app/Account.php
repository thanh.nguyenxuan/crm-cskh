<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = 'fptu_account';
	protected $fillable = ['name', 'gender', 'birthdate', 'national_id', 'province', 'lead_id', 'status', 'type', 'submit_place', 'submitted_at', 'registered_major', 'campaign', 'financial_status', 'payment_method', 'invoice_number', 'receiving_place'];
}
