<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'fptu_department';
    protected $fillable = array('name', 'province', 'description');

    CONST IKIDS = 13;
    CONST HOLDING = 14;


    public static function listData()
    {
        return self::where('hide', 0)->orderBy('sort_index','ASC')->get()->pluck('name', 'id');
    }

    public static function getListDepartmentId($type = null)
    {
        if($type == 'SMIS'){
            return array(
                1,  //SMIS-HN-CG-DV
                3,  //SMIS-HN-BĐ-TK
                4,  //SMIS-HN-HBT-LY
                5,  //SMIS-HN-HĐ-CA
                6,  //SMIS-HP-NQ-LHP
                7,  //SMIS-HP-NQ-VC
                11, //SMIS-QN-HL-NTH
                12, //SMIS-HCM-Q2-AK
                13, //IMS-TB-TL-HTC
                15, //SMIS-HP-DK-AD
                17, //SMIS-HN-TX-NT
                18, //SMIS-HN-TL-THT
                19, //IMS-HN-NTL-TH
            );
        }else if($type == 'GIS'){
            return array(
                2,  //GIS-HN-CG-DV
                8,  //GIS-HP-DK-AD
                16, //GIS-HN-TL-THT
            );
        }else if($type == 'ASC'){
            return array(
                20,  //ASC-AquaTots-CG
                21,  //ASC-AquaTots-DK
                22, //ASC-Jacpa
                25, //ASC-AquaTots-THT
            );
        }else{
            return array();
        }
    }

    public static function getOptionsSMIS($selected_value = null)
    {
        $list_department_id = self::getListDepartmentId('SMIS');
        $data = self::where('hide', 0)->whereIn('id', $list_department_id)->get()->pluck('name', 'id');
        return Utils::generateOptions($data, $selected_value, 'Tất cả');
    }

    public static function getOptionsGIS($selected_value = null)
    {
        $list_department_id = self::getListDepartmentId('GIS');
        $data = self::where('hide', 0)->whereIn('id', $list_department_id)->get()->pluck('name', 'id');
        return Utils::generateOptions($data, $selected_value, 'Tất cả');
    }

    public static function getListGroupDepartment()
    {
        return array(
            'SMIS' => 'SMIS',
            'GIS' => 'GIS',
            'ASC' => 'ASC',
        );
    }

    public static function getOptions($selected_value = null, $multiple = false){
        return Utils::generateOptions(self::listData(), $selected_value, 'Tất cả', false, null, $multiple);
    }

    public static function getOptionsGroup($selected_value = null){
        return Utils::generateOptions(self::getListGroupDepartment(), $selected_value, 'Tất cả');
    }
}
