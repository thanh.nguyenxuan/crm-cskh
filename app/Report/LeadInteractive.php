<?php

namespace App\Report;

use App\Department;
use App\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LeadInteractive
{
    CONST INTERACTED = 1;
    CONST NO_INTERACT = 2;

    public $start_date;
    public $end_date;
    public $department_id;
    public $user_id;
    public $lead_id;
    public $assignee_id;
    public $interact;

    public static function getListInteractOptions($selected_value = null)
    {
        $data = array(
            self::INTERACTED => 'Đã tương tác',
            self::NO_INTERACT => 'Chưa tương tác',
        );
        return \App\Lead\CallLog::generateOptions($data, $selected_value, 'Tất cả');
    }

    public function search()
    {
        $data = array();
        if(!empty($this->start_date) && !empty($this->end_date)){
            $start_date = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $this->start_date)));
            $end_date = date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $this->end_date)));

            $query = DB::table('fptu_lead')
                ->select('fptu_lead.*',
                    DB::raw('(SELECT user_id FROM fptu_lead_assignment WHERE deleted_at IS NULL AND lead_id = fptu_lead.id ORDER BY id DESC LIMIT 1) AS assignee'),
                    DB::raw('(SELECT created_at FROM edf_call_log WHERE lead_id = fptu_lead.id ORDER BY id ASC LIMIT 1) AS first_call_log_time'),
                    DB::raw('DATEDIFF((SELECT created_at FROM edf_call_log WHERE lead_id = fptu_lead.id ORDER BY id ASC LIMIT 1), fptu_lead.created_at) AS diff_day')
                )
                ->whereRaw("created_at >= '$start_date' AND created_at <= '$end_date'")
                ->orderBy('id', 'DESC');

            $departments = array();
            if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding')){
                if(!empty($this->department_id)){
                    $departments[] = $this->department_id;
                }
            }else{
                $departments = Auth::user()->getManageDepartments();
            }
            if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding') || Auth::user()->hasRole('teleteamleader')){
                $user_id = $this->user_id;
                $assignee_id = $this->user_id;
            }else{
                $user_id = Auth::user()->id;
                $assignee_id = Auth::user()->id;
            }

            if(!empty($departments)){
                $query->whereIn('created_by', function($sub_query) use($departments){
                    $departments_str = implode(',', Auth::user()->getManageDepartments());
                    $sub_query->select('id')
                        ->from(with(new User())->getTable())
                        ->whereRaw("(department_id IN ($departments_str)
                            OR id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                            OR id IN (SELECT user_id FROM group_user WHERE group_id IN (
                                SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                            ))
                        )");
                });
            }
            if(!empty($user_id)){
                $query->where("created_by",$user_id);
            }

            if(!empty($assignee_id)){
                $query->whereRaw("(
                    (created_by = '$assignee_id' AND id NOT IN (SELECT lead_id FROM fptu_lead_assignment))
                    OR
                    (id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = '$assignee_id' AND deleted_at IS NULL))
                )");
            }

            if(!empty($this->interact)){
                if($this->interact == self::INTERACTED){
                    $query->whereRaw('id IN (SELECT lead_id FROM edf_call_log)');
                }else if($this->interact == self::NO_INTERACT){
                    $query->whereRaw('id NOT IN (SELECT lead_id FROM edf_call_log)');
                }
            }

            if(!empty($this->lead_id)){
                $query->where('id', $this->lead_id);
            }

            $data = $query->get();
        }
        return $data;
    }

}