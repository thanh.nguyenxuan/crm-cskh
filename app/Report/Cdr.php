<?php

namespace App\Report;

use App\Department;
use App\Lead\CallLog;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Cdr
{
    CONST DIRECTION_INBOUND     = 1;    // gọi đến
    CONST DIRECTION_OUTBOUND    = 0;    // gọi đi
    CONST DIRECTION_LOCAL       = 2;    // nội bộ

    CONST STATUS_INBOUND_COMPLETE_AGENT     = 'COMPLETEAGENT';      // gọi đến thành công, kết thúc bởi người nhận
    CONST STATUS_INBOUND_COMPLETE_CALLER    = 'COMPLETECALLER';     // gọi đến thành công, kết thúc bởi người gọi
    CONST STATUS_INBOUND_TRANSFER           = 'TRANSFER';           // gọi đến, chuyển tiếp
    CONST STATUS_INBOUND_ABANDON            = 'ABANDON';            // gọi đến, đã kết nối hệ thống nhưng rớt trước khi kết nối tới agent
    CONST STATUS_INBOUND_RING_NO_ANSWER     = 'RINGNOANSWER';       // gọi đến, không nghe máy

    CONST STATUS_OUTBOUND_ANSWER            = 'ANSWER';             // gọi đi, có nghe máy
    CONST STATUS_OUTBOUND_CANCEL            = 'CANCEL';             // gọi đi, agent hủy
    CONST STATUS_OUTBOUND_FAIL              = 'FAIL';               // gọi đi, kết nối không thành công
    CONST STATUS_OUTBOUND_BUSY              = 'BUSY';               // gọi đi, máy bận
    CONST STATUS_OUTBOUND_NOANSWER          = 'NOANSWER';           // gọi đi, không nghe máy

    /*
    public $host           = 'https://cxcs001.cloudpbx.vn/api/';
    public $token          = '$1$/u28EV27$kT766.Wt3qSMlOZGRybgI1';
    public $organization   = 'demo02.cloudpbx.vn';
    public $port           = '8083';
    */

    /*CONST HOST           = 'https://cpbx003.cloudpbx.vn:8083';
    CONST API_HOST       = 'https://cpbx003.cloudpbx.vn/api/';
    CONST TOKEN          = '$1$nMJdCv91$fhi5ooFxmTW.xjx0YC1a6/';
    CONST ORGANIZATION   = 'edufit.cloudpbx.vn';
    CONST PORT           = '8083';*/

    CONST HOST           = 'https://edufit-sip.cloudpbx.vn:8083';
    CONST API_HOST       = 'https://edufit-sip.cloudpbx.vn:8089/api';
    CONST TOKEN          = '$1$rzWxCe08$xoTWdb6LBLC3tCnNpJcwD0';
    CONST ORGANIZATION   = 'edufit.cloudpbx.vn';
    CONST PORT           = '8083';

    public $type;
    public $date_time;
    public $session;
    public $number_phone;
    public $extension;
    public $queue;
    public $status;
    public $wait_time;
    public $talk_time;
    public $recordingfile;
    public $download_url;

    /**
     * Chiều cuộc gọi
     *
     * @return array
     */
    public static function getListDirection(){
        return array(
            self::DIRECTION_OUTBOUND    => 'Gọi đi',
            self::DIRECTION_INBOUND     => 'Gọi đến',
            self::DIRECTION_LOCAL       => 'Nội bộ',
        );
    }

    /**
     * Trạng thái cuộc gọi
     * @param $direction
     * @return array
     */
    public static function getListStatus($direction = null){
        $data = array();

        if(isset($direction)){
            if($direction == self::DIRECTION_INBOUND){
                $data = array(
                    self::STATUS_INBOUND_COMPLETE_AGENT     => 'Thành công, kết thúc bởi nhân viên',
                    self::STATUS_INBOUND_COMPLETE_CALLER    => 'Thành công, kết thúc bởi người gọi',
                    self::STATUS_INBOUND_TRANSFER           => 'Chuyển tiếp',
                    self::STATUS_INBOUND_ABANDON            => 'Đã kết nối hệ thống nhưng rớt trước khi kết nối tới nhân viên',
                    self::STATUS_INBOUND_RING_NO_ANSWER     => 'Không nghe máy',
                );
            }else if($direction == self::DIRECTION_OUTBOUND){
                $data = array(
                    self::STATUS_OUTBOUND_ANSWER            => 'Có trả lời',
                    self::STATUS_OUTBOUND_CANCEL            => 'Hủy bởi nhân viên',
                    self::STATUS_OUTBOUND_FAIL              => 'Kết nối không thành công',
                    self::STATUS_OUTBOUND_BUSY              => 'Máy bận',
                    self::STATUS_OUTBOUND_NOANSWER          => 'Không nghe máy',
                );
            }
        }else{
            $data = array(
                self::STATUS_INBOUND_COMPLETE_AGENT     => 'Gọi đến thành công, kết thúc bởi nhân viên',
                self::STATUS_INBOUND_COMPLETE_CALLER    => 'Gọi đến thành công, kết thúc bởi người gọi',
                self::STATUS_INBOUND_TRANSFER           => 'Gọi đến, chuyển tiếp',
                self::STATUS_INBOUND_ABANDON            => 'Gọi đến, đã kết nối hệ thống nhưng rớt trước khi kết nối tới nhân viên',
                self::STATUS_INBOUND_RING_NO_ANSWER     => 'Gọi đến, không nghe máy',

                self::STATUS_OUTBOUND_ANSWER            => 'Gọi đi, có trả lời',
                self::STATUS_OUTBOUND_CANCEL            => 'Gọi đi, hủy bởi nhân viên',
                self::STATUS_OUTBOUND_FAIL              => 'Gọi đi, kết nối không thành công',
                self::STATUS_OUTBOUND_BUSY              => 'Gọi đi, máy bận',
                self::STATUS_OUTBOUND_NOANSWER          => 'Gọi đi, không nghe máy',
            );
        }

        return $data;
    }

    /**
     * Telesales - Nhân viên nghe máy
     *
     * @param $department_id
     * @return array
     */
    public static function getListUser($department_id = null){
        $data = array();
        if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding')) {
            if(!empty($department_id)){
                $data = User::whereRaw("(department_id = $department_id
                        OR id IN (SELECT user_id FROM user_department WHERE department_id = $department_id)   
                        OR id IN (SELECT user_id FROM group_user WHERE group_id IN (
                            SELECT group_id FROM group_department WHERE department_id = $department_id
                        ))
                    )")
                    ->where('active', 1)->whereNull('deleted_at')->select(DB::raw("CONCAT(username,' - ', full_name, ' - ', CASE WHEN extension IS NOT NULL THEN extension ELSE '' END) as username"), 'id')->orderBy('username', 'ASC')->pluck('username', 'id')->toArray();
            }else{
                $data = User::where('active', 1)->whereNull('deleted_at')->select(DB::raw("CONCAT(username,' - ', full_name, ' - ', CASE WHEN extension IS NOT NULL THEN extension ELSE '' END) as username"), 'id')->orderBy('username', 'ASC')->pluck('username', 'id')->toArray();
            }
        }else{
            $departments_str = implode(',', Auth::user()->getManageDepartments());
            $data = User::whereRaw("(department_id IN ($departments_str)
                    OR id IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str))   
                    OR id IN (SELECT user_id FROM group_user WHERE group_id IN (
                        SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                    ))
                )")
                ->where('active', 1)->whereNull('deleted_at')->select(DB::raw("CONCAT(username,' - ', full_name, ' - ', CASE WHEN extension IS NOT NULL THEN extension ELSE '' END) as username"), 'id')->orderBy('username', 'ASC')->pluck('username', 'id')->toArray();
        }
        return $data;
    }

    public static function getListDepartment(){
        $data = array();
        if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding')){
            $data = Department::all()->pluck('name', 'id')->toArray();
        }
        return $data;
    }

    /**
     * tạo html option cho select
     *
     * @param $data array (key => value)
     * @param $selected_value
     * @param $empty_text
     * @return string
     */
    public static function generateOptions($data, $selected_value = null, $empty_text = null){
        $html = "";
        if(!empty($empty_text)){
            $html.= "<option value=''>{$empty_text}</option>";
        }
        foreach ($data as $key => $value){
            $selected = '';
            if(!empty($selected_value)){
                if(is_array($selected_value)){
                    $selected = in_array($key,$selected_value) ? "selected" : "";
                }else{
                    $selected = ($key == $selected_value) ? "selected" : "";
                }
            }
            $html.= "<option value='{$key}' {$selected}>{$value}</option>";
        }
        return $html;
    }

    /**
     * Các option cho filter chiều cuộc gọi
     *
     * @param $selected_value
     * @return string
     */
    public static function getOptionsDirection($selected_value = null){
        return self::generateOptions(self::getListDirection(), $selected_value, 'Tất cả');
    }

    /**
     * Các option cho filter trạng thái cuộc gọi
     *
     * @param $selected_value
     * @param $direction
     * @return string
     */
    public static function getOptionsStatus($selected_value = null, $direction = null){
        return self::generateOptions(self::getListStatus($direction), $selected_value, null);
    }

    /**
     * Các option cho filter telesales
     *
     * @param $selected_value
     * @param $department_id
     * @return string
     */
    public static function getOptionsUser($selected_value = null, $department_id = null){
        return self::generateOptions(self::getListUser($department_id), $selected_value, 'Tất cả');
    }

    /**
     * Các option cho filter cơ sở
     *
     * @param $selected_value
     * @param $department_id
     * @return string
     */
    public static function getOptionsDepartment($selected_value = null){
        return self::generateOptions(self::getListDepartment(), $selected_value, 'Tất cả');
    }


    public static function callAPI($method, $url, $header = array(), $data = array(), $timeout = FALSE, &$http_status = ''){
        $curl = curl_init();

        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        if($timeout){
            curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
        }

        // EXECUTE:
        $result = curl_exec($curl);
        $http_status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

//        if(!$result){die("Connection Failure");}
        curl_close($curl);
        return $result;
    }

    /**
     * @param $start_date
     * @param $end_date
     * @param array $extra_params
     * @return Cdr[]
     */
    public function getReportCdr($start_date, $end_date, $extra_params = array())
    {
        $data = array();

        $method = 'GET';
        $url = self::API_HOST . '/api_report_cdr.php';
        $header = array();
        $params = array(
            'token' => self::TOKEN,
            'organization' => self::ORGANIZATION,
        );

        if(!empty($start_date) && !empty($end_date)){
            $start_date = date('Y-m-d 00:00:00', strtotime(str_replace('/','-',$start_date)));
            $end_date = date('Y-m-d 23:59:59', strtotime(str_replace('/','-',$end_date)));

            $params['start_date'] = $start_date;
            $params['end_date'] = $end_date;

            if(isset($extra_params['type'])){
                $params['type'] = $extra_params['type'];
            }

            if(isset($extra_params['phone_number'])){
                $params['phone_number'] = $extra_params['phone_number'];
            }

            if(isset($extra_params['status'])){
                $params['status'] = $extra_params['status'];
            }

            if(isset($extra_params['call_session'])){
                $params['session'] = $extra_params['call_session'];
            }
        }

        $response = json_decode(self::callAPI($method,$url,$header,$params, FALSE, $http_status));

        $data['result'] = null;
        $data['message'] = null;
        $data['data'] = array();
        if(is_array($response) && !empty($response)){
            $data['result'] = $response[0]->result;
            $data['message'] = $response[0]->message;
            $response = $response[0]->data;
            foreach ($response as $cdr_detail){
                $model = new self();
                $model->type            = $cdr_detail->type;
                $model->date_time       = $cdr_detail->date_time;
                $model->session         = $cdr_detail->session;
                $model->number_phone    = $cdr_detail->number_phone;
                $model->extension       = $cdr_detail->extension;
                $model->queue           = $cdr_detail->queue;
                $model->status          = $cdr_detail->status;
                $model->wait_time       = $cdr_detail->wait_time;
                $model->talk_time       = $cdr_detail->talk_time;
                $model->recordingfile   = $cdr_detail->recordingfile;
                $model->download_url    = !empty($cdr_detail->recordingfile) ? $cdr_detail->recordingfile.'&stream=1' : '';

                $data['data'][] = $model;
            }
        }else{
            $data['result'] = $response->result;
            $data['message'] = $response->message;
        }

        return $data;
    }

    public static function convertStatusForCallLog($status)
    {
        switch ($status){
            case self::STATUS_OUTBOUND_ANSWER:
                return CallLog::STATUS_ANSWERED;
            case self::STATUS_OUTBOUND_BUSY:
            case self::STATUS_OUTBOUND_NOANSWER:
            case self::STATUS_OUTBOUND_FAIL:
                return CallLog::STATUS_NOT_ANSWER;
            case self::STATUS_OUTBOUND_CANCEL:
                return CallLog::STATUS_NOT_CONNECTED;
            default:
                return FALSE;
        }
    }

    public static function syncCallLog($all = FALSE)
    {
        $query = CallLog::where('status', 1)
            ->whereNotNull('data')
            ->where('data', 'LIKE', '%"recordingfile":""%');
        if(!$all){
            $end_date = date('Y-m-d H:00:00');
            $start_date = date('Y-m-d H:i:s', strtotime($end_date . ' -1 hours'));
            $query->whereRaw("created_at >= '$start_date' AND created_at <= '$end_date'");
        }
        $callLogs = $query->get();
        if(!empty($callLogs)){
            $total = count($callLogs);
            $i = 0;
            $cdr = new self();
            foreach ($callLogs as $callLog){
                echo $callLog->id;
                $i++;
                $data = json_decode($callLog->data);
                $success = FALSE;
                if(!empty($data)
                    && !empty($data->session)
                    && !empty($data->talk_time) && $data->talk_time > 0
                    && !empty($data->date_time)
                ) {
                    $start_date = date('Y-m-d 00:00:00', strtotime(str_replace('/','-',$data->date_time)));
                    $end_date = date('Y-m-d 00:00:00', strtotime(str_replace('/','-',$data->date_time)));
                    $cdrData = $cdr->getReportCdr($start_date, $end_date, ['call_session' => $data->session]);

                    if(!empty($cdrData['data'][0])) {
                        $callLog->data = json_encode($cdrData['data'][0]);
                        if($callLog->save()){
                            $success = TRUE;
                        };
                    }
                }
                if($success){
                    echo " - OK - ";
                }else{
                    echo " - F - ";
                }
                echo "$i/$total" . "\n";
            }
        }


    }

}