<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 16/01/2018
 * Time: 10:25 AM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPref extends Model
{
    protected $table = 'fptu_user_pref';
    protected $fillable = ['user_id', 'key', 'value'];
}