<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Notify extends Model
{
    public $table = 'notify';

    CONST STATUS_NEW = 1;
    CONST STATUS_PENDING = 2;
    CONST STATUS_SEEN = 5;

    CONST TYPE_SYNC_LEAD = 'sync_lead';
    CONST TYPE_ASSIGN_LEAD = 'assign_lead';
    CONST TYPE_ALARM_LEAD_CARE = 'alarm_lead_care';
    CONST TYPE_IMPORT_LEAD_ASSIGNMENT = 'import_lead_assignment';
    CONST TYPE_ALARM_LEAD_DAILY_KPI = 'alarm_lead_daily_kpi';

    public static function getStatusListData()
    {
        return array(
            self::STATUS_NEW => 'Mới',
            self::STATUS_PENDING => 'Mới',
            self::STATUS_SEEN => 'Đã xem',
        );
    }

    public static function getOptionsStatus($selected_value)
    {
        $data = array(
            self::STATUS_NEW => 'Mới',
            self::STATUS_SEEN => 'Đã xem',
        );
        return Utils::generateOptions($data, $selected_value, 'Tất cả');
    }

    public static function getOptionsDuplicate($selected_value)
    {
        $data = array(
            '1' => 'Có',
            '0' => 'Không',
        );
        return Utils::generateOptions($data, $selected_value, 'Tất cả');

    }

    public static function notifyLeadNewAndNoAnswer()
    {
        $query = DB::table('fptu_lead AS t');
        $query->select('t.id',
            't.status',
            't.created_by',
            DB::raw('(SELECT user_id FROM fptu_lead_assignment WHERE lead_id = t.id AND deleted_at IS NULL ORDER BY id DESC LIMIT 1) as assignee_id')
        );
        $query->whereNull('t.deleted_at');
        $query->whereIn('t.status', array(LeadStatus::STATUS_NEW, LeadStatus::STATUS_NO_ANSWER));
        $query->orderBy('id', 'DESC');
        $leads = $query->get();

        $data_group_by_assignee = array();
        foreach ($leads as $lead){
            $index = !empty($lead->assignee_id)
                ? $lead->assignee_id
                : (!empty($lead->created_by)
                    ? $lead->created_by
                    : 4
                );
            if(!isset($data_group_by_assignee[$index]['new'])){
                $data_group_by_assignee[$index]['new'] = array();
            }
            if(!isset($data_group_by_assignee[$index]['no_answer'])){
                $data_group_by_assignee[$index]['no_answer'] = array();
            }

            if($lead->status == LeadStatus::STATUS_NEW){
                $data_group_by_assignee[$index]['new'][] = $lead->id;
            }
            if($lead->status == LeadStatus::STATUS_NO_ANSWER){
                $data_group_by_assignee[$index]['no_answer'][] = $lead->id;
            }
        }

        foreach ($data_group_by_assignee as $assignee_id => $lead_data)
        {
            $count_new = count($lead_data['new']);
            $count_no_answer = count($lead_data['no_answer']);
            $notify = new Notify();
            $notify->content = "Hiện bạn đang có: <br/> $count_new Lead trạng thái \"Mới\", $count_no_answer Lead trạng thái \"Không nghe máy\"";
            $notify->user_id = $assignee_id;
            $notify->status = Notify::STATUS_NEW;
            $notify->created_at = date('Y-m-d H:i:s');
            $notify->type = Notify::TYPE_ALARM_LEAD_CARE;
            $notify->data = json_encode($lead_data);
            $notify->save();
        }

    }

    public static function notifyLeadDailyKPI()
    {
        $start_date = date('Y-m-d 00:00:00');
        $end_date = date('Y-m-d 23:59:59');
        $query = DB::table('fptu_user AS t');
        $query->select('t.id',
            DB::raw("(SELECT count(*) FROM edf_call_log WHERE created_by = t.id AND status = 1 AND created_at >= '$start_date' AND created_at <= '$end_date') AS total_call_log"),
            DB::raw("(SELECT count(*) FROM fptu_lead WHERE deleted_at IS NULL AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE deleted_at IS NULL AND user_id = t.id) AND hot_lead_at >= '$start_date' AND hot_lead_at <= '$end_date') AS total_hot_lead"),
            DB::raw("(SELECT count(*) FROM fptu_lead WHERE deleted_at IS NULL AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE deleted_at IS NULL AND user_id = t.id) AND showup_school_at >= '$start_date' AND showup_school_at <= '$end_date') AS total_showup"),
            DB::raw("(SELECT count(*) FROM fptu_lead WHERE deleted_at IS NULL AND id IN (SELECT lead_id FROM fptu_lead_assignment WHERE deleted_at IS NULL AND user_id = t.id) AND activated_at >= '$start_date' AND activated_at <= '$end_date') AS total_waitlist")
        );
        $query->where('t.active', 1);
        $query->whereRaw("t.id IN (SELECT user_id FROM fptu_role_user WHERE role_id = (SELECT id FROM fptu_role WHERE name = 'telesales'))");
        $query->whereIn('t.department_id', Department::getListDepartmentId('SMIS'));
        $data = $query->get();

        $kpi_call_log   = 25;
        $kpi_hot_lead   = 3;
        $kpi_showup     = 2;
        $kpi_waitlist   = 1;

        foreach ($data as $item)
        {
            $finish = TRUE;
            $kpi = array();
            if($item->total_call_log < $kpi_call_log){
                $finish = FALSE;
                $kpi[] = ($kpi_call_log - $item->total_call_log) . ' Logcall';
            }
            if($item->total_hot_lead < $kpi_hot_lead){
                $finish = FALSE;
                $kpi[] = ($kpi_hot_lead - $item->total_hot_lead) . ' HotLead';
            }
            if($item->total_showup < $kpi_showup){
                $finish = FALSE;
                $kpi[] = ($kpi_showup - $item->total_showup) . ' Showup';
            }
            if($item->total_waitlist < $kpi_waitlist){
                $finish = FALSE;
                $kpi[] = ($kpi_waitlist - $item->total_waitlist) . ' WL';
            }

            $item->finish = $finish;
            if($finish){
                $content = '<label class="time">'.date('d/m/Y') . ':</label><p>Chúc mừng bạn đã hoàn thành sứ mệnh của Chiến binh cứu số trong ngày hôm nay!</p>';
            }else{
                $content = '<label class="time">'.date('d/m/Y') . ':</label>:<p>Chỉ còn '.implode(', ',$kpi).' là bạn sẽ hoàn thành mục tiêu.</p><p>Chiến binh cứu số cố lên!</p>';
            }

            $skip = FALSE;
            if($finish){
                $notify = DB::table('notify')
                    ->where('user_id', $item->id)
                    ->where('data', 'LIKE', '%"finish":true%')
                    ->whereDate('created_at', date("Y-m-d"))
                    ->first();
                if($notify){
                    $skip = TRUE;
                }
            }

            if(!$skip){
                $notify = new Notify();
                $notify->content = $content;
                $notify->user_id = $item->id;
                $notify->status = Notify::STATUS_NEW;
                $notify->created_at = date('Y-m-d H:i:s');
                $notify->type = Notify::TYPE_ALARM_LEAD_DAILY_KPI;
                $notify->data = json_encode($item);
                $notify->save();
            }
        }

    }

}
