<?php
/**
 * Created by PhpStorm.
 * User: ledaihoang
 * Date: 24/11/2017
 * Time: 2:39 PM
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Online extends Model
{
    protected $table = 'fptu_online';

    public function getOnline($user_id)
    {
        //$online = new \App\Online();
        $online = $this->where('user_id', $user_id)->pluck('minutes')->first();
        return $online;
    }

    public static function getMinutes($user_id)
    {
        //$online = new \App\Online();
        $minutes = Online::where('user_id', $user_id)->pluck('minutes')->first();
        return $minutes;
    }

    public function setOnline($user_id, $plus_extra_minutes = true)
    {
        //$online = \App\Online::where('user_id', $user_id)->pluck('minutes')->first();
        $online = new \App\Online();
        $last_online = $this->where('user_id', $user_id)->pluck('last_online')->first();
        $minutes = $this->getOnline($user_id);
        if ($plus_extra_minutes) {
            $minutes += (ceil(env('ONLINE_CHECK_INTERVAL') / 60000));
            $last_online = strtotime($last_online) + env('ONLINE_CHECK_INTERVAL') / 1000;
        } else {
            $last_online = date('Y-m-d H:i:s');
        }
        $this->where('user_id', $user_id)->update(['minutes' => $minutes, 'last_online' => $last_online]);
        return $last_online;
    }

    public function getOnlineStatus($user_id)
    {
        $online = $this->where('user_id', $user_id)->pluck('last_online')->first();
        $last_online = strtotime($online) + env('ONLINE_CHECK_INTERVAL') / 1000;
        $now = time();
        $is_online = $last_online > $now;
        //var_dump($last_online);
        echo 'Last Online: ' . date('d/m/Y H:i:s', strtotime($online)) . '<br />';
        echo 'Now: ' . date('d/m/Y H:i:s') . '<br />';
        echo 'Next Confirm: ' . date('d/m/Y H:i:s', $last_online) . '<br />';
        echo 'Online Status: ' . ($is_online ? 'Online' : 'Offline');
        return $is_online;
    }
}