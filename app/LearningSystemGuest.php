<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LearningSystemGuest extends Model
{
    protected $table = 'learning_system_guest';

    public static function listData()
    {
        return self::where('status',1)->get()->pluck('name', 'id');
    }
}
