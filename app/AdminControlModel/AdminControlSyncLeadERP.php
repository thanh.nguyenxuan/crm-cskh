<?php

namespace App\AdminControlModel;

use App\Lead;
use App\LeadSource;
use App\LeadStatus;
use App\ManualLeadAssigner;
use App\Utils;
use PHPExcel_IOFactory;
use PhpMyAdmin\Util;

class AdminControlSyncLeadERP
{
    public $filepath;

    public function sync()
    {
        $result = array(
            'success' => array(
                'insert' => array(),
                'update' => array()
            ),
            'fail' => array(
                'insert' => array(),
                'update' => array()
            ),
        );

        if (!file_exists($this->filepath)) {
            die("FILE NOT FOUND");
        }

        $objPHPExcel = PHPExcel_IOFactory::load($this->filepath);
        $data = $objPHPExcel->getActiveSheet()->toArray();

        for ($i=1; $i<count($data); $i++){
            $success = false;

            $correct_id         = intval($data[$i][0]);
            $id                 = intval($data[$i][1]);
            $department_id      = intval($data[$i][2]);
            $created_at         = Utils::convertExcelDateToPHP($data[$i][3], 'Y-m-d H:i:s');
            $enrolled_at        = Utils::convertExcelDateToPHP($data[$i][3], 'Y-m-d H:i:s');
            $parent             = Utils::ultimate_trim($data[$i][5]);
            $student            = Utils::ultimate_trim($data[$i][6]);
            $phone1             = Utils::ultimate_trim($data[$i][7]);
            $phone2             = Utils::ultimate_trim($data[$i][8]);
            $email              = Utils::ultimate_trim($data[$i][9]);
            $email_2            = Utils::ultimate_trim($data[$i][10]);
            $status             = intval($data[$i][11]);
            $code               = Utils::ultimate_trim($data[$i][12]);
            $class              = Utils::ultimate_trim($data[$i][13]);
            $address            = Utils::ultimate_trim($data[$i][14]);

            if(!empty($correct_id)){
                $type = 'update';
                $lead = Lead::find($correct_id);
                if($lead){
                    $lead->name = $parent;
                    $lead->student_name = $student;
                    $lead->phone1 = $phone1;
                    $lead->phone2 = $phone2;
                    $lead->email = $email;
                    $lead->email_2 = $email_2;
                    $lead->student_code = $code;
                    $lead->class_name = $class;
                    $lead->address = $address;
                    $lead->status = 15;
                    $lead->enrolled = 1;
                    if(empty($lead->enrolled_at)){
                        $lead->enrolled_at = $enrolled_at;
                    }
                    $lead->new_promoter = 1;
                    $lead->new_promoter_at = $lead->enrolled_at;
                    if($lead->save()){
                        $success = true;
                    }
                }
            }else{
                $type = 'insert';

                $lead = new Lead();
                $lead->name = $parent;
                $lead->student_name = $student;
                $lead->phone1 = $phone1;
                $lead->phone2 = $phone2;
                $lead->email = $email;
                $lead->email_2 = $email_2;
                $lead->student_code = $code;
                $lead->class_name = $class;
                $lead->address = $address;
                $lead->status = 15;
                $lead->enrolled = 1;
                $lead->enrolled_at = $enrolled_at;
                $lead->created_at = $created_at;
                $lead->created_by = 4;
                $lead->department_id = $department_id;
                if($lead->save()){
                    $success = true;

                    $lead_source = new LeadSource;
                    $lead_source->lead_id = $lead->id;
                    $lead_source->source_id = 6;
                    $lead_source->created_by = 4;
                    $lead_source->created_at = date('Y-m-d H:i:s');
                    $lead_source->save();

                    $assigner = new ManualLeadAssigner();
                    $assigner->lead_id = $lead->id;
                    $assigner->user_id = 4;
                    $assigner->created_by = 4;
                    $lead_source->created_at = date('Y-m-d H:i:s');
                    $assigner->save();
                }
            }

            if($success) {
                $result['success'][$type][$i] = $lead->id;
            }else{
                $result['fail'][$type][$i] = $lead->id;
            }
        }

        echo json_encode($result);

        return $result;
    }
}
