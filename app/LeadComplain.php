<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadComplain extends Model
{
    protected $table = 'lead_complain';

    CONST STATUS_RECEIVED = 1;
    CONST STATUS_PROCESS_LEVEL_TEACHER = 3;
    CONST STATUS_PROCESS_LEVEL_HEADMASTER = 5;
    CONST STATUS_FINISH = 9;
    CONST STATUS_NOTIFIED = 10;


    public static function statusListData()
    {
        return array(
            self::STATUS_RECEIVED                   => 'Tiếp nhận',
            self::STATUS_PROCESS_LEVEL_TEACHER      => 'Đang giải quyết  (Cấp giáo viên)',
            self::STATUS_PROCESS_LEVEL_HEADMASTER   => 'Đang giải quyết (Cấp Hiệu Trưởng)',
            self::STATUS_FINISH                     => 'Đã hoàn thành',
            self::STATUS_NOTIFIED                   => 'Đã thông tin lại kết quả PH & Đóng',
        );
    }

}
