<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GuestClass extends Model
{
    protected $table = 'guest_class';

    public static function listData()
    {
        return self::where('status',1)->get()->pluck('name', 'id');
    }
}
