<?php

namespace App\Mail;

use App\EmailMarketing\EmailCampaign;
use App\EmailMarketing\MailMerger;
use App\Lead;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use App\EmailMarketing\EmailTemplate;
use Illuminate\Support\Facades\DB;

class SmsNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $call;

    public function __construct($call)
    {
        $this->call = $call;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        set_time_limit(-1);
        $email = null;
        $phone = $this->call->phone_number;
        $lead = null;
        if (!empty($phone)) {
        }
        $subject = 'Cuộc gọi đến từ ';
        if (!empty($lead) && !empty($lead->id)) {
            $this->call->lead_id = $lead->id;
            $this->call->lead_name = $lead->name;
            $subject .= 'lead ' . $lead->name;
        } else {
            $subject .= 'lead mới';
        }
        $subject .= ' ' . $phone;
        $email = $this->from('call.notification@daihocfpt.edu.vn', 'Incoming Call Notification')
            ->subject($subject)
            ->view('call.call_notification', ['call' => $this->lead]);
        return $email;
    }
}
