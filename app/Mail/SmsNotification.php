<?php

namespace App\Mail;

use App\EmailMarketing\EmailCampaign;
use App\EmailMarketing\MailMerger;
use App\Lead;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use App\EmailMarketing\EmailTemplate;
use Illuminate\Support\Facades\DB;

class SmsNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $sms;

    public function __construct($sms)
    {
        $this->sms = $sms;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        set_time_limit(-1);
        $email = null;
        $phone = $this->sms->to;
        $this->sms->notified = 1;
        $this->sms->save();
        $lead = null;
        if (!empty($phone)) {
            $lead = Lead::where('phone1', $phone)->orWhere('phone2', $phone)->orWhere('phone3', $phone)->orWhere('phone4', $phone)->first();
        }
        $subject = 'Tin nhắn SMS từ ';
        if (!empty($lead) && !empty($lead->id)) {
            $this->sms->lead_id = $lead->id;
            $this->sms->lead_name = $lead->name;
            $subject .= 'lead ' . $lead->name;
        } else {
            $subject .= 'lead mới';
        }
        $subject .= ' ' . $phone;
        $email = $this->from('sms.notification@daihocfpt.edu.vn', 'SMS Notification')
            ->subject($subject)
            ->view('sms.sms_notification', ['sms' => $this->sms]);
        return $email;
    }
}
