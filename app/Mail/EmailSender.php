<?php

namespace App\Mail;

use App\EmailMarketing\EmailCampaign;
use App\EmailMarketing\MailMerger;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use App\EmailMarketing\EmailTemplate;
use Illuminate\Support\Facades\DB;

class EmailSender extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $email;
    public $test_email;

    public function __construct($email, $test = false)
    {
        $this->email = $email;
        $this->test_email = $test;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        set_time_limit(0);
        $campaign_id = $this->email->campaign_id;
        $campaign = null;
        $email = null;
        $content = '';
        if (!empty($campaign_id))
            $campaign = EmailCampaign::find($this->email->campaign_id);
        if (!empty($campaign) && $campaign->id) {
            $mailMerger = new MailMerger();
            if (!empty($campaign->template)) {
                $latest_template = $campaign->getLatestTemplateRevision();
                $template_content = $latest_template->content;
            }
            $data = get_object_vars(json_decode($this->email->data));
            $content = $mailMerger->parse($template_content, $data);
            $test_email = $this->test_email;
            $email_id = $this->email->id;
            $email = $this->from($campaign->sender, $campaign->sender_name)
                ->subject($campaign->subject)
                ->view('email.templates.EmailGenericTemplate')->with(['email_id' => $email_id, 'content' => $content, 'test' => $test_email]);
            $attachments = DB::table('fptu_email_campaign_attachment')
                ->where('email_campaign_id', $campaign_id)
                ->get();
            foreach ($attachments as $attachment) {
                $full_path = '';
                if ($attachment->personalized) {
                    $full_path = $attachment->path . '/' . $attachment->email_campaign_id . '/' . $this->email->id . '/' . $attachment->id . '.' . $attachment->file_type;
                } else {
                    $full_path = $attachment->path . '/' . $attachment->email_campaign_id . '/' . $attachment->file_name;
                }
                if (!empty($full_path) && file_exists($full_path)) {
                    $email->attach($full_path, [
                        'as' => $attachment->file_name,
                        'mime' => $attachment->mime_type
                    ]);
                }
            }
            if (empty($this->test_email)) {
                $this->email->status = 'sent';
                $this->email->sent_at = date('Y-m-d H:i:s');
                $this->email->save();
            }
        }
        return $email;
    }
}
