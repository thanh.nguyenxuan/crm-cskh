<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Merging extends Model
{
    protected $table = 'fptu_merging';
    protected $fillable = array('master_id', 'duplicate_id');
}
