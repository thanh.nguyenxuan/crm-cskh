<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadType extends Model
{
    protected $table = 'fptu_lead_type';


    public static function listData()
    {
        return self::where('status',1)->get()->pluck('name', 'id');
    }
}
