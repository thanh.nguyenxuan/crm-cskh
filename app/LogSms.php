<?php

namespace App;

use App\Lead\CallLog;
use Grpc\Call;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LogSms extends Model
{
    CONST STATUS_NEW = 1;
    CONST STATUS_CANCEL = 4;
    CONST STATUS_FAIL = 8;
    CONST STATUS_SUCCESS = 10;

    CONST TYPE_SINGLE = 'single';
    CONST TYPE_MULTIPLE = 'multiple';

    protected $table = 'log_sms';

    public $start_date;
    public $end_date;
    public $department_id;
    public $department_level;
    public $user_id;
    public $role_id;

    public static function getAllStatus()
    {
        return array(
            self::STATUS_NEW => 'Mới',
            self::STATUS_CANCEL => 'Hủy',
            self::STATUS_FAIL => 'Thất bại',
            self::STATUS_SUCCESS => 'Thành công',
        );
    }

    public static function getReportStatusOption($selected_value = null)
    {
        $data =  array(
            self::STATUS_FAIL => 'Thất bại',
            self::STATUS_SUCCESS => 'Thành công',
        );
        return CallLog::generateOptions($data, $selected_value, 'Tất cả');
    }

    public static function getListType()
    {
        return array(
            self::TYPE_SINGLE => 'Đơn lẻ',
            self::TYPE_MULTIPLE => 'Nhiều',
        );
    }

    public static function getTypeOptions($selected_value = null)
    {
        $data = self::getListType();
        return CallLog::generateOptions($data, $selected_value, 'Tất cả');
    }

    public static function getListBrandName()
    {
        return DB::table('telcom')->select('brandname')->get()->pluck('brandname', 'brandname');
    }

    public static function getBrandNameOptions($selected_value = null){
        $data = self::getListBrandName();
        return CallLog::generateOptions($data, $selected_value, 'Tất cả');
    }

    public static function getResponseDescription($response)
    {
        if(is_object($response)){
            $r = $response->CODE;
        }else{
            $r = intval($response);
        }
        switch ($r){
            case 0:
                $des = 'Thành công';
                break;
            case 1:
                $des = 'Thất bại';
                break;
            case 2:
                $des = 'Lỗi xác thực';
                break;
            case 3:
                $des = 'Brandname không hợp lệ';
                break;
            case 99:
                $des = 'Lỗi khác';
                break;
            default:
                $des = null;
        }
        return $des;
    }


    public function search()
    {
        $data = array();
        if(!empty($this->start_date) && !empty($this->end_date)){
            $start_date = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $this->start_date)));
            $end_date = date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $this->end_date)));
            $departments = array();

            $query = DB::table('log_sms')
                ->whereRaw("created_at >= '$start_date' AND created_at <= '$end_date'")
                ->orderBy('id', 'DESC');

            if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding')){
                $departments[] = $this->department_id;
            }else{
                $departments = Auth::user()->getManageDepartments();
            }
            if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('holding') || Auth::user()->hasRole('teleteamleader')){
                $user_id = $this->user_id;
            }else{
                $user_id = Auth::user()->id;
            }

            if(!empty($departments)){
                $query->whereIn('created_by', function($sub_query) use($departments){
                    $sub_query->select('id')
                        ->from(with(new User())->getTable())
                        ->whereIn('department_id', $departments);
                });
                $departments_str = implode(',',$departments);
                $query->whereRaw("(created_by IN (SELECT id FROM fptu_user WHERE department_id IN ($departments_str))
                    OR created_by IN (SELECT user_id FROM user_department WHERE department_id IN ($departments_str)   
                    OR created_by IN (SELECT user_id FROM group_user WHERE group_id IN (
                        SELECT group_id FROM group_department WHERE department_id IN ($departments_str)
                    )
                )");
            }
            if(!empty($user_id)){
                $query->whereRaw("(created_by = '$user_id' OR original_user_id = '$user_id')");
            }
            if(!empty($this->role_id)){
                $role_id = $this->role_id;
                $query->whereIn('created_by', function($sub_query) use($role_id){
                    $sub_query->select('user_id')
                        ->from('fptu_role_user')
                        ->where('role_id', $role_id);
                });
            }
            if(!empty($this->department_level)){
                $department_level = $this->department_level;
                $query->whereIn('created_by', function($sub_query) use ($department_level){
                    $sub_query->select('id')
                        ->from(with(new User())->getTable())
                        ->whereIn('department_id', function($sub_query_2) use ($department_level){
                            $sub_query_2->select('id')
                                ->from(with(new Department())->getTable())
                                ->where('name', 'like', "$department_level%");
                        });
                });
            }
            if(!empty($this->lead_id)){
                $query->where('lead_id', $this->lead_id);
            }
            if(!empty($this->brand_name)){
                $query->where('brand_name', $this->brand_name);
            }
            if(!empty($this->status)){
                $query->where('status', $this->status);
            }
            if(!empty($this->type)){
                $query->where('type', $this->type);
            }

            $data = $query->get();
        }
        return $data;
    }

    public static function findByLead($lead_id)
    {
        $query = self::whereRaw("
                lead_id = '$lead_id'
                OR lead_id LIKE '$lead_id%'
                OR $lead_id LIKE '%,$lead_id,%'
                OR lead_id LIKE '%,$lead_id'
            ")
            ->whereNull('deleted_at')
            ->orderBy('created_at', 'DESC')
        ;
        return $query->get();
    }
}
