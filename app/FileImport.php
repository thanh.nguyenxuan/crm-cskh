<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileImport extends Model
{
    protected $table = 'fptu_import';
}
