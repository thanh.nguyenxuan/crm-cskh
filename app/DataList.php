<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DataList extends Model
{
    protected $table = 'fptu_list';

    public static function getItems($list_id)
    {
        $items = ListItem::where('list_id', $list_id)->orderBy('order')->get();
        return $items;
    }

    public static function findItemName($item_name, $list_id)
    {
        $found = ListItem::where('list_id', $list_id)->where('name', 'LIKE', $item_name)->first();
        if (!empty($found))
            return $found['id'];
        return false;
    }
}

