<?php

namespace App;

use App\Lead\CallLog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class Dashboard
{

    public $start_date;
    public $end_date;
    public $department_id;


    public function searchLead()
    {
        $cache_key = __CLASS__ . '_' . __FUNCTION__
            . '_user_' . Auth::user()->id
            . '_start_date_' . $this->start_date
            . '_end_date_' . $this->end_date
            . '_department_id_' . $this->department_id
        ;
        $data = Cache::store('file')->get($cache_key);
        $cache_timeout = Cache::store('file')->get($cache_key.'_timeout');
        $timeout = 60*1;

        if(!$cache_timeout || time() >= $cache_timeout){
            $data = null;
        }
        if(!$data){
            if (!empty($this->start_date) && !empty($this->end_date)) {
                $start_date = date('Y-m-d 00:00:00', strtotime(str_replace('/', '-', $this->start_date)));
                $end_date = date('Y-m-d 23:59:59', strtotime(str_replace('/', '-', $this->end_date)));
                $query = DB::table('fptu_lead AS t')
                    ->select(
                        'id',
                        'status',
                        'hot_lead_at',
                        'showup_school_at',
                        'activated_at',
                        'new_sale_at',
                        'enrolled_at',
                        DB::raw("(SELECT source_id FROM fptu_lead_source WHERE lead_id = t.id LIMIT 1) AS source_id")
//                        DB::raw('(SELECT id FROM edf_call_log WHERE lead_id = t.id AND status = '.CallLog::STATUS_ANSWERED.' LIMIT 1) as have_log_call'),
//                        DB::raw("(SELECT id FROM fptu_alarm WHERE object_type = 'lead' AND object_id = t.id LIMIT 1) as have_alarm")
                    )
                    ->whereNull('deleted_at')
                    ->where('created_at', '>=', $start_date)
                    ->where('created_at', '<=', $end_date);
                if(!empty($this->department_id)){
                    $query->where('t.department_id', $this->department_id);
                }else{
                    if (!Auth::user()->hasRole('holding') && !in_array(Department::HOLDING, Auth::user()->getManageDepartments())) {
                        $query->where(function ($sub_query) {
                            $sub_query->whereIn('t.department_id', Auth::user()->getManageDepartments())
                                ->orWhereRaw('t.id IN (SELECT lead_id FROM fptu_lead_assignment WHERE user_id = ? AND deleted_at IS NULL)', [Auth::id()]);
                        });
                    }
                }
                if(Auth::user()->hasRole('telesales')){
                    $query->whereRaw('t.id IN (SELECT lead_id FROM fptu_lead_assignment WHERE deleted_at IS NULL AND user_id = '.Auth::user()->id.')');
                }
                $data = $query->get();
                Cache::store('file')->put($cache_key, $data, $timeout);
                Cache::store('file')->put($cache_key.'_timeout', time()+$timeout, $timeout);
            }
        }

        return $data;
    }

    public function getStatCount()
    {
        $data = array(
            'lead'      => array('total' => 0, 'new'        => 0, 'need_call'       => 0, 'have_alarm'  => 0),
            'hot_lead'  => array('total' => 0, 'need_alarm' => 0, 'need_confirm'    => 0),
            'show_up'   => array('total' => 0),
            'wait_list' => array('total' => 0),
            'new_sale'  => array('total' => 0),
            'enroll'    => array('total' => 0),
        );

        $leads = $this->searchLead();

        foreach ($leads as $lead){
            if(!in_array($lead->status, [
                LeadStatus::STATUS_HOT_LEAD,
                LeadStatus::STATUS_SHOWUP_SCHOOL,
                LeadStatus::STATUS_WAITLIST,
                LeadStatus::STATUS_NEW_SALE,
                LeadStatus::STATUS_ENROLL,
            ])) {
                $data['lead']['total']++;
                if($lead->status == LeadStatus::STATUS_NEW){
                    $data['lead']['new']++;
                }
                if(empty($lead->have_call_log)){
                    $data['lead']['need_call']++;
                }
                if(!empty($lead->have_alarm)){
                    $data['lead']['have_alarm']++;
                }
            }
            if($lead->status == LeadStatus::STATUS_HOT_LEAD){
                $data['hot_lead']['total']++;
                if(empty($data->have_alarm)){
                    $data['hot_lead']['need_alarm']++;
                }else{
                    $data['hot_lead']['need_confirm']++;
                }
            }
            if($lead->status == LeadStatus::STATUS_SHOWUP_SCHOOL){
                $data['show_up']['total']++;
            }
            if($lead->status == LeadStatus::STATUS_WAITLIST){
                $data['wait_list']['total']++;
            }
            if($lead->status == LeadStatus::STATUS_NEW_SALE){
                $data['new_sale']['total']++;
            }
            if($lead->status == LeadStatus::STATUS_ENROLL){
                $data['enroll']['total']++;
            }
        }

        return $data;
    }

    public function getConversionRate()
    {
        $data = array(
            'count' => array(
                'lead' => 0,
                'hot_lead' => 0,
                'show_up' => 0,
                'wait_list' => 0,
                'new_sale' => 0,
                'enroll' => 0,
            ),
            'percent' => array(
                'lead' => 0,
                'hot_lead' => 0,
                'show_up' => 0,
                'wait_list' => 0,
                'new_sale' => 0,
                'enroll' => 0,
            )
        );

        $leads = $this->searchLead();

        $count_lead = count($leads);
        $count_hot_lead = 0;
        $count_show_up = 0;
        $count_wait_list = 0;
        $count_new_sale = 0;
        $count_enroll = 0;

        foreach ($leads as $lead) {
            if (!empty($lead->hot_lead_at)) {
                $count_hot_lead++;
            }
            if (!empty($lead->showup_school_at)) {
                $count_show_up++;
            }
            if (!empty($lead->activated_at)) {
                $count_wait_list++;
            }
            if (!empty($lead->new_sale_at)) {
                $count_new_sale++;
            }
            if (!empty($lead->enrolled_at)) {
                $count_enroll++;
            }
        }

        $data['count']['lead'] = $count_lead;
        $data['count']['hot_lead'] = $count_hot_lead;
        $data['count']['show_up'] = $count_show_up;
        $data['count']['wait_list'] = $count_wait_list;
        $data['count']['new_sale'] = $count_new_sale;
        $data['count']['enroll'] = $count_enroll;

        $data['percent']['lead'] = 100 * 100;
        $data['percent']['hot_lead'] = round($count_hot_lead / $count_lead, 2) * 100;
        $data['percent']['show_up'] = round($count_show_up / $count_lead, 2) * 100;
        $data['percent']['wait_list'] = round($count_wait_list / $count_lead, 2) * 100;
        $data['percent']['new_sale'] = round($count_new_sale / $count_lead, 2) * 100;
        $data['percent']['enroll'] = round($count_enroll / $count_lead, 2) * 100;

        return $data;
    }


    public function getSourceRate()
    {
        $data = array(
            'labels' => array(),
            'items' => array()
        );
        $onlineSource = Source::onlineSource();
        $sources = Source::where('hide', 0)->whereIn('id', $onlineSource)->orderBy('sort_index', 'ASC')->get();

        $data['labels'][] = 'Tất cả';
        $data['items'][0]['count']['lead']        = 0;
        $data['items'][0]['count']['hot_lead']    = 0;
        $data['items'][0]['count']['show_up']     = 0;
        $data['items'][0]['count']['wait_list']   = 0;
        $data['items'][0]['count']['new_sale']    = 0;
        $data['items'][0]['count']['enroll']      = 0;

        foreach ($sources as $source){
            $data['labels'][] = $source->name;
            $data['items'][$source->id]['count']['lead']        = 0;
            $data['items'][$source->id]['count']['hot_lead']    = 0;
            $data['items'][$source->id]['count']['show_up']     = 0;
            $data['items'][$source->id]['count']['wait_list']   = 0;
            $data['items'][$source->id]['count']['new_sale']    = 0;
            $data['items'][$source->id]['count']['enroll']      = 0;
        }

        $leads = $this->searchLead();
        foreach ($leads as $lead) {
            if(!empty($lead->source_id) && in_array($lead->source_id, $onlineSource)){
                $data['items'][0]['count']['lead']++;
                $data['items'][$lead->source_id]['count']['lead']++;
                if (!empty($lead->hot_lead_at)) {
                    $data['items'][0]['count']['hot_lead']++;
                    $data['items'][$lead->source_id]['count']['hot_lead']++;
                }
                if (!empty($lead->showup_school_at)) {
                    $data['items'][0]['count']['show_up']++;
                    $data['items'][$lead->source_id]['count']['show_up']++;
                }
                if (!empty($lead->activated_at)) {
                    $data['items'][0]['count']['wait_list']++;
                    $data['items'][$lead->source_id]['count']['wait_list']++;
                }
                if (!empty($lead->new_sale_at)) {
                    $data['items'][0]['count']['new_sale']++;
                    $data['items'][$lead->source_id]['count']['new_sale']++;
                }
                if (!empty($lead->enrolled_at)) {
                    $data['items'][0]['count']['enroll']++;
                    $data['items'][$lead->source_id]['count']['enroll']++;
                }
            }
        }
        $data['items'] = array_values($data['items']);

        return $data;
    }

    public static function getLeadRedirectUrl($params = array()){

        $statusLeadListData = \App\LeadStatus::listDataLead()->toArray();
        $lead_redirect_url = route('lead.index') . '?clear_filter=1&status_id[]=';
        $lead_redirect_url .= implode('&status_id[]=', array_keys($statusLeadListData));
        foreach ($params as $param){
            $lead_redirect_url.= "&$param";
        }
        return $lead_redirect_url;

    }

}
